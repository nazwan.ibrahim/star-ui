
$(function () {
  
    listoftraining();
  

  });
    
  $(document).on("click", ".apply-event", function () {
    let id_training_course = $(this).data("id");
  
    // Fetch the current data for the course
    $.ajax({
      url: `${host}/api/event/${id_training_course}/applyTraining`,
      type: "GET",
      dataType: "JSON",
    })
      .done((res) => {
        // Redirect the user to the edit-training-course page with the course ID as a parameter
        window.location.href = `/admin/apply-event/${id_training_course}`;
      })
      .catch((err) => {
        // Handle error
      });
      
  });
  
  
  
  
      let listoftraining = () => {
  
           let getList = `${host}/api/event/listOfTraining`;
           
  
          $.ajax({
            url: getList,
            type: "GET",
            dataType: "JSON",
          })
            .done((res) => {
              //createTable(res.data);
              tableEventList(res.listoftraining);
            })
            .catch((err) => {});
        };
  
   
  
        let tableEventList = (data) => {
          let senarai = $("#listoftraining  ").DataTable({
            data: data,
            //  order: [[4, "desc"]],
            columnDefs: [
              {
                targets: 0,
                data: null,
                searchable: false,
                orderable: false,
              },
              {
                  targets: 1,
                  data: "training_name",
                  render: function (data, type, full, meta) {
                    if (data === null) {
                      return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
                    }
                    if (data !== null || data !== "") {
                      return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                    }
                  },
                },
                {
                  targets: 2,
                  data: "date_start",
                  render: function (data, type, full, meta) {
                    if (data === null) {
                      return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
                    }
                    if (data !== null || data !== "") {
                      return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                    }
                  },
                },
                {
                  targets: 3,
                  data: "date_end",
                  render: function (data, type, full, meta) {
                    if (data === null) {
                      return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
                    }
                    if (data !== null || data !== "") {
                      return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                    }
                  },
                },
                {
                  targets: 4,
                  data: "venue",
                  render: function (data, type, full, meta) {
                    if (data === null) {
                      return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
                    }
                    if (data !== null || data !== "") {
                      return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                    }
                  },
                },
                {
                  targets: 5,
                  data: "fee",
                  render: function (data, type, full, meta) {
                    if (data === null) {
                      return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
                    }
                    if (data !== null || data !== "") {
                      return `<td width="50%" align="left"><p class="table-content">${data}</p></td>`;
                    }
                  },
                },
                {
                  targets: 6,
                  data: "id_training_app_status",
                  render: function (data, type, full, meta) {              
                      if (data === null) {
                        return `<td width="50%" align="left">-</td>`;
                      } else {
                          if (data === 1) {
                              return `<td width="50%" align="left"><p class="open-status">Open</p></td>`;
                          } else if (data === 2) {
                              return `<td width="50%" align="left"><p class="closed-status">Closed</p></td>`;
                          } else if (data === 3) {
                              return `<td width="50%" align="left"><p class="fully-booked-value">Fully Booked</p></td>`;
                          }
                      }
                  }
              },
            ],
            language: {
              info: "_START_ to _END_ from _TOTAL_ entry",
              infoEmpty: "0 from 0 to 0 entry",
              lengthMenu: " _MENU_ Entry",
              paginate: {
                next: "Next",
                previous: "Previous",
              },
              zeroRecords: "No record found",
            },
          });
        
          senarai
            .on("order.dt search.dt", function () {
              senarai
                .column(0, { search: "applied", order: "applied" })
                .nodes()
                .each(function (cell, i) {
                  cell.innerHTML = i + 1;
                });
            })
            .draw();
        
          $("#filterStatus").on("change", (e) => {
            // console.log(e.target.value)
            senarai.column(5).search(e.target.value).draw();
          });
        
          $("#filterJenisKenderaan").on("change", (e) => {
            console.log(e.target.value);
            senarai.column(1).search(e.target.value).draw();
          });
        
          $("#search_input").on("keyup", () => {
            let searched = $("#search_input").val();
            // console.log(searched)
            senarai.search(searched).draw();
          });
        
          $("#resetFilter").on("click", () => {
            $("#filterStatus").val("").trigger("change");
            $("#filterJenisKenderaan").val("").trigger("change");
            $("#search_input").val("");
        
            senarai.search("").columns().search("").draw();
          });
        };

         // Add a click event handler for the "Attend" button

  
$(function () {
  
    upcomingevent();
       
  });
  
  
  
  
      // let upcomingevent = () => {
  
      //      let getList = `${host}/api/event/upcomingEvent`;
           
  
      //     $.ajax({
      //       url: getList,
      //       type: "GET",
      //       dataType: "JSON",
      //     })
      //       .done((res) => {
      //         //createTable(res.data);
      //         tableUpcomingList(res.upcomingevent);
      //       })
      //       .catch((err) => {});
      //   };
  
   
  
      //   let tableUpcomingList = (data) => {
      //     let senarai = $("#upcomingevent  ").DataTable({
      //       data: data,
      //       //  order: [[4, "desc"]],
      //       columnDefs: [
      //         {
      //           targets: 0,
      //           data: null,
      //           searchable: false,
      //           orderable: false,
      //         },
      //         {
      //             targets: 1,
      //             data: "training_name",
      //             render: function (data, type, full, meta) {
      //               if (data === null) {
      //                 return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
      //               }
      //               if (data !== null || data !== "") {
      //                 return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
      //               }
      //             },
      //           },
      //           {
      //             targets: 2,
      //             data: "date_start",
      //             render: function (data, type, full, meta) {
      //               if (data === null) {
        
      //                 return `<td width="40%" align="left"><p class="table-content">To be announce</p></td>`;
      //               }
      //               if (data !== null || data !== "") {
      //                 return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
      //               }
      //             },
      //           },
      //           {
      //             targets: 3,
      //             data: "date_end",
      //             render: function (data, type, full, meta) {
      //               if (data === null) {
      //                 return `<td width="40%" align="left"><p class="table-content">To be announce</p></td>`;
      //               }
      //               if (data !== null || data !== "") {
      //                 return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
      //               }
      //             },
      //           },
      //           {
      //             targets: 4,
      //             data: "venue",
      //             render: function (data, type, full, meta) {
      //               if (data === null) {
      //                 return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
      //               }
      //               if (data !== null || data !== "") {
      //                 return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
      //               }
      //             },
      //           },
      //           {
      //             targets: 5,
      //             data: "fee",
      //             render: function (data, type, full, meta) {
      //               if (data === null) {
      //                 return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
      //               }
      //               if (data !== null || data !== "") {
      //                 return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
      //               }
      //             },
      //           },
      //           {
      //             targets: 6,
      //             data: "id_training_app_status",
      //             render: function (data, type, full, meta) {              
      //                 if (data === null) {
      //                   return `<td width="50%" align="left">-</td>`;
      //                 } else {
      //                     if (data === 1) {
      //                         return `<td width="50%" align="left"><p class="open-status">Open</p></td>`;
      //                     } else if (data === 2) {
      //                         return `<td width="50%" align="left"><p class="closed-status">Closed</p></td>`;
      //                     } else if (data === 3) {
      //                         return `<td width="50%" align="left"><p class="fully-booked-value">Fully Booked</p></td>`;
      //                     }
      //                 }
      //             }
      //         },
      //       ],
      //       language: {
      //         info: "_START_ to _END_ from _TOTAL_ entry",
      //         infoEmpty: "0 from 0 to 0 entry",
      //         lengthMenu: " _MENU_ Entry",
      //         paginate: {
      //           next: "Next",
      //           previous: "Previous",
      //         },
      //         zeroRecords: "No record found",
      //       },
      //     });
        
      //     senarai
      //       .on("order.dt search.dt", function () {
      //         senarai
      //           .column(0, { search: "applied", order: "applied" })
      //           .nodes()
      //           .each(function (cell, i) {
      //             cell.innerHTML = i + 1;
      //           });
      //       })
      //       .draw();
        
      //     $("#filterStatus").on("change", (e) => {
      //       // console.log(e.target.value)
      //       senarai.column(5).search(e.target.value).draw();
      //     });
        
      //     $("#filterJenisKenderaan").on("change", (e) => {
      //       console.log(e.target.value);
      //       senarai.column(1).search(e.target.value).draw();
      //     });
        
      //     $("#search_input").on("keyup", () => {
      //       let searched = $("#search_input").val();
      //       // console.log(searched)
      //       senarai.search(searched).draw();
      //     });
        
      //     $("#resetFilter").on("click", () => {
      //       $("#filterStatus").val("").trigger("change");
      //       $("#filterJenisKenderaan").val("").trigger("change");
      //       $("#search_input").val("");
        
      //       senarai.search("").columns().search("").draw();
      //     });
      //   };



      let upcomingevent = () => {
  
        let getList = `${host}/api/event/upcomingEvent`;
        

       $.ajax({
         url: getList,
         type: "GET",
         dataType: "JSON",
       })
         .done((res) => {
           //createTable(res.data);
           tableUpcomingList(res.upcomingevent);
         })
         .catch((err) => {});
     };



     let tableUpcomingList = (data) => {
       let senarai = $("#upcomingevent  ").DataTable({
         data: data,
         //  order: [[4, "desc"]],
         columnDefs: [
           {
             targets: 0,
             data: null,
             searchable: false,
             orderable: false,
           },
           {
               targets: 1,
               data: "training_name",
               render: function (data, type, full, meta) {
                 if (data === null) {
                   return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
                 }
                 if (data !== null || data !== "") {
                   return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                 }
               },
             },
             {
               targets: 2,
               data: "course_category",
               render: function (data, type, full, meta) {
                 if (data === null) {
     
                   return `<td width="40%" align="left"><p class="table-content">To be announce</p></td>`;
                 }
                 if (data !== null || data !== "") {
                   return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                 }
               },
             },
             {
               targets: 3,
               data: "training_type",
               render: function (data, type, full, meta) {
                 if (data === null) {
                   return `<td width="40%" align="left"><p class="table-content">To be announce</p></td>`;
                 }
                 if (data !== null || data !== "") {
                   return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                 }
               },
             },
             {
               targets: 4,
               data: "batch_no_full",
               render: function (data, type, full, meta) {
                 if (data === null) {
                   return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
                 }
                 if (data !== null || data !== "") {
                   return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                 }
               },
             },
             {
               targets: 5,
               data: "date_start",
               render: function (data, type, full, meta) {
                 if (data === null) {
                   return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
                 }
                 if (data !== null || data !== "") {
                   return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                 }
               },
             },
             {
              targets: 6,
              data: "date_end",
              render: function (data, type, full, meta) {
                if (data === null) {
                  return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
                }
                if (data !== null || data !== "") {
                  return `<td align="left" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                }
              },
            },
            {
              targets: 7,
              data: "id_approval_status",
              render: function (data, type, full, meta) {
                  if (data === 1) {
                      return `<td width="50%" align="left"><p class="null-value">Pending Approval</p></td>`;
                  } else if (data === 2){
                      return `<td width="50%" align="left"><p class="success-status">Approved</p></td>`;
                  } 
                  else if (data === 3){
                      return `<td width="50%" align="left"><p class="failed-status">Rejected</p></td>`;
                  } 
                  else {
                      return `<td width="50%" align="left">-</td>`;
                  }
              }
          },
         ],
         language: {
           info: "_START_ to _END_ from _TOTAL_ entry",
           infoEmpty: "0 from 0 to 0 entry",
           lengthMenu: " _MENU_ Entry",
           paginate: {
             next: "Next",
             previous: "Previous",
           },
           zeroRecords: "No record found",
         },
       });
     
       senarai
         .on("order.dt search.dt", function () {
           senarai
             .column(0, { search: "applied", order: "applied" })
             .nodes()
             .each(function (cell, i) {
               cell.innerHTML = i + 1;
             });
         })
         .draw();
     
       $("#filterStatus").on("change", (e) => {
         // console.log(e.target.value)
         senarai.column(5).search(e.target.value).draw();
       });
     
       $("#filterJenisKenderaan").on("change", (e) => {
         console.log(e.target.value);
         senarai.column(1).search(e.target.value).draw();
       });
     
       $("#search_input").on("keyup", () => {
         let searched = $("#search_input").val();
         // console.log(searched)
         senarai.search(searched).draw();
       });
     
       $("#resetFilter").on("click", () => {
         $("#filterStatus").val("").trigger("change");
         $("#filterJenisKenderaan").val("").trigger("change");
         $("#search_input").val("");
     
         senarai.search("").columns().search("").draw();
       });
     };