$(function() {

  courselist();

  // $(document).on("click", ".delete-course", function() {
  //     let id_training_course = $(this).data("id");

  //     deleteList(id_training_course);
  // });

  // $(document).on("click", ".update-course", function() {
  //     let id_training_course = $(this).data("id");

  //     updateList(id_training_course);
  // });
});

// let deleteList = (id_training_course) => {
//   let deleteLink = `${host}/api/training/${id_training_course}/deleteCourseTraining`;

//   $.ajax({
//           url: deleteLink,
//           type: "DELETE",
//           dataType: "JSON",
//       })
//       .done((res) => {
//           courselist();


//       })
//       .catch((err) => {});
// };

// $(document).on("click", ".update-course", function() {
//   let id_training_course = $(this).data("id");

//   // Fetch the current data for the course
//   $.ajax({
//           url: `${host}/api/training/${id_training_course}/showCourseTraining`,
//           type: "GET",
//           dataType: "JSON",
//       })
//       .done((res) => {
//           // Redirect the user to the edit-training-course page with the course ID as a parameter
//           window.location.href = `/admin/edit-training-course/${id_training_course}`;
//       })
//       .catch((err) => {
//           // Handle error
//       });

// });




let courselist = () => {

  let getList = `${host}/api/parameter/countryList`;


  $.ajax({
          url: getList,
          type: "GET",
          dataType: "JSON",
      })
      .done((res) => {
          //createTable(res.data);
          tableCountryList(res.countrylist);
      })
      .catch((err) => {});
};



let tableCountryList = (data) => {
  console.log(data);
  let senarai = $("#countryManagementList").DataTable({
      data: data,
      dom: 'lfrtip',
      ordering: false,
      initComplete: function() {
          this.api().columns().every(function() {
              var column = this;
              $('input', column.header()).on('keyup change clear', function() {
                  if (column.search() !== this.value) {
                      // Use case-insensitive search for the "Status" column (index 5)
                      column.search(this.value, false, true).draw();
                  }
              });
          });
      },
      responsive: true,
      columns: [{
              data: null,
              searchable: false,
              orderable: false,
              render: function(data, type, full, meta) {
                  return meta.row + 1;
              }
          },

          {
              targets: 1,
              data: "name",
              render: function(data, type, full, meta) {
                  if (data === null) {
                      return `<td width="50%" align="left"><p class="table-content">-</p></td>`;
                  }
                  if (data !== null && data !== "") {
                      return `<td align="left" width="50%">
                               ${data}
                          </td>`;
                  }
              },
          },
          {
            targets: 2,
            data: "iso",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
              }
              if (data !== null || data !== "") {
                return `<td align="left" width="40%"><p class="table-content" style="text-align: left;">${data}</p></td>`;
              }
            },
          },
          {
            targets: 3,
            data: "iso3",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
              }
              if (data !== null || data !== "") {
                return `<td align="left" width="40%"><p class="table-content" style="text-align: left;">${data}</p></td>`;
              }
            },
          },
          {
            targets: 4,
            data: "numcode",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
              }
              if (data !== null || data !== "") {
                return `<td align="left" width="40%"><p class="table-content" style="text-align: left;">${data}</p></td>`;
              }
            },
          },
          {
            targets: 5,
            data: "phonecode",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
              }
              if (data !== null || data !== "") {
                return `<td align="left" width="40%"><p class="table-content" style="text-align: left;">${data}</p></td>`;
              }
            },
          },
          {
              targets: 6,
              data: "id",
              render: function(data, type, full, meta) {
                  // Assuming you have URLs for view, edit, and delete actions
                  if (data !== null && data !== "") {
                      return `<div>
                            <a href="${host}/parameterManagement/edit-country/${data}" class="btn btn-sm" data-id="${data}">
                            <i class="fas fa-edit" style="font-size: 16px; color: #3498DB;"></i>
                          </a>
                            <a href="${host}/parameterManagement/delete-country/${data}" class="btn btn-sm delete-course" data-id="${data}">
                            <i class="fas fa-trash-alt" style="font-size: 16px; color: #3498DB;"></i>
                            </a>
                          </div>`;
                  } else {
                      return "-";
                  }
              },
          }
      ],
      lengthMenu: [5, 10, 15, 20],
      pageLength: 20,
      language: {
          info: "_START_ to _END_ from _TOTAL_ entry",
          infoEmpty: "0 from 0 to 0 entry",
          lengthMenu: " _MENU_ Entry",
          paginate: {
              next: "Next",
              previous: "Previous",
          },
          zeroRecords: "No record found",
      },
  });

  var addUserButton = $('<button id="addUserButton" class="circular-button" style="color: white">' +
      '<i class="fas fa-plus"></i>' +
      '</button>');

  addUserButton.on('click', function() {
      window.location.href = "/parameterManagement/add-country";
  });

  addUserButton.css({
      width: '37px',
      height: '37px',
      backgroundColor: '#3498DB',
      border: '2px solid #3498DB',
      borderRadius: '50%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      cursor: 'pointer',
  });

  addUserButton.css('margin-left', '10px');
  $('#countryManagementList_wrapper .dataTables_filter').append(addUserButton);

  senarai
      .on("order.dt search.dt", function() {
          senarai
              .column(0, {
                  search: "applied",
                  order: "applied"
              })
              .nodes()
              .each(function(cell, i) {
                  cell.innerHTML = i + 1;
              });
      })
      .draw();

  $("#filterStatus").on("change", (e) => {
      // console.log(e.target.value)
      senarai.column(5).search(e.target.value).draw();
  });

  $("#filterJenisKenderaan").on("change", (e) => {
      console.log(e.target.value);
      senarai.column(1).search(e.target.value).draw();
  });

  $("#search_input").on("keyup", () => {
      let searched = $("#search_input").val();
      // console.log(searched)
      senarai.search(searched).draw();
  });

  $("#resetFilter").on("click", () => {
      $("#filterStatus").val("").trigger("change");
      $("#filterJenisKenderaan").val("").trigger("change");
      $("#search_input").val("");

      senarai.search("").columns().search("").draw();
  });
};


//////


// function updateCourse() {
//   // Define variables
//   const userID = "{{ $userid }}"; // Assuming this is defined elsewhere in your script
//   const id_training_course = "{{ $courselist[0]->id_training_course }}"; // Assuming this is defined elsewhere in your script
//   const host = "{{ env('API_SERVER') }}";

//   // Prepare the data to send to the server
//   const courseName = $('#courseName').val();
//   const courseCategory = $('#courseCategory').val();
//   const trainingType = $('#trainingType').val();
//   const courseDescription = $('#courseDescription').val();
//   const certificateCode = $('#certificateCode').val();
//   const isFree = $('#is_free').prop('checked') ? 1 : 0;
//   const isPaid = $('#is_paid').prop('checked') ? 1 : 0;
//   const fee = $('#Fee').val();
//   const receipt = $('#resitFee').val();
//   const reTypes = []; // Array to store selected re_types

//   // Get selected re_types
//   $('input[name="re_types[]"]:checked').each(function() {
//       reTypes.push($(this).val());
//   });

//   // Prepare the data object to send in the request
//   const data = {
//       course_name: courseName,
//       course_category: courseCategory,
//       training_type: trainingType,
//       course_description: courseDescription,
//       is_free: isFree,
//       is_paid: isPaid,
//       fee: fee,
//       receipt: receipt,
//       re_types: reTypes,
//   };

//   // Create the URL for the AJAX request
//   const putCourseInfo = `${host}/api/training/${id_training_course}/updateCourseTraining`;

//   // Perform the AJAX request
//   $.ajax({
//       url: putCourseInfo,
//       type: 'PUT', // Assuming you're using the PUT method for updates
//       data: data,
//       dataType: 'json',
//       success: function(response) {
//           // Handle the success response here
//           console.log('Course updated successfully', response);
//           // You can redirect to another page or show a success message here
//           window.location.href = `${host}/admin/training-course-management`;
//       },
//       error: function(xhr, status, error) {
//           // Handle errors here
//           console.error('Error updating course', xhr.responseText);
//           // You can display an error message to the user
//       },
//   });
// }