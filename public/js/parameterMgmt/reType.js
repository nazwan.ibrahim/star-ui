$(function() {

  reTypelist();
});


let reTypelist = () => {

  let getList = `${host}/api/parameter/reTypeList`;


  $.ajax({
          url: getList,
          type: "GET",
          dataType: "JSON",
      })
      .done((res) => {
          //createTable(res.data);
          tablereTypeList(res.reTypelist);
      })
      .catch((err) => {});
};



let tablereTypeList = (data) => {
  console.log(data);
  let senarai = $("#reTypeManagementList").DataTable({
      data: data,
      dom: 'lfrtip',
      ordering: false,
      initComplete: function() {
          this.api().columns().every(function() {
              var column = this;
              $('input', column.header()).on('keyup change clear', function() {
                  if (column.search() !== this.value) {
                      // Use case-insensitive search for the "Status" column (index 5)
                      column.search(this.value, false, true).draw();
                  }
              });
          });
      },
      responsive: true,
      columns: [{
              data: null,
              searchable: false,
              orderable: false,
              render: function(data, type, full, meta) {
                  return meta.row + 1;
              }
          },
          {
              targets: 1,
              data: "re_type",
              render: function(data, type, full, meta) {
                  if (data === null) {
                      return `<td width="50%" align="left"><p class="table-content">-</p></td>`;
                  }
                  if (data !== null && data !== "") {
                      return `<td align="left" width="50%">
                                 ${data}
                          </td>`;
                  }
              },
          },
          {
            targets: 2,
            data: "status",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
              }
              if (data !== null || data !== "") {
                if (data === 1) {
                  return  `<td width="15%"><p class="active-status" style="text-align: center;">ACTIVE</p></td>`;
                } else if (data === 0 || data ===null) {
                  return  `<td width="15%"><p class="inactive-status" style="text-align: center;">INACTIVE</p></td>`;
                } else {
                  return "-";
                }
               
              }
            },
          },
          {
              targets: 4,
              data: "id",
              render: function(data, type, full, meta) {
                  // Assuming you have URLs for view, edit, and delete actions
                  if (data !== null && data !== "") {
                      return `<div>
                            <a href="${host}/parameterManagement/edit-reType/${data}" class="btn btn-sm" data-id="${data}">
                            <i class="fas fa-edit" style="font-size: 16px; color: #3498DB;"></i>
                          </a>
                            <a href="${host}/parameterManagement/delete-reType/${data}" class="btn btn-sm delete-course" data-id="${data}">
                            <i class="fas fa-trash-alt" style="font-size: 16px; color: #3498DB;"></i>
                            </a>
                          </div>`;
                  } else {
                      return "-";
                  }
              },
          }
      ],
      lengthMenu: [5, 10, 15, 20],
      pageLength: 20,
      language: {
          info: "_START_ to _END_ from _TOTAL_ entry",
          infoEmpty: "0 from 0 to 0 entry",
          lengthMenu: " _MENU_ Entry",
          paginate: {
              next: "Next",
              previous: "Previous",
          },
          zeroRecords: "No record found",
      },
  });

  var addUserButton = $('<button id="addUserButton" class="circular-button" style="color: white">' +
      '<i class="fas fa-plus"></i>' +
      '</button>');

  addUserButton.on('click', function() {
      window.location.href = "/parameterManagement/add-reType";
  });

  addUserButton.css({
      width: '37px',
      height: '37px',
      backgroundColor: '#3498DB',
      border: '2px solid #3498DB',
      borderRadius: '50%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      cursor: 'pointer',
  });

  addUserButton.css('margin-left', '10px');
  $('#reTypeManagementList_wrapper .dataTables_filter').append(addUserButton);

  senarai
      .on("order.dt search.dt", function() {
          senarai
              .column(0, {
                  search: "applied",
                  order: "applied"
              })
              .nodes()
              .each(function(cell, i) {
                  cell.innerHTML = i + 1;
              });
      })
      .draw();

  $("#filterStatus").on("change", (e) => {
      // console.log(e.target.value)
      senarai.column(5).search(e.target.value).draw();
  });

  $("#filterJenisKenderaan").on("change", (e) => {
      console.log(e.target.value);
      senarai.column(1).search(e.target.value).draw();
  });

  $("#search_input").on("keyup", () => {
      let searched = $("#search_input").val();
      // console.log(searched)
      senarai.search(searched).draw();
  });

  $("#resetFilter").on("click", () => {
      $("#filterStatus").val("").trigger("change");
      $("#filterJenisKenderaan").val("").trigger("change");
      $("#search_input").val("");

      senarai.search("").columns().search("").draw();
  });
};
