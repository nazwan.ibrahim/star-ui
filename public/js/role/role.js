$(function () {
    roleList();
});

let roleList = () => {
    let getList = `${host}/api/role/rolelist`;

    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((res) => {

        tableRoleList(res.roleList);

    })
    .catch((err) => {
        // Handle error
    });
};
  
   
  
let tableRoleList = (data) => {
    let senarai = $("#roleList").DataTable({
        data: data,
        dom: 'lrtip',
        
        // Rest of your configuration
        
        initComplete: function () {
            // Clone the header row (thead) and add search inputs for the first three columns
            var headerRow = $('#roleList thead tr').clone(true).addClass('filters');
            
            $(headerRow).find('th').each(function (index) {
                var input;
                if (index === 1 || index === 2 || index === 3) {
                    input = $('<input type="text" placeholder="Search..."/>')
                        .on('keyup', function () {
                            senarai.column(index).search(this.value).draw();
                        });
                }1
                if (index !== 1 && index !== 2 || index !== 3) {
                    $(this).text('');
                }
                $(this).html(input);
            });
            
            // Append the modified cloned row to the DataTable header
            $(headerRow).appendTo('#roleList thead');
        },
    
            
            //  order: [[4, "desc"]],
            columnDefs: [
              {
                targets: 0,
                data: null,
                searchable: false,
                orderable: false,
              },
              {
                targets: 1,
                data: "code",
                render: function (data, type, full, meta) {
                  if (data === null) {
                    return `<td align="left" width="40%"><p class="poppins-semibold-14">-</p></td>`;
                  }
                  if (data !== null || data !== "") {
                    return `<td align="left" width="40%"><p class="poppins-semibold-14" style="text-align: left;">${data}</p></td>`;
                  }
                },
              },
              {
                  targets: 2,
                  data: "role",
                  render: function (data, type, full, meta) {
                    if (data === null) {
                      return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
                    }
                    if (data !== null || data !== "") {
                      return `<td align="left" width="40%"><p class="poppins-semibold-14" style="text-align: left;">${data}</p></td>`;
                    }
                  },
                },
                {
                    targets: 3,
                    data: "status",
                    render: function (data, type, full, meta) {
                        if (data === null) {
                            return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
                          }
                          if (data !== null || data !== "") {
                            let statusText = '';
                            if (data === 1) {
                                statusText = 'Active';
                            } else if (data === 0) {
                                statusText = 'Inactive';
                            }
                            return `<td align="left" width="40%"><p class="poppins-semibold-14" style="text-align: left;">${statusText}</p></td>`;
                        }
                    },
                  },
            
              {
                targets: 4,
                data: "id",
                render: function (data, type, full, meta) {
                
                  if (data !== null && data !== "") {
                    return `<div>
                              <a href="${host}/role/edit-role/${data}" class="btn btn-sm update-course" data-id="${data}">
                                <i class="" style="font-size: 18px;"></i> Edit
                              </a>
                              <a href="${host}/role/deleteRole/${data}" class="btn btn-sm delete-course" data-id="${data}">
                              <i class="" style="font-size: 18px;"></i> Delete
                              </a>
                            </div>`;
                  } else {
                    return "-";
                  }
                },
              }
            ],
            language: {
              info: "show _START_ to _END_ from _TOTAL_ entry",
              infoEmpty: "show 0 from 0 to 0 entry",
              lengthMenu: " _MENU_ Entry",
              paginate: {
                next: "Next",
                previous: "Previous",
              },
              zeroRecords: "No record found",
            },
          });
        
          senarai
            .on("order.dt search.dt", function () {
              senarai
                .column(0, { search: "applied", order: "applied" })
                .nodes()
                .each(function (cell, i) {
                  cell.innerHTML = i + 1;
                });
            })
            .draw();
        
          $("#filterStatus").on("change", (e) => {
            // console.log(e.target.value)
            senarai.column(5).search(e.target.value).draw();
          });
        
          $("#filterJenisKenderaan").on("change", (e) => {
            console.log(e.target.value);
            senarai.column(1).search(e.target.value).draw();
          });
        
          $("#search_input").on("keyup", () => {
            let searched = $("#search_input").val();
            // console.log(searched)
            senarai.search(searched).draw();
          });
        
          $("#resetFilter").on("click", () => {
            $("#filterStatus").val("").trigger("change");
            $("#filterJenisKenderaan").val("").trigger("change");
            $("#search_input").val("");
        
            senarai.search("").columns().search("").draw();
          });
        };
        

  
  
       
     
     
  
  
        
  