
$(function(){

  displayProfile();


});

function displayProfile(){

    let userid = userID;
    let getUserInfo = `${host}/api/profile/${userid}/userInformation/`;

   // console.log(getUserInfo);

  $.ajax({

  url: getUserInfo,
    type: 'GET',
    contentType: "application/json; charset-utf-8",
    dataType: 'json',
    success: function (response) {
      let info = response.userInfo;
      console.log(info, 'info');

      $.each(info, function(index, value) {

          (value.fullname == null || value.fullname == '') ? $('#FullName').val('n/a') : $('#FullName').val(value.fullname);
          (value.ic_no == null || value.ic_no == '') ? $('#IcNo').val('n/a') : $('#IcNo').val(value.ic_no);
          (value.gender == null || value.gender == '') ? $('#Gender').val('n/a') : $('#Gender').val(value.gender);
          (value.phone_no == null || value.phone_no == '') ? $('#PhoneNumber').val('n/a') : $('#PhoneNumber').val(value.phone_no);
          (value.address_1 == null || value.address_1 == '') ? $('#AddressLine1').val('n/a') : $('#AddressLine1').val(value.address_1);
          (value.address_2 == null || value.address_2 == '') ? $('#AddressLine2').val('n/a') : $('#AddressLine2').val(value.address_2);
          (value.postcode == null || value.postcode == '') ? $('#Postcode').val('n/a') : $('#Postcode').val(value.postcode);
          (value.email == null || value.email == '') ? $('#EmailAddress').val('n/a') : $('#EmailAddress').val(value.email);
          (value.position == null || value.position == '') ? $('#Position').val('n/a') : $('#Position').val(value.position);
          (value.statename == null || value.statename == '') ? $('#State').val('n/a') : $('#State').val(value.statename);
          (value.countryname == null || value.countryname == '') ? $('#Country').val('n/a') : $('#Country').val(value.countryname);
          (value.bumi_status == null || value.bumi_status == '') ? $('#Status').val('n/a') : $('#Status').val(value.bumi_status);
          (value.qualification == null || value.qualification == '') ? $('#Qualification').val('n/a') : $('#Qualification').val(value.qualification);
          (value.education_level == null || value.education_level == '') ? $('#EducationLevel').val('n/a') : $('#EducationLevel').val(value.education_level);
          (value.companyname == null || value.companyname == '') ? $('#CompanyName').val('n/a') : $('#CompanyName').val(value.companyname);
          (value.company_address_1 == null || value.company_address_1 == '') ? $('#CompanyAddressLine1').val('n/a') : $('#CompanyAddressLine1').val(value.company_address_1);
          (value.company_address_2 == null || value.company_address_2 == '') ? $('#CompanyAddressLine2').val('n/a') : $('#CompanyAddressLine2').val(value.company_address_2);

          (value.fullname == null || value.fullname == '') ? $('#nameAtPic').text('n/a') : $('#nameAtPic').text(value.fullname);

         
      });

      }
  });
}


  function updateInfo(){
     // alert('ts');
     let userid = userID;

       let putUserInfo = `${host}/api/profile/${userid}/UpdateUserInformation`;

       console.log('update',putUserInfo);
        var detailsData = {

            "UserID": userid,
            "FullName" : $("#FullName").val(),
            "IcNo" : $("#IcNo").val(),
            "Gender" : $("#Gender").val(),

            "PhoneNumber" : $("#PhoneNumber").val(),
            "AddressLine1" : $("#AddressLine1").val(),
            "AddressLine2" : $("#AddressLine2").val(),

            "Postcode" : $("#Postcode").val(),
            "EmailAddress" : $("#EmailAddress").val(),
            "Position" : $("#Position").val(),

            "State" : $("#State").val(),
            "Country" : $("#Country").val(),
            "Status" : $("#Status").val(),

            "Qualification" : $("#Qualification").val(),
            "EducationLevel" : $("#EducationLevel").val(),

            "CompanyName" : $("#CompanyName").val(),
            "CompanyAddressLine1" : $("#CompanyAddressLine1").val(),
            "CompanyAddressLine2" : $("#CompanyAddressLine2").val(),
       
          }
    
    
    console.log(detailsData);
     //alert(putResetPassword)
   // event.preventDefault(); // Prevent the form from submitting
   
    var settings = {
      "url": putUserInfo,
      "method": "PUT",
      "timeout": 0,
      "data": detailsData,
      "dataType": 'json',
    };
//alert(settings)
    $.ajax(settings).done(function (response) {
     //alert("Ajax request successful");
      // setTimeout( () => {
      //   $("#loadingModal").modal("hide");
      //   $("#loadingModal").removeClass("in");
      //   $('body').removeClass('modal-open');
      //   $("#berjaya-simpan-modal").modal({
      //     backdrop: 'static', 
      //     keyboard: false
      //   },"show");
      //   $("#loadingModal").hide();
      // }, 2000);

      // console.log(response);
     // $('#berjaya-simpan-modal').on('hidden.bs.modal', function () {
        window.location.href = `${host}/profile/userProfile`;
       

    //  });
    });
  }


  //upload

var maxFileSize = 8000000; //8mb
var isErrorFileType = false;
var isErrorFileSize = false;
var uploadedFilesArray = [];
var uploadedFilesArray1 = [];
var uploadedEditedFilesArray = [];
var dataForm = new FormData();

  function onChangeUploadFile() {
    // reset
    isErrorFileType = false;
    isErrorFileSize = false;
    uploadedFilesArray = [];
    var allowedExtensions = ["jpg", "jpeg", "png", "img"];
  
    $.each($('#uploadCoverImg-0')[0].files, function (index, val) {
      uploadedFilesArray.push(this);
  
      var fileExtension = val.name.split('.').pop().toLowerCase();
      var isValidExtension = allowedExtensions.includes(fileExtension);
  
      // if (val.type != "image/jpeg" && val.type != "image/jpg" && val.type != "image/png") {
      //   isErrorFileType = true;
      // }
  
      if (!isValidExtension) {
        isErrorFileType = true;
      }
  
      if (val.size > maxFileSize) {
        isErrorFileSize = true;
      }
    });
  
    if (isErrorFileType) {
      $('#error-uploadedImageInvalidType').show();
    } else {
      $('#error-uploadedImageInvalidType').hide();
    }
  
    if (isErrorFileSize) {
      $('#error-uploadedMaxSizeLimitReached').show();
    } else {
      $('#error-uploadedMaxSizeLimitReached').hide();
    }
  
    if ($('#uploadCoverImg-0')[0].files[0]) {
      $('#error-uploadCoverImg').hide();
    }
  
    processUploadedImage();
  }
  

  function processUploadedImage() {
    if (!uploadedFilesArray.length) {
  
      $('#error-uploadedPic').show();
      $('#parentDivUploadedFileDisplay-0').html('');
      $('#parentDivUploadedFileDisplay-0').hide();
      $('#btnuploadfileParentDiv-0').show();
    } else {
  
      $('#btnuploadfileParentDiv-0').hide();
      $('#parentDivUploadedFileDisplay-0').show();
      $('#parentDivUploadedFileDisplay-0').html('');
  
      for (var i = 0; i < uploadedFilesArray.length; i++) {
        $('#parentDivUploadedFileDisplay-0').append(`
          <div class="book-cover-card-body">
            <img id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="book cover" height="160">
            <div class="book-cover-card-body">
              <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
            </div>
            <a  onclick="padamFail(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
          </div>`);
      }
    }
    if (uploadedFilesArray.length != 0) {
      //reset data
      dataForm = new FormData();
  
      $.each(uploadedFilesArray, function (index, val) {
        let file = val;
        dataForm.append('file' + index, file);
      });
    }
  }


  function uploadButton() {
    $('#uploadCoverImg-0').val(null);
    $('#uploadCoverImg-0').trigger('click');
  }

  
function padamFail(fileIndex) {
  uploadedFilesArray.splice(fileIndex, 1);
  processUploadedImage();
}

function processUploadedImage() {
  if (!uploadedFilesArray.length) {

    $('#error-uploadedGambarBilik').show();
    $('#parentDivUploadedFileDisplay-0').html('');
    $('#parentDivUploadedFileDisplay-0').hide();
    $('#btnuploadfileParentDiv-0').show();
  } else {

    $('#btnuploadfileParentDiv-0').hide();
    $('#parentDivUploadedFileDisplay-0').show();
    $('#parentDivUploadedFileDisplay-0').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-0').append(`
        <div class="book-cover-card-body">
          <img id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="book cover" height="160">
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="padamFail(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}

////////////////upload ic
function onChangeUploadFileIC() {
  // reset
  isErrorFileType = false;
  isErrorFileSize = false;
  uploadedFilesArray = [];
  var allowedExtensions = ["jpg", "jpeg", "png", "img"];

  $.each($('#uploadLampiranImg-1')[0].files, function (index, val) {
    uploadedFilesArray.push(this);

    var fileExtension = val.name.split('.').pop().toLowerCase();
    var isValidExtension = allowedExtensions.includes(fileExtension);

    // if (val.type != "image/jpeg" && val.type != "image/jpg" && val.type != "image/png") {
    //   isErrorFileType = true;
    // }

    if (!isValidExtension) {
      isErrorFileType = true;
    }

    if (val.size > maxFileSize) {
      isErrorFileSize = true;
    }
  });

  if (isErrorFileType) {
    $('#error-uploadedImageInvalidType').show();
  } else {
    $('#error-uploadedImageInvalidType').hide();
  }

  if (isErrorFileSize) {
    $('#error-uploadedMaxSizeLimitReached').show();
  } else {
    $('#error-uploadedMaxSizeLimitReached').hide();
  }

  if ($('#uploadLampiranImg-1')[0].files[0]) {
    $('#error-uploadCoverImg').hide();
  }

  processUploadedImageIC();
}


function processUploadedImageIC() {
  if (!uploadedFilesArray.length) {

    $('#error-uploadedPic').show();
    $('#parentDivUploadedFileDisplay-1').html('');
    $('#parentDivUploadedFileDisplay-1').hide();
    $('#btnuploadfileParentDiv-0').show();
  } else {

    $('#btnuploadfileParentDiv-0').hide();
    $('#parentDivUploadedFileDisplay-1').show();
    $('#parentDivUploadedFileDisplay-1').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-1').append(`
        <div class="book-cover-card-body">
          <img id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="book cover" height="160">
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="padamFailIC(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}


function uploadButtonIC() {
  $('#uploadLampiranImg-1').val(null);
  $('#uploadLampiranImg-1').trigger('click');
}


function padamFailIC(fileIndex) {
  uploadedFilesArray.splice(fileIndex, 1);
  processUploadedImageIC();
}

function processUploadedImageIC() {
  if (!uploadedFilesArray.length) {

    $('#error-uploadedGambarBilik').show();
    $('#parentDivUploadedFileDisplay-1').html('');
    $('#parentDivUploadedFileDisplay-1').hide();
    $('#btnuploadfileParentDiv-0').show();
  } else {

    $('#btnuploadfileParentDiv-0').hide();
    $('#parentDivUploadedFileDisplay-1').show();
    $('#parentDivUploadedFileDisplay-1').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-1').append(`
        <div class="book-cover-card-body">
          <img id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="book cover" height="160">
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="padamFailIC(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}

////////////upload passport
function onChangeUploadFilePassport() {
  // reset
  isErrorFileType = false;
  isErrorFileSize = false;
  uploadedFilesArray = [];
  var allowedExtensions = ["jpg", "jpeg", "png", "img"];

  $.each($('#uploadLampiranImg-2')[0].files, function (index, val) {
    uploadedFilesArray.push(this);

    var fileExtension = val.name.split('.').pop().toLowerCase();
    var isValidExtension = allowedExtensions.includes(fileExtension);

    // if (val.type != "image/jpeg" && val.type != "image/jpg" && val.type != "image/png") {
    //   isErrorFileType = true;
    // }

    if (!isValidExtension) {
      isErrorFileType = true;
    }

    if (val.size > maxFileSize) {
      isErrorFileSize = true;
    }
  });

  if (isErrorFileType) {
    $('#error-uploadedImageInvalidType').show();
  } else {
    $('#error-uploadedImageInvalidType').hide();
  }

  if (isErrorFileSize) {
    $('#error-uploadedMaxSizeLimitReached').show();
  } else {
    $('#error-uploadedMaxSizeLimitReached').hide();
  }

  if ($('#uploadLampiranImg-1')[0].files[0]) {
    $('#error-uploadCoverImg').hide();
  }

  processUploadedImagePassport();
}


function processUploadedImagePassport() {
  if (!uploadedFilesArray.length) {

    $('#error-uploadedPic').show();
    $('#parentDivUploadedFileDisplay-2').html('');
    $('#parentDivUploadedFileDisplay-2').hide();
    $('#btnuploadfileParentDiv-0').show();
  } else {

    $('#btnuploadfileParentDiv-0').hide();
    $('#parentDivUploadedFileDisplay-2').show();
    $('#parentDivUploadedFileDisplay-2').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-2').append(`
        <div class="book-cover-card-body">
          <img id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="book cover" height="160">
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="padamFailPassport(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}


function uploadButtonPassport() {
  $('#uploadLampiranImg-2').val(null);
  $('#uploadLampiranImg-2').trigger('click');
}


function padamFailPassport(fileIndex) {
  uploadedFilesArray.splice(fileIndex, 1);
  processUploadedImagePassport();
}

function processUploadedImagePassport() {
  if (!uploadedFilesArray.length) {

    $('#error-uploadedGambarBilik').show();
    $('#parentDivUploadedFileDisplay-2').html('');
    $('#parentDivUploadedFileDisplay-2').hide();
    $('#btnuploadfileParentDiv-0').show();
  } else {

    $('#btnuploadfileParentDiv-0').hide();
    $('#parentDivUploadedFileDisplay-2').show();
    $('#parentDivUploadedFileDisplay-2').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-2').append(`
        <div class="book-cover-card-body">
          <img id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="book cover" height="160">
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="padamFailPassport(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}

////////////upload scan document
function onChangeUploadFileDoc() {
  // reset
  isErrorFileType = false;
  isErrorFileSize = false;
  uploadedFilesArray = [];
  var allowedExtensions = ["jpg", "jpeg", "png", "img"];

  $.each($('#uploadLampiranImg-3')[0].files, function (index, val) {
    uploadedFilesArray.push(this);

    var fileExtension = val.name.split('.').pop().toLowerCase();
    var isValidExtension = allowedExtensions.includes(fileExtension);

    // if (val.type != "image/jpeg" && val.type != "image/jpg" && val.type != "image/png") {
    //   isErrorFileType = true;
    // }

    if (!isValidExtension) {
      isErrorFileType = true;
    }

    if (val.size > maxFileSize) {
      isErrorFileSize = true;
    }
  });

  if (isErrorFileType) {
    $('#error-uploadedImageInvalidType').show();
  } else {
    $('#error-uploadedImageInvalidType').hide();
  }

  if (isErrorFileSize) {
    $('#error-uploadedMaxSizeLimitReached').show();
  } else {
    $('#error-uploadedMaxSizeLimitReached').hide();
  }

  if ($('#uploadLampiranImg-3')[0].files[0]) {
    $('#error-uploadCoverImg').hide();
  }

  processUploadedImageDoc();
}


function processUploadedImageDoc() {
  if (!uploadedFilesArray.length) {

    $('#error-uploadedPic').show();
    $('#parentDivUploadedFileDisplay-3').html('');
    $('#parentDivUploadedFileDisplay-3').hide();
    $('#btnuploadfileParentDiv-0').show();
  } else {

    $('#btnuploadfileParentDiv-0').hide();
    $('#parentDivUploadedFileDisplay-3').show();
    $('#parentDivUploadedFileDisplay-3').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-3').append(`
        <div class="book-cover-card-body">
          <img id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="book cover" height="160">
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="padamFailDoc(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}


function uploadButtonDoc() {
  $('#uploadLampiranImg-3').val(null);
  $('#uploadLampiranImg-3').trigger('click');
}


function padamFailDoc(fileIndex) {
  uploadedFilesArray.splice(fileIndex, 1);
  processUploadedImageDoc();
}

function processUploadedImageDoc() {
  if (!uploadedFilesArray.length) {

    $('#error-uploadedGambarBilik').show();
    $('#parentDivUploadedFileDisplay-3').html('');
    $('#parentDivUploadedFileDisplay-3').hide();
    $('#btnuploadfileParentDiv-0').show();
  } else {

    $('#btnuploadfileParentDiv-0').hide();
    $('#parentDivUploadedFileDisplay-3').show();
    $('#parentDivUploadedFileDisplay-3').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-3').append(`
        <div class="book-cover-card-body">
          <img id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="book cover" height="160">
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="padamFailDoc(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}


