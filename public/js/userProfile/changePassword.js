// $('#change_password_form').validate({
//     ignore:'.ignore',
//     errorClass:'invalid',
//     validClass:'success',
//     rules:{
//             current_password:{
//                 required:true,
//                 minlength:6,
//                 maxlength:100
//             },
//             new_password:{
//                 required:true,
//                 minlength:6,
//                 maxlength:100
//             },
//             confirm_password:{
//                 required:true,
//                 equalTo:'#new_password'   
//             }
//         },

//         messages:{
//             current_password:{
//                 required:"Please enter your password",
//             },
//             new_password:{
//                 required:"Enter your new password",
//             },
//             confirm_password:{
//                 required:"Need to confirm your new password",
//             },
//         },
//         submitHandler:function(form){
//             $.LoadingOverlay("show");
//             form.submit();
//         }
   




// })

const putResetPassword = `${host}/api/password/changepassword`;
const getall = `${host}/api/login/alluser`;

$(function(){

    MaklumatUserID();
 

  });

function MaklumatUserID(){

  let userid = userID;
  let getUserInfo = `${host}/api/profile/${userid}/userInformation/`;



    $.ajax({
      url: getUserInfo,
      type: 'GET',
      contentType: "application/json; charset-utf-8",
      dataType: 'json',
      success: function (response) {
        let info = response.userInfo;
        console.log(info, 'info');
  
        $.each(info, function(index, value) {
  
            (value.fullname == null || value.fullname == '') ? $('#FullName').val('n/a') : $('#FullName').val(value.fullname);
            (value.ic_no == null || value.ic_no == '') ? $('#IcNo').val('n/a') : $('#IcNo').val(value.ic_no);
            (value.gender == null || value.gender == '') ? $('#Gender').val('n/a') : $('#Gender').val(value.gender);
            (value.phone_no == null || value.phone_no == '') ? $('#PhoneNumber').val('n/a') : $('#PhoneNumber').val(value.phone_no);
            (value.address_1 == null || value.address_1 == '') ? $('#AddressLine1').val('n/a') : $('#AddressLine1').val(value.address_1);
            (value.address_2 == null || value.address_2 == '') ? $('#AddressLine2').val('n/a') : $('#AddressLine2').val(value.address_2);
            (value.postcode == null || value.postcode == '') ? $('#Postcode').val('n/a') : $('#Postcode').val(value.postcode);
            (value.email == null || value.email == '') ? $('#EmailAddress').val('n/a') : $('#EmailAddress').val(value.email);
            (value.position == null || value.position == '') ? $('#Position').val('n/a') : $('#Position').val(value.position);
            (value.statename == null || value.statename == '') ? $('#State').val('n/a') : $('#State').val(value.statename);
            (value.countryname == null || value.countryname == '') ? $('#Country').val('n/a') : $('#Country').val(value.countryname);
            (value.bumi_status == null || value.bumi_status == '') ? $('#Status').val('n/a') : $('#Status').val(value.bumi_status);
            (value.qualification == null || value.qualification == '') ? $('#Qualification').val('n/a') : $('#Qualification').val(value.qualification);
            (value.education_level == null || value.education_level == '') ? $('#EducationLevel').val('n/a') : $('#EducationLevel').val(value.education_level);
            (value.companyname == null || value.companyname == '') ? $('#CompanyName').val('n/a') : $('#CompanyName').val(value.companyname);
            (value.company_address_1 == null || value.company_address_1 == '') ? $('#CompanyAddressLine1').val('n/a') : $('#CompanyAddressLine1').val(value.company_address_1);
            (value.company_address_2 == null || value.company_address_2 == '') ? $('#CompanyAddressLine2').val('n/a') : $('#CompanyAddressLine2').val(value.company_address_2);
  
            (value.fullname == null || value.fullname == '') ? $('#nameAtPic').text('n/a') : $('#nameAtPic').text(value.fullname);
  
           
        });
      }
    });
}


    function resetPassword(){
        // alert("test");
          var detailsData = {

              "CurentPassword" : $("#current_password").val(),
              "NewPassword" : $("#new_password").val(),
              "ConfirmPassword" : $("#confirm_password").val(),
         
            }
      
      
      console.log(detailsData);
       //alert(putResetPassword)
    //  event.preventDefault(); // Prevent the form from submitting
     
      var settings = {
        "url": putResetPassword,
        "method": "PUT",
        "timeout": 0,
        "data": detailsData,
        "dataType": 'json',
      };
 //alert(settings)
      $.ajax(settings).done(function (response) {
       //alert("Ajax request successful");
        // setTimeout( () => {
        //   $("#loadingModal").modal("hide");
        //   $("#loadingModal").removeClass("in");
        //   $('body').removeClass('modal-open');
        //   $("#berjaya-simpan-modal").modal({
        //     backdrop: 'static', 
        //     keyboard: false
        //   },"show");
        //   $("#loadingModal").hide();
        // }, 2000);

        // console.log(response);
       // $('#berjaya-simpan-modal').on('hidden.bs.modal', function () {
          window.location.href = `${host}/participant`;
         

      //  });
      });
    }

  