

    let TrainerList = () => {

       // userid = userID;
        usertype = userType;
        companyid = companyID;

       
            if(usertype == 1){
                $('#userType').text("Super admin");
              } else if(usertype == 2){
                $('#userType').text("Seda Admin");
              } else if(usertype == 3){
                $('#userType').text("Training Partner");
              } else if(usertype == 4){
                $('#userType').text("Trainer");
              } else if(usertype == 5){
                $('#userType').text("Participant");
              }
         

        let getList = `${host}/api/profile/${usertype}/${companyid}/TrainerListAttachment`;
     
        $.ajax({
          url: getList,
          type: "GET",
          dataType: "JSON",
        })
          .done((res) => {
            //createTable(res.data);
            let trainers = res.trainerlist
            tableScheduleList(trainers);

           console.log(trainers);
         
          })
          .catch((err) => {});
      };
 

      let tableScheduleList = (data) => {
        let senarai = $("#Trainer").DataTable({
          data: data,
          //  order: [[4, "desc"]],
          columnDefs: [
            {
              targets: 0,
              data: null,
              searchable: false,
              orderable: false,
            },
            {
              targets: 1,
              data: "fullname",
              render: function (data, type, full, meta) {
                if (data === null) {
                  return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
                }
                if (data !== null || data !== "") {
                  return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
                }
              },
            },
            {
              targets: 2,
              data: "certificate_no",
              render: function (data, type, full, meta) {
                
             
                    if (data && data !== '' && data !== null) {
                        return `<td align="left" width="><p class="poppins-semibold-14">${data}</p></td>`;
                    
                    } else {
                        return '<td width=""><p class="poppins-semibold-14">-</p></td>';
                    }
            }   
            },
          
            {
                targets: 3,
                data: "state_name",
                render: function (data, type, full, meta) {
                  if (data && data !== '' && data !== null) {
                    return `<td align="left" width=""><p class="poppins-semibold-14">${data}</p></td>`;
                  
                  } else {
                    return '<td width=""><p class="poppins-semibold-14">-</p></td>';
                  }
                },
            },
            {
              targets: 4,
              data: "training_partner",
              render: function (data, type, full, meta) {

                return `<td width=""><div class="col-2 mr-3 text-center mt-2">${data}</div><p class="poppins-semibold-14"></p></td>`;
                // if(data && data != '' && data != null) {

                    // if (!full.cert_cover) {
                    //     return '<td width=""><div class="col-2 mr-3 text-center mt-2">No attachment yet</div><p class="poppins-semibold-14"></p></td>';
                    //   } else {
                    //     return `<td width=""><div class="col-2 mr-3"><a href="data:application/pdf;base64,${full.file_cover}" download="${full.file_name}.pdf">${full.file_name}.pdf</a></div><p class="poppins-semibold-14"></p></td>`;
                    // }
                    
                
              },
            },

            {
                targets: 5,
                render: function (data, type, full, meta) {
                  if (!full.cert_cover) {
                    return '<td width=""><div class="col-2 mr-3 text-center mt-2">No attachment yet</div><p class="poppins-semibold-14"></p></td>';
                  } else {
                    return `<td width=""><div class="col-2 mr-3"><a href="data:application/pdf;base64,${full.cert_cover}" download="${full.cert_name}.pdf">${full.cert_name}.pdf</a></div><p class="poppins-semibold-14"></p></td>`;
                  }
                },
              },
           
            {
              targets: 6,
              data: "status",
              //visible: false,
              render: function (data, type, full, meta) {

                 let link = "";
               
                  if (full != null || data == 0) {
                    return  `<td width=""><p class="poppins-semibold-14">Active</p></td>`;
                  }else {
                    return  `<td width=""><p class="poppins-semibold-14">In Active</p></td>`;
                  }
               
              },
            },
          ],
          language: {
            info: "view _START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "view 0 from 0 to 0 entry",
            lengthMenu: "view _MENU_ Entry",
            paginate: {
              next: "Next",
              previous: "Previous",
            },
            zeroRecords: "No record found",
          },
        });
      
        senarai
          .on("order.dt search.dt", function () {
            senarai
              .column(0, { search: "applied", order: "applied" })
              .nodes()
              .each(function (cell, i) {
                cell.innerHTML = i + 1;
              });
          })
          .draw();
      
        // $("#filterStatus").on("change", (e) => {
        //   // console.log(e.target.value)
        //   senarai.column(5).search(e.target.value).draw();
        // });
      
        // $("#filterJenisKenderaan").on("change", (e) => {
        //   console.log(e.target.value);
        //   senarai.column(1).search(e.target.value).draw();
        // });
      
        // $("#search_input").on("keyup", () => {
        //   let searched = $("#search_input").val();
        //   // console.log(searched)
        //   senarai.search(searched).draw();
        // });
      
        // $("#resetFilter").on("click", () => {
        //   $("#filterStatus").val("").trigger("change");
        //   $("#filterJenisKenderaan").val("").trigger("change");
        //   $("#search_input").val("");
      
        //   senarai.search("").columns().search("").draw();
        // });
      };
      

      $(function(){

        TrainerList();
       
    });