
$(function(){

  ExamList();
  TrainingDetails();


});


let TrainingDetails = () => {


ID =   $(this).data("id");

let getList = `${host}/api/exam/${ID}/examDetails`;
$.ajax({
 url: getList,
 type: "GET",
 dataType: "JSON",
})
 .done((res) => {
   //createTable(res.data);
   data = res.trainingDetails;
  
   (value.training_name == null || value.training_name == '') ? $('#provide_name').text('n/a') : $('#provide_name').text(value.training_name); 
   (value.date_start == null || value.date_start == '') ? $('#date_start').text('n/a') : $('#date_start').text(value.date_start);
   (value.date_end == null || value.date_end == '') ? $('#date_end').text('n/a') : $('#date_end').text(value.date_end);
   (value.venue == null || value.venue == '') ? $('#venue').text('n/a') : $('#venue').text(value.venue);

 })
 .catch((err) => {});
};


  let ExamList = () => {

       let getList = `${host}/api/exam/examTrainingList`;
      $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
      })
        .done((res) => {
          //createTable(res.data);
          tableExamList(res.examlist);
          console.log(res.examlist);
        })
        .catch((err) => {});
    };


    let tableExamList = (data) => {
      let senarai = $("#examTrainingList").DataTable({
        data: data,
        //  order: [[4, "desc"]],
        columnDefs: [
          {
            targets: 0,
            data: null,
            searchable: false,
            orderable: false,
          },
          {
            targets: 1,
            data: "course_name",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="30%" align="left"><p class="poppins-semibold-14">-</p></td>`;
              }
              if (data !== null || data !== "") {
                return `<td align="left" width="30%"><p class="poppins-semibold-14">${data}</p></td>`;
              }
            },
          },
          {
            targets: 2,
            data: "id_training_option",
            render: function (data, type, full, meta) {
              if (data === 1) {
                return `<td width="10%" align="left"><p class="poppins-semibold-14">In house</p></td>`;
              }
              else if (data === 2) {
                return `<td align="left" width="40%"><p class="poppins-semibold-14">Traning Partner</p></td>`;
              }else{
                return `<td width="10%" align="left"><p class="poppins-semibold-14">-</p></td>`;
              }
            },
          },
          {
            targets: 3,
            data: "trainingpartner",
            render: function (data, type, full, meta) {
              if (data === null || data === "") {
                return `<td align="left" width="15%"><p class="poppins-semibold-14">-</p></td>`;
              }else if (data !== null || data !== "") {
                return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
              }
              
            },
          },
          {
              targets: 4,
              data: "date_start",
              render: function (data, type, full, meta) {
                if (data && data !== '' && data !== null) {
                  // Assuming 'data1' and 'data2' are the two data values you want to display
                  var data1 = full.date_start; // Replace 'data1' with your actual data field name
                  var data2 = full.date_end; // Replace 'data2' with your actual data field name
            
                  // Combine 'data1' and 'data2' into one string
                  var combinedData = data1 + ' to ' + data2;
            
                  // You can also use HTML for formatting
                  var formattedData = '<p class="poppins-semibold-14">' + combinedData + '</p>';
            
                  return formattedData;
                } else {
                  return '<td width="15%"><p class="poppins-semibold-14">-</p></td>';
                }
              },
            },
          // {
          //   targets: 5,
          //   data: "venue",
          //   render: function (data, type, full, meta) {
          //     if(data && data != '' && data != null) {
              
          //       return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
          //     } else {
          //       return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
          //     }
              
          //   },
          // },
          {
            targets: 5,
            data: "batch_no_full",
            render: function (data, type, full, meta) {
              if(data && data != '' && data != null) {
              
                return  `<div class="dark-turquoise-status poppins-medium-12">${data}</p></div>`;
              } else {
                return `<div class="dark-turquoise-status poppins-medium-12">-</p></div>`;
              }
              
            },
          },
          {
            targets: 6,
            data: "endorsed_status",
            className: "text-center",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="20%" align="center"><p class="table-content">-</p></td>`;
              }
              if (data == 1 ) {
                return `<td align="center" width="30%"><p class="table-content" style="text-align: center;">Endorsed</p></td>`;
              }
              if (data == 2 ) {
                return `<td align="center" width="30%"><p class="table-content" style="text-align: center;">Query</p></td>`;
              }
              if (data == 3 ) {
                return `<td align="center" width="30%"><p class="table-content" style="text-align: center;">Not Endorse</p></td>`;
              }
            },
          },
          {
            targets: 7,
            data: "id_training_status",
            render: function (data, type, full, meta) {
              
              if(data == 3) {
                  //return `<div class="dark-turquoise-status poppins-medium-12 success-status" >Completed</div>`;
                  return  `<div style="font-size: 14px;border-radius: 8px;
                  border: 1px solid #3498DB;
                  background: var(--success-25, #F6FEF9);
                  padding: 4px 8px;
                  color: #3498DB;">Completed</div>`;
                  
                  }else if(data == 2){
                    return `<div style=" font-size: 14px; border-radius: 8px;
                    border: 1px solid #18eeaa;
                    background: var(--success-25, #f6faf7);
                    padding: 4px 8px;
                    color: #18eeaa;">On Going</div>`;
                  }
                  else if(data == 1){
                    return `<div style="font-size: 14px; border-radius: 8px;
                    border: 1px solid  #F79009;
                    background: var(--warning-25, #FFFCF5);
                    padding: 4px 8px;
                    color: #F79009;">Up Coming</div>`;
                  }else{
                    return `<div>-</div>`;
                  }
            },
          },
        
          {
            targets: 8,
            data: "id_training_course",
            //visible: false,
            render: function (data, type, full, meta) {


                if (data != null || data !== "") {
                  if((full.id_training_option == 1)&&(full.course_name !== "Grid Connected Photovoltaic (GCPV) System Design Course")){
                    return  `<a href="${host}/exam/endorseExamResult/${data}/${full.batch_no_full}" class="btn btn-sm">
                               <i class="fas fa-check" alt="Endorse" title="Endorse" style="font-size: 18px; color: #3498DB;"></i>
                             </a>
                             <a href="${host}/exam/updateExamResultAdmin/${data}/${full.batch_no_full}" class="btn btn-sm">
                                 <i class="fas fa-edit" alt="Update" title="Update" style="font-size: 18px; color: #3498DB;"></i>
                               </a>`;
                     }else if((full.id_training_option == 2) &&(full.course_name !== "Grid Connected Photovoltaic (GCPV) System Design Course")){
   
                       return  `<a href="${host}/exam/endorseExamResult/${data}/${full.batch_no_full}" class="btn btn-sm">
                       <i class="fas fa-check" alt="Endorse" title="Endorse" style="font-size: 18px; color: #3498DB;"></i>
                     </a>`;
   
                     }else if((full.course_name == "Grid Connected Photovoltaic (GCPV) System Design Course") &&(full.id_training_option == 2))
                     {
   
                       return  `<a href="${host}/exam/endorseExamResult_GCPVDESIGN/${data}/${full.batch_no_full}" class="btn btn-sm">
                       <i class="fas fa-check" alt="Endorse" title="Endorse" style="font-size: 18px; color: #3498DB;"></i>
                     </a>`;
   
                     }else if((full.course_name == "Grid Connected Photovoltaic (GCPV) System Design Course") &&(full.id_training_option == 1))
                     {
   
                       return  `<a href="${host}/exam/endorseExamResult_GCPVDESIGN/${data}/${full.batch_no_full}" class="btn btn-sm">
                       <i class="fas fa-check" alt="Endorse" title="Endorse" style="font-size: 18px; color: #3498DB;"></i>
                     </a>
                     <a href="${host}/exam/updateExamResultAdmin_GCPVDESIGN/${data}/${full.batch_no_full}" class="btn btn-sm">
                     <i class="fas fa-edit" alt="Update" title="Update" style="font-size: 18px; color: #3498DB;"></i>
                   </a>`;
   
                     }else{
                       return  `-`;
                  }


                      //      return  `<a href="${host}/exam/viewExamResult" class="btn btn-sm">
                      //      <i class="icon-editIcon" style="font-size: 18px;"></i>
                      //  </a>
                      //  <a href="${host}/exam/updateExamResult/${data}" class="btn btn-sm">
                      //  <button class="btn btn-sm icon-style" data-id="${data}" 
                      //  <i class="fas fa-edit" alt="description" style="font-size: 18px; color: #3498DB;"></i>
                      //  </button></a>
                       
                      // `;
                }

                
              //return data;
            },
          },
        ],
        language: {
          info: "view _START_ to _END_ from _TOTAL_ entry",
          infoEmpty: "view 0 from 0 to 0 entry",
          lengthMenu: "view _MENU_ Entry",
          paginate: {
            next: "Next",
            previous: "Previous",
          },
          zeroRecords: "No record found",
        },
      });
    
      senarai
        .on("order.dt search.dt", function () {
          senarai
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
              cell.innerHTML = i + 1;
            });
        })
        .draw();
    
      $("#filterStatus").on("change", (e) => {
        // console.log(e.target.value)
        senarai.column(5).search(e.target.value).draw();
      });
    
      $("#filterJenisKenderaan").on("change", (e) => {
        console.log(e.target.value);
        senarai.column(1).search(e.target.value).draw();
      });
    
      $("#search_input").on("keyup", () => {
        let searched = $("#search_input").val();
        // console.log(searched)
        senarai.search(searched).draw();
      });
    
      $("#resetFilter").on("click", () => {
        $("#filterStatus").val("").trigger("change");
        $("#filterJenisKenderaan").val("").trigger("change");
        $("#search_input").val("");
    
        senarai.search("").columns().search("").draw();
      });
    };
    

    //////
    let endorseExamResult = () => {

      $(document).on('click', '#hantarEditBtn', function() {
      
  
          //$('#edit-result-modal').modal('hide');
          $('#confirmationEndorseModal').modal('show');
      
      });
     
      $("#confirmEndorse").on("click", () => {
          $("#confirmationEndorseModal").modal("hide");
         
          let editExamResult = host + '/api/exam/editExamResult';
          
    
        const data = {
  
          id_participant : $('#formEditResult input[name="id_participant_examresult"]').val(),
          id_training : $('#formEditResult input[name="id_training"]').val(),
  
          exam_1 : $('#formEditResult input[name="partA"]').val(),
          exam_2 : $('#formEditResult input[name="partB"]').val(),
          exam_3 :  $('#formEditResult input[name="partC"]').val(),
          totalResultPractical : $('#formEditResult input[name="totalResultPractical"]').val(),
          statusResultPracticalExam : $('#formEditResult select[name="statusResultPracticalExam"]').val(),
        
          
          
          exam_1_theory : $('#formEditResult input[name="part-AT"]').val(),
          exam_2_theory : $('#formEditResult input[name="part-BT"]').val(),
          exam_3_theory :  $('#formEditResult input[name="part-CT"]').val(),
          totalResultTheory : $('#formEditResult input[name="totalResultTheory"]').val(),
          statusResultTheory : $('#formEditResult select[name="statusResultTheory"]').val(),
        
          statusResultFinal : $('#formEditResult select[name="statusResultFinal"]').val(),
    
            };
          //console.log(data);
          let postData = $.ajax({
            url: editExamResult,
            method: "PUT",
            data: data,
          }) .done((res) => {
           console.log('data',res);
    
          //   setTimeout(() => {
    
          //     $("#loadingModal").modal("hide");
          //     $("#berjaya-simpan-modal").modal({
          //         backdrop: 'static',
          //         keyboard: false,
          //     });
          //     $("#loadingModal").modal("hide");
    
          // }, 2000);
    
    
          })
          .catch((err) => {
            //console.log(err);
          });
          $.when(postData).done( (res) => {
          //  $('#berjaya-kemaskini-modal').modal('show');
            let url = window.location.pathname;
            let ID = url.substring(url.lastIndexOf('/') + 1);
             window.location.href = `${host}/exam/updateExamResult/${ID}`;
    
         })
    
    
        });
    
   }
  