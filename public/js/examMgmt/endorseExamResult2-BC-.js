

// $(document).ready(function () {
//     endorseExamResult();
// });

// $(document).on('click', '#editResult', function() {

//   let url = window.location.pathname;
//   let ID = url.substring(url.lastIndexOf('/') + 1);
  
 
  let url = window.location.pathname;
  let segments = url.split('/');
  let ID = segments[segments.length - 2];
   
  let getDetails = host + '/api/exam/'+ ID +'/examDetailsAdmin';

 
 // console.log('examDetailss',ID);

  $.ajax({
    url: getDetails,
    type: 'GET',
   // contentType: "application/json",
    dataType: "JSON",
}).done((res) => {
    let data = res.trainingDetails;
    console.log(data);

    $.each(data, function(index, value) {

   
    (value.training_name == null || value.training_name == '') ? $('#provider_name').text('n/a') : $('#provider_name').text(value.training_name);
    (value.date_start == null || value.date_start == '') ? $('#date_start').text('n/a') : $('#date_start').text(value.date_start);
    (value.date_end == null || value.date_end == '') ? $('#date_end').text('n/a') : $('#date_end').text(value.date_end);
    (value.venue == null || value.venue == '') ? $('#venue').text('n/a') : $('#venue').text(value.venue);
    (value.batch_no == null || value.batch_no == '') ? $('#batch_no').val('n/a') : $('#batch_no').val(value.batch_no);
   
    
    });


}).catch((err) => {
    //console.log(err);
});
 

// });



  $(document).on('click', '#editResult', function() {
    
    ID =   $(this).data("id");
    
    let getExamResult = `${host}/api/exam/${ID}/getExamResult`;

    console.log(ID);


      $.ajax({
            url: getExamResult,
            type: 'GET',
            //contentType: "application/json",
            dataType: "JSON",
            }).done((res) => {
            let data = res.exam;
            console.log('exam',data);
      
            $.each(data, function(index, value) {

           

            (value.part_A == null || value.part_A == '') ? $('#partA').val('n/a') : $('#partA').val(value.part_A);
            (value.part_B == null || value.part_B == '') ? $('#partB').val('n/a') : $('#partB').val(value.part_B);
            (value.part_C == null || value.part_C == '') ? $('#partC').val('n/a') : $('#partC').val(value.part_C);
            (value.total_mark_practical == null || value.total_mark_practical == '') ? $('#totalResultPractical').val('n/a') : $('#totalResultPractical').val(value.total_mark_practical);
           
            (value.result_status_practical == null || value.result_status_practical == '') ? $('#statusResultPracticalExam').val('n/a') : $('#statusResultPracticalExam').val(value.result_status_practical);
        

            (value.part_A_theory == null || value.part_A_theory == '') ? $('#part-AT').val('n/a') : $('#part-AT').val(value.part_A_theory);
            (value.part_B_theory == null || value.part_B_theory == '') ? $('#part-BT').val('n/a') : $('#part-BT').val(value.part_B_theory);
            (value.part_C_theory == null || value.part_C_theory == '') ? $('#part-CT').val('n/a') : $('#part-CT').val(value.part_C_theory);
             (value.total_mark_theory == null || value.total_mark_theory == '') ? $('#totalResultTheory').val('n/a') : $('#totalResultTheory').val(value.total_mark_theory);
             
            (value.result_status_theory == null || value.result_status_theory == '') ? $('#statusResultTheory').val('n/a') : $('#statusResultTheory').val(value.result_status_theory);

            (value.final_verdict_status == null || value.final_verdict_status == '') ? $('#statusResultFinal').val('n/a') : $('#statusResultFinal').val(value.final_verdict_status);
        
            (value.id_participant_examresult == null || value.id_participant_examresult == '') ? $('#id_participant_examresult').val('n/a') : $('#id_participant_examresult').val(value.id_participant_examresult);
            (value.id_training == null || value.id_training == '') ? $('#id_training').val('n/a') : $('#id_training').val(value.id_training);
            });
        

        }).catch((err) => {
            //console.log(err);
        });

    $('#edit-result-modal').modal('show');

});





let endorseExamResult = () => {

    $("#EndorseBtn").on("click", () => {
        $("#confirmationEndorseModal").modal('show');
    });
      

    // $(document).on('click', '#EndorseBtn', function() {
    //   //  alert('k');
    

    //     //$('#edit-result-modal').modal('hide');
    //     $('#confirmationEndorseModal').modal('show');
    
    // });
   
    $("#submitEndorse").on("click", () => {
        $("#confirmationEndorseModal").modal("hide");
       
        let editExamResult = host + '/api/exam/editExamResultEndorsed';
        
  
      const data = {

        Batch_No : $('#formEndorseResult input[name="batch_no"]').val(),
        
          };
        //console.log(data);
        let postData = $.ajax({
          url: editExamResult,
          method: "PUT",
          data: data,
        }) .done((res) => {
         console.log('data',res);
  
      
        if (res.status === "ERROR" && res.errors && res.errors.certificate_no) {
          // Display the error message in your modal
          $('#error-message').text(res.errors.certificate_no);
      
          // Open or show your modal
          setTimeout(() => {
  
            $("#error-modal").modal("show");
          
        }, 2000);
      }

      
      if (res.status === "ERROR" && res.errors && res.errors.mykad_no) {
        // Display the error message in your modal
        $('#error-message').text(res.errors.mykad_no);
    
        // Open or show your modal
        setTimeout(() => {

          $("#error-modal").modal("show");
        
      }, 2000);
    }  


        if (res.status === "ERROR" && res.errors && res.errors.email) {
          // Display the error message in your modal
          $('#error-message').text(res.errors.email);
      
          // Open or show your modal
          setTimeout(() => {

            $("#error-modal").modal("show");
          
        }, 2000);
      }


        if (res.status === "ERROR" && res.errors && res.errors.mobile_no) {
          // Display the error message in your modal
          $('#error-message').text(res.errors.mobile_no);

          // Open or show your modal
          setTimeout(() => {

            $("#error-modal").modal("show");
          
        }, 2000);
      }
       
  
  
        })
        .catch((err) => {
        //  console.log(err);
        });
        $.when(postData).done( (res) => {
         
        //  $('#berjaya-kemaskini-modal').modal('show');
          let url = window.location.pathname;
          let ID = url.substring(url.lastIndexOf('/') + 1);
         // window.location.href = `${host}/exam/endorseExamResult/${ID}`;
  
       })
  
  
      });
  
 }


 let queryExamResult = () => {

    $("#QueryBtn").on("click", () => {
        $("#confirmationQueryModal").modal('show');
    });
      

    // $(document).on('click', '#EndorseBtn', function() {
    //   //  alert('k');
    

    //     //$('#edit-result-modal').modal('hide');
    //     $('#confirmationEndorseModal').modal('show');
    
    // });
   
    $("#submitQuery").on("click", () => {
        $("#confirmationQueryModal").modal("hide");
       
        let editExamResult = host + '/api/exam/editExamResultQuery';
        
  
      const data = {

        Batch_No : $('#formEndorseResult input[name="batch_no"]').val(),
        
          };
        //console.log(data);
        let postData = $.ajax({
          url: editExamResult,
          method: "PUT",
          data: data,
        }) .done((res) => {
         console.log('data',res);
  
        //   setTimeout(() => {
  
        //     $("#loadingModal").modal("hide");
        //     $("#berjaya-simpan-modal").modal({
        //         backdrop: 'static',
        //         keyboard: false,
        //     });
        //     $("#loadingModal").modal("hide");
  
        // }, 2000);
  
  
        })
        .catch((err) => {
          //console.log(err);
        });
        $.when(postData).done( (res) => {
        //  $('#berjaya-kemaskini-modal').modal('show');
          let url = window.location.pathname;
          let ID = url.substring(url.lastIndexOf('/') + 1);
           window.location.href = `${host}/exam/endorseExamResult/${ID}`;
  
       })
  
  
      });
  
 }



    let ExamList = () => {

        // let url = window.location.pathname;
        // let batch_no = url.substring(url.lastIndexOf('/') + 1);

        let url = window.location.pathname;
        let segments = url.split('/');
        let batch_no = segments[segments.length - 2];
    

        let getExamList = `${host}/api/exam/${batch_no}/examNameListAdmin`;
       
        $.ajax({
          url: getExamList,
          type: "GET",
          dataType: "JSON",
        })
          .done((res) => {
         // console.log(res.processedData);
         let resultList = res.examlist;
          
        tableExamNameList(resultList);


          })
          .catch((err) => {});
      };
 

      let tableExamNameList = (data) => {
       //console.log('d',data);
        // const dataArray = Object.values(data);
        let senarai = $("#examResultUpdate").DataTable({
          data: data,
          // data:dataArray,
          //  order: [[4, "desc"]],
          columnDefs: [
            {
              targets: 0,
              data: null,
              searchable: false,
              orderable: false,
            },
            {
              targets: 1,
              data: "fullname",
             //data: "id_training_participant",
              render: function (data, type, full, meta) {
                
                return data;
              },
            },
            {
              targets: 2,
               data: "exam_type",
            //data:"exam_type",
              render: function (data, type, full, meta) {
                
                return data;
              },
            },
            {
                targets: 3,
                data: "part_A",
                render: function (data, type, full, meta) {
                  
                    return data;
                },
              },
            {
              targets: 4,
              data: "part_B",
              render: function (data, type, full, meta) {
                
                return data;
                
              },
            },
            {
              targets: 5,
              data: "part_C",
              render: function (data, type, full, meta) {
              
                return data;
              },
            },
            {
              targets: 6,
              data: "total_mark_practical",
              className: "text-center",
              render: function (data, type, full, meta) {
              
      
                return data;
              },
            },
            {
              targets: 7,
              data: "result_status_practical",
              //visible: false,
              render: function (data, type, full, meta) {

             
                return data;
              },
            },
            // {
            //     targets: 8,
            //     data: "exam_type_2",
            //     //visible: false,
            //     render: function (data, type, full, meta) {
  
            //      return data;
            //     },
            // },
            {
                targets: 8,
                data: "part_A_theory",
                //visible: false,
                render: function (data, type, full, meta) {
  
              
                  return data;
                },
            },
            {
                targets: 9,
                data: "part_B_theory",
                //visible: false,
                render: function (data, type, full, meta) {
  
                  return data;
                },
            },
            {
                targets: 10,
                data: "part_C_theory",
                //visible: false,
                render: function (data, type, full, meta) {
  
                  return data;
                },
            },
            {
                targets: 11,
                data: "total_mark_theory",
                //visible: false,
                render: function (data, type, full, meta) {
  
                return data;
                },
            },
            {
                targets: 12,
                data: "result_status_theory",
                //visible: false,
                render: function (data, type, full, meta) {
  
                return data;
                },
            },
            {
                targets: 13,
                data: "final_verdict_status",
                //visible: false,
                render: function (data, type, full, meta) {
  
                return data;
                },
            },
            {
                targets: 14,
                data: "endorsed_status",
                //visible: false,
                render: function (data, type, full, meta) {

                    if (data == null || data == '0') {
                        return `Waiting for endorsement`;    
                       
                    }
                   else if (data == 1 ) {
                        return `Endorsed`;    
                       
                    }
                    else if (data == 2 ) {
                        return `Query`;    
                       
                    }
                // return data;
                },
            },
            
         
          ],
          language: {
            info: "view _START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "view 0 from 0 to 0 entry",
            lengthMenu: "view _MENU_ Entry",
            paginate: {
              next: "Next",
              previous: "Previous",
            },
            zeroRecords: "No record found",
          },
        });
      
        senarai
          .on("order.dt search.dt", function () {
            senarai
              .column(0, { search: "applied", order: "applied" })
              .nodes()
              .each(function (cell, i) {
                cell.innerHTML = i + 1;
              });
          })
          .draw();
      
        $("#filterStatus").on("change", (e) => {
          // console.log(e.target.value)
          senarai.column(5).search(e.target.value).draw();
        });
      
        $("#filterJenisKenderaan").on("change", (e) => {
          console.log(e.target.value);
          senarai.column(1).search(e.target.value).draw();
        });
      
        $("#search_input").on("keyup", () => {
          let searched = $("#search_input").val();
          // console.log(searched)
          senarai.search(searched).draw();
        });
      
        $("#resetFilter").on("click", () => {
          $("#filterStatus").val("").trigger("change");
          $("#filterJenisKenderaan").val("").trigger("change");
          $("#search_input").val("");
      
          senarai.search("").columns().search("").draw();
        });
      };
      




let getTrainingPartner = host + '/api/exam/trainingPartner';


let loadTP = () => {
  $.ajax({
    url: getTrainingPartner,
    type: "GET",
    dataType: "JSON",
  }).done((res) => {
   
    allUserList = res.tp;
    //console.log('aaa',allUserList);
    $(allUserList).each((index, value) => {
      $('select[name="trainingPartnerDropdown"]').append(
        '<option value="' + value.name + '">' + value.name + "</option>"
      );

     
    });

    
  });
};

      
$(function(){

  ExamList();
  //tableExamNameList();
  endorseExamResult();
  queryExamResult();
  loadTP();
 
});
