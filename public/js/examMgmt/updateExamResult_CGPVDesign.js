let senarai;
let findheader;
$(function(){

    ExamList();
    //tableExamNameList();
    //TPUpdateExamResult();
   
});


// $(document).ready(function () {
//     updateExamResult();
// });

// $(document).on('click', '#editResult', function() {
   let url = window.location.pathname;
   let batch_full = url.substring(url.lastIndexOf('/') + 1);
   let segments = url.split('/');
   let ID = segments[segments.length - 2];
  
  let getDetails = `${host}/api/exam/${ID}/${batch_full}/examDetails`;
  //console.log('examDetails',ID);

  $.ajax({
    url: getDetails,
    type: 'GET',
   // contentType: "application/json",
    dataType: "JSON",
}).done((res) => {
    let data = res.trainingDetails;
    console.log(data);

    $.each(data, function(index, value) {

   
    (value.training_name == null || value.training_name == '') ? $('#provider_name').text('n/a') : $('#provider_name').text(value.training_name);
    (value.date_start == null || value.date_start == '') ? $('#date_start').text('n/a') : $('#date_start').text(value.date_start);
    (value.date_end == null || value.date_end == '') ? $('#date_end').text('n/a') : $('#date_end').text(value.date_end);
    (value.venue == null || value.venue == '') ? $('#venue').text('n/a') : $('#venue').text(value.venue);
    (value.batch_no_full == null || value.batch_no_full == '') ? $('#batch_no').text('n/a') : $('#batch_no').text(value.batch_no_full);
    
    
    });


}).catch((err) => {
    //console.log(err);
});
 

// });



  $(document).on('click', '#editResult', function() {
    
     ID =   $(this).data("id");
     console.log('s',ID);

    let url = window.location.pathname;
    let batch_full = url.substring(url.lastIndexOf('/') + 1);
    
    
//alert(batch_full);
    let getExamResult = `${host}/api/exam/${ID}/${batch_full}/getExamResult`;


      $.ajax({
            url: getExamResult,
            type: 'GET',
            //contentType: "application/json",
            dataType: "JSON",
        }).done((res) => {
            let data = res.exam;
            console.log('exam',data);
      
            $.each(data, function(index, value) {

           

            (value.part_A == null || value.part_A == '') ? $('#partA').val('n/a') : $('#partA').val(value.part_A);
            (value.part_B == null || value.part_B == '') ? $('#partB').val('n/a') : $('#partB').val(value.part_B);
            (value.part_C == null || value.part_C == '') ? $('#partC').val('n/a') : $('#partC').val(value.part_C);
            (value.total_mark_practical == null || value.total_mark_practical == '') ? $('#totalResultPractical').val('n/a') : $('#totalResultPractical').val(value.total_mark_practical);
           
            (value.result_status_practical == null || value.result_status_practical == '') ? $('#statusResultPracticalExam').val('n/a') : $('#statusResultPracticalExam').val(value.result_status_practical);
        

            (value.part_A_theory == null || value.part_A_theory == '') ? $('#part-AT').val('n/a') : $('#part-AT').val(value.part_A_theory);
            (value.part_B_theory == null || value.part_B_theory == '') ? $('#part-BT').val('n/a') : $('#part-BT').val(value.part_B_theory);
            (value.part_C_theory == null || value.part_C_theory == '') ? $('#part-CT').val('n/a') : $('#part-CT').val(value.part_C_theory);
             (value.total_mark_theory == null || value.total_mark_theory == '') ? $('#totalResultTheory').val('n/a') : $('#totalResultTheory').val(value.total_mark_theory);
             
            (value.result_status_theory == null || value.result_status_theory == '') ? $('#statusResultTheory').val('n/a') : $('#statusResultTheory').val(value.result_status_theory);

            (value.final_verdict_status == null || value.final_verdict_status == '') ? $('#statusResultFinal').val('n/a') : $('#statusResultFinal').val(value.final_verdict_status);
        
            (value.id_participant_examresult == null || value.id_participant_examresult == '') ? $('#id_participant_examresult').val('n/a') : $('#id_participant_examresult').val(value.id_participant_examresult);
            (value.id_training == null || value.id_training == '') ? $('#id_training').val('n/a') : $('#id_training').val(value.id_training);
            });
        

        }).catch((err) => {
            //console.log(err);
        });

    $('#edit-result-modal').modal('show');

});






    let ExamList = () => {

        let url = window.location.pathname;
        let batch_full = url.substring(url.lastIndexOf('/') + 1);
        let segments = url.split('/');
        let ID = segments[segments.length - 2];
       // batch_full = batch_no_full;
// alert(batch_full);alert(ID);
        let getExamList = `${host}/api/exam/${ID}/${batch_full}/examNameList_GCPVDESIGN`;
        // alert(ID);
        $.ajax({
          url: getExamList,
          type: "GET",
          dataType: "JSON",
        })
          .done((res) => {
         findheader = res.headerName;
         let resultList = res.examlist;

         console.log('findheader',findheader.fundamental_exam_1);
         console.log('findheader',findheader.fundamental_exam_2);
         console.log('findheader',findheader.fundamental_exam_3);

         console.log('findheader',findheader.design_exam_1);
         console.log('findheader',findheader.design_exam_2);
         console.log('findheader',findheader.design_exam_3);

         console.log('findheader',findheader.practical_exam_1);
         console.log('findheader',findheader.practical_exam_2);
         console.log('findheader',findheader.practical_exam_3);
        

         console.log('findheaderd',findheader);


         console.log(resultList);
           
        tableExamNameList(resultList,findheader);
        tableExamNameList2(resultList,findheader);
        tableExamNameList3(resultList,findheader);

      

         


          })
          .catch((err) => {});
      };
 
      var table = document.getElementById("examResultUpdate");

// Get the first row of the table (assuming the headers are in the first row)
var headerRow = table.getElementsByTagName("thead")[0].getElementsByTagName("tr")[0];

// Count the number of cells (columns) in the header row
var numColumns = headerRow.cells.length;
console.log("Number of columns: " + numColumns);

var columnNames = [];
for (var i = 0; i < headerRow.cells.length; i++) {
    var columnName = headerRow.cells[i].textContent || headerRow.cells[i].innerText;
    columnNames.push(columnName.trim());
}

console.log("Column names: " + columnNames.join(', '));


console.log("Column 0: " + columnNames[0]);
console.log("Column 1: " + columnNames[1]);
console.log("Column 2: " + columnNames[2]);
console.log("Column 3: " + columnNames[3]);
console.log("Column 4: " + columnNames[4]);
console.log("Column 5: " + columnNames[5]);
console.log("Column 6: " + columnNames[6]);
console.log("Column 7: " + columnNames[7]);
console.log("Column 8: " + columnNames[8]);
console.log("Column 9: " + columnNames[9]);

console.log("Column 10: " + columnNames[10]);
console.log("Column 11: " + columnNames[11]);
console.log("Column 12: " + columnNames[12]);
console.log("Column 13: " + columnNames[13]);
console.log("Column 14: " + columnNames[14]);
console.log("Column 15: " + columnNames[15]);
console.log("Column 16: " + columnNames[16]);




let tableExamNameList = (data) => {
        
  if (numColumns === 19) //template 1
    { 
      if(
        (columnNames[3] === findheader.fundamental_exam_1 && columnNames[4] === findheader.fundamental_exam_2 && columnNames[6] === "Fundamental Result")
        && columnNames[7] === findheader.design_exam_1 && columnNames[8] === findheader.design_exam_2 && columnNames[10] === "Design Result"
        && columnNames[11] === findheader.practical_exam_1 && columnNames[12] === findheader.practical_exam_2 && columnNames[13] === findheader.practical_exam_3 && columnNames[15]  === "Practical Result")  
        {  // alert('template 8 ')
           senarai = $("#examResultUpdate").DataTable
          ({
            data: data,
            // data:dataArray,
            //  order: [[4, "desc"]],
            columnDefs: 
            [
              {
                targets: 0,
                data: function (row, type, full, meta) {
                // Use meta.row to get the row index (starting from 0)
                return meta.row + 1;
                },
                searchable: false,
                orderable: false,
              },
              {
                targets: 1,
                data: "fullname",
              //data: "id_training_participant",
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                },
              },
              {
                targets: 2,
                data: "exam_type",
              //data:"exam_type",
                render: function (data, type, full, meta) {
                  
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                  <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                },
              },
              {
                  targets: 3,
                  data: "part_A_theory",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index  
                    if((data === '') || (data === null)) {                   
                    return `<input type="text" value="" id="part_a_theory${rowIndex}" name="part_a_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  }else if(data !=''){     
                    return `<input type="text" value="${data}" id="part_a_theory${rowIndex}" name="part_a_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  }
                  },
                },
              {
                targets: 4,
                data: "part_B_theory",
                render: function (data, type, full, meta) {

                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) { 
                  return `<input type="text" value="" id="part_b_theory${rowIndex}" name="part_b_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                }else if(data !=''){    
                  return `<input type="text" value="${data}" id="part_b_theory${rowIndex}" name="part_b_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                }
                },
              },
              {
                targets: 5,
                data: "part_C_theory",
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) { 
                  return `<input type="text" value="" id="part_c_theory${rowIndex}" name="part_c_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                }else if(data !=''){   
                  return `<input type="text" value="${data}" id="part_c_theory${rowIndex}" name="part_c_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                }
                },
              },
              {
                targets: 6,
                data: "total_mark_theory",
                className: "text-center",
                render: function (data, type, full, meta) {
                 
                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) { 
                  return `<input type="text" value="" id="total_mark_theory${rowIndex}" name="total_mark_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                }else if(data !=''){   
                  return `<input type="text" value="${data}" id="total_mark_theory${rowIndex}" name="total_mark_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                 }
                },
              },
              {
                targets: 7,
                data: "result_status_theory",
                //visible: false,
                render: function (data, type, full, meta) {

                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) { 
                  return `<input type="text" value="" id="result_status_theory${rowIndex}" name="result_status_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                  }else if(data !=''){  
                    return `<input type="text" value="${data}" id="result_status_theory${rowIndex}" name="result_status_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                  }
                },
              },
          
              {
                  targets: 8,
                  data: "part_A",
                  //visible: false,
                  render: function (data, type, full, meta) {
    
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) { 
                    return `<input type="text" value="" id="part_a${rowIndex}" name="part_a${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  }else if(data !=''){  
                    return `<input type="text" value="${data}" id="part_a${rowIndex}" name="part_a${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                   }
                  },
              },
              {
                  targets: 9,
                  data: "part_B",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) { 
                    return `<input type="text" value="" id="part_b${rowIndex}" name="part_b${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  }else if(data !=''){ 
                    return `<input type="text" value="${data}" id="part_b${rowIndex}" name="part_b${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  }
                }
              },
              {
                  targets: 10,
                  data: "part_C",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) { 
                    return `<input type="text" value="" id="part_c${rowIndex}" name="part_c${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  }else if(data !=''){ 
                    return `<input type="text" value="${data}" id="part_c${rowIndex}" name="part_c${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  }
                  },
              },
              {
                  targets: 11,
                  data: "total_mark_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) { 
                    return `<input type="text" value="" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  }else if(data !=''){ 
                    return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                   }
                  },
              },
              {
                  targets: 12,
                  data: "result_status_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) { 

                    return `<input type="text" value="" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                  }else if(data !=''){  
                    return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                  }
                  },
              },
              {
                  targets: 13,
                  data: "final_verdict_status",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) { 
                    return `<input type="text" value="" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                  }else if(data !=''){  
                    return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                  }
                  },
              },
              {
                targets: 14,
                data: "submitted_status",
                //visible: false,
                render: function (data, type, full, meta) {

                
                if (data === null || data === '') {
                  return `Not Submit`;
                
                }else if (data !== null || data !== '') {
                  return `Submitted to SEDA`;
                }
                },
              },
              {
                  targets: 15,
                  data: "id",
                  //visible: false,
                  render: function (data, type, full, meta) {

                      if (data !== null || data !== '') {
                          return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}" title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                        
                      }
                 //return data;
                  },
              },
            ],
                language: {
                info: "view _START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "view 0 from 0 to 0 entry",
                lengthMenu: "view _MENU_ Entry",
                paginate: {
                  next: "Next",
                  previous: "Previous",
                },
                zeroRecords: "No record found",
              },
          });
       
      
      } else {
      // Your code for the second condition
      console.log("Condition 2 matched");
            } 
    } else if (numColumns === 10) //tableExamNameList_S5//template 2
    {
      if
        (columnNames[3] === findheader.exam_2 && columnNames[4] === findheader.exam_2 && columnNames[6] === "Theory Result" )  
        
        {  //alert('template 8 ')
          let senarai = $("#examResultUpdate").DataTable({
          data: data,
          // data:dataArray,
          //  order: [[4, "desc"]],
          columnDefs: [
            {
              targets: 0,
                data: function (row, type, full, meta) {
                // Use meta.row to get the row index (starting from 0)
                return meta.row + 1;
                },
                searchable: false,
                orderable: false,
            },
            {
              targets: 1,
              data: "fullname",
             //data: "id_training_participant",
              render: function (data, type, full, meta) {

                var rowIndex = meta.row; // Get the row index 
                return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
              },
            },
            {
              targets: 2,
              data: "exam_type",
            //data:"exam_type",
              render: function (data, type, full, meta) {

                var rowIndex = meta.row; // Get the row index 
                return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
               
              },
            },
            {
                targets: 3,
                data: "part_A_theory",
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index 
                  return `<input type="text" value="${data}" id="part_a_theory${rowIndex}" name="part_a_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                },
              },
            {
              targets: 4,
              data: "part_B_theory",
              render: function (data, type, full, meta) {
                var rowIndex = meta.row; // Get the row index 
                return `<input type="text" value="${data}" id="part_b_theory${rowIndex}" name="part_b_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                
              },
            },
            
            {
              targets: 5,
              data: "total_mark_theory",
              className: "text-center",
              render: function (data, type, full, meta) {

                var rowIndex = meta.row; // Get the row index 
                return `<input type="text" value="${data}" id="total_mark_theory${rowIndex}" name="total_mark_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"
                oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" >`;
              },
            },
            {
              targets: 6,
              data: "result_status_theory",
              //visible: false,
              render: function (data, type, full, meta) {
     
                var rowIndex = meta.row; // Get the row index 
                return `<input type="text" value="${data}" id="result_status_theory${rowIndex}" name="result_status_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
              },
            },
            
            {
                targets: 7,
                data: "final_verdict_status",
                //visible: false,
                render: function (data, type, full, meta) {
     
                  var rowIndex = meta.row;
                  return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                },
            },
     
            {
             targets: 8,
             data: "submitted_status",
             //visible: false,
             render: function (data, type, full, meta) {
     
              if (data === null || data === '') {
                return `Not Submit Yet`;
              
              }else if (data !== null || data !== '') {
                return `Submitted to SEDA`;
              }
             },
           },
            {
                targets: 9,
                data: "id",
                //visible: false,
                render: function (data, type, full, meta) {
     
                    if (data !== null || data !== '') {
                        return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}"title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                       
                    }
                // return data;
                },
            },
            
         
          ],
          language: {
            info: "view _START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "view 0 from 0 to 0 entry",
            lengthMenu: "view _MENU_ Entry",
            paginate: {
              next: "Next",
              previous: "Previous",
            },
            zeroRecords: "No record found",
          },
        });
      }
    }else if (numColumns === 15) //tableExamNameList_S2 template 3
      {
        if(
          (columnNames[3] === findheader.exam_1 && columnNames[4] === findheader.exam_2 && columnNames[5] === findheader.exam_3 && columnNames[7] === "Theory Result")
          && columnNames[8] === findheader.practical_exam_1 && columnNames[9] === findheader.practical_exam_2  && columnNames[11] === "Practical Result")  
          {
           // alert('template 8 ')
            let senarai = $("#examResultUpdate").DataTable({
              data: data,
              // data:dataArray,
              //  order: [[4, "desc"]],
              columnDefs: [
                {
                  targets: 0,
                  data: function (row, type, full, meta) {
                  // Use meta.row to get the row index (starting from 0)
                  return meta.row + 1;
                  },
                   searchable: false,
                   orderable: false,
                },
                {
                  targets: 1,
                  data: "fullname",
                 //data: "id_training_participant",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                  },
                },
                {
                  targets: 2,
                  data: "exam_type",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                    <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  },
                },
                {
                    targets: 3,
                    data: "part_A_theory",
                    render: function (data, type, full, meta) {
                      
                    var rowIndex = meta.row; // Get the row index                    
                    return `<input type="text" value="${data}" id="part_a_theory${rowIndex}" name="part_a_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                    },
                  },
                {
                  targets: 4,
                  data: "part_B_theory",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="part_b_theory${rowIndex}" name="part_b_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  
                    
                  },
                },
                {
                  targets: 5,
                  data: "part_C_theory",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="part_c_theory${rowIndex}" name="part_c_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                    
                  },
                },
                
                {
                  targets: 6,
                  data: "total_mark_theory",
                  className: "text-center",
                  render: function (data, type, full, meta) {

                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="total_mark_theory${rowIndex}" name="total_mark_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;

                  },
                },
                {
                  targets: 7,
                  data: "result_status_theory",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="result_status_theory${rowIndex}" name="result_status_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                 
                  },
                },
                {
                  targets: 8,
                  data: "part_A",
                  //visible: false,
                  render: function (data, type, full, meta) {

                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="part_a${rowIndex}" name="part_a${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  },
                },
                {
                  targets: 9,
                  data: "part_B",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="part_b${rowIndex}" name="part_b${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  },
                },
                {
                  targets: 10,
                  data: "total_mark_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  },
              },
              {
                  targets: 11,
                  data: "result_status_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                  },
              },
                
                {
                    targets: 12,
                    data: "final_verdict_status",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                    },
                },
         
                {
                 targets: 13,
                 data: "submitted_status",
                 //visible: false,
                 render: function (data, type, full, meta) {
         
                  if (data === null || data === '') {
                    return `Not Submit Yet`;
                  
                  }else if (data !== null || data !== '') {
                    return `Submitted to SEDA`;
                  }
                 },
               },
                {
                    targets: 14,
                    data: "id",
                    //visible: false,
                    render: function (data, type, full, meta) {
         
                        if (data !== null || data !== '') {
                            return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}"title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                           
                        }
                    // return data;
                    },
                },
                
             
              ],
              language: {
                info: "view _START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "view 0 from 0 to 0 entry",
                lengthMenu: "view _MENU_ Entry",
                paginate: {
                  next: "Next",
                  previous: "Previous",
                },
                zeroRecords: "No record found",
              },
            });
          }
      }//endif
      else if (numColumns === 11) //tableExamNameList_S4//template 4
      {
        if(
          (columnNames[3] === findheader.exam_1 && columnNames[4] === findheader.exam_2 && columnNames[5] === findheader.exam_3 && columnNames[7] === "Theory Result")) 
          {  ///alert('template 8 ')
            let senarai = $("#examResultUpdate").DataTable({
              data: data,
              // data:dataArray,
              //  order: [[4, "desc"]],
              columnDefs: [
                {
                  targets: 0,
                  data: function (row, type, full, meta) {
                  // Use meta.row to get the row index (starting from 0)
                  return meta.row + 1;
                  },
                  searchable: false,
                  orderable: false,
                },
                {
                  targets: 1,
                  data: "fullname",
                 //data: "id_training_participant",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  },
                },
                {
                  targets: 2,
                  data: "exam_type",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                    <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                  },
                },
                {
                    targets: 3,
                    data: "part_A_theory",
                    render: function (data, type, full, meta) {
                      
                      var rowIndex = meta.row; // Get the row index                    
                      return `<input type="text" value="${data}" id="part_a_theory${rowIndex}" name="part_a_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                     
                    },
                  },
                {
                  targets: 4,
                  data: "part_B_theory",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="part_b_theory${rowIndex}" name="part_b_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  
                    
                  },
                },
                {
                  targets: 5,
                  data: "part_C_theory",
                  render: function (data, type, full, meta) {
                  
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="part_c_theory${rowIndex}" name="part_c_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  },
                },
                {
                  targets: 6,
                  data: "total_mark_theory",
                  className: "text-center",
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="total_mark_theory${rowIndex}" name="total_mark_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  },
                },
                {
                  targets: 7,
                  data: "result_status_theory",
                  //visible: false,
                  render: function (data, type, full, meta) {
    
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="result_status_theory${rowIndex}" name="result_status_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                  },
                },
            
               
                {
                    targets: 8,
                    data: "final_verdict_status",
                    //visible: false,
                    render: function (data, type, full, meta) {
      
                      var rowIndex = meta.row;
                      return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                    },
                },
                {
                  targets: 9,
                  data: "submitted_status",
                  //visible: false,
                  render: function (data, type, full, meta) {
          
                   if (data === null || data === '') {
                     return `Not Submit Yet`;
                   
                   }else if (data !== null || data !== '') {
                     return `Submitted to SEDA`;
                   }
                  },
                },
                {
                    targets: 10,
                    data: "id",
                    //visible: false,
                    render: function (data, type, full, meta) {
    
                        if (data !== null || data !== '') {
                            return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}"title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                           
                        }
                    // return data;
                    },
                },
                
             
              ],
              language: {
                info: "view _START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "view 0 from 0 to 0 entry",
                lengthMenu: "view _MENU_ Entry",
                paginate: {
                  next: "Next",
                  previous: "Previous",
                },
                zeroRecords: "No record found",
              },
            });

          }
      }else if (numColumns === 14) //template 5
      { 
        if(
          (columnNames[3] === findheader.exam_1 && columnNames[4] === findheader.exam_2 && columnNames[5] === findheader.exam_3 && columnNames[7] === "Theory Result")
          && columnNames[8] === findheader.practical_exam_1 && columnNames[10] === "Practical Result")  
          {  //alert('template 8 ')
            let senarai = $("#examResultUpdate").DataTable({
              data: data,
              columnDefs: [
                {
                  targets: 0,
                  data: function (row, type, full, meta) {
                  // Use meta.row to get the row index (starting from 0)
                  return meta.row + 1;
                  },
                  searchable: false,
                  orderable: false,
                },
                {
                  targets: 1,
                  data: "fullname",
                 //data: "id_training_participant",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                    
                  },
                },
                {
                  targets: 2,
                   data: "exam_type",
                //data:"exam_type",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                    <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  },
                },
                {
                    targets: 3,
                    data: "part_A_theory",
                    render: function (data, type, full, meta) {
                      
                      var rowIndex = meta.row; // Get the row index                    
                      return `<input type="text" value="${data}" id="part_a_theory${rowIndex}" name="part_a_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                     
                    },
                  },
                {
                  targets: 4,
                  data: "part_B_theory",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="part_b_theory${rowIndex}" name="part_b_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                 
                  },
                },
                {
                  targets: 5,
                  data: "part_C_theory",
                  render: function (data, type, full, meta) {
                  
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="part_c_theory${rowIndex}" name="part_c_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  },
                },
                {
                  targets: 6,
                  data: "total_mark_theory",
                  className: "text-center",
                  render: function (data, type, full, meta) {
                  
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="total_mark_theory${rowIndex}" name="total_mark_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  },
                },
                {
                  targets: 7,
                  data: "result_status_theory",
                  //visible: false,
                  render: function (data, type, full, meta) {
    
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="result_status_theory${rowIndex}" name="result_status_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                    
                  },
                },
            
                {
                    targets: 8,
                    data: "part_A",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row; // Get the row index 
                      return `<input type="text" value="${data}" id="part_a${rowIndex}" name="part_a${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                    },
                },
               
                
                {
                    targets: 9,
                    data: "total_mark_practical",
                    //visible: false,
                    render: function (data, type, full, meta) {
      
                      var rowIndex = meta.row;
                      return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                    },
                },
                {
                    targets: 10,
                    data: "result_status_practical",
                    //visible: false,
                    render: function (data, type, full, meta) {
      
                      var rowIndex = meta.row;
                      return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                    },
                },
                {
                    targets: 11,
                    data: "final_verdict_status",
                    //visible: false,
                    render: function (data, type, full, meta) {
      
                      var rowIndex = meta.row;
                      return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                    },
                },
                {
                  targets: 12,
                  data: "submitted_status",
                  //visible: false,
                  render: function (data, type, full, meta) {
          
                   if (data === null || data === '') {
                     return `Not Submit Yet`;
                   
                   }else if (data !== null || data !== '') {
                     return `Submitted to SEDA`;
                   }
                  },
                },
                {
                    targets: 13,
                    data: "id",
                    //visible: false,
                    render: function (data, type, full, meta) {
    
                        if (data !== null || data !== '') {
                            return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}"title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                           
                        }
                    // return data;
                    },
                },
                
             
              ],
              language: {
                info: "view _START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "view 0 from 0 to 0 entry",
                lengthMenu: "view _MENU_ Entry",
                paginate: {
                  next: "Next",
                  previous: "Previous",
                },
                zeroRecords: "No record found",
              },
            });
          

          }
        }//endifelse if (numColumns === 17) 
        else if (numColumns === 13) //template 6
        { 
          if(
            (columnNames[3] === findheader.exam_1 && columnNames[4] === findheader.exam_2 && columnNames[6] === "Theory Result")
            && columnNames[7] === findheader.practical_exam_1  && columnNames[9] === "Practical Result")  
            { //alert(60);
              let senarai = $("#examResultUpdate").DataTable({
                data: data,
                // data:dataArray,
                //  order: [[4, "desc"]],
                columnDefs: [
                  {
                    targets: 0,
                    data: function (row, type, full, meta) {
                    // Use meta.row to get the row index (starting from 0)
                    return meta.row + 1;
                    },
                    searchable: false,
                    orderable: false,
                  },
                  {
                    targets: 1,
                    data: "fullname",
                   //data: "id_training_participant",
                    render: function (data, type, full, meta) {
                      
                      var rowIndex = meta.row; // Get the row index  
                      return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                    },
                  },
                  {
                    targets: 2,
                     data: "exam_type",
                  //data:"exam_type",
                    render: function (data, type, full, meta) {
                      
                      var rowIndex = meta.row; // Get the row index  
                      return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                      <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                    },
                  },
                  {
                      targets: 3,
                      data: "part_A_theory",
                      render: function (data, type, full, meta) {
                        
                        var rowIndex = meta.row; // Get the row index                    
                        return `<input type="text" value="${data}" id="part_a_theory${rowIndex}" name="part_a_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                       
                      },
                  },
                  {
                    targets: 4,
                    data: "part_B_theory",
                    render: function (data, type, full, meta) {
                      
                      var rowIndex = meta.row; // Get the row index 
                      return `<input type="text" value="${data}" id="part_b_theory${rowIndex}" name="part_b_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  
                      
                    },
                  },
                  
                  {
                    targets: 5,
                    data: "total_mark_theory",
                    className: "text-center",
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row; // Get the row index 
                      return `<input type="text" value="${data}" id="total_mark_theory${rowIndex}" name="total_mark_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                    },
                  },
                  {
                    targets: 6,
                    data: "result_status_theory",
                    //visible: false,
                    render: function (data, type, full, meta) {
      
                      var rowIndex = meta.row; // Get the row index 
                      return `<input type="text" value="${data}" id="result_status_theory${rowIndex}" name="result_status_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                    },
                  },
                  {
                    targets: 7,
                    data: "part_A",
                    //visible: false,
                    render: function (data, type, full, meta) {
      
                      var rowIndex = meta.row; // Get the row index 
                      return `<input type="text" value="${data}" id="part_a${rowIndex}" name="part_a${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                    },
                  },             
                  {
                    targets: 8,
                    data: "total_mark_practical",
                    //visible: false,
                    render: function (data, type, full, meta) {
      
                      var rowIndex = meta.row;
                      return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                    },
                  },
                  {
                    targets: 9,
                    data: "result_status_practical",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                    },
                  },
                  {
                      targets: 10,
                      data: "final_verdict_status",
                      //visible: false,
                      render: function (data, type, full, meta) {
                        var rowIndex = meta.row;
                        return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                      },
                  },
                  {
                   targets: 11,
                   data: "submitted_status",
                   //visible: false,
                   render: function (data, type, full, meta) {
           
                   return data;
                   },
                  },
                  {
                      targets: 12,
                      data: "id",
                      //visible: false,
                      render: function (data, type, full, meta) {
      
                          if (data !== null || data !== '') {
                              return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}"title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                             
                          }
                      // return data;
                      },
                  },
                  
               
                ],
                language: {
                  info: "view _START_ to _END_ from _TOTAL_ entry",
                  infoEmpty: "view 0 from 0 to 0 entry",
                  lengthMenu: "view _MENU_ Entry",
                  paginate: {
                    next: "Next",
                    previous: "Previous",
                  },
                  zeroRecords: "No record found",
                },
              });
            
            }
        }
          
};


      
//have to start new because datatatble stop reflect.
       let tableExamNameList2 = (data) => {
  
       if (numColumns === 14) //template 7
       { 
         if
           ((columnNames[3] === findheader.exam_1 && columnNames[4] === findheader.exam_2 && columnNames[5] === "Total" && columnNames[6] === "Theory Result")
           && columnNames[7] === findheader.practical_exam_1 && columnNames[8] === findheader.practical_exam_2 && columnNames[9] === "Total" && columnNames[10] === "Practical Result")  
           { //alert('template 7');
             let senarai = $("#examResultUpdate").DataTable({
               data: data,
               // data:dataArray,
               //  order: [[4, "desc"]],
               columnDefs: [
                 {
                    targets: 0,
                    data: function (row, type, full, meta) {
                    // Use meta.row to get the row index (starting from 0)
                    return meta.row + 1;
                    },
                    searchable: false,
                    orderable: false,
                 },
                 {
                   targets: 1,
                   data: "fullname",
                  //data: "id_training_participant",
                   render: function (data, type, full, meta) {

                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                    
                   },
                 },
                 {
                   targets: 2,
                    data: "exam_type",
                 //data:"exam_type",
                   render: function (data, type, full, meta) {
                     
                     var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                    <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                   },
                 },
                 {
                     targets: 3,
                     data: "part_A_theory",
                     render: function (data, type, full, meta) {
                       
                      var rowIndex = meta.row; // Get the row index                    
                      return `<input type="text" value="${data}" id="part_a_theory${rowIndex}" name="part_a_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                     },
                 },
                 {
                   targets: 4,
                   data: "part_B_theory",
                   render: function (data, type, full, meta) {
                     
                      var rowIndex = meta.row; // Get the row index 
                      return `<input type="text" value="${data}" id="part_b_theory${rowIndex}" name="part_b_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                      
                   },
                 },
                 
                 {
                   targets: 5,
                   data: "total_mark_theory",
                   className: "text-center",
                   render: function (data, type, full, meta) {
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="total_mark_theory${rowIndex}" name="total_mark_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                   },
                 },
                 {
                   targets: 6,
                   data: "result_status_theory",
                   //visible: false,
                   render: function (data, type, full, meta) {
     
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="result_status_theory${rowIndex}" name="result_status_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                   },
                 },
                 {
                   targets: 7,
                   data: "part_A",
                   //visible: false,
                   render: function (data, type, full, meta) {
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="part_a${rowIndex}" name="part_a${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                   },
                 },             
                 {
                  targets: 8,
                  data: "part_B",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row; // Get the row index 
                    return `<input type="text" value="${data}" id="part_b${rowIndex}" name="part_b${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  },
                },      
                 {
                   targets: 9,
                   data: "total_mark_practical",
                   //visible: false,
                   render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                   },
                 },
                 {
                   targets: 10,
                   data: "result_status_practical",
                   //visible: false,
                   render: function (data, type, full, meta) {
     
                    var rowIndex = meta.row;
                    return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                   },
                 },
                 {
                     targets: 11,
                     data: "final_verdict_status",
                     //visible: false,
                     render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                     },
                 },
                 {
                  targets: 12,
                  data: "submitted_status",
                  //visible: false,
                  render: function (data, type, full, meta) {
          
                  return data;
                  },
                 },
                 {
                     targets: 13,
                     data: "id",
                     //visible: false,
                     render: function (data, type, full, meta) {
     
                         if (data !== null || data !== '') {
                             return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}"title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                            
                         }
                     // return data;
                     },
                 },
                 
              
               ],
               language: {
                 info: "view _START_ to _END_ from _TOTAL_ entry",
                 infoEmpty: "view 0 from 0 to 0 entry",
                 lengthMenu: "view _MENU_ Entry",
                 paginate: {
                   next: "Next",
                   previous: "Previous",
                 },
                 zeroRecords: "No record found",
               },
             });
           
          }
       } else if (numColumns === 15) //template 8 
       {
       if(
         (columnNames[3] === findheader.exam_1 && columnNames[4] === findheader.exam_2 && columnNames[6] === "Theory Result")
         && columnNames[7] === findheader.practical_exam_1 && columnNames[8] === findheader.practical_exam_2 && columnNames[9] === findheader.practical_exam_3 && columnNames[11] === "Practical Result")  
         {
        //  alert('d');
          let senarai = $("#examResultUpdate").DataTable({
            data: data,
            // data:dataArray,
            //  order: [[4, "desc"]],
            columnDefs: [
              {
                targets: 0,
                data: function (row, type, full, meta) {
                // Use meta.row to get the row index (starting from 0)
                return meta.row + 1;
                },
                searchable: false,
                orderable: false,
              },
              {
                targets: 1,
                data: "fullname",
               //data: "id_training_participant",
                render: function (data, type, full, meta) {
                  
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                },
              },
              {
                targets: 2,
                 data: "exam_type",
              //data:"exam_type",
                render: function (data, type, full, meta) {
                  
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                  <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                },
              },
              {
                  targets: 3,
                  data: "part_A_theory",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index                    
                    return `<input type="text" value="${data}" id="part_a_theory${rowIndex}" name="part_a_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                   
                  },
              },
              {
                targets: 4,
                data: "part_B_theory",
                render: function (data, type, full, meta) {
                  
                  var rowIndex = meta.row; // Get the row index 
                  return `<input type="text" value="${data}" id="part_b_theory${rowIndex}" name="part_b_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  
                  
                },
              },
              
              {
                targets: 5,
                data: "total_mark_theory",
                className: "text-center",
                render: function (data, type, full, meta) {
              
                  var rowIndex = meta.row; // Get the row index 
                  return `<input type="text" value="${data}" id="total_mark_theory${rowIndex}" name="total_mark_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                },
              },
              {
                targets: 6,
                data: "result_status_theory",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index 
                  //return `<input type="text" value="${data}" id="result_status_theory${rowIndex}" name="result_status_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})">`;
                  return `<input type="text" value="${data}" id="result_status_theory${rowIndex}" name="result_status_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})"
                  oninput="handleInput(this)">`;
                },
              },
              {
                targets: 7,
                data: "part_A",
                //visible: false,
                render: function (data, type, full, meta) {
  
                  var rowIndex = meta.row; // Get the row index 
                  return `<input type="text" value="${data}" id="part_a${rowIndex}" name="part_a${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                },
              },             
              {
               targets: 8,
               data: "part_B",
               //visible: false,
               render: function (data, type, full, meta) {
                var rowIndex = meta.row; // Get the row index 
                return `<input type="text" value="${data}" id="part_b${rowIndex}" name="part_b${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;;
               },
             },    
             {
              targets: 9,
              data: "part_C",
              //visible: false,
              render: function (data, type, full, meta) {
                var rowIndex = meta.row;
                return `<input type="text" value="${data}" id="part_c${rowIndex}" name="part_c${rowIndex}" style="width: 60px;height:40px;text-align:center"
                oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
              },
            },      
              {
                targets: 10,
                data: "total_mark_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                },
              },
              {
                targets: 11,
                data: "result_status_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                },
              },
              {
                  targets: 12,
                  data: "final_verdict_status",
                  //visible: false,
                  render: function (data, type, full, meta) {
    
                    var rowIndex = meta.row;
                    return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                  },
              },
              {
               targets: 13,
               data: "submitted_status",
               //visible: false,
               render: function (data, type, full, meta) {
       
               return data;
               },
              },
              {
                  targets: 14,
                  data: "id",
                  //visible: false,
                  render: function (data, type, full, meta) {
  
                      if (data !== null || data !== '') {
                          return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}"title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                         
                      }
                  // return data;
                  },
              },
              
           
            ],
            language: {
              info: "view _START_ to _END_ from _TOTAL_ entry",
              infoEmpty: "view 0 from 0 to 0 entry",
              lengthMenu: "view _MENU_ Entry",
              paginate: {
                next: "Next",
                previous: "Previous",
              },
              zeroRecords: "No record found",
            },
          });
        
        }
       }//endif
      
      }


//have to start new
let tableExamNameList3 = (data) => {

      if (numColumns === 14) //template 9 
      {
        if(
        (columnNames[3] === findheader.exam_1 && columnNames[5] === "Theory Result")
        && columnNames[6] === findheader.practical_exam_1 && columnNames[7] === findheader.practical_exam_2 && columnNames[8] === findheader.practical_exam_3 && columnNames[10] === "Practical Result")  
        {
         let senarai = $("#examResultUpdate").DataTable({
           data: data,
           // data:dataArray,
           //  order: [[4, "desc"]],
           columnDefs: [
             {
              targets: 0,
              data: function (row, type, full, meta) {
                  // Use meta.row to get the row index (starting from 0)
                  return meta.row + 1;
              },
              searchable: false,
              orderable: false,
             },
             {
               targets: 1,
               data: "fullname",
              //data: "id_training_participant",
               render: function (data, type, full, meta) {
                var rowIndex = meta.row; // Get the row index  
                return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                
               },
             },
             {
               targets: 2,
                data: "exam_type",
             //data:"exam_type",
               render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                  <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
               },
             },
             {
                 targets: 3,
                 data: "part_A_theory",
                 render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index                    
                  return `<input type="text" value="${data}" id="part_a_theory${rowIndex}" name="part_a_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                 },
             },
             
             {
               targets: 4,
               data: "total_mark_theory",
               className: "text-center",
               render: function (data, type, full, meta) {
                var rowIndex = meta.row; // Get the row index 
                return `<input type="text" value="${data}" id="total_mark_theory${rowIndex}" name="total_mark_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"
                oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
               },
             },
             {
               targets: 5,
               data: "result_status_theory",
               //visible: false,
               render: function (data, type, full, meta) {
                var rowIndex = meta.row; // Get the row index 
                return `<input type="text" value="${data}" id="result_status_theory${rowIndex}" name="result_status_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
               },
             },
             {
               targets: 6,
               data: "part_A",
               //visible: false,
               render: function (data, type, full, meta) {
                var rowIndex = meta.row; // Get the row index 
                return `<input type="text" value="${data}" id="part_a${rowIndex}" name="part_a${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
               },
             },             
             {
              targets: 7,
              data: "part_B",
              //visible: false,
              render: function (data, type, full, meta) {
                var rowIndex = meta.row; // Get the row index 
                return `<input type="text" value="${data}" id="part_b${rowIndex}" name="part_b${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
              },
            },    
            {
             targets: 8,
             data: "part_C",
             //visible: false,
             render: function (data, type, full, meta) {
              var rowIndex = meta.row;
              return `<input type="text" value="${data}" id="part_c${rowIndex}" name="part_c${rowIndex}" style="width: 60px;height:40px;text-align:center"
              oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
             },
           },      
             {
               targets: 9,
               data: "total_mark_practical",
               //visible: false,
               render: function (data, type, full, meta) {
                var rowIndex = meta.row;
                return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
               },
             },
             {
               targets: 10,
               data: "result_status_practical",
               //visible: false,
               render: function (data, type, full, meta) {
                var rowIndex = meta.row;
                return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
               },
             },
             {
                 targets: 11,
                 data: "final_verdict_status",
                 //visible: false,
                 render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                 },
             },
             {
              targets: 12,
              data: "submitted_status",
              //visible: false,
              render: function (data, type, full, meta) {
      
              return data;
              },
             },
             {
                 targets: 13,
                 data: "id",
                 //visible: false,
                 render: function (data, type, full, meta) {
 
                     if (data !== null || data !== '') {
                         return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}"title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                        
                     }
                 // return data;
                 },
             },
             
          
           ],
           language: {
             info: "view _START_ to _END_ from _TOTAL_ entry",
             infoEmpty: "view 0 from 0 to 0 entry",
             lengthMenu: "view _MENU_ Entry",
             paginate: {
               next: "Next",
               previous: "Previous",
             },
             zeroRecords: "No record found",
           },
         });
       
       }
      }  if (numColumns === 13) //template 10 
      {
        if(
        (columnNames[3] === findheader.exam_1 && columnNames[5] === "Theory Result")
        && columnNames[6] === findheader.practical_exam_1 && columnNames[7] === findheader.practical_exam_2 && columnNames[9] === "Practical Result")  
        {
          let senarai = $("#examResultUpdate").DataTable({
            data: data,
            // data:dataArray,
            //  order: [[4, "desc"]],
            columnDefs: [
              {
                targets: 0,
                data: function (row, type, full, meta) {
                // Use meta.row to get the row index (starting from 0)
                return meta.row + 1;
              },
              searchable: false,
              orderable: false,
              },
              {
                targets: 1,
                data: "fullname",
               //data: "id_training_participant",
                render: function (data, type, full, meta) {
                  
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                },
              },
              {
                targets: 2,
                 data: "exam_type",
              //data:"exam_type",
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                  <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                },
              },
              {
                  targets: 3,
                  data: "part_A_theory",
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row; // Get the row index                    
                    return `<input type="text" value="${data}" id="part_a_theory${rowIndex}" name="part_a_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  },
              },
              
              {
                targets: 4,
                data: "total_mark_theory",
                className: "text-center",
                render: function (data, type, full, meta) {
                
                  var rowIndex = meta.row; // Get the row index 
                  return `<input type="text" value="${data}" id="total_mark_theory${rowIndex}" name="total_mark_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                },
              },
              {
                targets: 5,
                data: "result_status_theory",
                //visible: false,
                render: function (data, type, full, meta) {
  
                  var rowIndex = meta.row; // Get the row index 
                  return `<input type="text" value="${data}" id="result_status_theory${rowIndex}" name="result_status_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                },
              },
              {
                targets: 6,
                data: "part_A",
                //visible: false,
                render: function (data, type, full, meta) {
  
                  var rowIndex = meta.row; // Get the row index 
                  return `<input type="text" value="${data}" id="part_a${rowIndex}" name="part_a${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                },
              },             
              {
               targets: 7,
               data: "part_B",
               //visible: false,
               render: function (data, type, full, meta) {
                var rowIndex = meta.row; // Get the row index 
                return `<input type="text" value="${data}" id="part_b${rowIndex}" name="part_b${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
               },
             },    
                
              {
                targets: 8,
                data: "total_mark_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                },
              },
              {
                targets: 9,
                data: "result_status_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)" >`;
                },
              },
              {
                  targets: 10,
                  data: "final_verdict_status",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                  },
              },
              {
               targets: 11,
               data: "submitted_status",
               //visible: false,
               render: function (data, type, full, meta) {
       
               return data;
               },
              },
              {
                  targets: 12,
                  data: "id",
                  //visible: false,
                  render: function (data, type, full, meta) {
  
                      if (data !== null || data !== '') {
                          return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}"title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                         
                      }
                  // return data;
                  },
              },
              
           
            ],
            language: {
              info: "view _START_ to _END_ from _TOTAL_ entry",
              infoEmpty: "view 0 from 0 to 0 entry",
              lengthMenu: "view _MENU_ Entry",
              paginate: {
                next: "Next",
                previous: "Previous",
              },
              zeroRecords: "No record found",
            },
          });
        }

      }if (numColumns === 12) //template 11 
      {
        if(
        (columnNames[3] === findheader.exam_1 && columnNames[5] === "Theory Result")
        && columnNames[6] === findheader.practical_exam_1 && columnNames[8] === "Practical Result")  
        {
          let senarai = $("#examResultUpdate").DataTable({
            data: data,
            // data:dataArray,
            //  order: [[4, "desc"]],
            columnDefs: [
              {
                targets: 0,
                data: function (row, type, full, meta) {
                  // Use meta.row to get the row index (starting from 0)
                  return meta.row + 1;
                },
                searchable: false,
                orderable: false,
              },
              {
                targets: 1,
                data: "fullname",
               //data: "id_training_participant",
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                },
              },
              {
                targets: 2,
                 data: "exam_type",
              //data:"exam_type",
                render: function (data, type, full, meta) {
                  
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                  <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                },
              },
              {
                  targets: 3,
                  data: "part_A_theory",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index                    
                    return `<input type="text" value="${data}" id="part_a_theory${rowIndex}" name="part_a_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                  },
              },
              
              {
                targets: 4,
                data: "total_mark_theory",
                className: "text-center",
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index 
                  return `<input type="text" value="${data}" id="total_mark_theory${rowIndex}" name="total_mark_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                },
              },
              {
                targets: 5,
                data: "result_status_theory",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index 
                  return `<input type="text" value="${data}" id="result_status_theory${rowIndex}" name="result_status_theory${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                },
              },
              {
                targets: 6,
                data: "part_A",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index 
                  return `<input type="text" value="${data}" id="part_a${rowIndex}" name="part_a${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                },
              },             
             
              {
                targets: 7,
                data: "total_mark_practical",
                //visible: false,
                render: function (data, type, full, meta) {
  
                  var rowIndex = meta.row;
                  return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})">`;
                },
              },
              {
                targets: 8,
                data: "result_status_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                },
              },
              {
                  targets: 9,
                  data: "final_verdict_status",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" oninput="handleInput(this)">`;
                  },
              },
              {
               targets: 10,
               data: "submitted_status",
               //visible: false,
               render: function (data, type, full, meta) {
       
               return data;
               },
              },
              {
                  targets: 11,
                  data: "id",
                  //visible: false,
                  render: function (data, type, full, meta) {
  
                      if (data !== null || data !== '') {
                          return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}"title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                         
                      }
                  // return data;
                  },
              },
              
           
            ],
            language: {
              info: "view _START_ to _END_ from _TOTAL_ entry",
              infoEmpty: "view 0 from 0 to 0 entry",
              lengthMenu: "view _MENU_ Entry",
              paginate: {
                next: "Next",
                previous: "Previous",
              },
              zeroRecords: "No record found",
            },
          });
        }

      }
    }
//////////////////////////


///////////////////////
    

//////////////////////////////////////
       
//////////////////////////////
      

////////////////////////////////////////////
      
///////////////////////////////////
      function validateBorangKemaskini() {
        let IsError = false;
      
        if ($("#partA").val()) {
          $("#error-partA").hide();
        } else {
          $("#error-partA").show();
          IsError = true
        }
      
        return IsError;
      }
      /////////////////////
      function validateInput(input) {
        // Remove any non-numeric or non-decimal characters
        input.value = input.value.replace(/[^0-9.]/g, '');
      
        // Ensure the value is not more than 100
        if (parseFloat(input.value) > 100) {
            input.value = '100';
        }
      }


      function MarkAndCalculate(rowIndex) {
        AutoCalculate(rowIndex);
        MarkAutoSave(rowIndex);
       
    }

    function handleInput(input) { //make it auto Pass or Failed by press p or f
      // convert to lowercase 1st
      let inputValue = input.value.toLowerCase();
  
      // Check if the input is 'p' or 'f' (case-insensitive) and set the corresponding result
      if (inputValue === 'p') {
          input.value = 'Pass';
      } else if (inputValue === 'f') {
          input.value = 'Failed';
      }
    }
   
     
      function AutoCalculate(rowIndex) {
        // Get the values of part A and part B theory from the respective input fields
        // var partAValue = parseFloat(document.getElementById(`part_a_theory${rowIndex}`).value) || 0;
        // var partBValue = parseFloat(document.getElementById(`part_b_theory${rowIndex}`).value) || 0;
        // var partCValue = parseFloat(document.getElementById(`part_c_theory${rowIndex}`).value) || 0;
    
        // // Calculate the total marks
        // var total_mark_theory = partAValue + partBValue + partCValue;
      
        // // Update the value of the total mark theory input field
        // document.getElementById(`total_mark_theory${rowIndex}`).value = totalMarks;

        //////////////////////////////////////////////////////////////////////////////////

        // let partAtheory = parseFloat($('#part_a_theory' + rowIndex).val()) || 0;
        // let partBtheory = parseFloat($('#part_b_theory' + rowIndex).val()) || 0;
        // let partCtheory = parseFloat($('#part_c_theory' + rowIndex).val()) || 0;

        // let partApractical = parseFloat($('#part_A' + rowIndex).val()) || 0;
        // let partBpractical = parseFloat($('#part_B' + rowIndex).val()) || 0;
        // let partCpractical = parseFloat($('#part_C' + rowIndex).val()) || 0;

        

        // // Calculate the total marks
        // let total_mark_theory = partAtheory + partBtheory + partCtheory;
        // let total_mark_practical = partApractical + partBpractical + partCpractical;

        // // Update the value of the total mark theory input field
        // $('#total_mark_theory' + rowIndex).val(total_mark_theory);
        // $('#total_mark_practical' + rowIndex).val(total_mark_practical);

        //make final total is 100

        /////////////////////////////////////////////////////////////////////////////////

        // let partAtheory = parseFloat($('#part_a_theory' + rowIndex).val()) || 0;
        // let partBtheory = parseFloat($('#part_b_theory' + rowIndex).val()) || 0;
        // let partCtheory = parseFloat($('#part_c_theory' + rowIndex).val()) || 0;
    
        // // Calculate the total marks
        // let total_mark_theory = partAtheory + partBtheory + partCtheory;
    
        // // Limit the total mark theory to 100
        // total_mark_theory1 = Math.min(total_mark_theory, 100);
    
        // // Update the value of the total mark theory input field
        // $('#total_mark_theory' + rowIndex).val(total_mark_theory1);

      }
    
     
        let examArray= [];

        const MarkAutoSave = (id) => {
          let partAtheory = parseFloat($('#part_a_theory' + id).val()) || 0;
          let partBtheory = parseFloat($('#part_b_theory' + id).val()) || 0;
          let partCtheory = parseFloat($('#part_c_theory' + id).val()) || 0;


          let partApractical = parseFloat($('#part_a' + id).val()) || 0;
          let partBpractical = parseFloat($('#part_b' + id).val()) || 0;
          let partCpractical = parseFloat($('#part_c' + id).val()) || 0;
  
          // Calculate the total marks
          let total_mark_theory = partAtheory + partBtheory + partCtheory;
          let total_mark_practical = partApractical + partBpractical + partCpractical;
         
          total_mark_theory = Math.min(total_mark_theory, 100);
          total_mark_practical = Math.min(total_mark_practical, 100);
  
          // Update the value of the total mark theory input field
          $('#total_mark_theory' + id).val(total_mark_theory);
          $('#total_mark_practical' + id).val(total_mark_practical);

          $('#result_status_theory' + id).on('input', function () {
            // Get the current value of the input field and convert to lowercase
            let inputValue = $(this).val().toLowerCase();
        
            // Check if the input is 'p' or 'f' (case-insensitive) and set the corresponding result
            if (inputValue === 'p') {
                $(this).val('Pass');
            } else if (inputValue === 'f') {
                $(this).val('Failed');
            }
        });
        
            let exist = examArray.find( x => x.id == id );
              
                    let id_Training_participant = $('#id_training_participant'+id).val();
                    let id_Training = $('#id_training'+id).val();
                    let id_Participant_examresult = $('#id_participant_examresult'+id).val();

                    let part_A_theory = $('#part_a_theory'+id).val();
                    let part_B_theory = $('#part_b_theory'+id).val();
                    let part_C_theory = $('#part_c_theory'+id).val();
                    let total_Mark_theory = $('#total_mark_theory'+id).val();
                    let result_Status_theory = $('#result_status_theory'+id).val();
                    let part_A = $('#part_a'+id).val();
                    let part_B = $('#part_b'+id).val();
                    let part_C = $('#part_c'+id).val();
                    let total_Mark_practical = $('#total_mark_practical'+id).val();
                
                    let result_Status_practical = $('#result_status_practical'+id).val();
                    let final_Verdict_status = $('#final_verdict_status'+id).val();  

                  //  console.log('Autosave:', HubungkaitdgnPegawai);  
            if(exist === undefined){

              // let dataToSave = {
              
              examArray.push({
              
                    id: id,
                    
                    id_Training_participant : $('#id_training_participant'+id).val(),
                    id_Training : $('#id_training'+id).val(),
                    id_Participant_examresult : $('#id_participant_examresult'+id).val(),

                    part_A_theory : $('#part_a_theory'+id).val(),
                    part_B_theory : $('#part_b_theory'+id).val(),
                    part_C_theory : $('#part_c_theory'+id).val(),
                    total_Mark_theory : $('#total_mark_theory'+id).val(),
                    result_Status_theory : $('#result_status_theory'+id).val(),
                    part_A : $('#part_a'+id).val(),
                    part_B : $('#part_b'+id).val(),
                    part_C : $('#part_c'+id).val(),
                    total_Mark_practical : $('#total_mark_practical'+id).val(),
                 
                    result_Status_practical : $('#result_status_practical'+id).val(),
                    final_Verdict_status : $('#final_verdict_status'+id).val(),
              
                  // }
                });
              
                // console.log('Autosave part_A_theory:', part_A_theory);
                // console.log('Autosave part_B_theory:', part_B_theory);
                // console.log('Autosave part_C_theory:', part_C_theory);
                // console.log('Autosave total_Mark_theory:', total_Mark_theory);
                // console.log('Autosave result_Status_theory:', result_Status_theory);
                // console.log('Autosave part_A:', part_A);
                // console.log('Autosave part_B:', part_B);
                // console.log('Autosave part_C:', part_C);
                // console.log('Autosave total_Mark_practical:', total_Mark_practical);
                // console.log('Autosave result_Status_practical:', result_Status_practical);
                // console.log('Autosave final_Verdict_status:', final_Verdict_status);
                


        
            }else if(exist !== undefined){
        
                    exist.id_Training_participant = $('#id_training_participant'+id).val(),
                    exist.id_Training = $('#id_training'+id).val(),
                    exist.id_Participant_examresult = $('#id_participant_examresult'+id).val(),

                    exist.part_A_theory = $('#part_a_theory'+id).val();
                    exist.part_B_theory = $('#part_b_theory'+id).val();
                    exist.part_C_theory = $('#part_c_theory'+id).val();
                    exist.total_Mark_theory = $('#total_mark_theory'+id).val();
                    exist.result_Status_theory = $('#result_status_theory'+id).val();
                    exist.part_A = $('#part_a'+id).val();
                    exist.part_B = $('#part_b'+id).val();
                    exist.part_C = $('#part_c'+id).val();
                    exist.total_Mark_practical = $('#total_mark_practical'+id).val();
                
                    exist.result_Status_practical = $('#result_status_practical'+id).val();
                    exist.final_Verdict_status = $('#final_verdict_status'+id).val();
            }
        
           // $('#deleteKH'+id).show();
        
        
        
       
        
      
    }



//       let examArray = [];

//       const MarkAutoSave = (id) => {

  
//                     let id_Training_participant = $('#id_training_participant'+id).val();
//                     let id_Training = $('#id_training'+id).val();
//                     let part_A_theory = $('#part_a_theory'+id).val();
//                     let part_B_theory = $('#part_b_theory'+id).val();
//                     let part_C_theory = $('#part_c_theory'+id).val();
//                     let total_Mark_theory = $('#total_mark_theory'+id).val();
//                     let result_Status_theory = $('#result_status_theory'+id).val();
//                     let part_A = $('#part_a'+id).val();
//                     let part_B = $('#part_b'+id).val();
//                     let part_C = $('#part_c'+id).val();
//                     let total_Mark_practical = $('#total_mark_practical'+id).val();
                
//                     let result_Status_practical = $('#result_status_practical'+id).val();
//                     let final_Verdict_status = $('#final_verdict_status'+id).val();  


//                     let dataToSave = {
//                         //id: id,
                       

//                         id_Training_participant : $('#id_training_participant'+id).val(),
//                         id_Training : $('#id_training'+id).val(),
//                         part_A_theory : $('#part_a_theory'+id).val(),
//                         part_B_theory : $('#part_b_theory'+id).val(),
//                         part_C_theory : $('#part_c_theory'+id).val(),
//                         total_Mark_theory : $('#total_mark_theory'+id).val(),
//                         result_Status_theory : $('#result_status_theory'+id).val(),
//                         part_A : $('#part_a'+id).val(),
//                         part_B : $('#part_b'+id).val(),
//                         part_C : $('#part_c'+id).val(),
//                         total_Mark_practical : $('#total_mark_practical'+id).val(),
                                     
//                         result_Status_practical : $('#result_status_practical'+id).val(),
//                         final_Verdict_status : $('#final_verdict_status'+id).val(),
//                     };

//                 let exist = examArray.find(x => x.id == id);

//                 if (exist === undefined) {
//                     examArray.push(dataToSave);
//                 } else {
//                 // Update existing data in the array
//                   Object.assign(exist, dataToSave);
//                 }
//               };

//               // Set up an interval to continuously monitor the value of $('#id_training' + id).val()
//               let monitorInterval = setInterval(function () {
//               let idTrainingValue = $('#id_training' + id).val();
//               let id_Training_participant = $('#id_training_participant' + id).val();
//               //console.log(`Value of id_training${id}:`, idTrainingValue);

//               // You can add additional logic here based on the monitored value

//           }, 1000); // Check every 1000 milliseconds (1 second)

// // Later, if you want to stop monitoring
// // clearInterval(monitorInterval);