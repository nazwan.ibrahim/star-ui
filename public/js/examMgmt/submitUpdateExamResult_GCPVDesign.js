
let TPUpdateExamResult_GCPVDesign = () => {
   
       
    let url = window.location.pathname;
    let batch_full = url.substring(url.lastIndexOf('/') + 1);
    let segments = url.split('/');
    let ID = segments[segments.length - 2];
    $("#SubmitBtn").on("click", () => {
        $("#confirmationEditModal").modal('show');
    });
      
   
    $("#submitKemaskiniBtn").on("click", () => {
        $("#confirmationEditModal").modal("hide");
       
        // let editExamResult = host + '/api/exam/editExamResult';
        let editExamResult_GCPV = `${host}/api/exam/${ID}/${batch_full}/editExamResult_GCPV`;
        
        let obj = {
            examArray_GCPV : examArray_GCPV,
        }


    
        let postData = $.ajax({
          url: editExamResult_GCPV,
          method: "PUT",
          data: obj,
        }) .done((res) => {
         console.log('data',res);
  
            
         $("#success-save-modal").modal({
                backdrop: 'static',
                keyboard: false,
                }).modal('show');
        })
        .catch((err) => {
          //console.log(err);
        });
        $.when(postData).done( (res) => {
        
          

          // window.location.href = `${host}/exam/updateExamResult/${ID}/${batch_full}`;

          
           
  
       })
  
  
      });
  
 }


 let TPUpdateExamResultBC = () => {

       
    let url = window.location.pathname;
    let batch_full = url.substring(url.lastIndexOf('/') + 1);
    let segments = url.split('/');
    let ID = segments[segments.length - 2];
  
          
              //let batch_full = url.substring(url.lastIndexOf('/') + 1);
  
      $(document).on('click', '#hantarEditBtn', function() {
      
  //alert('a');
          //$('#edit-result-modal').modal('hide');
          $('#confirmationEditModal').modal('show');
      
      });
     
      $("#submitKemaskiniBtn").on("click", () => {
          $("#confirmationEditModal").modal("hide");
         
          // let editExamResult = host + '/api/exam/editExamResult';
          let editExamResult = `${host}/api/exam/${ID}/${batch_full}/editExamResult`;
          
    
        const data = {
  
          id_participant : $('#formEditResult input[name="id_participant_examresult"]').val(),
          id_training : $('#formEditResult input[name="id_training"]').val(),
  
          exam_1 : $('#formEditResult input[name="partA"]').val(),
          exam_2 : $('#formEditResult input[name="partB"]').val(),
          exam_3 :  $('#formEditResult input[name="partC"]').val(),
          totalResultPractical : $('#formEditResult input[name="totalResultPractical"]').val(),
          statusResultPracticalExam : $('#formEditResult select[name="statusResultPracticalExam"]').val(),
        
          
          
          exam_1_theory : $('#formEditResult input[name="part-AT"]').val(),
          exam_2_theory : $('#formEditResult input[name="part-BT"]').val(),
          exam_3_theory :  $('#formEditResult input[name="part-CT"]').val(),
          totalResultTheory : $('#formEditResult input[name="totalResultTheory"]').val(),
          statusResultTheory : $('#formEditResult select[name="statusResultTheory"]').val(),
        
          statusResultFinal : $('#formEditResult select[name="statusResultFinal"]').val(),
    
            };
          //console.log(data);
          let postData = $.ajax({
            url: editExamResult,
            method: "PUT",
            data: data,
          }) .done((res) => {
           console.log('data',res);
    
          //   setTimeout(() => {
    
          //     $("#loadingModal").modal("hide");
          //     $("#berjaya-simpan-modal").modal({
          //         backdrop: 'static',
          //         keyboard: false,
          //     });
          //     $("#loadingModal").modal("hide");
    
          // }, 2000);
    
    
          })
          .catch((err) => {
            //console.log(err);
          });
          $.when(postData).done( (res) => {
          
            
  
             window.location.href = `${host}/exam/updateExamResult/${ID}/${batch_full}`;
  
            
             
    
         })
    
    
        });
    
   }
  
  


 $(function(){

    TPUpdateExamResult_GCPVDesign();
   
  });