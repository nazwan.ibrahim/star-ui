
$(function(){

    ExamList();
    TrainingDetails();


});


let TrainingDetails = () => {


  ID =   $(this).data("id");

  let getList = `${host}/api/exam/${ID}/examDetails`;
 $.ajax({
   url: getList,
   type: "GET",
   dataType: "JSON",
 })
   .done((res) => {
     //createTable(res.data);
     data = res.trainingDetails;
    
     (value.training_name == null || value.training_name == '') ? $('#provide_name').text('n/a') : $('#provide_name').text(value.training_name); 
     (value.date_start == null || value.date_start == '') ? $('#date_start').text('n/a') : $('#date_start').text(value.date_start);
     (value.date_end == null || value.date_end == '') ? $('#date_end').text('n/a') : $('#date_end').text(value.date_end);
     (value.venue == null || value.venue == '') ? $('#venue').text('n/a') : $('#venue').text(value.venue);

   })
   .catch((err) => {});
};


    let ExamList = () => {

          userid = userID;

         let getList = `${host}/api/exam/${userid}/examTrainingListTP`;
             
        //  let userid = userID;

        //  let getList = `${host}/api/training/${userid}/trainingManagementListTP`;
         
        $.ajax({
          url: getList,
          type: "GET",
          dataType: "JSON",
        })
          .done((res) => {
            //createTable(res.data);
            tableExamList(res.examlist);
            console.log(res.examlist);
          })
          .catch((err) => {});
      };
 

      let tableExamList = (data) => {
        let senarai = $("#examTrainingList").DataTable({
          data: data,
          //  order: [[4, "desc"]],
          columnDefs: [
            {
              targets: 0,
              data: null,
              searchable: false,
              orderable: false,
            },
            {
              targets: 1,
              data: "course_name",
              render: function (data, type, full, meta) {
                if (data === null) {
                  return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
                }
                if (data !== null || data !== "") {
                  return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
                }
              },
            },
          
             {
              targets: 2,
              data: "batch_no_full",
              render: function (data, type, full, meta) {

                if ((data == null) || (data == '')){
                  return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
                }
                else if (data !== null || data !== "") {
                return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                }
               
              },
            },
            {
                targets: 3,
                data: "date_start",
                render: function (data, type, full, meta) {
                  if (data && data !== '' && data !== null) {
                    // Assuming 'data1' and 'data2' are the two data values you want to display
                    var data1 = full.date_start; // Replace 'data1' with your actual data field name
                    var data2 = full.date_end; // Replace 'data2' with your actual data field name
              
                    // Combine 'data1' and 'data2' into one string
                    var combinedData = data1 + ' to ' + data2;
              
                    // You can also use HTML for formatting
                    var formattedData = '<p class="poppins-semibold-14">' + combinedData + '</p>';
              
                    return formattedData;
                  } else {
                    return '<td width="15%"><p class="poppins-semibold-14">-</p></td>';
                  }
                },
              },
            {
              targets: 4,
              data: "venue",
              render: function (data, type, full, meta) {
                if(data && data != '' && data != null) {
                
                  return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                } else {
                  return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
                }
                
              },
            },
            {
              targets: 5,
              data: "endorsed_training",
              className: "text-center",
              render: function (data, type, full, meta) {
                if (data === null) {
                  return `<td width="20%" align="center"><p class="table-content">-</p></td>`;
                }
                if (data == 1 ) {
                  return `<td align="center" width="30%"><p class="table-content" style="text-align: center;">Endorsed</p></td>`;
                }
                if (data == 2 ) {
                  return `<td align="center" width="30%"><p class="table-content" style="text-align: center;">Query</p></td>`;
                }
                if (data == 3 ) {
                  return `<td align="center" width="30%"><p class="table-content" style="text-align: center;">Not Endorse</p></td>`;
                }
              },
            },
            {
              targets: 6,
              data: "status",
              render: function (data, type, full, meta) {
                
                if(data == 1) {
                  
                    return `<div class="paid-status poppins-medium-12">Up coming</div>`;
                }else if(data == 2) {
                  
                  return `<div class="null-value poppins-medium-12">On Going</div>`;
                }else if(data == 3) {
                  
                  return `<div class="success-status poppins-medium-12">Completed</div>`;
                }else
                 {
                  return "-";
                }
              },
            },
            
            {
              targets:7,
              data: "id_training_course",
              //visible: false,
              render: function (data, type, full, meta) {

  
                  if (data != null || data !== "") {
            
                   return  `<a href="${host}/exam/updateExamResult/${data}/${full.batch_no_full}" class="btn btn-sm">
                              <i class="fas fa-edit" alt="description" style="font-size: 18px; color: #3498DB;"></i>
                            </a>`;


                        //      return  `<a href="${host}/exam/viewExamResult" class="btn btn-sm">
                        //      <i class="icon-editIcon" style="font-size: 18px;"></i>
                        //  </a>
                        //  <a href="${host}/exam/updateExamResult/${data}" class="btn btn-sm">
                        //  <button class="btn btn-sm icon-style" data-id="${data}" 
                        //  <i class="fas fa-edit" alt="description" style="font-size: 18px; color: #3498DB;"></i>
                        //  </button></a>
                         
                        // `;
                  }

                  
                //return data;
              },
            },
          ],
          language: {
            info: "view _START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "view 0 from 0 to 0 entry",
            lengthMenu: "view _MENU_ Entry",
            paginate: {
              next: "Next",
              previous: "Previous",
            },
            zeroRecords: "No record found",
          },
        });
      
        senarai
          .on("order.dt search.dt", function () {
            senarai
              .column(0, { search: "applied", order: "applied" })
              .nodes()
              .each(function (cell, i) {
                cell.innerHTML = i + 1;
              });
          })
          .draw();
      
    
      
        $("#filterJenisKenderaan").on("change", (e) => {
          console.log(e.target.value);
          senarai.column(1).search(e.target.value).draw();
        });
      
        $("#search_input").on("keyup", () => {
          let searched = $("#search_input").val();
          // console.log(searched)
          senarai.search(searched).draw();
        });
      
        $("#resetFilter").on("click", () => {
          $("#filterStatus").val("").trigger("change");
          $("#filterJenisKenderaan").val("").trigger("change");
          $("#search_input").val("");
      
          senarai.search("").columns().search("").draw();
        });
      };
      

      //////
