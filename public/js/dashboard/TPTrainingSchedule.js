document.addEventListener("DOMContentLoaded", function () {
  const createDoughnutChart = (ctx, data, label) => {
    console.log('Chart Data:', data);
    return new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: Object.keys(data),
            datasets: [{
                data: Object.values(data),
                backgroundColor: [
                    'rgba(115, 192, 222, 1)',
                    'rgba(83, 112, 198, 1)',
                    'rgba(250, 199, 87, 1)',
                    'rgba(238, 102, 102, 1)',
                    'rgba(146, 204, 118, 1)',
                    'rgba(128, 0, 0, 1)',
                ],
            }],
        },
        options: {
            responsive: true,
            legend: {
                display: false,
            },
            title: {
                display: true,
                text: label,
            },
        },
    });
  };
  // Assuming you have the API responses available as SECounts, RECounts, and EECounts
    // SEList
    let chart1Ctx = document.getElementById('doughnut-chart-1').getContext('2d');
    $.ajax({
        url: `${host}/api/dashboard/SEList`,
        type: "GET",
        dataType: "JSON",
    })
        .done((response) => {
            if (response.status === 'success') {
                if (Object.keys(response.SECounts).length > 0) {
                    createDoughnutChart(chart1Ctx, response.SECounts, 'SE Chart');
                } else {
                    // displayNoDataMessage('doughnut-chart-1');
                    document.getElementById('no-data-message').style.display = 'block';
                }
            } else {
                console.error('Error:', response.message);
            }
        })
        .fail((err) => {
            console.error('AJAX request failed:', err);
        });
    
    // Function to display a message when no data is available
    function displayNoDataMessage(chartId) {
        const chartContainer = document.getElementById(chartId);
        const messageElement = document.createElement('p');
        messageElement.textContent = 'No data available';
        chartContainer.innerHTML = ''; // Clear any existing content
        chartContainer.appendChild(messageElement);
    }

    // REList
    let chart2Ctx = document.getElementById('doughnut-chart-2').getContext('2d');
    $.ajax({
        url: `${host}/api/dashboard/REList`,
        type: "GET",
        dataType: "JSON",
    })
        .done((response) => {
            if (response.status === 'success') {
                if(Object.keys(response.RECounts).length > 0){
                    createDoughnutChart(chart2Ctx, response.RECounts, 'RE Chart');
                }else{
                    document.getElementById('no-data-message2').style.display = 'block';
                }
            } else {
                console.error('Error:', response.message);
            }
        })
        .fail((err) => {
            console.error('AJAX request failed:', err);
        });

    // EEList
    let chart3Ctx = document.getElementById('doughnut-chart-3').getContext('2d');
    $.ajax({
        url: `${host}/api/dashboard/EEList`,
        type: "GET",
        dataType: "JSON",
    })
        .done((response) => {
            if (response.status === 'success') {
                if(Object.keys(response.EECounts).length > 0){
                    createDoughnutChart(chart3Ctx, response.EECounts, 'EE Chart');
                }else{
                    document.getElementById('no-data-message3').style.display = 'block';
                }
            } else {
                console.error('Error:', response.message);
            }
        })
        .fail((err) => {
            console.error('AJAX request failed:', err);
        });

    
});

$(function(){

  CompetencyChart();
  ScheduleList();
  HistoryList();
  //TrainingDetails();


});




  let ScheduleList = () => {
    userid = userID;
       let getList = `${host}/api/dashboard/trainingPartner/${userid}/TPTrainingSchedule`;

   
      $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
      })
        .done((res) => {
          //createTable(res.data);
          tableScheduleList(res.TrainingSchedule);
          console.log(res.TrainingSchedule);
        })
        .catch((err) => {});
    };


    let tableScheduleList = (data) => {
      let senarai = $("#TPSchedule").DataTable({
        data: data,
        //  order: [[4, "desc"]],
        columnDefs: [
          {
            targets: 0,
            data: null,
            searchable: false,
            orderable: false,
          },
          {
            targets: 1,
            data: "training_name",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
              }
              if (data !== null || data !== "") {
                return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
              }
            },
          },
          {
            targets: 2,
            data: "course_category",
            render: function (data, type, full, meta) {
              return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
            },
          },
          {
              targets: 3,
              data: "training_type",
              render: function (data, type, full, meta) {
                if (data && data !== '' && data !== null) {
                  return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                
                } else {
                  return '<td width="15%"><p class="poppins-semibold-14">-</p></td>';
                }
              },
            },
          {
            targets: 4,
            data: "date_start",
            render: function (data, type, full, meta) {
              if(data && data != '' && data != null) {
              
                return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
              } else {
                return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
              }
              
            },
          },

          {
              targets: 5,
              data: "date_end",
              render: function (data, type, full, meta) {
                if(data && data != '' && data != null) {
                
                  return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                } else {
                  return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
                }
                
              },
            },
         
          {
            targets: 6,
            data: "status",
            //visible: false,
            render: function (data, type, full, meta) {

               let link = "";
              // if (data == null || data === "") {
              //     return `<a href="view-exam-result" class="btn btn-sm">
              //                   <i class="icon-editIcon" style="font-size: 18px;"></i>
              //               </a>`;
              //   }
                if (data != null || data !== "") {
                  return  `<td width="15%"><p class="poppins-semibold-14">Active</p></td>`;
                }
              //return data;
            },
          },
        ],
        language: {
          info: "view _START_ to _END_ from _TOTAL_ entry",
          infoEmpty: "view 0 from 0 to 0 entry",
          lengthMenu: "view _MENU_ Entry",
          paginate: {
            next: "Next",
            previous: "Previous",
          },
          zeroRecords: "No record found",
        },
      });
    
      senarai
        .on("order.dt search.dt", function () {
          senarai
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
              cell.innerHTML = i + 1;
            });
        })
        .draw();
    
      $("#filterStatus").on("change", (e) => {
        // console.log(e.target.value)
        senarai.column(5).search(e.target.value).draw();
      });
    
      $("#filterJenisKenderaan").on("change", (e) => {
        console.log(e.target.value);
        senarai.column(1).search(e.target.value).draw();
      });
    
      $("#search_input").on("keyup", () => {
        let searched = $("#search_input").val();
        // console.log(searched)
        senarai.search(searched).draw();
      });
    
      $("#resetFilter").on("click", () => {
        $("#filterStatus").val("").trigger("change");
        $("#filterJenisKenderaan").val("").trigger("change");
        $("#search_input").val("");
    
        senarai.search("").columns().search("").draw();
      });
    };
    

    //////History////////////////////
    let HistoryList = () => {
      userid = userID;

      let getList = `${host}/api/dashboard/trainingPartner/${userid}/TPTrainingHistory`;
     
     $.ajax({
       url: getList,
       type: "GET",
       dataType: "JSON",
     })
       .done((res) => {
         //createTable(res.data);
         tableHistoryList(res.TrainingHistory);
         console.log(res.TrainingHistory);
       })
       .catch((err) => {});
   };


   let tableHistoryList = (data) => {
     let senarai = $("#TPHistory").DataTable({
       data: data,
       //  order: [[4, "desc"]],
       columnDefs: [
         {
           targets: 0,
           data: null,
           searchable: false,
           orderable: false,
         },
         {
           targets: 1,
           data: "training_name",
           render: function (data, type, full, meta) {
             if (data === null) {
               return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
             }
             if (data !== null || data !== "") {
               return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
             }
           },
         },
         {
           targets: 2,
           data: "course_category",
           render: function (data, type, full, meta) {
             return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
           },
         },
         {
             targets: 3,
             data: "training_type",
             render: function (data, type, full, meta) {
               if (data && data !== '' && data !== null) {
                 return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
               
               } else {
                 return '<td width="15%"><p class="poppins-semibold-14">-</p></td>';
               }
             },
           },
           {
              targets: 4,
              data: "date_start",
              render: function (data, type, full, meta) {
                if(data && data != '' && data != null) {
                
                  return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                } else {
                  return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
                }
                
              },
            },

            {
                targets: 5,
                data: "date_end",
                render: function (data, type, full, meta) {
                  if(data && data != '' && data != null) {
                  
                    return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                  } else {
                    return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
                  }
                  
                },
              },
              {
                targets: 6,
                data: "status",
                render: function (data, type, full, meta) {
                  
                  
                    return  `<td width="15%"><p class="poppins-semibold-14">Complete</p></td>`;
                 
                  
                },
              },
           
            {
              targets:7,
              data: "id",
              //visible: false,
              render: function (data, type, full, meta) {

                 let link = "";
               
                  // if (data != null || data !== "") {

                 
            
                   return  `
                              <a href="${host}/dashboard/training-partner/attendance-list/${full.batch_no}/${data}" class="btn btn-sm">
                              <button class="btn btn-sm icon-style" data-id="${data}" 
                              <i class="icon-sunting" data-id="${data}">View Details</i>
                              </button></a>
                              
                             `;
                  // }
                 
                
              },
            },
       ],
       language: {
         info: "view _START_ to _END_ from _TOTAL_ entry",
         infoEmpty: "view 0 from 0 to 0 entry",
         lengthMenu: "view _MENU_ Entry",
         paginate: {
           next: "Next",
           previous: "Previous",
         },
         zeroRecords: "No record found",
       },
     });
   
     senarai
       .on("order.dt search.dt", function () {
         senarai
           .column(0, { search: "applied", order: "applied" })
           .nodes()
           .each(function (cell, i) {
             cell.innerHTML = i + 1;
           });
       })
       .draw();
   
     $("#filterStatus").on("change", (e) => {
       // console.log(e.target.value)
       senarai.column(5).search(e.target.value).draw();
     });
   
     $("#filterJenisKenderaan").on("change", (e) => {
       console.log(e.target.value);
       senarai.column(1).search(e.target.value).draw();
     });
   
     $("#search_input").on("keyup", () => {
       let searched = $("#search_input").val();
       // console.log(searched)
       senarai.search(searched).draw();
     });
   
     $("#resetFilter").on("click", () => {
       $("#filterStatus").val("").trigger("change");
       $("#filterJenisKenderaan").val("").trigger("change");
       $("#search_input").val("");
   
       senarai.search("").columns().search("").draw();
     });
   };

   // Assume you have a canvas element with id 'competency-chart'
   const competencyChartCanvas = document.getElementById('competency-chart').getContext('2d');

   let CompetencyChart = () => {
     let getChartData = `${host}/api/dashboard/CompetencyChart`;

     $.ajax({
         url: getChartData,
         type: 'GET',
         dataType: 'JSON',
     })
     .done((response) => {
         if (response.status === 'success') {
             console.log('Competency List:', response.TrainingCount);
             if(Object.keys(response.TrainingCount).length > 0){
                 // Store training data globally
                 renderCompetencyChart(response.TrainingCount);
             }else{
                 document.getElementById('no-data-message4').style.display = 'block';
             }
         } else {
             console.error('Error:', response.message);
         }
     })
     .fail((err) => {
         console.error('AJAX request failed:', err);
     });
 // };

const renderCompetencyChart = (trainingCountData) => {
   const groupedData = groupByYearAndCompetency(trainingCountData);
   const years = Object.keys(groupedData).sort();
   const competencies = Array.from(new Set(trainingCountData.map(training => training.course_name)));

   const datasets = competencies.map((competency, index) => {
       return {
           label: competency,
           backgroundColor: getRandomColor(),
           data: years.map(year => groupedData[year][competency] || 0),
           stack: 'Stack 0', // Use the same stack for all competencies
       };
   });

   const chartData = {
       labels: years,
       datasets: datasets,
   };

   const competencyChart = new Chart(competencyChartCanvas, {
       type: 'bar',
       data: chartData,
       options: {
           scales: {
               x: {
                   stacked: true,
               },
               y: {
                   stacked: true,
               },
           },
       },
   });

   // Create DataTable
   const tableData = competencies.map(competency => {
       const rowData = [competency];
       years.forEach(year => {
           rowData.push(groupedData[year][competency] || 0);
       });
       return rowData;
   });

   // Add headers for DataTable
   const tableHeaders = ['Training Course'].concat(years);

   var $j = jQuery.noConflict();
   $j(document).ready(function () {
       // DataTable initialization
       $('#dataTable').DataTable({
           data: tableData,
           columns: tableHeaders.map(header => ({ title: header })),
       });
   });
};

function groupByYearAndCompetency(trainingCountData) {
   const groupedData = {};

   trainingCountData.forEach(training => {
       const year = moment(training.date_end, 'DD/MM/YYYY').year();
       const competency = training.course_name;

       if (!groupedData[year]) {
           groupedData[year] = {};
       }

       // Ensure all competencies exist for the current year
       if (!groupedData[year][competency]) {
           groupedData[year][competency] = 0;
       }

       groupedData[year][competency]++;
   });

   return groupedData;
};

// Function to generate random colors
function getRandomColor() {
   const letters = '0123456789ABCDEF';
   let color = '#';
   for (let i = 0; i < 6; i++) {
       color += letters[Math.floor(Math.random() * 16)];
   }
   return color;
}
}




   

