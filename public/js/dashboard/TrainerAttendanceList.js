
$(function(){

    ExamList();
    //TrainingDetails();


});


// let TrainingDetails = () => {


//   ID =   $(this).data("id");

//   let getList = `${host}/api/exam/${ID}/examDetails`;

  
//  $.ajax({
//    url: getList,
//    type: "GET",
//    dataType: "JSON",
//  })
//    .done((res) => {
//      //createTable(res.data);
//      data = res.trainingDetails;
    
//      (value.training_name == null || value.training_name == '') ? $('#provide_name').text('n/a') : $('#provide_name').text(value.training_name); 
//      (value.date_start == null || value.date_start == '') ? $('#date_start').text('n/a') : $('#date_start').text(value.date_start);
//      (value.date_end == null || value.date_end == '') ? $('#date_end').text('n/a') : $('#date_end').text(value.date_end);
//      (value.venue == null || value.venue == '') ? $('#venue').text('n/a') : $('#venue').text(value.venue);

//    })
//    .catch((err) => {});
// };


    let ExamList = () => {

         let getList = `${host}/api/dashboard/trainer/TrainerAttendanceList`;
     
        $.ajax({
          url: getList,
          type: "GET",
          dataType: "JSON",
        })
          .done((res) => {
            //createTable(res.data);
            tableExamList(res.examlist);
            console.log(res.examlist);
          })
          .catch((err) => {});
      };
 

      let tableExamList = (data) => {
        let senarai = $("#TPattendanceList").DataTable({
          data: data,
          //  order: [[4, "desc"]],
          columnDefs: [
            {
              targets: 0,
              data: null,
              searchable: false,
              orderable: false,
            },
            {
              targets: 1,
              data: "fullname",
              render: function (data, type, full, meta) {
                if (data === null) {
                  return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
                }
                if (data !== null || data !== "") {
                  return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
                }
              },
            },
            {
              targets: 2,
              data: "attend_check",
              render: function (data, type, full, meta) {

                if (data === 1) {
                  return `<td width="40%" align="left"><p class="poppins-semibold-14"><input type="checkbox" name="attendCheck[]" value="${data}" checked></p></td>`;
                }
                if (data == null || data == "") {
                  return `<td align="left" width="40%"><p class="poppins-semibold-14"><input type="checkbox" name="attendCheck[]" value=""></p></td>`;
                }
              },
            },
            {
                targets: 3,
                data: "date_attend",
                render: function (data, type, full, meta) {
                  if (data && data !== '' && data !== null) {
                    return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                  
                  } else {
                    return '<td width="15%"><p class="poppins-semibold-14">-</p></td>';
                  }
                },
              },
            {
              targets: 4,
              data: "remark",
              render: function (data, type, full, meta) {
                if(data && data != '' && data != null) {
                
                  return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                } else {
                  return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
                }
                
              },
            },
           
            // {
            //   targets: 5,
            //   data: "id",
            //   //visible: false,
            //   render: function (data, type, full, meta) {

            //      let link = "";
            //     // if (data == null || data === "") {
            //     //     return `<a href="view-exam-result" class="btn btn-sm">
            //     //                   <i class="icon-editIcon" style="font-size: 18px;"></i>
            //     //               </a>`;
            //     //   }
            //       if (data != null || data !== "") {
            
            //        return  `

            //                   <a href="${host}/exam/updateExamResult/${data}" class="btn btn-sm">
            //                   <button class="btn btn-sm icon-style" data-id="${data}" 
            //                   <i class="icon-sunting" data-id="${data}">View Details User</i>
            //                   </button></a>
                              
            //                  `;
            //       }
            //     //return data;
            //   },
            // },
          ],
          language: {
            info: "view _START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "view 0 from 0 to 0 entry",
            lengthMenu: "view _MENU_ Entry",
            paginate: {
              next: "Next",
              previous: "Previous",
            },
            zeroRecords: "No record found",
          },
        });
      
        senarai
          .on("order.dt search.dt", function () {
            senarai
              .column(0, { search: "applied", order: "applied" })
              .nodes()
              .each(function (cell, i) {
                cell.innerHTML = i + 1;
              });
          })
          .draw();
      
        $("#filterStatus").on("change", (e) => {
          // console.log(e.target.value)
          senarai.column(5).search(e.target.value).draw();
        });
      
        $("#filterJenisKenderaan").on("change", (e) => {
          console.log(e.target.value);
          senarai.column(1).search(e.target.value).draw();
        });
      
        $("#search_input").on("keyup", () => {
          let searched = $("#search_input").val();
          // console.log(searched)
          senarai.search(searched).draw();
        });
      
        $("#resetFilter").on("click", () => {
          $("#filterStatus").val("").trigger("change");
          $("#filterJenisKenderaan").val("").trigger("change");
          $("#search_input").val("");
      
          senarai.search("").columns().search("").draw();
        });
      };
      

      //////
