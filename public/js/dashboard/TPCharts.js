document.addEventListener("DOMContentLoaded", function () {
    // Function to create a doughnut chart
    const createDoughnutChart = (ctx, data, label) => {
        console.log('Chart Data:', data);
        return new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: Object.keys(data),
                datasets: [{
                    data: Object.values(data),
                    backgroundColor: [
                        'rgba(115, 192, 222, 1)',
                        'rgba(83, 112, 198, 1)',
                        'rgba(250, 199, 87, 1)',
                        'rgba(238, 102, 102, 1)',
                        'rgba(146, 204, 118, 1)',
                        'rgba(128, 0, 0, 1)',
                    ],
                }],
            },
            options: {
                responsive: true,
                legend: {
                    display: false,
                },
                title: {
                    display: true,
                    text: label,
                },
            },
        });
    };

    // Assuming you have the API responses available as SECounts, RECounts, and EECounts
    // SEList
    let chart1Ctx = document.getElementById('doughnut-chart-1').getContext('2d');
    $.ajax({
        url: `${host}/api/dashboard/SEList`,
        type: "GET",
        dataType: "JSON",
    })
        .done((response) => {
            if (response.status === 'success') {
                createDoughnutChart(chart1Ctx, response.SECounts, 'SE Chart');
            } else {
                console.error('Error:', response.message);
            }
        })
        .fail((err) => {
            console.error('AJAX request failed:', err);
        });

    // REList
    let chart2Ctx = document.getElementById('doughnut-chart-2').getContext('2d');
    $.ajax({
        url: `${host}/api/dashboard/REList`,
        type: "GET",
        dataType: "JSON",
    })
        .done((response) => {
            if (response.status === 'success') {
                createDoughnutChart(chart2Ctx, response.RECounts, 'RE Chart');
            } else {
                console.error('Error:', response.message);
            }
        })
        .fail((err) => {
            console.error('AJAX request failed:', err);
        });

    // EEList
    let chart3Ctx = document.getElementById('doughnut-chart-3').getContext('2d');
    $.ajax({
        url: `${host}/api/dashboard/EEList`,
        type: "GET",
        dataType: "JSON",
    })
        .done((response) => {
            if (response.status === 'success') {
                createDoughnutChart(chart3Ctx, response.EECounts, 'EE Chart');
            } else {
                console.error('Error:', response.message);
            }
        })
        .fail((err) => {
            console.error('AJAX request failed:', err);
        });
});

var dataTableInstance;

/* All Training List Section */
$(function(){
    TPList();
    SEList();
    REList();
    EEList();
    TrainingCards();
    CompetencyChart();
});

let TPList = () => {
    ID =   userID;

    let getList = `${host}/api/dashboard/TPList/${ID}`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        if (response.status === 'success') {
            console.log($);              // Check if jQuery is loaded
            console.log($.fn.DataTable);  // Check if DataTables is loaded
            initializeDataTable(response.TPList);
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let initializeDataTable = (data) => {
    console.log('Table Data from reponse: ', data);
    var $j = jQuery.noConflict();
    $j(document).ready(function () {
        dataTableInstance = $("#TPList").DataTable({
            data: data,
            responsive: true,
            columns: [
                {
                    data: null,
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return meta.row + 1;
                    }
                },
                {
                    data: "programme_name",
                    render: function (data, type, full, meta) {
                        return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                    }
                },
                {
                    data: "date_start",
                    render: function (data, type, full, meta) {
                        if (data) {
                            return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                        } else {
                            return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                        }
                    }
                },
                {
                    data: "date_end",
                    render: function (data, type, full, meta) {
                        if (data) {
                            return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                        } else {
                            return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                        }
                    }
                },
                {
                    data: "venue",
                    render: function (data, type, full, meta) {
                        return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                    }
                },
                {
                    data: "fee",
                    render: function (data, type, full, meta) {
                        return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                    }
                },
                {
                    data: "status_id",
                    render: function (data, type, full, meta) {
                        if (full.status_name) {
                            return `<td width="50%" align="center"><p class="table-content">${full.status_name}</p></td>`;
                        } else {
                            return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                        }
                    }
                },
            ],
            language: {
                info: "_START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "0 from 0 to 0 entry",
                lengthMenu: " _MENU_ Entry",
                paginate: {
                    next: "Next",
                    previous: "Previous",
                },
                zeroRecords: "No record found",
            },
            initComplete: function () {
                this.api().columns().every(function () {
                    let column = this;
                    $('input', column.header()[1]).on('keyup change', function () {
                        if (column.search() !== this.value) {
                            column
                                .search(this.value)
                                .draw();
                        }
                    });
                });
            },
        });
    });

    dataTableInstance.on("order.dt search.dt", function () {
        dataTableInstance
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
    }).draw();
};

let SEList = () => {
    let getList = `${host}/api/dashboard/SEList`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        if (response.status === 'success') {
            console.log('SE List : ', response.SECounts);
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let REList = () => {
    let getList = `${host}/api/dashboard/REList`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        if (response.status === 'success') {
            console.log('RE List : ', response.RECounts);
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let EEList = () => {
    let getList = `${host}/api/dashboard/EEList`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        if (response.status === 'success') {
            console.log('EE List : ', response.EECounts);
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let TrainingCards = () => {
    let getList = `${host}/api/dashboard/TrainingCards`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        if (response.status === 'success') {
            console.log('TrainingCards List : ', response.TrainingCards);
            renderTrainingCards(response.TrainingCards);
            setupCarouselNavigation(response.TrainingCards);
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

const renderTrainingCards = (trainingCardsData) => {
    const createTrainingCardElement = (training) => {
        return `
            <div class="training-cards col-md-3" style="border: 1px solid; margin-left: 3%; margin-right: 3%; max-height: 1100px; overflow: hidden;">
                <img src="/storage/${training.training_image}" style="width: 100%; height: 50%; margin-top: 2%;">
                <label class="card-title">${training.course_name}</label>
                <div class="mx-auto" style="display: flex; flex-direction: row;">
                    <i class="fas fa-map-marker mx-2" style="color: #3498DB;"></i>
                    <label class="card-location">${training.training_venue}</label>
                </div>
                <div style="justify-content: start; text-align: start; margin: 5%; margin-bottom: 10%;">
                    <label class="unconfirmed-details">
                        <i class="fas fa-calendar-alt"><label>${training.date_start}</label></i>
                    </label>
                    <label class="unconfirmed-details">
                        RM ${training.training_fee !== null ? training.training_fee : '-'}
                    </label>
                </div>
            </div>`;
    };
    

    const container = document.getElementById('training-cards-container');
    container.innerHTML = '';

    for (let i = 0; i < trainingCardsData.length; i++) {
        const cardElement = createTrainingCardElement(trainingCardsData[i]);
        container.innerHTML += cardElement;
    }
};

const setupCarouselNavigation = (trainingCardsData) => {
    const container = document.querySelector('.carousel-container');
    const cardsContainer = document.getElementById('training-cards-container');
    const cards = cardsContainer.getElementsByClassName('training-cards');
    let currentIndex = 0;

    const showCards = () => {
        for (let i = 0; i < cards.length; i++) {
            cards[i].style.display = 'none';
        }

        for (let i = currentIndex; i < currentIndex + 3; i++) {
            if (cards[i]) {
                cards[i].style.display = 'block';
            }
        }

        // Disable/enable buttons based on currentIndex
        const leftButton = container.querySelector('#carousel-button-left');
        const rightButton = container.querySelector('#carousel-button-right');

        leftButton.disabled = currentIndex === 0;
        rightButton.disabled = currentIndex >= trainingCardsData.length - 3;
    };

    const navigateCarousel = (direction) => {
        if (direction === 'left') {
            currentIndex = Math.max(currentIndex - 3, 0);
        } else if (direction === 'right') {
            currentIndex = Math.min(currentIndex + 3, trainingCardsData.length - 3);
        }

        showCards();
    };

    const leftButton = container.querySelector('#carousel-button-left');
    const rightButton = container.querySelector('#carousel-button-right');

    leftButton.addEventListener('click', () => navigateCarousel('left'));
    rightButton.addEventListener('click', () => navigateCarousel('right'));

    showCards();
};

// Assume you have a canvas element with id 'competency-chart'
const competencyChartCanvas = document.getElementById('competency-chart').getContext('2d');

let CompetencyChart = () => {
    let getChartData = `${host}/api/dashboard/CompetencyChart`;

    $.ajax({
        url: getChartData,
        type: 'GET',
        dataType: 'JSON',
    })
    .done((response) => {
        if (response.status === 'success') {
            console.log('Competency List:', response.TrainingCount);
            renderCompetencyChart(response.TrainingCount);
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

const renderCompetencyChart = (trainingCountData) => {
    const groupedData = groupByYearAndCompetency(trainingCountData);
    const years = Object.keys(groupedData).sort();
    const competencies = Array.from(new Set(trainingCountData.map(training => training.course_name)));

    const datasets = competencies.map((competency, index) => {
        return {
            label: competency,
            backgroundColor: getRandomColor(),
            data: years.map(year => groupedData[year][competency] || 0),
            stack: 'Stack 0', // Use the same stack for all competencies
        };
    });

    const chartData = {
        labels: years,
        datasets: datasets,
    };

    const competencyChart = new Chart(competencyChartCanvas, {
        type: 'bar',
        data: chartData,
        options: {
            scales: {
                x: {
                    stacked: true,
                },
                y: {
                    stacked: true,
                },
            },
        },
    });

    // Create DataTable
    const tableData = competencies.map(competency => {
        const rowData = [competency];
        years.forEach(year => {
            rowData.push(groupedData[year][competency] || 0);
        });
        return rowData;
    });

    // Add headers for DataTable
    const tableHeaders = ['Training Course'].concat(years);

    var $j = jQuery.noConflict();
    $j(document).ready(function () {
        // DataTable initialization
        $('#dataTable').DataTable({
            data: tableData,
            columns: tableHeaders.map(header => ({ title: header })),
        });
    });
};

function groupByYearAndCompetency(trainingCountData) {
    const groupedData = {};

    trainingCountData.forEach(training => {
        const year = moment(training.date_end, 'DD/MM/YYYY').year();
        const competency = training.course_name;

        if (!groupedData[year]) {
            groupedData[year] = {};
        }

        // Ensure all competencies exist for the current year
        if (!groupedData[year][competency]) {
            groupedData[year][competency] = 0;
        }

        groupedData[year][competency]++;
    });

    return groupedData;
};

// Function to generate random colors
function getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
};

