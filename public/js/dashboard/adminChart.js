document.addEventListener("DOMContentLoaded", function () {
    // Function to create a doughnut chart
    const createDoughnutChart = (ctx, data, label) => {
        console.log('Chart Data:', data);
        return new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: Object.keys(data),
                datasets: [{
                    data: Object.values(data),
                    backgroundColor: [
                        'rgba(115, 192, 222, 1)',
                        'rgba(83, 112, 198, 1)',
                        'rgba(250, 199, 87, 1)',
                        'rgba(238, 102, 102, 1)',
                        'rgba(146, 204, 118, 1)',
                        'rgba(128, 0, 0, 1)',
                    ],
                }],
            },
            options: {
                responsive: true,
                legend: {
                    display: false,
                },
                title: {
                    display: true,
                    text: label,
                },
            },
        });
    };

    // Assuming you have the API responses available as SECounts, RECounts, and EECounts
    // SEList
    let chart1Ctx = document.getElementById('doughnut-chart-1').getContext('2d');
    $.ajax({
        url: `${host}/api/dashboard/SEList`,
        type: "GET",
        dataType: "JSON",
    })
        .done((response) => {
            if (response.status === 'success') {
                if (Object.keys(response.SECounts).length > 0) {
                    createDoughnutChart(chart1Ctx, response.SECounts, 'SE Chart');
                } else {
                    // displayNoDataMessage('doughnut-chart-1');
                    document.getElementById('no-data-message').style.display = 'block';
                }
            } else {
                console.error('Error:', response.message);
            }
        })
        .fail((err) => {
            console.error('AJAX request failed:', err);
        });
    
    // Function to display a message when no data is available
    function displayNoDataMessage(chartId) {
        const chartContainer = document.getElementById(chartId);
        const messageElement = document.createElement('p');
        messageElement.textContent = 'No data available';
        chartContainer.innerHTML = ''; // Clear any existing content
        chartContainer.appendChild(messageElement);
    }

    // REList
    let chart2Ctx = document.getElementById('doughnut-chart-2').getContext('2d');
    $.ajax({
        url: `${host}/api/dashboard/REList`,
        type: "GET",
        dataType: "JSON",
    })
        .done((response) => {
            if (response.status === 'success') {
                if(Object.keys(response.RECounts).length > 0){
                    createDoughnutChart(chart2Ctx, response.RECounts, 'RE Chart');
                }else{
                    document.getElementById('no-data-message2').style.display = 'block';
                }
            } else {
                console.error('Error:', response.message);
            }
        })
        .fail((err) => {
            console.error('AJAX request failed:', err);
        });

    // EEList
    let chart3Ctx = document.getElementById('doughnut-chart-3').getContext('2d');
    $.ajax({
        url: `${host}/api/dashboard/EEList`,
        type: "GET",
        dataType: "JSON",
    })
        .done((response) => {
            if (response.status === 'success') {
                if(Object.keys(response.EECounts).length > 0){
                    createDoughnutChart(chart3Ctx, response.EECounts, 'EE Chart');
                }else{
                    document.getElementById('no-data-message3').style.display = 'block';
                }
            } else {
                console.error('Error:', response.message);
            }
        })
        .fail((err) => {
            console.error('AJAX request failed:', err);
        });
});

// Assume you have a canvas element with id 'competency-chart'
const competencyChartCanvas = document.getElementById('competency-chart').getContext('2d');

let CompetencyChart = () => {
    let getChartData = `${host}/api/dashboard/CompetencyChart`;

    $.ajax({
        url: getChartData,
        type: 'GET',
        dataType: 'JSON',
    })
    .done((response) => {
        if (response.status === 'success') {
            console.log('Competency List:', response.TrainingCount);
            if(Object.keys(response.TrainingCount).length > 0){
                // Store training data globally
                renderCompetencyChart(response.TrainingCount);
            }else{
                document.getElementById('no-data-message4').style.display = 'block';
            }
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

const renderCompetencyChart = (trainingCountData) => {
    const groupedData = groupByYearAndCompetency(trainingCountData);
    const years = Object.keys(groupedData).sort();
    const competencies = Array.from(new Set(trainingCountData.map(training => training.course_name)));

    const datasets = competencies.map((competency, index) => {
        return {
            label: competency,
            backgroundColor: getRandomColor(),
            data: years.map(year => groupedData[year][competency] || 0),
            stack: 'Stack 0', // Use the same stack for all competencies
        };
    });

    const chartData = {
        labels: years,
        datasets: datasets,
    };

    const competencyChart = new Chart(competencyChartCanvas, {
        type: 'bar',
        data: chartData,
        options: {
            scales: {
                x: {
                    stacked: true,
                },
                y: {
                    stacked: true,
                },
            },
        },
    });

    // Create DataTable
    const tableData = competencies.map(competency => {
        const rowData = [competency];
        years.forEach(year => {
            rowData.push(groupedData[year][competency] || 0);
        });
        return rowData;
    });

    // Add headers for DataTable
    const tableHeaders = ['Training Course'].concat(years);

    var $j = jQuery.noConflict();
    $j(document).ready(function () {
        // DataTable initialization
        $('#dataTable').DataTable({
            data: tableData,
            columns: tableHeaders.map(header => ({ title: header })),
        });
    });
};

// function populateYearDropdown(years, selectedYear) {
//     const dropdown = document.getElementById('minYear');

//     // Clear existing options
//     dropdown.innerHTML = '';

//     // Add default option
//     const defaultOption = document.createElement('option');
//     defaultOption.value = 'all';
//     defaultOption.text = 'All Years';
//     dropdown.appendChild(defaultOption);

//     // Add options for each year
//     years.forEach(year => {
//         const option = document.createElement('option');
//         option.value = year;
//         option.text = year;
//         dropdown.appendChild(option);
//     });

//     // Set the selected option based on the provided year
//     dropdown.value = selectedYear;
// }

// function updateChart() {
//     const currentYear = new Date().getFullYear();
//     const minYear = document.getElementById('minYear').value;
//     var year;

//     if (competencyChart) {
//         competencyChart.destroy();
//     }

//     // Filter the data based on the selected year range
//     const filteredData = trainingCountData.filter(training => {
//         year = moment(training.date_end, 'DD/MM/YYYY').year();
//         return minYear === 'all' || (year >= parseInt(minYear) && year <= currentYear);
//     });

//     $('#dataTable').DataTable().destroy();
    
//     if(Object.keys(filteredData).length > 0){
//         // Render the updated chart
//         renderCompetencyChart(filteredData);
//     }else{
//         document.getElementById('no-data-message4').style.display = 'block';
//     }
// }

// function groupByYearAndCompetency(trainingCountData) {
//     const groupedData = {};

//     trainingCountData.forEach(training => {
//         const year = moment(training.date_end, 'DD/MM/YYYY').year();
//         const competency = training.course_name;

//         if (!groupedData[year]) {
//             groupedData[year] = {};
//         }

//         // Ensure all competencies exist for the current year
//         if (!groupedData[year][competency]) {
//             groupedData[year][competency] = 0;
//         }

//         groupedData[year][competency]++;
//     });

//     return groupedData;
// }

function groupByYearAndCompetency(trainingCountData) {
    const groupedData = {};

    trainingCountData.forEach(training => {
        const year = moment(training.date_end, 'DD/MM/YYYY').year();
        const competency = training.course_name;

        if (!groupedData[year]) {
            groupedData[year] = {};
        }

        // Ensure all competencies exist for the current year
        if (!groupedData[year][competency]) {
            groupedData[year][competency] = 0;
        }

        groupedData[year][competency]++;
    });

    return groupedData;
};

// Function to generate random colors
function getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

var dataTableInstance;

/* All Training List Section */
$(function(){
    CompetencyChart();
    getListCourse();
    getListState();
    AllList();
    AllTrainingList();
    SEList();
    REList();
    EEList();
    
    TrainingCards();
    
});


////////////
let getListCourse = () => {
    let getCourse = `${host}/api/dashboard/searchCourse`;
    $.ajax({
        type: 'GET',
        url: getCourse,
       
      })
      .done(res => {
        console.log('lklkl',res.trainingCourse);
        $.each(res.trainingCourse, (index, value) => {
          $('#course_filter').append(`<option value="${value.course_name}">${value.course_name}</option>`);
        });

        $.each(res.trainingVenue, (index, value) => {
            $('#venue_filter').append(`<option value="${value.venue}">${value.venue}</option>`);
          });
  
      }).catch(err => {
        console.log(err);
      });
  }

  let getListState = () => {
    let getRegion = `${host}/api/dashboard/searchState`;
    $.ajax({
        type: 'GET',
        url: getRegion,
       
      })
      .done(res => {
        console.log('lklkl',res.trainingRegion);

        $.each(res.trainingState, (index, value) => {
          $('#state_filter').append(`<option value="${value.state_name}">${value.state_name}</option>`);
        });

        $.each(res.trainingDate, (index, value) => {
            $('#startDate_filter').append(`<option value="${value.date_start}">${value.date_start}</option>`);
          });

          $.each(res.trainingDate, (index, value) => {
            $('#endDate_filter').append(`<option value="${value.date_end}">${value.date_end}</option>`);
          });
  
      }).catch(err => {
        console.log(err);
      });
  }


///////////
let AllList = () => {
    let getList = `${host}/api/dashboard/AllTrainingList`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        console.log('API Response:', response);
        initializeDataTable(response.AllTrainingList);
            console.log('Training List:', response.AllTrainingList);
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let initializeDataTable = (data) => {
    let dataTable = $("#AllList").DataTable({
        data: data,
        responsive: true,
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                data: "programme_name",
                searchable: true,
                targets: [1],
                render: function (data, type, full, meta) {
                    let editLink = "/admin/approve-training-registration/" + full.id;
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content hover-link"> <a href="${editLink}" class="btn btn-sm hover-link">
                           ${data}
                        </a></p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">-</div></td>';
                    }
                }
            },
            {
                data: "course_category",
                render: function (data, type, full, meta) {
                    return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                }
            },
            {
                data: "type_name",
                render: function (data, type, full, meta) {
                    return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                }
            },
            {
                data: "batch_no_full",
                render: function (data, type, full, meta) {
                    return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                }
            },
            {
                data: "date_start",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "date_end",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "id_approval_status",
                render: function (data, type, full, meta) {
                    if (data === 1) {
                        return `<td width="50%" align="center"><p class="null-value">Pending Approval</p></td>`;
                    } else if (data === 2){
                        return `<td width="50%" align="center"><p class="active-status">Approved</p></td>`;
                    } 
                    else if (data === 3){
                        return `<td width="50%" align="center"><p class="failed-status">Rejected</p></td>`;
                    } 
                    else if (data === 4){
                        return `<td width="50%" align="left"><p class="reserved-status">Draft</p></td>`;
                    } 
                    else {
                        return `<td width="50%" align="center">-</td>`;
                    }
                }
            }
        ],
        lengthMenu: [5, 10, 15, 20],
        pageLength: 5,
        language: {
            info: "_START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
                next: "Next",
                previous: "Previous",
            },
            zeroRecords: "No record found",
        },
    });
};


let SEList = () => {
    let getList = `${host}/api/dashboard/SEList`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        if (response.status === 'success') {
            console.log('SE List : ', response.SECounts);
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
}

let REList = () => {
    let getList = `${host}/api/dashboard/REList`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        if (response.status === 'success') {
            console.log('RE List : ', response.RECounts);
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let EEList = () => {
    let getList = `${host}/api/dashboard/EEList`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        if (response.status === 'success') {
            console.log('EE List : ', response.EECounts);
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let TrainingCards = () => {
    let getList = `${host}/api/dashboard/TrainingCards`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        if (response.status === 'success') {
            console.log('TrainingCards List : ', response.TrainingCards);
            renderTrainingCards(response.TrainingCards);
            setupCarouselNavigation(response.TrainingCards);
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

// Function to handle image loading error
// function imageError(img) {
//     img.onerror = null;  // Remove the onerror attribute to prevent infinite loop
//     img.src = '';  // Clear the src attribute
//     img.alt = 'Image not available';  // Set alt attribute to display alternative text
// }
function handleImageError(img) {
    img.style.display = 'none'; // Hide the image
    document.getElementById('imageErrorMessage').style.display = 'block'; // Show the error message
}

const renderTrainingCards = (trainingCardsData) => {
    const createTrainingCardElement = (training) => {
        return `
        <div class="training-cards col-md-3">
    <div class="image-container">
        <img src="/storage/${training.training_image}" onerror="handleImageError(this)">
        <p id="imageErrorMessage">Image not available</p>
    </div>

    <label class="card-title">${training.course_name}</label>
    <div class="mx-auto" style="display: flex; flex-direction: row;">
        <i class="fas fa-map-marker mx-2" style="color: #3498DB;"></i>
        <label class="card-location">${training.training_venue}</label>
    </div>
    <div style="justify-content: start; text-align: start; margin: 5%; margin-bottom: 10%;">
        <label class="unconfirmed-details">
            <i class="fas fa-calendar-alt"><label>${training.date_start}</label></i>
        </label>
        <label class="unconfirmed-details">
            RM ${training.training_fee !== null ? training.training_fee : '-'}
        </label>
    </div>
</div>
`;
    };

    const container = document.getElementById('training-cards-container');
    container.innerHTML = '';

    for (let i = 0; i < trainingCardsData.length; i++) {
        const cardElement = createTrainingCardElement(trainingCardsData[i]);
        container.innerHTML += cardElement;
    }
};

const setupCarouselNavigation = (trainingCardsData) => {
    const container = document.querySelector('.carousel-container');
    const cardsContainer = document.getElementById('training-cards-container');
    const cards = cardsContainer.getElementsByClassName('training-cards');
    let currentIndex = 0;

    const showCards = () => {
        for (let i = 0; i < cards.length; i++) {
            cards[i].style.display = 'none';
        }

        for (let i = currentIndex; i < currentIndex + 3; i++) {
            if (cards[i]) {
                cards[i].style.display = 'block';
            }
        }

        // Disable/enable buttons based on currentIndex
        const leftButton = container.querySelector('#carousel-button-left');
        const rightButton = container.querySelector('#carousel-button-right');

        leftButton.disabled = currentIndex === 0;
        rightButton.disabled = currentIndex >= trainingCardsData.length - 3;
    };

    const navigateCarousel = (direction) => {
        if (direction === 'left') {
            currentIndex = Math.max(currentIndex - 3, 0);
        } else if (direction === 'right') {
            currentIndex = Math.min(currentIndex + 3, trainingCardsData.length - 3);
        }

        showCards();
    };

    const leftButton = container.querySelector('#carousel-button-left');
    const rightButton = container.querySelector('#carousel-button-right');

    leftButton.addEventListener('click', () => navigateCarousel('left'));
    rightButton.addEventListener('click', () => navigateCarousel('right'));

    showCards();
};

