
$(function(){

    userList();


});


    let userList = () => {

         let getList = `${host}/api/user/userManagementList`;
        $.ajax({
          url: getList,
          type: "GET",
          dataType: "JSON",
        })
          .done((res) => {
            //createTable(res.data);
            tableUserList(res.userlist);
          })
          .catch((err) => {});
      };

      let tableUserList = (data) => {
        let senarai = $("#userManagementList").DataTable({
          data: data,
          dom: 'lfrtip', 
        ordering: false, 
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                $('input', column.header()).on('keyup change clear', function () {
                    if (column.search() !== this.value) {
                        // Use case-insensitive search for the "Status" column (index 5)
                        column.search(this.value, false, true).draw();
                    }
                });
            });
          },
          //  order: [[4, "desc"]],
          columnDefs: [
            {
              targets: 0,
              className: 'text-left', 
              data: null,
              searchable: false,
              orderable: false,
            },
            {
              targets: 1,
              className: 'text-left', 
              data: "id",
              render: function (data, type, full, meta) {
                if (data === null) {
                  return `<td width="50%"  align="center"><p class="table-content">-</p></td>`;
                }
                if (data !== null && data !== "") {
                  return `<td align="center" width="50%">
                            <a href="/admin/view-registered/${data}">
                              <p class="table-content" style="font-size: normal; color: #3498DB;">${full.fullname ?? full.companyname}</p>
                            </a>
                          </td>`;
                }
              },
            },
            {
              targets: 2,
              className: 'number-left', 
              data: "ic_no",
              render: function (data, type, full, meta) {
                if (data === null) {
                  return `<td width="50%" align="center"><p class="table-content">-</p></td>`;
                }
                if (data !== null || data !== "") {
                  return `<td align="left" width="50%"><p class="table-content">${data}</p></td>`;
                }
              },
            },
            {
                targets: 3,
                className: 'text-left', 
                data: "email",
                render: function (data, type, full, meta) {
                  if (data === null) {
                    return `<td width="50%" align="center"><p class="table-content">-</p></td>`;
                  }
                  if (data !== null || data !== "") {
                    return `<td align="left" width="50%"><p class="table-content">${data}</p></td>`;
                  }
                },
              },
              {
                targets: 4,
                className: 'text-left', 
                data: "companyname",
                render: function (data, type, full, meta) {
                  if (data === null) {
                    return `<td width="40%" align="center"><p class="table-content">-</p></td>`;
                  }
                  if (data !== null || data !== "") {
                    return `<td align="center" width="40%"><p class="table-content">${data}</p></td>`;
                  }
                },
              },
              {
                targets: 5,
                className: 'text-left', 
                data: "user_type",
                render: function (data, type, full, meta) {
                  if (data === 1) {
                    return `<div class="dark-turquoise-status table-content">Super Admin</div>`;
                  } else if (data === 2) {
                    return `<div class="dark-turquoise-status table-content">SEDA Admin</div>`;
                  }else if (data === 3) {
                    return `<div class="dark-turquoise-status table-content">Training Partner</div>`;
                  }else if (data === 4) {
                    return `<div class="dark-turquoise-status table-content">Trainer</div>`;
                  }else if (data === 5) {
                    return `<div class="dark-turquoise-status table-content">Participant</div>`;
                  }                  
                  else {
                    return "-";
                  }
                },
              },
              {
                targets: 6,
                data: "status",
                render: function (data, type, full, meta) {
                  if (data === null) {
                    return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
                  }
                  if (data !== null || data !== "") {
                    if (data === 1) {
                      return  `<td width="15%"><p class="active-status" style="text-align: center;">ACTIVE</p></td>`;
                    } else if (data === 0 || data ===null) {
                      return  `<td width="15%"><p class="inactive-status" style="text-align: center;">INACTIVE</p></td>`;
                    } else {
                      return "-";
                    }
                   
                  }
                },
              },
            {
              targets: 7,
              data: "id",
              render: function (data, type, full, meta) {
                // Assuming you have URLs for view, edit, and delete actions
                let editLink = "/admin/edit-registered/" + data;
                let deleteLink = "/admin/user-management/" + data;
              
                if (data !== null && data !== "") {
                  return `<div>
                            <a href="${editLink}" class="btn btn-sm">
                              <i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i>
                            </a>
                            <a href="${deleteLink}" class="btn btn-sm">
                              <i class="fas fa-trash-alt" style="font-size: 18px; color: #3498DB;"></i>
                            </a>
                          </div>`;
                } else {
                  return "-";
                }
              },
            }
          ],
          language: {
            info: " _START_ to _END_ from _TOTAL_ entry",
            infoEmpty: " 0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
              next: "Next",
              previous: "Previous",
            },
            zeroRecords: "No record found",
          },
        });

        var addUserButton = $('<button id="addUserButton" class="circular-button" style="color: white">' +
                            '<i class="fas fa-plus"></i>' +
                            '</button>');

        addUserButton.on('click', function () {
          window.location.href = "/admin/register-training-partner";
        });

        addUserButton.css({
          width: '37px',
          height: '37px',
          backgroundColor: '#3498DB',
          border: '2px solid #3498DB',
          borderRadius: '50%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          cursor: 'pointer',
        });
      
        addUserButton.css('margin-left', '10px');
        $('#userManagementList_wrapper .dataTables_filter').append(addUserButton);

        senarai
          .on("order.dt search.dt", function () {
            senarai
              .column(0, { search: "applied", order: "applied" })
              .nodes()
              .each(function (cell, i) {
                cell.innerHTML = i + 1;
              });
          })
          .draw();
      
        $("#filterStatus").on("change", (e) => {
          // console.log(e.target.value)
          senarai.column(5).search(e.target.value).draw();
        });
      
        $("#filterJenisKenderaan").on("change", (e) => {
          console.log(e.target.value);
          senarai.column(1).search(e.target.value).draw();
        });
      
        $("#search_input").on("keyup", () => {
          let searched = $("#search_input").val();
          // console.log(searched)
          senarai.search(searched).draw();
        });
      
        $("#resetFilter").on("click", () => {
          $("#filterStatus").val("").trigger("change");
          $("#filterJenisKenderaan").val("").trigger("change");
          $("#search_input").val("");
      
          senarai.search("").columns().search("").draw();
        });
      };
      

      //////
