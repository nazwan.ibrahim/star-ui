var maxFileSize = 8000000; //8mb
var isErrorFileType = false;
var isErrorFileSize = false;
var uploadedFilesArray = [];
var uploadedFilesArray1 = [];
var uploadedEditedFilesArray = [];
var dataForm = new FormData();

$(function () {
  $('.deleteBtn').hide();
  $(".refreshBtn").on("click", () => {
    this.location.reload();
  });
})

function uploadButton() {
  $('#uploadCoverImg-0').val(null);
  $('#uploadCoverImg-0').trigger('click');
}

function onChangeUploadFile() {
  // reset
  isErrorFileType = false;
  isErrorFileSize = false;
  uploadedFilesArray = [];
  var allowedExtensions = ["jpg", "jpeg", "png", "img"];

  $.each($('#uploadCoverImg-0')[0].files, function (index, val) {
    uploadedFilesArray.push(this);

    var fileExtension = val.name.split('.').pop().toLowerCase();
    var isValidExtension = allowedExtensions.includes(fileExtension);

    // if (val.type != "image/jpeg" && val.type != "image/jpg" && val.type != "image/png") {
    //   isErrorFileType = true;
    // }

    if (!isValidExtension) {
      isErrorFileType = true;
    }

    if (val.size > maxFileSize) {
      isErrorFileSize = true;
    }
  });

  if (isErrorFileType) {
    $('#error-uploadedImageInvalidType').show();
  } else {
    $('#error-uploadedImageInvalidType').hide();
  }

  if (isErrorFileSize) {
    $('#error-uploadedMaxSizeLimitReached').show();
  } else {
    $('#error-uploadedMaxSizeLimitReached').hide();
  }

  if ($('#uploadCoverImg-0')[0].files[0]) {
    $('#error-uploadCoverImg').hide();
  }

  processUploadedImage();
}


function padamFail(fileIndex) {
  uploadedFilesArray.splice(fileIndex, 1);
  processUploadedImage();
}

function processUploadedImage() {
  if (!uploadedFilesArray.length) {

    $('#error-uploadedGambarBilik').show();
    $('#parentDivUploadedFileDisplay-0').html('');
    $('#parentDivUploadedFileDisplay-0').hide();
    $('#btnuploadfileParentDiv-0').show();
  } else {

    $('#btnuploadfileParentDiv-0').hide();
    $('#parentDivUploadedFileDisplay-0').show();
    $('#parentDivUploadedFileDisplay-0').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-0').append(`
        <div class="book-cover-card-body">
          <img id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="book cover" height="160">
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="padamFail(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}





function uploadButton1(index) {
  $('#uploadCoverImg-' + index).val(null);
  $('#uploadCoverImg-' + index).trigger('click');
}

function onChangeUploadFile1(index) {
  // reset
  isErrorFileType = false;
  isErrorFileSize = false;
  uploadedFilesArray1 = [];
  $.each($('#uploadCoverImg-' + index)[0].files, function (index, val) {
    uploadedFilesArray1.push(this);
    if (val.type != "image/jpeg" && val.type != "image/jpg" && val.type != "image/png") {
      isErrorFileType = true;
    }
    if (val.size > maxFileSize) {
      isErrorFileSize = true;
    }
  });
  if (isErrorFileType) {
    $('#error-uploadedImageInvalidType').show();
  } else {
    $('#error-uploadedImageInvalidType').hide();
  }
  if (isErrorFileSize) {
    $('#error-uploadedMaxSizeLimitReached').show();
  } else {
    $('#error-uploadedMaxSizeLimitReached').hide();
  }

  if ($('#uploadCoverImg-' + index)[0].files[0]) {
    $('#error-uploadCoverImg').hide();
  }

  processUploadedImage1(index);
}

function padamFail1(fileIndex, index) {
  uploadedFilesArray1.splice(fileIndex, 1);
  processUploadedImage1(index);
}

function processUploadedImage1(index) {
  if (!uploadedFilesArray1.length) {

    $('#error-uploadedGambarBilik').show();
    $('#parentDivUploadedFileDisplay-' + index).html('');
    $('#parentDivUploadedFileDisplay-' + index).hide();
    $('#btnuploadfileParentDiv-' + index).show();
  } else {

    $('#btnuploadfileParentDiv-' + index).hide();
    $('#parentDivUploadedFileDisplay-' + index).show();
    $('#parentDivUploadedFileDisplay-' + index).html('');

    for (var i = 0; i < uploadedFilesArray1.length; i++) {
      $('#parentDivUploadedFileDisplay-' + index).append(`
        <div class="card col-2 border rounded-lg p-2 mx-2">
          <img id="lampiranAduan" class="card-img-top" src="` + URL.createObjectURL(uploadedFilesArray1[i]) + `" alt="book cover" height="160">
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray1[i].name}</p>
          </div>
          <a  onclick="padamFail1(${i}, ${index})" class="book-cover-card-btn cursor-pointer">Padam Fail</a>
        </div>`);
    }
  }
  if (uploadedFilesArray1.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray1, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}

// $("#hantarBtn").on("click", () => {
//   validateBorang();

//   if(validateBorang() == false){
//     $("#confirmationModal").modal("show");
//   }
//   else{
//     $('html, body').animate({
//       scrollTop: $('#maklumatDaftar').offset().top + 'px'
//   }, 1000);
//   }
// });

// function validateBorang() {
//   let IsError = false;

//   if ($("#filterJenisCenderamata-0").val()) {
//     $("#error-jenisCenderamata").hide();
//     } else {
//     $("#error-jenisCenderamata").show();
//     IsError = true
//   }

//   if ($("#harga-0").val()) {
//     $("#error-harga").hide();
//     } else {
//     $("#error-harga").show();
//     IsError = true
//   }

//   if ($("#kuantiti-0").val()) {
//     $("#error-kuantiti").hide();
//     } else {
//     $("#error-kuantiti").show();
//     IsError = true
//   }

//   if ($("#tarikhPenerimaan-0").val()) {
//     $("#error-tarikh").hide();
//     } else {
//     $("#error-tarikh").show();
//     IsError = true
//   }

//   if (uploadedFilesArray.length > 0) {
//     $('#error-uploadedGambarBilik').hide();
//   } else {
//     $('#error-uploadedGambarBilik').show();
//     IsError = true;
//   }

// return IsError;

// }

var frm = $('.form-list');
var countImg = $('.card-img-top').length;
var refnum;
// submit form
$("#submitBorangDaftar").on("click", () => {
  $("#loadingModal").modal({
    backdrop: "static",
    keyboard: false,
    show: true,
  });


  let postAPIBook = `${host}/api/book/addBookInformation`;

  $(".form-list").each(function (i) {
    let form = new FormData();
   
    
    form.append("BookTitle", $("#BookTitle").val());
    form.append("BookDescription", $("#BookDescription").val());
    form.append("BookCode", $("#BookCode").val());
    form.append("SellPrice", $("#SellPrice").val());
    form.append("StockIn", $("#StockIn").val());
    form.append("Createdby", userID);
    

    // code daus v2
    let fileInputImages = $("#uploadCoverImg-" + i)[0];
    for (j = 0; j < fileInputImages.files.length; j++) {
      form.append("FailGambar[]", $("#uploadCoverImg-" + i)[0].files[j]);
    }
   //  console.log(...form);

    let postData = $.ajax({
        url: postAPIBook,
        type: "POST",
        mimeType: "multipart/form-data",
        processData: false,
        contentType: false,
        async: false,
        data: form,
      }).done((res) => {
        // Handle success here
        console.log("Operation successful");
      }).fail((err) => {
        // Handle failure here
        console.error(err);
      }).always(() => {
        // This part is executed regardless of success or failure
        $("#loadingModal").modal("hide"); // Hide the loading modal
      //  $("#loadingModal").modal("hide");
      //   setTimeout(() => {
      //     // $("#berjaya-simpan-modal").modal({
      //     //   backdrop: 'static',
      //     //   keyboard: false,
      //     // });
      //   }, 1000);
        location.replace(
              `${host}/book/bookInventory`
            );

      });
   
  })

});

//Book Inventory List


$(function(){

  InventoryList();
 
});

  let InventoryList = () => {

       let getInventoryList = `${host}/api/book/bookInventory`;
      $.ajax({
        url: getInventoryList,
        type: "GET",
        dataType: "JSON",
      })
        .done((res) => {
          //createTable(res.data);
          tableInventorylist(res.bookInventorylist);
          console.log(res.bookInventorylist);
        })
        .catch((err) => {});
    };


    let tableInventorylist = (data) => {
      let senarai = $("#BookInventoryList").DataTable({
        data: data,
        //  order: [[4, "desc"]],
        columnDefs: [
          {
            targets: 0,
            data: null,
            searchable: false,
            orderable: false,
          },
          {
            targets: 1,
            data: "book_title",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
              }
              if (data !== null || data !== "") {
                return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
              }
            },
          },
          {
            targets: 2,
            data: "online_price",
            render: function (data, type, full, meta) {
              return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
            },
          },
          
          {
            targets: 3,
            data: "stock_in",
            render: function (data, type, full, meta) {
              if(data && data != '' && data != null) {
               
                return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
              } else {
                return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
              }
              
            },
          },
          {
            targets: 4,
            data: "stock_out",
            render: function (data, type, full, meta) {
            
              if(data && data != '' && data != null) {
                  return `<div class="dark-turquoise-status poppins-medium-12">${data}</div>`;
              }else
               {
                return "-";
              }
            },
          },
          {
            targets: 5,
            data: "status",
            className: "text-center",
            render: function (data, type, full, meta) {
              
    
              if(data == '0' && data != null) {
                  return `<div class="dark-turquoise-status poppins-medium-12">Active</div>`;
              }else
               {
                return "Not active";
              }


            },
          },
          {
            targets: 6,
            data: "id",
            //visible: false,
            render: function (data, type, full, meta) {

              let editLink = "/book/bookInventoryUpdate/" + data;
              let deleteLink = "/book/bookInventory" + data;
      
                if (data != null || data !== "") {

                  
                  return `<div>
                  <a href="${editLink}" class="btn btn-sm">
                    <i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i>
                  </a>
                  <a href="${deleteLink}" class="btn btn-sm">
                    <i class="fas fa-trash-alt" style="font-size: 18px; color: #3498DB;"></i>
                  </a>
                </div>`;
              } else {
                return "-";
              };
            },
          },
        ],
        language: {
          info: "view _START_ to _END_ from _TOTAL_ entry",
          infoEmpty: "view 0 from 0 to 0 entry",
          lengthMenu: "view _MENU_ Entry",
          paginate: {
            next: "Next",
            previous: "Previous",
          },
          zeroRecords: "No record found",
        },
      });

      
    
      senarai
        .on("order.dt search.dt", function () {
          senarai
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
              cell.innerHTML = i + 1;
            });
        })
        .draw();
    
      $("#filterStatus").on("change", (e) => {
        // console.log(e.target.value)
        senarai.column(5).search(e.target.value).draw();
      });
    
      $("#filterJenisKenderaan").on("change", (e) => {
        console.log(e.target.value);
        senarai.column(1).search(e.target.value).draw();
      });
    
      $("#search_input").on("keyup", () => {
        let searched = $("#search_input").val();
        // console.log(searched)
        senarai.search(searched).draw();
      });
    
      $("#resetFilter").on("click", () => {
        $("#filterStatus").val("").trigger("change");
        $("#filterJenisKenderaan").val("").trigger("change");
        $("#search_input").val("");
    
        senarai.search("").columns().search("").draw();
      });
    };
    

 let openDeleteModal = (val) => {
      let id = $(val).data("id");
      
  
      $("#deleteModalTitle").text("Delete book");
      $("#hapus-modal").modal("show");
  
      // Listen for the modal to be fully shown
      // $("#hapus-modal").on("shown.bs.modal", function () {
          // Bind the delete action to the button with ID "deleteid"
          // $("#deleteid").on("click", () => {
          //     $("#hapus-modal").modal("hide");
          //     let dataID = id;
          //     let deleteAPI = `${host}/api/book/${dataID}/bookDelete`;
              

          //     $.ajax({
          //         url: deleteAPI,
          //         type: "PUT",
          //         dataType: "JSON",
          //         data: { userID: userid },
          //     })
          //     .done((res) => {
          //         // Handle success, e.g., update the UI
          //         console.log("Deleted successfully");
          //     })
          //     .catch((err) => {
          //         // Handle errors
          //         console.error("Delete failed", err);
          //     });
          // });
          openDeleteModalOperation(id);
      // });
  }
   
////////
let openDeleteModalOperation = (val) => {
$("#submitBtn").on("click", () => {
  $("#hapus-modal").modal("hide");
  $("#loadingModal").modal("show");
  //alert(val);
  
  let dataID = val;
  let deleteAPI = `${host}/api/book/${dataID}/bookDelete`;

 

  const data = {
    userID: userID
  };
  //console.log(data);
  let postData = $.ajax({
    url: deleteAPI,
    method: "PUT",
    data: data,
  })
    .done((res) => {
      // console.log(res);
//       setTimeout( () => {
        

//         $("#loadingModal").modal("hide");
//         $("#loadingModal").removeClass("in");
//         $('body').removeClass('modal-open');
//         $("#tempahanBaharuBerjayaModal").modal("show");
//         $("#loadingModal").hide();
// //         $("#loadingModal").modal("hide");
//           },2000);
          setTimeout(() => {
            $("#loadingModal").modal("hide");
            $("#successDeleteModal").modal({
                backdrop: 'static',
                keyboard: false,
            });
            $("#loadingModal").modal("hide");
        }, 2000);

      //location.replace(host + "/tempahan/kenderaan/pengguna/papan-pemuka");
    })
    .catch((err) => {
      // 
    });
  });
}

