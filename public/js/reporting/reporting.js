$(function(){
    trainingList();
});

let trainingList = () => {
    let getList = `${host}/api/reporting/trainingList`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((res) => {
        if (res.status === 'success') {
            tableTrainingList(res.reportingTrainingList);
        } else {
            console.error('Error:', res.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let tableTrainingList = (data) => {
    reportingTrainingList = $("#trainingReportList").DataTable({
        data: data,
        responsive: true,
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                data: "id",
                render: function (data, type, full, meta) {
                    let viewLink = "/admin/training-details/" + data;
            
                    if (data !== null && data !== "") {
                        return `<div>
                            <p>${full.course_name}<p>
                        </div>`;
                    } else {
                        return "-";
                    }
                }
            },
            {
                data: "date_start",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "date_end",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "venue",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "total_participant",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "new_fee",
                render: function (data, type, full, meta) {
                    if (data) {
                        const amountInNumeric = parseFloat(data);
            
                        const formatter = new Intl.NumberFormat('en-MY', {
                            style: 'currency',
                            currency: 'MYR'
                        });
            
                        const formattedAmount = formatter.format(amountInNumeric);
            
                        return `<td width="50%" align="center"><p class="table-content">${formattedAmount}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><p class="table-content">-</p></td>';
                    }
                }
            },
            {
                data: "status",
                render: function (data, type, full, meta) {
                    let statusText = data === 1 ? '<span class="active-status">Active</span>' : data === 2 ? '<span class="success-status">Completed</span>' : '-';
                    return `<td width="50%" align="center"><p class="table-content">${statusText}</p></td>`;
                }
            },
            {
                data: "region",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center" class="hidden-column"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "batch_no",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center" class="hidden-column"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "year_end",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center" class="hidden-column"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "id_training_option",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center" class="hidden-column"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "id_training_type",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center" class="hidden-column"><p class="table-content">${data}</p></td>` : '-';
                }
            },
        ],
        select: {
            style: 'multi',
            selector: 'td:first-child',
        },
        columnDefs: [
            { targets: [8, 9, 10, 11, 12], visible: false }
        ],
        language: {
            info: "_START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
                next: "Next",
                previous: "Previous",
            },
            zeroRecords: "No record found",
        },
        
    });

    $('#trainingType_filter').on('change', function () {
        reportingTrainingList.column(12).search($(this).val()).draw();
    });

    $('#trainingOption_filter').on('change', function () {
        reportingTrainingList.column(11).search($(this).val()).draw();
    });

    $('#year_filter').on('change', function () {
        reportingTrainingList.column(10).search($(this).val()).draw();
    });

    $('#batch_filter').on('change', function () {
        reportingTrainingList.column(9).search($(this).val()).draw();
    });

    $('#region_filter').on('change', function () {
        reportingTrainingList.column(8).search($(this).val()).draw();
    });

    $('#status_filter').on('change', function () {
        reportingTrainingList.column(7).search($(this).val()).draw();
    });

    reportingTrainingList.on("order.dt search.dt", function () {
        reportingTrainingList
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        })
        .draw();

};

$(function(){
    paymentList();
});

let paymentList = () => {
    let getList = `${host}/api/reporting/paymentList`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        if (response.status === 'success') {
            tablePaymentList(response.reportingPaymentList, response.courseList);
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let tablePaymentList = (data, courseList) => {
    reportingPaymentList = $("#paymentReportList").DataTable({
        data: data,
        responsive: true,
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                data: "id",
                render: function (data, type, full, meta) {
                    let viewLink = "/admin/payment-details/" + data;
            
                    if (data !== null && data !== "") {
                        return `<div>
                            <p>${full.course_name}</p>
                        </div>`;
                    } else {
                        return "-";
                    }
                }
            },
            {
                data: null,
                render: function (data, type, full, meta) {
                    if (data.date_start) {
                        return `<td width="50%" align="center"><p class="table-content">${data.date_end}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: null,
                render: function (data, type, full, meta) {
                    if (data.date_end) {
                        return `<td width="50%" align="center"><p class="table-content">${data.date_end}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "venue",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "new_fee",
                render: function (data, type, full, meta) {
                    if (data) {
                        const amountInNumeric = parseFloat(data);
            
                        const formatter = new Intl.NumberFormat('en-MY', {
                            style: 'currency',
                            currency: 'MYR'
                        });
            
                        const formattedAmount = formatter.format(amountInNumeric);
            
                        return `<td width="50%" align="center"><p class="table-content">${formattedAmount}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><p class="table-content">-</p></td>';
                    }
                }
            },
            {
                data: "has_fee_receipt",
                render: function (data, type, full, meta) {
                    if (data === 1) {
                        return `<td width="15%"><p class="paid-status" style="text-align: center;">PAID</p></td>`;
                    } else if (data === 0) {
                        return `<td width="15%"><p class="failed-status" style="text-align: center;">UNPAID</p></td>`;
                    } else {
                        return "-";
                    }
                }
            },
            
            {
                data: "year_end",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center" class="hidden-column"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "region",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center" class="hidden-column"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "state",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center" class="hidden-column"><p class="table-content">${data}</p></td>` : '-';
                }
            },
        ],
        select: {
            style: 'multi',
            selector: 'td:first-child',
            type: 'string-case-insensitive',
        },
        columnDefs: [
            { targets: [ 7, 8, 9], visible: false }
        ],
        language: {
            info: "_START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
                next: "Next",
                previous: "Previous",
            },
            zeroRecords: "No record found",
        },
        
    });

    let courseDropdown = $('#course_filter');
    courseDropdown.empty();
    courseDropdown.append('<option value="">All</option>');

    $.each(courseList, function(index, course) {
        courseDropdown.append($('<option></option>').attr('value', course.course_name).text(course.course_name));
    });

    $('#course_filter').on('change', function () {
        reportingPaymentList.column(1).search($(this).val()).draw();
    });

    $('#state_filter').on('change', function () {
        reportingPaymentList.column(9).search($(this).val()).draw();
    });

    $('#year_filter').on('change', function () {
        reportingPaymentList.column(7).search($(this).val()).draw();
    });

    $('#payment_filter').on('change', function () {
        reportingPaymentList.column(6).search($(this).val()).draw();
    });

    $('#region_filter').on('change', function () {
        reportingPaymentList.column(8).search($(this).val()).draw();
    });

    $('#endDate_filter').on('change', function () {
        reportingPaymentList.column(3).search($(this).val()).draw();
    });

    reportingPaymentList.on("order.dt search.dt", function () {
        reportingPaymentList
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        })
        .draw();

};

$(function(){
    accessLog();
});

let accessLog = () => {
    let getList = `${host}/api/reporting/accessLog`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        if (response.status === 'success') {
            accessLogList(response.accessLog);
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let accessLogList = (data) => {
    accessLog = $("#accessLog").DataTable({
        data: data,
        responsive: true,
        columns: [
            {
                data: "username",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "activity_time",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "ip_address",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "activity",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "browser",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
        ],
        language: {
            info: "_START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
                next: "Next",
                previous: "Previous",
            },
            zeroRecords: "No record found",
        },
        
    });

};

$(function(){
    auditTrail();
});

let auditTrail = () => {
    let getList = `${host}/api/reporting/auditTrail`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        if (response.status === 'success') {
            auditTrailList(response.auditTrail);
        } else {
            console.error('Error:', response.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let auditTrailList = (data) => {
    auditTrail = $("#auditTrail").DataTable({
        data: data,
        responsive: true,
        columns: [
            {
                data: "username",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "activity_time",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "ip_address",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "activity",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "browser",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
        ],
        language: {
            info: "_START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
                next: "Next",
                previous: "Previous",
            },
            zeroRecords: "No record found",
        },
        
    });

};