
  let senarai;
        let trainingalllist = () => {
            
          let userid = userID;

             let getList = `${host}/api/training/trainingManagementListAll`;
             
  
            $.ajax({
              url: getList,
              type: "GET",
              dataType: "JSON",
            })
              .done((res) => {
                //createTable(res.data);
                
                tableTrainingList(res.trainingalllist);
  
              })
              .catch((err) => {});
          };
    
     
    
          let tableTrainingList = (data) => {
            senarai = $("#trainingManagementListAll").DataTable({
              data: data,
              responsive: true,
              //  order: [[4, "desc"]],
              columnDefs: [
                {
                  targets: 0,
                  data: null,
                  searchable: false,
                  orderable: false,
                },
                {
                  targets: 1,
                  data: "company_name",
                  render: function (data, type, full, meta) {
                    if (data === null) {
                      return `<td width="50%" align="center"><p class="table-content">-</p></td>`;
                    }
                    if (data !== null || data !== "") {
                      return `<td align="center" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                    }
                  },
                },
                {
                    targets: 2,
                    data: "training_name",
                    render: function (data, type, full, meta) {
                      if (data === null) {
                        return `<td width="50%" align="center"><p class="table-content">-</p></td>`;
                      }
                      if (data !== null || data !== "") {
                        return `<td align="center" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                      }
                    },
                  },
                  {
                    targets: 3,
                    data: "category_name",
                    render: function (data, type, full, meta) {
                      if (data === null) {
                        return `<td width="50%" align="center"><p class="table-content">-</p></td>`;
                      }
                      if (data !== null || data !== "") {
                        return `<td align="center" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                      }
                    },
                  },
                  {
                    targets: 4,
                    data: "type_name",
                    render: function (data, type, full, meta) {
                      if (data === null) {
                        return `<td width="50%" align="center"><p class="table-content">-</p></td>`;
                      }
                      if (data !== null || data !== "") {
                        return `<td align="center" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                      }
                    },
                  },
                  {
                    targets:5,
                    data: "batch_no_full",
                    render: function (data, type, full, meta) {
                      if (data === null) {
                        return `<td width="50%" align="center"><p class="table-content">-</p></td>`;
                      }
                      if (data !== null || data !== "") {
                        return `<td align="center" width="40%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                      }
                    },
                  },
                  {
                    targets: 6,
                    data: "date_start",
                    render: function (data, type, full, meta) {
                      if (data && data !== '' && data !== null) {
                        // Assuming 'data1' and 'data2' are the two data values you want to display
                        var data1 = full.date_start; // Replace 'data1' with your actual data field name
                        var data2 = full.date_end; // Replace 'data2' with your actual data field name
                  
                        // Combine 'data1' and 'data2' into one string
                        var combinedData = data1 + ' to ' + data2;
                  
                        // You can also use HTML for formatting
                        var formattedData = '<p class="poppins-semibold-14">' + combinedData + '</p>';
                  
                        return formattedData;
                      } else {
                        return '<td width="15%"><p class="poppins-semibold-14">-</p></td>';
                      }
                    },
                  },
                  // {
                  //   targets: 5,
                  //   data: "date_start",
                  //   render: function (data, type, full, meta) {
                  //     if (data === null) {
                  //       return `<td width="20%" align="center"><p class="table-content">-</p></td>`;
                  //     }
                  //     if (data !== null || data !== "") {
                  //       return `<td align="center" width="30%"><p class="table-content" style="text-align: center;">${data}</p></td>`;
                  //     }
                  //   },
                  // },
                  {
                    targets: 7,
                    data: "endorsed_status",
                    render: function (data, type, full, meta) {
                      if (data === null) {
                        return `<td width="20%" align="center"><p class="table-content">-</p></td>`;
                      }
                      if (data == 1 ) {
                        return `<td align="center" width="30%"><p class="table-content" style="text-align: center;">Endorsed</p></td>`;
                      }
                      if (data == 2 ) {
                        return `<td align="center" width="30%"><p class="table-content" style="text-align: center;">Query</p></td>`;
                      }
                      if (data == 3 ) {
                        return `<td align="center" width="30%"><p class="table-content" style="text-align: center;">Not Endorse</p></td>`;
                      }
                    },
                  },
                  {
                    targets: 8,
                    data: "status",
                    render: function (data, type, full, meta) {              
                        if (data === null) {
                          return `<td width="50%" align="center"><p class="table-content">-</p></td>`;
                        } else {
                            if (data === 1) {
                                return `<td width="30%" align="center"><p class="open-status">Up Coming</p></td>`;
                            } else if (data === 3) {
                                return `<td width="30%" align="center"><p class="closed-status">Completed</p></td>`;
                            } else if (data === 2) {
                              return `<td width="30%" align="center"><p class="closed-status">On Going</p></td>`;
                          }
                        }
                    }
                },
                {
                  targets: 9,
                  data: "id_training_course",
                  render: function (data, type, full, meta) {
                    if (data !== null && data !== "") {
                      // return `<div>
                      //           <a href="${host}/admin/training-management-attendance/${data}" class="btn btn-sm" data-id="${data}">
                      //             <i class="fas fa-eye" style="font-size: 18px; color: #3498DB;"></i>
                      //            </a>
                      //           <a href="${host}/admin/training-management-feedback/${data}" class="btn btn-sm feedback-detail" data-id="${data}">
                      //           <i class="fas fa-comments" style="font-size: 18px; color: #3498DB;"></i>
                      //           </a>
                      //           <a class="btn btn-sm postpone-detail" data-id="${data}">
                      //           <i class="fas fa-calendar-alt" style="font-size: 18px; color: #3498DB;"></i>
                      //           </a>
                      //         </div>`;

                      return  `
                      <a href="${host}/admin/training-management/attendance-list/${full.batch_no_full}/${data}" class="btn btn-sm" data-id="${data}">
                          <i class="fas fa-eye" style="font-size: 18px; color: #3498DB;"></i>
                      </a>
                    `;


                    } else {
                      return "-";
                    }
                  },
                }
              ],
              language: {
                info: "show_START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "show 0 from 0 to 0 entry",
                lengthMenu: " _MENU_ Entry",
                paginate: {
                  next: "Next",
                  previous: "Previous",
                },
                zeroRecords: "No record found",
              },
            });
          
            senarai
              .on("order.dt search.dt", function () {
                senarai
                  .column(0, { search: "applied", order: "applied" })
                  .nodes()
                  .each(function (cell, i) {
                    cell.innerHTML = i + 1;
                  });
              })
              .draw();
          
            $("#filterStatus").on("change", (e) => {
              // console.log(e.target.value)
              senarai.column(5).search(e.target.value).draw();
            });
          
            $("#filterJenisKenderaan").on("change", (e) => {
              console.log(e.target.value);
              senarai.column(1).search(e.target.value).draw();
            });
          
            $("#search_input").on("keyup", () => {
              let searched = $("#search_input").val();
              // console.log(searched)
              senarai.search(searched).draw();
            });
          
            $("#resetFilter").on("click", () => {
              $("#filterStatus").val("").trigger("change");
              $("#filterJenisKenderaan").val("").trigger("change");
              $("#search_input").val("");
          
              senarai.search("").columns().search("").draw();
            });
          };
  
  
          let selectedDataId;
          let userid = userID; // Move the declaration here
          
          $(document).on("click", ".postpone-detail", function () {
            selectedDataId = $(this).data("id");
            // Open the modal
            $("#attendModal").modal("show");
          
            // Fetch the data and populate the modal input fields
            let getTraining = `${host}/api/training/${selectedDataId}/trainingPostpone`;
          
            $.ajax({
              url: getTraining,
              type: 'GET',
              contentType: "application/json; charset-utf-8",
              dataType: 'json',
              success: function (response) {
                const info = response.trainingPostpone;
          
                $.each(info, function(index, value) {
                  $('#training_name').text(value.training_name || 'n/a');
                  $('#company_name').text(value.company_name || 'n/a');
                  $('#date_start').text(value.date_start || 'n/a');
                  $('#date_end').text(value.date_end || 'n/a');
                  $('#training_status').text(value.training_status || 'n/a');
                });
              },
              error: function (error) {
                console.log("Error:", error);
              }
            });
          
            // Continue with the rest of your code
            let getUserInfo = `${host}/api/profile/${userid}/userInformation/`;
          
            $.ajax({
              url: getUserInfo,
              type: 'GET',
              contentType: "application/json; charset-utf-8",
              dataType: 'json',
              success: function (response) {
                let info = response.userInfo;
                console.log(info, 'info');
          
                $.each(info, function(index, value) {
                  $('#fullname').text(value.fullname || 'n/a');
                });
              }
            });
          });
          
  
  // Handle form submission for updating data
  // $("#updateAttendForm").submit(function (e) {
  //     e.preventDefault();
    
  //     const dataId = selectedDataId;
  
  //     const date_start = $("#newStartDate").val();
  //     const date_end = $("#newEndDate").val();
  //     const remarks = $("#remarks").val();
  
  //     const updatedData = {
  //       date_start: date_start,
  //       date_end: date_end,
  //       remarks: remarks,
  
  //     };
  
  //     $.ajax({
  //         url: `${host}/api/training/${dataId}/updateTrainingPostpone `, 
  //         type: 'PUT',
  //         data: updatedData,
  //         success: function (response) {
  //             console.log("Data updated successfully:", response);
  //             showSuccessModal("Data updated successfully");
  //             setTimeout(function() {
  //               location.reload();
  //             });
  //             $("#attendModal").modal("hide");
  //         },
  //         error: function (error) {
  //             console.log("Error:", error);
  //         }
  //     });
  // });
  
  
  // function showSuccessModal(message) {
  //   $('#successMessage').text(message);
    
  //   $('#successModal').modal('show');
  
    
  // }     


  $(function () {
    trainingalllist();
  });