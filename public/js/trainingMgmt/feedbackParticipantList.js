$(function(){
    participantList();
    queryExamResult();
    submitFeedback();
});


let url = window.location.pathname;
let batch_full = url.substring(url.lastIndexOf('/') + 1);
let segments = url.split('/');
let ID = segments[segments.length - 2];

let getDetails = `${host}/api/exam/${ID}/${batch_full}/examDetailsTPSubmit`;
//console.log('examDetails',ID);

$.ajax({
 url: getDetails,
 type: 'GET',
// contentType: "application/json",
 dataType: "JSON",
}).done((res) => {
 let data = res.trainingDetails;
 console.log('sss',data);

 $.each(data, function(index, value) {


 (value.training_name == null || value.training_name == '') ? $('#provider_name').text('n/a') : $('#provider_name').text(value.training_name);
 (value.date_start == null || value.date_start == '') ? $('#date_start').text('n/a') : $('#date_start').text(value.date_start);
 (value.date_end == null || value.date_end == '') ? $('#date_end').text('n/a') : $('#date_end').text(value.date_end);
 (value.venue == null || value.venue == '') ? $('#venue').text('n/a') : $('#venue').text(value.venue);
 (value.batch_no_full == null || value.batch_no_full == '') ? $('#batch_no').text('n/a') : $('#batch_no').text(value.batch_no_full);
 
 
 });


}).catch((err) => {
 //console.log(err);
});


let participantList = () => {

    let url = window.location.pathname;
    let ID = url.substring(url.lastIndexOf('/') + 1);

    let segments = url.split('/');
    let batch_no = segments[segments.length - 2];
   

    let getList = `${host}/api/training/${batch_no}/${ID}/participantFeedbackList`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((res) => {
        if (res.status === 'success') {
           data = res.participantlist;
           $.each(data, function(index, value) {

            (value.training_name == null || value.training_name == '') ? $('#trainingName').text('n/a') : $('#trainingName').text(value.training_name);
           
           });
            tableTrainingList(data);
        } else {
            console.error('Error:', res.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let tableTrainingList = (data) => {
    reportingTrainingList = $("#feedbackReportList").DataTable({
        data: data,
        responsive: true,
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                data: "fullname",
                render: function (data, type, full, meta) {
                   
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                   
                }
            },
            {
                data: "ic_no",
                render: function (data, type, full, meta) {
                   
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                   
                }
            },

            {
                data: "submitted_status",
                render: function (data, type, full, meta) {
                    // if (data !== null || data !== "") {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>`: 'Not Submit';
                    // } else{
                    //     return data ? `<td width="50%" align="center"><p class="table-content">-</p></td>`;
                    //     }
                }
            },
            {
                data: "endorse_feedback",
                render: function (data, type, full, meta) {
                     if (data !== null || data == 1) {
                    return `<td width="50%" align="center"><p class="table-content">Submited to SEDA</p></td>`;
                    } else if (data === null || data === "0") {
                        return `<td width="50%" align="center"><p class="table-content">Not submit</p></td>`;
                        }
                }
            },
           
           
           
            {
                data: "id_training_participant",
                render: function (data, type, full, meta) {
                    return  `
                    <td width="50%" align="center"><p class="table-content"><a href="${host}/training-partner/training-management/participant-feedback/${full.batch_no_full}/${data}" class="btn btn-sm">
                    <button class="btn btn-sm icon-style" data-id="${data}" 
                    <i class="icon-sunting" data-id="${data}">View Feedback</i>
                    </button></a></p></td>
              
                  `;
                    
                }
            },
        ],
        language: {
            info: "_START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
                next: "Next",
                previous: "Previous",
            },
            zeroRecords: "No record found",
        },
        
    });

    

    reportingTrainingList.on("order.dt search.dt", function () {
        reportingTrainingList
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        })
        .draw();

};




let queryExamResult = () => {

    // $("#QueryBtn").on("click", () => {
    //     $("#confirmationQueryModal").modal('show');
    // });
    let url = window.location.pathname;
    let ID = url.substring(url.lastIndexOf('/') + 1);
  
    let segments = url.split('/');
    let batch_no = segments[segments.length - 2];

   
    $("#AttendanceButton").on("click", () => {
        //$("#confirmationQueryModal").modal("hide");
        
        
        let editExamResult = `${host}/api/training/${batch_no}/${ID}/updateFeedbackSubmit`;
        
        Submitted = "Submitted";
        userid = userID
      const data = {

        //atch_No : $('#formEndorseResult input[name="batch_no"]').val(),
        submitted_status: Submitted,
        userid : userid,
          };
        //console.log(data);
        let postData = $.ajax({
          url: editExamResult,
          method: "PUT",
          data: data,
        }) .done((res) => {
         console.log('data',res);
  
         setTimeout(() => {

          $("#success-modal").modal("show");
        
      }, 2000);
 
  
  
        })
        .catch((err) => {
          //console.log(err);
        });
        $.when(postData).done( (res) => {
        //  $('#berjaya-kemaskini-modal').modal('show');
          let url = window.location.pathname;
          let ID = url.substring(url.lastIndexOf('/') + 1);
          // window.location.href = `${host}/exam/endorseExamResult/${ID}`;
  
       })
  
  
      });
  
 }

 let submitFeedback = () => {

    $("#submitToSeda").on("click", () => {
       
        $("#confirmationEndorseModal").modal('show');
    });
      

    // $(document).on('click', '#EndorseBtn', function() {
    //   //  alert('k');
    

    //     //$('#edit-result-modal').modal('hide');
    //     $('#confirmationEndorseModal').modal('show');
    
    // });
   
    $("#submitToEndorse").on("click", () => {
        $("#confirmationEndorseModal").modal("hide");

        let url = window.location.pathname;
         ID = url.substring(url.lastIndexOf('/') + 1);
        let segments = url.split('/');
         batch_full = segments[segments.length - 2];
       
        let editExamResult = host + '/api/exam/submitFeedbackToEndorse';
        
  
      const data = {

        Batch_No : batch_full,
        Training_ID : ID,
        
          };
        //console.log(data);
        let postData = $.ajax({
          url: editExamResult,
          method: "PUT",
          data: data,
        }) .done((res) => {
         console.log('data',res);
  
        //   setTimeout(() => {
  
            
        //     $("#success-modal").modal({
        //         backdrop: 'static',
        //         keyboard: false,
        //     });
           
  
        // }, 2000);
  
  
        })
        .catch((err) => {
          //console.log(err);
        });
        $.when(postData).done( (res) => {
        //  $('#berjaya-kemaskini-modal').modal('show');
        setTimeout(() => {
  
            
            $("#success-modal").modal({
                backdrop: 'static',
                keyboard: false,
            }).modal('show');
           
  
        }, 2000);
  
  
  
       })
  
  
      });
  
 }



