$(function () {
  trainingtplist();
});
let senarai;
      let trainingtplist = () => {
  
           let getList = `${host}/api/reporting/trainingManagementListTP`;
           

          $.ajax({
            url: getList,
            type: "GET",
            dataType: "JSON",
          })
            .done((res) => {
              //createTable(res.data);
              
              tableTrainingList(res.trainingtplist);

            })
            .catch((err) => {});
        };
  
   
  
        let tableTrainingList = (data) => {
          senarai = $("#trainingManagementListTP").DataTable({
            data: data,
            responsive: true,
            //  order: [[4, "desc"]],
            columnDefs: [
              {
                targets: 0,
                data: null,
                searchable: false,
                orderable: false,
              },
              {
                targets: 1,
                data: "company_name",
                render: function (data, type, full, meta) {
                  if (data === null) {
                    return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
                  }
                  if (data !== null || data !== "") {
                    return `<td align="left" width="40%"><p class="poppins-semibold-14" style="text-align: left;">${data}</p></td>`;
                  }
                },
              },
              {
                  targets: 2,
                  data: "training_name",
                  render: function (data, type, full, meta) {
                    if (data === null) {
                      return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
                    }
                    if (data !== null || data !== "") {
                      return `<td align="left" width="40%"><p class="poppins-semibold-14" style="text-align: left;">${data}</p></td>`;
                    }
                  },
                },
                {
                  targets: 3,
                  data: "category_name",
                  render: function (data, type, full, meta) {
                    if (data === null) {
                      return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
                    }
                    if (data !== null || data !== "") {
                      return `<td align="left" width="40%"><p class="poppins-semibold-14" style="text-align: left;">${data}</p></td>`;
                    }
                  },
                },
                {
                  targets: 4,
                  data: "type_name",
                  render: function (data, type, full, meta) {
                    if (data === null) {
                      return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
                    }
                    if (data !== null || data !== "") {
                      return `<td align="left" width="40%"><p class="poppins-semibold-14" style="text-align: left;">${data}</p></td>`;
                    }
                  },
                },
                {
                  targets: 5,
                  data: "date_start",
                  render: function (data, type, full, meta) {
                    if (data === null) {
                      return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
                    }
                    if (data !== null || data !== "") {
                      return `<td align="left" width="40%"><p class="poppins-semibold-14" style="text-align: left;">${data}</p></td>`;
                    }
                  },
                },
                {
                  targets: 6,
                  data: "date_end",
                  render: function (data, type, full, meta) {
                    if (data === null) {
                      return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
                    }
                    if (data !== null || data !== "") {
                      return `<td align="left" width="40%"><p class="poppins-semibold-14" style="text-align: left;">${data}</p></td>`;
                    }
                  },
                },
                {
                    targets: 7,
                    data: "training_status",
                    render: function (data, type, full, meta) {
                      if (data === null) {
                        return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
                      }
                      if (data !== null || data !== "") {
                        let statusText = '';
                        if (data === 1) {
                            statusText = 'Active';
                        } else if (data === 2) {
                            statusText = 'Completed';
                        }
                        return `<td align="left" width="40%"><p class="poppins-semibold-14" style="text-align: left;">${statusText}</p></td>`;
                    }
                    },
                  },
              {
                targets: 8,
                data: "id",
                render: function (data, type, full, meta) {
                  // Assuming you have URLs for view, edit, and delete actions
                  if (data !== null && data !== "") {
                    return `<div>
                              <a href="${host}/admin/training-management-attendance/${data}" class="btn btn-sm" data-id="${data}">
                                <i class="icon-editIcon" style="font-size: 18px;"></i>Attend
                               </a>
                              <a class="btn btn-sm view-detail" data-id="${data}">
                              <i class="icon-deleteIcon" style="font-size: 18px;"></i> View
                              </a>
                              <a class="btn btn-sm postpone-detail" data-id="${data}">
                              <i class="icon-deleteIcon" style="font-size: 18px;"></i> Postpone
                              </a>
                            </div>`;
                  } else {
                    return "-";
                  }
                },
              }
            ],
            language: {
              info: "show_START_ to _END_ from _TOTAL_ entry",
              infoEmpty: "show 0 from 0 to 0 entry",
              lengthMenu: " _MENU_ Entry",
              paginate: {
                next: "Next",
                previous: "Previous",
              },
              zeroRecords: "No record found",
            },
          });
        
          senarai
            .on("order.dt search.dt", function () {
              senarai
                .column(0, { search: "applied", order: "applied" })
                .nodes()
                .each(function (cell, i) {
                  cell.innerHTML = i + 1;
                });
            })
            .draw();
        
          $("#filterStatus").on("change", (e) => {
            // console.log(e.target.value)
            senarai.column(5).search(e.target.value).draw();
          });
        
          $("#filterJenisKenderaan").on("change", (e) => {
            console.log(e.target.value);
            senarai.column(1).search(e.target.value).draw();
          });
        
          $("#search_input").on("keyup", () => {
            let searched = $("#search_input").val();
            // console.log(searched)
            senarai.search(searched).draw();
          });
        
          $("#resetFilter").on("click", () => {
            $("#filterStatus").val("").trigger("change");
            $("#filterJenisKenderaan").val("").trigger("change");
            $("#search_input").val("");
        
            senarai.search("").columns().search("").draw();
          });
        };


        let selectedDataId;

  $(document).on("click", ".postpone-detail", function () {
    selectedDataId = $(this).data("id");

    // Open the modal
    $("#attendModal").modal("show");

    // Fetch the data and populate the modal input fields
    let getTraining = `${host}/api/training/${selectedDataId}/trainingPostpone`;

    $.ajax({
      url: getTraining,
      type: 'GET',
      contentType: "application/json; charset-utf-8",
      dataType: 'json',
      success: function (response) {
        const info = response.trainingPostpone;

        $.each(info, function(index, value) {
          $('#training_name').text(value.training_name || 'n/a');
          $('#company_name').text(value.company_name || 'n/a');
          $('#date_start').text(value.date_start || 'n/a');
          $('#date_end').text(value.date_end || 'n/a');
          $('#training_status').text(value.training_status || 'n/a');

      });
      },
      error: function (error) {
        console.log("Error:", error);
      }
    });
});

// Handle form submission for updating data
$("#updateAttendForm").submit(function (e) {
    e.preventDefault();
  
    const dataId = selectedDataId;

    const date_start = $("#newStartDate").val();
    const date_end = $("#newEndDate").val();
    const remarks = $("#remarks").val();

    const updatedData = {
      date_start: date_start,
      date_end: date_end,
      remarks: remarks,

    };

    $.ajax({
        url: `${host}/api/training/${dataId}/updateTrainingPostpone `, 
        type: 'PUT',
        data: updatedData,
        success: function (response) {
            console.log("Data updated successfully:", response);
            showSuccessModal("Data updated successfully");
            setTimeout(function() {
              location.reload();
            });
            $("#attendModal").modal("hide");
        },
        error: function (error) {
            console.log("Error:", error);
        }
    });
});


function showSuccessModal(message) {
  $('#successMessage').text(message);
  
  $('#successModal').modal('show');

  
}     