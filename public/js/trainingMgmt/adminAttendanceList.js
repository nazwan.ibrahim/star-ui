
$(function(){

  ExamList();
  queryExamResult();
  StatusList();


});

let url = window.location.pathname;
let batch_full = url.substring(url.lastIndexOf('/') + 1);
let segments = url.split('/');
let ID = segments[segments.length - 2];

let getDetails = `${host}/api/training/${ID}/${batch_full}/trainingProvider`;
//console.log('examDetails',ID);

$.ajax({
 url: getDetails,
 type: 'GET',
// contentType: "application/json",
 dataType: "JSON",
}).done((res) => {
 let data = res.trainingDetails;
 console.log('sss',data);

 $.each(data, function(index, value) {


 (value.training_name == null || value.training_name == '') ? $('#provider_name').text('n/a') : $('#provider_name').text(value.training_name);
 (value.date_start == null || value.date_start == '') ? $('#date_start').text('n/a') : $('#date_start').text(value.date_start);
 (value.date_end == null || value.date_end == '') ? $('#date_end').text('n/a') : $('#date_end').text(value.date_end);
 (value.venue == null || value.venue == '') ? $('#venue').text('n/a') : $('#venue').text(value.venue);
 (value.batch_no_full == null || value.batch_no_full == '') ? $('#batch_no_full').text('n/a') : $('#batch_no_full').text(value.batch_no_full);
 
 
 });


}).catch((err) => {
 //console.log(err);
});


  let ExamList = () => {

      let url = window.location.pathname;
      let ID = url.substring(url.lastIndexOf('/') + 1);
      let segments = url.split('/');
      let batch_no = segments[segments.length - 2];


      let getList = `${host}/api/training/${batch_no}/${ID}/adminAttendanceList`;
   
      $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
      })
        .done((res) => {
          //createTable(res.data);
          tableExamList(res.examlist);
          console.log(res.examlist);
        })
        .catch((err) => {});

      

        // Datepicker initialization
        $('#datePickerIn').datepicker({
          format: 'dd/mm/yyyy', // Set the desired date format
      })
      .on('changeDate', function(e) {
          // Get the selected date
          var selectedDate = e.date;
      
          // Format the date as needed (e.g., "DD/MM/YYYY")
          var formattedDate = moment(selectedDate).format("DD/MM/YYYY");
      
          // Set the value of your date input
          $('.date-input-date-in').val(formattedDate);
      });
      
          $('#timePickerIn').datetimepicker({
            useCurrent: true, // Allow picking past times
           })
          .on('dp.change', function(e) {
            // Get the current time from #timePickerIn
            var currentTime = moment(e.date);
            
            // Format the time as needed (e.g., "hh:mm A")
            var formattedTime = currentTime.format("hh:mm A");
            
            // Set the value of your time input
            $('.date-input-time-in').val(formattedTime);
          });
          
        $('#timePickerOut').datetimepicker({
          useCurrent: true, // Allow picking past times
        })
        .on('dp.change', function(e) {
            // Get the current time from #timepicker1
            var currentTime = moment(e.date);
        
            // Format the time as needed (e.g., "hh:mm A")
            var formattedTime = currentTime.format("hh:mm A");
        
            // Set the value of #datepicker2
            $('.date-input-time-out').val(formattedTime);
        });
  };
  var selectAllCheckbox = $('<input type="checkbox" id="selectAllCheckbox" style="width: 40px;">');
  $('#TrainerAttendanceList thead tr th:nth-child(4)').prepend(selectAllCheckbox); // Assuming "Attend Check" is the 4th column

// Handle "Select All" checkbox functionality
selectAllCheckbox.change(function () {
$('.attendCheckbox').prop('checked', $(this).prop('checked'));
});


    let tableExamList = (data) => {
      let senarai = $("#AdminAttendanceList").DataTable({
        data: data,
        dom: 'lfrtip', 
        ordering: false, 
          responsive: true,
        //  order: [[4, "desc"]],
        columnDefs: [
          {
            data: null,
            searchable: false,
            orderable: false,
            render: function (data, type, full, meta) {
                return meta.row + 1;
            }
        },
        {
          targets: 0,
          data: null,
          searchable: false,
          orderable: false,
        },
        {
          targets: 1,
          data: "id_training_participant",
          visible: false,
          render: function (data, type, full, meta) {
            if (data === null) {
              return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
            }
            if (data !== null || data !== "") {
              return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
            }
          },
        },
        {
          targets: 2,
          className: 'text-left', 
          data: "fullname",
          render: function (data, type, full, meta) {
            if (data === null) {
              return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
            }
            if (data !== null || data !== "") {
              return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
            }
          },
        },
        {             
          targets: 3,
          data: "attend_check",
          orderable: false, 
          render: function (data, type, full, meta) {
            
            return '<td align="left" width="40%"><p class="poppins-semibold-14"><input class="attendCheckbox" type="checkbox" name="attendCheck[]" value="" style="width: 20px; height: 20px;"></p></td>';                  
          },
      },
        
        {
          targets: 4,
          data: "date_attend",
          orderable: false,
          render: function (data, type, full, meta) {
              var formattedTime = "";
      
              var datetimepicker = moment(data, "DD:MM:YYYY");
              formattedTime = datetimepicker.format("DD/MM/YYYY");
      
              return `<td align="left" width="15%"><input type="text" name="date_attend" class="form-control date-input-date-in" value=""></td>`;
          }
      },
      
          {
            targets: 5,
            data: "time_check",
            "orderable": false,
            render: function (data, type, full, meta) {
                // return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                var momentTime = moment(data, "HH:mm:ss");
                var formattedTime = momentTime.format("h:mm A");
              
                return `<td align="left" width="15%"><input type="text" name="time_check" id="time_check" class="form-control date-input-time-in" value="">
                </td>`;
              
  
          },
        },
          {
            targets: 6,
            data: "time_out",
            "orderable": false,
            render: function (data, type, full, meta) {
                // return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                var momentTime = moment(data, "HH:mm:ss");
                var formattedTime = momentTime.format("h:mm A");
               
                return `<td align="left" width="15%"><input type="text" name="time_out" id="time_out" class="form-control date-input-time-out" value=""></input>
                </td>`;
              
            },
          },
        {
          targets: 7,
          data: "remark",
          render: function (data, type, full, meta) {
            
              return `<td width="15%"><input type="text" name="remark" id="remark" class="form-control" value=""></td>`;
            
            
          },
        },

        {
          targets: 8,
          data: "participant_attendance_id",
          visible: false,
          render: function (data, type, full, meta) {
            if (data === null) {
              return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
            }
            if (data !== null || data !== "") {
              return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
            }
          },
        },
         
      
        ],
        language: {
          info: "view _START_ to _END_ from _TOTAL_ entry",
          infoEmpty: "view 0 from 0 to 0 entry",
          lengthMenu: "view _MENU_ Entry",
          paginate: {
            next: "Next",
            previous: "Previous",
          },
          zeroRecords: "No record found",
        },
      });
    
      senarai
        .on("order.dt search.dt", function () {
          senarai
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
              cell.innerHTML = i + 1;
            });
        })
        .draw();
    
      $("#filterStatus").on("change", (e) => {
        // console.log(e.target.value)
        senarai.column(5).search(e.target.value).draw();
      });
    
      $("#filterJenisKenderaan").on("change", (e) => {
        console.log(e.target.value);
        senarai.column(1).search(e.target.value).draw();
      });
    
      $("#search_input").on("keyup", () => {
        let searched = $("#search_input").val();
        // console.log(searched)
        senarai.search(searched).draw();
      });
    
      $("#resetFilter").on("click", () => {
        $("#filterStatus").val("").trigger("change");
        $("#filterJenisKenderaan").val("").trigger("change");
        $("#search_input").val("");
    
        senarai.search("").columns().search("").draw();
      });


      let url = window.location.pathname;
      let ID = url.substring(url.lastIndexOf('/') + 1);
    
      let segments = url.split('/');
      let batch_no = segments[segments.length - 2];

      $("#insertAttendanceButton").on("click", (event) => {
        event.preventDefault(); // Add this line
        let insertAttend = `${host}/api/training/${batch_no}/${ID}/insertAttendance`;
    
        const attendanceData = [];
    
        $("#AdminAttendanceList tbody tr").each(function () {
          const rowData = senarai.row($(this)).data();

          if (rowData) {
              const data = {
                  id_training_participant: rowData.id_training_participant,
                  id_training: ID,
                  attend_check: $(this).find("input[name='attendCheck[]'").is(":checked") ? 1 : 0,
                  date_attend: $(this).find("input[name='date_attend']").val(),
                  time_check: convertTo24HourFormat($(this).find("input[name='time_check']").val()),
                  time_out: convertTo24HourFormat($(this).find("input[name='time_out']").val()),
                  remark: $(this).find("input[name='remark']").val(),
              };
  
              attendanceData.push(data);
          }
        });
    
        const postData = $.ajax({
            url: insertAttend,
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify({ attendanceData }),
        })
        .done((res) => {
            console.log('data', res);
            $("#insertAttendanceButton").prop("disabled", true);

            setTimeout(() => {
              $("#success-modal").modal("show");
              
              // Reload the page after showing the success modal
              location.reload();
          }, 1000);
        })
        .fail((err) => {
            console.error('Error:', err.responseText);
        })
        .always(() => {
            // Code to execute regardless of success or failure
        });
    });

    function convertTo24HourFormat(time) {
      return moment(time, "hh:mm A").format("HH:mm");
  }
    
      
  };
    

    //////
    let queryExamResult = () => {

      // $("#QueryBtn").on("click", () => {
      //     $("#confirmationQueryModal").modal('show');
      // });
      let url = window.location.pathname;
      let ID = url.substring(url.lastIndexOf('/') + 1);
    
      let segments = url.split('/');
      let batch_no = segments[segments.length - 2];

     
      $("#AttendanceButton").on("click", () => {
          
         
          let editExamResult = `${host}/api/training/${batch_no}/${ID}/updateAttendance`;
          
          Submitted = "Submitted";
          userid = userID
        const data = {
  
          //atch_No : $('#formEndorseResult input[name="batch_no"]').val(),
          submitted_status: Submitted,
          userid : userid,
            };
          //console.log(data);
          let postData = $.ajax({
            url: editExamResult,
            method: "PUT",
            data: data,
          }) .done((res) => {
           console.log('data',res);
    
           setTimeout(() => {
  
            $("#success-modal").modal("show");
          
        }, 2000);
   
    
    
          })
          .catch((err) => {
            //console.log(err);
          });
          $.when(postData).done( (res) => {
          //  $('#berjaya-kemaskini-modal').modal('show');
            let url = window.location.pathname;
            let ID = url.substring(url.lastIndexOf('/') + 1);
            // window.location.href = `${host}/exam/endorseExamResult/${ID}`;
    
         })
         
    
    
        });
      

      
        
   }




   let StatusList = () => {

    let url = window.location.pathname;
    let ID = url.substring(url.lastIndexOf('/') + 1);
    let segments = url.split('/');
    let batch_no = segments[segments.length - 2];


    let getList = `${host}/api/training/${batch_no}/${ID}/adminStatusList`;
 
    $.ajax({
      url: getList,
      type: "GET",
      dataType: "JSON",
    })
      .done((res) => {
        //createTable(res.data);
        tableStatusList(res.statuslist);
        console.log(res.statuslist);
      })
      .catch((err) => {});






  let tableStatusList = (data) => {
    let senarai = $("#AdminStatusList").DataTable({
      data: data,
      //  order: [[4, "desc"]],
      columnDefs: [
        {
          targets: 0,
          data: null,
          searchable: false,
          orderable: false,
        },
        {
          targets: 1,
          data: "id_training_participant",
          visible: false,
          render: function (data, type, full, meta) {
            if (data === null) {
              return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
            }
            if (data !== null || data !== "") {
              return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
            }
          },
        },
        {
          targets: 2,
          className: 'text-left', 
          data: "fullname",
          render: function (data, type, full, meta) {
            if (data === null) {
              return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
            }
            if (data !== null || data !== "") {
              return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
            }
          },
        
        }, 
        {
          targets: 3,
          className: 'text-left', 
          data: "attend_count",
          render: function (data, type, full, meta) {
            if (data === null) {
              return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
            }
            if (data !== null || data !== "") {
              return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
            }
          },
        
        },
        {
          targets: 4,
          data: "total_attend",
          render: function (data, type, full, meta) {
            if (data === null) {
              return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
            }
        
            var attend_count = full.attend_count; // Assuming full object contains attend_count
        
            if (attend_count === null) {
              return `<td width="40%" align="left"><p class="table-content">-</p></td>`;
            }
        
            if (attend_count == data) {
              return `<td width="15%"><p class="active-status" style="text-align: center;">COMPLETED</p></td>`;
            } else if (attend_count < data) {
              return `<td width="15%"><p class="inactive-status" style="text-align: center;">INCOMPLETE</p></td>`;
            } else {
              return "-";
            }
          },
        },
        {
          targets: 5,
          data: "remarks",
          render: function (data, type, full, meta) {
            if (data === null || data === "") {
              return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
            } else {
              // Split the comma-separated remarks and create a list
              const remarkList = data.split(',').map(remark => `<p>${remark.trim()}</p>`).join('');
              return `<td align="left" width="40%">${remarkList}</td>`;
            }
          },
        },
       
    
      ],
      language: {
        info: "view _START_ to _END_ from _TOTAL_ entry",
        infoEmpty: "view 0 from 0 to 0 entry",
        lengthMenu: "view _MENU_ Entry",
        paginate: {
          next: "Next",
          previous: "Previous",
        },
        zeroRecords: "No record found",
      },
    });
  
    senarai
      .on("order.dt search.dt", function () {
        senarai
          .column(0, { search: "applied", order: "applied" })
          .nodes()
          .each(function (cell, i) {
            cell.innerHTML = i + 1;
          });
      })
      .draw();
  
    $("#filterStatus").on("change", (e) => {
      // console.log(e.target.value)
      senarai.column(5).search(e.target.value).draw();
    });
  
    $("#filterJenisKenderaan").on("change", (e) => {
      console.log(e.target.value);
      senarai.column(1).search(e.target.value).draw();
    });
  
    $("#search_input").on("keyup", () => {
      let searched = $("#search_input").val();
      // console.log(searched)
      senarai.search(searched).draw();
    });
  
    $("#resetFilter").on("click", () => {
      $("#filterStatus").val("").trigger("change");
      $("#filterJenisKenderaan").val("").trigger("change");
      $("#search_input").val("");
  
      senarai.search("").columns().search("").draw();
    });     
    
    };
   }
   
  