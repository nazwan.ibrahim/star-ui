$(function(){
    
    participantView();
   // tableView();
});



let participantView = () => {

    let url = window.location.pathname;
    let ID = url.substring(url.lastIndexOf('/') + 1);

    let segments = url.split('/');
    let batch_no = segments[segments.length - 2];
   

    let getList = `${host}/api/training/${batch_no}/${ID}/participantFeedbackView`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((res) => {
        if (res.status === 'success') {
           data = res.viewFeedback;
           console.log(data);
        //    $.each(data, function(index, value) {

        //     (value.training_name == null || value.training_name == '') ? $('#trainingName').text('n/a') : $('#trainingName').text(value.training_name);
           
        //    });
           tableView(data);
        } else {
            //console.error('Error:', res.message);
        }
    })
    .fail((err) => {
       // console.error('AJAX request failed:', err);
    });
};

let tableView = (data) => {
    reportingTrainingView = $("#feedbackView").DataTable({
        data: data,
        //responsive: true,
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                data: "question",
                render: function (data, type, full, meta) {
                   
                    if (data !== null || data !== "") {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                        } else{
                            return  `<td width="50%" align="center"><p class="table-content">-</p></td>`;
                            }
                   // return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                   
                }
            },

            {
                data: "feedback",
                render: function (data, type, full, meta) {
                    if (data !== null || data !== "") {
                    return  `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else{
                        return `<td width="50%" align="center"><p class="table-content">-</p></td>`;
                        }
                }
            },
           
           

        ],
        language: {
            info: "_START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
                next: "Next",
                previous: "Previous",
            },
            zeroRecords: "No record found",
        },
        
    });

    

    reportingTrainingView.on("order.dt search.dt", function () {
        reportingTrainingView
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        })
        .draw();

};


