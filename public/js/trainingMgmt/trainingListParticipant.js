/* Upcoming Training Section Start */
$(function() {
    trainingparticipantlist();
    trainingparticipantpaylist();

});

let trainingparticipantlist = () => {

    let url = window.location.pathname;
    let ID = url.substring(url.lastIndexOf('/') + 1);
    
    let getList = host + '/api/training/'+ ID +'/trainingManagementListParticipantTP';
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((res) => {
        console.log('API Response:', res);
        if (res.status === 'success') {
            tableTrainingList(res.trainingparticipantlist);
            console.log('Upcoming List:', res.trainingparticipantlist);
        } else {
            console.error('Error:', res.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let tableTrainingList = (data) => {

    if ($.fn.DataTable.isDataTable("#trainingparticipantlist")) {
        $("#trainingparticipantlist").DataTable().destroy();
    }

    let dataTable = $("#trainingparticipantlist").DataTable({
        data: data,
        responsive: true,
        dom: 'lfrtip', 
        ordering: false, 
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                $('input', column.header()).on('keyup change clear', function () {
                    if (column.search() !== this.value) {
                        // Use case-insensitive search for the "Status" column (index 5)
                        column.search(this.value, false, true).draw();
                    }
                });
            });
        },
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                data: "fullname",
                searchable: true,
                targets: [1],
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "ic_no",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "phone_no",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "email",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "id_approval_status",
                render: function (data, type, full, meta) {
                    if (data === 1) {
                        return `<td width="50%" align="center"><p class="null-value">Pending Approval</p></td>`;
                    } else if (data === 2){
                        return `<td width="50%" align="center"><p class="active-status">Approved</p></td>`;
                    } 
                    else if (data === 3){
                        return `<td width="50%" align="center"><p class="failed-status">Rejected</p></td>`;
                    } 
                    else {
                        return `<td width="50%" align="center">-</td>`;
                    }
                }
            },
            {
                data: "id",
                render: function (data, type, full, meta) {
                    let approveLink = "/training-partner/approve-training-participant/" + data;
                    let rejectLink = "/training-partner/reject-training-participant/" + data;
            
                    if (data !== null && data !== "") {
                        return `<div>
                            <a href="${approveLink}" class="btn btn-sm" onclick="return confirm('Are you sure you want to approve?')">
                                <i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i>
                            </a>
                            <a href="${rejectLink}" class="btn btn-sm" onclick="return confirm('Are you sure you want to reject?')">
                                <i class="fas fa-trash-alt" style="font-size: 18px; color: #3498DB;"></i>
                            </a>
                          </div>`;
                    } else {
                        return "-";
                    }
                }
            },
        ],
        lengthMenu: [5, 10, 15, 20],
        pageLength: 5,
        language: {
            info: "_START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
                next: "Next",
                previous: "Previous",
            },
            zeroRecords: "No record found",
        },
    });

    $('#trainingList').on('click', '.delete-training-button', function (e) {
        e.preventDefault();
        let trainingId = $(this).data('training-id');

        $.ajax({
            url: `/api/training/softDelete/${trainingId}`,
            type: "GET",
            dataType: "JSON",
        })
        .done((res) => {
            if (res.status === 'success') {
                dataTable.row($(this).closest('tr')).remove().draw();
                console.log('Training record marked as deleted successfully.');
        
                window.location.href = '/admin/training-management';
            } else {
                console.error('Error:', res.message);
            }
        })
        .fail((err) => {
            console.error('AJAX request failed:', err);
        });
    });
};
/* Training List Section End */


let trainingparticipantpaylist = () => {

    let url = window.location.pathname;
    let ID = url.substring(url.lastIndexOf('/') + 1);
    
    let getList = host + '/api/training/'+ ID +'/trainingManagementListParticipantPayTP';
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((res) => {
        console.log('API Response:', res);
        if (res.status === 'success') {
            tableTrainingPayList(res.trainingparticipantpaylist);
            console.log('Upcoming List:', res.trainingparticipantpaylist);
        } else {
            console.error('Error:', res.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let tableTrainingPayList = (data) => {

    if ($.fn.DataTable.isDataTable("#trainingparticipantpaylist")) {
        $("#trainingparticipantpaylist").DataTable().destroy();
    }
    let dataTable = $("#trainingparticipantpaylist").DataTable({
        data: data,
        responsive: true,
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                data: "fullname",
                searchable: true,
                targets: [1],
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "ic_no",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "phone_no",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "email",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "status_payment",
                render: function (data, type, full, meta) {
                    if (data === 0) {
                        return `<td width="50%" align="center"><p class="null-value">Pending Payment</p></td>`;
                    } else if (data === 1){
                        return `<td width="50%" align="center"><p class="active-status">Approved</p></td>`;
                    } 
                    else if (data === 3){
                        return `<td width="50%" align="center"><p class="failed-status">Rejected</p></td>`;
                    } 
                    else {
                        return `<td width="50%" align="center">-</td>`;
                    }
                }
            },
            {
                data: "id",
                render: function (data, type, full, meta) {
                    let approveLink = "/training-partner/approve-pay-training-participant/" + data;
                    let rejectLink = "/training-partner/reject-pay-training-participant/" + data;
            
                    if (data !== null && data !== "") {
                        return `<div>
                            <a href="${approveLink}" class="btn btn-sm" onclick="return confirm('Are you sure you want to approve?')">
                                <i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i>
                            </a>
                            <a href="${rejectLink}" class="btn btn-sm" onclick="return confirm('Are you sure you want to reject?')">
                                <i class="fas fa-trash-alt" style="font-size: 18px; color: #3498DB;"></i>
                            </a>
                          </div>`;
                    } else {
                        return "-";
                    }
                }
            },
        ],
        lengthMenu: [5, 10, 15, 20],
        pageLength: 5,
        language: {
            info: "_START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
                next: "Next",
                previous: "Previous",
            },
            zeroRecords: "No record found",
        },
    });

    $('#trainingList').on('click', '.delete-training-button', function (e) {
        e.preventDefault();
        let trainingId = $(this).data('training-id');

        $.ajax({
            url: `/api/training/softDelete/${trainingId}`,
            type: "GET",
            dataType: "JSON",
        })
        .done((res) => {
            if (res.status === 'success') {
                dataTable.row($(this).closest('tr')).remove().draw();
                console.log('Training record marked as deleted successfully.');
        
                window.location.href = '/admin/training-management';
            } else {
                console.error('Error:', res.message);
            }
        })
        .fail((err) => {
            console.error('AJAX request failed:', err);
        });
    });
};
/* Training List Section End */