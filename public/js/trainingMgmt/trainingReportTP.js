
$(function(){

  displayProfile();


});

function displayProfile(){

  let url = window.location.pathname;
  let segments = url.split('/');
  let batch_no = segments[segments.length - 2];

  let ID = url.substring(url.lastIndexOf('/') + 1);
   
    let userid = ID;
    let getUserInfo = `${host}/api/training/${batch_no}/${userid}/participantReportDetails`;

   // console.log(getUserInfo);

  $.ajax({

  url: getUserInfo,
    type: 'GET',
    contentType: "application/json; charset-utf-8",
    dataType: 'json',
    success: function (response) {
      let info = response.provider;
      let infoCourse = response.course;
      let infoReport = response.report;
      let infoExam = response.assessment;
      let infopicIC = response.picIC;
      console.log(info, 'info');

      $.each(info, function(index, value) {

       // (value.course_name == null || value.course_name == '') ? $('#trainingName').text('n/a') : $('#trainingName').text(value.course_name);
        (value.providername == null || value.providername == '') ? $('#providerCompany').text('n/a') : $('#providerCompany').text(value.providername);
        (value.providername == null || value.providername == '') ? $('#providerCompanyV').text('n/a') : $('#providerCompanyV').val(value.providername);
        
        (value.date_start == null || value.date_start == '') ? $('#dateStart').text('n/a') : $('#dateStart').text(value.date_start);
        (value.date_start == null || value.date_start == '') ? $('#dateStartV').text('n/a') : $('#dateStartV').val(value.date_start);

        
        (value.date_end == null || value.date_end == '') ? $('#dateEnd').text('n/a') : $('#dateEnd').text(value.date_end);
        (value.date_end == null || value.date_end == '') ? $('#dateEndV').text('n/a') : $('#dateEndV').val(value.date_end);

        
        (value.venue_address == null || value.venue_address == '') ? $('#Venueid').text('n/a') : $('#Venueid').text(value.venue_address);
        (value.venue_address == null || value.venue_address == '') ? $('#VenueidV').text('n/a') : $('#VenueidV').val(value.venue_address);

       // (value.trainincourse_nameg_name == null || value.course_name == '') ? $('#courseName').text('n/a') : $('#courseName').text(value.course_name);
        (value.training_code == null || value.training_code == '') ? $('#courseCode').text('n/a') : $('#courseCode').text(value.training_code);
        (value.training_code == null || value.training_code == '') ? $('#courseCodeV').text('n/a') : $('#courseCodeV').val(value.training_code);
        
        
      });
      $.each(infoCourse, function(index, value) {

        (value.course_name == null || value.course_name == '') ? $('#trainingName').text('n/a') : $('#trainingName').text(value.course_name);
        (value.course_name == null || value.course_name == '') ? $('#trainingNameV').text('n/a') : $('#trainingNameV').val(value.course_name);
        
       
        (value.course_name == null || value.course_name == '') ? $('#courseName').text('n/a') : $('#courseName').text(value.course_name);
        (value.course_name == null || value.course_name == '') ? $('#courseNameV').text('n/a') : $('#courseNameV').val(value.course_name);

        (value.batch_no_full == null || value.batch_no_full == '') ? $('#BatchNumber').text('n/a') : $('#BatchNumber').text(value.batch_no_full);
        (value.batch_no_full == null || value.batch_no_full == '') ? $('#BatchNumberV').text('n/a') : $('#BatchNumberV').val(value.batch_no_full);
        
       
        
      });
     
      $.each(infoReport, function(index, value) {

        //   (value.training_name == null || value.training_name == '') ? $('#courseName').text('n/a') : $('#courseName').text(value.training_name);
        //   (value.training_code == null || value.training_code == '') ? $('#courseCode').text('n/a') : $('#courseCode').text(value.training_code);
        //   (value.date_start == null || value.date_start == '') ? $('#dateStart2').text('n/a') : $('#dateStart2').text(value.date_start);
        //   (value.trainername == null || value.trainername == '') ? $('#CompetenttrainerName').text('n/a') : $('#CompetenttrainerName').text(value.trainername);
        //   (value.training_description == null || value.training_description == '') ? $('#training_description').text('n/a') : $('#training_description').text(value.training_description);
        //   (value.description == null || value.description == '') ? $('#description').text('n/a') : $('#description').text(value.description);
      });

      $.each(infoExam, function(index, value) {
         
        //   (value.participant_name == null || value.participant_name == '') ? $('#ParticipantName').text('n/a') : $('#ParticipantName').text(value.participant_name);
        //   (value.result_status_practical == null || value.result_status_practical == '') ? $('#PracticalResult').text('n/a') : $('#PracticalResult').text(value.result_status_practical);
        //   (value.result_status_theory == null || value.result_status_theory == '') ? $('#TheoryResult').text('n/a') : $('#TheoryResult').text(value.result_status_theory);
        //   (value.final_verdict_status == null || value.final_verdict_status == '') ? $('#FinalResult').text('n/a') : $('#FinalResult').text(value.final_verdict_status);
         
      });

      $.each(infopicIC, function(index, value) {
      if (!value.Icpic_path) {
        $('#ResultCoverImg').empty().append('<div class="text-center mt-2"> No image </div>');
      } else {
         $('#ResultCoverImg').html(`<a href="data:image/png;base64,${value.Icpic_path}" download="${value.Icpic_name}.png">${value.Icpic_name}.png</a>`);
      }

      if (!value.passportpic_path) {
        $('#ActivityCoverImg').empty().append('<div class="text-center mt-2"> No image </div>');
      } else {
         $('#ActivityCoverImg').html(`<a href="data:image/png;base64,${value.passportpic_path}" download="${value.passportpic_name}.png">${value.passportpic_name}.png</a>`);
      }
      });

      }
  });
}


///Thecodebelowdidnuseyet

  //////////
  function updateInfo(){
    //alert('jk');

    let url = window.location.pathname;
    let segments = url.split('/');
    let batch_no = segments[segments.length - 2];
  
    let ID = url.substring(url.lastIndexOf('/') + 1);
     
     // let userid = ID;
    
    let userid = userID;
   // alert(userid);
      let putUserInfo = `${host}/api/training/${batch_no}/${ID}/trainingReportTPInformation`;


  $(".form-list").each(function (i) {
    let form = new FormData();
   
    
    form.append("UserID", userid);
    form.append("trainingNameV", $("#trainingNameV").val());
    form.append("providerCompanyV", $("#providerCompanyV").val());
    form.append("dateStartV", $("#dateStartV").val());
    form.append("dateEndV", $("#dateEndV").val());

    form.append("VenueidV", $("#VenueidV").val());
    form.append("courseNameV", $("#courseNameV").val());
    form.append("courseCodeV", $("#courseCodeV").val());
    form.append("BatchNumberV", $("#BatchNumberV").val());
    form.append("Competent_trainer", $("#Competent_trainer").val());

    form.append("training_Synopsis", $("#training_Synopsis").val());
    form.append("training_Introduction", $("#training_Introduction").val());
    form.append("trainer", $("#trainer").val());
    form.append("training_conclusion", $("#training_conclusion").val());

    form.append("Createdby", userID);


    let fileInputResult = $("#uploadResult-" + i)[0];
    for (j = 0; j < fileInputResult.files.length; j++) {
      form.append("FailKeputusan[]", $("#uploadResult-" + i)[0].files[j]);
    }

    let fileInputReport = $("#uploadReport-" + i)[0];
    for (j = 0; j < fileInputReport.files.length; j++) {
      form.append("FailReport[]", $("#uploadReport-" + i)[0].files[j]);
    }

    let fileInputReceipt = $("#uploadReceipt-" + i)[0];
    for (j = 0; j < fileInputReceipt.files.length; j++) {
      form.append("FailResit[]", $("#uploadReceipt-" + i)[0].files[j]);
    }

    let fileInputActivity = $("#uploadActivity-" + i)[0];
    for (j = 0; j < fileInputReceipt.files.length; j++) {
      form.append("FailAktiviti[]", $("#uploadActivity-" + i)[0].files[j]);
    }

   


    console.log(...form);

    let postData = $.ajax({
      url: putUserInfo,
      type: "post",
      mimeType: "multipart/form-data",
      processData: false,
      contentType: false,
      async: false,
      data: form,
    }).done((res) => {
     
      $("#success-save-modal").modal({
        backdrop: 'static',
        keyboard: false,
        }).modal('show');

    }).fail((err) => {
      // Handle failure here
      console.error(err);
    }).always(() => {
    
    
      // location.replace(
      //       `${host}/training-partner/training-management/training-report/${batch_no}/${ID}`
      //     );

    });
 
})

// });
}

  //upload image

var maxFileSize = 8000000; //8mb
var isErrorFileType = false;
var isErrorFileSize = false;
var uploadedFilesArray = [];
var uploadedFilesArray1 = [];
var uploadedEditedFilesArray = [];
var dataForm = new FormData();






////////////////upload result
function onChangeUploadFileResult() {
  // reset
  isErrorFileType = false;
  isErrorFileSize = false;
  uploadedFilesArray = [];
  var allowedExtensions = ["jpg", "jpeg", "png", "img"];

  $.each($('#uploadResult-0')[0].files, function (index, val) {
    uploadedFilesArray.push(this);

    var fileExtension = val.name.split('.').pop().toLowerCase();
    var isValidExtension = allowedExtensions.includes(fileExtension);

    // if (val.type != "image/jpeg" && val.type != "image/jpg" && val.type != "image/png") {
    //   isErrorFileType = true;
    // }

    if (!isValidExtension) {
      isErrorFileType = true;
    }

    if (val.size > maxFileSize) {
      isErrorFileSize = true;
    }
  });

  if (isErrorFileType) {
    $('#error-uploadedImageInvalidType').show();
  } else {
    $('#error-uploadedImageInvalidType').hide();
  }

  if (isErrorFileSize) {
    $('#error-uploadedMaxSizeLimitReached').show();
  } else {
    $('#error-uploadedMaxSizeLimitReached').hide();
  }

  if ($('#uploadResult-0')[0].files[0]) {
    $('#error-uploadSedaFile').hide();
  }

  processUploadedResult();
}


function processUploadedResult() {
  if (!uploadedFilesArray.length) {

    // $('#error-uploadedPic').show();
    $('#parentDivUploadedFileDisplay-1').html('');
    $('#parentDivUploadedFileDisplay-1').hide();
    $('#btnuploadfileParentDiv-1').show();
  } else {

    $('#btnuploadfileParentDiv-1').hide();
    $('#parentDivUploadedFileDisplay-1').show();
    $('#parentDivUploadedFileDisplay-1').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-1').append(`
        <div class="book-cover-card-body">
          <div id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="upload result" height="160"></div>
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="padamFailResult(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}


function uploadButtonResult() {
  $('#uploadResult-0').val(null);
  $('#uploadResult-0').trigger('click');
}


function padamFailResult(fileIndex) {
  uploadedFilesArray.splice(fileIndex, 1);
  processUploadedResult();
}

// function processUploadedResult() {
//   if (!uploadedFilesArray.length) {

//     $('#error-uploadedGambarBilik').show();
//     $('#parentDivUploadedFileDisplay-1').html('');
//     $('#parentDivUploadedFileDisplay-1').hide();
//     $('#btnuploadfileParentDiv-0').show();
//   } else {

//     $('#btnuploadfileParentDiv-0').hide();
//     $('#parentDivUploadedFileDisplay-1').show();
//     $('#parentDivUploadedFileDisplay-1').html('');

//     for (var i = 0; i < uploadedFilesArray.length; i++) {
//       $('#parentDivUploadedFileDisplay-1').append(`
//         <div class="book-cover-card-body">
//           <div id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="Result" height="160"></div>
//           <div class="book-cover-card-body">
//             <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
//           </div>
//           <a  onclick="padamFailResult(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
//         </div>`);
//     }
//   }
//   if (uploadedFilesArray.length != 0) {
//     //reset data
//     dataForm = new FormData();

//     $.each(uploadedFilesArray, function (index, val) {
//       let file = val;
//       dataForm.append('file' + index, file);
//     });
//   }
// }

//////////////////////upload report///////////////////////////////

function onChangeUploadFileReport() {
  // reset
  isErrorFileType = false;
  isErrorFileSize = false;
  uploadedFilesArray = [];
  var allowedExtensions = ["jpg", "jpeg", "png", "img"];

  $.each($('#uploadReport-0')[0].files, function (index, val) {
    uploadedFilesArray.push(this);

    var fileExtension = val.name.split('.').pop().toLowerCase();
    var isValidExtension = allowedExtensions.includes(fileExtension);

    // if (val.type != "image/jpeg" && val.type != "image/jpg" && val.type != "image/png") {
    //   isErrorFileType = true;
    // }

    if (!isValidExtension) {
      isErrorFileType = true;
    }

    if (val.size > maxFileSize) {
      isErrorFileSize = true;
    }
  });

  if (isErrorFileType) {
    $('#error-uploadedImageInvalidType').show();
  } else {
    $('#error-uploadedImageInvalidType').hide();
  }

  if (isErrorFileSize) {
    $('#error-uploadedMaxSizeLimitReached').show();
  } else {
    $('#error-uploadedMaxSizeLimitReached').hide();
  }

  if ($('#uploadReport-0')[0].files[0]) {
    $('#error-uploadSedaFile').hide();
  }

  processUploadedReport();
}


function processUploadedReport() {
  if (!uploadedFilesArray.length) {

    // $('#error-uploadedPic').show();
    $('#parentDivUploadedFileDisplay-2').html('');
    $('#parentDivUploadedFileDisplay-2').hide();
    $('#btnuploadfileParentDiv-2').show();
  } else {

    $('#btnuploadfileParentDiv-2').hide();
    $('#parentDivUploadedFileDisplay-2').show();
    $('#parentDivUploadedFileDisplay-2').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-2').append(`
        <div class="book-cover-card-body">
          <div id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="Upload Report" height="160"></div>
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="padamFailReport(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}


function uploadButtonReport() {
  $('#uploadReport-0').val(null);
  $('#uploadReport-0').trigger('click');
}


function padamFailReport(fileIndex) {
  uploadedFilesArray.splice(fileIndex, 1);
  processUploadedReport();
}



/////////////////////end report///////////////////////////////



//////////////////////upload receipt///////////////////////////////

function onChangeUploadFileReceipt() {
  // reset
  isErrorFileType = false;
  isErrorFileSize = false;
  uploadedFilesArray = [];
  var allowedExtensions = ["jpg", "jpeg", "png", "img"];

  $.each($('#uploadReceipt-0')[0].files, function (index, val) {
    uploadedFilesArray.push(this);

    var fileExtension = val.name.split('.').pop().toLowerCase();
    var isValidExtension = allowedExtensions.includes(fileExtension);

    // if (val.type != "image/jpeg" && val.type != "image/jpg" && val.type != "image/png") {
    //   isErrorFileType = true;
    // }

    if (!isValidExtension) {
      isErrorFileType = true;
    }

    if (val.size > maxFileSize) {
      isErrorFileSize = true;
    }
  });

  if (isErrorFileType) {
    $('#error-uploadedImageInvalidType').show();
  } else {
    $('#error-uploadedImageInvalidType').hide();
  }

  if (isErrorFileSize) {
    $('#error-uploadedMaxSizeLimitReached').show();
  } else {
    $('#error-uploadedMaxSizeLimitReached').hide();
  }

  if ($('#uploadReport-0')[0].files[0]) {
    $('#error-uploadSedaFile').hide();
  }

  processUploadedReceipt();
}


function processUploadedReceipt() {
  if (!uploadedFilesArray.length) {

    // $('#error-uploadedPic').show();
    $('#parentDivUploadedFileDisplay-3').html('');
    $('#parentDivUploadedFileDisplay-3').hide();
    $('#btnuploadfileParentDiv-3').show();
  } else {

    $('#btnuploadfileParentDiv-3').hide();
    $('#parentDivUploadedFileDisplay-3').show();
    $('#parentDivUploadedFileDisplay-3').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-3').append(`
        <div class="book-cover-card-body">
          <div id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="Upload Report" height="160"></div>
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="padamFailReceipt(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}


function uploadButtonReceipt() {
  $('#uploadReceipt-0').val(null);
  $('#uploadReceipt-0').trigger('click');
}


function padamFailReceipt(fileIndex) {
  uploadedFilesArray.splice(fileIndex, 1);
  processUploadedReceipt();
}



/////////////////////end receipt///////////////////////////////


////////////upload activity
function onChangeUploadFileActivity() {
  // reset
  isErrorFileType = false;
  isErrorFileSize = false;
  uploadedFilesArray = [];
  var allowedExtensions = ["jpg", "jpeg", "png", "img"];

  $.each($('#uploadActivity-0')[0].files, function (index, val) {
    uploadedFilesArray.push(this);

    var fileExtension = val.name.split('.').pop().toLowerCase();
    var isValidExtension = allowedExtensions.includes(fileExtension);

    // if (val.type != "image/jpeg" && val.type != "image/jpg" && val.type != "image/png") {
    //   isErrorFileType = true;
    // }

    if (!isValidExtension) {
      isErrorFileType = true;
    }

    if (val.size > maxFileSize) {
      isErrorFileSize = true;
    }
  });

  if (isErrorFileType) {
    $('#error-uploadedImageInvalidType').show();
  } else {
    $('#error-uploadedImageInvalidType').hide();
  }

  if (isErrorFileSize) {
    $('#error-uploadedMaxSizeLimitReached').show();
  } else {
    $('#error-uploadedMaxSizeLimitReached').hide();
  }

  if ($('#uploadReport-0')[0].files[0]) {
    $('#error-uploadSedaFile').hide();
  }

  processUploadedActivity();
}


function processUploadedActivity() {
  if (!uploadedFilesArray.length) {

    // $('#error-uploadedPic').show();
    $('#parentDivUploadedFileDisplay-4').html('');
    $('#parentDivUploadedFileDisplay-4').hide();
    $('#btnuploadfileParentDiv-3').show();
  } else {

    $('#btnuploadfileParentDiv-4').hide();
    $('#parentDivUploadedFileDisplay-4').show();
    $('#parentDivUploadedFileDisplay-4').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-4').append(`
        <div class="book-cover-card-body">
          <div id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="Upload Report" height="160"></div>
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="padamFailActivity(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}


function uploadButtonActivity() {
  $('#uploadActivity-0').val(null);
  $('#uploadActivity-0').trigger('click');
}


function padamFailReceipt(fileIndex) {
  uploadedFilesArray.splice(fileIndex, 1);
  processUploadedReceipt();
}


// function processUploadedActivity() {
//   if (!uploadedFilesArray.length) {

//     $('#error-uploadedGambarBilik').show();
//     $('#parentDivUploadedFileDisplay-2').html('');
//     $('#parentDivUploadedFileDisplay-2').hide();
//     $('#btnuploadfileParentDiv-0').show();
//   } else {

//     $('#btnuploadfileParentDiv-0').hide();
//     $('#parentDivUploadedFileDisplay-2').show();
//     $('#parentDivUploadedFileDisplay-2').html('');

//     for (var i = 0; i < uploadedFilesArray.length; i++) {
//       $('#parentDivUploadedFileDisplay-2').append(`
//         <div class="book-cover-card-body">
//           <img id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="book cover" height="160">
//           <div class="book-cover-card-body">
//             <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
//           </div>
//           <a  onclick="padamFailActivity(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
//         </div>`);
//     }
//   }
//   if (uploadedFilesArray.length != 0) {
//     //reset data
//     dataForm = new FormData();

//     $.each(uploadedFilesArray, function (index, val) {
//       let file = val;
//       dataForm.append('file' + index, file);
//     });
//   }
// }

////////////////////////endactivity/////////////////////////////////
