
$(function(){

  displayProfile();


});

function displayProfile(){

  let url = window.location.pathname;
  let segments = url.split('/');
  let batch_no = segments[segments.length - 2];

  let ID = url.substring(url.lastIndexOf('/') + 1);
   
    let userid = ID;
    let getUserInfo = `${host}/api/training/${batch_no}/${userid}/paymentOutline`;

    // console.log(getUserInfo);

  $.ajax({

    url: getUserInfo,
      type: 'GET',
      contentType: "application/json; charset-utf-8",
      dataType: 'json',
      success: function (response) {
        let info = response.provider;
        let infoCourse = response.courseinfo;
        let infoExam = response.assessment;
        let paymentseda = response.paymenttoseda;
        // console.log('info', info);
        // console.log('paymentseda', paymentseda);
        // console.log('infoCourse', infoCourse);
  
        $.each(info, function(index, value) {
  
         // (value.course_name == null || value.course_name == '') ? $('#trainingName').text('n/a') : $('#trainingName').text(value.course_name);
          (value.providername == null || value.providername == '') ? $('#providerCompany').text('n/a') : $('#providerCompany').text(value.providername);
          (value.providerid == null || value.providerid == '') ? $('#companyID').text('n/a') : $('#companyID').val(value.providerid);
          
          (value.date_start == null || value.date_start == '') ? $('#dateStart').text('n/a') : $('#dateStart').text(value.date_start);
          (value.date_start == null || value.date_start == '') ? $('#dateStartV').text('n/a') : $('#dateStartV').val(value.date_start);
  
          
          (value.date_end == null || value.date_end == '') ? $('#dateEnd').text('n/a') : $('#dateEnd').text(value.date_end);
          (value.date_end == null || value.date_end == '') ? $('#dateEndV').text('n/a') : $('#dateEndV').val(value.date_end);
  
          
          (value.venue_address == null || value.venue_address == '') ? $('#Venueid').text('n/a') : $('#Venueid').text(value.venue_address);
          (value.venue_address == null || value.venue_address == '') ? $('#VenueidV').text('n/a') : $('#VenueidV').val(value.venue_address);
  
         // (value.trainincourse_nameg_name == null || value.course_name == '') ? $('#courseName').text('n/a') : $('#courseName').text(value.course_name);
          (value.training_code == null || value.training_code == '') ? $('#courseCode').text('n/a') : $('#courseCode').text(value.training_code);
          (value.training_code == null || value.training_code == '') ? $('#courseCodeV').text('n/a') : $('#courseCodeV').val(value.training_code);
          
          
        });
        $.each(infoCourse, function(index, value) {
  
          (value.course_name == null || value.course_name == '') ? $('#trainingName').text('n/a') : $('#trainingName').text(value.course_name);
          (value.id_training_course == null || value.id_training_course == '') ? $('#courseID').text('n/a') : $('#courseID').val(value.id_training_course);
          (value.trainingid == null || value.trainingid == '') ? $('#trainingID').text('n/a') : $('#trainingID').val(value.trainingid);
  
          (value.batch_no_full == null || value.batch_no_full == '') ? $('#BatchNumber').text('n/a') : $('#BatchNumber').text(value.batch_no_full);
          (value.batch_no_full == null || value.batch_no_full == '') ? $('#BatchNumberV').text('n/a') : $('#BatchNumberV').val(value.batch_no_full);
          
         
          
        });
       
  
        $.each(infoExam, function(index, value) {
           
          //   (value.participant_name == null || value.participant_name == '') ? $('#ParticipantName').text('n/a') : $('#ParticipantName').text(value.participant_name);
          //   (value.result_status_practical == null || value.result_status_practical == '') ? $('#PracticalResult').text('n/a') : $('#PracticalResult').text(value.result_status_practical);
          //   (value.result_status_theory == null || value.result_status_theory == '') ? $('#TheoryResult').text('n/a') : $('#TheoryResult').text(value.result_status_theory);
          //   (value.final_verdict_status == null || value.final_verdict_status == '') ? $('#FinalResult').text('n/a') : $('#FinalResult').text(value.final_verdict_status);
           
        });
  
        $.each(paymentseda, function(index, value) {

          (value.total_paid == null || value.total_paid == '') ? $('#TotalV').text('n/a') : $('#TotalV').val(value.total_paid);
  
          if (!value.invoice_upload_path || !value.invoice_upload_name || !value.invoice_upload_format) {
  
            $('#InvoiceCoverImg').empty().append('<div class="text-center mt-2"> No document </div>');
  
            } else {
            
              const mimeType = value.invoice_upload_format === 'pdf' ? 'application/pdf' : 'application/msword';
        
              $('#InvoiceCoverImg').html(`
  
                <a href="data:${mimeType};base64,${value.invoice_upload_path}" download="${value.invoice_upload_name}.${value.invoice_upload_format}">
                    ${value.invoice_upload_name}.${value.invoice_upload_format}
                </a>
  
              `);
            }
  
            if (!value.receipt_upload_path || !value.receipt_upload_name || !value.receipt_upload_format) {
  
              $('#ReceiptCoverImg').empty().append('<div class="text-center mt-2"> No document </div>');
    
              } else {
              
                const mimeType = value.receipt_upload_format === 'pdf' ? 'application/pdf' : 'application/msword';
          
                $('#ReceiptCoverImg').html(`
    
                  <a href="data:${mimeType};base64,${value.receipt_upload_path}" download="${value.receipt_upload_name}.${value.receipt_upload_format}">
                      ${value.receipt_upload_name}.${value.receipt_upload_format}
                  </a>
    
                `);
              }
    
  
        });
  
        }
    });

}


///Thecodebelowdidnuseyet

  //////////
  function updateInfo(){
    //alert('jk');

    let url = window.location.pathname;
    let segments = url.split('/');
    let batch_no = segments[segments.length - 2];
  
    let ID = url.substring(url.lastIndexOf('/') + 1);
     
     // let userid = ID;
    
    let userid = userID;
   // alert(userid);
      let putUserInfo = `${host}/api/training/${batch_no}/${ID}/savePaymentDocumentTP`;


  $(".form-list").each(function (i) {
    let form = new FormData();
   
    
    form.append("UserID", userid);
    form.append("courseID", $("#courseID").val());
    form.append("companyID", $("#companyID").val());
    form.append("trainingID", $("#trainingID").val());
    form.append("Createdby", userID);


    let fileInputReceipt = $("#uploadReceipt-" + i)[0];
    for (j = 0; j < fileInputReceipt.files.length; j++) {
      form.append("FileReceipt[]", $("#uploadReceipt-" + i)[0].files[j]);
    }


    console.log(...form);

    let postData = $.ajax({
      url: putUserInfo,
      type: "post",
      mimeType: "multipart/form-data",
      processData: false,
      contentType: false,
      async: false,
      data: form,
    }).done((res) => {
     
      $("#success-save-modal").modal({
        backdrop: 'static',
        keyboard: false,
        }).modal('show');

    }).fail((err) => {
      // Handle failure here
      console.error(err);
    }).always(() => {
    
    
      // location.replace(
      //       `${host}/training-partner/training-management/training-report/${batch_no}/${ID}`
      //     );

    });
 
})

// });
}

  //upload image

var maxFileSize = 8000000; //8mb
var isErrorFileType = false;
var isErrorFileSize = false;
var uploadedFilesArray = [];
var uploadedFilesArray1 = [];
var uploadedEditedFilesArray = [];
var dataForm = new FormData();








//////////////////////upload receipt///////////////////////////////

function onChangeUploadFileReceipt() {
  // reset
  isErrorFileType = false;
  isErrorFileSize = false;
  uploadedFilesArray = [];
  var allowedExtensions = ["jpg", "jpeg", "png", "img"];

  $.each($('#uploadReceipt-0')[0].files, function (index, val) {
    uploadedFilesArray.push(this);

    var fileExtension = val.name.split('.').pop().toLowerCase();
    var isValidExtension = allowedExtensions.includes(fileExtension);

    // if (val.type != "image/jpeg" && val.type != "image/jpg" && val.type != "image/png") {
    //   isErrorFileType = true;
    // }

    if (!isValidExtension) {
      isErrorFileType = true;
    }

    if (val.size > maxFileSize) {
      isErrorFileSize = true;
    }
  });

  if (isErrorFileType) {
    $('#error-uploadedImageInvalidType').show();
  } else {
    $('#error-uploadedImageInvalidType').hide();
  }

  if (isErrorFileSize) {
    $('#error-uploadedMaxSizeLimitReached').show();
  } else {
    $('#error-uploadedMaxSizeLimitReached').hide();
  }

  if ($('#uploadReceipt-0')[0].files[0]) {
    $('#error-uploadSedaFile').hide();
  }

  processUploadedReceipt();
}


function processUploadedReceipt() {
  if (!uploadedFilesArray.length) {

    // $('#error-uploadedPic').show();
    $('#parentDivUploadedFileDisplay-1').html('');
    $('#parentDivUploadedFileDisplay-1').hide();
    $('#btnuploadfileParentDiv-1').show();
  } else {

    $('#btnuploadfileParentDiv-1').hide();
    $('#parentDivUploadedFileDisplay-1').show();
    $('#parentDivUploadedFileDisplay-1').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-1').append(`
        <div class="book-cover-card-body">
          <div id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="Upload Receipt" height="160"></div>
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="deleteReceipt(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}


function uploadButtonReceipt() {
  $('#uploadReceipt-0').val(null);
  $('#uploadReceipt-0').trigger('click');
}


function deleteReceipt(fileIndex) {
  uploadedFilesArray.splice(fileIndex, 1);
  processUploadedReceipt();
}



/////////////////////end receipt///////////////////////////////

