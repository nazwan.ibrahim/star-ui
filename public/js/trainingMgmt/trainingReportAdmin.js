
$(function(){

  displayReport();


});

function displayReport(){

  let url = window.location.pathname;
  let segments = url.split('/');
  let batch_no = segments[segments.length - 2];

  let courseID = url.substring(url.lastIndexOf('/') + 1);
   
    //let userid = ID;
    let getUserInfo = `${host}/api/training/${batch_no}/${courseID}/ReportDetails`;

   // console.log(getUserInfo);

  $.ajax({

  url: getUserInfo,
    type: 'GET',
    contentType: "application/json; charset-utf-8",
    dataType: 'json',
    success: function (response) {
      let info = response.myreport;
      let myendorse = response.DoneEndorsed;
      // let infoReport = response.report;
      // let infoExam = response.assessment;
      // let infopicIC = response.picIC;
      console.log(info, 'info');
      console.log(myendorse, 'myendorse');

      $.each(info, function(index, value) {

        (value.training_name == null || value.training_name == '') ? $('#trainingName').text('n/a') : $('#trainingName').text(value.training_name);
        (value.provider_name == null || value.provider_name == '') ? $('#providerCompany').text('n/a') : $('#providerCompany').text(value.provider_name);
        (value.date_start == null || value.date_start == '') ? $('#dateStart').text('n/a') : $('#dateStart').text(value.date_start);
        (value.date_end == null || value.date_end == '') ? $('#dateEnd').text('n/a') : $('#dateEnd').text(value.date_end);
        (value.venue == null || value.venue == '') ? $('#Venueid').text('n/a') : $('#Venueid').text(value.venue);
        (value.training_code == null || value.training_code == '') ? $('#courseCode').text('n/a') : $('#courseCode').text(value.training_code);
        (value.batch_no_full == null || value.batch_no_full == '') ? $('#BatchNumber').text('n/a') : $('#BatchNumber').text(value.batch_no_full);
        (value.training_name == null || value.training_name == '') ? $('#courseName').text('n/a') : $('#courseName').text(value.training_name);
        (value.competent_trainer == null || value.competent_trainer == '') ? $('#CompetenttrainerName').text('n/a') : $('#CompetenttrainerName').text(value.competent_trainer);
        (value.synopsis == null || value.synopsis == '') ? $('#training_description').text('n/a') : $('#training_description').text(value.synopsis);
        (value.introduction == null || value.introduction == '') ? $('#training_introduction').text('n/a') : $('#training_introduction').text(value.introduction);
        (value.conclusion == null || value.conclusion == '') ? $('#training_conclusion').text('n/a') : $('#training_conclusion').text(value.conclusion);

      
        console.log('value.result_upload_path',value.result_upload_path);

       if (!value.result_upload_path || !value.result_upload_name || !value.result_upload_format) {
        $('#ExamResultCoverImg').empty().append('<div class="text-center mt-2"> No document </div>');
        } else {
        const mimeType = value.result_upload_format === 'pdf'
            ? 'application/pdf'
            : (value.result_upload_format === 'xlsx'
                ? 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                : 'application/msword');
    
        $('#ExamResultCoverImg').html(`
            <a href="data:${mimeType};base64,${value.result_upload_path}" download="${value.result_upload_name}.${value.result_upload_format}">
                ${value.result_upload_name}.${value.result_upload_format}
            </a>
        `);
       }
        // if (!value.result_upload_path || !value.result_upload_name || !value.result_upload_format) {

        //   $('#ExamResultCoverImg').empty().append('<div class="text-center mt-2"> No document </div>');

        //   } else {
          
        //     const mimeType = value.result_upload_format === 'pdf' ? 'application/pdf' : 'application/msword';
      
        //     $('#ExamResultCoverImg').html(`

        //       <a href="data:${mimeType};base64,${value.result_upload_path}" download="${value.result_upload_name}.${value.result_upload_format}">
        //           ${value.result_upload_name}.${value.result_upload_format}
        //       </a>

        //     `);
        //   }


          if (!value.report_upload_path || !value.report_upload_name || !value.report_upload_format) {

            $('#ReportCoverImg').empty().append('<div class="text-center mt-2"> No document </div>');
  
            } else {
            
              const mimeType = value.report_upload_format === 'pdf' ? 'application/pdf' : 'application/msword';
        
              $('#ReportCoverImg').html(`
  
                <a href="data:${mimeType};base64,${value.report_upload_path}" download="${value.report_upload_name}.${value.report_upload_format}">
                    ${value.report_upload_name}.${value.report_upload_format}
                </a>
  
              `);
            }


            if (!value.payment_upload_path || !value.payment_upload_name || !value.payment_upload_format) {

              $('#ReceiptCoverImg').empty().append('<div class="text-center mt-2"> No document </div>');
    
              } else {
              
                const mimeType = value.payment_upload_format === 'pdf' ? 'application/pdf' : 'application/msword';
          
                $('#ReceiptCoverImg').html(`
    
                  <a href="data:${mimeType};base64,${value.payment_upload_path}" download="${value.payment_upload_name}.${value.payment_upload_format}">
                      ${value.payment_upload_name}.${value.payment_upload_format}
                  </a>
    
                `);
              }
  

        if (!value.supportive_activity_path || !value.supportive_activity_name || !value.supportive_activity_format) {

          $('#ActivityCoverImg').empty().append('<div class="text-center mt-2"> No document </div>');

          } else {

          const mimeType = value.result_upload_format === 'pdf' ? 'application/pdf' : 'application/msword';
    
          $('#ActivityCoverImg').html(`

            <a href="data:${mimeType};base64,${value.supportive_activity_path}" download="${value.supportive_activity_name}.${value.supportive_activity_format}">
                ${value.supportive_activity_name}.${value.supportive_activity_format}
            </a>

          `);
        }
      });


      // $.each(myendorse, function(index, value) {

      //   console.log(value.fullname);
        (myendorse.fullname == null || myendorse.fullname == '') ? $('#EndorseBy').text('n/a') : $('#EndorseBy').text(myendorse.fullname);

        (myendorse.endorsed_date == null || myendorse.endorsed_date == '') ? $('#EndorseDate').text('n/a') : $('#EndorseDate').text(myendorse.endorsed_date);
      
    
      // });
     


      }
  });
}


