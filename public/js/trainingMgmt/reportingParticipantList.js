$(function(){
    participantList();
});

let participantList = () => {

    let url = window.location.pathname;
    let ID = url.substring(url.lastIndexOf('/') + 1);

    let getList = `${host}/api/training/${ID}/participantReportList`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((res) => {
        if (res.status === 'success') {
           data = res.participantlist;
           $.each(data, function(index, value) {

            (value.training_name == null || value.training_name == '') ? $('#trainingName').text('n/a') : $('#trainingName').text(value.training_name);
           
           });
            tableTrainingList(data);
        } else {
            console.error('Error:', res.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let tableTrainingList = (data) => {
    reportingTrainingList = $("#trainingReportList").DataTable({
        data: data,
        responsive: true,
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                data: "fullname",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
                data: "date_start",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "date_end",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "batch_no",
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
           
            {
                data: "id_users",
                render: function (data, type, full, meta) {
                    return  `
                    <td width="50%" align="center"><p class="table-content"><a href="${host}/training-partner/training-management/training-report/${full.batch_no}/${data}" class="btn btn-sm">
                    <button class="btn btn-sm icon-style" data-id="${data}" 
                    <i class="icon-sunting" data-id="${data}">Reporting Details</i>
                    </button></a></p></td>
              
                  `;
                    
                }
            },
        ],
        language: {
            info: "_START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
                next: "Next",
                previous: "Previous",
            },
            zeroRecords: "No record found",
        },
        
    });

    

    reportingTrainingList.on("order.dt search.dt", function () {
        reportingTrainingList
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        })
        .draw();

};
