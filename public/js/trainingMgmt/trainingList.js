/* Upcoming Training Section Start */
$(function() {
    trainingMgmtList();
});

let trainingMgmtList = () => {
    let getList = `${host}/api/training/trainingManagementList`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((res) => {
        console.log('API Response:', res);
            tableTrainingList(res.upcomingList);
            console.log('Upcoming List:', res.upcomingList);
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let tableTrainingList = (data) => {
    let dataTable = $("#upcomingList").DataTable({
        data: data,
        responsive: true,
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                data: "course_name",
                searchable: true,
                targets: [1],
                render: function (data, type, full, meta) {
                    let editLink = "/admin/approve-training-registration/" + full.id;
                    if (data) {
                      
                        return `<td width="50%" align="center"><p class="table-content hover-link"> <a href="${editLink}" class="btn btn-sm hover-link">
                       ${data}
                    </a></p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">-</div></td>';
                    }
                }
            },
            {
                data: "course_category",
                render: function (data, type, full, meta) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                }
            },
            {
                data: "training_type",
                render: function (data, type, full, meta) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                }
            },
            {
                data: "batch_no_full",
                render: function (data, type, full, meta) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                }
            },
            {
                data: "date_start",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "date_end",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "id_approval_status",
                render: function (data, type, full, meta) {
                    if (data === 1) {
                        return `<td width="50%" align="center"><p class="null-value">Pending Approval</p></td>`;
                    } else if (data === 2){
                        return `<td width="50%" align="center"><p class="active-status">Approved</p></td>`;
                    } 
                    else if (data === 3){
                        return `<td width="50%" align="center"><p class="failed-status">Rejected</p></td>`;
                    } 
                    else if (data === 4){
                        return `<td width="50%" align="left"><p class="draft-value">Draft</p></td>`;
                        // return `<td width="50%" align="left"><p class="reserved-status">Reserved</p></td>`;
                    } 
                    else {
                        return `<td width="50%" align="center">-</td>`;
                    }
                }
            },
            {
                data: "id",
                render: function (data, type, full, meta) {
                    let editLink = "/admin/edit-training-registration/" + data;
                    let deleteLink = `${host}/api/training/softDelete/${data}`;


                    {/* <a href="${showLink}" class="btn btn-sm">
                                <i class="fas fa-eye" style="font-size: 18px; color: #3498DB; display: none;"></i>
                            </a>
                            <a href="${listLink}" class="btn btn-sm">
                                <i class="fas fa-key" style="font-size: 18px; color: #3498DB; display: none;"></i>
                            </a> */}

                    if (full.id_approval_status === 1) {
                        return `<div>
                        <a href="${editLink}" class="btn btn-sm">
                            <i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i>
                        </a>
                        <a href="${deleteLink}" class="btn btn-sm">
                            <i class="fas fa-trash-alt" style="font-size: 18px; color: #3498DB;"></i>
                        </a>
                          </div>`;
                    } else if (full.id_approval_status === 2) {
                        return `<div>
                        <a href="${editLink}" class="btn btn-sm">
                            <i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i>
                        </a>
                        <a href="${participantlink}" class="btn btn-sm">
                            <i class="fas fa-key" style="font-size: 18px; color: #3498DB;"></i>
                            </a>
                       
                        </div>`;
                    } else if (full.id_approval_status === 3) {
                        return `<div>
                        <a href="${editLink}" class="btn btn-sm">
                            <i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i>
                        </a>
                       
                        </div>`;
                    }else if (full.id_approval_status === 4) {
                        return `<div>
                        <a href="${editLink}" class="btn btn-sm">
                            <i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i>
                        </a>
                       
                        </div>`;
                    }
                }
            }
            
        ],
        lengthMenu: [5, 10, 15, 20],
        pageLength: 5,
        language: {
            info: "_START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
                next: "Next",
                previous: "Previous",
            },
            zeroRecords: "No record found",
        },
    });

    $('#trainingList').on('click', '.delete-training-button', function (e) {
        e.preventDefault();
        let trainingId = $(this).data('training-id');

        $.ajax({
            url: `/api/training/softDelete/${trainingId}`,
            type: "GET",
            dataType: "JSON",
        })
        .done((res) => {
            if (res.status === 'success') {
                dataTable.row($(this).closest('tr')).remove().draw();
                console.log('Training record marked as deleted successfully.');
        
                window.location.href = '/admin/training-management';
            } else {
                console.error('Error:', res.message);
            }
        })
        .fail((err) => {
            console.error('AJAX request failed:', err);
        });
    });

    var viewReportButton = $('<button id="viewReportButton" class="circular-button" style="color: white"><i class="fas fa-plus"></i></button>');
    viewReportButton.on('click', function () {
        window.location.href = "/admin/training-registration";
    });

    viewReportButton.css({
        width: '37px',
        height: '37px',
        backgroundColor: '#3498DB',
        border: '2px solid #3498DB',
        borderRadius: '50%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        cursor: 'pointer',
        display: 'none',
    });

    /* viewReportButton.css('margin-left', '10px');
    $('.dataTables_filter').append(viewReportButton);

    let dropdown = $('<select id="dropdownList" class="filter-dropdown" style="width: auto;">' +
                '<option value="" disabled selected>Filter</option>' +
                '<option value="option1">Option 1</option>' +
                '<option value="option2">Option 2</option>' +
                '<option value="option3">Option 3</option>' +
                '</select>');
    $('.dataTables_filter').prepend(dropdown); */

    dataTable.on("order.dt search.dt", function () {
        dataTable
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        })
        .draw();
};

/* Upcoming Training Section End */


/* Training List Section Start */
$(function(){
    trainingMgmtList2();
});

let trainingMgmtList2 = () => {
    let getList = `${host}/api/training/trainingManagementList2`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((response) => {
        console.log('API Response:', response);
            tableTrainingList2(response.trainingList);
            console.log('Training List:', response.trainingList);
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
};

let tableTrainingList2 = (data) => {
    let dataTable = $("#trainingList").DataTable({
        data: data,
        responsive: true,
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                data: "course_name",
                searchable: true,
                targets: [1],
                render: function (data, type, full, meta) {
                    let editLink = "/admin/approve-training-registration/" + full.id;
                    if (data) {
                      
                        return `<td width="50%" align="center"><p class="table-content hover-link"> <a href="${editLink}" class="btn btn-sm hover-link">
                       ${data}
                    </a></p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "course_category",
                render: function (data, type, full, meta) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                }
            },
            {
                data: "training_type",
                render: function (data, type, full, meta) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                }
            },
            {
                data: "batch_no_full",
                render: function (data, type, full, meta) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                }
            },
            {
                data: "date_start",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "date_end",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                        return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                    }
                }
            },
            {
                data: "id_approval_status",
                render: function (data, type, full, meta) {
                    if (data === 1) {
                        return `<td width="50%" align="center"><p class="null-value">Pending Approval</p></td>`;
                    } else if (data === 2){
                        return `<td width="50%" align="center"><p class="active-status">Approved</p></td>`;
                    } 
                    else if (data === 3){
                        return `<td width="50%" align="center"><p class="failed-status">Rejected</p></td>`;
                    } 
                    else {
                        return `<td width="50%" align="center">-</td>`;
                    }
                }
            },
            {
                data: "id",
                render: function (data, type, full, meta) {
                    let participantlink = "/admin/training-management-participant-list/" + data;
                    let paylink = "/admin/training-management-participant-pay-list/" + data;


                    {/* <a href="${showLink}" class="btn btn-sm">
                                <i class="fas fa-eye" style="font-size: 18px; color: #3498DB; display: none;"></i>
                            </a>
                            <a href="${listLink}" class="btn btn-sm">
                                <i class="fas fa-key" style="font-size: 18px; color: #3498DB; display: none;"></i>
                            </a> */}

                     if (full.id_approval_status === 2) {
                        return `<div>
                       
                        <a href="${participantlink}" class="btn btn-sm">
                            <i class="fas fa-users" style="font-size: 18px; color: #3498DB;"></i>
                            </a>

                            <a href="${paylink}" class="btn btn-sm">
                            <i class="fas fa-bank" style="font-size: 18px; color: #3498DB;"></i>
                            </a>
                       
                        </div>`;
                    }
                }
            }
            
        ],
        lengthMenu: [5, 10, 15, 20],
        pageLength: 5,
        language: {
            info: "_START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
                next: "Next",
                previous: "Previous",
            },
            zeroRecords: "No record found",
        },
    });

    $('#page-length-container').html($('#upcomingList_paginate'));

    $('#trainingList').on('click', '.delete-training-button', function (e) {
        e.preventDefault();
        let trainingId = $(this).data('training-id');

        $.ajax({
            url: `/api/training/softDelete/${trainingId}`,
            type: "GET",
            dataType: "JSON",
        })
        .done((res) => {
            if (res.status === 'success') {
                dataTable.row($(this).closest('tr')).remove().draw();
                console.log('Training record marked as deleted successfully.');
        
                window.location.href = '/admin/training-management';
            } else {
                console.error('Error:', res.message);
            }
        })
        .fail((err) => {
            console.error('AJAX request failed:', err);
        });
    });

    var viewReportButton = $('<button id="viewReportButton" class="circular-button" style="color: white"><i class="fas fa-plus"></i></button>');
    viewReportButton.on('click', function () {
        window.location.href = "/admin/training-registration";
    });

    viewReportButton.css({
        width: '37px',
        height: '37px',
        backgroundColor: '#3498DB',
        border: '2px solid #3498DB',
        borderRadius: '50%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        cursor: 'pointer',
    });

    viewReportButton.css('margin-left', '10px');
    $('.dataTables_filter').append(viewReportButton);

    dataTable.on("order.dt search.dt", function () {
        dataTable
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        })
        .draw();
};

/* Training List Section End */