const mix = require('laravel-mix');
const glob = require('glob');


  // mix.js('resources/js/app.js', 'public/js');
   // mix.js('resources/js/core/app.js', 'public/js/core/')
   //    .js('resources/js/hello/hello.js', 'public/js/hello')

 
   function mixAssetsDir(query, cb) {
      (glob.sync('resources/' + query) || []).forEach(f => {
        f = f.replace(/[\\\/]+/g, '/');
        cb(f, f.replace('resources', 'public'));
      });
    }
  // script js
mixAssetsDir('js/core/**/*.js', (src, dest) => mix.scripts(src, dest));

// custom script js
mixAssetsDir('js/**/*.js', (src, dest) => mix.scripts(src, dest));

// custom script js for users
mixAssetsDir('assets/js/**/*.js', (src, dest) => mix.scripts(src, dest));


//css
// themes Core stylesheets
mixAssetsDir('sass/core/**/!(_)*.css', (src, dest) => mix.sass(src, dest.replace(/(\\|\/)sass(\\|\/)/, '$1css$2').replace(/\.css$/, '.css')));

// pages Core stylesheets
mixAssetsDir('sass/pages/**/!(_)*.css', (src, dest) => mix.sass(src, dest.replace(/(\\|\/)sass(\\|\/)/, '$1css$2').replace(/\.css$/, '.css')));

// Themescss task
mixAssetsDir('sass/plugins/**/!(_)*.css', (src, dest) => mix.sass(src, dest.replace(/(\\|\/)sass(\\|\/)/, '$1css$2').replace(/\.css$/, '.css')));

// Core stylesheets
mixAssetsDir('sass/themes/**/!(_)*.css', (src, dest) => mix.sass(src, dest.replace(/(\\|\/)sass(\\|\/)/, '$1css$2').replace(/\.css$/, '.css')));

// custom blank file for users
mixAssetsDir('assets/css/**/!(_)*.css', (src, dest) => mix.sass(src, dest.replace(/(\\|\/)css(\\|\/)/, '$1css$2').replace(/(\\|\/)css(\\|\/)/, '$1css$2').replace(/\.css$/, '.css')));

