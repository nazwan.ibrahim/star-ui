@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Hello')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
@endsection

@section('page-styles')
<style>
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .form-group {
        display: flex;
        align-items: center;
        /* Vertical alignment */
    }

    .label-group {
        text-align: left;
        color: #454545;
        font-family: Open Sans;
        font-size: 18px;
        font-style: normal;
        font-weight: 600;
    }

    .title-group {
        text-align: left;
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .span-group {
        color: #454545;
        font-family: Open Sans;
        font-size: 18px;
        font-style: normal;
        font-weight: 400;
    }

    .form-group {
        display: flex;
        align-items: center;
        /* Vertical alignment */
    }
</style>
@endsection

@section('content')
<main style="padding:50px;margin:50px;">
    <section class="header-top-margin">
        <div class="card" style="padding: 50px; margin: 50px;">
            {{-- <form method="POST" action="/submit">
    @csrf --}}

            <div class="form-group">
                <label class="font-title title-group mb-4" style="display: inline; font-weight: bold;">Personal Information</label>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Salutation :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->salutation }}</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Full Name :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->fullname }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Identity Type :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->identity_type }}</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">IC No / Passport No :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->ic_no }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Gender :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->user_gendername }}</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; display: inline; width: 500px;">Email Address :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->email }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Phone Number :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->phone_no }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Address Line 1 :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->address_1 }}</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Address Line 2 :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->address_2 }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">State :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->user_statename }}</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Postcode :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->postcode }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Country :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->user_countryname }}</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="nationality" style="font-weight: bold; width: 500px;">Nationality :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->nationality }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Certificate Number :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->certificate_no }}</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; display: inline; width: 500px;">Certificate Category :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->certificate_category }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="chooseFile" style="font-weight: bold; width: 500px;">Copy of IC :</label>
                        @if($viewUser && $viewUser->ICpic_path)
                        <a href="{{ asset('storage/' . $viewUser->ICpic_path) }}" target="_blank" style="margin-right: 6px;"><i class="fas fa-file-pdf"></i>
                            <i>{{ $viewUser->ICpic_name }}</i>
                        </a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group" style="margin-top: 2px; margin-bottom: 30px;">
                        <label class="label-group" for="chooseFile" style="font-weight: bold; width: 500px;">Passport Photo :</label>
                        <div style="display: flex; flex-direction: column; align-items: flex-start; margin-left: 15px;">
                            @if($viewUser && $viewUser->passportpic_path)
                            <img id="imagePreview" src="{{ asset('storage/' . $viewUser->passportpic_path) }}" alt="Passport Photo" style="max-width: 100%; max-height: 150px;">
                            @else
                            <img id="imagePreview" src="#" alt="Passport Photo" style="max-width: 100%; max-height: 150px; display: none;">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group" style="margin-top: 2px;">
                <label class="font-title title-group mb-4" style="font-weight: bold; display: inline;">Educational Information</label>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Higher Education Level :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->education_level }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Qualification :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->qualification }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Supporting Documents :</label>
                        <span class="span-group" style="margin-left: 15px;">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>File Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="fileList">
                                    @if ($viewUploadedFile)
                                    @foreach ($viewUploadedFile as $index => $file)
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $file->file_name }}</td>
                                        <td>
                                            <a href="{{ asset('storage/eduFiles/' . $file->file_name) }}" target="_blank" title="View">
                                                <i class="fas fa-file-pdf"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="3">No data available</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group" style="margin-top: 2px;">
                <label class="font-title title-group mb-4" style="font-weight: bold; display: inline;">Company Information</label>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Company Name :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->companyname }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Phone Number :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->companyphone_no }}</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Fax No :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->fax_no }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Address Line 1 :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->company_address_1 }}</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Address Line 2 :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->company_address_2 }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">State :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->company_state }}</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Postcode :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->postcode }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px;">
                        <label class="label-group" for="name" style="font-weight: bold; width: 500px;">Country :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->company_countryname }}</span>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group" style="margin-top: 2px; margin-bottom: 30px;">
                        <label class="label-group" l for="name" style="font-weight: bold; width: 500px;">Position :</label>
                        <span class="span-group" style="margin-left: 15px;">{{ $viewUser->position }}</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
{{-- <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script> --}}
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

@endsection

@section('page-scripts')
{{-- our own js --}}

@endsection