@extends('layouts.contentLayoutMaster')

{{-- Page Title --}}
@section('title', 'Edit User Management')

{{-- Vendor CSS --}}
@section('vendor-styles')
{{-- Add your vendor styles here --}}
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .centered-container {
        display: flex;
        justify-content: flex-start;
        align-items: center;
        max-width: 100%;
    }

    .header-top-margin {
        margin-top: 7%;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .title-margin {
        margin-top: 95px;
        padding-bottom: 15px;
        margin-bottom: 15px;
        margin-left: 100px;
        justify-content: flex-start;
        display: flex;
    }

    .field-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .checkbox-label {
        color: #454545;
        font-family: Inter;
        font-size: 18px;
        font-style: normal;
        font-weight: 500;
        line-height: 28px;
    }

    .checkbox-size {
        display: flex;
        width: 32px;
        height: 32px;
        justify-content: center;
        align-items: center;
        flex-shrink: 0;
    }

    .form-row {
        display: flex !important;
        width: 1200px;
    }

    .date-field-size {
        display: flex !important;
        width: 50%;
    }

    /* Widen the main container */
    .container {
        max-width: 100%;
        /* Adjust the width as needed */
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    /* Style for labels */
    .form-group label {
        display: block;
        text-align: left;
        /* Align labels to the left */
        margin-bottom: 5px;
        /* Add vertical gap between label and field */
    }

    ul {
        list-style-type: none;
        padding: 0;
    }

    li {
        text-align: left;
    }
</style>
@endsection

@section('content')
<main>
    <div class="header-top-margin">
        <h3 class="page-title items-align-center"><b>Personal Information test</b></h3>
        <div class="container">
            <div class="centered-container">
                <!-- Registration Form -->
                <form method="POST" action="{{ route('admin.updateUser', ['id' => $id]) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="salutation"><b>Salutation</b></label>
                            <select class="form-control" id="salutation" name="salutation">
                                <option value=""> -- Please Select -- </option>
                                @foreach($salutation as $salute)
                                <option value="{{ $salute->id }}" {{ $viewUser->salutation == $salute->salutation ? 'selected' : '' }}>{{ $salute->salutation }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="fullname"><b>Full Name</b><small class="text-muted"> (Name as per IC / Passport)</small></label>
                            <input type="text" class="form-control text-uppercase" oninput="updateInput(this)" id="fullname" name="fullname" placeholder="Please Enter FullName" value="{{ $viewUser->fullname ?? '' }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="identity"><b>Identity Type</b></label>
                            <select class="form-control" id="identity" name="identity">
                            <option value=""> -- Please Select -- </option>
                                @foreach($identity as $identity)
                                <option value="{{ $identity->id_identity }}" {{ $viewUser->identity_type == $identity->identity_type ? 'selected' : '' }}>{{ $identity->identity_type }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="ic_no"><b>Ic No / Passport Number</b></label>
                            <input type="text" class="form-control text-uppercase" id="ic_no" name="ic_no" placeholder="Please Enter IC" value="{{ $viewUser->ic_no ?? '' }}">

                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="nationality"><b>Nationality</b></label>
                            <select class="form-control" id="nationality" name="nationality" required>
                            <option value=""> -- Please Select -- </option>
                                @foreach($nationality as $nation)
                                <option value="{{ $nation->id_nationality }}" {{ $viewUser->nationality == $nation->nationality ? 'selected' : '' }}>{{ $nation->nationality }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="gender"><b>Gender</b></label>
                            <select class="form-control" id="gender" name="gender" required>
                            <option value=""> -- Please Select -- </option>
                                @foreach($genders as $gender)
                                <option value="{{ $gender->id }}" {{ $viewUser->user_gendername == $gender->name ? 'selected' : '' }}>{{ $gender->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="email"><b>Email</b></label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Please Enter Your Email" value="{{ $viewUser->email ?? '' }}" readonly>
                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="phone_no"><b>Phone Number</b></label>
                            <input type="number" class="form-control number" pattern="[0-9]*" id="phone_no" name="phone_no" placeholder="Please Enter Phone Number" value="{{ $viewUser->phone_no ?? '' }}">

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="address_1"><b>Address Line 1</b></label>
                            <input type="text" class="form-control" id="address_1" name="address_1" placeholder="Please Enter Address Line 1" value="{{ $viewUser->address_1 ?? '' }}">
                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="address_2"><b>Address Line 2</b></label>
                            <input type="text" class="form-control" id="address_2" name="address_2" placeholder="Please Enter Address Line 2" value="{{ $viewUser->address_2 ?? '' }}">

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="postcode"><b>Postcode</b></label>
                            <input type="number" class="form-control number" pattern="[0-9]*" id="postcode" name="postcode" placeholder="Please Enter Postcode" value="{{ $viewUser->postcode ?? '' }}">
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="city"><b>City</b></label>
                            <select class="form-control" id="city" name="city" required>
                            <option value=""> -- Please Select -- </option>
                                @foreach($cities as $city)
                                <option value="{{ $city->id }}" {{ $viewUser->user_cityname == $city->cityname ? 'selected' : '' }}>{{ $city->cityname }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="state"><b>State</b></label>
                            <select class="form-control" id="state" name="state" required>
                            <option value=""> -- Please Select -- </option>
                                @foreach($states as $state)
                                <option value="{{ $state->id }}" {{ $viewUser->user_statename == $state->name ? 'selected' : '' }}>{{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="country"><b>Country</b></label>
                            <select class="form-control" id="country" name="country" onchange="updateStates()" required>
                            <option value=""> -- Please Select -- </option>
                                @foreach($countries as $country)
                                <option value="{{ $country->id }}" {{ $viewUser->user_countryname == $country->name ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="passportpic_name"><b>Passport Photo</b></label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="passportpic_name" name="FailGambarPassport" onchange="previewImage(this)">
                                <label class="custom-file-label" for="passportpic_name">
                                    @if($viewUser && $viewUser->user_passportpic_name)
                                    {{ $viewUser->user_passportpic_name }}
                                    @else
                                    Choose file
                                    @endif
                                </label>

                                <ul>
                                    <li><small class="text-muted">Accepted file types: png, .jpeg, .jpg. Maximum file size: 5MB.</small></li>
                                    <li><small class="text-muted">Coloured passport-sized photograph (in the size of 3.5 cm x 5 cm)</small></li>
                                    <li><small class="text-muted">Passport picture size, Width: 35 mm, Height: 50 mm ; Resolution (DPI), 600</small></li>
                                </ul>
                            </div>
                            <div class="mt-2">
                                @if($viewUser && $viewUser->user_passportpic_path)
                                <img id="imagePreview" src="{{ asset('storage/' . $viewUser->user_passportpic_path) }}" alt="Passport Photo" style="max-width: 100%; max-height: 150px;">
                                @else
                                <img id="imagePreview" src="#" alt="Passport Photo" style="max-width: 100%; max-height: 150px; display: none;">
                                @endif
                            </div>
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="ICpic_name"><b>Scanned IC / Passport</b></label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="ICpic_name" name="FailGambarIC" onchange="updateFileName(this)">
                                <label class="custom-file-label" for="ICpic_name">
                                    Choose file
                                </label>
                                <small class="text-muted">Accepted file types: pdf, Maximum file size: 5MB.</small>
                            </div>
                            <div class="mt-2" style="display: flex; align-items: center;">
                                @if($viewUser && $viewUser->user_ICpic_path)
                                <a href="{{ asset('storage/' . $viewUser->user_ICpic_path) }}" target="_blank" style="margin-right: 6px;"><i class="fas fa-file-pdf"></i></a>
                                <i>{{ $viewUser->user_ICpic_name }}</i>
                                @endif
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h3 class="page-title items-align-center"><b>Educational Information</b></h3>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="education_level"><b>Higher Education Level</b></label>
                            <select class="form-control" id="education_level" name="education_level" required>
                            <option value=""> -- Please Select -- </option>
                                @foreach($education_level as $education_level)
                                <option value="{{ $education_level->id }}" {{ $viewUser->education_level == $education_level->education_level ? 'selected' : '' }}>{{ $education_level->education_level }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="qualification"><b>Qualification</b></label>
                            <input type="text" class="form-control" id="qualification" name="qualification" placeholder="Please Enter Qualification" value="{{ $viewUser->user_qualification ?? '' }}">

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="supportingDocument"><b>Supporting Documents</b></label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="edu_file" name="edu_file[]" onchange="updateFileName(this)" multiple>
                                <label class="custom-file-label" for="edu_file">Choose File</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>File Name</th>
                                        <th colspan='2'>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="fileList">
                                    @if ($viewUploadedFile)
                                    @foreach ($viewUploadedFile as $index => $file)
                                    <tr data-file-id="{{ $file->file_id }}">
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $file->file_name }}</td>
                                        <td>
                                            <a href="{{ asset('storage/eduFiles/' . $file->file_name) }}" target="_blank" title="View">
                                                <i class="fas fa-file-pdf"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" class="delete-file" title="Delete">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="4">No data available</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <hr>
                    <h3 class="page-title items-align-center"><b>Company Information</b></h3>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="companyname"><b>Company Name</b></label>
                            <input type="text" class="form-control" id="companyname" name="companyname" placeholder="Please Enter Company Name" value="{{ $viewUser->companyname ?? '' }}">

                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="position"><b>Position</b></label>
                            <input type="text" class="form-control" id="position" name="position" placeholder="Please Enter Postion" value="{{ $viewUser->position ?? '' }}">

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="companyphone_no"><b>Phone Number</b></label>
                            <input type="number" class="form-control number" pattern="[0-9]*" id="companyphone_no" name="companyphone_no" placeholder="Please Enter Company Phone" value="{{ $viewUser->companyphone_no ?? '' }}">

                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="companyfax_no"><b>Fax Number</b></label>
                            <input type="number" class="form-control number" pattern="[0-9]*" id="companyfax_no" name="companyfax_no" placeholder="Please Enter Company Fax" value="{{ $viewUser->companyfax_no ?? '' }}">

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="company_address_1"><b>Address Line 1</b></label>
                            <input type="text" class="form-control" id="company_address_1" name="company_address_1" placeholder="Please Enter Company Address 1" value="{{ $viewUser->company_address_1 ?? '' }}">

                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="company_address_2"><b>Address Line 2</b></label>
                            <input type="text" class="form-control" id="company_address_2" name="company_address_2" placeholder="Please Enter Company Address 2" value="{{ $viewUser->company_address_2 ?? '' }}">

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="company_postcode"><b>Postcode</b></label>
                            <input type="number" class="form-control number" id="company_postcode" pattern="[0-9]*" name="company_postcode" placeholder="Please Enter Postcode" value="{{ $viewUser->company_postcode ?? '' }}">
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="company_city"><b>City</b></label>
                            <select class="form-control" id="company_city" name="company_city" required>
                            <option value=""> -- Please Select -- </option>
                                @foreach($cities as $city)
                                <option value="{{ $city->id }}" {{ $viewUser->company_city == $city->cityname ? 'selected' : '' }}>{{ $city->cityname }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="company_state"><b>State</b></label>
                            <select class="form-control" id="company_state" name="company_state" required>
                            <option value=""> -- Please Select -- </option>
                                @foreach($states as $state)
                                <option value="{{ $state->id }}" {{ $viewUser->company_state == $state->name ? 'selected' : '' }}>{{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="company_country"><b>Country</b></label>
                            <select class="form-control" id="company_country" name="company_country" required>
                                <option value=""> -- Select a Country -- </option>
                                @foreach($countries as $country)
                                <option value="{{ $country->id }}" {{ $viewUser->company_country == $country->name ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <hr>
                    <!-- Submit Button -->
                    <div class="d-flex justify-content-end mt-4">
                        <div style="margin-right: 1%;">
                            <a href="{{ route('admin.user-management') }}" class="btn btn-secondary-6 btn-block">CANCEL</a>
                        </div>
                        <button type="submit" class="btn btn-primary" style="margin-right: 1%; color: white; width: 90px;" id="hantarBtn">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">SAVE</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection

@section('vendor-scripts')
{{-- Add your vendor scripts here --}}
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
@endsection

@section('page-scripts')
<script>
    let host = "{{ env('API_SERVER') }}";
</script>

<script>
    function previewImage(input) {
        var fileInput = input;
        var preview = document.getElementById('imagePreview');
        var label = document.querySelector('.custom-file-label');

        // Clear existing preview
        preview.style.display = 'none';
        preview.src = '';

        // Update label with the selected file name
        label.textContent = fileInput.files[0] ? fileInput.files[0].name : 'Choose file';

        // Show preview if an image file is selected
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                preview.src = e.target.result;
                preview.style.display = 'block';
            };

            reader.readAsDataURL(fileInput.files[0]);
        }
    }

    function updateFileName(input) {
        var label = input.nextElementSibling;
        var fileName = input.files[0] ? input.files[0].name : 'Choose File';
        label.textContent = fileName;
    }

    document.addEventListener('DOMContentLoaded', function() {
        var deleteButtons = document.querySelectorAll('.delete-file');

        deleteButtons.forEach(function(button) {
            button.addEventListener('click', function(event) {
                var fileEntryRow = event.target.closest('tr');
                var fileId = fileEntryRow.getAttribute('data-file-id');

                $.ajax({
                    type: 'POST',
                    url: '/delete-file',
                    data: {
                        _token: '{{ csrf_token() }}',
                        fileId: fileId
                    },
                    success: function(data) {

                        fileEntryRow.remove();

                    },
                    error: function(error) {
                        console.error('Failed to delete record: ', error);
                    },
                });
            });
        });
    });
</script>
<script>
    function updatePostcode() {
    var selectedPostcode = $('#postcode').val();

    $.ajax({
        url: '/get-postcode/' + selectedPostcode,
        type: 'GET',
        success: function (response) {
            console.log('RESPONSE', response);

            // Clear existing options
            $('#state').empty();
            $('#city').empty();
            $('#country').empty();

            // Append new options
            $('#state').append('<option value="' + response.state_id + '">' + response.state + '</option>');
            $('#city').append('<option value="' + response.city_id + '">' + response.cityname + '</option>');
            $('#country').append('<option value="' + response.country_id + '">' + response.country + '</option>');
        },
        error: function (error) {
            console.error('Error fetching postcode data:', error);
        }
    });
}


    function updateInput(input) {
        input.value = input.value.toUpperCase();
    }
</script>

@endsection