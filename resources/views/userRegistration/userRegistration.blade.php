@extends('layouts.contentLayoutLogin')
{{-- page Title --}}
@section('title','New Registration')
{{-- vendor css --}}
<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha384-KyZXEAg3QhqLMpG8r+R1fhFfMvVr8O4lFf81Z2F5g1pMquJuu1TC6Xc4rBxWm2Ir5"
crossorigin="anonymous"/>
@section('vendor-styles')

@endsection

@section('page-styles')
{{-- add css style here --}}
<style>
#main-conten1t {
  max-width: 1280px;
  margin: 100px auto;
}

/* Add some styling to the progress bullets */
#progress-bullets {
  list-style-type: none;
  padding: 0;
  display: flex;
  justify-content: center;
}

#progress-bullets li {
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background-color: #ccc;
  margin: 0 8px;
  cursor: pointer;
}

#progress-bullets li.active {
  background-color: #007bff; /* Change the color for the active page */
}


.noborderlogin{
    display: block;
    width: 500px;
    margin-top:100px;
    flex-direction: column;
    align-items: center;
    gap: var(--spacing-32, 32px);
    
    background: #fff; 
}

.borderlogin{
    display: block;
    width: 500px;
    margin-top:30px;
    margin-bottom:20px;
    flex-direction: column;
    align-items: center;
    gap: var(--spacing-32, 32px);
    border-radius: 17px;
    border: 1px solid var(--primary-500-base-color, #4B94D8);
    background: #fff; 
}

.title_header{
    color: #656565;
    text-align: center;
    font-family: Rajdhani;
    font-size: 32px;
    font-style: normal;
    font-weight: 700;
    line-height: 38px; /* 118.75% */
}
.title{
    color: #656565;
    text-align: center;
    font-family: Rajdhani;
    font-size: 32px;
    font-style: normal;
    font-weight: 700;
    line-height: 38px; /* 118.75% */
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .contentUpdate{
        color: #454545;
        font-family: Open Sans;
        font-size: 22px;
        font-style: normal;
        font-weight: 600;
        line-height: 28px; /* 155.556% */
        text-align: center;
    }

    .contentUpdate2{
        color: #454545;
        font-family: Open Sans;
        font-size: 18px;
        font-style: normal;
        font-weight: 600;
        line-height: 28px; /* 155.556% */
        text-align: left;
    }
   
</style>
@endsection

@section('content')
<body class="text-center">

<div class="loading-overlay" id="page_loader">
    <div class="loader"></div>
    <div class="loader-text"></div>
</div>
 
<!-- Page Content -->
<main class="form-signin" style="max-width: 600px !important;">
    <div class="noborderlogin"><p class="mt-3 mb-3 fw-bold page-title">SEDA Training and Registration System (STAR)</p></div>
        <div class=" borderlogin">
            <div style="margin:30px;">
                <form id="form_login" action="{{ route('createUserRegistration') }}" method="post" class="user-form">      

                    @csrf
                    <div class="col-lg-8" style="margin: auto">
                        <h4 class="mt-3 mb-3 fw-bold contentUpdate" style="margin-bottom: 2%;">Registration</h4>            
                    </div>
                    <div id="errorMessage" class="alert alert-danger" style="display: none;"></div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                   
                    <div class="form-section" id="page1">
                        <div class="mt-3">
                            <label for="floatingInput" style="margin-bottom: 2%;">Name (As in Mykad/Passport): </label>
                            <input type="text" oninput="updateInput(this)" class="form-control" style="margin-bottom: 2%;" id="fullname" name="fullname" placeholder="Full Name" required>
                        </div>
                        <div class="mt-3">
                            <label for="floatingInput" style="margin-bottom: 2%;">Identity Type:</label>
                            <select class="form-control" id="identity" aria-placeholder="Please Select" name="identity" required>
                                <option> -- Please Select -- </option>
                                @foreach($identity as $identity)
                                    <option value="{{ $identity->id_identity }}">{{ $identity->identity_type }}</option>
                                @endforeach
                            </select>                            
                        </div>
                        <div class="mt-3">
                            <label for="floatingInput" style="margin-bottom: 2%;">Mykad Number/Passport Number:</label>
                            <input type="text" class="form-control" style="margin-bottom: 2%;" id="ic_no" name="ic_no"  placeholder="Mykad / Passport Number" required>
                            
                        </div>

                        <div class="mt-3">
                            <label for="floatingInput" style="margin-bottom: 2%;">Phone Number:</label>
                            <input type="number" class="form-control number" style="margin-bottom: 2%;" id="phone_no" name="phone_no" pattern="[0-9]*"  placeholder="Phone Number" maxlength="11" minlength="10" required>
                            
                        </div>
                        <div>
                            <p class="mt-3">
                                <div class="small float-start">
                                    <label>
                                        Already have an account? <a href="{{ route('login') }}">Sign In</a>
                                    </label>
                                </div>
                            </p>
                            <br/>
                        </div>
                        <button type="button" class="w-100 btn btn-primary text-white fw-bold mt-3" id="nextButton">Next</button>
                    </div>


                    <div class="form-section" id="page2" style="display: none;">
                    
                        <div class="mt-3">
                            <label for="floatingInput" style="margin-bottom: 2%;">Email:</label>
                            <input type="text" class="form-control" style="margin-bottom: 2%;" id="email" name="email"  placeholder="Email" required>
                        </div>

                        <div class="mt-3">
                            <label for="floatingInput" style="margin-bottom: 2%;">Password:</label>
                            <div class="input-group">
                            <input type="password" class="form-control" style="margin-bottom: 2%;" id="password" name="password"  placeholder="Password" required oninput="validatePassword()">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-outline-secondary" onclick="togglePassword('password')">
                                        <i class="fas fa-eye" aria-hidden="true"></i>
                                    </button>
                                </div>                            
                            </div>
                        </div>


                        <div class="mt-3">
                            <label for="floatingInput" style="margin-bottom: 2%;">Confirm Password:</label>
                            <div class="input-group">
                            <input type="password" class="form-control" style="margin-bottom: 2%;" id="confirmpassword" name="confirmpassword"  placeholder="Confirm Password" required oninput="validatePassword()">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-outline-secondary" onclick="togglePassword('password')">
                                        <i class="fas fa-eye" aria-hidden="true"></i>
                                    </button>
                                </div>                            
                            </div>
                        </div>



                        <span id="password-error" class="text-danger"></span>
                        @if($errors->any('password'))
                        <span class="text-danger">{{$errors->first('password')}}</span>
                        @endif
                        <div>
                        <p class="mt-3">
                            <div class="small float-start">
                                <label>
                                    <input type="checkbox" id="terms" name="terms" value="1" required>
                                    By checking this box, you agree to our 
                                    <a href="https://www.seda.gov.my/terms" target="_blank">Terms &amp; Conditions</a> 
                                    and our 
                                    <a href="https://www.seda.gov.my/overview-of-seda" target="_blank">Privacy Policy</a>
                                </label>
                            </div>
                        </p>

                            <br/>
                        </div>  

                        <div>
                            <p class="mt-3">
                                <div class="small float-start">
                                    <label>
                                        Already have an account? <a href="{{ route('login') }}">Sign In</a>
                                    </label>
                                </div>
                            </p>
                            <br/>
                        </div>
                        <button type="button" class="w-100 btn btn-basic fw-bold mt-3" id="prevButton">Previous</button>
                        <button type="submit" class="w-100 btn btn-primary text-white fw-bold mt-3" id="submitButton">Submit</button> 
                    </div>
                </form>

                <div>
                    <!-- Progress Bullets -->
                    <p>
                        <ul id="progress-bullets">
                            <li class="active"></li>
                            <li></li>
                        </ul>
                    </p>
                </div>

            </div>
        </div>
</main>
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
    {{-- <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>  --}}
    {{-- <script src="{{ asset('js/vendors/jquery-3.7.1.min.js') }}"></script> --}}
    <script>
    // Get references to form sections and buttons
    const page1 = document.getElementById("page1");
    const page2 = document.getElementById("page2");
    const nextButton = document.getElementById("nextButton");
    const prevButton = document.getElementById("prevButton");
    const submitButton = document.getElementById("submitButton");
    const progressBullets = document.querySelectorAll("#progress-bullets li");

    let currentPage = 1;

    

    // Function to update progress bullets
    function updateProgressBullets() {
        progressBullets.forEach((bullet, index) => {
            if (index === currentPage - 1) {
                bullet.classList.add("active");
            } else {
                bullet.classList.remove("active");
            }
        });
    }
    

    function nextPage() {
        if (currentPage === 1) {
            // Check if page 1 is active
            // You can add additional validation here if needed
            const fullname = document.getElementById("fullname").value;
            const ic_no = document.getElementById("ic_no").value;
            const phone_no = document.getElementById("phone_no").value;

            if (!fullname || !ic_no || !phone_no) {
            // If any of the fields on page 1 is empty, display an error message
            errorMessage.textContent = "Please fill out all the fields.";
            errorMessage.style.display = "block";
            return;
        }
        }

        if (currentPage < 2) {
            currentPage++;
            page1.style.display = currentPage === 1 ? "block" : "none";
            page2.style.display = currentPage === 2 ? "block" : "none";
            nextButton.style.display = currentPage === 1 ? "block" : "none";
            prevButton.style.display = currentPage === 2 ? "block" : "none";
            submitButton.style.display = currentPage === 2 ? "block" : "none";

            updateProgressBullets();
        }
    }


    // Function to show the previous page and hide the current page
    function prevPage() {
        if (currentPage > 1) {
            currentPage--;
            page1.style.display = currentPage === 1 ? "block" : "none";
            page2.style.display = currentPage === 2 ? "block" : "none";
            nextButton.style.display = currentPage === 1 ? "block" : "none";
            prevButton.style.display = currentPage === 2 ? "block" : "none";
            submitButton.style.display = currentPage === 2 ? "block" : "none";

            updateProgressBullets();
        }
    }

    // Event listeners for the navigation buttons
    nextButton.addEventListener("click", nextPage);
    prevButton.addEventListener("click", prevPage);

</script>

<script>

function togglePassword(inputId) {
        var passwordInput = document.getElementById(inputId);
        if (passwordInput.type === "password") {
            passwordInput.type = "text";
            
        } else {
            passwordInput.type = "password";
        }
    }

    function validatePassword() {
        var passwordInput = document.getElementById('password');
        var otherPasswordInput = document.getElementById('confirmpassword');
        var passwordError = document.getElementById('password-error');
        
        // Check password requirements
        var regex = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]+$/;
        if (passwordInput.value.length < 8 || !regex.test(passwordInput.value)) {
            passwordError.textContent = 'Password must be at least 8 characters and contains symbol, number, and uppercase letter.';
            passwordInput.setCustomValidity('Invalid password');
        } else if (passwordInput.value !== otherPasswordInput.value) {
            passwordError.textContent = 'Passwords do not match.';
            passwordInput.setCustomValidity('Passwords do not match');
        }
         else {
            passwordError.textContent = '';
            passwordInput.setCustomValidity('');
        }
    }


</script>

<script>

function updateInput(input) {
        input.value = input.value.toUpperCase();
    }
</script>

@endsection
