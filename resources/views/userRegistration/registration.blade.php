@extends('layouts.contentLayoutMain')
{{-- page Title --}}
@section('title','Hello')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
{{-- add css style here --}}
@endsection

@section('content')
<body class="text-center">

<div class="loading-overlay" id="page_loader">
    <div class="loader"></div>
    <div class="loader-text"></div>
</div>

<main class="form-signin">
    <form id="form_login">
        <input type="hidden" name="_token" value="PjwUFBULkEBFHDwVncKTZ6k8oM55Zg8PAAtNUPjD">            <input type="hidden" id="event_id" value="">
        
        <a href="#"><img src="{{ asset('images/Big_title.png') }}"  alt="STAR" width="100%" height="100%"></a>
        <div class="col-lg-8" style="margin: auto">
       
        <h4 class="mt-3 mb-3 fw-bold card-title">Pendaftaran Pengguna</h4>
        
            
        </div>

        <div class="form-floating">
            <input type="text" class="form-control" id="user_id" name="user_id"  placeholder="MyKad atau E-mel">
            <label for="floatingInput">Nama: </label>
        </div>
        
        <div class="form-floating">
            <input type="text" class="form-control" id="user_id" name="user_id"  placeholder="MyKad atau E-mel">
            <label for="floatingInput">Emel:</label>
        </div>

        <div class="form-floating">
            <input type="text" class="form-control" id="user_id" name="user_id"  placeholder="MyKad atau E-mel">
            <label for="floatingInput">MyKad:</label>
        </div>

        <div class="form-floating">
            <input type="text" class="form-control" id="user_id" name="user_id"  placeholder="MyKad atau E-mel">
            <label for="floatingInput">Alamat: </label>
        </div>


        <div>        
            <button class=" w-75 fw-bold btn btn-primary btn-round text-white" id="btnRegister">Daftar</button>
        </div>
    </form>
</main>

  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
    {{-- <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>  --}}
    
@endsection

@section('page-scripts')
{{-- our own js --}}
<script src="{{asset('/js/hello2/hello2.js')}}"></script>
<script src="{{asset('/js/hello/hello.js')}}"></script>

@endsection
