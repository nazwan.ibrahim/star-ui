@extends('layouts.contentLayoutMaster')

{{-- Page Title --}}
@section('title', 'Edit User Management')

{{-- Vendor CSS --}}
@section('vendor-styles')
{{-- Add your vendor styles here --}}
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .centered-container {
        display: flex;
        justify-content: flex-start;
        align-items: center;
        max-width: 100%;
    }

    .header-top-margin {
        margin-top: 7%;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .title-margin {
        margin-top: 95px;
        padding-bottom: 15px;
        margin-bottom: 15px;
        margin-left: 100px;
        justify-content: flex-start;
        display: flex;
    }

    .field-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .checkbox-label {
        color: #454545;
        font-family: Inter;
        font-size: 18px;
        font-style: normal;
        font-weight: 500;
        line-height: 28px;
    }

    .checkbox-size {
        display: flex;
        width: 32px;
        height: 32px;
        justify-content: center;
        align-items: center;
        flex-shrink: 0;
    }

    .form-row {
        display: flex !important;
        width: 1200px;
    }

    .date-field-size {
        display: flex !important;
        width: 50%;
    }

    /* Widen the main container */
    .container {
        max-width: 100%;
        /* Adjust the width as needed */
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    /* Style for labels */
    .form-group label {
        display: block;
        text-align: left;
        /* Align labels to the left */
        margin-bottom: 5px;
        /* Add vertical gap between label and field */
    }
    ul {
        list-style-type: none;
        padding: 0;
    }

    li {
        text-align: left;
    }
</style>
@endsection

@section('content')
<main>
    <div class="header-top-margin">
        <h3 class="page-title items-align-center"><b>Personal Information</b></h3>
        <div class="container">
            <div class="centered-container">
                <!-- Registration Form -->
                <form method="POST" action="{{ route('admin.updateUser', ['id' => $id]) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="salutation"><b>Salutation</b></label>
                            <select class="form-control" id="salutation" name="salutation">
                                <option value=""> -- Please Select -- </option>
                                @foreach($salutation as $salute)
                                <option value="{{ $salute->id }}" {{ $viewUser->salutation == $salute->salutation ? 'selected' : '' }}>{{ $salute->salutation }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="fullname"><b>Full Name</b><small class="text-muted"> (Name as per IC / Passport)</small></label>
                            <input type="text" class="form-control text-uppercase" oninput="updateInput(this)" id="fullname" name="fullname" placeholder="Please Enter FullName" value="{{ $viewUser->fullname ?? '' }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="identity"><b>Identity Type</b></label>
                            <select class="form-control" id="identity" name="identity">
                                <option value=""> -- Please Select -- </option>
                                @foreach($identity as $identity)
                                <option value="{{ $identity->id_identity }}" {{ $viewUser->identity_type == $identity->identity_type ? 'selected' : '' }}>{{ $identity->identity_type }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="ic_no"><b>Ic No / Passport Number</b></label>
                            <input type="text" class="form-control" id="ic_no" name="ic_no" value="{{ $viewUser->ic_no ?? '' }}">

                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="nationality"><b>Nationality</b></label>
                            <select class="form-control" id="nationality" name="nationality" required>
                                <option value=""> -- Select a Nationality -- </option>
                                @foreach($nationality as $nation)
                                <option value="{{ $nation->id_nationality }}" {{ $viewUser->nationality == $nation->nationality ? 'selected' : '' }}>{{ $nation->nationality }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="gender"><b>Gender</b></label>
                            <select class="form-control" id="gender" name="gender">
                                <option value=""> -- Select a Gender -- </option>
                                @foreach($genders as $gender)
                                <option value="{{ $gender->id }}" {{ $viewUser->user_gendername == $gender->name ? 'selected' : '' }}>{{ $gender->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="email"><b>Email</b></label>
                            <input type="text" class="form-control" id="email" name="email" value="{{ $viewUser->email ?? '' }}">
                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="phone_no"><b>Phone Number</b></label>
                            <input type="number" class="form-control number" pattern="[0-9]*" id="phone_no" name="phone_no" value="{{ $viewUser->phone_no ?? '' }}">

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="address_1"><b>Address Line 1</b></label>
                            <input type="text" class="form-control" id="address_1" name="address_1" value="{{ $viewUser->address_1 ?? '' }}">
                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="address_2"><b>Address Line 2</b></label>
                            <input type="text" class="form-control" id="address_2" name="address_2" value="{{ $viewUser->address_2 ?? '' }}">

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="postcode"><b>Postcode</b></label>
                            <input type="number" class="form-control number" pattern="[0-9]*" id="postcode" name="postcode" value="{{ $viewUser->postcode ?? '' }}">
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="city"><b>City</b></label>
                            <select class="form-control" id="city" name="city">
                                <option value=""> -- Select a City -- </option>
                                @foreach($cities as $city)
                                <option value="{{ $city->id }}" {{ $viewUser->user_cityname == $city->cityname ? 'selected' : '' }}>{{ $city->cityname }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="state"><b>State</b></label>
                            <select class="form-control" id="state" name="state">
                                <option value=""> -- Select a State -- </option>
                                @foreach($states as $state)
                                <option value="{{ $state->id }}" {{ $viewUser->user_statename == $state->name ? 'selected' : '' }}>{{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="country"><b>Country</b></label>
                            <select class="form-control" id="country" name="country">
                                <option value=""> -- Select a Country -- </option>
                                @foreach($countries as $country)
                                <option value="{{ $country->id }}" {{ $viewUser->user_countryname == $country->name ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="passportpic_name"><b>Passport Photo</b></label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="passportpic_name" name="FailGambarPassport" onchange="previewImage(this)">
                                <label class="custom-file-label" for="passportpic_name">
                                    @if($viewUser && $viewUser->user_passportpic_name)
                                    {{ $viewUser->user_passportpic_name }}
                                    @else
                                    Choose file
                                    @endif
                                </label>
                                <ul>
                                    <li><small class="text-muted">Accepted file types: png, .jpeg, .jpg. Maximum file size: 5MB.</small></li>
                                    <li><small class="text-muted">Coloured passport-sized photograph (in the size of 3.5 cm x 5 cm)</small></li>
                                    <li><small class="text-muted">Passport picture size, Width: 35 mm, Height: 50 mm ; Resolution (DPI), 600</small></li>
                                </ul>
                            </div>
                           <div class="mt-2">
                                @if($viewUser && $viewUser->user_passportpic_path)
                                <img id="imagePreview" src="{{ asset('storage/' . $viewUser->user_passportpic_path) }}" alt="Passport Photo" style="max-width: 100%; max-height: 150px;">
                                @else
                                <img id="imagePreview" src="#" alt="Passport Photo" style="max-width: 100%; max-height: 150px; display: none;">
                                @endif
                            </div>
                            <small class="text-muted">Accepted file types: pdf, Maximum file size: 5MB.</small>

                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="ICpic_name"><b>Scanned IC / Passport</b></label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="ICpic_name" name="FailGambarIC" onchange="updateFileName(this)">
                                <label class="custom-file-label" for="ICpic_name">
                                    Choose file
                                </label>
                            </div>
                            <div class="mt-2" style="display: flex; align-items: center;">
                                @if($viewUser && $viewUser->user_ICpic_path)
                                <a href="{{ asset('storage/' . $viewUser->user_ICpic_path) }}" target="_blank" style="margin-right: 6px;"><i class="fas fa-file-pdf"></i></a>
                                <i>{{ $viewUser->user_ICpic_name }}</i>
                                @endif
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h3 class="page-title items-align-center"><b>Company Information</b></h3>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="companyname"><b>Company Name</b></label>
                            <input type="text" class="form-control" id="companyname" name="companyname" value="{{ $viewUser->companyname ?? '' }}">

                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="position"><b>Position</b></label>
                            <input type="text" class="form-control" id="position" name="position" value="{{ $viewUser->position ?? '' }}">

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="companyphone_no"><b>Phone Number</b></label>
                            <input type="number" class="form-control number" pattern="[0-9]*" id="companyphone_no" name="companyphone_no" value="{{ $viewUser->companyphone_no ?? '' }}">

                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="companyfax_no"><b>Fax Number</b></label>
                            <input type="number" class="form-control number" pattern="[0-9]*" id="companyfax_no" name="companyfax_no" placeholder="Please Enter Company Fax" value="{{ $viewUser->companyfax_no ?? '' }}">

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="company_address_1"><b>Address Line 1</b></label>
                            <input type="text" class="form-control" id="company_address_1" name="company_address_1" value="{{ $viewUser->company_address_1 ?? '' }}">

                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="company_address_2"><b>Address Line 2</b></label>
                            <input type="text" class="form-control" id="company_address_2" name="company_address_2" value="{{ $viewUser->company_address_2 ?? '' }}">

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="company_postcode"><b>Postcode</b></label>
                            <input type="number" class="form-control number" pattern="[0-9]*" id="company_postcode" name="company_postcode" placeholder="Please Enter Postcode" value="{{ $viewUser->company_postcode ?? '' }}">
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="company_city"><b>City</b></label>
                            <select class="form-control" id="company_city" name="company_city">
                                <option value=""> -- Select a City -- </option>
                                @foreach($cities as $city)
                                <option value="{{ $city->id }}" {{ $viewUser->company_city == $city->cityname ? 'selected' : '' }}>{{ $city->cityname }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="company_state"><b>State</b></label>
                            <select class="form-control" id="company_state" name="company_state">
                                <option value=""> -- Select a State -- </option>
                                @foreach($states as $state)
                                <option value="{{ $state->id }}" {{ $viewUser->company_state == $state->name ? 'selected' : '' }}>{{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="company_country"><b>Country</b></label>
                            <select class="form-control" id="company_country" name="company_country" onchange="updateStates()">
                                <option value=""> -- Select a Country -- </option>
                                @foreach($countries as $country)
                                <option value="{{ $country->id }}" {{ $viewUser->company_country == $country->name ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <hr>

                    @if(isset($viewMou) && count($viewMou) > 0)

                    <div id=mouPage>
                        @foreach ($viewMou as $mou)
                        <div class="mou-section" data-mou-id="{{ $mou->mou_id }}">
                            <h3 class="page-title items-align-center"><b>Memorandum of Understanding (Mou)</b></h3>
                            <div class="border rounded-lg p-3 mb-3">
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="mou_name"><b>Name</b></label>
                                        <input type="text" class="form-control" id="mou_name" name="mou_name[]" value="{{ $mou->mou_name ?? '' }}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="mou_start_date"><b>Start Date</b></label>
                                        <input type="date" class="form-control" id="mou_start_date" name="mou_start_date[]" value="{{ $mou->mou_start_date ?? '' }}">
                                    </div>
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="mou_end_date"><b>End Date</b></label>
                                        <input type="date" class="form-control" id="mou_end_date" name="mou_end_date[]" value="{{ $mou->mou_end_date ?? '' }}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="mou_file"><b>Supporting Documents</b></label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="mou_file" name="mou_file[]" onchange="updateFileName(this)" multiple>
                                            <label class="custom-file-label" for="loa_file">Choose File</label>
                                        </div>
                                        <div class="form-group" style="margin-top:10px;">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>File Name</th>
                                                        <th colspan='2'>Action</th>
                                                    </tr>
                                                </thead>
                                                @if(isset($mou))
                                                <tbody id="fileList_{{ $mou->mou_id }}">
                                                    @php
                                                    $filesForMou = $viewUploadedFileMou->where('id', $mou->mou_id);
                                                    $fileCount = 0;
                                                    @endphp

                                                    @if ($filesForMou->isNotEmpty())
                                                    @foreach ($filesForMou as $fileEntry)
                                                    <tr data-file-id="{{ $fileEntry->file_id }}">
                                                        <td>{{ ++$fileCount }}</td>
                                                        <td>{{ $fileEntry->file_name }}</td>
                                                        <td>
                                                            <a href="{{ asset('storage/' . $fileEntry->file_path) }}" target="_blank" title="View">
                                                                <i class="fas fa-file-pdf"></i>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0);" class="delete-file" title="Delete">
                                                                <i class="fas fa-trash-alt"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr>
                                                        <td colspan="4">No data available</td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                                @endif
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="mou_remarks"><b>Remark</b></label>
                                        <input type="text" class="form-control" id="mou_remarks" name="mou_remarks[]" value="{{ $mou->mou_remarks ?? '' }}">
                                    </div>
                                    <input type="hidden" name="mou_id[]" value="{{ $mou->mou_id }}">
                                </div>
                                <button type="button" class="btn btn-danger removeMou">Remove</button>
                            </div>

                        </div>
                        @endforeach
                        <button type="button" id="addMou" class="btn btn-primary">New MOU</button>
                        <hr>
                    </div>

                    @else
                    <div id=mouPage>
                        <div class="mou-section">
                            <h3 class="page-title items-align-center"><b>Memorandum of Understanding (Mou)</b></h3>
                            <div class="border rounded-lg p-3 mb-3">
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="mou_name"><b>Name</b></label>
                                        <input type="text" class="form-control" id="mou_name" name="mou_name[]" value="{{ $mou->mou_name ?? '' }}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="mou_start_date"><b>Start Date</b></label>
                                        <input type="date" class="form-control" id="mou_start_date" name="mou_start_date[]" value="{{ $mou->mou_start_date ?? '' }}">
                                    </div>
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="mou_end_date"><b>End Date</b></label>
                                        <input type="date" class="form-control" id="mou_end_date" name="mou_end_date[]" value="{{ $mou->mou_end_date ?? '' }}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="mou_file"><b>Supporting Documents</b></label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="mou_file" name="mou_file[]" onchange="updateFileName(this)" multiple>
                                            <label class="custom-file-label" for="loa_file">Choose File</label>
                                        </div>
                                        <div class="form-group" style="margin-top:10px;">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>File Name</th>
                                                        <th colspan='2'>Action</th>
                                                    </tr>
                                                </thead>
                                                @if (isset($mou))
                                                <tbody id="fileList_{{ $mou->mou_id }}">
                                                    @php
                                                    $filesForMou = $viewUploadedFileMou->where('id', $mou->mou_id);
                                                    $fileCount = 0;
                                                    @endphp

                                                    @if ($filesForMou->isNotEmpty())
                                                    @foreach ($filesForMou as $fileEntry)
                                                    <tr>
                                                        <td>{{ ++$fileCount }}</td>
                                                        <td>{{ $fileEntry->file_name }}</td>
                                                        <td>
                                                            <a href="{{ asset('storage/' . $fileEntry->file_path) }}" target="_blank" title="View">
                                                                <i class="fas fa-file-pdf"></i>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="" title="Download">
                                                                <i class="fas fa-trash-alt"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr>
                                                        <td colspan="4">No data available</td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                                @endif
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="mou_remarks"><b>Remark</b></label>
                                        <input type="text" class="form-control" id="mou_remarks" name="mou_remarks[]" value="{{ $mou->mou_remarks ?? '' }}">
                                    </div>
                                    @if (isset($mou))
                                    <input type="hidden" name="mou_id[]" value="{{ $mou->mou_id }}">
                                    @endif
                                </div>
                                <button type="button" class="btn btn-danger removeMou">Remove</button>
                            </div>
                        </div>
                        <button type="button" id="addMou" class="btn btn-primary">New MOU</button>
                        <hr>
                    </div>
                    @endif

                    @if(isset($viewLoa) && count($viewLoa) > 0)

                    <div id=loaPage>
                        @foreach ($viewLoa as $loa)
                        <div class="loa-section" data-loa-id="{{ $loa->loa_id }}">
                            <h3 class="page-title items-align-center"><b>Letter of Approval (LoA)</b></h3>
                            <div class="border rounded-lg p-3 mb-3">
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_name"><b>Name</b></label>
                                        <input type="text" class="form-control" id="loa_name" name="loa_name[]" value="{{ $loa->loa_name ?? '' }}">
                                    </div>
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_course"><b>Course</b></label>
                                        <input type="text" class="form-control" id="loa_course" name="loa_course[]" value="{{ $loa->loa_course ?? '' }}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_start_date"><b>Start Date</b></label>
                                        <input type="date" class="form-control" id="loa_start_date" name="loa_start_date[]" value="{{ $loa->loa_start_date ?? '' }}">
                                    </div>
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_end_date"><b>End Date</b></label>
                                        <input type="date" class="form-control" id="loa_end_date" name="loa_end_date[]" value="{{ $loa->loa_end_date ?? '' }}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_audit_facilities"><b>Audit Facilities</b>
                                            <input type="checkbox" class="form-check" name="loa_audit_facilities[]" value="1" {{ $loa->loa_audit_facilities ? 'checked' : '' }}>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_file"><b>Supporting Documents</b></label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="loa_file" name="loa_file[]" onchange="updateFileName(this)" multiple>
                                            <label class="custom-file-label" for="loa_file">Choose File</label>
                                        </div>
                                        <div class="form-group" style="margin-top:10px;">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>File Name</th>
                                                        <th colspan='2'>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="fileList_{{ $loa->loa_id }}">
                                                    @php
                                                    $filesForLoa = $viewUploadedFileLoa->where('id', $loa->loa_id);
                                                    $fileCount = 0;
                                                    @endphp

                                                    @if ($filesForLoa->isNotEmpty())
                                                    @foreach ($filesForLoa as $fileEntry)
                                                    <tr data-file-id="{{ $fileEntry->file_id }}">
                                                        <td>{{ ++$fileCount }}</td>
                                                        <td>{{ $fileEntry->file_name }}</td>
                                                        <td>
                                                            <a href="{{ asset('storage/' . $fileEntry->file_path) }}" target="_blank" title="View">
                                                                <i class="fas fa-file-pdf"></i>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0);" class="delete-file" title="Delete">
                                                                <i class="fas fa-trash-alt"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr>
                                                        <td colspan="4">No data available</td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_remarks"><b>Remark</b></label>
                                        <input type="text" class="form-control" id="loa_remarks" name="loa_remarks[]" value="{{ $loa->loa_remarks ?? '' }}">
                                    </div>
                                    <input type="hidden" name="loa_id[]" value="{{ $loa->loa_id }}">
                                </div>
                                <button type="button" class="btn btn-danger removeLoa">Remove</button>
                            </div>

                        </div>

                        @endforeach
                        <button type="button" id="addLoa" class="btn btn-primary">New LOA</button>
                        <hr>
                    </div>

                    @else
                    <div id=loaPage>
                        <div class="loa-section">
                            <h3 class="page-title items-align-center"><b>Letter of Approval (LoA)</b></h3>
                            <div class="border rounded-lg p-3 mb-3">
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_name"><b>Name</b></label>
                                        <input type="text" class="form-control" id="loa_name" name="loa_name[]" value="{{ $loa->loa_name ?? '' }}">
                                    </div>
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_course"><b>Course</b></label>
                                        <input type="text" class="form-control" id="loa_course" name="loa_course[]" value="{{ $loa->loa_course ?? '' }}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_start_date"><b>Start Date</b></label>
                                        <input type="date" class="form-control" id="loa_start_date" name="loa_start_date[]" value="{{ $loa->loa_start_date ?? '' }}">
                                    </div>
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_end_date"><b>End Date</b></label>
                                        <input type="date" class="form-control" id="loa_end_date" name="loa_end_date[]" value="{{ $loa->loa_end_date ?? '' }}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_audit_facilities"><b>Audit Facilities</b>
                                            <input type="checkbox" class="form-check" name="loa_audit_facilities[]" value="1">
                                        </label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_file"><b>Supporting Documents</b></label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="loa_file" name="loa_file[]" onchange="updateFileName(this)" multiple>
                                            <label class="custom-file-label" for="loa_file">Choose File</label>

                                        </div>
                                        <div class="form-group" style="margin-top:10px;">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>File Name</th>
                                                        <th colspan='2'>Action</th>
                                                    </tr>
                                                </thead>
                                                @if (isset($loa))

                                                <tbody id="fileList_{{ $loa->loa_id }}">
                                                    @php
                                                    $filesForLoa = $viewUploadedFileLoa->where('id', $loa->loa_id);
                                                    $fileCount = 0;
                                                    @endphp

                                                    @if ($filesForLoa->isNotEmpty())
                                                    @foreach ($filesForLoa as $fileEntry)
                                                    <tr>
                                                        <td>{{ ++$fileCount }}</td>
                                                        <td>{{ $fileEntry->file_name }}</td>
                                                        <td>
                                                            <a href="{{ asset('storage/' . $fileEntry->file_path) }}" target="_blank" title="View">
                                                                <i class="fas fa-file-pdf"></i>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="" title="Download">
                                                                <i class="fas fa-trash-alt"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr>
                                                        <td colspan="4">No data available</td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                                @endif
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6 py-2">
                                        <label class="field-title" for="loa_remarks"><b>Remark</b></label>
                                        <input type="text" class="form-control" id="loa_remarks" name="loa_remarks[]" value="{{ $loa->loa_remarks ?? '' }}">
                                    </div>
                                    @if (isset($loa))
                                    <input type="hidden" name="loa_id[]" value="{{ $loa->loa_id }}">
                                    @endif
                                </div>
                                <button type="button" class="btn btn-danger removeLoa">Remove</button>
                            </div>
                        </div>
                        <button type="button" id="addLoa" class="btn btn-primary">New LOA</button>
                        <hr>
                    </div>
                    @endif

                    <h2 class="page-title">List of Trainer</h2>
                    <div id="userIdAttribute" data-user-id="{{ $viewUser->user_id }}" style="display: none;"></div>
                    <div id="addForm" class="border rounded-lg p-3" style="display: none;">
                        <div class="form-row">
                            <div class="form-group col-md-6 py-2">
                                <label class="field-title" for="loa_name"><b>Trainer name</b></label>
                                <select class="form-control" id="trainer_name" name="trainer_name[]">
                                    <option value=""> -- Select Trainer -- </option>
                                    @foreach($trainers as $trainer)
                                    <option value="{{ $trainer->id }}" data-email="{{ $trainer->email }}" data-position="{{ $trainer->position }}" data-status="{{ $trainer->status }}">{{ $trainer->fullname }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6 py-2">
                                <label class="field-title" for="loa_course"><b>Email Address</b></label>
                                <input type="text" class="form-control" id="trainer_email" name="trainer_email[]" value="{{ $trainer->trainer_email ?? '' }}" disabled>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6 py-2">
                                <label class="field-title" for="loa_name"><b>Position</b></label>
                                <input type="text" class="form-control" id="trainer_position" name="trainer_position[]" value="{{ $trainer->trainer_position ?? '' }}" disabled>
                            </div>
                            <div class="form-group col-md-6 py-2">
                                <label class="field-title" for="loa_course"><b>Status</b></label>
                                <input type="text" class="form-control" id="trainer_status" name="trainer_status[]" value="{{ $trainer->trainer_status ?? '' }}" disabled>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center align-items-center pt-3 pb-2">
                            <button type="button" class="btn btn-primary" id="saveTrainer">Add</button>
                        </div>
                    </div>


                    <div>
                        <div style="text-align: right; margin-bottom: 10px; margin-top: 10px;">
                            <button type="button" class="btn btn-primary rounded-circle" id="addTrainer"><i class="fas fa-plus"></i></button>
                        </div>
                        <table id="tptable" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Position</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>

                                </tr>
                            </thead>

                            <tbody style="text-align: left;width:100%;" id="tpTableBody">
                                @if ($viewTrainer->isEmpty())
                                <tr>
                                    <td colspan="7">No data available</td>
                                </tr>
                                @else
                                @foreach ($viewTrainer as $trainer)
                                <tr class="tp-row">
                                    <td class="row-number">{{ $loop->index + 1 }}</td>
                                    <td>
                                        {{ $trainer->trainer_name }}
                                    </td>
                                    <td>
                                        {{ $trainer->trainer_email }}
                                    </td>
                                    <td>
                                        {{ $trainer->trainer_position }}
                                    </td>
                                    <td>
                                        @if($trainer->trainer_status == 1)
                                        Active
                                        @else
                                        Inactive
                                        @endif
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger remove-btn" data-trainer-id="{{ $trainer->trainer_id }}">Remove</button>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            
                        </table>
                    </div>
                    <!-- Submit Button -->
                    <div class="d-flex justify-content-end mt-4">
                        <div style="margin-right: 1%;">
                            <a href="{{ route('admin.user-management') }}" class="btn btn-secondary-6 btn-block">CANCEL</a>
                        </div>
                        <button type="submit" class="btn btn-primary" style="margin-right: 1%; color: white; width: 90px;" id="saveBtn">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">SAVE</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection

@section('vendor-scripts')
{{-- Add your vendor scripts here --}}
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
@endsection

@section('page-scripts')
<script>
    let host = "{{ env('API_SERVER') }}";
</script>

<script>
    document.getElementById('addMou').addEventListener('click', function() {
        var mouSection = document.querySelector('.mou-section').cloneNode(true);

        mouSection.querySelectorAll('input').forEach(function(input) {
            input.value = null;
        });

        mouSection.setAttribute('data-mou-id', null);

        var fileListTbody = mouSection.querySelector('.table tbody');

        if (fileListTbody) {
            while (fileListTbody.firstChild) {
                fileListTbody.removeChild(fileListTbody.firstChild);
            }
        }

        document.getElementById('mouPage').appendChild(mouSection);

    });

    document.addEventListener('DOMContentLoaded', function() {
        var deleteButtons = document.querySelectorAll('.delete-file');

        deleteButtons.forEach(function(button) {
            button.addEventListener('click', function(event) {
                var fileEntryRow = event.target.closest('tr');
                var fileId = fileEntryRow.getAttribute('data-file-id');

                $.ajax({
                    type: 'POST',
                    url: '/delete-file',
                    data: {
                        _token: '{{ csrf_token() }}',
                        fileId: fileId
                    },
                    success: function(data) {

                        fileEntryRow.remove();

                    },
                    error: function(error) {
                        console.error('Failed to delete record: ', error);
                    },
                });
            });
        });
    });

    document.getElementById('mouPage').addEventListener('click', function(e) {
        if (e.target && e.target.classList.contains('removeMou')) {

            var mouSection = e.target.closest('.mou-section');
            var mouId = mouSection.getAttribute('data-mou-id');

            $.ajax({
                type: 'POST',
                url: '/remove-mou',
                data: {
                    _token: '{{ csrf_token() }}',
                    mouId: mouId
                },
                success: function(data) {

                    mouSection.remove();

                },
                error: function(error) {
                    console.error('Failed to delete record: ', error);
                },
            });
        }
    });
</script>

<script>
    document.getElementById('addLoa').addEventListener('click', function() {
        var loaSection = document.querySelector('.loa-section').cloneNode(true);

        loaSection.querySelectorAll('input').forEach(function(input) {
            input.value = null;
        });

        loaSection.setAttribute('data-loa-id', null);

        var fileListTbody = loaSection.querySelector('.table tbody');

        if (fileListTbody) {
            while (fileListTbody.firstChild) {
                fileListTbody.removeChild(fileListTbody.firstChild);
            }
        }

        document.getElementById('loaPage').appendChild(loaSection);
    });

    document.getElementById('loaPage').addEventListener('click', function(e) {
        if (e.target && e.target.classList.contains('removeLoa')) {

            var loaSection = e.target.closest('.loa-section');
            var loaId = loaSection.getAttribute('data-loa-id');

            $.ajax({
                type: 'POST',
                url: '/remove-loa',
                data: {
                    _token: '{{ csrf_token() }}',
                    loaId: loaId
                },
                success: function(data) {

                    loaSection.remove();

                },
                error: function(error) {
                    console.error('Failed to delete record: ', error);
                },
            });
        }
    });
</script>

<script>
    function previewImage(input) {
        var fileInput = input;
        var preview = document.getElementById('imagePreview');
        var label = document.querySelector('.custom-file-label');

        // Clear existing preview
        preview.style.display = 'none';
        preview.src = '';

        // Update label with the selected file name
        label.textContent = fileInput.files[0] ? fileInput.files[0].name : 'Choose file';

        // Show preview if an image file is selected
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                preview.src = e.target.result;
                preview.style.display = 'block';
            };

            reader.readAsDataURL(fileInput.files[0]);
        }
    }

    function updateFileName(input) {
        var label = input.nextElementSibling;
        var fileName = input.files[0] ? input.files[0].name : 'Choose File';
        label.textContent = fileName;
    }
</script>

<script>
    var trainerIdsToSave = [];
    var userID = document.getElementById('userIdAttribute').getAttribute('data-user-id');

    document.getElementById('addTrainer').addEventListener('click', function() {
        // Toggle the visibility of the form
        document.getElementById('addForm').style.display = 'block';
    });

    document.getElementById('saveTrainer').addEventListener('click', function() {
        // Get values from the form
        var selectedTrainerElement = document.getElementById('trainer_name');

        // Check if a trainer is selected
        if (!selectedTrainerElement.value) {
            alert('Please select a trainer.');
            return;
        }

        var selectedOption = selectedTrainerElement.options[selectedTrainerElement.selectedIndex];
        var trainerId = selectedOption.value;
        var name = selectedOption.text.trim();
        var email = selectedOption.getAttribute('data-email').trim();
        var position = selectedOption.getAttribute('data-position').trim();
        var status = selectedOption.getAttribute('data-status').trim();

        // Convert status to a human-readable format
        var statusText = (status == 1) ? 'Active' : 'Inactive';

        // Add logic to handle saving the trainer data to the table
        var tableBody = document.getElementById('tpTableBody');

        // Add the new row to the table
        var newRow = document.createElement('tr');
        newRow.innerHTML = `
            <td class="row-number">${tableBody.children.length + 1}</td>
            <td>${name}</td>
            <td>${email}</td>
            <td>${position}</td>
            <td>${statusText}</td>
            <td>
                <button type="button" class="btn btn-danger remove" onclick="removeRow(this)">Remove</button>
            </td>
        `;
        tableBody.appendChild(newRow);

        trainerIdsToSave.push(trainerId);
    });

    // Function to remove the row
    function removeRow(button) {
        var row = button.closest('tr');
        row.remove();
    }

    document.getElementById('saveBtn').addEventListener('click', function() {

        if (trainerIdsToSave.length === 0) {
            return;
        }

        $.ajax({
            url: '/save-trainer',
            method: 'POST',
            data: {
                trainerIds: trainerIdsToSave,
                userID: userID,
                _token: '{{ csrf_token() }}',
            },
            success: function(response) {
                console.log(data);
            },
            error: function(error) {
                console.error('Error saving all trainer IDs:', error);
            },
            complete: function() {
                // Clear the array after saving
                trainerIdsToSave = [];
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        // Handler for trainer_name change event
        $('#trainer_name').on('change', function() {
            // Get the selected trainer option
            var selectedTrainer = $(this).find(':selected');

            // Populate email and position fields
            $('#trainer_email').val(selectedTrainer.data('email'));
            $('#trainer_position').val(selectedTrainer.data('position'));

            var status = selectedTrainer.data('status');
            $('#trainer_status').val(status == 1 ? 'Active' : 'Inactive');
        });
    });
</script>

<script>
    var userID = document.getElementById('userIdAttribute').getAttribute('data-user-id');

    $(document).ready(function() {
        $('.tp-row').on('click', '.remove-btn', function() {
            var $row = $(this).closest('tr');
            var trainerId = $(this).data('trainer-id');

            if (confirm('Are you sure to remove this trainer?')) {
                $.ajax({
                    type: 'POST',
                    url: '/remove-trainer',
                    data: {
                        _token: '{{ csrf_token() }}',
                        userID: userID,
                        trainerId: trainerId
                    },
                    success: function(response) {

                        if (response.success) {
                            alert('Trainer removed successfully.');
                            $row.remove();
                        } else {
                            alert('Failed to remove trainer. Please try again.');
                        }
                    },
                    error: function() {
                        alert('Error while removing trainer. Please try again.');
                    }
                });
            }
        });
    });


    function updatePostcode() {
    var selectedPostcode = $('#postcode').val();

    $.ajax({
        url: '/get-postcode/' + selectedPostcode,
        type: 'GET',
        success: function (response) {
            console.log('RESPONSE', response);

            // Clear existing options
            $('#state').empty();
            $('#city').empty();
            $('#country').empty();

            // Append new options
            $('#state').append('<option value="' + response.state_id + '">' + response.state + '</option>');
            $('#city').append('<option value="' + response.city_id + '">' + response.cityname + '</option>');
            $('#country').append('<option value="' + response.country_id + '">' + response.country + '</option>');
        },
        error: function (error) {
            console.error('Error fetching postcode data:', error);
        }
    });
}
</script>
@endsection