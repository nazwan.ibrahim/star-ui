@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','New User Registration')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

    .btn-add-2:hover {
        color: #fff;
    }

    .btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
        color: #fff !important;
        opacity: 0.5;
    }

    .btn-secondary-6 {
        background-color: #fff;
        color: #5A8DEE;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        padding: 10px 20px;
        font-size: 16px;
        font-family: poppins-semibold, sans-serif;
        line-height: 20px;
        text-align: left;
    }

    .btn-secondary-6:hover {
        background-color: #5A8DEE;
        color: #fff;
    }

    .font-title {
        color: var(--gray-950, #0C111D);
        width: 300px;
        font-family: Inter;
        font-size: 24px;
        font-style: normal;
        font-weight: 600;
        line-height: 32px;
        text-align: left;
    }

    .form-group {
        display: flex;
        flex-direction: column; /* Stack elements vertically */
        align-items: center; /* Center horizontally */
    }

    .title-group {
        display: flex;
        flex-direction: column; /* Stack elements vertically */
        align-items: start; /* Center horizontally */
        text-align: left;
        margin-bottom: 2%;
    }

    .label-container {
        width: 100%; /* Full width labels */
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
    }

    label {
        text-align: left;
        margin-bottom: 5px; /* Spacing between label and input */
        width: 48%; /* Adjust as needed for your design */
    }

    .form-control {
        width: 100%; /* Full width input fields */
        margin-right: 0; /* Remove right margin */
    }

    .input-container {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    }

    .radio-container {
        display: flex;
        flex-direction: row;
    }
</style>
@endsection

@section('content')
<main style="padding: 2%; margin: 2%;">
    <div class="title-group">
        <label class="page-title">New User Registration</label>
    </div>
    <section>
        <div class="card" style="padding: 50px; margin: 50px;">
                    <div id="errorMessage" class="alert alert-danger" style="display: none;"></div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                <div class="row">
                    <label style="font-weight: bold;">Type Registration<span style="color: #ff0000">*</span></label>
                    <div class="col-sm-6 mt-4">
                        <div class="form-group radio-container">
                            <div class="form-check" style="margin-right: 7%;">
                                <input type="radio" id="training_partner" name="type_user" class="form-check-input" value="3">
                                <label class="form-check-label" style="display: inline;" for="training_partner">Training Partner</label>
                            </div>
                            <div class="form-check" style="margin-right: 7%;">
                                <input type="radio" id="trainer" name="type_user" class="form-check-input" value="4">
                                <label class="form-check-label" style="display: inline;" for="trainer">Trainer</label>
                            </div>
                            @if($userType == 1)
                            <div class="form-check" style="margin-right: 7%;" >
                                <input type="radio" id="admin" name="type_user" class="form-check-input" value="2">
                                <label class="form-check-label" style="display: inline;" for="admin">SEDA Admin</label>
                            </div>
                            @endif
                            <div class="form-check" style="margin-right: 7%;">
                                <input type="radio" id="participant" name="type_user" class="form-check-input" value="5" onclick="updateUserType('5')">
                                <label class="form-check-label" style="display: inline;" for="participant">Participant</label>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Training Partner Section --}}
                <form id="trainingPartnerForm" action="{{ route('admin.create-registration') }}" method="POST">
                    @csrf
                    <div id="trainingPartnerSection" style="display: none;">
                       <div class="row mt-4">
                           <div class="col-md-6">
                               <input type="hidden" id="user_type" name="type_user" class="form-check-input" value="3" selected>
                               <div class="form-group input-container">
                                   <div class="label-container">
                                       <label for="companyName" style="font-weight: bold;">Company Name<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                       <input type="text" oninput="updateInput(this)" class="form-control" id="companyName" name="companyName" placeholder="Please Enter Your Company Name" value="" required>
                                   </div>
                               </div>
                           </div>
                           <div class="col-md-6">
                               <div class="form-group input-container">
                                   <div class="label-container">
                                       <label for="phone_no" style="font-weight: bold;">Email<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                       <input type="text" class="form-control" id="email" name="email"placeholder="Please Enter Your Email"  value="" required>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="row">
                           <div class="col-md-6">
                               <div class="form-group input-container">
                                   <div class="label-container">
                                       <label for="phone_no" style="font-weight: bold;">Phone Number<span style="color: #ff0000; font-weight: bold;"></span></label>
                                       <input type="number" class="form-control number" pattern="[0-9]*" id="phone_no" name="phone_no" placeholder="Please Enter Your Phone No" value="">
                                   </div>
                               </div>
                           </div>
                           <div class="col-md-6">
                               <div class="form-group input-container">
                                   <div class="label-container">
                                       <label for="fax_no" style="font-weight: bold;">Fax Number<span style="color: #ff0000; font-weight: bold;"></span></label>
                                       <input type="number" class="form-control number" pattern="[0-9]*" id="fax_no" name="fax_no" placeholder="Please Enter Your Fax No" value="">
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="row">
                           <div class="col-md-6">
                               <div class="form-group input-container">
                                   <div class="label-container">
                                       <label for="country" style="font-weight: bold;">Country<span style="color: #ff0000; font-weight: bold;">*</label>
                                       <select class="form-control" id="country" name="country" onchange="updateStates()" required>
                                           <option> -- Please Select -- </option>
                                           @foreach($countries as $country)
                                           <option value="{{ $country->id }}">{{ $country->name }}</option>
                                           @endforeach
                                       </select>
                                   </div>
                               </div>
                           </div>
                           <div class="col-md-6">
                               <div class="form-group input-container">
                                   <label for="state" style="font-weight: bold;">State<span style="color: #ff0000; font-weight: bold;">*</label>
                                   <select class="form-control" id="state" name="state" required>
                                       <option> -- Please Select -- </option>
                                   </select>
                               </div>
                           </div>
                       </div>
                       <div class="d-flex justify-content-center mt-4">
                           <div style="margin-right: 1%;">
                               <a href="{{ route('admin.user-management') }}" class="btn btn-secondary-6 btn-block">CANCEL</a>
                           </div>
                           <button type="submit" class="btn btn-primary" style="margin-left: 1%;" id="trainingPartnerBtn">
                               <i class="bx bx-check d-block d-sm-none"></i>
                               <span class="d-none d-sm-block">CREATE</span>
                           </button>
                       </div>
                       </div>
                </form>

               
                {{-- Trainer Section --}}
                <form id="trainerForm">
                    @csrf
                    <div id="trainerSection" style="display: none;">
                        <div class="row mt-4">
                            <div class="col-md-6">
                                <div class="form-group input-container">
                                    <div class="label-container">
                                        <label for="mykad_no" style="font-weight: bold;">MyKad Number/Passport Number<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                        <input type="number" class="form-control" id="mykad_no" name="mykad_no"  placeholder="Please Enter Your Mykad No" value="" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group input-container">
                                    <div class="label-container">
                                        <label for="certificate_no" style="font-weight: bold;">Certificate Number<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                        <input type="text" class="form-control" id="certificate_no" name="certificate_no" placeholder="Please Enter Your Certificate No" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center mt-4">
                            <div style="margin-right: 1%;">
                                <a href="{{ route('admin.user-management') }}" class="btn btn-secondary-6 btn-block">CANCEL</a>
                            </div>
                            <button type="submit" class="btn btn-primary" style="margin-left: 1%;" id="trainerBtn">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">CHECK</span>
                            </button>
                        </div>
                </form>
                <form id="" action="{{ route('admin.create-registration') }}" method="POST">
                    @csrf
                    <div class="row  mt-4">
                        <div class="col-md-6">
                        <input type="hidden" id="user_type" name="type_user" class="form-check-input" value="4" selected>
                            <div class="form-group input-container">
                                <div class="label-container">
                                    <label for="fullname" style="font-weight: bold;">Full Name<span style="color:#ff0000; font-weight: bold;">*</span></label>
                                    <input type="text" oninput="updateInput(this)" class="form-control" id="fullname" name="fullname" placeholder="Please Enter Your Full Name" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group input-container">
                                <div class="label-container">
                                    <label for="ic_no" style="font-weight: bold;">MyKad Number / Passport Number<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                    <input type="text" class="form-control" id="ic_no" name="ic_no" placeholder="Please Enter MyKad No." value="" required>                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group input-container">
                                <div class="label-container">
                                    <label for="email" style="font-weight: bold;">Email<span style="color:#ff0000; font-weight: bold;">*</span></label>
                                    <input type="text" class="form-control" id="email" name="email" value="" placeholder="Please Enter Your Email" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group input-container">
                                <div class="label-container">
                                    <label for="phone_no" style="font-weight: bold;">Phone Number<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                    <input type="number" class="form-control number" id="phone_no" name="phone_no" pattern="[0-9]*" value="" placeholder="Please Enter Your Phone Number" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group input-container">
                                <div class="label-container">
                                    <label for="certificate_category" style="font-weight: bold;">Certificate Category<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                    <input type="text" class="form-control" id="certificate_category" name="certificate_category" value="" placeholder="Please Enter Your Certificate Category" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group input-container">
                                <div class="label-container">
                                    <label for="certificate_no1" style="font-weight: bold;">Certificate Number<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                    <input type="text" class="form-control" id="certificate_no1" name="certificate_no1" value="" placeholder="Please Enter Your Certificate Number" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group input-container">
                                <div class="label-container">
                                    <label for="batch" style="font-weight: bold;">Batch<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                    <input type="text" class="form-control" id="batch" name="batch" value="" placeholder="Please Enter Your Batch" required>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-md-6">
                            <div class="form-group input-container">
                                <div class="label-container">
                                    <label for="batch" style="font-weight: bold;">Training Partner<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                    <select class="form-control" id="training_partner" name="training_partner" required>
                                       <option> -- Please Select Training Partner-- </option>
                                       @foreach($companies as $company)
                                       <option value="{{ $company->id }}">{{ $company->name }}</option>
                                       @endforeach
                                   </select>                                
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    <div class="d-flex justify-content-center mt-4">
                        <div style="margin-right: 1%;">
                            <a href="{{ route('admin.user-management') }}" class="btn btn-secondary-6 btn-block">CANCEL</a>
                        </div>
                        <button type="submit" class="btn btn-primary" style="margin-left: 1%;" id="promoteBtn">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">PROMOTE</span>
                        </button>
                    </div>
                    </div>
                </form>

                {{-- SEDA Admin Section --}}
                <form id="adminForm" action="{{ route('admin.create-registration') }}" method="POST">
                    @csrf
                    <div id="adminSection" style="display: none;">
                        <div class="row mt-4">
                            <div class="col-md-6">
                                <input type="hidden" id="user_type" name="type_user" class="form-check-input" value="2" selected>
                                <div class="form-group input-container">
                                    <div class="label-container">
                                        <label for="fullname" style="font-weight: bold;">Full Name<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                        <input type="text" oninput="updateInput(this)" class="form-control" id="fullname" name="fullname" placeholder="Please Enter Your Full Name" value="" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group input-container">
                                    <div class="label-container">
                                        <label for="ic_no" style="font-weight: bold;">MyKad Number<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                        <input type="number" class="form-control" id="ic_no" name="ic_no" placeholder="Please Enter Your Mykad No" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group input-container">
                                    <div class="label-container">
                                        <label for="qp_no" style="font-weight: bold;">Staff Number<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                        <input type="text" class="form-control" id="staffno" name="staffno" placeholder="Please Enter Your Staff No" value="" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group input-container">
                                    <div class="label-container">
                                        <label for="position" style="font-weight: bold;">Position<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                        <input type="text" class="form-control" id="position" name="position" placeholder="Please Enter Your Position" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group input-container">
                                    <div class="label-container">
                                        <label for="department" style="font-weight: bold;">Department<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                        <input type="text" class="form-control" id="department" name="department" placeholder="Please Enter Your Department" value="" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group input-container">
                                    <div class="label-container">
                                        <label for="phone_no" style="font-weight: bold;">Phone Number<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                        <input type="number" class="form-control number" id="phone_no" name="phone_no" pattern="[0-9]*" placeholder="Please Enter Your Phone No" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group input-container">
                                    <div class="label-container">
                                        <label for="email" style="font-weight: bold;">Email<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Please Enter Your Email" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center mt-4">
                            <div style="margin-right: 1%;">
                                <a href="{{ route('admin.user-management') }}" class="btn btn-secondary-6 btn-block">CANCEL</a>
                            </div>
                            <button type="submit" class="btn btn-primary" style="margin-left: 1%;" id="adminBtn">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">CREATE</span>
                            </button>
                        </div>
                    </div>
                </form>


                {{-- Potential Participant Section --}}
                <form id="participantForm" action="{{ route('admin.create-registration') }}" method="POST">
                    @csrf
                    <div id="potentialParticipantSection" style="display: none;">
                        <div class="row mt-4">
                            <div class="col-md-6">
                                <input type="hidden" id="user_type" name="type_user" class="form-check-input" value="5" selected>
                                <div class="form-group input-container">
                                    <div class="label-container">
                                        <label for="fullname" style="font-weight: bold;">Name<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                        <input type="text" oninput="updateInput(this)" class="form-control" id="fullname" name="fullname" placeholder="Please Enter Your Name" value="" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group input-container">
                                    <div class="label-container">
                                        <label for="phone_no" style="font-weight: bold;">Email<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Please Enter Your Email" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group input-container">
                                    <div class="label-container">
                                        <label for="company" style="font-weight: bold;">Company Name<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                        <input type="text" class="form-control" id="company" name="company" placeholder="Please Enter Your Company" value="" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group input-container">
                                    <div class="label-container">
                                        <label for="phone_no" style="font-weight: bold;">Phone Number<span style="color: #ff0000; font-weight: bold;"></span></label>
                                        <input type="text" class="form-control number" pattern="[0-9]*" id="phone_no" name="phone_no" placeholder="Please Enter Your Phone No" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center mt-4">
                            <div style="margin-right: 1%;">
                                <a href="{{ route('admin.user-management') }}" class="btn btn-secondary-6 btn-block">CANCEL</a>
                            </div>
                            <button type="submit" class="btn btn-primary" style="margin-left: 1%;" id="participantBtn">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">CREATE</span>
                            </button>
                        </div>
                </form>

                {{--<form id=""action="route('admin.upload-bulk-registration')" enctype="multipart/form-data" method="POST" onsubmit="return validateForm()">
                    @csrf
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="form-group input-container">
                                <div class="label-container">
                                    <label for="batch" style="font-weight: bold;">UPLOAD BULK PARTICIPANT<span style="color: #ff0000; font-weight: bold;">*</span></label>
                                    <input type="file" class="form-control" id="batch" name="uploadBulk" accept=".xls, .xlsx, .csv" required>
                                    <small class="text-muted">Accepted file types: .xls, .xlsx, .csv. Maximum file size: 2MB.</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group input-container">
                        <small class="text-muted">
                            <a href="{{ asset('storage/downloads/uploadparticipant.xlsx') }}" download>Download Example Excel File</a> to see the required format.
                        </small>
                    </div>
                    <div class="d-flex justify-content-center mt-4">
                        <div style="margin-right: 1%;">
                            <a href="{{ route('admin.user-management') }}" class="btn btn-secondary-6 btn-block">CANCEL</a>
                        </div>
                        <button type="submit" class="btn btn-primary" style="margin-left: 1%;" id="promoteBtn">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">UPLOAD</span>
                        </button>
                    </div>
                </div>  
                </form> --}}

            </div>
        </div>

    </section>
</main>
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
{{-- <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>  --}}
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
@endsection

@section('page-scripts')
    {{-- your existing JavaScript --}}
    <script>
        var trainingPartnerRadio = document.getElementById('training_partner');
        var trainerRadio = document.getElementById('trainer');
        var potentialParticipantRadio = document.getElementById('participant');
        var adminRadio = document.getElementById('admin');
        /* var trainerSection = Code to select the trainer section, e.g., document.getElementById("trainerSection") */

        if (adminRadio !== null) {
            adminRadio.addEventListener('change', toggleSections);
        }
        trainingPartnerRadio.addEventListener('change', toggleSections);
        trainerRadio.addEventListener('change', toggleSections);
        potentialParticipantRadio.addEventListener('change', toggleSections);

        function toggleSections() {
            var trainingPartnerSection = document.getElementById('trainingPartnerSection');
            var adminSection = document.getElementById('adminSection');
            var potentialParticipantSection = document.getElementById('potentialParticipantSection');
            var trainerSection = document.getElementById('trainerSection'); // Add this line

            if (trainingPartnerRadio.checked) {
                trainingPartnerSection.style.display = 'block';
            } else {
                trainingPartnerSection.style.display = 'none';
            }

            if (trainerRadio.checked) {
                trainerSection.style.display = 'block';
            } else {
                trainerSection.style.display = 'none';
            }

           

            if (potentialParticipantRadio.checked) {
                potentialParticipantSection.style.display = 'block';
            } else {
                potentialParticipantSection.style.display = 'none';
            }

            if (adminRadio !== null && adminRadio.checked) {
                adminSection.style.display = 'block';
            } else {
                adminSection.style.display = 'none';
            }
        }
        function updateUserType(value) {
            document.getElementById('user_type').value = value;
        }

        $(document).ready(function () {
            var trainerForm = $('#trainerForm');
            var trainerRadioButton = $('input[name="type_user"][value="4"]');
            var icNoField = $('#ic_no');
            var certificateNoField = $('#certificate_no1');
            var fullnameField = $('#fullname');
            var certificateCategoryField = $('#certificate_category');
            var batchField = $('#batch');

            trainerForm.submit(function (e) {
                e.preventDefault();
            
                var formData = trainerForm.serialize();
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.getQP') }}",
                    data: formData,
                    success: function (response) {
                        if (response.status === 'OK') {

                            console.log('DATA', response);
                            
                            // Assuming the nested data is under the 'result' key
                            icNoField.val(response.result.mykad_no);
                            certificateNoField.val(response.result.certificate_no);
                            fullnameField.val(response.result.name);
                            certificateCategoryField.val(response.result.certificate_category);
                            batchField.val(response.result.batch);  // Ensure 'batch' is present in the response

                            trainerRadioButton.prop('checked', true).trigger('change');
                        } else {
                            alert('Record Not Found.');
                        }
                    },
                    error: function () {
                        alert('Error during API call.');
                    }
                });
            });
        });


        function validateForm() {
        var fileInput = document.getElementById('batch');
        var fileSize = fileInput.files[0].size; // in bytes
        var maxSize = 2 * 1024 * 1024; // 2MB

        if (!['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'text/csv'].includes(fileInput.files[0].type)) {
            alert('Please upload a valid file type: .xls, .xlsx, .csv');
            return false;
        }

        if (fileSize > maxSize) {
            alert('File size exceeds the maximum limit of 2MB.');
            return false;
        }

        return true;
    }


    function updateInput(input) {
        input.value = input.value.toUpperCase();
    }

    function updateStates() {
        var selectedCountryId = $('#country').val();
    
        $.ajax({
            url: '/get-states/' + selectedCountryId,
            type: 'GET',
            success: function(response) {
                $('#state').empty();
            
                $.each(response, function(index, state) {
                    $('#state').append('<option value="' + state.id + '">' + state.name + '</option>');
                });
            },
            error: function(error) {
                console.error('Error fetching states:', error);
            }
        });
    }

</script>

@endsection
