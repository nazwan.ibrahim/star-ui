@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Hello')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
{{-- add css style here --}}
@endsection

@section('content')
    <div class="container mt-5">
        <h1>Hello App</h1>
        
        
     
    </div>

  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
    {{-- <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>  --}}
    
@endsection

@section('page-scripts')
{{-- our own js --}}
<script src="{{asset('/js/hello2/hello2.js')}}"></script>
<script src="{{asset('/js/hello/hello.js')}}"></script>

@endsection
