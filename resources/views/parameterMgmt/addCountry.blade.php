@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Training Course Management')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('page-styles')
<style>
    /* Your custom page styles here */
    /* Align items to the left */
    .centered-container {
        display: flex;
        justify-content: flex-start;
        align-items: center;
        max-width: 100%;
    }

    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 55px;
        margin-bottom: 55px;
    }

    .title-margin {
        margin-top: 95px;
        padding-bottom: 15px;
        margin-bottom: 15px;
        margin-left: 100px;
        justify-content: flex-start;
        display: flex;
    }

    .container {
        max-width: 1200px; /* Adjust the width as needed */
        display: flex;
        flex-direction: column;
        align-items: start;
    }

    /* Style for labels */
    .form-group label {
        display: block;
        text-align: left; /* Align labels to the left */
        margin-bottom: 5px; /* Add vertical gap between label and field */
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .form-checkin {
  display: block;
  min-height: 1.44rem;
  padding-left: 1.5em;
  margin-bottom: 0.15rem;
}

.form-check-labell {
  top: 0 !important;
  cursor: pointer;
  font-weight: normal;
  margin-bottom: 0.25rem;
  font-size: 0.85rem;
  color: #000000;
  width:100px;
}

.form-row {
        
        width: 80%;
    }
</style>
@endsection
@section('content')
<main class="header-top-margin col-md-12 mx-auto">
    <div class="text-center mb-3">
        <h2 class="page-title py-2" style="text-align: left !important; margin-top: 5%;">Add Country</h2>
    </div>
    @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
    @else
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
    @endif
    <div class="col-md-12" style="display: flex; justify-content: left; align-items: left;">
        <div class="col-md-5">
            <form action="{{ route('addNewCountry') }}" method="post">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-12 py-2">
                        <label class="field-title" ><b>Country Name</b></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="form-group col-md-12 py-2">
                        <label class="field-title" ><b>ISO Alpa-2</b></label>
                        <input type="text" class="form-control" onkeydown="return /[a-z]/i.test(event.key)" id="iso" name="iso">
                    </div>
                    <div class="form-group col-md-12 py-2">
                        <label class="field-title" ><b>ISO Alpa-3</b></label>
                        <input type="text" class="form-control" onkeydown="return /[a-z]/i.test(event.key)" id="iso3" name="iso3">
                    </div>
                    <div class="form-group col-md-12 py-2">
                        <label class="field-title"><b>Number Code</b></label>
                        <input type="text" class="form-control" id="numcode" name="numcode">
                    </div>
                    <div class="form-group col-md-12 py-2">
                        <label class="field-title"><b>Phone Code</b></label>
                        <input type="text" class="form-control" inputmode="numeric"  id="phonecode" name="phonecode">
                    </div>
                </div>
                <div class="d-flex justify-content-end mt-4">
                    <div style="margin-right: 1%;">
                        <a href="{{ route('country') }}" class="btn btn-secondary-6 btn-block">CANCEL</a>
                    </div>
                    <button type="submit" class="btn btn-primary" style="margin-right: 1%; color: white; width: 90px;" id="hantarBtn">
                        <i class="bx bx-check d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">SAVE</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</br>


       
    </section>
</main>
  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script> 
    
@endsection



@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";

</script>


<!-- <script src="{{ asset('js/parameterMgmt/gender.js') }}"></script> -->

@endsection
