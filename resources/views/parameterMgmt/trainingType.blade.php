@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Training Course Management')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .dataTables_length {
        display: none;
    }

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .btn-add-user:hover {
        background-color: #3e6be1;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }   

    .btn-actions {
        display: flex;
        gap: 10px;
    }

    .btn-edit,
    .btn-delete {
        background-color: #5A8DEE;
        color: #fff;
        border: none;
        border-radius: 5px;
        padding: 5px 10px;
        font-size: 14px;
        font-weight: 600;
        cursor: pointer;
        transition: background-color 0.3s ease-in-out;
    }

    .btn-edit:hover,
    .btn-delete:hover {
        background-color: #3e6be1;
    }
    
     /* Add styles for the table */
     .table {
    width: 100%;
    border-collapse: collapse;
    margin: 0;
    }

   

    .table th, .table td {
    padding: 10px;
    text-align: center;
    border: 1px solid #ddd;
    }

    /* Add hover effect on table rows */
    .table tbody tr:hover {
    background-color: #f5f5f5;
}


</style>
@endsection
@section('content')
<main class="header-top-margin col-md-12 mx-auto">
    <div class="text-center mb-3">
        <h2 class="page-title py-2" style="text-align: left !important; margin-top: 5%;">Training Type Management</h2>
    </div>
    @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
    @else
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
    @endif
</br>

    @csrf
    @method('DELETE')
    <div class="table-responsive">
        <div id="trainingTypeManagementList_wrapper" class="dataTables_wrapper no-footer">
            <table class="table table-striped dataTable no-footer" id="trainingTypeManagementList" style="width: 100%; border-bottom: white;">
                <thead class="table-header">
                    <tr>
                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;" width="30">No.</th>
                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Name</th>
                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Code</th>
                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Status</th>
                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Action</th>
                    </tr>
                    <tr>
                        <th class="table-title text-center" width="40">Filter</th>
                        <th class="table-title text-center"> 
                            <input type="text" placeholder="Search Name" class="form-control" />
                        </th>
                        <th class="table-title text-center"> 
                            <input type="text" placeholder="Search Code" class="form-control" />
                        </th>
                        <th class="table-title text-center"> 
                            <input type="text" placeholder="Search Status" class="form-control" />
                        </th>
                        <th class="table-title text-center"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
       
    </section>
</main>
  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script> 
    
@endsection



@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";

</script>


<script src="{{ asset('js/parameterMgmt/trainingType.js') }}"></script>

@endsection
