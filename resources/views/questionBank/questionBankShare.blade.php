@extends('layouts.contentLayoutMaster')

{{-- Page Title --}}
@section('title', 'Question Bank')

{{-- Vendor CSS --}}
@section('vendor-styles')
{{-- Add your vendor styles here --}}
@endsection

@section('page-styles')
<style>
    /* Your custom page styles here */
    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .title-margin {
        margin-top: 95px;
        padding-bottom: 15px;
        margin-bottom: 15px;
        margin-left: 100px;
        justify-content: flex-start;
        display: flex;
    }

    .form-row {
        display: flex !important;
    }

    /* Style for labels */
    .form-group label {
        display: block;
        text-align: left;
        /* Align labels to the left */
        margin-bottom: 5px;
        /* Add vertical gap between label and field */
    }
</style>
@endsection

@section('content')
<main class="header-top-margin col-md-12 mx-auto" style="bottom: 10px;left: 100px;">

    <div class="text-center mb-3">
        <h2 class="page-title py-2" style="text-align: left !important; margin-top: 5%;">Share Question Bank</h2>
    </div>

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @else
    @if ($message = Session::get('error'))
    <div class="alert alert-danger">
        <p>{{ $message }}</p>
    </div>
    @endif
    @endif

    <div class="container">
        <div class="centered-container">
            <form id="form_login" action="{{ route('admin.questionbanksharesubmit', ['id' => $bankid]) }}" method="POST" class="user-form">
                @csrf
                <div style="width: 100%; height: 100%; padding: 40px; background: white; border-radius: 16px; border: 1px #D0D5DD solid; flex-direction: column; justify-content: center; align-items: flex-end; gap: 24px; display: inline-flex">
                    <div style="align-self: stretch; text-align: center; color: #656565; font-size: 32px; font-family: Rajdhani; font-weight: 600; line-height: 38px; word-wrap: break-word">Share Question Bank Details</div>

                    <div style="align-self: stretch; height: 100%; flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 24px; display: flex">
                        <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
                            <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Training Course</div>
                            @foreach ($outputData as $index => $item)
                            <input type="text" class="form-control" placeholder="" name="training_course" id="training_course" value="{{ $item['course_name'] }}" readonly>
                        </div>
                        <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
                            <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Batch No.</div>
                            <input type="text" class="form-control" placeholder="" name="batch_no" id="batch_no" value="{{ $item['batch_no'] }}" style="width: 100%; padding: 8px; border: none; outline: none; border-radius: 4px;" readonly>
                            @endforeach
                        </div>
                        <div style="align-self: stretch; justify-content: flex-start; align-items: center; gap: 16px; display: inline-flex">
                            <div style="flex: 1 1 0; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: inline-flex">
                                <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Training Partner</div>
                                <select name="training_partner" style="align-self: stretch; padding-left: 14px; padding-right: 14px; padding-top: 10px; padding-bottom: 10px; background: white; box-shadow: 0px 1px 2px rgba(16, 24, 40, 0.05); border-radius: 8px; overflow: hidden; border: 1px #D0D5DD solid; justify-content: flex-start; align-items: center; gap: 8px; display: inline-flex">
                                    <option value="0">-- Please Choose --</option>
                                    @foreach($trainingpartner as $tp)
                                    <option value="{{ $tp->company_id }}">{{ $tp->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
                            <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
                                <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Password</div>
                                <div style="align-self: stretch; padding-left: 14px; padding-right: 14px; padding-top: 10px; padding-bottom: 10px; background: white; box-shadow: 0px 1px 2px rgba(16, 24, 40, 0.05); border-radius: 8px; overflow: hidden; border: 1px #D0D5DD solid; justify-content: flex-start; align-items: center; gap: 8px; display: inline-flex">
                                    <input type="password" placeholder="Password" name="password_file" id="password_input" style="width: 100%; padding: 8px; border: none; outline: none; border-radius: 4px;">
                                    <input type="checkbox" onclick="togglePasswordVisibility()">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="display: flex; align-items: center; margin: 0 auto; gap: 24px; margin: top 10px;">
                        <div>
                            <button type="button" class="btn btn-light" style="margin-right: 1%; width: 90px;" id="cancelBtn">
                                <a href="{{ route('admin.questionbanksharelist') }}" style="color: black; text-decoration: none;">
                                    <i class="bx bx-check d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">CANCEL</span>
                                </a>
                            </button>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-primary" style="margin-right: 1%; color: white; width: 90px;" id="delBtn">
                                <a href="" style="color: white; text-decoration: none;">
                                    <i class="bx bx-check d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">SUBMIT</span>
                                </a>
                            </button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
    </div>
</main>
@endsection

@section('vendor-scripts')
{{-- Add your vendor scripts here --}}
@endsection

@section('page-scripts')
<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>

<script>
    function togglePasswordVisibility() {
        var passwordInput = document.getElementById("password_input");

        if (passwordInput.type === "password") {
            passwordInput.type = "text";
        } else {
            passwordInput.type = "password";
        }
    }

    function goBack() {
        window.history.back();
    }
</script>
@endsection