@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Question Bank Submission')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
        #sectionFormMain>div>div:nth-child(5)>div.col-4 {
            margin-top: 19px;
        }
    }


    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    /* .dataTables_length {
        display: none;
    } */

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }

    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
        border-bottom: none;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
        border-bottom: none;
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .filter-dropdown {
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }

    /* Add styles for the table */
    .table {
        width: 100%;
        border-collapse: collapse;
        margin: 0;
    }



    .table th,
    .table td {
        padding: 10px;
        text-align: center;
        border: 1px solid #ddd;
    }

    /* Add hover effect on table rows */
    .table tbody tr:hover {
        background-color: #f5f5f5;
    }
</style>
@endsection

@section('content')
<main class="header-top-margin col-md-12 mx-auto">
    <section class="header-top-margin">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @else
        @if ($message = Session::get('error'))
        <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
        @endif
        @endif


        @csrf
        @method('DELETE')
        <div class="pull-left" style="text-align: left;">
            <a class="btn btn-secondary" href="{{ route('TP-training-schedule-history') }}">
                <i class="fas fa-arrow-left"></i> Back
            </a>
        </div>
        </br>
        <div class="text-center mb-3">
            <h2 class="page-title py-2" style="text-align: left !important; margin-top: 1%;">List Submission of Question Bank </h2>
        </div>
        </br>
        <div class="pull-right" style="text-align: right;">
            <a class="btn btn-success" href="{{ route('questionbanksubmissionnew') }}">New Question Bank</a>
        </div>

        <div class="table-responsive">
            <div id="questionList_wrapper" class="dataTables_wrapper no-footer">
                <table class="table table-width-style dataTable no-footer" id="submitList" style="width: 100%; border-bottom: white;">

                    <thead class="table-header">
                        <tr>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">No.</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Course</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Batch No.</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Theory (Question)</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Theory (Solution)</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Practical (Question)</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Practical (Solution)</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Remarks</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Status</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($outputData as $index => $item)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $item['course_name'] }}</td>
                            <td>{{ $item['batch_no'] }}</td>
                            <td>
                                @if (isset($item['file_names']['Theory']['Question']))
                                @foreach ($item['file_names']['Theory']['Question'] as $fileName)
                                {{ $fileName }}<br>
                                @endforeach
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if (isset($item['file_names']['Theory']['Solution']))
                                @foreach ($item['file_names']['Theory']['Solution'] as $fileName)
                                {{ $fileName }}<br>
                                @endforeach
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if (isset($item['file_names']['Practical']['Question']))
                                @foreach ($item['file_names']['Practical']['Question'] as $fileName)
                                {{ $fileName }}<br>
                                @endforeach
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if (isset($item['file_names']['Practical']['Solution']))
                                @foreach ($item['file_names']['Practical']['Solution'] as $fileName)
                                {{ $fileName }}<br>
                                @endforeach
                                @else
                                -
                                @endif
                            </td>
                            <td>{{ $item['remarks'] }}</td>
                            <td>
                                @if($item['status_id'] == '2')
                                <a class="btn btn-success"> {{ $item['status_name'] }}</a>
                                @elseif($item['status_id'] == '3')
                                <a class="btn btn-danger"> {{ $item['status_name'] }}</a>
                                @elseif($item['status_id'] == '1')
                                <a class="btn btn-warning"> {{ $item['status_name'] }}</a>
                                @endif

                            </td>
                            <td>
                                @if($item['status_id'] == '2' || $item['status_id'] == '1')
                                <a class="btn btn-secondary" href="{{ url('/training-partner/question-bank-submission-view',['id'=>$item['id_question_bank']])}}">View</a>
                                @elseif($item['status_id'] == '3')
                                <a class="btn btn-primary" href="{{ url('/training-partner/question-bank-submission-edit',['id'=>$item['id_question_bank']])}}">Edit</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="text-center mb-3">
                <h2 class="page-title py-2" style="text-align: left !important; margin-top: 5%;">List Requested of Question Bank</h2>
            </div>
        </div>
        </br>

        <div class="pull-right" style="text-align: right;">
            <a class="btn btn-success" href="{{ route('questionbankrequestnew') }}">New Request</a>
        </div>



        <div class="table-responsive">
            <div id="questionList_wrapper" class="dataTables_wrapper no-footer">
                <table class="table table-width-style dataTable no-footer" id="requestList" style="width: 100%; border-bottom: white;">
                    <thead class="table-header">
                        <tr>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">No.</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Training Partner</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Course</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Batch No.</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Theory (Question)</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Theory (Solution)</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Practical (Question)</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Practical (Solution)</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Status</th>
                            <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($outputDataRequest as $index1 => $item1)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $item1['company_name'] ?? '-' }}</td>
                            <td>{{ $item1['course_name'] }}</td>
                            <td>{{ $item1['batch_no'] }}</td>
                            <td>
                                @if (isset($item1['Theory']['Question']))
                                @foreach ($item1['Theory']['Question'] as $version)
                                @if($item1['status_name'] == 'Approved')
                                <a class="btn btn-success"> Success </a>
                                @elseif($item1['status_name'] == 'Rejected')
                                <a class="btn-danger"> {{ $item1['status_name'] }}</a>
                                @else
                                <a class="btn btn-warning"> Requested</a>
                                @endif
                                @endforeach
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if (isset($item1['Theory']['Solution']))
                                @foreach ($item1['Theory']['Solution'] as $version)
                                @if($item1['status_name'] == 'Approved')
                                <a class="btn btn-success"> Success </a>
                                @elseif($item1['status_name'] == 'Rejected')
                                <a class="btn-danger"> {{ $item1['status_name'] }}</a>
                                @else
                                <a class="btn btn-warning"> Requested</a>
                                @endif
                                @endforeach
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if (isset($item1['Practical']['Question']))
                                @foreach ($item1['Practical']['Question'] as $version)
                                @if($item1['status_name'] == 'Approved')
                                <a class="btn btn-success"> Success </a>
                                @elseif($item1['status_name'] == 'Rejected')
                                <a class="btn-danger"> {{ $item1['status_name'] }}</a>
                                @else
                                <a class="btn btn-warning"> Requested</a>
                                @endif @endforeach
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if (isset($item1['Practical']['Solution']))
                                @foreach ($item1['Practical']['Solution'] as $version)
                                @if($item1['status_name'] == 'Approved')
                                <a class="btn btn-success"> Success </a>
                                @elseif($item1['status_name'] == 'Rejected')
                                <a class="btn-danger"> {{ $item1['status_name'] }}</a>
                                @else
                                <a class="btn btn-warning"> Requested</a>
                                @endif @endforeach
                                @else
                                -
                                @endif
                            </td>
                            <td>
                                @if($item1['status_name'] == 'Approved')
                                <a class="btn btn-success"> Completed </a>
                                @elseif($item1['status_name'] == 'Rejected')
                                <a class="btn-danger"> {{ $item1['status_name'] }}</a>
                                @else
                                <a class="btn btn-warning"> Submitted</a>
                                @endif

                            </td>
                            <td>
                                @if($item1['status_name'] == 'Approved')
                                <a class="btn btn-secondary" href="{{ url('/training-partner/question-bank-request-view',['id'=>$item['id_question_bank']])}}">View</a>
                                @else
                                <a>-</a>
                                @endif

                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>

    </section>
</main>


@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
@endsection

@section('page-scripts')

<script>
    $(document).ready(function() {
        $('#submitList').DataTable({
            dom: 'lfrtip',
            ordering: false,
            initComplete: function() {
                this.api().columns().every(function() {
                    var column = this;
                    console.log(column.header()); // Output column headers to the console
                    $('input', column.header()).on('keyup change clear', function() {
                        if (column.search() !== this.value) {
                            // Use case-insensitive search for the "Status" column (index 5)
                            column.search(this.value, false, true).draw();
                        }
                    });
                });
            },
            responsive: true,
            lengthMenu: [5, 10, 15, 20],
            pageLength: 5,
            language: {
                info: "_START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "0 from 0 to 0 entry",
                lengthMenu: " _MENU_ Entry",
                paginate: {
                    next: "Next",
                    previous: "Previous",
                },
                zeroRecords: "No record found",
            },
        });
    });


    $(document).ready(function() {
        $('#requestList').DataTable({
            dom: 'lfrtip',
            ordering: false,
            initComplete: function() {
                this.api().columns().every(function() {
                    var column = this;
                    console.log(column.header()); // Output column headers to the console
                    $('input', column.header()).on('keyup change clear', function() {
                        if (column.search() !== this.value) {
                            // Use case-insensitive search for the "Status" column (index 5)
                            column.search(this.value, false, true).draw();
                        }
                    });
                });
            },
            responsive: true,
            lengthMenu: [5, 10, 15, 20],
            pageLength: 5,
            language: {
                info: "_START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "0 from 0 to 0 entry",
                lengthMenu: " _MENU_ Entry",
                paginate: {
                    next: "Next",
                    previous: "Previous",
                },
                zeroRecords: "No record found",
            },
        });
    });
</script>





<script>
    $(document).ready(function() {
        $('.download-link').click(function(e) {
            e.preventDefault();
            var file = $(this).data('file');
            window.location.href = $(this).attr('href');
        });
    });
</script>

@endsection