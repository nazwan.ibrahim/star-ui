@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title', 'Question Bank')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
        #sectionFormMain>div>div:nth-child(5)>div.col-4 {
            margin-top: 19px;
        }
    }


    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    /* .dataTables_length {
        display: none;
    } */

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }

    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
        border-bottom: none;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
        border-bottom: none;
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .filter-dropdown {
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }

    /* Add styles for the table */
    .table {
        width: 100%;
        border-collapse: collapse;
        margin: 0;
    }



    .table th,
    .table td {
        padding: 10px;
        text-align: center;
        border: 1px solid #ddd;
    }

    /* Add hover effect on table rows */
    .table tbody tr:hover {
        background-color: #f5f5f5;
    }
</style>
@endsection

@section('content')
<main class="header-top-margin col-md-12 mx-auto">
    <section class="header-top-margin">
        @csrf
        @method('DELETE')
        <div class="pull-left" style="text-align: left;">
            <a class="btn btn-secondary" href="{{ route('admin.questionbanksharelist') }}">
                <i class="fas fa-arrow-left"></i> Back
            </a>
        </div>
        </br>

        <div class="mt-5">
            <ul class="nav nav-tabs" id="Tabs">
                <li class="nav-item">
                    <a class="nav-link active" id="tab1-tab" data-toggle="tab" href="#tab1">Submission Question Bank List</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tab2-tab" data-toggle="tab" href="#tab2">Approved Question Bank List</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tab3-tab" data-toggle="tab" href="#tab3">Requested Question Bank List</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tab3-tab" data-toggle="tab" href="#tab4">Shared Requested Question Bank List</a>
                </li>
            </ul>

            <div class="tab-content mt-2">
                <div class="tab-pane fade show active" id="tab1">
                    <div class="text-center mb-3">
                        <h2 class="page-title py-2" style="text-align: left !important; margin-top: 1%;">Submission Question Bank List</h2>
                    </div>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @else
                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    @endif

                    <div class="table-responsive">
                        <div id="userManagementList_wrapper" class="dataTables_wrapper no-footer">
                            <table class="table table-width-style dataTable no-footer" id="submissionQuestionTable" style="width: 100%; border-bottom: white;">
                                <thead class="table-header">
                                    <tr>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">No.</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Training Partner</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Course</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Batch No.</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Theory (Question)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Theory (Solution)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Practical (Question)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Practical (Solution)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Remarks</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Status</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($outputData as $index => $item)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $item['company_name'] }}</td>
                                        <td>{{ $item['course_name'] }}</td>
                                        <td>{{ $item['batch_no'] }}</td>
                                        <td>
                                            @if (isset($item['file_names']['Theory']['Question']))
                                            @foreach ($item['file_names']['Theory']['Question'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($item['file_names']['Theory']['Solution']))
                                            @foreach ($item['file_names']['Theory']['Solution'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($item['file_names']['Practical']['Question']))
                                            @foreach ($item['file_names']['Practical']['Question'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($item['file_names']['Practical']['Solution']))
                                            @foreach ($item['file_names']['Practical']['Solution'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>{{ $item['remarks'] }}</td>
                                        <td>
                                            @if($item['status_id'] == '2')
                                            <a class="btn btn-success"> {{ $item['status_name'] }}</a>
                                            @elseif($item['status_id'] == '3')
                                            <a class="btn btn-danger"> {{ $item['status_name'] }}</a>
                                            @elseif($item['status_id'] == '1')
                                            <a class="btn btn-warning"> {{ $item['status_name'] }}</a>
                                            @endif

                                        </td>
                                        <td>

                                            @if($item['status_id'] == '1')
                                            <a class="btn btn-primary" href="{{ url('admin/question-bank-verify',['id' => $item['id_question_bank']]) }}">Verify</a>
                                            @elseif($item['status_id'] == '2' || $item['status_id'] == '3')
                                            <a class="btn btn-secondary" href="{{ url('admin/question-bank-show',['id'=>$item['id_question_bank']]) }}">Show</a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab2">
                    <div class="text-center mb-3">
                        <h2 class="page-title py-2" style="text-align: left !important; margin-top: 1%;">Approved Question Bank List</h2>
                    </div>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @else
                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    @endif

                    <div class="table-responsive">
                        <div id="userManagementList_wrapper" class="dataTables_wrapper no-footer">
                            <table class="table table-width-style dataTable no-footer" id="approvedQuestionTable" style="width: 100%; border-bottom: white;">
                                <thead class="table-header">
                                    <tr>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">No.</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Training Partner</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Course</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Batch No.</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Theory (Question)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Theory (Solution)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Practical (Question)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Practical (Solution)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Remarks</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Status</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($outputData2 as $index => $item)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $item['company_name'] }}</td>
                                        <td>{{ $item['course_name'] }}</td>
                                        <td>{{ $item['batch_no'] }}</td>
                                        <td>
                                            @if (isset($item['file_names']['Theory']['Question']))
                                            @foreach ($item['file_names']['Theory']['Question'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($item['file_names']['Theory']['Solution']))
                                            @foreach ($item['file_names']['Theory']['Solution'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($item['file_names']['Practical']['Question']))
                                            @foreach ($item['file_names']['Practical']['Question'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($item['file_names']['Practical']['Solution']))
                                            @foreach ($item['file_names']['Practical']['Solution'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>{{ $item['remarks'] }}</td>
                                        <td>
                                            @if($item['status_id'] == '2')
                                            <a class="btn btn-success"> {{ $item['status_name'] }}</a>
                                            @elseif($item['status_id'] == '3')
                                            <a class="btn btn-danger"> {{ $item['status_name'] }}</a>
                                            @elseif($item['status_id'] == '1')
                                            <a class="btn btn-warning"> {{ $item['status_name'] }}</a>
                                            @endif

                                        </td>
                                        <td>

                                            @if($item['status_id'] == '1')
                                            <a class="btn btn-primary" href="{{ url('admin/question-bank-verify',['id' => $item['id_question_bank']]) }}">Verify</a>
                                            @elseif($item['status_id'] == '2' || $item['status_id'] == '3')
                                            <a class="btn btn-secondary" href="{{ url('admin/question-bank-show',['id'=>$item['id_question_bank']]) }}">Show</a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab3">
                    <div class="text-center mb-3">
                        <h2 class="page-title py-2" style="text-align: left !important; margin-top: 1%;">Requested Question Bank List</h2>
                    </div>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @else
                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    @endif

                    <div class="table-responsive">
                        <div id="userManagementList_wrapper" class="dataTables_wrapper no-footer">
                            <table class="table table-width-style dataTable no-footer" id="requestedQuestionTable" style="width: 100%; border-bottom: white;">
                                <thead class="table-header">
                                    <tr>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">No.</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Training Partner</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Course</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Batch No.</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Theory (Question)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Theory (Solution)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Practical (Question)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Practical (Solution)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($outputData4 as $index => $item)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $item['company_name'] }}</td>
                                        <td>{{ $item['course_name'] }}</td>
                                        <td>{{ $item['batch_no'] }}</td>
                                        <td>
                                            @if (isset($item['file_names']['Theory']['Question']))
                                            @foreach ($item['file_names']['Theory']['Question'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($item['file_names']['Theory']['Solution']))
                                            @foreach ($item['file_names']['Theory']['Solution'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($item['file_names']['Practical']['Question']))
                                            @foreach ($item['file_names']['Practical']['Question'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($item['file_names']['Practical']['Solution']))
                                            @foreach ($item['file_names']['Practical']['Solution'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>


                                        <td>

                                            <a class="btn btn-primary" href="{{ url('admin/question-bank-share',['id' => $item['id_question_bank']])  }}">Share</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab4">
                    <div class="text-center mb-3">
                        <h2 class="page-title py-2" style="text-align: left !important; margin-top: 1%;">Shared Requested Question Bank List</h2>
                    </div>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @else
                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    @endif

                    <div class="table-responsive">
                        <div id="userManagementList_wrapper" class="dataTables_wrapper no-footer">
                            <table class="table table-width-style dataTable no-footer" id="sharedQuestionList" style="width: 100%; border-bottom: white;">
                                <thead class="table-header">
                                    <tr>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">No.</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Training Partner</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Course</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Theory (Question)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Theory (Solution)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Practical (Question)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Practical (Solution)</th>
                                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($outputData3 as $index => $item)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $item['company_name'] }}</td>
                                        <td>{{ $item['course_name'] }}</td>
                                        <td>
                                            @if (isset($item['file_names']['Theory']['Question']))
                                            @foreach ($item['file_names']['Theory']['Question'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($item['file_names']['Theory']['Solution']))
                                            @foreach ($item['file_names']['Theory']['Solution'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($item['file_names']['Practical']['Question']))
                                            @foreach ($item['file_names']['Practical']['Question'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($item['file_names']['Practical']['Solution']))
                                            @foreach ($item['file_names']['Practical']['Solution'] as $fileName)
                                            {{ $fileName }}<br>
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                        </td>


                                        <td>

                                            <a class="btn btn-info" href="{{url('admin/question-bank-show',['id'=>$item['id_question_bank']])}}">Show</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

    </section>
</main>

@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment@1.1.2"></script>
<script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="{{ asset('js/dashboard/trainerCharts.js') }}"></script>
@endsection

@section('page-scripts')

<script>
    $(document).ready(function() {
        $('#submissionQuestionTable').DataTable({
            dom: 'lfrtip',
            ordering: false,
            initComplete: function() {
                this.api().columns().every(function() {
                    var column = this;
                    $('input', column.header()).on('keyup change clear', function() {
                        if (column.search() !== this.value) {
                            // Use case-insensitive search for the "Status" column (index 5)
                            column.search(this.value, false, true).draw();
                        }
                    });
                });
            },
            responsive: true,
            lengthMenu: [5, 10, 15, 20],
            pageLength: 5,
            language: {
                info: "_START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "0 from 0 to 0 entry",
                lengthMenu: " _MENU_ Entry",
                paginate: {
                    next: "Next",
                    previous: "Previous",
                },
                zeroRecords: "No record found",
            },
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#approvedQuestionTable').DataTable({
            dom: 'lfrtip',
            ordering: false,
            initComplete: function() {
                this.api().columns().every(function() {
                    var column = this;
                    $('input', column.header()).on('keyup change clear', function() {
                        if (column.search() !== this.value) {
                            // Use case-insensitive search for the "Status" column (index 5)
                            column.search(this.value, false, true).draw();
                        }
                    });
                });
            },
            responsive: true,
            lengthMenu: [5, 10, 15, 20],
            pageLength: 5,
            language: {
                info: "_START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "0 from 0 to 0 entry",
                lengthMenu: " _MENU_ Entry",
                paginate: {
                    next: "Next",
                    previous: "Previous",
                },
                zeroRecords: "No record found",
            },
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#sharedQuestionList').DataTable({
            dom: 'lfrtip',
            ordering: false,
            initComplete: function() {
                this.api().columns().every(function() {
                    var column = this;
                    $('input', column.header()).on('keyup change clear', function() {
                        if (column.search() !== this.value) {
                            // Use case-insensitive search for the "Status" column (index 5)
                            column.search(this.value, false, true).draw();
                        }
                    });
                });
            },
            responsive: true,
            lengthMenu: [5, 10, 15, 20],
            pageLength: 5,
            language: {
                info: "_START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "0 from 0 to 0 entry",
                lengthMenu: " _MENU_ Entry",
                paginate: {
                    next: "Next",
                    previous: "Previous",
                },
                zeroRecords: "No record found",
            },
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#requestedQuestionTable').DataTable({
            dom: 'lfrtip',
            ordering: false,
            initComplete: function() {
                this.api().columns().every(function() {
                    var column = this;
                    $('input', column.header()).on('keyup change clear', function() {
                        if (column.search() !== this.value) {
                            // Use case-insensitive search for the "Status" column (index 5)
                            column.search(this.value, false, true).draw();
                        }
                    });
                });
            },
            responsive: true,
            lengthMenu: [5, 10, 15, 20],
            pageLength: 5,
            language: {
                info: "_START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "0 from 0 to 0 entry",
                lengthMenu: " _MENU_ Entry",
                paginate: {
                    next: "Next",
                    previous: "Previous",
                },
                zeroRecords: "No record found",
            },
        });
    });
</script>


@endsection