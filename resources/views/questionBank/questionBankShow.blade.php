@extends('layouts.contentLayoutMaster')

{{-- Page Title --}}
@section('title', 'Question Bank')

{{-- Vendor CSS --}}
@section('vendor-styles')
{{-- Add your vendor styles here --}}
@endsection

@section('page-styles')
<style>
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    /* .dataTables_length {
        display: none;
    } */

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }

    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
        border-bottom: none;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
        border-bottom: none;
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .filter-dropdown {
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }

    /* Add styles for the table */
    .table {
        width: 100%;
        border-collapse: collapse;
        margin: 0;
    }



    .table th,
    .table td {
        padding: 10px;
        text-align: center;
        border: 1px solid #ddd;
    }

    /* Add hover effect on table rows */
    .table tbody tr:hover {
        background-color: #f5f5f5;
    }
</style>
@endsection

@section('content')
<main class="header-top-margin col-md-12 mx-auto" style="bottom: 10px;left: 100px;">


    <div class="text-center mb-3">
        <h2 class="page-title py-2" style="text-align: left !important; margin-top: 5%;">Question Bank Details</h2>
    </div>

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @else
    @if ($message = Session::get('error'))
    <div class="alert alert-danger">
        <p>{{ $message }}</p>
    </div>
    @endif
    @endif
    <div class="container">
        <div class="centered-container">
            <form id="form_login" action="{{ route('admin.questionBankReupload', ['id' => $bankid])}}" method="post" class="user-form" enctype="multipart/form-data">
                @csrf
                <div style="width: 100%; height: 100%; padding: 40px; background: white; border-radius: 16px; border: 1px #D0D5DD solid; flex-direction: column; justify-content: center; align-items: flex-end; gap: 24px; display: inline-flex">
                    <div style="align-self: stretch; text-align: center; color: #656565; font-size: 32px; font-family: Rajdhani; font-weight: 600; line-height: 38px; word-wrap: break-word">Details Question Bank</div>
                    <div style="align-self: stretch; height: 516px; flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 24px; display: flex">
                        <div style="align-self: stretch; justify-content: flex-start; align-items: center; gap: 16px; display: inline-flex">
                            <div style="flex: 1 1 0; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: inline-flex">
                                <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Training Partner</div>
                                @foreach ($outputData as $index => $item)
                                <input type="text" class="form-control" id="company_name" placeholder="" name="company_name" value="{{ $item['company_name'] }}" readonly>
                            </div>
                        </div>
                        <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
                            <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Theory (Question)</div>
                            <div style="display: flex; align-items: center;">
                                <div style="flex-shrink: 0; margin-right: 8px;">
                                    <input type="file" class="form-control" id="theoryquestion" placeholder="" name="theoryquestion" accept=".pdf, .doc, .docx, .xls, .xlsx">
                                </div>
                                <div style="flex-shrink: 1;">
                                    <label for="theoryquestion" style="margin-left: 8px;">
                                        @if (isset($item['file_names']['Theory']['Question']) && count($item['file_names']['Theory']['Question']) > 0)
                                        @foreach ($item['file_names']['Theory']['Question'] as $fileName)
                                        <a href="{{ asset('storage/uploads/' . $fileName) }}" download="_blank"><i class="fas fa-cloud-download-alt"></i></a>
                                        <i>{{ $fileName }}</i>
                                        @endforeach
                                        @else
                                        <span>No attachment</span>
                                        @endif
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
                            <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Theory (Solution)</div>
                            <div style="display: flex; align-items: center;">
                                <div style="flex-shrink: 0; margin-right: 8px;">
                                    <input type="file" class="form-control" id="theorysolution" placeholder="" name="theorysolution" accept=".pdf, .doc, .docx, .xls, .xlsx">
                                </div>
                                <div style="flex-shrink: 1;">
                                    <label for="theorysolution" style="margin-left: 8px;">
                                        @if (isset($item['file_names']['Theory']['Solution']) && count($item['file_names']['Theory']['Solution']) > 0)
                                        @foreach ($item['file_names']['Theory']['Solution'] as $fileName)
                                        <a href="{{ asset('storage/uploads/' . $fileName) }}" download="_blank"><i class="fas fa-cloud-download-alt"></i></a>
                                        <i>{{ $fileName }}</i>
                                        @endforeach
                                        @else
                                        <span>No attachment</span>
                                        @endif
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
                            <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Practical (Question)</div>
                            <div style="display: flex; align-items: center;">
                                <div style="flex-shrink: 0; margin-right: 8px;">
                                    <input type="file" class="form-control" id="practicalquestion" placeholder="" name="practicalquestion" accept=".pdf, .doc, .docx, .xls, .xlsx">
                                </div>
                                <div style="flex-shrink: 1;">
                                    <label for="practicalquestion" style="margin-left: 8px;">
                                        @if (isset($item['file_names']['Practical']['Question']) && count($item['file_names']['Practical']['Question']) > 0)
                                        @foreach ($item['file_names']['Practical']['Question'] as $fileName)
                                        <a href="{{ asset('storage/uploads/' . $fileName) }}" download="_blank"><i class="fas fa-cloud-download-alt"></i></a>
                                        <i>{{ $fileName }}</i>
                                        @endforeach
                                        @else
                                        <span>-</span>
                                        @endif
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
                            <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Practical (Solution)</div>
                            <div style="display: flex; align-items: center;">
                                <div style="flex-shrink: 0; margin-right: 8px;">
                                    <input type="file" class="form-control" id="practicalsolution" placeholder="" name="practicalsolution" accept=".pdf, .doc, .docx, .xls, .xlsx">
                                </div>
                                <div style="flex-shrink: 1;">
                                    <label for="practicalsolution" style="margin-left: 8px;">
                                        @if (isset($item['file_names']['Practical']['Solution']) && count($item['file_names']['Practical']['Solution']) > 0)
                                        @foreach ($item['file_names']['Practical']['Solution'] as $fileName)
                                        <a href="{{ asset('storage/uploads/' . $fileName) }}" download="_blank"><i class="fas fa-cloud-download-alt"></i></a>
                                        <i>{{ $fileName }}</i>
                                        @endforeach
                                        @else
                                        <span>-</span>
                                        @endif
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>

                    <div style="display: flex; align-items: center; margin: 0 auto; gap: 24px;">
                        <div>
                            <button type="button" class="btn btn-light" style="margin-right: 1%; width: 90px;" id="cancelBtn">
                                <a href="{{ route('admin.questionbank-list') }}" style="color: black; text-decoration: none;">
                                    <i class="bx bx-check d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">CANCEL</span>
                                </a>
                            </button>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-primary" style="margin-right: 1%; width: 90px;" id="saveBtn">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">UPDATE</span>
                            </button>
                        </div>
                        <div>
                            <button type="button" class="btn btn-danger" style="margin-right: 1%; color: white; width: 90px;" id="delBtn">
                                <a href="{{ url('admin/question-bank-delete',['id'=>$item['id_question_bank']]) }}" style="color: white; text-decoration: none;">
                                    <i class="bx bx-check d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">DELETE</span>
                                </a>
                            </button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
    </div>
    </div>
    </div>
</main>
@endsection

@section('vendor-scripts')
{{-- Add your vendor scripts here --}}
@endsection

@section('page-scripts')
<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>

<script>
    // Add any custom scripts here if needed
</script>
@endsection