@extends('layouts.contentLayoutMaster')

{{-- Page Title --}}
@section('title', 'Question Bank')

{{-- Vendor CSS --}}
@section('vendor-styles')
{{-- Add your vendor styles here --}}
@endsection

@section('page-styles')
<style>
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    /* .dataTables_length {
        display: none;
    } */

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }

    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
        border-bottom: none;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
        border-bottom: none;
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .filter-dropdown {
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }

    /* Add styles for the table */
    .table {
        width: 100%;
        border-collapse: collapse;
        margin: 0;
    }



    .table th,
    .table td {
        padding: 10px;
        text-align: center;
        border: 1px solid #ddd;
    }

    /* Add hover effect on table rows */
    .table tbody tr:hover {
        background-color: #f5f5f5;
    }
</style>
@endsection

@section('content')
<main class="header-top-margin col-md-12 mx-auto" style="bottom: 10px;left: 100px;">

    <div class="text-left" style="margin-top: 5%;">
        <a class="btn btn-secondary" href="{{ route('questionbankrequestlist') }}">
            <i class="fas fa-arrow-left"></i> Back
        </a>
        <h2 class="page-title py-2">Requested Question Bank</h2>
    </div>


    <div class="container">
        <div class="centered-container">

            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @else
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
            @endif
            @endif
            <div class="container">
                <div class="centered-container">


                    <form id="form_login" class="user-form" enctype="multipart/form-data">
                        @csrf

                        <div style="width: 100%; height: 100%; padding: 40px; background: white; border-radius: 16px; border: 1px #D0D5DD solid; flex-direction: column; justify-content: center; align-items: flex-end; gap: 24px; display: inline-flex">
                            <div style="align-self: stretch; text-align: center; color: #656565; font-size: 32px; font-family: Rajdhani; font-weight: 600; line-height: 38px; word-wrap: break-word">Question Bank Details</div>
                            <div style="align-self: stretch; height: 516px; flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 24px; display: flex">
                                <div style="align-self: stretch; justify-content: flex-start; align-items: center; gap: 16px; display: inline-flex">
                                    <div style="flex: 1 1 0; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: inline-flex">
                                        <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Training Course</div>
                                        @foreach ($outputData as $courseName => $data)
                                        <input type="text" class="form-control" id="course" placeholder="" name="course_name" value="{{ $courseName }}" readonly>
                                    </div>
                                </div>
                                <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
                                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Theory (Question)</div>
                                    @if (isset($data['file_names']['Theory']['Question']) && count($data['file_names']['Theory']['Question']) > 0)
                                    @foreach ($data['file_names']['Theory']['Question'] as $fileName)
                                    <a href="{{ asset('storage/uploads/' .$fileName) }}" download="_blank">{{ $fileName }}</a>
                                    @endforeach
                                    @else
                                    <span>-</span>
                                    @endif
                                </div>
                                <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
                                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Theory (Solution)</div>
                                    @if (isset($data['file_names']['Theory']['Solution']) && count($data['file_names']['Theory']['Solution']) > 0)
                                    @foreach ($data['file_names']['Theory']['Solution'] as $fileName)
                                    <a href="{{ asset('storage/uploads/' .$fileName) }}" download="_blank">{{ $fileName }}</a>
                                    @endforeach
                                    @else
                                    <span>-</span>
                                    @endif
                                </div>
                                <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
                                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Practical (Question)</div>
                                    @if (isset($data['file_names']['Practical']['Question']) && count($data['file_names']['Practical']['Question']) > 0)
                                    @foreach ($data['file_names']['Practical']['Question'] as $fileName)
                                    <a href="{{ asset('storage/uploads/' .$fileName) }}" download="_blank">{{ $fileName }}</a>
                                    @endforeach
                                    @else
                                    <span>-</span>
                                    @endif
                                </div>

                                <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
                                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Practical (Solution)</div>
                                    @if (isset($data['file_names']['Practical']['Solution']) && count($data['file_names']['Practical']['Solution']) > 0)
                                    @foreach ($data['file_names']['Practical']['Solution'] as $fileName)
                                    <a href="{{ asset('storage/uploads/' . $fileName) }}" download="_blank">{{ $fileName }}</a>
                                    @endforeach
                                    @else
                                    <span>-</span>
                                    @endif
                                </div>
                                @endforeach
                            </div>
                        </div>
                </div>

                </form>
            </div>
        </div>
    </div>
</main>
@endsection

@section('vendor-scripts')
{{-- Add your vendor scripts here --}}
@endsection

@section('page-scripts')
<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>
<script>
    function goBack() {
        window.history.back();
    }
</script>
@endsection