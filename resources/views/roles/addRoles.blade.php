@extends('layouts.contentLayoutMaster')

{{-- Page Title --}}
@section('title', 'Training Course')

{{-- Vendor CSS --}}
@section('vendor-styles')
{{-- Add your vendor styles here --}}
@endsection

@section('page-styles')
<style>
    /* Your custom page styles here */
    /* Align items to the left */
    .centered-container {
        display: flex;
        justify-content: flex-start;
        align-items: center;
        max-width: 100%;
    }

    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .title-margin {
        margin-top: 95px;
        padding-bottom: 15px;
        margin-bottom: 15px;
        margin-left: 100px;
        justify-content: flex-start;
        display: flex;
    }

    .form-row {
        display: flex !important;
    }

    /* Widen the main container */
    .container {
        max-width: 1200px; /* Adjust the width as needed */
        display: flex;
    }

    /* Style for labels */
    .form-group label {
        display: block;
        text-align: left; /* Align labels to the left */
        margin-bottom: 5px; /* Add vertical gap between label and field */
    }
</style>
@endsection

@section('content')
<main style="padding:50px;margin:4px;">

    <div class="header-top-margin">
    <h3 class="title-margin items-align-center"><b>Add New Role</b></h3>
        <div class="container">
            <div class="centered-container">
<form id="form_login" action="" method="post" class="user-form">
@csrf

<div class="form-group py-2">
    <label for="code"><b>Code</b></label>
    <input type="text" class="form-control" id="code" placeholder="code" name="code">
</div>
<div class="form-group py-2">
    <label for="role"><b>Role</b></label>
    <input type="text" class="form-control" id="role" placeholder="role" name="role">
</div>


<div class="d-flex justify-content-center">                           
    <button type="submit" class="btn btn-primary" id="hantarBtn">
        <i class="bx bx-check d-block d-sm-none"></i>
        <span class="d-none d-sm-block">submit</span>
    </button>
    <div>
        <a href="{{ route('roleList') }}" class="btn btn-secondary-6 btn-block">Cancel</a>
    </div>
</div>

</form>
</div>
</div>
</div>
</main>
@endsection

@section('vendor-scripts')
{{-- Add your vendor scripts here --}}
@endsection

@section('page-scripts')
<script>
    let host = "{{ env('API_SERVER') }}";

</script>

<script>
    // Add any custom scripts here if needed

</script>
@endsection
