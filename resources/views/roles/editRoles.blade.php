@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Role Management')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection
@section('page-styles')
<style>
  thead input {
    width: 100%;
  }

  .table thead {
    text-transform: capitalize;
  }

  .header-color {
    color: #2B6AB2 !important;
  }


</style>
@endsection

@section('content')
<main class="" style="padding-top:50px;padding-bottom:100px;">
  <section class="header-top-margin">


    

          
  </section>
</main>


  @endsection

  @section('vendor-scripts')
  <script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
  <script src="{{asset('vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
  
  @endsection

  @section('page-scripts')
  <script>
    let userID = "{{ $userid }}";
    let host = "{{ env('API_SERVER') }}";
   
  </script>


  <!-- <script src="{{ asset('js/book') }}" ></script> -->
 
  @endsection

  