@extends('layouts.contentLayoutLogin')
{{-- page Title --}}
@section('title','Login')
{{-- vendor css --}}
@section('page-styles')

<style>

a,
a:hover {
    color: #2c2c2c;
    text-decoration: none;
}

a:hover {
    color: #5b5c5c;
    text-decoration: none;
}


.noborderlogin{
    display: block;
    width: 500px;
    margin-top:100px;
    flex-direction: column;
    align-items: center;
    gap: var(--spacing-32, 32px);
    
    background: #fff; 
}

.borderlogin{
    display: block;
    width: 500px;
    margin-top:30px;
    margin-bottom: 15%;
    flex-direction: column;
    align-items: center;
    gap: var(--spacing-32, 32px);
    border-radius: 17px;
    border: 1px solid var(--primary-500-base-color, #4B94D8);
    background: #fff; 
}

.title_header{
    color: #656565;

    text-align: center;
    font-family: Rajdhani;
    font-size: 32px;
    font-style: normal;
    font-weight: 700;
    line-height: 38px; /* 118.75% */
}
    .title{
        color: #656565;

    text-align: center;
    font-family: 'Times New Roman', Times, serif;
    font-size: 32px;
    font-style: normal;
    font-weight: 700;
    line-height: 38px; /* 118.75% */
    }


.form-label {

align-items: left;  
display: flex;
align-items: center;
gap: 8px;
flex: 1 0 0;

}

.form-control {
        width: 100%; /* Full width input fields */
        margin-right: 0; /* Remove right margin */
    }

    .form-group {
        display: flex;
        flex-direction: column; /* Stack elements vertically */
        align-items: center; /* Center horizontally */
    }
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@endsection


@section('content')
     <div class="loading-overlay" id="page_loader">
        <div class="loader"></div>
        <div class="loader-text"></div>
    </div>

    <main class="form-signin" style="over-flow:auto; max-width: 600px !important;">
    <div class="noborderlogin">
    </div>
    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                  @else
                      @if ($message = Session::get('error'))
                          <div class="alert alert-danger mx-auto">
                              <p>{{ $message }}</p>
                          </div>
                      @endif
                  @endif
    <div class="borderlogin" style="justify-content: center;">
        <div style="margin:30px;">
            <form id="form_login">
                @csrf
                <p class="mt-3 mb-3 fw-bold title">VERIFY CERTIFICATE</p>
                <div>
                    <label for="ic_no">Mykad / Passport Number</label>
                    <input type="text" class="form-control" id="ic_no" name="ic_no" placeholder="Mykad/Passport">
                </div>
                <div class="">
                    <label for="certificate_no">Certificate Number</label>
                    <input type="text" class="form-control" id="certificate_no" name="certificate_no" placeholder="Certificate Number">
                </div>
                <br>
                <div style="align-content: center">
                    <button type="submit" class="w-75 btn btn-primary text-white fw-bold btn-round mt-2" id="verifyBtn">Verify</button>
                </div>
            </form>
        </div>
    </div>                

        <div class="response-data">
        <p class="mt-3 mb-3 fw-bold title">RESULT</p>

                <div class="form-group">
                    <label for="fullname" style="font-weight: bold;">Name</label>
                    <input type="text" class="form-control" id="fullname" name="fullname"  readonly>
                </div>
                <div class="form-group">
                    <label for="ic_no" style="font-weight: bold;">Mykad / Passport Number</label>
                    <input type="text" class="form-control" id="ic_no" name="ic_no"  readonly>
                </div>
                <div class="form-group">
                    <label for="certificate_category" style="font-weight: bold;">Certificate Category</label>
                    <input type="text" class="form-control" id="certificate_category" name="certificate_category"  readonly>
                </div>
                <div class="form-group">
                    <label for="certificate_no" style="font-weight: bold;">Certificate Number</label>
                    <input type="text" class="form-control" id="certificate_no" name="certificate_no" readonly>
                </div>
                <div class="form-group">
                    <label for="institute" style="font-weight: bold;">Institute</label>
                    <input type="text" class="form-control" id="institute" name="institute" readonly>
                </div>
                <div class="form-group">
                    <label for="batch" style="font-weight: bold;">Batch</label>
                    <input type="text" class="form-control" id="batch" name="batch" readonly>
                </div>
               
    
</main>

 @endsection
    
@section('page-scripts')

<script>
    $(document).ready(function () {
            var trainerForm = $('#form_login');
            var icNoField = $('#ic_no');
            var certificateNoField = $('#certificate_no');
            var fullnameField = $('#fullname');
            var certificateCategoryField = $('#certificate_category');
            var batchField = $('#batch');
            var instituteField = $('#institute');


            trainerForm.submit(function (e) {
                e.preventDefault();
            
                var formData = trainerForm.serialize();
                $.ajax({
                    type: 'POST',
                    url: "{{ route('verifyCertificate') }}",
                    data: formData,
                    success: function (response) {
                        if (response.status === 'OK') {
                            
                            // Assuming the nested data is under the 'result' key
                            icNoField.val(response.result.mykad_no);
                            certificateNoField.val(response.result.certificate_no);
                            fullnameField.val(response.result.name);
                            certificateCategoryField.val(response.result.certificate_category);
                            batchField.val(response.result.batch);  // Ensure 'batch' is present in the response

                            trainerRadioButton.prop('checked', true).trigger('change');
                        } else {
                            alert('Record Not Found.');
                        }
                    },
                    error: function () {
                        alert('Error during API call.');
                    }
                });
            });
        });

</script>

@endsection
