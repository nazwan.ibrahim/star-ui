@extends('layouts.contentLayoutLogin')
{{-- page Title --}}
@section('title','Login')
{{-- vendor css --}}
@section('page-styles')
<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<style>

a,
a:hover {
    color: #2c2c2c;
    text-decoration: none;
}

a:hover {
    color: #5b5c5c;
    text-decoration: none;
}


.noborderlogin{
    display: block;
    width: 500px;
    margin-top:100px;
    flex-direction: column;
    align-items: center;
    gap: var(--spacing-32, 32px);
    
    background: #fff; 
}

.borderlogin{
    display: block;
    width: 500px;
    margin-top:30px;
    margin-bottom:20px;
    flex-direction: column;
    align-items: center;
    gap: var(--spacing-32, 32px);
    border-radius: 17px;
    border: 1px solid var(--primary-500-base-color, #4B94D8);
    background: #fff; 
}

.page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

.title_header{
    color: #656565;

    text-align: center;
    font-family: Rajdhani;
    font-size: 32px;
    font-style: normal;
    font-weight: 700;
    line-height: 38px; /* 118.75% */
}
    .title{
    color: #656565;
    text-align: center;
    font-family: Rajdhani;
    font-size: 32px;
    font-style: normal;
    font-weight: 700;
    line-height: 38px; /* 118.75% */
    }


.form-label {

align-items: left;  
display: flex;
align-items: center;
gap: 8px;
flex: 1 0 0;

}
</style>

@endsection


@section('content')
     <div class="loading-overlay" id="page_loader">
        <div class="loader"></div>
        <div class="loader-text"></div>
    </div>

    <main class="form-signin" style="over-flow:auto; max-width: 600px !important;">
        
        <div class="noborderlogin"><p class="mt-3 mb-3 fw-bold page-title">SEDA Training and Registration System (STAR)</p></div>
            <div class=" borderlogin">
                <div style="margin:30px;">
                    <form id="form_login" method="post" action="{{ route('authenticate') }}">
        
                        @csrf
                        {{--         
                        <div style="margin-top:-20px; ">
                            <a href="#"><img src="{{ asset('images/Big_title.png') }}"  alt="STAR" width="100%" height="100%"></a>
                        </div> --}}
                        <h4 class="mt-3 mb-3 fw-bold contentUpdate" style="text-align: center;">Login</h4>  

                        @if ($message = Session::get('success'))
                            <div style="align-content: center;text-align: center;" class="alert alert-success" id="successAlert">
                                <p>{{ $message }}</p>
                            </div>
                        @else
                            @if ($message = Session::get('error'))
                                <div style="align-content: center;text-align: center;" class="alert alert-danger" id="errorAlert">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                        @endif   
                        <div>
                            <label for="floatingInput" style="margin-bottom: 2%;">Email</label>
                            <input type="text" class="form-control" style="margin-bottom: 2%;" id="email" name="email"  placeholder="Email">
                            {{-- <label for="floatingInput">Email</label> --}}
                        </div>
                        <div class="">
                            <label for="floatingInput" style="margin-bottom: 2%;">Password</label> 
                            <input type="password" class="form-control" style="margin-bottom: 2%;"r id="password" name="password"  placeholder="Password">
                            
                            <div class="small float-start font-size: 12px;">
                                <label>
                                    
                                    <input type="checkbox"  id="togglePassword" onclick="showPassword()" > Show Password
                                </label>         
                            </div>     
                        </div>
                        <br>
                  
                        <div style="align-content: center;text-align: center;"><button type="submit" class="w-75 btn btn-primary text-white fw-bold btn-round mt-2">Log In</button></div>        
                    </form>
                   
                        <hr>
                        <p class="mt-2">
                        <div class="small fw-bold float-start">
                                <label><input type="checkbox" name="remember"> Remember Me</label>
                            </div>

                            <div class="float-end mt-n1">
                                <a href="{{ route('forgot-password') }}">Forgot password?</a> 
                            </div>     
                        </p>
                        <br/>
                        <div class="mt-3" style="border: 2px dashed #2c2c2c; border-radius: 5px; padding: 5px; text-align: center;">
                            Don't have an account? <a href="{{ route('user-register') }}" class="fw-bold">Sign Up</a>   
                        </div>
                    
                </div>
     
            
            </div>
        </div>
    
    </main>
 @endsection
    
@section('page-scripts')
<script>
        function showPassword() {
        const togglePassword = document.querySelector("#togglePassword");
        const x = document.getElementById("password");
    
        if (x.type === "password") {
            x.type = "text";
            togglePassword.classList.toggle("bxs-show");
        } else {
            x.type = "password";
        }
        }

        setTimeout(function() {
        var successAlert = document.getElementById('successAlert');
        var errorAlert = document.getElementById('errorAlert');

        if (successAlert !== null) {
            successAlert.style.display = 'none';
        }

        if (errorAlert !== null) {
            errorAlert.style.display = 'none';
        }
    }, 5000);


    
</script>
@endsection
