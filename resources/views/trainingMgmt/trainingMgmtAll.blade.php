@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Training Management Training Partner')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.5.2/dist/js/bootstrap.bundle.min.js"></script>

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.icon-editIcon:before {
  content: "\e916";
}
.contentUpdate{
    color: #454545;
    font-family: Open Sans;
    font-size: 18px;
    font-style: normal;
    font-weight: 600;
    line-height: 28px; /* 155.556% */
    text-align: left;
}

.table-row td {
        padding: 10px;
        border-bottom: none;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

.table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

.icon-editIcon:before {
  content: "\e916";
}

.fully-booked-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .closed-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .open-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
        border-bottom: none;
    }

     /* Add styles for the table */
     .table {
    width: 100%;
    border-collapse: collapse;
    margin: 0;
    }

   

    .table th, .table td {
    padding: 10px;
    text-align: center;
    border: 1px solid #ddd;
    }

    /* Add hover effect on table rows */
    .table tbody tr:hover {
    background-color: #f5f5f5;
}

</style>
@endsection

@section('content')
<main>
    <section class="header-top-margin">
    @if ($message = Session::get('success'))
         <div style="align-content: center;text-align: center;" class="alert alert-success" id="successAlert">
             <p>{{ $message }}</p>
         </div>
     @else
         @if ($message = Session::get('error'))
             <div style="align-content: center;text-align: center;" class="alert alert-danger" id="errorAlert">
                 <p>{{ $message }}</p>
             </div>
         @endif
     @endif  
    <div class="row">
        <h2 class="page-title py-2" style="text-align: left !important; margin-left: 5%;">Training List</h2>
    </div>
</br>

                  @csrf    
    <div class="table-responsive col-md-12 mx-auto">

        <table class="table table-striped dataTable no-footer" id="trainingManagementListAll" style="width: 100%; border-bottom: white;">

            {{-- <thead class="table-header">
                <tr>
                    <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;" width=40;>No.</th>
                    <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Partner Name</th>
                    <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Training Course</th>
                    <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Course Category</th>
                    <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;" width=55;>Training Type</th>
                    <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;" width=35;>Training Date (Start)</th>
                    <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;" width=35;>Training Date (End)</th>
                    <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Status</th>
                    <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;" width=25;>Action</th>
                </tr>
            </thead> --}}
            <thead class="table-header">
              <tr>
                  <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;" width=40;>No.</th>
                  <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Partner Name</th>
                  <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Training Course</th>
                 
                  <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Course Category</th>
                  <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Training Type</th>
                  <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Batch No</th>
                  <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Training Date</th>
                  <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Endorsed Status</th>
                  <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Status</th>
                  <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Action</th>
              </tr>
          </thead>

            <tbody>
            </tbody>

        </table>
   
    </section>
</main>

<div class="modal fade" id="attendModal" tabindex="-1" role="dialog" aria-labelledby="attendModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="attendModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalButton">
            <span aria-hidden="true">&times;</span>
            </button>
      </div>
      
      <div class="modal-body">
        <!-- Place your attend form here -->
        <!-- Example form: -->
        <form id="updateAttendForm" style="text-align: left">
          <!-- Form fields go here -->
          <h5 class="modal-title" id="attendModalLabel"></h5>
          <div class="row">
            <div class="card mt-7">
            <div>
                <span class="contentUpdate">Training Name:</span>&nbsp;<span id="training_name" name="training_name"></span>
              </div>
              <div>
                <span class="contentUpdate">Conducted By:</span>&nbsp;<span id="company_name" name="company_name"></span>
              </div>
              <div>
                <span class="contentUpdate">Current Start Date:</span>&nbsp;<span id="date_start" name="date_start"></span>
              </div>
              <div>
                <span class="contentUpdate">Current End Date :</span>&nbsp;<span id="date_end" name="date_end"></span>
              </div>
              <div>
                <span class="contentUpdate">Requested By :</span>&nbsp;<span id="fullname" name="fullname"></span>
              </div>
            </div>
            <div class="form-group">
              <label for="newStartDate">New Start Date</label>
              <input type="date" class="form-control" id="newStartDate" name="newStartDate">
            </div>
            <div class="form-group">
              <label for="newEndDate">New End Date</label>
              <input type="date" class="form-control" id="newEndDate" name="newEndDate">
            </div>
            <div class="form-group">
              <label for="remarks">Remarks</label>
              <textarea class="form-control" id="remarks" name="remarks"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeModalButtonFooter">Cancel</button>
            <button type="submit" class="btn btn-primary" id="confirmUpdate">Confirm</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

  
<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="successModalLabel">Success</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeModalButtonSuccess">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="successMessage">Data updated successfully.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
<script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>

    <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script> 
    
@endsection



@section('page-scripts')

<script>
  $(document).ready(function () {
    // Add an event listener to the close button
    $('#closeModalButton').on('click', function () {
      $('#attendModal').modal('hide');
    });
  });

  $('#closeModalButtonFooter').on('click', function () {
      $('#attendModal').modal('hide');
    });

    $('#closeModalButtonSuccess').on('click', function () {
      $('#successModal').modal('hide');
    });


    setTimeout(function() {
        var successAlert = document.getElementById('successAlert');
        var errorAlert = document.getElementById('errorAlert');

        if (successAlert !== null) {
            successAlert.style.display = 'none';
        }

        if (errorAlert !== null) {
            errorAlert.style.display = 'none';
        }
    }, 5000);
</script>
</script>
<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";

</script>


<script src="{{ asset('js/trainingMgmt/trainingManagementListAll.js') }}"></script>
@endsection
