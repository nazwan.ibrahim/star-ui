@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','TP - Training Report')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .content-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 20px;
        font-style: normal;
        font-weight: 600;
    }

    .contentUpdate{
        color: #454545;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
        line-height: 28px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.form-group {
    display: flex;
    align-items: center; /* Vertical alignment */
}

label {
    width: 100px; /* Adjust as needed for your design */
    text-align: left; /* Adjust for alignment (left, right, center) */
    /* margin-right: 10px; Spacing between label and input */
}


.fullBorder {
  border: 1px solid rgb(242, 244, 247);
  border-radius: 5px;
  padding: 10px 10px 10px 10px;
}
</style>
@endsection

@section('content')
<!-- Navbar -->
<!-- <nav class="navbar navbar-expand-lg"> -->


<!-- End Navbar -->

<main class="header-top-margin" style="justify-content: center; display: flex; margin-top:-5px;">
    <section class="col-md-11 header-top-margin">
        @php
    $lastParameter = request()->segment(count(request()->segments()));
    $segments = request()->segments();
    $secondLastParameter = count($segments) >= 2 ? $segments[count($segments) - 2] : null;
   
     @endphp   
    
    @foreach ($reportcheck as $rep) 
   
<div class="container col-md-12" style="margin-top:20px;">
    <ol class="breadcrumb p-0 mb-0 poppins-semibold-12 light-grey">
        <li class="nav-item mg-sm ">
            @if ($rep->endorse_attendance == 1)
                <span style="float:left;margin:8px 1px 1px 1px">
                    <input type="checkbox" id="myCheckbox" name="myCheckbox" checked disabled>
                </span>
                <a class="nav-link" href="{{ route('admin.attendance', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">
                    Attendance List
                    <span class="sr-only">(current)</span>
                </a>
            @elseif ($rep->endorse_attendance !== 1 || $rep->endorse_attendance == '')
                <a class="nav-link" href="{{ route('admin.attendance', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">
                    Attendance List
                    <span class="sr-only">(current)</span>
                </a>
            @endif
        </li>

        <li class="nav-item mg-sm ">
            @if ($rep->endorse_feedback == 1)
                <span style="float:left;margin:8px 1px 1px 1px">
                    <input type="checkbox" id="myCheckbox" name="myCheckbox" checked disabled>
                </span>
                <a class="nav-link" href="{{ route('admin-feedbacklist', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">
                    Feedback
                    <span class="sr-only">(current)</span>
                </a>
            @elseif ($rep->endorse_feedback !== 1 || $rep->endorse_feedback == '')
                <a class="nav-link" href="{{ route('admin-feedbacklist', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">
                    Feedback
                    <span class="sr-only">(current)</span>
                </a>
            @endif
        </li>     

        <li class="nav-item mg-sm ">
            @if (($rep->endorse_exam == 1) && ($rep->id_training_type == '2'))
            <span style="float:left;margin:8px 1px 1px 1px">
                <input type="checkbox" id="myCheckbox" name="myCheckbox" checked disabled>
            </span>
            <a class="nav-link" href="{{ route('endorse-exam-result2', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">
                Exam Result
                <span class="sr-only">(current)</span>
            </a>
            @elseif (($rep->endorse_exam !== 1 || $rep->endorse_exam == '') && ($rep->id_training_type == '2'))
            <a class="nav-link" href="{{ route('endorse-exam-result2', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">
                Exam Result
                <span class="sr-only">(current)</span>
            </a>
            @endif
        </li>

        <li class="nav-item mg-sm ">
          @if ($rep->endorse_report == 1)
          <span style="float:left;margin:8px 1px 1px 1px">
              <input type="checkbox" id="myCheckbox" name="myCheckbox" checked disabled>
          </span>
          <a class="nav-link" href="{{ route('admin-TrainingReport-TM', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">
              Training Report
              <span class="sr-only">(current)</span>
          </a>
          @elseif ($rep->endorse_report !== 1 || $rep->endorse_report == '')
          <a class="nav-link" href="{{ route('admin-TrainingReport-TM', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">
              Training Report
              <span class="sr-only">(current)</span>
          </a>
          @endif
        </li>

        <li class="nav-item mg-sm ">
          @if ($rep->endorse_report == 1)
          <span style="float:left;margin:8px 1px 1px 1px">
              <input type="checkbox" id="myCheckbox" name="myCheckbox" checked disabled>
          </span>
          <a class="nav-link" href="{{ route('Admin-Payment-to-seda', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">
              Payment To Seda
              <span class="sr-only">(current)</span>
          </a>
          @elseif ($rep->endorse_report !== 1 || $rep->endorse_report == '')
          <a class="nav-link" href="{{ route('Admin-Payment-to-seda', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">
              Payment To Seda
              <span class="sr-only">(current)</span>
          </a>
          @endif
        </li>
    </ol>
</div>
@endforeach
      <div>
        
    <div class="card mt-7" style="padding:50px;margin:50px;background-color:#fbf9f9">
    
    {{-- <form method="POST" action="/submit">
        @csrf --}}
        
        <div class="form-group">
            <label class="page-title" style="white-space: nowrap;">Training Report</label>
        </div>
    
        <div class="row">
        <div>
    
    
        <div class="row">
                <div class="col-auto" style="position: relative;">
                    <h4 class="user-name mb-0"><span id="trainingName"></span></h4>
                 </div>
    
            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="contentUpdate" for="name">Provider :</label>
                        <span class="contentUpdate" id="providerCompany"></span>
                    </div>
                </div>  
            </div>
    
            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="contentUpdate" for="name">Start Date :</label>
                        <span class="contentUpdate" id="dateStart"></span>
                    </div>
                </div>
            </div>
    
            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="contentUpdate" for="name">End Date :</label>
                        <span class="contentUpdate" id="dateEnd"></span>
                    </div>
                </div>
            </div>

            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="contentUpdate" for="name">Venue :</label>
                        <span class="contentUpdate" id="Venueid"></span>
                    </div>
                </div>
            </div>
    
            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate" id="dateStart">Training Name: </span>
                        <span class="contentUpdate" id="courseName" style="margin-left:2.5em;"></span>
                    </div>
                </div>
            </div>
    
            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate">Training Code : </span>
                        <span class="contentUpdate" id="courseCode" style="margin-left:2.5em;"> </span>
                    </div>
                </div>
            </div>


            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate">Training Batch :  </span>
                        <span class="contentUpdate" id="BatchNumber" style="margin-left:2.5em;"> </span>
                       
                    </div>
                </div>
            </div>
    

            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate">Competent Trainers :</span>
                        <span class="contentUpdate" id="CompetenttrainerName" style="margin-left:2.5em;"></span>
                    </div>
                </div>
            </div>    
    
            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate">Synopsis :  </span>
                        <span class="contentUpdate" id="training_description" style="margin-left:2.5em;"></span>
                    </div>
                </div>
            </div>

            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate">Introduction :  </span>
                        <span class="contentUpdate" id="training_introduction" style="margin-left:2.5em;"></span>
                    </div>
                </div>
            </div>
    
          
    
            <label class="content-title" style="margin: 1%;">Results and Conclusion</label>
    

    
            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate">Conclusion :  </span>
                        <span class="contentUpdate" id="training_conclusion" style="margin-left:2.5em;"></span>
                    </div>
                </div>
            </div>
    
            
            <div class="row" style="margin-top: 1%;margin-left: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate"  style="margin: 2%;">APPENDIX 1 - Details of result : </span>
                        <span class="contentUpdate" id="ExamResultCoverImg"></span> <div class="form-group"> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 1%;margin-left: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate"  style="margin: 2%;">APPENDIX 2 - Details of report : </span>
                        <span class="contentUpdate" id="ReportCoverImg"></span> <div class="form-group"> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 1%;margin-left: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate"  style="margin: 2%;">APPENDIX 3 - Details of receipt : </span>
                        <span class="contentUpdate" id="ReceiptCoverImg"></span> <div class="form-group"> 
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="row" style="margin-top: 1%;margin-left: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate"  style="margin: 2%;">APPENDIX 4 - Schedule of activities :  </span>
                        <span class="contentUpdate" id="ActivityCoverImg"></span> <div class="form-group"> 
                        </div>
                    </div>
                </div>
            </div>
            
           
           
                <div class="d-flex justify-content-end pt-3 pb-2">
                    <div style="margin: 1%;">
                        <a href="" class="btn btn-secondary-6 btn-block">Back</a>
                        
                    </div>
        
                    {{-- <div style="margin: 1%;">
                        <a href="" class="btn btn-secondary-6 btn-block">Submit</a>
                    </div> --}}
                </div>
            </div>

            <div class="form-group">
                <div class="fullBorder"
                    style="background-color: #e7ebef;margin-top:1rem;">
                    <div id=""
                            style="padding: 5px;font-style: italic;font-size: 12px;">
                            Endorsed By: <span id="EndorseBy"></span>  Endorsed Date: <span id="EndorseDate"></span> 
                        </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</main>
@endsection



@section('page-scripts')
<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>

<script src="{{ asset('js/trainingMgmt/trainingReportAdmin.js') }}"></script>

@endsection


@section('vendor-scripts')

@endsection