@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Training Report')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .dataTables_length {
        display: none;
    }

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .filter-dropdown{
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }
    
    .btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.title{
    color: #656565;
    font-family: Rajdhani;
    font-size: 26px;
    font-style: normal;
    font-weight: 600;
    line-height: 38px; /* 146.154% */
}

.contentUpdate{
    color: #454545;
    font-family: Open Sans;
    font-size: 18px;
    font-style: normal;
    font-weight: 600;
    line-height: 28px; /* 155.556% */
}
</style>
@endsection

@section('content')
<main class="header-top-margin" style="justify-content: center; display: flex;">

    @php
    $lastParameter = request()->segment(count(request()->segments()));
    $segments = request()->segments();
    $secondLastParameter = count($segments) >= 2 ? $segments[count($segments) - 2] : null;
    
    @endphp
<section class="col-md-11">
    <div class="container col-md-12" style="margin-top:20px;">
        <ol class="breadcrumb p-0 mb-0 poppins-semibold-12 light-grey">
                 
                <li class="nav-item mg-sm ">
                    {{-- <a class="nav-link" href="{{ route('admin.attendance',$lastParameter) }}">Attendance List</a> --}}
                    <a class="nav-link" href="{{ route('TP.attendance', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Attendance List</a>
                        <span class="sr-only">(current)</span>
                    </a>
                </li>      
                <li class="nav-item mg-sm ">
                    <a class="nav-link" href="{{ route('TP-feedbacklist', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Feedback</a>
                    {{-- <a class="nav-link" href="{{ route('trainingpartner.feedbackquestion',$lastParameter) }}">Feedback</a> --}}
                        <span class="sr-only">(current)</span>
                    </a>
                </li>    
                @foreach ($reportcheck as $rep) 
                  @if (($rep->id_training_type == '2')) 
                    <li class="nav-item mg-sm ">
                        <a class="nav-link" href="{{ route('endorse-exam-resultTP', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Submit Exam Result</a>
                        {{-- <a class="nav-link" href="{{ route('TP-UpdateExamResult-List', $secondLastParameter) }}">Endorse Exam Result --}}
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                  @endif
                @endforeach
                <li class="nav-item mg-sm ">
                    <a class="nav-link" href="{{ route('TP-TrainingReport-TM', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Training Report</a>
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item mg-sm ">
                  <a class="nav-link" href="{{ route('TP-Payment-to-seda', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Payment To Seda</a>
                      <span class="sr-only">(current)</span>
                  </a>
              </li>
        </ol>
    </div>

    <div class="row">
        <h2 class="page-title py-2" style="text-align: left !important; margin-bottom: 1%;margin-top: 1%;"></h2>
      </div>
      <div class="row">
        <h2 class="page-title py-2" style="text-align: left !important; margin-bottom: 1%;">Feedback</h2>
      </div>
    <div class="card mt-7" style="text-align: left; background-color: #fbf9f9; padding: 2%; margin-bottom: 2%;">
      <div class="row">
          
              <div><span class="contentUpdate">Provider:</span>&nbsp;&nbsp;<span class='title' id="provider_name"></span></div>
              <div><span class="contentUpdate">Start Date :</span>&nbsp;&nbsp;<span id="date_start"></span></div>
              <div><span class="contentUpdate">End Date :</span>&nbsp;&nbsp;<span id="date_end"></span></div>
              <div><span class="contentUpdate">Venue :</span>&nbsp;&nbsp;<span  id="venue"></span></div>
         
      </div>
    </div>

    <div class="table-responsive">
        <div id="trainingReportList_wrapper" class="dataTables_wrapper no-footer">
            <table class="table table-bordered table-striped dataTable no-footer" id="feedbackReportList" style="width: 100%;">
                <thead class="table-header">
                    <tr>
                        <th class="table-title text-center" width="40">No.</th>
                        <th class="table-title text-center">Participant Name</th>
                        <th class="table-title text-center">MyKad/Passport No</th>
                        <th class="table-title text-center">Feedback Status</th>
                        <th class="table-title text-center">Submitted Status</th>        
                        <th class="table-title text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="d-flex justify-content-end">
                <div style="margin:10px;">
                  <a href="" class="btn btn-secondary-6 btn-block">Cancel</a>
                </div>
                <div style="margin:10px;">
                  <button  id="submitToSeda" class="btn btn-secondary-6 btn-block">
                   Submit
                  </button>
                </div>   
            </div>
        </div>
    </div>
</main>
</section>


<!-- ENDORSE -->
<section>
<div class="modal" id="confirmationEndorseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      
      <div class="modal-content">
        <div class="modal-header" style="padding:1.3rem;" id="kemaskini-jenis-cenderamata">
          <h4 class="modal-title text-3" id="myModalLabel33">Submit Participant Feedback
          </h4>
          <button type="button" class="close model-remove-icon-bg" data-dismiss="modal" aria-label="Close">
            <i class="bx bx-x modal-close-icon-1"></i>
          </button>
        </div>

        <div class="modal-body" style="padding: 1.3rem 1.3rem 0 1.3rem;">
          <div class="modal-content-border">
            <form id="formEndorseResult">
              
                <div class="row">
                    <div class="col-sm-12">
                      <p class="mb-0">
                        Are you sure you want to submit these participants feedback?

                      </p>
                    </div>
              
            </form>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
            <span class="d-none d-sm-block">No</span>
          </button>
         
          <button type="button" class="btn btn-primary ml-1" id="submitToEndorse">
            <i class="bx bx-check d-block d-sm-none"></i>
            <span id="spanSimpan" class="d-none d-sm-block">Yes</span>
          </button>
        </div>


        
      </div>
    </div>
  </div> 
</section>

<section>
    <div class="modal" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        
        <div class="modal-content">
          <div class="modal-header" style="padding:1.3rem;">
            <h4 class="modal-title text-3" id="myModalLabel33">
            </h4>
        
          </div>

          <div class="modal-body" style="padding: 1.3rem 1.3rem 0 1.3rem;">
            <div class="modal-content-border">
              <form id="formEndorseResult">
                
                  <div class="row">
                      <div class="col-sm-12">
                        <p class="mb-0">
                          Successfully submitted the participant feedback.

                        </p>
                      </div>
                
              </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
              <span class="d-none d-sm-block">close</span>
            </button>
           
           
          </div>


          
        </div>
      </div>
    </div>
   </section>


@endsection


@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    
@endsection



@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>

<script src="{{ asset('js/trainingMgmt/feedbackParticipantList.js') }}"></script>

@endsection