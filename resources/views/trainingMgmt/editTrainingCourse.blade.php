@extends('layouts.contentLayoutMaster')

{{-- Page Title --}}
@section('title', 'Edit Training Course')

@section('vendor-styles')
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .centered-container {
        display: flex;
        justify-content: flex-start;
        align-items: center;
        max-width: 100%;
    }

    .header-top-margin {
        margin-top: 7%;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .title-margin {
        margin-top: 95px;
        padding-bottom: 15px;
        margin-bottom: 15px;
        margin-left: 100px;
        justify-content: flex-start;
        display: flex;
    }

    .field-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .checkbox-label {
        color: #454545;
        font-family: Inter;
        font-size: 18px;
        font-style: normal;
        font-weight: 500;
        line-height: 28px;
    }

    .checkbox-size {
        display: flex;
        width: 32px;
        height: 32px;
        justify-content: center;
        align-items: center;
        flex-shrink: 0;
    }


    .form-row {  
        width: 80%;
        display: flex !important;

    }

    .date-field-size {
        display: flex !important;
        width: 50%;
    }

    /* Widen the main container */
    .container {
        max-width: 100%;
        /* Adjust the width as needed */
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    /* Style for labels */
    .form-group label {
        display: block;
        text-align: left;
        /* Align labels to the left */
        margin-bottom: 5px;
        /* Add vertical gap between label and field */
    }
</style>
@endsection

@section('content')
<main>
    <div class="header-top-margin">
        <h3 class="page-title title-margin" style="margin-top: 5%;"><b>Edit Training Course</b></h3>
        <div class="col-md-12" style="display: flex; justify-content: left; align-items: left;">
            <div class="col-md-12">
                <form action="{{ route('admin.update-training-course', ['id' => $courselist[0]->id_training_course]) }}" method="post">
                    @csrf
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @else
                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    @endif
                    </br>
                    <div class="form-row">
                        <!-- Course Name -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="courseName"><b>Course Name</b></label>
                            <input type="text" class="form-control" id="courseName" placeholder="Please Insert Course Name" name="course_name" value="{{ $courselist[0]->course_name ?? '' }}" oninput="updateInput(this)">
                        </div>

                        <!-- Course Category -->
                        <div class="form-group col-md-6 py-2">
                            <label for="partnerName"><b>Course Category</b></label>
                            <select class="form-control" id="categoryName" name="course_category" onchange="toggleRetypesVisibility()" required>
                            <option> -- Please Select -- </option>
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}" @if(isset($courselist[0]) && $courselist[0]->id_course_category == $category->id) selected @endif>
                                    {{ $category->course_name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    @if($selectedReTypes)
                    <div class="form-group col-md-6 py-2" id="retypes" class="form-row">
                            <label for="retype"><b>RE Type</b></label>
                            <div class="col-md-5">
                                @forelse($re_types as $re_type)
                                    <div class="form-check">
                                        <div class="row">
                                            <div class="col-md-4" style="margin-bottom: 0.7%;">
                                                <input class="form-check-input" multiple type="checkbox" value="{{ $re_type->id }}" id="categoryCheckbox{{ $re_type->id }}" name="re_types[]" @if(in_array($re_type->id, $selectedReTypes)) checked @endif>
                                            </div>
                                            <div class="col-md-8" style="margin-bottom: 0.7%;">
                                                <label class="form-check-label" for="categoryCheckbox{{ $re_type->id }}">
                                                    {{ $re_type->re_type }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                <p>No RE types available</p>
                            @endforelse
                        </div>
                    </div>
                    @endif

                    <div class="form-row">                      
                        <div class="form-group col-md-6 py-2">
                            <label for="training_option"><b>Training Option</b></label>
                            <select class="form-control" id="training_option" name="training_option" onchange="toggleTpaVisibility()" required disabled>
                                <option> -- Please Select -- </option>
                                @foreach($options as $option)
                                <option value="{{ $option->id }}" @if(isset($courselist[0]) && $courselist[0]->id_training_option == $option->id) selected @endif >
                                {{ $option->option }}  
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label for="trainingType"><b>Training Type</b></label>
                            <select class="form-control" id="trainingType" name="training_type">
                                <option> -- Please Select -- </option>
                                @foreach($types as $type)
                                <option value="{{ $type->id }}" @if(isset($courselist[0]) && $courselist[0]->id_training_type == $type->id) selected @endif>
                                    {{ $type->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label for="training_cert_code"><b>Certificate Code</b></label>
                            <select class="form-control" id="training_cert_code" name="training_cert_code">
                                <option> -- Please Select -- </option>
                                @foreach($certs as $cert)
                                <option value="{{ $cert->id }}" @if(isset($courselist[0]) && $courselist[0]->id_training_cert == $cert->id) selected @endif>
                                    {{ $cert->code }} - {{ $cert->param }}

                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    @if (!$trainingpartnerlist->isEmpty())
                    <div id="tPa" class="form-row" style="display: none;">
                        <h4 class="page-title">Training Partner</h4>

                        <table id="tptable" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Training Partner</th>
                                    <th scope="col">Course Code</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>

                            <tbody style="text-align: left;width:100%;" id="tpTableBody">
                                @php
                                $totalRows = 0;
                                @endphp
                               
                                @foreach ($trainingpartnerlist as $partner)
                                    <tr class="tp-row" data-tp-id="{{ $partner->id_training_partner_course }}">
                                        <td class="row-number">{{ ++$totalRows }}</td>
                                        <td>
                                            <select class="form-control" name="training_partners[]">
                                                @foreach($partners as $tp)
                                                    <option value="{{ $tp->id }}" {{ $partner->id == $tp->id ? 'selected' : '' }}>
                                                        {{ $tp->name }}
                                                    </option>
                                                @endforeach
                                                <!-- Add an option for a new training partner -->
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="course_codes[]" value="{{ $partner->course_code }}" placeholder="Please Enter Course Code">
                                        </td>
                                        <input type="hidden" name="id_training_partner_course[]" value="{{ $partner->id_training_partner_course }}">
                                        <td>
                                            <button type="button" class="btn btn-danger remove-tp-row">Remove</button>
                                        </td>
                                    </tr>
                                @endforeach

                                <!-- Add a row for adding a new training partner -->
                                <tr class="tp-row" data-tp-id="">
                                    <td class="row-number">{{ ++$totalRows }}</td>
                                    <td>
                                        <select class="form-control" name="training_partners[]">
                                            <option value="">-- Please Select --</option>
                                            @foreach($partners as $tp)
                                            <option value="{{ $tp->id }}">{{ $tp->name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="course_codes[]" placeholder="Please Enter Course Code">
                                    </td>
                                    <input type="hidden" name="id_training_partner_course[]" value="">
                                    <td>
                                        <button type="button" class="btn btn-danger remove-tp-row">Remove</button>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" class="text-right">
                                        <button type="button" class="btn btn-primary" id="addTPCourseCode">Add</button>
                                    </td>
                                </tr>
                            </tfoot>
                            @endif
                        </table>
               
                    


                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="newFee"><b>New Fee (Local)</b></label>
                            <input type="number" class="form-control" id="newFee" placeholder="Please Insert New Fee" name="new_fee" step=".01" value="{{$courselist[0]->new_fee}}">
                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="newFeeInt"><b>New Fee (International)</b></label>
                            <input type="number" class="form-control" id="newFeeInt" placeholder="Please Insert New Fee" name="new_fee_int" step=".01" value="{{$courselist[0]->new_fee_int}}">
                        </div>
                    </div>

                    @if($feeList)
                    <div class="form-row">
                        <h4 class="page-title">Re-Sit Fee-Local (RM)</h4>

                        <table id="feeTable" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Fee Type</th>
                                    <th scope="col">Amount (RM)</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>

                            <tbody style="text-align: left;width:100%;" id="feeTableBody">
                                @php
                                $totalRows = 0;
                                @endphp
                                @if ($feeList->isEmpty())
                                <tr>
                                    <td colspan="3">No data available</td>
                                </tr>
                                @else
                                @foreach ($feeList as $fee)
                                @if($fee->fee_category == 'Local')

                                <tr class="feetype-row" data-resit-id="{{ $fee->id_resit_fee }}">
                                    <td class="rows-number">{{ ++$totalRows }}</td>
                                    <td>
                                        <select class="form-control" name="local_feetype[]">
                                            <option value="">-- Please Select --</option>
                                            @foreach($feetypes as $fees)
                                            <option value="{{ $fees->id_fee }}" {{ $fee->id_fee == $fees->id_fee ? 'selected' : '' }}>
                                                {{ $fees->fee_name }}
                                            </option>
                                            @endforeach
                                            
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="local_amount[]" step=".01" value="{{ $fee->amount }}" placeholder="Please Enter Amount">
                                    </td>
                                    <input type="hidden" name="local_id_resit_fee[]" value="{{ $fee->id_resit_fee }}">
                                    <td>
                                        <button type="button" class="btn btn-danger remove-feetype-row">Remove</button>
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                                @endif
                                <tr class="feetype-row" data-resit-id="">
                                    <td class="rows-number">{{ ++$totalRows }}</td>
                                    <td>
                                        <select class="form-control" name="local_feetype[]">
                                            <option value="">-- Please Select --</option>
                                            @foreach($feetypes as $fees)
                                            <option value="{{ $fees->id_fee }}">{{ $fees->fee_name }}</option>
                                            @endforeach
                                            <!-- Add an option for a new training partner -->
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="local_amount[]" step=".01" placeholder="Please Enter Amount">
                                    </td>
                                    <input type="hidden" name="local_id_resit_fee[]" value="">
                                    <td colspan="4">
                                        <button type="button" class="btn btn-danger remove-feetype-row">Remove</button>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                <td colspan="4" class="text-right">
                                        <button type="button" class="btn btn-primary" id="addFeeType">Add</button>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        @endif
                    </div>

                    @if($feeList)
                    <div class="form-row">
                        <h4 class="page-title">Re-Sit Fee-International (RM)</h4>

                        <table id="feeIntTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Fee Type</th>
                                <th scope="col">Amount (RM)</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>

                        <tbody style="text-align: left; width:100%;" id="feeIntTableBody">
                            @php
                            $totalRows = 0;
                            @endphp

                            @if ($feeList->isEmpty())
                                <tr>
                                    <td colspan="3">No data available</td>
                                </tr>
                            @else
                                @foreach ($feeList as $fee)
                                    @if($fee->fee_category == 'Int')
                                        <tr class="feetypeint-row" data-resit-id="{{ $fee->id_resit_fee }}">
                                            <td class="rowsw-number">{{ ++$totalRows }}</td>
                                            <td>
                                                <select class="form-control" name="inter_feetype[]">
                                                    <option value="">-- Please Select --</option>
                                                    @foreach($feetypes as $fees)
                                                        <option value="{{ $fees->id_fee }}" {{ $fee->id_fee == $fees->id_fee ? 'selected' : '' }}>
                                                            {{ $fees->fee_name }}
                                                        </option>
                                                    @endforeach
                                                    <!-- Add an option for a new training partner -->
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="inter_amount[]" step=".01" value="{{ $fee->amount }}" placeholder="Please Enter Amount">
                                            </td>
                                            <input type="hidden" name="inter_id_resit_fee[]" value="{{ $fee->id_resit_fee }}">
                                            <td>
                                                <button type="button" class="btn btn-danger remove-feetypeint-row">Remove</button>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif

                            <!-- Add a new row when there is no data available or when "Add" button is clicked -->
                            <tr class="feetypeint-row" data-resit-id="">
                                <td class="rowsw-number">{{ ++$totalRows }}</td>
                                <td>
                                    <select class="form-control" name="inter_feetype[]">
                                        <option value="">-- Please Select --</option>
                                        @foreach($feetypes as $fees)
                                            <option value="{{ $fees->id_fee }}">{{ $fees->fee_name }}</option>
                                        @endforeach
                                        <!-- Add an option for a new training partner -->
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="inter_amount[]" step=".01" placeholder="Please Enter Amount">
                                </td>
                                <input type="hidden" name="inter_id_resit_fee[]" value="">
                                <td colspan="4">
                                    <button type="button" class="btn btn-danger remove-feetypeint-row">Remove</button>
                                </td>
                            </tr>
                        </tbody>

                        <tfoot>
                            <tr>
                            <td colspan="4" class="text-right">
                                    <button type="button" class="btn btn-primary" id="addFeeTypeInt">Add</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    @endif
                </div>

                    <div class="form-row">
                        <div class="form-group col-md-12 py-2">
                            <label for="courseDescription"><b>Description</b></label>
                            <textarea class="form-control" id="courseDescription" name="course_description" placeholder="Please Insert Description">{{ $courselist[0]->course_description ?? '' }}</textarea>
                        </div>
                    </div>


                    <div class="form-row d-flex justify-content-end mt-4">
                        <div style="margin-right: 1%;">
                            <a href="{{ route('admin.training-course-management')}}" class="btn btn-secondary-6 btn-block">CANCEL</a>
                        </div>
                        <button type="submit" class="btn btn-primary" style="margin-right: 1%; color: white; width: 90px;" id="saveBtn">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">SAVE</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection

@section('vendor-scripts')
<script>
    document.getElementById('addTPCourseCode').addEventListener('click', function() {
        var newRow = document.querySelector('.tp-row').cloneNode(true);

        var lastRowNumber = document.querySelectorAll('.tp-row').length;
        newRow.querySelector('.row-number').textContent = lastRowNumber + 1;

        newRow.querySelector('input').value = '';

        document.getElementById('tpTableBody').appendChild(newRow);
    });



    document.getElementById('tpTableBody').addEventListener('click', function(event) {
        if (event.target.classList.contains('remove-tp-row')) {
            var rowToRemove = event.target.closest('.tp-row');
            var tpId = rowToRemove.dataset.tpId;

            if (rowToRemove.previousElementSibling !== null) {
                removeTpc(tpId, rowToRemove);
            }
        }
    });

//     $(document).on('click', '.remove-tp-row', function() {
//     var rowCount = $('.tp-row').length;

//     // Check if it's not the first row before removing
//     // if (rowCount > 1) {
//     //         $(this).closest('tr').remove();
//     //     updateRowNumbers(); // You can add a function to update row numbers if needed
//     // } else {
//     //     alert('Cannot remove the first row.');
//     // }
// });



function removeTpc(tpId, rowToRemove) {
    // Display a confirmation dialog
    var isConfirmed = confirm('Are you sure you want to delete this record?');

    // Check the user's response
    if (isConfirmed) {
        $.ajax({
            type: 'POST',
            url: '/admin/remove-tpc',
            data: {
                _token: '{{ csrf_token() }}',
                tpId: tpId
            },
            success: function(data) {
                // Remove the row if the deletion was successful
                rowToRemove.remove();
                updateRowNumbers();
            },
            error: function(error) {
                console.error('Failed to delete record: ', error);
            },
        });
    } else {
        // Do nothing if the user cancels the deletion
        console.log('Deletion canceled by user.');
    }
}


    function updateRowNumbers() {
        var rows = document.querySelectorAll('.tp-row');
        rows.forEach(function(row, index) {
            row.querySelector('td').textContent = index + 1;
        });
    }
</script>
<script>
    document.getElementById('addFeeType').addEventListener('click', function() {
        var newRow = document.querySelector('.feetype-row').cloneNode(true);
        var lastRowNumber = document.querySelectorAll('.feetype-row').length;
        newRow.querySelector('.rows-number').textContent = lastRowNumber + 1;

        newRow.querySelector('input').value = '';

        document.getElementById('feeTableBody').appendChild(newRow);
    });

    document.getElementById('addFeeTypeInt').addEventListener('click', function() {
        var newRow = document.querySelector('.feetypeint-row').cloneNode(true);
        var lastRowNumber = document.querySelectorAll('.feetypeint-row').length;
        newRow.querySelector('.rowsw-number').textContent = lastRowNumber + 1;

        newRow.querySelector('input').value = '';

        document.getElementById('feeIntTableBody').appendChild(newRow);
    });


    $(document).on('click', '.remove-feetype-row', function() {
    var rowCount = $('.feetype-row').length;

    // Check if it's not the first row before removing
    if (rowCount > 1) {
        $(this).closest('tr').remove();
        updateRowNumbers();
    } else {
        alert('Cannot remove the first row.');
    }
    });

    function removeLocalFee(resitId, rowToRemove) {
        $.ajax({
            type: 'POST',
            url: '/admin/remove-fee',
            data: {
                _token: '{{ csrf_token() }}',
                resitId: resitId
            },
            success: function(data) {

                rowToRemove.remove();

                updateRowNumbers();
            },
            error: function(error) {
                console.error('Failed to delete record: ', error);
            },
        });
    }

    function updateRowNumbers() {
        var rows = document.querySelectorAll('.feetype-row');
        rows.forEach(function(row, index) {
            row.querySelector('.rows-number').textContent = index + 1;
        });
    }
</script>
<script>
    document.getElementById('addFeeTypeInt').addEventListener('click', function() {
        var newRow = document.querySelector('.feetypeint-row').cloneNode(true);
        var lastRowNumber = document.querySelectorAll('.feetypeint-row').length;
        newRow.querySelector('.rowsw-number').textContent = lastRowNumber + 1;

        newRow.querySelector('input').value = '';

        document.getElementById('feeIntTableBody').appendChild(newRow);
    });

   

    $(document).on('click', '.remove-feetypeint-row', function() {
    var rowCount = $('.feetypeint-row').length;

    // Check if it's not the first row before removing
    if (rowCount > 1) {
        $(this).closest('tr').remove();
        updateRowNumbers();
    } else {
        alert('Cannot remove the first row.');
    }
    });

    function removeInterFee(resitId, rowToRemove) {
        $.ajax({
            type: 'POST',
            url: '/admin/remove-fee',
            data: {
                _token: '{{ csrf_token() }}',
                resitId: resitId
            },
            success: function(data) {

                rowToRemove.remove();

                updateRowNumbers();
            },
            error: function(error) {
                console.error('Failed to delete record: ', error);
            },
        });
    }

    function updateRowNumbers() {
        var rows = document.querySelectorAll('.feetypeint-row');
        rows.forEach(function(row, index) {
            row.querySelector('.rowsw-number').textContent = index + 1;
        });
    }


    function toggleRetypesVisibility() {
    var courseCategorySelect = document.getElementById('categoryName');
    var retypesDiv = document.getElementById('retypes');

    if (courseCategorySelect.value === 'RenewableEnergy') {
        retypesDiv.style.display = 'block';
    } else {
        retypesDiv.style.display = 'none';
    }
}

$(document).ready(function () {
    $('input[name="re_types[]"]').prop('disabled', true);

    $('#categoryName').change(function () {
        var selectedCategory = $(this).val();
        var isRenewableEnergy = (selectedCategory === '1');

        $('#retypes').toggle(isRenewableEnergy);
        $('input[name="re_types[]"]').prop('disabled', !isRenewableEnergy);

        if (isRenewableEnergy) {
            $('input[name="re_types[]"]').prop('checked', false);
        }
    });
});


function toggleTpaVisibility() {
        var trainingOptionSelect = document.getElementById('trainingOption');
        var tPaDiv = document.getElementById('tPa');

        // Adjust the value below to match the value that should trigger the display
        if (trainingOptionSelect.value === 'TrainingPartner') {
            tPaDiv.style.display = 'block';
        } else {
            tPaDiv.style.display = 'none';
        }
    }

    $(document).ready(function () {
        $('#training_option').change(function () {
            var selectedCategory = $(this).val();
            var isTrainingPartner = (selectedCategory === '2');

            $('#tPa').toggle(isTrainingPartner);
        });
    });

    function updateInput(input) {
        input.value = input.value.toUpperCase();
    }

 
</script>

</script>
@endsection