@extends('layouts.contentLayoutMaster')

{{-- Page Title --}}
@section('title', 'Training Management')

{{-- Vendor CSS --}}
@section('vendor-styles')
{{-- Add your vendor styles here --}}
@endsection

@section('page-styles')
<style>
    /* Your custom page styles here */
    @media only screen and (max-width: 792px) {
        #sectionFormMain>div>div:nth-child(5)>div.col-4 {
            margin-top: 19px;
        }
    }

    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-small {
        font-size: 14px;
        padding: 5px 10px;
    }

    .filter-search-buttons {
        display: flex;
        justify-content: flex-end;
        margin-bottom: 10px;
    }

    .create-new-button {
        text-align: right;
    }

    .centered-container {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 5%;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

    .btn-add-2:hover {
        color: #fff;
    }

    .btn-add-2:disabled,
    .btn-add-2[disabled],
    .btn-add-2:hover {
        color: #fff !important;
        opacity: 0.5;
    }

    .btn-secondary-6 {
        background-color: #fff;
        color: #5A8DEE;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        padding: 10px 20px;
        font-size: 16px;
        font-family: poppins-semibold, sans-serif;
        line-height: 20px;
        text-align: left;
    }

    .btn-secondary-6:hover {
        background-color: #5A8DEE;
        color: #fff;
    }

    .font-title {
        color: var(--gray-950, #0C111D);
        /* Display xs/Semibold */
        font-family: Inter;
        font-size: 24px;
        font-style: normal;
        font-weight: 600;
        line-height: 32px;
        /* 133.333% */
    }
</style>
@endsection

@section('content')
<main>
    <section class="header-top-margin">
        <div class="centered-container">
            <div class="d-flex justify-center align-items-center py-4">
                <button class="btn btn-primary mx-4 active-tab" id="upcomingTrainingBtn">Upcoming Training</button>
                <button class="btn btn-secondary mx-4" id="trainingListBtn">Training List</button>
            </div>
        </div>

        <!-- Upcoming Training Tab -->
    <div class="container" id="upcomingTrainingTabContent">
        <div class="row">
            <div class="col-12">
                <div class="font-title" id="upcomingTrainingTab" style="display: none;">
                    <!-- Filter, Search, and "+" button -->
                    <div class="filter-search-buttons">
                        <!-- Align items to the right -->
                        <div class="ml-auto d-flex align-items-center">
                            <button class="btn btn-primary btn-small mr-2" data-toggle="modal"
                                data-target="#filterModalUpcoming">Filter</button>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search...">
                                <div class="input-group-append">
                                    <button class="btn btn-primary btn-small" type="button">Search</button>
                                </div>
                            </div>
                            <!-- Create New Training Button (temporary) -->
                            <button class="btn btn-primary btn-small create-new-button">+</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Table for Upcoming Training -->
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Programme</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Venue</th>
                        <th>Fee (RM)</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($trainings as $training)
                    <tr>
                        <td>{{ $training->id }}</td>
                        <td>{{ $training->training_name }}</td>
                        <td>{{ \Carbon\Carbon::parse($training->date_start)->format('d-m-Y') }}</td>
                        <td>{{ \Carbon\Carbon::parse($training->date_end)->format('d-m-Y') }}</td>
                        <td>{{ $training->venue }}</td>
                        <td>{{ $training->fee }}</td>
                        <td>
                            <a href="#" class="btn btn-info">Edit</a>
                            <a href="#" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- Pagination -->
        <div class="pagination">
            {{ $trainings->links() }}
        </div>
    </div>

        <!-- Training List Tab -->
        <div class="container" id="trainingListTabContent" style="display: none;">
            <div class="row">
                <div class="col-12">
                    <div class="font-title" id="trainingListTab" style="display: none;">
                        <!-- Filter, Search, and "+" button -->
                        <div class="filter-search-buttons">
                            <!-- Align items to the right -->
                            <div class="ml-auto d-flex align-items-center">
                                <button class="btn btn-primary btn-small mr-2" data-toggle="modal"
                                    data-target="#filterModalTrainingList">Filter</button>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary btn-small" type="button">Search</button>
                                    </div>
                                </div>
                                <!-- Create New Training Button (temporary) -->
                                <button class="btn btn-primary btn-small create-new-button">+</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Table for Training List -->
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Partner Name</th>
                            <th>Course Category</th>
                            <th>Training Type</th>
                            <th>Training Date (Start)</th>
                            <th>Training Date (End)</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($trainings as $training)
                    <tr>
                        <td>{{ $training->id }}</td>
                        <td>{{ $training->training_name }}</td>
                        <td>{{ $training->id_course_category}}</td>
                        <td>{{ $training->id_training_type}}</td>
                        <td>{{ \Carbon\Carbon::parse($training->date_start)->format('d-m-Y') }}</td>
                        <td>{{ \Carbon\Carbon::parse($training->date_end)->format('d-m-Y') }}</td>
                        <td>{{ $training->status }}</td>
                        <td>
                            <a href="#" class="btn btn-info">Edit</a>
                            <a href="#" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- Pagination -->
        <div class="pagination">
            {{ $trainings->links() }}
        </div>
        </div>
    </section>
</main>

<!-- Include jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<!-- Include Bootstrap JavaScript after jQuery -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<!-- Filter (Upcoming Training) Dialog -->
<div class="modal fade" id="filterModalUpcoming" tabindex="-1" role="dialog" aria-labelledby="filterModalUpcomingLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalUpcomingLabel">Filter (Upcoming Training)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Filter Form for Upcoming Training -->
                <form>
                    <div class="form-group">
                        <label for="filterCourse">Course</label>
                        <select class="form-control" id="filterCourse">
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="filterRegion">Region</label>
                        <select class="form-control" id="filterRegion">
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="filterState">State</label>
                        <select class="form-control" id="filterState">
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="filterStartDate">Start Date</label>
                        <input type="date" class="form-control" id="filterStartDate">
                    </div>
                    <div class="form-group">
                        <label for="filterEndDate">End Date</label>
                        <input type="date" class="form-control" id="filterEndDate">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Update</button>
            </div>
        </div>
    </div>
</div>

<!-- Filter (Training List) Dialog -->
<div class="modal fade" id="filterModalTrainingList" tabindex="-1" role="dialog" aria-labelledby="filterModalTrainingListLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalTrainingListLabel">Filter (Training List)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Filter Form for Training List -->
                <form>
                    <div class="form-group">
                        <label for="filterCourse">Status</label>
                        <select class="form-control" id="filterCourse">
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="filterRegion">Training Type</label>
                        <select class="form-control" id="filterRegion">
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="filterState">Course Category</label>
                        <select class="form-control" id="filterState">
                            <option>Option 1</option>
                            <option>Option 2</option>
                            <option>Option 3</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Update</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('vendor-scripts')
{{-- Add your vendor scripts here --}}
@endsection

@section('page-scripts')
<script src="{{ asset('/js/hello2/hello2.js') }}"></script>
<script src="{{ asset('/js/hello/hello.js') }}"></script>

<script>
<<<<<<< HEAD
    // Function to toggle between tabs
=======
    // Function to toggle between tabs ///
>>>>>>> 9b603cd39bd41c7d5b90df1c382446eff4582860
    function toggleTabs(activeTab) {
        // Get tab buttons and tab contents
        const upcomingTrainingBtn = document.getElementById('upcomingTrainingBtn');
        const trainingListBtn = document.getElementById('trainingListBtn');
        const upcomingTrainingTabContent = document.getElementById('upcomingTrainingTabContent');
        const trainingListTabContent = document.getElementById('trainingListTabContent');
        const upcomingTrainingTab = document.getElementById('upcomingTrainingTab');
        const trainingListTab = document.getElementById('trainingListTab');

        if (activeTab === 'upcomingTraining') {
            // Activate Upcoming Training tab
            upcomingTrainingBtn.classList.add('active-tab');
            trainingListBtn.classList.remove('active-tab');
            upcomingTrainingTabContent.style.display = 'block';
            trainingListTabContent.style.display = 'none';
            upcomingTrainingTab.style.display = 'block';
            trainingListTab.style.display = 'none';
        } else if (activeTab === 'trainingList') {
            // Activate Training List tab
            upcomingTrainingBtn.classList.remove('active-tab');
            trainingListBtn.classList.add('active-tab');
            upcomingTrainingTabContent.style.display = 'none';
            trainingListTabContent.style.display = 'block';
            upcomingTrainingTab.style.display = 'none';
            trainingListTab.style.display = 'block';
        }
    }

    // Event listeners for tab buttons
    document.getElementById('upcomingTrainingBtn').addEventListener('click', function () {
        toggleTabs('upcomingTraining');
    });

    document.getElementById('trainingListBtn').addEventListener('click', function () {
        toggleTabs('trainingList');
    });

    // Initialize with the Upcoming Training tab active
    toggleTabs('upcomingTraining');
</script>
@endsection
