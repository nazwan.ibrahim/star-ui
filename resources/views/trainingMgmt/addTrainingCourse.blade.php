@extends('layouts.contentLayoutMaster')

{{-- Page Title --}}
@section('title', 'Add Training Course')

@section('vendor-styles')
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@endsection

@section('page-styles')
<style>
    /* Your custom page styles here */
    /* Align items to the left */
    .centered-container {
        display: flex;
        justify-content: center;
        align-items: center;
        max-width: 100%;
    }

    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 100px;
        margin-bottom: 55px;
    }

    .title-margin {
        margin-top: 95px;
        padding-bottom: 15px;
        margin-bottom: 15px;
        margin-left: 100px;
        justify-content: flex-start;
        display: flex;
    }

    /* Widen the main container */
    .container {
        max-width: 1200px; /* Adjust the width as needed */
        display: flex;
        flex-direction: column;
        align-items: start;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
        text-align: center;
    }

    .form-row {  
        width: 80%;
    }

    /* Style for labels */
    .form-group label {
        display: block;
        text-align: left; /* Align labels to the left */
        margin-bottom: 5px; /* Add vertical gap between label and field */
    }

    .form-checkin {
        display: block;
        min-height: 1.44rem;
        padding-left: 1.5em;
        margin-bottom: 0.15rem;
    }

    .form-check-labell {
      top: 0 !important;
      cursor: pointer;
      font-weight: normal;
      margin-bottom: 0.25rem;
      font-size: 0.85rem;
      color: #000000;
      width:100px;
    }
    
    .form-group label {
        display: block;
        text-align: left; /* Align labels to the left */
        margin-bottom: 5px; /* Add vertical gap between label and field */
    }

</style>
@endsection

@section('content')
<main>
    <div class="header-top-margin">
        <h3 class="page-title title-margin" style="margin-top: 5%;"><b>Add Training Course</b></h3>
        <div class="col-md-12" style="display: flex; justify-content: left; align-items: left;">
            <div class="col-md-12">
                <form action="{{ route('admin.add-training-course-data') }}" method="post">
                @csrf

                    <div class="form-row">
                        <!-- Course Name -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="courseName"><b>Course Name</b></label>
                            <input type="text" class="form-control" id="courseName" placeholder="Please Insert Course Name" name="course_name" oninput="updateInput(this)" required>
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label for="partnerName"><b>Course Category</b></label>
                            <select class="form-control" id="categoryName" name="course_category" onchange="toggleRetypesVisibility()" required>
                                <option> -- Please Select -- </option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->course_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6 py-2" id="retypes" class="form-row" style="display: none;">
                        <label for="reTypes"><b>RE Type</b></label>                        
                        <div class="col-md-4 py-2" >
                            @foreach($re_types as $re_type)
                            <div class="form-check">
                                <div class="row">
                                    <div class="col-md-4" style="margin-bottom: 0.7%;">
                                        <input class="form-check-input" style="width: 30%;" type="checkbox" value="{{ $re_type->id }}" id="categoryCheckbox{{ $re_type->id }}" name="re_types[]" multiple>
                                    </div>
                                    <div class="col-md-8" style="margin-bottom: 0.7%;">
                                        <label class="form-check-label" for="categoryCheckbox{{ $re_type->id }}">
                                            {{ $re_type->re_type }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="col-md-6">
                            <!-- Empty column for spacing -->
                        </div>
                    </div>

                    <div class="form-row">                      
                        <div class="form-group col-md-6 py-2">
                            <label for="training_option"><b>Training Option</b></label>
                            <select class="form-control" id="training_option" aria-placeholder="Please Select" name="training_option" onchange="toggleTpaVisibility()" required>
                                <option> -- Please Select -- </option>
                                @foreach($options as $option)
                                    <option value="{{ $option->id }}">{{ $option->option }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label for="trainingType"><b>Training Type</b></label>
                            <select class="form-control" id="trainingType" name="training_type" required>
                                <option> -- Please Select -- </option>
                                @foreach($types as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label for="training_cert"><b>Certificate Code</b></label>
                            <select class="form-control" id="training_cert" name="training_cert" required>
                                <option> -- Please Select -- </option>
                                @foreach($certs as $cert)
                                    <option value="{{ $cert->id }}">{{ $cert->code }} - {{ $cert->param }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                <div id="tPa" class="form-row" style="display: none;">
                    <h4 class="page-title">Training Partner</h4>
                    <table id="tptable" class="table table-bordered width:80%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Training Partner</th>
                                <th scope="col">Course Code</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody style="text-align: left;width:100%;" id="tpTableBody">
                            <tr class="tp-row">
                                <td class="row-number">1</td>
                                <td>
                                    <select class="form-control" name="training_partners[]">
                                        <option value=""> -- Please Select -- </option>
                                        @foreach($partners as $partner)
                                            <option value="{{ $partner->id }}">{{ $partner->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="course_codes[]" placeholder="Please Enter Course Code">
                                </td class="text-right">
                                <td>
                                    <button type="button" class="btn btn-danger remove-tp-row">Remove</button>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4" class="text-right">
                                    <button type="button" class="btn btn-primary" id="addTPCourseCode">Add Row</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>


                <div class="form-row">
                    <div class="form-group col-md-6 py-2">
                        <label class="field-title" for="newFee"><b>New Fee - Local (RM)</b></label>
                        <input type="number" class="form-control" id="newFee" placeholder="Please Insert New Fee" name="new_fee" step=".01" required>
                    </div>
                    <div class="form-group col-md-6 py-2">
                        <label class="field-title" for="newFeeInt"><b>New Fee - International (RM)</b></label>
                        <input type="number" class="form-control" id="newFeeInt" placeholder="Please Insert New Fee" step=".01" name="new_fee_int" required >
                    </div>
                </div>

                <div class="form-row">
                    <h4 class="page-title">Re Sit Fee - Local</h4>
                    <table id="feeTable" class="table table-bordered width:80%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Fee Type</th>
                                <th scope="col">Amount (RM)</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody style="text-align: left;width:100%;" id="feeTableBody">
                            <tr class="feetype-row">
                                <td class="rows-number">1</td>
                                <td>
                                    <select class="form-control" name="fee_type[]">
                                        <option value=""> -- Please Select -- </option>
                                        @foreach($feetypes as $feetype)
                                            <option value="{{ $feetype->id_fee }}">{{ $feetype->fee_name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="amount[]" step=".01" placeholder="Please Enter Amount">
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger remove-feetype-row">Remove</button>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4" class="text-right">
                                    <button type="button" class="btn btn-primary" id="addFeeType">Add Row</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <div class="form-row">
                    <h4 class="page-title">Re Sit Fee - International</h4>
                    <table id="feeIntTable" class="table table-bordered width:80%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Fee Type</th>
                                <th scope="col">Amount (RM)</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody style="text-align: left;width:100%;" id="feeIntTableBody">
                            <tr class="feetypeint-row">
                                <td class="rowsw-number">1</td>
                                <td>
                                    <select class="form-control" name="fee_typeint[]">
                                        <option value=""> -- Please Select -- </option>
                                        @foreach($feetypes as $feetype)
                                            <option value="{{ $feetype->id_fee }}">{{ $feetype->fee_name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="amountint[]" step=".01" placeholder="Please Enter Amount">
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger remove-feetypeint-row">Remove</button>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4" class="text-right">
                                    <button type="button" class="btn btn-primary" id="addFeeTypeInt">Add Row</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12 py-2">
                        <label for="courseDescription"><b>Description (optional)</b></label>
                        <textarea type="text" class="form-control" id="courseDescription" placeholder="Please Insert Description" name="course_description"></textarea>
                    </div>
                </div>


                
                    <div class="form-row" style="display: flex; justify-content: end;">
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary" id="hantarBtn">
                                <i class="bx bx-check d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Submit</span>
                            </button>
                            <button class="btn btn-secondary ml-2">
                                <a href="{{ route('admin.training-course-management')}}" class="btn btn-secondary">Cancel</a>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection

@section('vendor-scripts')
{{-- Add your vendor scripts here --}}
@endsection

@section('page-scripts')
<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>

<script>

    function toggleRetypesVisibility() {
        var courseCategorySelect = document.getElementById('categoryName');
        var retypesDiv = document.getElementById('retypes');

        if (courseCategorySelect.value === 'RenewableEnergy') {
            retypesDiv.style.display = 'block';
        } else {
            retypesDiv.style.display = 'none';
        }
    }

  $(document).ready(function () {
        $('input[name="re_types[]"]').prop('disabled', true);

        $('#categoryName').change(function () {
            var selectedCategory = $(this).val();
            var isRenewableEnergy = (selectedCategory === '1');

            $('#retypes').toggle(isRenewableEnergy);

            $('input[name="re_types[]"]').prop('disabled', !isRenewableEnergy);

            if (isRenewableEnergy) {
                $('input[name="re_types[]"]').prop('checked', false);
            }
        });
    });


     $("#addPartner").click(function () {
            let partnerRow = $(".partner-row:first").clone();
            partnerRow.find("select, input").val("");
            partnerRow.find(".remove-partner").show(); 
            $("#partnersContainer").append(partnerRow);
        });

        $("#partnersContainer").on("click", ".remove-partner", function () {
        if ($(this).closest(".partner-row").is(":first-child")) {
            alert("Cannot remove the first partner row.");
        } else {
            $(this).closest(".partner-row").remove();
        }
    });
</script>

<script>
    $(document).ready(function () {
        $('.single-checkbox').on('change', function () {
            $('.single-checkbox').not(this).prop('checked', false);
        });
    });
</script>

<script>
    function toggleTpaVisibility() {
        var trainingOptionSelect = document.getElementById('trainingOption');
        var tPaDiv = document.getElementById('tPa');

        // Adjust the value below to match the value that should trigger the display
        if (trainingOptionSelect.value === 'TrainingPartner') {
            tPaDiv.style.display = 'block';
        } else {
            tPaDiv.style.display = 'none';
        }
    }

    $(document).ready(function () {
        $('#training_option').change(function () {
            var selectedCategory = $(this).val();
            var isTrainingPartner = (selectedCategory === '2');

            $('#tPa').toggle(isTrainingPartner);
        });
    });


    document.getElementById('addTPCourseCode').addEventListener('click', function () {
        var newRow = document.querySelector('.tp-row').cloneNode(true);

        var lastRowNumber = document.querySelectorAll('.tp-row').length;
        newRow.querySelector('.row-number').textContent = lastRowNumber + 1;

        newRow.querySelector('input').value = '';

        document.getElementById('tpTableBody').appendChild(newRow);
    });

    document.getElementById('tpTableBody').addEventListener('click', function (event) {
        if (event.target.classList.contains('remove-tp-row')) {
            var rowToRemove = event.target.closest('.tp-row');

            if (rowToRemove.previousElementSibling !== null) {
                rowToRemove.remove();

                updateRowNumbers();
            }
        }
    });

    function updateRowNumbers() {
        var rows = document.querySelectorAll('.tp-row');
        rows.forEach(function (row, index) {
            row.querySelector('.row-number').textContent = index + 1;
        });
    }
</script>

<script>

    document.getElementById('addFeeType').addEventListener('click', function () {
        var newRow = document.querySelector('.feetype-row').cloneNode(true);
        var lastRowNumber = document.querySelectorAll('.feetype-row').length;
        newRow.querySelector('.rows-number').textContent = lastRowNumber + 1;

        newRow.querySelector('input').value = '';

        document.getElementById('feeTableBody').appendChild(newRow);
    });

    document.getElementById('feeTableBody').addEventListener('click', function (event) {
        if (event.target.classList.contains('remove-feetype-row')) {
            var rowToRemove = event.target.closest('.feetype-row');

            if (rowToRemove.previousElementSibling !== null) {
                rowToRemove.remove();

                updateRowNumbers();
            }
        }
    });

    function updateRowNumbers() {
        var rows = document.querySelectorAll('.feetype-row');
        rows.forEach(function (row, index) {
            row.querySelector('.rows-number').textContent = index + 1;
        });
    }
</script>

<script>

    document.getElementById('addFeeTypeInt').addEventListener('click', function () {
        var newRow = document.querySelector('.feetypeint-row').cloneNode(true);
        var lastRowNumber = document.querySelectorAll('.feetypeint-row').length;
        newRow.querySelector('.rowsw-number').textContent = lastRowNumber + 1;

        newRow.querySelector('input').value = '';

        document.getElementById('feeIntTableBody').appendChild(newRow);
    });

    document.getElementById('feeIntTableBody').addEventListener('click', function (event) {
        if (event.target.classList.contains('remove-feetypeint-row')) {
            var rowToRemove = event.target.closest('.feetypeint-row');

            if (rowToRemove.previousElementSibling !== null) {
                rowToRemove.remove();

                updateRowNumbers();
            }
        }
    });

    function updateRowNumbers() {
        var rows = document.querySelectorAll('.feetypeint-row');
        rows.forEach(function (row, index) {
            row.querySelector('.rowsw-number').textContent = index + 1;
        });
    }

    function updateInput(input) {
        input.value = input.value.toUpperCase();
    }


</script>
@endsection
