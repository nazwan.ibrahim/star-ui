@extends('layouts.contentLayoutMaster')

{{-- Page Title --}}
@section('title', 'Training Verification - TP')

{{-- Vendor CSS --}}
@section('vendor-styles')
{{-- Add your vendor styles here --}}
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .centered-container {
        display: flex;
        justify-content: flex-start;
        align-items: center;
        max-width: 100%;
    }

    .header-top-margin {
        margin-top: 7%;
        padding-bottom: 20px;
    }


    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .title-margin {
        margin-top: 95px;
        padding-bottom: 15px;
        margin-bottom: 15px;
        margin-left: 3%;
        justify-content: flex-start;
        display: flex;
    }

    .field-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .checkbox-label {
        color: #454545;
        font-family: Inter;
        font-size: 18px;
        font-style: normal;
        font-weight: 500;
        line-height: 28px;
    }

    .checkbox-size {
        display: flex;
        width: 32px;
        height: 32px;
        justify-content: center;
        align-items: center;
        flex-shrink: 0;
    }

    .form-row {
        display: flex !important;
        width: 1200px;
    }

    .date-field-size {
        display: flex !important;
        width: 50%;
    }

    /* Widen the main container */
    .container {
        max-width: 100%;
        /* Adjust the width as needed */
        display: flex;
        flex-direction: column;
        align-items: left;
    }

    /* Style for labels */
    .form-group label {
        display: block;
        text-align: left;
        /* Align labels to the left */
        margin-bottom: 5px;
        /* Add vertical gap between label and field */
    }

    .radio-items {
        margin-left: 1%;
    }

    .form-control {
        display: block;
        width: 100%;
        box-sizing: border-box;
        white-space: normal;
        word-wrap: break-word;
    }

    .objective-table {
        table-layout: fixed;
        width: 100%;
    }

    .objective-row td {
        padding: 8px; /* Adjust padding as needed */
    }
</style>
@endsection

@section('content')
<main style="margin: 20px">
    <div class="header-top-margin">
        <h3 class="page-title title-margin" style="text-align: left;"><b>Training Verification</b></h3>
        <div class="container">
            <div class="col-md-8">
                <!-- Registration Form -->
                <form action="{{ route('update-training-registrationAppTP', ['id' => $training->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="form-row">
                        <!-- New and Resit Radio Buttons -->
                        <div class="form-check col-md-1 py-2">
                            <input type="radio" value="0" id="newRadio" name="is_ResitRadio" checked>
                            <label for="newRadio">
                                New
                            </label>
                        </div>
                        <div class="form-check col-md-1 py-2">
                            <input type="radio" value="1" id="resitRadio" name="is_ResitRadio">
                            <label for="resitRadio">
                                Re-sit
                            </label>
                        </div>
                    </div>

                    <div class="form-row">
                        <!-- Training Course Dropdown -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="trainingCourse"><b>Training Course</b></label>
                            <select class="form-control" id="trainingCourse" name="trainingCourse" readonly>
                                <option value="{{ $courseList->id_training_course }}">
                                    {{ $courseList->course_name }}
                                </option>
                            </select>
                        </div>

                        <!-- Training Option -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="training_option"><b>Training Option</b></label>
                            <select class="form-control" name="training_option" id="training_option" readonly>
                                <option value="{{ $courseList->option_id }}">{{ $courseList->option_name }}</option>
                            </select>
                        </div>
                    </div>

                    @if($selectedReTypes)
                    <div class="form-row">
                        <label for="partnerName"><b>RE Type</b></label>
                        <div class="col-md-6">
                            @foreach($re_types as $re_type)
                            <div class="form-check">
                                <div class="row">
                                    <div class="col-md-8" style="margin-bottom: 0.7%;">
                                        <label class="form-check-label" for="categoryCheckbox{{ $re_type->id }}">
                                            {{ $re_type->re_type }}
                                        </label>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 0.7%;">
                                        <input class="form-check-input single-checkbox" type="checkbox" value="{{ $re_type->id }}" id="categoryCheckbox{{ $re_type->id }}" name="re_types[]" @if(in_array($re_type->id, $selectedReTypes)) checked @endif>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @else
                    @endif

                    <div class="form-row">
                        <!-- Course Category Dropdown -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="course_id"><b>Course Category</b></label>
                            <select class="form-control" id="course_id" disabled>
                                <option value="{{ $courseList->course_id }}">{{ $courseList->category_name }}</option>
                            </select>
                        </div>

                        <!-- Training Type Dropdown -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="type_id"><b>Training Type</b></label>
                            <select class="form-control" id="type_id" readonly>
                                <option value="{{ $courseList->type_id }}">{{ $courseList->type_name }}</option>
                            </select>
                        </div>

                        <!-- Batch Number -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="batch_no_full"><b>Batch Number</b></label>
                            <input type="text" class="form-control" name="batch_no_full" id="batch_no_full" placeholder="Enter Batch Number" value="{{ $batchNoFull ?? '' }}">
                        </div>

                        <!-- Partner Name -->
                        <!-- <div class="form-group col-md-6 py-2">
                        <label class="field-title" for="partnerName"><b>Partner Name</b></label>
                        <select class="form-control" id="partnerName" name="partnerName" readonly>
                            @if($partnertr)
                                <option value="{{ $partnertr->company_id }}">{{ $partnertr->company_name }}</option>
                            @else
                                <option value="" disabled selected>-- Inhouse Training --</option>
                            @endif
                        </select>
                    </div> -->

                        <!-- Mode of Training Dropdown -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="training_mode"><b>Mode of Training</b></label>
                            <select class="form-control" name="training_mode" id="training_mode">
                                <option> -- Please Select -- </option>
                                @foreach($trainingMode as $mode)
                                <option value="{{ $mode->mode_id }}" {{ ($training->training_mode ?? '') == $mode->mode_id ? 'selected' : '' }}>
                                    {{ $mode->mode_name }}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        <!-- Training Code -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="training_code"><b>Training Code</b></label>
                            <input type="text" class="form-control" name="training_code" id="training_code" placeholder="Enter Training Code" value="{{ $training->training_code ?? '' }}">
                        </div>

                        <!-- Participant Limit -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="total_participant"><b>Participant Limit</b></label>
                            <input type="text" class="form-control" name="total_participant" id="total_participant" placeholder="Enter Participant Limit" value="{{ $training->total_participant ?? '' }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <!-- Venue Name -->
                        <div class="form-group col-md-6 py-2 venue-field">
                            <label class="field-title" for="venue"><b>Venue Name</b></label>
                            <input type="text" class="form-control" id="venue" name="venue" placeholder="Enter Venue Name" value="{{ $training->venue ?? '' }}">
                        </div>

                        <!-- Full Address -->
                        <div class="form-group col-md-6 py-2 venue-field">
                            <label class="field-title" for="trainingDescription"><b>Address</b></label>
                            <input type="text" class="form-control" id="venue_address" name="venue_address" placeholder="Enter Address" value="{{ $training->venue_address ?? '' }}"></input>
                        </div>

                        <!-- Full Address -->
                        <div class="form-group col-md-6 py-2 url-field">
                            <label class="field-title" for="venue"><b>URL</b></label>
                            <input type="text" class="form-control" id="url" name="url" placeholder="Enter URL Name" value="{{ $training->training_url ?? '' }}">
                        </div>

                        <!-- Region Dropdown -->
                        {{-- <div class="form-group col-md-6 py-2 region-field">
                            <label class="field-title" for="region"><b>Region</b></label>
                            <select class="form-control" id="region" name="region" onchange="updateStates()">
                                <option value=""> -- Select a Region -- </option>
                                @foreach($regions as $region)
                                <option value="{{ $region->id }}" {{ $training->region_name == $region->name? 'selected' : '' }}>{{ $region->name }}</option>
                                @endforeach
                            </select>
                        </div> --}}

                        <!-- State Dropdown -->
                        <div class="form-group col-md-6 py-2 state-field">
                            <label class="field-title" for="state"><b>State</b></label>
                            <select class="form-control" id="state" name="state">
                               
                                @foreach($states as $state)
                                <option value="{{ $state->id }}" {{ $training->state_name == $state->name ? 'selected' : '' }}>{{ $state->name }}</option>
                                @endforeach
                                <option value=""> Please select </option>
                                <option value="1"> JOHOR </option>
                                <option value="2"> KEDAH </option>
                                <option value="3"> KELANTAN </option>
                                <option value="4"> MELAKA </option>
                                <option value="5"> NEGERI SEMBILAN </option>
                                <option value="6"> PAHANG </option>
                                <option value="7"> PERAK </option>
                                <option value="8"> PERLIS </option>
                                <option value="9"> PULAU PINANG </option>
                                <option value="10"> SARAWAK </option>
                                <option value="11"> SABAH </option>
                                <option value="13"> TERENGGANU </option>
                                <option value="14"> LABUAN</option>
                                <option value="15"> PUTRAJAYA</option>
                            </select>
                            </select>
                        </div>
                    </div>

                    @if($selectedReTypes)
                    <div class="form-row">
                        <label for="partnerName"><b>RE Type</b></label>
                        <div class="col-md-6">
                            @foreach($re_types as $re_type)
                            <div class="form-check">
                                <div class="row">
                                    <div class="col-md-8" style="margin-bottom: 0.7%;">
                                        <label class="form-check-label" for="categoryCheckbox{{ $re_type->id }}">
                                            {{ $re_type->re_type }}
                                        </label>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 0.7%;">
                                        <input class="form-check-input single-checkbox" type="checkbox" value="{{ $re_type->id }}" id="categoryCheckbox{{ $re_type->id }}" name="re_types[]" @if(in_array($re_type->id, $selectedReTypes)) checked @endif>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @else
                    @endif

                    <!-- Training Date and Time Fields -->
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="date_start"><b>Training Start Date</b></label>
                            <input type="date" class="form-control" name="date_start" id="date_start" value="{{ $training->date_start ? date('Y-m-d', strtotime($training->date_start)) : '' }}">
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="date_end"><b>Training End Date</b></label>
                            <input type="date" class="form-control" name="date_end" id="date_end" value="{{ $training->date_end ? date('Y-m-d', strtotime($training->date_end)) : '' }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="start_time"><b>Training Start Time</b></label>
                            <input type="time" class="form-control" name="start_time" id="start_time" value="{{ $training->start_time ?? '' }}">
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="end_time"><b>Training End Time</b></label>
                            <input type="time" class="form-control" name="end_time" id="end_time" value="{{ $training->end_time ?? '' }}">
                        </div>
                    </div>

                    <!-- Advertisement Date Fields -->
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="advert_start"><b>Advert Start Date</b></label>
                            <input type="date" class="form-control" name="advert_start" id="advert_start" value="{{ $training->advert_start ? date('Y-m-d', strtotime($training->advert_start)) : '' }}">
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="advert_end"><b>Advert End Date</b></label>
                            <input type="date" class="form-control" name="advert_end" id="advert_end" value="{{ $training->advert_end ? date('Y-m-d', strtotime($training->advert_end)) : '' }}">
                        </div>
                    </div>

                    <!-- Sub Title: Trainers -->
                    <h4 class="field-title py-2" style="text-align: start"><b>Trainers</b></h4>

                    <!-- Table for Trainers -->
                    <table id="trainerRows" class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Trainer Name</th>
                                <th>Action</th> <!-- Add a new column for the remove button -->
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <select class="form-control" name="trainer_id[]">
                                       
                                        @foreach($trainerList as $trainerList)
                                        <option value="{{ $trainerList->trainer_id }}">{{ $trainerList->trainer_fullname }}</option>
                                        @endforeach
                                        <option value="">Select Trainer</option>
                                    </select>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger remove-row">Remove</button>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3">
                                    <button type="button" class="btn btn-primary" id="addRowButton">Add Row</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                    <div class="row">
                        <!-- <div class="form-row"> -->
                        <div class="col-md-6">
                            <label class="field-title" for="hrdf_claim"><b>Free / Paid</b></label>
                            <div>
                                <!-- Free/Paid Checkbox -->
                                <div class="form-check py-2">
                                    <input class="radio-items" type="radio" value="0" id="is_paid" name="is_paid" {{ ($training->is_paid == 0) ? 'checked' : '' }} >
                                    <label class="radio-items" for="is_paid">
                                   Free
                               </label>
                                    <input class="radio-items" type="radio" value="1" id="is_paid" name="is_paid" {{ ($training->is_paid == 1) ? 'checked' : '' }} >
                                    <label class="radio-items" for="is_paid">
                                   Paid
                               </label>
                           </div>
                            </div>
                        </div>
                        <!-- </div> -->

                        <!-- <div class="form-row"> -->
                        <div class="col-md-6">
                            <label class="field-title" for="hrdf_claim"><b>HRDF Claimable</b></label>
                            <div>
                                <!-- HRDF Claimable Dropdown -->
                                <div class="form-check py-2">
                                    <input class="radio-items" type="radio" value="1" id="hrdf_claim" name="hrdf_claim" {{ ($training->hrdf_claim == 1) ? 'checked' : '' }}>
                                    <label class="radio-items" for="hrdf_claim">
                                        Claimable
                                    </label>
                                    <input class="radio-items" type="radio" value="0" id="hrdf_claim" name="hrdf_claim" {{ ($training->hrdf_claim == 0) ? 'checked' : '' }}>
                                    <label class="radio-items" for="hrdf_claim">
                                        Non-Claimable
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- </div> -->
                    </div>

                    <div class="form-row">
                        <!-- Local Fee Fields -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="fee"><b>New Fee - Local (RM)</b></label>
                            <input type="text" class="form-control" name="new_fee" id="new_fee" value="{{ number_format($courseList->new_fee, 2) }}" readonly>
                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="fee"><b>New Fee - International (RM)</b></label>
                            <input type="text" class="form-control" name="new_fee_int" id="new_fee_int" value="{{ number_format($courseList->new_fee_int, 2) }}" readonly>
                        </div>
                    </div>

                    <div class="form-row">
                        <h2 class="page-title">Re Sit Fee - Local</h2>
                        <table class="table table-bordered width:80%">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Fee Type</th>
                                    <th scope="col">Amount</th>
                                </tr>
                            </thead>
                            <tbody style="text-align: left;width:100%;" id="feeTableBodyLocal">
                                @foreach($resitfees as $index => $resitfee)
                                @if($resitfee->fee_category === 'Local')
                                <tr class="feetype-row">
                                    <td class="rows-number">{{ $index + 1 }}</td>
                                    <td>
                                        <span class="form-control">{{ $resitfee->fee_name }}</span>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="amount_local[]" placeholder="Please Enter Amount" value="{{ $resitfee->amount }}" readonly>
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="form-row">
                        <h2 class="page-title">Re Sit Fee - International</h2>
                        <table class="table table-bordered width:80%">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Fee Type</th>
                                    <th scope="col">Amount</th>
                                </tr>
                            </thead>
                            <tbody style="text-align: left;width:100%;" id="feeTableBodyInt">
                                @foreach($resitfees as $index => $resitfee)
                                @if($resitfee->fee_category === 'Int')
                                <tr class="feetype-row">
                                    <td class="rows-number">{{ $index + 1 }}</td>
                                    <td>
                                        <span class="form-control">{{ $resitfee->fee_name }}</span>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="amount_int[]" placeholder="Please Enter Amount" value="{{ $resitfee->amount }}" readonly>
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <!-- International Fee Fields -->
                    <!-- <div class="form-row">
                    <div class="form-group col-md-6 py-2">
                        <label class="field-title" for="fee"><b>Re-sit Fee (RM)</b></label>
                        <input type="text" class="form-control" name="resit_fee" id="resit_fee" placeholder="Enter Fee" value="" readonly>
                    </div>
                    <div class="form-group col-md-6 py-2">
                        <label class="field-title" for="fee"><b>Re-sit Fee - International (RM)</b></label>
                        <input type="text" class="form-control" name="resit_fee_int" id="resit_fee_int" placeholder="Enter Fee" value="" readonly>
                    </div>
                </div> -->

                    <div class="form-row">
                        <!-- Supporting Document -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="supportingDocument"><b>Supporting Document</b></label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="supportingDocument" name="supporting_documents[]" multiple>
                                <label class="custom-file-label" for="supportingDocument"></label>
                            </div>
                        </div>

                        <!-- Training Notes Document -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="trainingNotesDocument"><b>Training Notes Document</b></label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="trainingNotes" name="training_documents[]" multiple>
                                <label class="custom-file-label" for="trainingNotes"></label>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <!-- Supporting Document Description -->
                        <div class="form-group col-md-6 py-2 mt-2">
                            <label class="field-title" for="document_description"><b>Supporting Document Description</b></label>
                            <input type="text" class="form-control" name="document_description" id="document_description" placeholder="Enter Description" value="{{ $training->document_description ?? '' }}">
                        </div>

                        <!-- Training Document Description -->
                        <div class="form-group col-md-6 py-2 mt-2">
                            <label class="field-title" for="training_notes"><b>Training Notes Description</b></label>
                            <input type="text" class="form-control" name="training_notes" id="training_notes" placeholder="Enter Description" value="{{ $training->document_description ?? '' }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <h2 class="page-title">Training Objectives</h2>
                        <table class="table table-bordered width:80%">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Objective</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="objective-table" style="text-align: left;" id="ObjectiveTableBody">
                            @foreach($objectives as $objective)
                            <tr class="objective-row">
                                <td class="rows-number">{{ $loop->iteration }}</td>
                                <td>
                                    <span class="form-control">{{ $objective->objective }}</span>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger remove-row">Remove</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">
                                <button type="button" class="btn btn-primary" id="addObjectiveRowButton">Add Row</button>
                            </td>
                        </tr>
                    </tfoot>
                    </table>
            </div>

            <div class="form-row">
                <!-- Training Poster -->
                <div class="form-group col-md-6 py-2">
                    <label class="field-title" for="training_image"><b>Training Poster</b></label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="training_image" name="training_image">
                        <label class="custom-file-label" for="training_image"></label>
                    </div>
                </div>

                <!-- Empty Class for Spacing -->
                <div class="form-group col-md-6 py-2">
                </div>
            </div>

            <!-- Training Description -->
            <div class="form-group py-2">
                <label class="field-title" for="training_description"><b>Training Description</b></label>
                <textarea class="form-control" name="training_description" id="training_description" rows="4" placeholder="Enter Training Description">{{ $training->training_description ?? '' }}</textarea>
            </div>

            <!-- Submit Button -->
            <input type="hidden" name="submit_status" id="submit_status" value="0">
            <div class="form-row" style="display: flex; justify-content: end;">
                <button class="btn btn-light" type="button" onclick="cancelAction()">Cancel</button>
                <button class="btn btn-secondary ml-2" type="button" onclick="setSubmitStatus(4)">Draft</button>
                <button class="btn btn-primary ml-2" type="button" onclick="setSubmitStatus(1)">Submit</button>
            </div>
            </form>
        </div>
    </div>
    </div>
</main>
@endsection

@section('vendor-scripts')
{{-- Add your vendor scripts here --}}
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
@endsection

@section('page-scripts')
<script src="{{ asset('/js/hello2/hello2.js') }}"></script>
<script src="{{ asset('/js/hello/hello.js') }}"></script>

<script>
    let host = "{{ env('API_SERVER') }}";
</script>

<script>
    function cancelAction() {
        window.location.href = "{{ route('TrainingManagementAppTP') }}";
    }

    function setSubmitStatus(status) {
        // Set the value of the hidden input
        document.getElementById('submit_status').value = status;

        // Submit the form
        document.querySelector('form').submit();
    }
</script>

<script>
    $(document).ready(function() {
        $.ajax({
            url: `${host}/api/training/courseManagementList`,
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                var trainingCourseSelect = $('#trainingCourseSelect');
                trainingCourseSelect.empty();

                trainingCourseSelect.append($('<option>', {
                    value: '',
                    text: 'Select Training Course'
                }));

                $.each(data.courselist, function(index, course) {
                    trainingCourseSelect.append($('<option>', {
                        value: course.id_training_course,
                        text: course.course_name
                    }));
                });
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        let isResit = 0;
        updateIsResit(isResit);

        // Handle New checkbox change event
        $('#newCheckbox').change(function() {
            if (this.checked) {
                $('#resitCheckbox').prop('checked', false);
            }
            updateIsResit(isResit);
        });

        // Handle Resit checkbox change event
        $('#resitCheckbox').change(function() {
            if (this.checked) {
                isResit = 1;
                $('#newCheckbox').prop('checked', false);
            }
            updateIsResit(isResit);
        });

        function updateIsResit(value) {
            $('#is_Resit').val(value);
        }
    });
</script>
<script>
    $(document).ready(function() {
        // Event listener for radio buttons
        $('input[name="is_paid"]').on('change', function() {
            var isPaid = $('input[name="is_paid"]:checked').val();
            var feeInput = $('#fee');

            // Disable or enable the fee input based on the selected value
            if (isPaid == 1) {
                feeInput.prop('disabled', false);
            } else {
                feeInput.prop('disabled', true);
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        // Event listener for Training Course dropdown change
        $('#trainingCourse').on('change', function() {
            updateFees();
        });

        // Event listener for radio buttons change
        $('input[name="is_paid"]').on('change', function() {
            updateFees();
        });

        // Initial update based on the current state
        updateFees();

        function updateFees() {
            var selectedCourse = $('#trainingCourse option:selected');
            var isPaid = $('input[name="is_paid"]:checked').val();

            // Set new fee and resit fee based on the selected course and paid status
            /* if (selectedCourse.length > 0) {
                var newFee = selectedCourse.data('new-fee') || '';
                var newFeeInt = selectedCourse.data('new-fee-int') || '';
                var resitFee = selectedCourse.data('resit-fee') || '';
                var resitFeeInt = selectedCourse.data('resit-fee-int') || '';

                // If paid is selected, enable input fields; otherwise, disable them
                if (isPaid == 1) {
                    $('#new_fee').val(newFee).prop('readonly', false);
                    $('#new_fee_int').val(newFeeInt).prop('readonly', false);
                    $('#resit_fee').val(resitFee).prop('readonly', false);
                    $('#resit_fee_int').val(resitFeeInt).prop('readonly', false);
                } else {
                    $('#new_fee').val('').prop('readonly', true);
                    $('#new_fee_int').val('').prop('readonly', true);
                    $('#resit_fee').val('').prop('readonly', true);
                    $('#resit_fee_int').val('').prop('readonly', true);
                }
            } */
        }
    });
</script>

<script>
    $(document).ready(function() {
        $('input[type="file"]').change(function(e) {
            const files = e.target.files;
            const label = $(e.target).closest('.custom-file').find('.custom-file-label');

            if (files.length > 1) {
                label.html(files.length + ' files selected');
            } else {
                label.html(files[0].name);
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        // Add row
        $('#addRowButton').click(function() {
            var rowCount = $('#trainerRows tbody tr').length + 1;

            var newRow = '<tr>' +
                '<td>' + rowCount + '</td>' +
                '<td>' +
                '<select class="form-control" name="trainer_name[]">' +
                '<option value="">Select Trainer</option>' +
                '<?php foreach ($trainer as $nameTr) : ?>' +
                '<option value="<?php echo $nameTr->fullname; ?>"><?php echo $nameTr->fullname; ?></option>' +
                '<?php endforeach; ?>' +
                '</select>' +
                '</td>' +
                '<td>' +
                '<button type="button" class="btn btn-danger remove-row">Remove</button>' +
                '</td>' +
                '</tr>';

            $('#trainerRows tbody').append(newRow);
        });

        // Remove row
        $('#trainerRows').on('click', '.remove-row', function() {
            $(this).closest('tr').remove();
            updateRowNumbers(); // Call a function to update row numbers if needed
        });

        // Function to update row numbers
        function updateRowNumbers() {
            $('#trainerRows tbody tr').each(function(index) {
                $(this).find('td:first').text(index + 1);
            });
        }
    });
</script>

<script>
    $(document).ready(function() {
        // Event listener for date fields change
        $("#date_start, #date_end, #advert_start, #advert_end").change(function(event) {
            // Parse dates
            var dateStart = new Date($("#date_start").val());
            var dateEnd = new Date($("#date_end").val());
            var advertStart = new Date($("#advert_start").val());
            var advertEnd = new Date($("#advert_end").val());

            // Check conditions
            if (advertStart > dateStart) {
                alert("Advertisement start date cannot be after training start date.");
                $("#advert_start").val(""); // Clear the selection
            }

            if (advertEnd < advertStart) {
                alert("Advertisement end date cannot be before advertisement start date.");
                $("#advert_end").val(""); // Clear the selection
            }

            if (dateEnd < dateStart) {
                alert("Training end date cannot be before training start date.");
                $("#date_end").val(""); // Clear the selection
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        // Function to enable/disable Partner Name dropdown based on Training Option
        function updatePartnerDropdownState() {
            var selectedOption = $('#training_option').val();

            // Check if the selected option is "Inhouse"
            if (selectedOption === 'Inhouse') {
                // Disable the Partner Name dropdown
                $('#partnerName').prop('disabled', true);
            } else {
                // Enable the Partner Name dropdown
                $('#partnerName').prop('disabled', false);
            }
        }

        // Event listener for Training Course dropdown change
        $('#trainingCourse').change(function() {
            // Your existing code to update dropdowns
            var selectedOption = $(this).find('option:selected');
            var category = selectedOption.data('category-name');
            var trainingType = selectedOption.data('course-type');
            var training_option = selectedOption.data('course-option');

            // Update Course Category dropdown
            $('#id_course_category').html('<option>' + category + '</option>');

            // Update Training Type dropdown
            $('#id_training_type').html('<option>' + trainingType + '</option>');

            // Update Training Option dropdown
            $('#training_option').html('<option>' + training_option + '</option>');

            // Call the function to enable/disable Partner Name dropdown
            updatePartnerDropdownState();
        });

        // Call the function on page load to handle initial state
        updatePartnerDropdownState();
    });
</script>

<script>
    // Flag to track whether the form is dirty (changes made)
    var formIsDirty = false;

    // Event listener for form changes
    $('form :input').change(function() {
        formIsDirty = true;
    });

    // Event listener for form submission
    $('form').submit(function() {
        // Reset the formIsDirty flag when the form is submitted
        formIsDirty = false;
    });

    // Event listener for beforeunload event
    window.addEventListener('beforeunload', function(e) {
        // Check if the form is dirty and show confirmation if true
        if (formIsDirty) {
            var confirmationMessage = 'You have unsaved changes. Are you sure you want to leave?';
            (e || window.event).returnValue = confirmationMessage;
            return confirmationMessage;
        }
    });
</script>

<script>
    $(document).ready(function() {
        $('#addObjectiveRowButton').on('click', function() {
            addRow();
        });

        $('#ObjectiveTableBody').on('click', '.remove-row', function() {
            $(this).closest('tr').remove();
            updateRowNumbers();
        });

        function addRow() {
            var newRow = '<tr class="objective-row">' +
                '<td class="rows-number">' + ($('#ObjectiveTableBody tr').length + 1) + '</td>' +
                '<td>' +
                '<input type="text" class="form-control" name="training_objective[]" required>' +
                '</td>' +
                '<td>' +
                '<button type="button" class="btn btn-danger remove-row">Remove</button>' +
                '</td>' +
                '</tr>';

            $('#ObjectiveTableBody').append(newRow);
        }

        function updateRowNumbers() {
            $('#ObjectiveTableBody tr').each(function(index) {
                $(this).find('.rows-number').text(index + 1);
            });
        }
    });
</script>

<script>
    $(document).ready(function() {
        // Initial state on page load
        updateFieldsVisibility();

        // Event listener for changes in the "Mode of Training" dropdown
        $('#training_mode').change(function() {
            updateFieldsVisibility();
        });

        function updateFieldsVisibility() {
            var selectedMode = $('#training_mode').val();

            // Hide all fields by default
            $('.venue-fields, .url-field, .state-field, .region-field').hide();

            if (selectedMode === '2') {
                $('.url-field').show();
            } else if (selectedMode === '1') {
                $('.venue-fields, .state-field, .region-field').show();
            } else if (selectedMode === '3') {
                $('.venue-fields, .url-field, .state-field, .region-field').show();
            } else if (selectedMode === '4') {
                $('.venue-fields, .url-field, .state-field, .region-field').show();
            } else {
                $('.venue-fields, .url-field, .state-field, .region-field').hide();
            }
        }
    });
</script>

<script>
    function updateStates() {
        // var selectedRegionId = $('#region').val();

        $.ajax({
            url: '/training-partner/get-states/' + selectedRegionId,
            type: 'GET',
            success: function(response) {
                var stateDropdown = $('#state');

                stateDropdown.empty().append('<option value=""> -- Select a State -- </option>');

                $.each(response, function(index, state) {
                    $('#state').append('<option value="' + state.id + '">' + state.name + '</option>');
                });
            },
            error: function(error) {
                console.error('Error fetching states:', error);
            }
        });
    }
</script>
@endsection