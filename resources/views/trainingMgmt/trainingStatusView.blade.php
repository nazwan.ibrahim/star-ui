@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Training Status')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.icon-editIcon:before {
  content: "\e916";
}

.title{
    color: #656565;
    font-family: Rajdhani;
    font-size: 26px;
    font-style: normal;
    font-weight: 600;
    line-height: 38px; /* 146.154% */
}

.contentUpdate{
    color: #333;
    font-family: Open Sans;
    font-size: 18px;
    font-style: normal;
    font-weight: 400;
    margin-bottom: 1%;
}

.contentUpdate2{
    color: #454545;
    font-size: 18px;
    font-family: Open Sans;
    font-style: normal;
    font-weight: 600;
    margin-bottom: 1%;
}

.font-title-2{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 18px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .table-row td {
        padding: 10px;
        border-bottom: none;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

.icon-editIcon:before {
  content: "\e916";
}

    .fully-booked-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .unattend-status {
        border-radius: 8px;
        border: 1px solid #F04438;
        background: var(--success-25, #FFFBFA);
        padding: 4px 8px;
        color: #F04438;
    }

    .attend-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
        border-bottom: none;
    }
</style>
@endsection

@section('content')
<main style="padding:50px;margin:4px;">
    <section class="header-top-margin">
    @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
    @else
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
    @endif
    <div class="row">
    </div>
        <div class="col-md-12 mx-auto">
            <div class="card mt-7" style="padding: 40px; margin-bottom: 2%; margin-top: 2%; background-color: #FFFFFF; text-align: left; border-radius: 16px; border: 1px #98A2B3 solid;">
                <h2 class="page-title py-2" style="text-align: left !important; margin-bottom: 1%;">{{ $listparticipant[0]->training_name }}</h2>
                <div style="margin-top: 2%;">
                    <span class="contentUpdate">Provider : </span>&nbsp;<span class="contentUpdate2">{{ $listparticipant[0]->company_name }}</span>
                </div>
                <div style="margin-top: 2%;">
                    <span class="contentUpdate">Start Date : </span>&nbsp;<span class="contentUpdate2">{{ $listparticipant[0]->date_start }}</span>
                </div>
                <div style="margin-top: 2%;">
                    <span class="contentUpdate">End Date : </span>&nbsp;<span class="contentUpdate2">{{ $listparticipant[0]->date_end }}</span>
                </div>
                <div style="margin-top: 2%;">
                    <span class="contentUpdate">Venue : </span>&nbsp;<span class="contentUpdate2">{{ $listparticipant[0]->venue }}</span>
                </div>
            </div>
        </div>

    @csrf    
    <div class="table-responsive col-md-12 mx-auto">

        <table class="table table-striped dataTable no-footer" id="trainingManagementTPAttendance" style="width: 100%; border-bottom: white;">

            <thead class="table-header">
                <tr>
                    <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;" width=40;>No.</th>
                    <th class="table-title" style="border-bottom: none; text-align: left; white-space: nowrap;">Participant</th>
                    <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Status</th>
                    <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;">Action</th>
                </tr>
            </thead>

            <tbody style="text-align: left;width:100%;">
            @if ($participants->isEmpty())
                <tr>
                    <td colspan="4">No data available</td>
                </tr>
            @else
                @foreach ($participants as $participant)
                    <tr>
                        <td style="border-bottom: none; text-align: center; white-space: nowrap;">{{ $loop->index + 1 }}</td>
                        <td style="border-bottom: none; text-align: left; white-space: nowrap;">{{ $participant->fullname }}</td>
                        <td width="15%" style="border-bottom: none; text-align: center; white-space: nowrap; align="center">
                            <p class="{{ $participant->attend_check == 1 ? 'attend-status' : 'unattend-status' }}">
                                {{ $participant->attend_check == 1 ? 'Attended' : 'Did Not Attend' }}
                            </p>
                        </td>
                        <td width="15%" class="mx-auto" style="border-bottom: none; text-align: center; white-space: nowrap;">
                            <form action="{{ route('admin.delete-participant', $participant->id) }}" method="POST">
                                <a class="edit-attendance" data-id="{{ $participant->id }}" href="#" style="font-size: 18px; color: #3498DB;"><i class="fas fa-eye"></i></a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm delete-participant">
                                    <i class="fas fa-trash-alt" style="font-size: 18px; color: #3498DB;"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </section>
</main>
@endsection

@section('page-scripts')
<script>
$(document).on("click", ".edit-attendance", function () {
    // Retrieve the participant's ID from data-id attribute
    const participantId = $(this).data("id");

    // Use the ID to fetch participant data and populate the modal fields
    // ... (Your logic to populate the modal goes here)

    // Show the modal
    $("#attendModal").modal("show");
});
</script>

<script>
    $(document).ready(function () {
    $(document).on('change', '.attendance-dropdown', function () {
        const userId = $(this).data('id');
        const newAttendanceStatus = $(this).val();
        const updateUrl = $(this).data('url');

        $.ajax({
            url: updateUrl,
            type: 'POST',
            dataType: 'json',
            data: {
                id: userId,
                attendance_status: newAttendanceStatus,
                _token: '{{ csrf_token() }}',
            },
            success: function (response) {
                console.log(response.message);
            },
            error: function (error) {
                console.error('Error:', error);
            },
        });
    });
});


$(document).on("click", ".delete-participant", function () {
      let id = $(this).data("id");
      
      deleteList(id);
  });

  let deleteList = (id) => {
  let deleteLink = `${host}/api/training/${id_training_course}/deleteParticipant`;

  $.ajax({
      url: deleteLink,
      type: "DELETE",
      dataType: "JSON",
  })
  .done((res) => {
  
  })
  .catch((err) => {
  });
};


</script>
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script> 
    <script src="{{ asset('js/vendors/jquery.min.js') }}"></script>

@endsection

