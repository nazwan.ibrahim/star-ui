@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Training Details')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .left-side {
        float: left;
        width: 33%;
    }

    .right-side {
        float: left;
        width: 67%;
    }

    .dataTables_length {
        display: none;
    }

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .filter-dropdown{
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }
</style>
@endsection

@section('content')
<main class="header-top-margin col-md-12 mx-auto">
    <!-- left side -->
    <div class="left-side py-4" style="border-right: 1px solid #98A2B3;">
        <div class="py-4" style="flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 33px; display: flex; padding-left: 13%">
            <div style="width: 300px; height: 300px; background: #EAECF0"></div>
            <div style="color: #656565; font-size: 32px; font-family: Rajdhani; font-weight: 600; word-wrap: break-word">Training Course</div>
        </div>
        <div style="flex-direction: row; justify-content: flex-start; align-items: flex-start; display: flex; padding-left: 13%; padding-bottom: 5%">
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; margin-right: 10%;">Status :</label>
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; font-weight: 600;">Completed</label>
        </div>
        <div style="flex-direction: row; justify-content: flex-start; align-items: flex-start; display: flex; padding-left: 13%; padding-bottom: 5%">
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; margin-right: 10%;">Mode of Training :</label>
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; font-weight: 600;">{{ $training->training_mode }}</label>
        </div>
        <div style="flex-direction: row; justify-content: flex-start; align-items: flex-start; display: flex; padding-left: 13%; padding-bottom: 5%">
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; margin-right: 10%;">Course Category :</label>
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; font-weight: 600;">{{ $training->course_name }}</label>
        </div>
        <div style="flex-direction: row; justify-content: flex-start; align-items: flex-start; display: flex; padding-left: 13%; padding-bottom: 5%">
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; margin-right: 10%;">Training Type :</label>
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; font-weight: 600;">{{ $training->training_name }}</label>
        </div>
        <div style="flex-direction: row; justify-content: flex-start; align-items: flex-start; display: flex; padding-left: 13%; padding-bottom: 5%">
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; margin-right: 10%;">Location :</label>
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; font-weight: 600;">{{ $training->venue }}. {{ $training->venue_address }}</label>
        </div>
        <div style="flex-direction: row; justify-content: flex-start; align-items: flex-start; display: flex; padding-left: 13%; padding-bottom: 5%">
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; margin-right: 10%;">Training Code :</label>
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; font-weight: 600;">{{ $training->training_code }}</label>
        </div>
        <div style="flex-direction: row; justify-content: flex-start; align-items: flex-start; display: flex; padding-left: 13%; padding-bottom: 5%">
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; margin-right: 10%;">Trainer Name :</label>
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; font-weight: 600;">Ahmad</label>
        </div>
        <div style="flex-direction: row; justify-content: flex-start; align-items: flex-start; display: flex; padding-left: 13%; padding-bottom: 5%">
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; margin-right: 10%;">Start Time :</label>
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; font-weight: 600;">{{ $training->start_time}}</label>
        </div>
        <div style="flex-direction: row; justify-content: flex-start; align-items: flex-start; display: flex; padding-left: 13%; padding-bottom: 5%">
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; margin-right: 10%;">End Time :</label>
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; font-weight: 600;">{{ $training->end_time}}</label>
        </div>
        <div style="flex-direction: row; justify-content: flex-start; align-items: flex-start; display: flex; padding-left: 13%; padding-bottom: 5%">
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; margin-right: 10%;">Start Date :</label>
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; font-weight: 600;">{{ $training->date_start }}</label>
        </div>
        <div style="flex-direction: row; justify-content: flex-start; align-items: flex-start; display: flex; padding-left: 13%; padding-bottom: 5%">
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; margin-right: 10%;">End Date :</label>
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; font-weight: 600;">{{ $training->date_end }}</label>
        </div>
        <div style="flex-direction: row; justify-content: flex-start; align-items: flex-start; display: flex; padding-left: 13%; padding-bottom: 5%">
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; margin-right: 10%;">No. of Participants :</label>
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; font-weight: 600;">{{ $training->total_participant }}</label>
        </div>
        <div style="flex-direction: row; justify-content: flex-start; align-items: flex-start; display: flex; padding-left: 13%; padding-bottom: 5%">
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; margin-right: 10%;">Fee :</label>
            <label style="display: inline-block; font-family: Open Sans; font-size: 16px; font-weight: 600;">{{ $training->fee }}</label>
        </div>
    </div>

    <!-- right side -->
    <div class="right-side py-4">
        <div class="py-4" style="flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 33px; display: flex; padding-left: 5%">
            <div style="color: #656565; font-size: 32px; font-family: Rajdhani; font-weight: 600; word-wrap: break-word">Description</div>
            <div style="display: inline-block; font-family: Open Sans; font-size: 20px; margin-right: 10%;">
                {{ $training->training_description }} 
            </div>
        </div>
        <div class="py-4" style="flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 33px; display: flex; padding-left: 5%">
            <div style="color: #656565; font-size: 32px; font-family: Rajdhani; font-weight: 600; word-wrap: break-word">Objective :</div>
            <ol style="font-family: Open Sans; font-size: 20px; margin-left: 10%;">
                <li>Test objective 1</li>
                <li>Test objective 2</li>
                <li>Test objective 3</li>
            </ol>
        </div>
        <div class="py-4" style="flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 33px; display: flex; padding-left: 5%">
            <div style="color: #656565; font-size: 32px; font-family: Rajdhani; font-weight: 600; word-wrap: break-word">Person of Interest :</div>
            <ol style="font-family: Open Sans; font-size: 20px; margin-left: 10%;">
                <li>MR A - +6012345842 - encik_a@gmail.com</li>
                <li>MR B - +6012345842 - encik_a@gmail.com</li>
            </ol>
        </div>
    </div>
</main>
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    
@endsection

@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>
@endsection