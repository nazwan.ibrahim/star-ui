@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','User Profile')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   

    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

    .btn-add-2:hover {
      color: #fff;
    }

    .btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
      color: #fff !important;
      opacity: 0.5;
    }  
    .btn-secondary-6 {
      background-color: #fff;
      color: #5A8DEE;
      border: 1px solid #5A8DEE;
      border-radius: 5px;
      padding: 10px 20px;
      font-size: 16px;
      font-family: poppins-semibold, sans-serif;
      line-height: 20px;
      text-align: left;
    }

    .btn-secondary-6:hover {
      background-color: #5A8DEE;
      color: #fff;
    }

    .font-title{
        color: var(--gray-950, #0C111D);
    /* Display xs/Semibold */
    font-family: Inter;
    font-size: 24px;
    font-style: normal;
    font-weight: 600;
    line-height: 32px; /* 133.333% */
    }

</style>
@endsection

@section('content')
<main>
  <section class="header-top-margin">
    <div class="d-flex justify-content-between align-items-center">
    <button class="btn btn-primary" id="upcomingTrainingBtn">Upcoming Training</button>
    <button class="btn btn-secondary" id="trainingListBtn">Training List</button>
    </div>

    <!-- Upcoming Training Tab -->
    <div class="font-title" id="upcomingTrainingTab">
    <div class="d-flex justify-content-between align-items-center">
    <div>
        <button class="btn btn-primary mr-2" data-toggle="modal" data-target="#filterModal">Filter</button>
    </div>
    <div class="input-group">
    <input type="text" class="form-control" placeholder="Search...">
    <div class="input-group-append">
      <button class="btn btn-primary" type="button">Search</button>
    </div>
    </div>
    <div>
        <button class="btn btn-info">Button #3</button>
    </div>
    </div>
    
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Programme</th>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Venue</th>
          <th>Fee (RM)</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>
            <a href="#" class="btn btn-info">Edit</a>
            <a href="#" class="btn btn-danger">Delete</a>
          </td>
        </tr>
        <tr>
          <td>2</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>
            <a href="#" class="btn btn-info">Edit</a>
            <a href="#" class="btn btn-danger">Delete</a>
          </td>
        </tr>
        <tr>
          <td>3</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>
            <a href="#" class="btn btn-info">Edit</a>
            <a href="#" class="btn btn-danger">Delete</a>
          </td>
        </tr>
        <tr>
          <td>4</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>
            <a href="#" class="btn btn-info">Edit</a>
            <a href="#" class="btn btn-danger">Delete</a>
          </td>
        </tr>
      </tbody>
    </table>
    </div>

    <!-- Training List Tab -->
    <div class="font-title" id="trainingListTab" style="display: none;">
    <div class="d-flex justify-content-between align-items-center">
    <div>
        <button class="btn btn-primary mr-2" data-toggle="modal" data-target="#filterModal">Filter</button>
    </div>
    <div class="input-group">
    <input type="text" class="form-control" placeholder="Search...">
    <div class="input-group-append">
      <button class="btn btn-primary" type="button">Search</button>
    </div>
    </div>
    <div>
        <button class="btn btn-info">Button #3</button>
    </div>
    </div>
    
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Partner Name</th>
          <th>Course Category</th>
          <th>Training Type</th>
          <th>Training Date (Start)</th>
          <th>Training Date (End)</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>
            <a href="#" class="btn btn-info">Edit</a>
            <a href="#" class="btn btn-danger">Delete</a>
          </td>
        </tr>
        <tr>
          <td>2</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>
            <a href="#" class="btn btn-info">Edit</a>
            <a href="#" class="btn btn-danger">Delete</a>
          </td>
        </tr>
        <tr>
          <td>3</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>
            <a href="#" class="btn btn-info">Edit</a>
            <a href="#" class="btn btn-danger">Delete</a>
          </td>
        </tr>
        <tr>
          <td>4</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>-</td>
          <td>
            <a href="#" class="btn btn-info">Edit</a>
            <a href="#" class="btn btn-danger">Delete</a>
          </td>
        </tr>
      </tbody>
    </table>
    </div>

    <div class="d-flex justify-content-between align-items-center">
      <div>
        <button class="btn btn-primary mr-2" data-toggle="modal" data-target="#filterModal">Filter</button>
      </div>
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search...">
        <div class="input-group-append">
          <button class="btn btn-primary" type="button">Search</button>
        </div>
      </div>
      <div>
        <button class="btn btn-info">Button #3</button>
      </div>
    </div>
  </section>
</main>

<!-- Filter (Upcoming Training) Dialog -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="filterModalLabel">Filter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- Filter Form -->
        <form>
          <div class="form-group">
            <label for="filterCourse">Course</label>
            <select class="form-control" id="filterCourse">
              <option>Option 1</option>
              <option>Option 2</option>
              <option>Option 3</option>
            </select>
          </div>
          <div class="form-group">
            <label for="filterRegion">Region</label>
            <select class="form-control" id="filterRegion">
              <option>Option 1</option>
              <option>Option 2</option>
              <option>Option 3</option>
            </select>
          </div>
          <div class="form-group">
            <label for="filterState">State</label>
            <select class="form-control" id="filterState">
              <option>Option 1</option>
              <option>Option 2</option>
              <option>Option 3</option>
            </select>
          </div>
          <div class="form-group">
            <label for="filterStartDate">Start Date</label>
            <input type="date" class="form-control" id="filterStartDate">
          </div>
          <div class="form-group">
            <label for="filterEndDate">End Date</label>
            <input type="date" class="form-control" id="filterEndDate">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Update</button>
      </div>
    </div>
  </div>
</div>

<!-- Filter (Training List) Dialog -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="filterModalLabel">Filter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- Filter Form -->
        <form>
          <div class="form-group">
            <label for="filterCourse">Status</label>
            <select class="form-control" id="filterStatus">
              <option>Option 1</option>
              <option>Option 2</option>
              <option>Option 3</option>
            </select>
          </div>
          <div class="form-group">
            <label for="filterRegion">Training Type</label>
            <select class="form-control" id="filterTrainingType">
              <option>Option 1</option>
              <option>Option 2</option>
              <option>Option 3</option>
            </select>
          </div>
          <div class="form-group">
            <label for="filterState">Course Category</label>
            <select class="form-control" id="filterCourseCategory">
              <option>Option 1</option>
              <option>Option 2</option>
              <option>Option 3</option>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Update</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('vendor-scripts')

@endsection

@section('page-scripts')
<script src="{{ asset('/js/hello2/hello2.js') }}"></script>
<script src="{{ asset('/js/hello/hello.js') }}"></script>

<script>
  document.getElementById('upcomingTrainingBtn').addEventListener('click', function() {
    document.getElementById('upcomingTrainingTab').style.display = 'block';
    document.getElementById('trainingListTab').style.display = 'none';
  });
  
  document.getElementById('trainingListBtn').addEventListener('click', function() {
    document.getElementById('upcomingTrainingTab').style.display = 'none';
    document.getElementById('trainingListTab').style.display = 'block';
  });
</script>
</script>
@endsection
