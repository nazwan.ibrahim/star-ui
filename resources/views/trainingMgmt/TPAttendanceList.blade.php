@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Trainer-Attendance list')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
   
<!-- datepicker styles -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css">
<link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css'>

@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.icon-editIcon:before {
  content: "\e916";
}

.table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid #18eeaa;
        background: var(--success-25, #f6faf7);
        padding: 4px 8px;
        color: #18d647;
    }

    .inactive-status {
        border-radius: 8px;
        border: 1px solid #F04438;
        background: var(--success-25, #FFFBFA);
        padding: 4px 8px;
        color: #F04438;
    }


</style>
@endsection

@section('content')
<main class="header-top-margin" style="justify-content: center; display: flex;">
    @php
    $lastParameter = request()->segment(count(request()->segments()));
    $segments = request()->segments();
    $secondLastParameter = count($segments) >= 2 ? $segments[count($segments) - 2] : null;
    
@endphp  
<section class="col-md-11">

  <div class="container col-md-12" style="margin-top:20px;">
    <ol class="breadcrumb p-0 mb-0 poppins-semibold-12 light-grey">
             
                    <li class="nav-item mg-sm ">
                        {{-- <a class="nav-link" href="{{ route('admin.attendance',$lastParameter) }}">Attendance List</a> --}}
                        <a class="nav-link" href="{{ route('TP.attendance', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Attendance List</a>
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>      
                    <li class="nav-item mg-sm ">
                        <a class="nav-link" href="{{ route('TP-feedbacklist', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Feedback</a>
                        {{-- <a class="nav-link" href="{{ route('trainingpartner.feedbackquestion',$lastParameter) }}">Feedback</a> --}}
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>     
                    @foreach ($reportcheck as $rep) 
                      @if (($rep->id_training_type == '2'))
                    <li class="nav-item mg-sm ">
                        <a class="nav-link" href="{{ route('endorse-exam-resultTP', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Submit Exam Result</a>
                        {{-- <a class="nav-link" href="{{ route('TP-UpdateExamResult-List', $secondLastParameter) }}">Endorse Exam Result --}}
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    @endif
                    @endforeach
                    <li class="nav-item mg-sm ">
                        <a class="nav-link" href="{{ route('TP-TrainingReport-TM', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Training Report</a>
                        {{-- <a class="nav-link" href="{{ route('TP-TrainingReport',$lastParameter) }}">Training Report --}}
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item mg-sm ">
                      <a class="nav-link" href="{{ route('TP-Payment-to-seda', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Payment To Seda</a>
                          <span class="sr-only">(current)</span>
                      </a>
                  </li>
                  </ol>
                </div>

    <div>
      
    <div class="row">
        <h2 class="page-title py-2" style="text-align: left !important; margin-bottom: 1%;margin-top: 1%;"></h2>
      </div>
      <div class="row">
        <h2 class="page-title py-2" style="text-align: left !important; margin-bottom: 1%;">List of Attendance</h2>
      </div>
    <div class="card mt-7" style="text-align: left; background-color: #fbf9f9; padding: 2%; margin-bottom: 2%;">
      <div class="row">
          
              <div><span class="contentUpdate">Provider:</span>&nbsp;&nbsp;<span class='title' id="provider_name"></span></div>
              <div><span class="contentUpdate">Start Date :</span>&nbsp;&nbsp;<span id="date_start"></span></div>
              <div><span class="contentUpdate">End Date :</span>&nbsp;&nbsp;<span id="date_end"></span></div>
              <div><span class="contentUpdate">Venue :</span>&nbsp;&nbsp;<span  id="venue"></span></div>
              <div><span class="contentUpdate">Batch No :</span>&nbsp;&nbsp;<span  id="batch_no_full"></span></div>

         
      </div>
    </div>

    
    <div class="table-responsive">
        <table class="table table-bordered dataTable no-footer" id="TPattendanceList" style="width: 100%; border-bottom: white;">
            <thead class="table-header">
                <tr >
                    <th class="table-title text-center" style="border-bottom: none;" width=40;>No.</th>
                    .<th class="table-title text-center" style="display: none;">Participant ID</th>
                    <th class="table-title text-center" style="border-bottom: none;">Participant</th>
                    <th class="table-title text-center" style="border-bottom: none;">Attend Check</th>                    
                    <th class="table-title text-center" style="border-bottom: none;"> 
                        <div class="datepicker date input-group" id="dateIn" style="width: 100%; margin-left: 50px;">
                          Attend Date
                          <div class='col-sm-6'>
                            <div class="input-group date" id="datePickerIn">
                              <input type='text' class="form-control" style="display:none" data-date-format="dd/mm/yyyy" />
                              <span class="input-group-addon"><i class="fa fa-calendar-o" aria-hidden="true"></i></span>
                            </div>
                          </div>
                        </div>
                      </th>
                    <th class="table-title text-center" style="border-bottom: none;"> 
                      <div class="datepicker date input-group" id="timeIn" style="width: 155px; margin-left: 50px;">
                        Time In
                        <!-- <div class="input-group-append">
                          <span class="input-group-text" style="margin-left: 11px;"><i class="fa fa-calendar"></i></span>
                        </div> -->
                        <div class='col-sm-6'>
                            <div class="input-group date" id="timePickerIn">
                                <input type='text' class="form-control" style="display:none" data-date-format="HH:mm" />
                                <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                            </div>
                        </div>
                      </div>
                    </th>
                    <th class="table-title text-center" id="timeOut" style="border-bottom: none;"> 
                      <div class="datepicker date input-group" style="width: 155px; margin-left: 50px;">
                        Time Out
                        <!-- <div class="input-group-append">
                          <span class="input-group-text" style="margin-left: 11px;"><i class="fa fa-calendar"></i></span>
                        </div> -->
                        <div class='col-sm-6'>
                            <div class="input-group date" id="timePickerOut">
                                <input type='text' class="form-control" style="display:none" data-date-format="HH:mm" />
                                <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                            </div>
                        </div>
                      </div>
                    </th>
                    <th class="table-title text-center" style="border-bottom: none;">Remark</th>  
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <div class="d-flex justify-content-end">
            <div style="margin:10px;">
              <a href="{{ route('exam-list-admin') }}" class="btn btn-secondary-6 btn-block">Back</a>
            </div>
            <div style="margin:10px;">
              <button  id="insertAttendanceButton" class="btn btn-secondary-6 btn-block">
                 Save
              </button>
            </div>   
            <div style="margin:10px;">
              <button  id="AttendanceButton" class="btn btn-secondary-6 btn-block">
                  Submit
              </button>
            </div>   
          </div>

          <h2 class="page-title py-2" style="text-align: left !important; margin-bottom: 1%;margin-top: 1%;">Status of Attendance</h2>

          <div class="table-responsive">
          <table class="table table-bordered dataTable no-footer text-left" id="TPStatusList" style="width: 100%; border-bottom: white;">
                  <thead class="table-header">
                  <tr>
                          <th class="table-title" style="border-bottom: none;" width="40">No.</th>
                          <th class="table-title text-center" style="display: none;">Participant ID</th>
                          <th class="table-title" style="border-bottom: none;">Participant</th>
                          <th class="table-title" style="border-bottom: none;">Total Attend</th>
                          <th class="table-title" style="border-bottom: none;">Status</th>
                          <th id="remark" class="table-title" style="border-bottom: none;">Remark</th>
                   </tr>
      </thead>
      <tbody>
      </tbody>
  </table>
  </section>
</main>

<section>
  <div class="modal" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      
      <div class="modal-content">
        <div class="modal-header" style="padding:1.3rem;">
          <h4 class="modal-title text-3" id="myModalLabel33">Success!
          </h4>
          <button type="button" class="close model-remove-icon-bg" data-dismiss="modal" aria-label="Close">
            <i class="bx bx-x modal-close-icon-1"></i>
          </button>
        </div>

        <div class="modal-body" style="padding: 1.3rem 1.3rem 0 1.3rem;">
          <div class="modal-content-border">
            <form id="formQueryResult">
            
                    
                <div class="row">
                  <div class="col-sm-12">
                    <p class="mb-0">
                      <label for="error-message"></label>
                      <br>
                      <span id="error-message">
                        Success!
                      </span>
                      <br>

                    </p>
                  </div>
                </div>
                   

            </form>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
            <span class="d-none d-sm-block">close</span>
          </button>
         
         
        </div>


        
      </div>
    </div>
  </div>
 </section>
  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script> 
    
  
@endsection



@section('page-scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js'></script>

  

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";

</script>


<script src="{{ asset('js/trainingMgmt/TPattendanceList.js') }}"></script>

@endsection
