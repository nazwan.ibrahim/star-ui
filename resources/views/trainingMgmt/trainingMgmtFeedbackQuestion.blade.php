@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Training Management Feedback Question')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }

    .dataTables_length {
        display: none;
    }

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .btn-add-user:hover {
        background-color: #3e6be1;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }   

    .btn-actions {
        display: flex;
        gap: 10px;
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.icon-editIcon:before {
  content: "\e916";
}

.page-title{
    color: #656565;
    font-family: Rajdhani;
    font-size: 26px;
    font-style: normal;
    font-weight: 600;
    line-height: 38px; /* 146.154% */
}

.contentUpdate{
    color: #454545;
    font-family: Open Sans;
    font-size: 18px;
    font-style: normal;
    font-weight: 600;
    line-height: 28px; /* 155.556% */
    text-align: left;
}

.font-title-2{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 18px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.table {
    width: 100%;
    border-collapse: collapse;
    margin: 0;
    }

   

    .table th, .table td {
    padding: 10px;
    text-align: center;
    border: 1px solid #ddd;
    }

    /* Add hover effect on table rows */
    .table tbody tr:hover {
    background-color: #f5f5f5;
    }

</style>
@endsection

@section('content')
<main style="padding:50px;margin:4px;" style="display: flex; justify-content: center;">

     
    <section class="header-top-margin">
    @csrf    

    <div class="row mx-auto">
      <label class="page-title" style="text-align: left;width:100%;">Training Feedback Question</label>
    </div>

    
    <div class="table-responsive">
        <div id="feedbackTablewrap" class="dataTables_wrapper no-footer">
            <table class="table table-striped dataTable no-footer" id="feedbackTable" style="width: 100%; border-bottom: white;">
                <thead class="table-header">
                    <tr>
                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;" width="30">No.</th>
                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Question Type</th>
                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;">Question</th>
                        <th class="table-title text-center" style="border-bottom: none; text-align: center; white-space: nowrap;" width="50">Action</th>
                    </tr>
                    <tr>
                        <th class="table-title text-center" width="40">Filter</th>
                        <th class="table-title text-center"> 
                            <input type="text" placeholder="Search" class="form-control" />
                        </th>
                        <th class="table-title text-center"> 
                            <input type="text" placeholder="Search" class="form-control" />
                        </th>
                        <th class="table-title text-center"></th>
                    </tr>
                </thead>
                <tbody style="text-align: left; width:100%;">
            @if ($feedbackquestion->isEmpty())
                <tr>
                    <td colspan="4">No data available</td>
                </tr>
            @else
                @foreach ($feedbackquestion as $feedback)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $feedback->feedback_type }}</td>
                        <td>{{ $feedback->question }}</td>
                        <td>
                            <button type="button" class="btn btn-danger delete-question" data-id="{{ $feedback->questionid }}" data-toggle="modal" data-target="#deleteModal">Delete</button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
            </table>
        </div>
</div>
    </section>
</main>

<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Confirm Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this feedback question?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="cancelBtn">Cancel</button>
                <form id="deleteForm">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="successModal" tabindex="-1" aria-labelledby="successModalLable" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="successModalLabel">Success</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body alert alert-success">
                Feedback Question added successfully.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="okBtn">OK</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="delete2Modal" tabindex="-1" aria-labelledby="delete2ModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="delete2ModalLabel">Success</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body alert alert-danger">
                Feedback Question deleted successfully.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="dltnBtn">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="newModal" tabindex="-1" aria-labelledby="newModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newModalLabel">New Question</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="{{ route('admin.addfeedbackquestion') }}" method="POST" >
                    @csrf

                    <div class="form-group">
                            <label class="field-title" for="courseName"><b>Question Type</b></label>
                            <input type="text" class="form-control" id="feedback_type" name="feedback_type" placeholder="Enter a new type">
                        </div>
                        <div class="form-group">
                            <label class="field-title" for="courseName"><b>Question</b></label>
                            <input type="text" class="form-control" id="newQuestion" name="newQuestion" placeholder="Enter a new question">
                        </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="addNewQuestionBtn">Add</button>

                <button type="button" class="btn btn-secondary" id="okBtnk">OK</button>
            </div>
        </div>
    </div>
</div>



  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script> 
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    <script>
    // Add this script to handle the OK button click
    $(document).ready(function () {
    $('#okBtn').on('click', function () {
        $('#successModal').modal('hide'); // This line will hide the modal

        // Refresh
        setTimeout(function () {
                location.reload();
            }, 500);
    });
});
</script>
    <script>
    $(document).ready(function () {

        $('#feedbackTable').DataTable({
        dom: 'lfrtip',
        ordering: false,
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                console.log(column.header()); // Output column headers to the console
                $('input', column.header()).on('keyup change clear', function () {
                    if (column.search() !== this.value) {
                        // Use case-insensitive search for the "Status" column (index 5)
                        column.search(this.value, false, true).draw();
                    }
                });
            });
        },
        responsive: true,
        lengthMenu: [5, 10, 15, 20],
        pageLength: 20,
        language: {
            info: "_START_ to _END_ from _TOTAL_ entry",
            infoEmpty: "0 from 0 to 0 entry",
            lengthMenu: " _MENU_ Entry",
            paginate: {
                next: "Next",
                previous: "Previous",
            },
            zeroRecords: "No record found",
        },
        });
        var addUserButton = $('<button id="addUserButton" class="circular-button" style="color: white">' +
                              '<i class="fas fa-plus"></i>' +
                              '</button>');

          addUserButton.on('click', function () {
            $('#newModal').modal('show');
          });

          addUserButton.css({
            width: '37px',
            height: '37px',
            backgroundColor: '#3498DB',
            border: '2px solid #3498DB',
            borderRadius: '50%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            cursor: 'pointer',
          });
        
          addUserButton.css('margin-left', '10px');
          $('#feedbackTablewrap .dataTables_filter').append(addUserButton);
        
         

    
        

        

$('#addQuestionForm').on('submit', function (e) {
    e.preventDefault(); // Prevent the default form submission

    var newQuestion = $('#newQuestion').val();

    if (newQuestion.trim() !== '') {
        console.log('Triggered');

        $.ajax({
            type: 'POST',
            url: "{{ route('admin.addfeedbackquestion') }}",
            data: {
                '_token': $('input[name=_token]').val(),
                'newQuestion': newQuestion,
            },
            success: function (data) {
                console.log(data);
                $('#newQuestion').val('');

                location.reload(); // This will refresh the DataTable

      
            },
            error: function (error) {
                console.log(error);
            }
        });

    }
});

$('#addNewQuestionBtn').on('click', function (e) {
    e.preventDefault(); // Prevent the default button click behavior
    
    var newQuestion = $('#newQuestion').val();
    var feedback_type = $('#feedback_type').val();

    if (newQuestion.trim() !== '') {
        console.log('Triggered');
        $('#addNewQuestionBtn').prop('disabled', true);

        $.ajax({
            type: 'POST',
            url: "{{ route('admin.addfeedbackquestion') }}",
            data: {
                '_token': $('input[name=_token]').val(),
                'newQuestion': newQuestion,
                'feedback_type': feedback_type,
            },

            success: function (data) {
                console.log("response ", data);

                // Update the table with the new data without reloading the page
                updateTable(data.newQuestion, data.feedback_type, data.questionId);

                if (data.success) {
                    $('#newModal').modal('hide');
                    $('#successModal').modal('show');
                } else {
                    $('#newModal').modal('hide');
                    $('#errorModal').modal('show');
                }
            },
            error: function (error) {
                console.log(error);

                $('#errorMessage').html('Error adding question. Please try again later.').show();
                $('#successMessage').hide();
            }
        });
    }
});

function updateTable(newQuestion, feedbackType, questionId) {
    // Assuming your table has an ID like 'feedbackTable'
    var table = $('#feedbackTable').DataTable();

    // Add the new row to the table
    table.row.add([
        table.data().count() + 1,  // Incremental index, you may need to adjust this based on your actual data
        feedbackType,
        newQuestion,
        '<button type="button" class="btn btn-danger delete-question" data-id="' + questionId + '">Delete</button>'
    ]).draw();

    // Optionally, you can sort the table after adding the new row
    table.order([0, 'asc']).draw();
}
});

$(document).ready(function () {
    $('.delete-question').on('click', function () {
        var questionId = $(this).data('id');
        var action = "/admin/delete-feedback/" + questionId;

        var form = $('#deleteForm');
        form.attr('action', action);

        // Show the modal
        $('#deleteModal').modal('show');
    });

    $('#deleteModal').on('shown.bs.modal', function (e) {
        $('#cancelBtn').on('click', function () {
            // Hide the modal
            $('#deleteModal').modal('hide');
        });
    });

    $('#deleteModal').on('hidden.bs.modal', function (e) {
        // Unbind the form submission event to prevent duplicate submissions
        $('#deleteForm').off('submit');
    });

    $('#deleteModal').on('shown.bs.modal', function (e) {
        // Bind the form submission to an event handler
        $('#deleteForm').on('submit', function (e) {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: $('#deleteForm').attr('action'),
                data: $('#deleteForm').serialize(),
                success: function (data) {
                    // Hide the modal on success
                    $('#deleteModal').modal('hide');

                    if (data.success) {
                        // Show the success modal
                        $('#delete2Modal').modal('show');
                    } else {
                        // Show the error message
                        $('#errorMessage').html('Error deleting question. Please try again later.').show();
                        $('#successMessage').hide();
                    }
                },
                error: function (error) {
                    console.log(error);

                    // Show the error message
                    $('#errorMessage').html('Error deleting question. Please try again later.').show();
                    $('#successMessage').hide();
                }
            });
        });
    });

    $('#dltnBtn').on('click', function () {
        // Hide the success modal
        $('#delete2Modal').modal('hide');

        // Optionally, refresh the page after a delay
        setTimeout(function () {
            location.reload();
        }, 500);
    });
});


   
</script>





@endsection



@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";

</script>



@endsection
