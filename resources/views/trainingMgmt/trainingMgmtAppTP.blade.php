@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Training Management')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    /* .dataTables_length {
        display: none;
    } */

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
        border-bottom: none;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
        text-align: left;
    }

    .table-row td {
        padding: 10px;
        border-bottom: none;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .failed-status {
        border-radius: 8px;
        border: 1px solid #F04438;
        background: var(--success-25, #FFFBFA);
        padding: 4px 8px;
        color: #F04438;
    }

    .reserved-status {
        border-radius: 8px;
        border: 1px solid #575353;
        background: var(--success-25, #FFFBFA);
        padding: 4px 8px;
        color: #575353;
    }

    .filter-dropdown{
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }

    /* Add styles for the table */
    .table {
    width: 100%;
    border-collapse: collapse;
    margin: 0;
    }

   

    .table th, .table td {
    padding: 10px;
    text-align: center;
    border: 1px solid #ddd;
    }

    /* Add hover effect on table rows */
    .table tbody tr:hover {
    background-color: #f5f5f5;
}

.textAlign{
    text-align: left;
}

</style>
@endsection

@section('content')

<main class="header-top-margin col-md-10 mx-auto">

<!-- <div class="container">
    <div class="row"> -->
        <!-- Upcoming Training Navbar -->
        <!-- <div class="col">
            <div style="background-color: #253C7B; height: 50%;">
                <div class="d-flex justify-content-center mt-4">
                    <div class="d-flex justify-content-center">
                        <button class="btn btn-secondary-6 show-upcoming-training" style="color: white; border: none;">
                            MY TRAINING LIST
                        </button>
                    </div>
                </div>
            </div>
        </div> -->

        <!-- Training List Navbar -->
        <!-- <div class="col">
            <div style="background-color: #253C7B; height: 50%;">
                <div class="d-flex justify-content-center mt-4">
                    <div class="d-flex justify-content-center">
                        <button class="btn btn-secondary-6 show-training-list" style="color: white; border: none;">
                            <i class="bx bx-check d-block d-sm-none"></i>
                            <span class="d-none d-sm-block">UPCOMING AND ONGOING TRAINING</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
@if ($message = Session::get('success'))
         <div style="align-content: center;text-align: center;" class="alert alert-success" id="successAlert">
             <p>{{ $message }}</p>
         </div>
     @else
         @if ($message = Session::get('error'))
             <div style="align-content: center;text-align: center;" class="alert alert-danger" id="errorAlert">
                 <p>{{ $message }}</p>
             </div>
         @endif
     @endif 


    <div class="mb-3">
        <h2 class="page-title py-2" style="text-align: left !important;">Training Management</h2>
    </div>

    {{-- Upcoming Training Section --}}
    <div class="table-responsive upcoming-training-section">
        <div id="upcomingList_wrapper" class="dataTables_wrapper no-footer">
            <table class="table table-striped dataTable no-footer" id="upcomingList" style="width: 100%; border-bottom: white;">
                <thead class="table-header">
                    <tr>
                        <th class="table-title text-center" style="border-bottom: none;">No.</th>
                        <th class="table-title text-center" style="border-bottom: none;">Programme</th>
                        <th class="table-title text-center" style="border-bottom: none;">Course Category</th>
                        <th class="table-title text-center" style="border-bottom: none;">Training Type</th>
                        <th class="table-title text-center" style="border-bottom: none;">Start Date</th>
                        <th class="table-title text-center" style="border-bottom: none;">End Date</th>
                        <th class="table-title text-center" style="border-bottom: none;">Status</th>
                        <th class="table-title text-center" style="border-bottom: none;">Action</th>
                    </tr>
                </thead>
                <tbody class="table-content">
                </tbody>
            </table>
        </div>
    </div>
    {{-- Upcoming Training Section --}}

    <div class="text-center mb-3">
        <h2 class="page-title py-2" style="text-align: left !important;">Upcoming & Ongoing Training</h2>
    </div>


    {{-- Training List Section --}}
    <div class="table-responsive training-list-section">
        <div id="trainingList_wrapper" class="dataTables_wrapper no-footer">
            <table class="table table-striped dataTable no-footer" id="trainingListTP2" style="width: 100%; border-bottom: white;">
                <thead class="table-header">
                    <tr>
                        <th class="table-title text-center" style="border-bottom: none;">No.</th>
                        <th class="table-title text-center" style="border-bottom: none;">Programme</th>
                        <th class="table-title text-center" style="border-bottom: none;">Course Category</th>
                        <th class="table-title text-center" style="border-bottom: none;">Training Type</th>
                        <th class="table-title text-center" style="border-bottom: none;">Start Date</th>
                        <th class="table-title text-center" style="border-bottom: none;">End Date</th>
                        <th class="table-title text-center" style="border-bottom: none;">Status</th>
                        <th class="table-title text-center" style="border-bottom: none;">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    {{-- Training List Section --}}
</main>
@endsection


@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    
@endsection



@section('page-scripts')

{{-- our own js --}}
<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>

<script src="{{ asset('js/trainingMgmt/trainingListAppTP.js') }}"></script>

<!-- <script>
    $(document).ready(function() {
    const $upcomingTrainingSection = $(".upcoming-training-section");
    const $trainingListSection = $(".training-list-section");
    const $viewReportButton1 = $("#viewReportButton1");
    const $viewReportButton2 = $("#viewReportButton2");

    $upcomingTrainingSection.show();
    $trainingListSection.hide();
    $viewReportButton1.hide();
    $viewReportButton2.hide();

    $(".show-upcoming-training").click(function() {
        $upcomingTrainingSection.show();
        $trainingListSection.hide();
        $viewReportButton1.show();
        $viewReportButton2.hide();
    });

    $(".show-training-list").click(function() {
        $upcomingTrainingSection.hide();
        $trainingListSection.show();
        $viewReportButton1.hide();
        $viewReportButton2.show();

        if (!$.fn.dataTable.isDataTable('#trainingList2')) {
            initializeDataTableTrainingList2();
        } else {
            refreshDataTableTrainingList2();
        }
    });

    if (!$.fn.dataTable.isDataTable('#upcomingTrainingList')) {
        initializeDataTableUpcomingTraining();
    } else {
        refreshDataTableUpcomingTraining();
    }

    if (!$.fn.dataTable.isDataTable('#trainingListTP2')) {
        initializeDataTableTrainingList2();
    } else {
        refreshDataTableTrainingList2();
    }
    });

    function initializeDataTableUpcomingTraining() {
        $("#upcomingTrainingList").DataTable({    
        });
    }

    function refreshDataTableUpcomingTraining() {
        let table = $("#upcomingTrainingList").DataTable();
        table.ajax.reload();
    }

    function initializeDataTableTrainingList2() {
        $("#trainingList2").DataTable({       
        });
    }

    function refreshDataTableTrainingList2() {
        let table = $("#trainingList2").DataTable();
        table.ajax.reload();
    }
</script> -->
@endsection