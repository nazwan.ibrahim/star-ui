@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','TP - Training Report')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .content-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 20px;
        font-style: normal;
        font-weight: 600;
    }

    .contentUpdate{
        color: #454545;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
        line-height: 28px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.form-group {
    display: flex;
    align-items: center; /* Vertical alignment */
}

label {
    width: 100px; /* Adjust as needed for your design */
    text-align: left; /* Adjust for alignment (left, right, center) */
    /* margin-right: 10px; Spacing between label and input */
}

</style>
@endsection

@section('content')
<!-- Navbar -->
<!-- <nav class="navbar navbar-expand-lg"> -->


<!-- End Navbar -->

<main style="padding:50px;margin:50px;text-align: left">

    @php
    $lastParameter = request()->segment(count(request()->segments()));
    $segments = request()->segments();
    $secondLastParameter = count($segments) >= 2 ? $segments[count($segments) - 2] : null;
    
    @endphp

<div class="container col-md-12" style="margin-top:-20px;">
    <ol class="breadcrumb p-0 mb-0 poppins-semibold-12 light-grey">
             
            <li class="nav-item mg-sm ">
                {{-- <a class="nav-link" href="{{ route('admin.attendance',$lastParameter) }}">Attendance List</a> --}}
                <a class="nav-link" href="{{ route('TP.attendance', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Attendance List</a>
                    <span class="sr-only">(current)</span>
                </a>
            </li>      
            <li class="nav-item mg-sm ">
                <a class="nav-link" href="{{ route('TP-feedbacklist', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Feedback</a>
                {{-- <a class="nav-link" href="{{ route('trainingpartner.feedbackquestion',$lastParameter) }}">Feedback</a> --}}
                    <span class="sr-only">(current)</span>
                </a>
            </li>     
            @foreach ($reportcheck as $rep) 
                 @if (($rep->id_training_type == '2')) 
                    <li class="nav-item mg-sm ">
                        <a class="nav-link" href="{{ route('endorse-exam-resultTP', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Submit Exam Result</a>
                        {{-- <a class="nav-link" href="{{ route('TP-UpdateExamResult-List', $secondLastParameter) }}">Endorse Exam Result --}}
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                @endif
            @endforeach
            <li class="nav-item mg-sm ">
                <a class="nav-link" href="{{ route('TP-TrainingReport-TM', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Training Report</a>
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item mg-sm ">
              <a class="nav-link" href="{{ route('TP-Payment-to-seda', ['batch_no' => $secondLastParameter, 'id' => $lastParameter]) }}">Payment To Seda</a>
                  <span class="sr-only">(current)</span>
              </a>
          </li>
    </ol>
</div>
   
  <section class="header-top-margin">
    <div class="card mt-7" style="padding:50px;margin:50px;background-color:#fbf9f9">
    
    {{-- <form method="POST" action="/submit">
        @csrf --}}
        
        <div class="form-group">
            <label class="page-title" style="white-space: nowrap;">Training Report</label>
        </div>
    
        <div class="row">
        <div>
    
<div class="row form-list">    
        <div class="row">
                <div class="col-auto" style="position: relative;">
                    <h4 class="user-name mb-0"><span id="trainingName"></span></h4>
                    <input type="hidden" id="trainingNameV" name="trainingNameV" value="">
                 </div>
    
            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="contentUpdate" for="name">Provider :</label>
                        <span class="contentUpdate" id="providerCompany"></span>
                        <input type="hidden" id="providerCompanyV" name="providerCompanyV" value="">
                    </div>
                </div>  
            </div>
    
            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="contentUpdate" for="name">Start Date :</label>
                        <span class="contentUpdate" id="dateStart"></span>
                        <input type="hidden" id="dateStartV" name="dateStartV" value="">
                    </div>
                </div>
            </div>
    
            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="contentUpdate" for="name">End Date :</label>
                        <span class="contentUpdate" id="dateEnd"></span>
                        <input type="hidden" id="dateEndV" name="dateEndV" value="">
                    </div>
                </div>
            </div>

            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="contentUpdate" for="name">Venue :</label>
                        <span class="contentUpdate" id="Venueid"></span>
                        <input type="hidden" id="VenueidV" name="VenueidV" value="">
                    </div>
                </div>
            </div>
    
            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate" id="dateStart">Training Name: </span>
                        <span class="contentUpdate" id="courseName"  style="margin-left:2.5em;"></span>
                        <input type="hidden" id="courseNameV" name="courseNameV" value="">
                    </div>
                </div>
            </div>
    
            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate">Training Code : </span>
                        <span class="contentUpdate" id="courseCode" style="margin-left:2.5em;"> </span>
                        <input type="hidden" id="courseCodeV" name="courseCodeV" value="">
                    </div>
                </div>
            </div>

            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate">Training Batch :  </span>
                        <span class="contentUpdate" id="BatchNumber" style="margin-left:2.5em;"> </span>
                        <input type="hidden" id="BatchNumberV" name="BatchNumberV" value="">
                    </div>
                </div>
            </div>
    
            {{-- <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate">Course Date :</span>
                        <span class="contentUpdate" id="dateStart2"> 9 Mar 2023</span>
                    </div>
                </div>
            </div> --}}
    
            <div class="row" style="margin: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate">Competent Trainers :</span>
                        <span class="contentUpdate" id="CompetenttrainerName"></span>
                        <select name="Competent_trainer" id="Competent_trainer" class="form-control smaller-input">
                                    <option value="">-- Please Choose --</option>
                                @foreach ($report as $rep)
                                    <option value="{{ $rep->trainername }}">{{ $rep->salutation }} {{ $rep->trainername }} </option>
                                @endforeach
                        </select>
                    </div>
                </div>
            </div>    


            <div class="row" style="margin-top: 1%;margin-left: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate">Synopsis :<br><div style="color: red; font-size:10px;">( * Maximum characters 200) </div> </span>
                        <span class="contentUpdate" id="CompetenttrainerName"></span>
                        <span class="contentUpdate" id="training_description" style="margin: 5%;"><textarea class="form-control smaller-input"  id="training_Synopsis" name="training_Synopsis" rows="4" cols="50" maxlength="200"></textarea></span>
                   
                    </div>
                </div>
            </div>    
    
            


            <div class="row" style="margin-top: 1%;margin-left: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate">Introduction : <br><div style="color: red; font-size:10px;">( * Maximum characters 200) </div> </span>
                        <span class="contentUpdate" id="training_description" style="margin: 2%;"><textarea class="form-control smaller-input" id="training_Introduction" name="training_Introduction" rows="4" cols="50" maxlength="200"></textarea></span>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 1%; margin-left: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate" style="margin: 2.5%;">Prepared By:</span>
                        <span class="contentUpdate" id="CompetenttrainerName"></span>
                        <select name="trainer" id="trainer" class="form-control smaller-input">
                                    <option value="">-- Please Choose --</option>
                                @foreach ($report as $rep)
                                    <option value="{{ $rep->trainername }}">{{ $rep->salutation }} {{ $rep->trainername }} </option>
                                @endforeach
                        </select>
                    
                    </div>
                </div>
            </div>    
    
           
    
            <label class="content-title" style="margin-top: 1%;margin-left: 1%;">Conclusion</label>
    
    
            <div class="row" style="margin-top: 1%;margin-left: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate">Conclusion : <br><div style="color: red; font-size:10px;">( * Maximum characters 200) </div> </span>
                        <span class="contentUpdate" id="description"  style="margin: 2%;"><textarea class="form-control" id="training_conclusion" name="training_conclusion" rows="4" cols="50" maxlength="200"></textarea></span>
                    </div>
                </div>
            </div>
    
           
            <div class="row" style="margin-top: 1%;margin-left: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate"  style="margin: 2%;">APPENDIX 1 - Details of result : <div style="color: red; font-size:10px;">( * format .exl) </div></span>
                        <span class="contentUpdate" id="ResultCoverImg"></span> <div class="form-group">
                            <div class="border rounded-lg p-3">
                                <div class="rounded-lg pb-2" style="border: 2px dashed #DFE3E7;">
                                    <input style="visibility:hidden;" type="file" class="uploadImg" id="uploadResult-0"
                                    name="uploadResult[]-0" accept=".jpg, .jpeg, .png, .img" onchange="onChangeUploadFileResult()" multiple>
                                    <button onclick="uploadButtonResult()" type="file" class="btn btn-block" id="btnuploadfileParentDiv-1">
                                        <div class="poppins-medium-14" style="color: #475F7B;">Please click here to upload your file</div>
                                        <div class="poppins-medium" style="font-size: 10px; color: #69809A">(Please choose your file to be uploaded)</div>
                                    </button>
                                    <div class="form-group row justify-content-center" id="parentDivUploadedFileDisplay-1"style="display: none"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row" style="margin-top: 1%;margin-left: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate"  style="margin: 2%;">APPENDIX 2 - Details of report : <div style="color: red; font-size:10px;">( * format .pdf) </div></span>
                        <span class="contentUpdate" id="ReportCoverImg"></span> <div class="form-group">
                            <div class="border rounded-lg p-3">
                                <div class="rounded-lg pb-2" style="border: 2px dashed #DFE3E7;">
                                    <input style="visibility:hidden;" type="file" class="uploadImg" id="uploadReport-0"
                                    name="uploadReport[]-0" accept=".jpg, .jpeg, .png, .img" onchange="onChangeUploadFileReport()" multiple>
                                    <button onclick="uploadButtonReport()" type="file" class="btn btn-block" id="btnuploadfileParentDiv-2">
                                        <div class="poppins-medium-14" style="color: #475F7B;">Please click here to upload your file</div>
                                        <div class="poppins-medium" style="font-size: 10px; color: #69809A">(Please choose your file to be uploaded)</div>
                                    </button>
                                    <div class="form-group row justify-content-center" id="parentDivUploadedFileDisplay-2"style="display: none"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row" style="margin-top: 1%;margin-left: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate"  style="margin: 2%;">APPENDIX 3 - Details of receipt :  <div style="color: red; font-size:10px;">( * format .pdf) </div></span>
                        <span class="contentUpdate" id="ReceiptCoverImg"></span> <div class="form-group">
                            <div class="border rounded-lg p-3">
                                <div class="rounded-lg pb-2" style="border: 2px dashed #DFE3E7;">
                                    <input style="visibility:hidden;" type="file" class="uploadImg" id="uploadReceipt-0"
                                        name="uploadReceipt-0[]" accept=".jpg, .jpeg, .png, .img" onchange="onChangeUploadFileReceipt()"multiple>
                                        <button onclick="uploadButtonReceipt()" type="file" class="btn btn-block" id="btnuploadfileParentDiv-3">
                                            <div class="poppins-medium-14" style="color: #475F7B;">Please click here to upload your file</div>
                                            <div class="poppins-medium" style="font-size: 10px; color: #69809A">(Please choose your file to be uploaded)</div>
                                        </button>
                                    <div class="form-group row justify-content-center" id="parentDivUploadedFileDisplay-3" style="display: none"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    
            <div class="row" style="margin-top: 1%;margin-left: 1%;">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="contentUpdate"  style="margin: 2%;">APPENDIX 4 - Schedule of activities : <div style="color: red; font-size:10px;">( * format .pdf) </div></span>
                        <span class="contentUpdate" id="ActivityCoverImg"></span> <div class="form-group">
                            <div class="border rounded-lg p-3">
                                <div class="rounded-lg pb-2" style="border: 2px dashed #DFE3E7;">
                                    <input style="visibility:hidden;" type="file" class="uploadImg" id="uploadActivity-0"
                                        name="uploadActivity-0[]" accept=".jpg, .jpeg, .png, .img" onchange="onChangeUploadFileActivity()"multiple>
                                        <button onclick="uploadButtonActivity()" type="file" class="btn btn-block" id="btnuploadfileParentDiv-4">
                                            <div class="poppins-medium-14" style="color: #475F7B;">Please click here to upload your file</div>
                                            <div class="poppins-medium" style="font-size: 10px; color: #69809A">(Please choose your file to be uploaded)</div>
                                        </button>
                                    <div class="form-group row justify-content-center" id="parentDivUploadedFileDisplay-4" style="display: none"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
           
                <div class="d-flex justify-content-end pt-3 pb-2">
                    <div style="margin: 1%;">
                        <a href="" class="btn btn-secondary-6 btn-block">Cancel</a>
                    </div>
        
                    <div style="margin: 1%;">
                        <button  id="submitBtn" class="btn btn-secondary-6 btn-block">Submit To Seda</button>
                    </div>
                </div>

                {{-- <div class="d-flex justify-content-end pt-3 pb-2">
                    <div style="margin:10px;">
                        <a href="{{ route('user-profile') }}" class="btn btn-secondary-6 btn-block">Back</a>
                    </div>
                    <div style="margin:10px;">
                        <button  id="saveBtn" class="btn btn-secondary-6 btn-block">Save</button>
                    </div>
               </div> --}}



        </div>
            </div>
        </div>
    </section>

      <!-- --->
      <section>
        <div class="row">
          <div class="col-12">
            <div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog"
              aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">SUBMIT TRAINING REPORT TO SEDA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <i class="bx bx-x"></i>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p class="mb-0">
                      Are you sure you want to submit this training report to SEDA?
                    </p>
                  </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                        <span class="d-none d-sm-block">No</span>
                      </button>
        
                      <button type="button" class="btn btn-primary ml-1" data-dismiss="modal" id="submitKemaskiniBtn">
                        <i class="bx bx-check d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Yes</span>
                      </button>
                    </div>
  
  
                    
                </div>
              </div>
            </div>
          </div>
        </div> 
  
      </section>

    <section>
        <div class="modal" id="success-save-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            
            <div class="modal-content">
              <div class="modal-header" style="padding:1.3rem;">
                <h4 class="modal-title text-3" id="myModalLabel33">
                </h4>
            
              </div>
    
              <div class="modal-body" style="padding: 1.3rem 1.3rem 0 1.3rem;">
                <div class="modal-content-border">
                  <form id="formEndorseResult">
                    
                      <div class="row">
                          <div class="col-sm-12">
                            <p class="mb-0">
                              Successfully submit the training report to SEDA.
                            </p>
                          </div>
                    
                  </form>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                  <span class="d-none d-sm-block">close</span>
                </button>
               
               
              </div>
    
    
              
            </div>
          </div>
        </div>
       </section>
</main>
@endsection



@section('page-scripts')
<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";


    $(document).ready(function() {
    $('#submitBtn').on('click', function() {
        $("#confirmationModal").modal('show');
     
        });

    });

    
    $("#submitKemaskiniBtn").on("click", () => {
        $("#confirmationModal").modal("hide");
        updateInfo();
    });
</script>

<script src="{{ asset('js/trainingMgmt/trainingReportTP.js') }}"></script>
{{-- ${host}/training-partner/training-management/training-report/${full.batch_no}/${data} --}}
@endsection


@section('vendor-scripts')

@endsection