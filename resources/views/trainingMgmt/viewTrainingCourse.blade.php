@extends('layouts.contentLayoutMaster')

{{-- Page Title --}}
@section('title', 'View Training Course')

@section('vendor-styles')
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    /* Your custom page styles here */
    /* Align items to the left */
    .centered-container {
        display: flex;
        justify-content: flex-start;
        align-items: center;
        max-width: 100%;
    }

    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 55px;
        margin-bottom: 55px;
    }

    .title-margin {
        margin-top: 95px;
        padding-bottom: 15px;
        margin-bottom: 15px;
        margin-left: 100px;
        justify-content: flex-start;
        display: flex;
    }

    .container {
        max-width: 1200px; /* Adjust the width as needed */
        display: flex;
        flex-direction: column;
        align-items: start;
    }

    /* Style for labels */
    .form-group label {
        display: block;
        text-align: left; /* Align labels to the left */
        margin-bottom: 5px; /* Add vertical gap between label and field */
    }

    .form-checkin {
  display: block;
  min-height: 1.44rem;
  padding-left: 1.5em;
  margin-bottom: 0.15rem;
}

.form-check-labell {
  top: 0 !important;
  cursor: pointer;
  font-weight: normal;
  margin-bottom: 0.25rem;
  font-size: 0.85rem;
  color: #000000;
  width:100px;
}

.form-row {
        
        width: 1400px;
    }
</style>
@endsection

@section('content')
<main>
    <div class="header-top-margin">
    <h3 class="title-margin items-align-center" style="margin-top: 5%;"><b>View Training Course</b></h3>
        <div class="col-md-12" style="display: flex; justify-content: left; align-items: left;">
            <div class="col-md-6">
                    <div class="form-row">
                        <!-- Course Name -->
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="courseName"><b>Course Name</b></label>
                            <input type="text" class="form-control" id="courseName" name="course_name" placeholder="Course Name" value="{{ $courselist[0]->course_name }}" disabled>
                        </div>
                        <div class="form-group col-md-6 py-2">
                            <label for="courseCategory"><b>Course Category</b></label>
                            <select class="form-control" id="courseCategory" name="course_category" disabled>
                                <option> -- Please Select -- </option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" 
                                        @if(isset($courselist[0]) && $courselist[0]->id_course_category == $category->id) selected @endif>
                                        {{ $category->course_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @if($selectedReTypes)
                    <div class="form-row">
                    <label for="retype"><b>RE Type</b></label>
                        <div class="col-md-5">
                            @foreach($re_types as $re_type)
                            <div class="form-checkin" >
                                <input class="form-check-input" type="checkbox" value="{{ $re_type->id }}" id="categoryCheckbox{{ $re_type->id }}" name="re_types[]"
                                @if(in_array($re_type->id, $selectedReTypes)) checked @endif disabled>
                                <label class="form-check-labell" for="re_typeCheckbox{{ $re_type->id }}">
                                    {{ $re_type->re_type }}
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @else
                    @endif
                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label for="trainingOption"><b>Training Option</b></label>
                            <select class="form-control" id="trainingOption" name="training_option" disabled>
                                <option> -- Please Select -- </option>
                                @foreach($options as $option)
                                    <option value="{{ $option->id }}" 
                                        @if(isset($courselist[0]) && $courselist[0]->id_training_option == $option->id) selected @endif>
                                        {{ $option->option }}
                                    </option>
                                @endforeach  
                            </select>
                        </div>
                    </div>
                <div class="form-row">
                    <h4 class="page-title">Training Partner</h4>

                    <table id="tptable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Training Partner</th>
                                <th scope="col">Course Code</th>
                            </tr>
                        </thead>
                        <tbody style="text-align: left;width:100%;" id="tpTableBody">
                        @if ($trainingpartnerlist->isEmpty())
                                <tr>
                                    <td colspan="3">No data available</td>
                                </tr>
                            @else
                            @foreach ($trainingpartnerlist as $partner)
                            <tr class="tp-row">
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $partner->training_partner }}</td>
                                <td>{{ $partner->course_code }}</td>
                            </tr>
                            @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>

                    <div class="form-row">
                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="newFee"><b>New Fee - Local (RM)</b></label>
                            <input type="text" class="form-control" id="newFee" name="new_fee" value="{{ number_format($courselist[0]->new_fee, 2) }}" disabled>
                        </div>

                        <div class="form-group col-md-6 py-2">
                            <label class="field-title" for="newFeeInt"><b>New Fee - International (RM)</b></label>
                            <input type="text" class="form-control" id="newFeeInt"  name="new_fee_int" step=".01" value="{{ number_format($courselist[0]->new_fee_int,2) }}" disabled>
                        </div>
                    </div>
                    
                            @if($feeList)
                            <div class="form-row">
                                <h4 class="page-title">Resit Fee - Local</h4>

                                <table id="tptable" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Fee Type</th>
                                            <th scope="col">Amount (RM)</th>
                                        </tr>
                                    </thead>
                                    <tbody style="text-align: left;width:100%;" id="tpTableBody">
                                        @if ($feeList->isEmpty())
                                            <tr>
                                                <td colspan="3">No data available</td>
                                            </tr>
                                        @else
                                            @foreach($feeList as $fees)
                                                @if($fees->fee_category == 'Local')
                                                <tr class="tp-row">
                                                    <td>{{ $loop->index + 1 }}</td>
                                                    <td>{{ $fees->fee_name }}</td>
                                                    <td>{{ number_format($fees->amount, 2) }}</td>]
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            @endif

                            @if($feeList)
                        
                            <div class="form-row">
                                <h4 class="page-title">Resit Fee - Int</h4>

                                <table id="tptable" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Fee Type</th>
                                            <th scope="col">Amount (RM)</th>
                                        </tr>
                                    </thead>
                                    <tbody style="text-align: left;width:100%;" id="tpTableBody">
                                        @if ($feeList->isEmpty())
                                            <tr>
                                                <td colspan="3">No data available</td>
                                            </tr>
                                        @else
                                            @foreach($feeList as $fees)
                                            @if($fees->fee_category == 'Int')
                                            <tr class="tp-row">
                                                <td>{{ $loop->index + 1 }}</td>
                                                <td>{{ $fees->fee_name }}</td>
                                                <td>{{ number_format($fees->amount, 2) }}</td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            @endif
                            <div class="form-row">
                                <div class="form-group col-md-10 py-2">
                                    <label class="field-title" for="course_description"><b>Course Description (RM)</b></label>
                                    <textarea class="form-control" id="course_description" name="course_description" rows="6" disabled>{{ $courselist[0]->course_description }}</textarea>
                                </div>
                            </div>



                    {{--@foreach($trainingpartnerlist as $partner)
                        <div class="form-row partner-row">
                            <div class="form-group col-md-6 py-2">
                                <label for="courseCode"><b>Training Partner</b></label>
                                <input type="text" class="form-control" id="training_partner" name='training_partner' value="{{ $partner->training_partner }}" disabled>
                            </div>
                            <div class="form-group col-md-6 py-2">
                                <label for="courseCode"><b>Course Code</b></label>
                                <input type="text" class="form-control" id="course_code" name='course_code' value="{{ $partner->course_code }}" disabled>
                            </div>
                        </div>
                    @endforeach--}}
                </form>
            </div>
        </div>
    </div>
</main>
@endsection

@section('vendor-scripts')
<script>
//  our own js 
let host = "{{ env('API_SERVER') }}";
let userID = "{{ $userid }}";
  
// Add this code within your <script> block
$(document).ready(function () {
    $('#updateBtn').on('click', function () {
        // Prepare the data to send to the server
        const courseId = "{{ $courselist[0]->id_training_course }}"; // Get the course ID
        const courseName = $('#courseName').val();
        const courseCategory = $('#courseCategory').val();
        const trainingType = $('#trainingType').val();
        const courseDescription = $('#courseDescription').val();
        const certificateCode = $('#certificateCode').val();
        const isFree = $('#is_free').prop('checked') ? 1 : 0;
        const isPaid = $('#is_paid').prop('checked') ? 1 : 0;
        const fee = $('#Fee').val();
        const receipt = $('#resitFee').val();
        const reTypes = []; // Array to store selected re_types

        // Get selected re_types
        $('input[name="re_types[]"]:checked').each(function () {
            reTypes.push($(this).val());
        });

        // Prepare the data object to send in the request
        const data = {
            course_name: courseName,
            course_category: courseCategory,
            training_type: trainingType,
            course_description: courseDescription,
            re_types: reTypes,
        };

        // Send the AJAX request to update the course
        $.ajax({
            url: `/admin/updateCourseTraining/${courseId}`, // Replace with your route URL
            type: 'PUT', // Assuming you're using the PUT method for updates
            data: data,
            success: function (response) {
                // Handle the success response here
                console.log('Course updated successfully', response);
                // You can redirect to another page or show a success message here
            },
            error: function (xhr, status, error) {
                // Handle errors here
                console.error('Error updating course', xhr.responseText);
                // You can display an error message to the user
            },
        });
    });
});


    // Format the input value on input change
    document.getElementById('newFee').addEventListener('input', function(event) {
        // Get the input value
        let inputValue = event.target.value;

        // Format the value with a thousand separator
        let formattedValue = parseFloat(inputValue).toLocaleString('en-MY', { style: 'decimal', maximumFractionDigits: 2 });

        // Set the formatted value back to the input
        event.target.value = formattedValue;
    });

    document.getElementById('newFeeInt').addEventListener('input', function(event) {
        // Get the input value
        let inputValue = event.target.value;

        // Format the value with a thousand separator
        let formattedValue = parseFloat(inputValue).toLocaleString('en-MY', { style: 'decimal', maximumFractionDigits: 2 });

        // Set the formatted value back to the input
        event.target.value = formattedValue;
    });
</script>


@endsection