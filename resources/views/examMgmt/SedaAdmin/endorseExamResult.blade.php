@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Exam Management')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

{{-- <link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet"> --}}
<link rel="stylesheet" 
    href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" 
    crossorigin="anonymous">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }
    
    .font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.font-title-2{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 18px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.icon-editIcon:before {
  content: "\e916";
}

.title{
    color: #656565;
    font-family: Rajdhani;
    font-size: 26px;
    font-style: normal;
    font-weight: 600;
    line-height: 38px; /* 146.154% */
}

.contentUpdate{
    color: #454545;
    font-family: Open Sans;
    font-size: 18px;
    font-style: normal;
    font-weight: 600;
    line-height: 28px; /* 155.556% */
}

.modal-dialog {
    max-width: 800px; /* Adjust the width as per your requirements */
}

.modal-content-overflow {
  overflow: auto; !important;
}


.table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .paid-status {
        border-radius: 8px;
        border: 1px solid #18eeaa;
        background: var(--success-25, #f6faf7);
        padding: 4px 8px;
        color: #18eeaa;
    }

    .failed-status {
        border-radius: 8px;
        border: 1px solid #F04438;
        background: var(--success-25, #FFFBFA);
        padding: 4px 8px;
        color: #F04438;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

   
    .filter-dropdown{
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }

         /* Add styles for the table */
         .table {
    width: 100%;
    border-collapse: collapse;
    margin: 0;
    }

   

    .table th, .table td {
    padding: 10px;
    text-align: center;
    border: 1px solid #ddd;
    }

    /* Add hover effect on table rows */
    .table tbody tr:hover {
    background-color: #f5f5f5;
}
</style>
@endsection

@section('content')

{{-- <main style="padding:50px;margin:50px;">
  <section class="header-top-margin"> --}}
 
<main style="padding-top:20px;padding-bottom:50px;padding-left:50px;padding-right:50px;margin:50px;">
    <section class="header-top-margin">
      
    
    <div style="width: 100%; height: 100%; padding: 40px; background: white; border-radius: 16px; border: 1px #98A2B3 solid; flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 24px; display: inline-flex">
    
    <div style="align-self: stretch; height: 160px; flex-direction: column; justify-content: flex-start; align-items: center; gap: 16px; display: flex">
        <div style="align-self: stretch; justify-content: flex-start; align-items: flex-start; gap: 24px; display: inline-flex">
            <div style="width: 95px; color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">Provider :</div>
            <span class='title' id="provider_name" style="width: 500px; color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word"></span>
        </div>
        <div style="align-self: stretch; justify-content: flex-start; align-items: center; gap: 24px; display: inline-flex">
            <div style="width: 95px; color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">Start Date :</div>
            <span id="date_start" style="color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word"></span>
        </div>
        <div style="align-self: stretch; justify-content: flex-start; align-items: center; gap: 24px; display: inline-flex">
            <div style="width: 95px; color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">End Date :</div>
            <span id="date_end" style="color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word"></span>
        </div>
        <div style="align-self: stretch; justify-content: flex-start; align-items: center; gap: 24px; display: inline-flex">
            <div style="width: 95px; color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">Venue :</div>
            <span id="venue" style="color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word"></span>
        </div>
    </div>
</div>

    <div class="row">
        
      <div class="table-responsive">
        <div id="upcomingList_wrapper" class="dataTables_wrapper no-footer">
            <table class="table table-bordered table-striped dataTable no-footer" id="examResultUpdate" style="width: 100%;">
                <thead class="table-header"> <tr>
                 
                        @foreach ($HeaderResult as $header)
                       
                        {!! $header->header_row_no !!}
                        {!! $header->participant_name !!}
                        {!! $header->header_exam_sit !!}   
                        {!! $header->exam_1 !!}
                        {!! $header->exam_2 !!}
                        {!! $header->exam_3 !!}
                        {!! $header->total_theory_result !!}
                        {!! $header->theory_result !!}
                        {!! $header->practical_exam_1 !!}
                        {!! $header->practical_exam_2 !!}
                        {!! $header->practical_exam_3 !!}
                        {!! $header->total_practical_result !!}
                        {!! $header->practical_result !!}
                        {!! $header->final_result !!}
                        {!! $header->header_status !!}
                        {!! $header->header_action !!}
                        @endforeach
                       
                      {{-- <thead>
                        <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2">Name</th>
                            <th rowspan="2">New / Res-sit</th>
                            <th colspan="5">Theory Exam</th>
                            <th colspan="5">Practical Exam</th>
                            <th rowspan="2">Final Result (Pass/Fail)</th>
                            <th rowspan="2">Status</th>
                            <th rowspan="2">Action</th>   
                        </tr>
                        <tr>
                            <th rowspan="2">Part A</th>
                            <th rowspan="2">Part B</th>
                            <th rowspan="2">Part C</th>
                            <th rowspan="2">Total</th>
                            <th rowspan="2">Pass / Fail</th>
                            <th rowspan="2">Part A</th>
                            <th rowspan="2">Part B</th>
                            <th rowspan="2">Part C</th>
                           <th rowspan="2">Total</th>
                            <th rowspan="2">Pass / Fail</th> 
                        </tr>
                      </thead> --}}

                </tr></thead> 
                <tbody class="tablecontent">
               
              </tbody>
   
            </table>
        </div>
      </div>

      <div class="d-flex justify-content-end">
          <div style="margin:10px;">
            <a href="{{ route('exam-list-admin') }}" class="btn btn-secondary-6 btn-block">Back</a>
          </div>
          <div style="margin:10px;">
            <button  id="QueryBtn" class="btn btn-secondary-6 btn-block">
              Query
            </button>
         </div>
        {{-- <div style="margin:10px;">
          <button  id="EndorseBtn" class="btn btn-secondary-6 btn-block">
              Endorse
          </button>
        </div> --}}
         
      </div>


    


    <!-- ENDORSE -->
   <div class="modal" id="confirmationEndorseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        
        <div class="modal-content">
          <div class="modal-header" style="padding:1.3rem;" id="kemaskini-jenis-cenderamata">
            <h4 class="modal-title text-3" id="myModalLabel33">ENDORSE EXAM RESULT
            </h4>
            <button type="button" class="close model-remove-icon-bg" data-dismiss="modal" aria-label="Close">
              <i class="bx bx-x modal-close-icon-1"></i>
            </button>
          </div>

          <div class="modal-body" style="padding: 1.3rem 1.3rem 0 1.3rem;">
            <div class="modal-content-border">
              <form id="formEndorseResult">
                
                  <div class="row">
                      <div class="col-sm-12">
                        <p class="mb-0">
                          Are you sure you want to endorse these/this exam result?

                        </p>
                      </div>
                 
                      <input type="hidden" class="form-control mt-1" name="batch_no" id="batch_no">
              </form>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
              <span class="d-none d-sm-block">No</span>
            </button>
           
            <button type="button" class="btn btn-primary ml-1" id="submitEndorse">
              <i class="bx bx-check d-block d-sm-none"></i>
              <span id="spanSimpan" class="d-none d-sm-block">Yes</span>
            </button>
          </div>


          
        </div>
      </div>
    </div> 
    

   
    <!--2-->
  
    <!-- modal -->
        
       



    

      <!--modal confirmation -->
      



      <!-- --->
      
      
{{-- endorse --}}




    



    </section> 
    <section>
    <div class="modal" id="confirmationQueryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        
        <div class="modal-content">
          <div class="modal-header" style="padding:1.3rem;">
            <h4 class="modal-title text-3" id="myModalLabel33">QUERY EXAM RESULT
            </h4>
            <button type="button" class="close model-remove-icon-bg" data-dismiss="modal" aria-label="Close">
              <i class="bx bx-x modal-close-icon-1"></i>
            </button>
          </div>

          <div class="modal-body" style="padding: 1.3rem 1.3rem 0 1.3rem;">
            <div class="modal-content-border">
              <form id="formQueryResult">
                
                  {{-- <div class="row">
                      <div class="col-sm-12">
                        <p class="mb-0">
                          <label for="myDropdown">Please select a Training Partner to send query:</label>
                            <br>
                            <select id="trainingPartnerDropdown" name="trainingPartnerDropdown">
                                <option value="">Please Select</option>
                            </select>
                            <br>
                        </p>
                      </div>
                  </div> --}}
                      
                  <div class="row">
                    <div class="col-sm-12">
                      <p class="mb-0">
                      <label for="myquery" style="text-align: left;margin: left 50px;">Enter your query:</label>
                        <br>
                        <textarea id="myquery" rows="4" cols="80">
                        </textarea>
                        <input type="hidden" class="form-control mt-1" name="batch_no_query" id="batch_no_query">
                        <input type="hidden" class="form-control mt-1" name="id_training_course" id="id_training_course">
                        <br>
                      </p>
                    </div>
                </div>
                     

              </form>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
              <span class="d-none d-sm-block">Cancel</span>
            </button>
           
            <button type="button" class="btn btn-primary ml-1" id="submitQuery">
              <i class="bx bx-check d-block d-sm-none"></i>
              <span id="spanSimpan" class="d-none d-sm-block">Send Query</span>
            </button>
          </div>


          
        </div>
      </div>
    </div>
   </section>

   <section>
    <div class="modal" id="error-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        
        <div class="modal-content">
          <div class="modal-header" style="padding:1.3rem;">
            <h4 class="modal-title text-3" id="myModalLabel33">Error!
            </h4>
            <button type="button" class="close model-remove-icon-bg" data-dismiss="modal" aria-label="Close">
              <i class="bx bx-x modal-close-icon-1"></i>
            </button>
          </div>

          <div class="modal-body" style="padding: 1.3rem 1.3rem 0 1.3rem;">
            <div class="modal-content-border">
              <form id="formQueryResult">
              
                      
                  <div class="row">
                    <div class="col-sm-12">
                      <p class="mb-0">
                        <label for="error-message">Enter your query:</label>
                        <br>
                        <span id="error-message">
                          
                        </span>
                        <br>

                      </p>
                    </div>
                  </div>
                     

              </form>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
              <span class="d-none d-sm-block">close</span>
            </button>
           
           
          </div>


          
        </div>
      </div>
    </div>
   </section>

    {{-- </section> --}}
</main>
  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    {{-- <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script>  --}}
    
@endsection



@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";


    $(document).ready(function() {
  $('#openModal1Button').click(function() {
    $('#modal1').modal('show');
  });

  $('#openModal2Button').click(function() {
    $('#modal2').modal('show');
  });
});

window.addEventListener("message", function(event) {
  // Handle the message and prepare a response
  var response = "This is a response.";

  // Send the response
  event.source.postMessage(response, event.origin);
}, false);
</script>




<script src="{{ asset('js/examMgmt/endorseExamResult.js') }}"></script>


@endsection
