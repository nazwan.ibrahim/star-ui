@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Set Exam Table')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

{{-- <link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet"> --}}
<link rel="stylesheet" 
    href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" 
    crossorigin="anonymous">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }
    
    .font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.font-title-2{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 18px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.icon-editIcon:before {
  content: "\e916";
}

.title{
    color: #656565;
    font-family: Rajdhani;
    font-size: 26px;
    font-style: normal;
    font-weight: 600;
    line-height: 38px; /* 146.154% */
}

.contentUpdate{
    color: #454545;
    font-family: Open Sans;
    font-size: 18px;
    font-style: normal;
    font-weight: 600;
    line-height: 28px; /* 155.556% */
}

.modal-dialog {
    max-width: 800px; /* Adjust the width as per your requirements */
}

.header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .table {
        width: 100%;
        border-collapse: collapse;
        margin: 20px 0;
        background-color: #f5f5f5;
    }

    /* .table th {
        background-color: #333;
        color: #fff;
        padding: 10px;
    } */

    .table td, .table th {
        border: 1px solid #ccc;
        padding: 8px;
        text-align: left;
    }

    .inner-table {
        width: 100%;
    }

    .inner-table td {
        padding: 6px;
        text-align: left;
    }

    .inner-table strong {
        color: #333;
    }
</style>

    
</style>
@endsection

    @section('content')
    <main style="padding: 30px; margin: 30px;">
        <section class="header-top-margin">
            <div class="text-center mb-3">
                <h2 class="page-title py-2" style="text-align: left !important;">Exam Configuration</h2>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @else
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                @endif
            @endif
            <form method="POST" action="{{ route('admin.create-exam-config') }}">
    @csrf
    <div class="form-group text-left">
        <label for="configuration_name">Configuration Name</label>
        <input name="configuration_name" class="form-control smaller-input" id="configuration_name" required>
    </div>
    <div class="form-group text-left">
        <label for="training">Training</label>
        <select name="training" id="training" class="form-control smaller-input">
            <option value="">-- Please Choose --</option>
            @foreach ($training as $training)
                <option value="{{ $training->id }}">{{ $training->training_name }}</option>
            @endforeach
        </select>
    </div>


    <div class="form-group text-left">
        <label for="configuration_data">Configuration</label>
        <textarea name="configuration_data" class="form-control smaller-textarea" id="configuration_data" required
            placeholder='{"key1": "value1", "key2": "value2"}'></textarea>
    </div>


    <div class="form-group text-left">
        <button id="generateJsonButton" type="button" class="btn btn-primary">Generate JSON Data</button>
    </div>

    <div class="form-group text-left">
        <label for="parts">Parts:</label>
        <input type="text" name="parts[]" class="smaller-input" required>
        <input type="text" name="parts[]" class="smaller-input" required>
    </div>
    <div class="form-group text-left">
        <button type="submit" class="btn btn-primary">Save Exam Configuration</button>
    </div>

    
</form>


        <hr>

        <table class="table">
            <thead>
                <tr>
                    <th>Configuration Name</th>
                    <th>Configuration Data</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($examConfigurations as $examConfiguration)
                    <tr>
                        <td>{{ $examConfiguration->name }}</td>
                        <td>
                            <table class="inner-table">
                                @php
                                $configurationData = json_decode($examConfiguration->configuration, true);
                                $categories = ['Theory', 'Practical'];

                                foreach ($categories as $category) {
                                    $categoryData = [];

                                    foreach ($configurationData as $key => $value) {
                                        if (strpos($key, 'key') !== false && $value === $category) {
                                            $categoryData[] = str_replace('key', 'Part ', $key);
                                        }
                                    }

                                    if (!empty($categoryData)) {
                                        echo "<tr><td><strong>$category Configuration</strong></td></tr>";
                                        foreach ($categoryData as $partKey) {
                                            echo "<tr><td>$partKey</td></tr>";
                                        }
                                    }
                                }
                                @endphp
                            </table>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    <table class="table">
        <thead>
            <tr>
                <th>Category</th>
                <th>Key</th>
                <th>Value</th>
            </tr>
        </thead>
        <tbody>
            @php
            foreach ($examConfigurations as $examConfiguration) {
                $configurationData = json_decode($examConfiguration->configuration, true);
                
                foreach ($configurationData as $key => $value) {
                    if (strpos($key, 'key') !== false) {
                        echo "<tr>";
                        echo "<td>$value</td>"; // Category
                        echo "<td>$key</td>"; // Key
                        echo "<td>$value</td>"; // Value
                        echo "</tr>";
                    }
                }
            }
            @endphp
        </tbody>
    </table>


</div>



    </section>
</main>

  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    {{-- <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script>  --}}
    
@endsection



@section('page-scripts')

<script>
    document.getElementById('generateJsonButton').addEventListener('click', function() {
        const jsonData = {
            key1: 'Theory',
            key2: 'Part A',
            key3: 'Practical',
            key4: 'Part A',
        };

        // Convert JSON object to a string and set it as the textarea value
        document.getElementById('configuration_data').value = JSON.stringify(jsonData, null, 2);
    });
</script>




@endsection
