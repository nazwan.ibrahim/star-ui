@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Set Exam Table')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

{{-- <link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet"> --}}
<link rel="stylesheet" 
    href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" 
    crossorigin="anonymous">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }
    
    .font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.font-title-2{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 18px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.icon-editIcon:before {
  content: "\e916";
}

.title{
    color: #656565;
    font-family: Rajdhani;
    font-size: 26px;
    font-style: normal;
    font-weight: 600;
    line-height: 38px; /* 146.154% */
}

.contentUpdate{
    color: #454545;
    font-family: Open Sans;
    font-size: 18px;
    font-style: normal;
    font-weight: 600;
    line-height: 28px; /* 155.556% */
}

.modal-dialog {
    max-width: 800px; /* Adjust the width as per your requirements */
}

.header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .table {
        width: 100%;
        border-collapse: collapse;
        margin: 20px 0;
        background-color: #f5f5f5;
    }

    /* .table th {
        background-color: #333;
        color: #fff;
        padding: 10px;
    } */

    .table td, .table th {
        border: 1px solid #ccc;
        padding: 8px;
        text-align: left;
    }

    .inner-table {
        width: 100%;
    }

    .inner-table td {
        padding: 6px;
        text-align: left;
    }

    .inner-table strong {
        color: #333;
    }
    .clearfix::after {
    content: "";
    display: table;
    clear: both;
}

</style>

    

@endsection



    @section('content')
   
    <main style="padding: 30px; margin: 30px;">

       
          

          
      
            

        <section class="header-top-margin">
           
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @else
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                @endif
            @endif
            <div class="card mt-7" style="padding:50px;margin:50px;background-color:#fbf9f9">
                <div class="text-center mb-3">
                    <h2 class="page-title py-2" style="text-align: left !important;">Exam Configuration</h2>
                </div>

               <div style="float: right"> <div class="col-lg-3 container" style="float:right; margin-top:20px;text-align:right important;">
                    <div class="breadcrumb poppins-semibold-12"> 
                              <li class="">  
                                 <a class="nav-link"  href="{{ route('admin-PreviewTableResult') }}">Preview header table exam</a> </a>
                               </li>      
                              
                    </div>
                </div></div>
            <form method="POST" action="{{ route('admin.create-exam-config') }}">
    @csrf
    <div class="form-group text-left">
        <label for="configuration_name">Configuration Name</label>
        <input name="configuration_name" class="form-control smaller-input" id="configuration_name" required>
    </div>
    <div class="form-group text-left">
        <label for="training">Training</label>
        <select name="training" id="training" class="form-control smaller-input" onchange="handleDropdownChange(this)">>
            <option value="">-- Please Choose --</option>
            @foreach ($training as $training)
                <option value="{{ $training->id_training_course }}">{{ $training->course_name }} </option>
            @endforeach
        </select>
    </div>
   <div id="warningMessage" style="font-size: 18px; color:red"></div>
    <div class="form-group text-left">
        <label for="training">Exam Table Header  : </label>
    </div>
    
    
   

    <div class="form-group text-left">
        <table>
            <tr>
                <td><div style="">
                        <span><input type="checkbox" value="<th>Theory Result</th>" id="Theory_Result" name="Theory_Result"></span><span style="margin-left:10px;" >Theory Result</span>
                    </div>
                </td><td style="width: 5em;">&nbsp;</td>
                <td><div style="">
                    <span><input type="checkbox" value="<th>Practical Result</th>" id="Practical_Result" name="Practical_Result"></span><span style="margin-left:10px;" >Practical Result</span>
                    </div>
                </td>
                
            </tr>

            <tr>
                <td> <div style="margin:10px 10px 10px 30px;">
                        <span><input type="checkbox" value="<th>Part A</th>" id="Theory_Part_A" name="Theory_Part_A"></span>
                        <span style="margin-left:10px;">
                            <input type="text" value="" id="theory_TextPart_A" name="theory_TextPart_A" placeholder="Theory Part A">
                        </span>
                    </div>
                </td>
                <td style="width: 5em;">&nbsp;</td>
                <td><div style="margin:10px 10px 10px 30px;">
                    <span><input type="checkbox" value="<th>Part A</th>" id="Practical_Part_A" name="Practical_Part_A"></span>
                    <span style="margin-left:10px;">
                        <input type="text" value="" id="practical_TextPart_A" name="practical_TextPart_A" placeholder="Practical Part A">
                    </span>
                    </div>
                </td>
                
            </tr>
            
            <tr>
                <td><div style="margin:10px 10px 10px 30px;">
                    <span><input type="checkbox" value="<th>Part B</th>" id="Theory_Part_B" name="Theory_Part_B"></span><span style="margin-left:10px;"><input type="text" value="" id="theory_TextPart_B" name="theory_TextPart_B" placeholder="Theory Part B"></span>
                    </div>
                </td>
                <td style="width: 5em;">&nbsp;</td>
                <td><div style="margin:10px 10px 10px 30px;">
                        <span><input type="checkbox" value="<th>Part B</th>" id="Practical_Part_B" name="Practical_Part_B"></span><span style="margin-left:10px;"><input type="text" value="" id="practical_TextPart_B" name="practical_TextPart_B" placeholder="Practical Part B"></span>
                    </div>
                </td>
                
            </tr>

            <tr>
                <td><div style="margin:10px 10px 10px 30px;">
                        <span><input type="checkbox" value="<th>Part C</th>" id="Theory_Part_C" name="Theory_Part_C"></span><span style="margin-left:10px;"><input type="text" value="" id="theory_TextPart_C" name="theory_TextPart_C" placeholder="Theory Part C"></span>
                    </div>
                </td>
                <td style="width: 5em;">&nbsp;</td>
                <td><div style="margin:10px 10px 10px 30px;">
                        <span><input type="checkbox" value="<th>Part C</th>" id="Practical_Part_C" name="Practical_Part_C"></span><span style="margin-left:10px;"><input type="text" value="" id="practical_TextPart_C" name="practical_TextPart_C" placeholder="Practical Part C"></span>
                    </div>
                </td>
                
            </tr>
            <tr>
                <td><div style="margin:10px 10px 10px 30px;">
                        <span><input type="checkbox" value="<th>Part D</th>" id="Theory_Part_D" name="Theory_Part_D"></span><span style="margin-left:10px;"><input type="text" value="" id="theory_TextPart_D" name="theory_TextPart_D" placeholder="Theory Part D"></span>
                    </div>
                </td>
                <td style="width: 5em;">&nbsp;</td>
                <td><div style="margin:10px 10px 10px 30px;">
                        <span><input type="checkbox" value="<th>Part D</th>" id="Practical_Part_D" name="Practical_Part_D"></span><span style="margin-left:10px;"><input type="text" value="" id="practical_TextPart_D" name="practical_TextPart_D" placeholder="Practical Part D"></span>
                    </div>
                </td>
                
            </tr>
            <tr>
                <td><div style="margin:10px 10px 10px 30px;">
                        <span><input type="checkbox" value="<th>Part E</th>" id="Theory_Part_E" name="Theory_Part_E"></span><span style="margin-left:10px;"><input type="text" value="" id="theory_TextPart_E" name="theory_TextPart_E" placeholder="Theory Part E"></span>
                    </div>
                </td>
                <td style="width: 5em;">&nbsp;</td>
                <td><div style="margin:10px 10px 10px 30px;">
                        <span><input type="checkbox" value="<th>Part E</th>" id="Practical_Part_E" name="Practical_Part_E"></span><span style="margin-left:10px;"><input type="text" value="" id="practical_TextPart_E" name="practical_TextPart_E" placeholder="Practical Part E"></span>
                    </div>
                </td>
                
            </tr>
            
        </table>

    </div>

 


    <div class="form-group text-left">
        <table id="GCPV">
        <tr>
            <td><div style="">

                    <span><input type="checkbox" value="<th>GCPV System Design</th>" id="gCPV_System_Design" name="gCPV_System_Design" onclick="handleCheckboxChange(this)"></span><span style="margin-left:10px;">GCPV System Design</span>
                  
             
                    <input type="hidden" value="{{ $trainingCGPV->id_training_course }}" id="gCPV_System_Design2" name="gCPV_System_Design2">
        
           
                </div>
            </td><td style="">&nbsp;</td><td style="">&nbsp;</td><td style="">&nbsp;</td>
              
        </tr>
            <tr>
                <td><div style="margin:10px 0 0 30px;">
                        <span><input type="checkbox" value="<th>Fundamental Result</th>" id="fundamental_Result" name="fundamental_Result"><span style="margin-left:10px;">Fundamental Result</span></span>
                    </div>
                </td>
                <td><div style="margin:10px 0 0 30px;">
                        <span><input type="checkbox" value="<th>Design Result</th>" id="design_Result" name="design_Result"><span style="margin-left:10px;">Design Result</span></span>
                    </div>
                </td>
                <td><div style="margin:10px 0 0 30px;">
                    <span><input type="checkbox" value="<th>Practical Result</th>" id="cgpvpractical_Result" name="cgpvpractical_Result"><span style="margin-left:10px;">Practical Result</span></span>
                </div>
            </td>
                
            </tr>

            <tr>
                <td style="width: 20em;"> <div style="margin:10px 10px 10px 60px;">
                        <span><input type="checkbox" value="<th>Fundamental Part A</th>" id="Fundamental_Part_A" name="Fundamental_Part_A"></span>
                        <span style="margin-left:10px;">
                            <input type="text" value="" id="fundamental_TextPart_A" name="fundamental_TextPart_A" placeholder="Fundamental Part A">
                        </span>
                    </div>
                </td>
                <td style="width: 20em;"><div style="margin:10px 10px 10px 60px;">
                    <span><input type="checkbox" value="<th>Design Part A</th>" id="Design_Part_A" name="Design_Part_A"></span>
                    <span style="margin-left:10px;">
                        <input type="text" value="" id="design_TextPart_A" name="design_TextPart_A" placeholder="Design Part A">
                    </span>
                </div></td>
                <td style="width: 20em;"><div style="margin:10px 10px 10px 60px;">
                    <span><input type="checkbox" value="<th>PracticalGCPV Part A</th>" id="PracticalGCPV_Part_A" name="PracticalGCPV_Part_A"></span>
                    <span style="margin-left:10px;">
                        <input type="text" value="" id="practicalGCPV_TextPart_A" name="practicalGCPV_TextPart_A" placeholder="Practical Part A">
                    </span>
                    </div>
                </td>
                
            </tr>
            
            <tr>
                <td><div style="margin:10px 10px 10px 60px;">
                    <span><input type="checkbox" value="<th>Fundamental Part B</th>" id="Fundamental_Part_B" name="Fundamental_Part_B"></span>
                    <span style="margin-left:10px;">
                        <input type="text" value="" id="fundamental_TextPart_B" name="fundamental_TextPart_B" placeholder="Fundamental Part B">
                    </span>
                    </div>
                </td>
                <td style="width: 5em;"><div style="margin:10px 10px 10px 60px;">
                    <span><input type="checkbox" value="<th>Design Part B</th>" id="Design_Part_B" name="Design_Part_B"></span>
                    <span style="margin-left:10px;">
                        <input type="text" value="" id="design_TextPart_B" name="design_TextPart_B" placeholder="Design Part B">
                    </span>
                    </div>
                </td>
                <td><div style="margin:10px 10px 10px 60px;">
                    <span><input type="checkbox" value="<th>Practical Part B</th>" id="PracticalGCPV_Part_B" name="PracticalGCPV_Part_B"></span>
                    <span style="margin-left:10px;">
                        <input type="text" value="" id="practicalGCPV_TextPart_B" name="practicalGCPV_TextPart_B" placeholder="Practical Part B"></span>
                    </div>
                </td>
                
            </tr>

            <tr>
                <td><div style="margin:10px 10px 10px 60px;">
                    <span><input type="checkbox" value="<th>Fundamental Part C</th>" id="Fundamental_Part_C" name="Fundamental_Part_C"></span>
                    <span style="margin-left:10px;">
                        <input type="text" value="" id="fundamental_TextPart_C" name="fundamental_TextPart_C" placeholder="Fundamental Part C"></span>
                    </div>
                </td>
                <td><div style="margin:10px 10px 10px 60px;">
                    <span><input type="checkbox" value="<th>Design Part C</th>" id="Design_Part_C" name="Design_Part_C"></span>
                    <span style="margin-left:10px;">
                        <input type="text" value="" id="design_TextPart_C" name="design_TextPart_C" placeholder="Design Part C">
                    </span>
                </div></td>
                <td><div style="margin:10px 10px 10px 60px;">
                        <span><input type="checkbox" value="<th>Practical Part C</th>" id="PracticalGCPV_Part_C" name="PracticalGCPV_Part_C"></span>
                        <span style="margin-left:10px;">
                            <input type="text" value="" id="practicalGCPV_TextPart_C" name="practicalGCPV_TextPart_C" placeholder="Practical Part C"></span>
                    </div>
                </td>
                
            </tr>
            
        </table>

    </div>
 


    {{-- <div style="width: 26px; height: 26px; position: relative">Theory Result
        <input type="checkbox" value="<th>Theory Result</th>" id="Theory_Result" name="Theory_Result">
        </div> --}}
   
    <div class="form-group text-left" style="margin-top:30px;">
        <button type="submit" class="btn btn-primary">Save Exam Configuration</button>
    </div>

    
</form></div>
        </section>


Default Table Header
        <div class="table-responsive">
            <div id="upcomingList_wrapper" class="dataTables_wrapper no-footer">
                <table class="table table-bordered table-striped dataTable no-footer" id="trainingL" style="width: 100%;">
                    <tr><thead class="table-header"> 
                           
                        <th>No</th>
                        <th>Name</th>
                        <th>New/Re-sit</th>
                        <th>Final Result<br>(Pass / Fail)</th>
                        <th>Status</th>
                        <th>Action</th>
                    
                           
                    </tr></thead>
                    <tbody class="tablecontent"><tr>
                        <td>  No Data Yet</td>
                    </tr></tbody>
       
                </table>
            </div>
        </div>
      

</main>

  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    {{-- <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script>  --}}
    
@endsection



@section('page-scripts')

<script>
    function handleCheckboxChange(checkbox) {
        // Disable the dropdown if the checkbox is checked
        document.getElementById('training').disabled = checkbox.checked;

        // Uncheck the checkbox if the dropdown is selected
        if (!checkbox.checked) {
            checkbox.checked = true; 
        }
    }

    function handleDropdownChange(dropdown) {
        // Disable the checkbox if an option is selected in the dropdown
        document.getElementById('gCPV_System_Design').disabled = dropdown.value !== '';

        // Reset the checkbox state if an option is selected in the dropdown
        document.getElementById('gCPV_System_Design').checked = false;

        var selectedValue = dropdown.value;
    
        if (selectedValue) {
        // Assuming you have a function to check if the value exists
        var valueExists = checkDropdownValue(selectedValue);

        if (valueExists) {
            // Display a warning message
            alert('This value already exists in the database.');
            // You can also update a specific element with the warning message
            // Example: document.getElementById('warningMessage').innerText = 'This value already exists in the database.';
        } else {
            // Clear the warning message
            // Example: document.getElementById('warningMessage').innerText = '';
        }
    }
    }


    function checkDropdownValue(selValue) {
        //var selectedValue = $('#training').val();
           //alert(selValue);

        // Only proceed if a value is selected
        if (selValue) {
            // Make an AJAX request to check if the value exists
            $.ajax({
                url: '/admin/exam/getResult/' + selValue, // Replace with your actual route
                method: 'GET',
                success: function (response) {
                 //  console.log(response.previewHeaderResult);
                 if (response.previewHeaderResult && response.previewHeaderResult.length > 0) {
                      console.log(response.previewHeaderResult);
                        // Display a warning message
                      // alert("red alertbhere");
                        $('#warningMessage').text('This table header already exists in the database!!! Please delete or choose new course.');
                    } else {
                        // Clear the warning message
                        // alert("red alerts");
                        $('#warningMessage').text('');
                    }
                },
                error: function (error) {
                    console.error('Error checking dropdown value:', error);
                }
            });
        }
    }
</script>

@endsection
