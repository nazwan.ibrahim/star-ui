@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Exam Management')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

{{-- <link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet"> --}}
<link rel="stylesheet" 
    href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" 
    crossorigin="anonymous">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }
    
    .font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.font-title-2{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 18px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.icon-editIcon:before {
  content: "\e916";
}

.title{
    color: #656565;
    font-family: Rajdhani;
    font-size: 26px;
    font-style: normal;
    font-weight: 600;
    line-height: 38px; /* 146.154% */
}

.contentUpdate{
    color: #454545;
    font-family: Open Sans;
    font-size: 18px;
    font-style: normal;
    font-weight: 600;
    line-height: 28px; /* 155.556% */
}

.modal-dialog {
    max-width: 800px; /* Adjust the width as per your requirements */
}



</style>
@endsection

@section('content')
<main style="padding-top:20px;padding-bottom:50px;padding-left:50px;padding-right:50px;margin:50px;">
    <section class="header-top-margin">
  
    <div style="width: 100%; height: 100%; padding: 40px; background: white; border-radius: 16px; border: 1px #98A2B3 solid; flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 24px; display: inline-flex">
    
    <div style="align-self: stretch; height: 170px; flex-direction: column; justify-content: flex-start; align-items: center; gap: 16px; display: flex">
        <div style="align-self: stretch; justify-content: flex-start; align-items: flex-start; gap: 24px; display: inline-flex">
            <div style="width: 95px; color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">Provider :</div>
            <span id="provider_name" style="color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word"></span>
        </div>
        <div style="align-self: stretch; justify-content: flex-start; align-items: center; gap: 24px; display: inline-flex">
            <div style="width: 95px; color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">Start Date :</div>
            <span id="date_start" style="color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word"></span>
        </div>
        <div style="align-self: stretch; justify-content: flex-start; align-items: center; gap: 24px; display: inline-flex">
            <div style="width: 95px; color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">End Date :</div>
            <span id="date_end" style="color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word"></span>
        </div>
        <div style="align-self: stretch; justify-content: flex-start; align-items: center; gap: 24px; display: inline-flex">
            <div style="width: 95px; color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">Venue :</div>
            <span id="venue" style="color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word"></span>
        </div>
        <div style="align-self: stretch; justify-content: flex-start; align-items: center; gap: 24px; display: inline-flex">
          <div style="width: 95px; color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">Batch:</div>
          <span id="batch_no" style="color: #454545; font-size: 18px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word"></span>
      </div>
        
    </div>
</div>

    <div class="row">
        
      


        <div class="table-responsive">
          <div id="upcomingList_wrapper" class="dataTables_wrapper no-footer">
              <table class="table table-bordered table-striped dataTable no-footer" id="examResultUpdate" style="width: 100%;">
                  <thead class="table-header"> <tr>
                   
                          @foreach ($HeaderResult as $header)
                         
                          {!! $header->header_row_no !!}
                          {!! $header->participant_name !!}
                          {!! $header->header_exam_sit !!}   
                          {!! $header->exam_1 !!}
                          {!! $header->exam_2 !!}
                          {!! $header->exam_3 !!}
                          {!! $header->total_theory_result !!}
                          {!! $header->theory_result !!}
                          {!! $header->practical_exam_1 !!}
                          {!! $header->practical_exam_2 !!}
                          {!! $header->practical_exam_3 !!}
                          {!! $header->total_practical_result !!}
                          {!! $header->practical_result !!}
                          {!! $header->final_result !!}
                          {!! $header->header_status !!}
                          {!! $header->header_action !!}
                          @endforeach
                         
                        {{-- <thead>
                          <tr>
                              <th rowspan="2">No</th>
                              <th rowspan="2">Name</th>
                              <th rowspan="2">New / Res-sit</th>
                              <th colspan="5">Theory Exam</th>
                              <th colspan="5">Practical Exam</th>
                              <th rowspan="2">Final Result (Pass/Fail)</th>
                              <th rowspan="2">Status</th>
                              <th rowspan="2">Action</th>   
                          </tr>
                          <tr>
                              <th rowspan="2">Part A</th>
                              <th rowspan="2">Part B</th>
                              <th rowspan="2">Part C</th>
                              <th rowspan="2">Total</th>
                              <th rowspan="2">Pass / Fail</th>
                              <th rowspan="2">Part A</th>
                              <th rowspan="2">Part B</th>
                              <th rowspan="2">Part C</th>
                             <th rowspan="2">Total</th>
                              <th rowspan="2">Pass / Fail</th> 
                          </tr>
                        </thead> --}}

                  </tr></thead> 
                  <tbody class="tablecontent">
                 
                </tbody>
     
              </table>
          </div>
      </div>
    </div>
    <div class="d-flex justify-content-end">
      <div style="margin:10px;">
        <a href="{{ route('exam-list') }}" class="btn btn-secondary-6 btn-block">Back</a>
      </div>
      {{-- <div style="margin:10px;">
        <button  id="QueryBtn" class="btn btn-secondary-6 btn-block">
          Query
        </button>
      </div> --}}
    <div style="margin:10px;">
      <button  id="SubmitBtn" class="btn btn-secondary-6 btn-block">
          Save
      </button>
    </div>
     
    </div>

    <!-- modal -->
        
        


      </section>

      <section>
        <div class="modal fade text-left" id="edit-result-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            
            <div class="modal-content">
              <div class="modal-header" style="padding:1.3rem;" id="kemaskini-jenis-cenderamata">
                <h4 class="modal-title text-3" id="myModalLabel33">UPDATE EXAM RESULT
                </h4>
                <button type="button" class="close model-remove-icon-bg" data-dismiss="modal" aria-label="Close">
                  <i class="bx bx-x modal-close-icon-1"></i>
                </button>
              </div>

              <div class="modal-body" style="padding: 1.3rem 1.3rem 0 1.3rem;">
                <div class="modal-content-border">
                  <form id="formEditResult">
                   

                    <span class="font-title">Theory Exam</span>&nbsp;&nbsp; <label for="name"><span style="color:#ff0000">Update the related field only<span style="color:#ff0000">*</span></label>
                    <p> </p><hr>
                      <div class="row">
                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label for="part-AT">Part A</label>
                                  <input type="text" class="form-control" id="part-AT" name="part-AT"
                                      value="" required>
                                      {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                          Please fill in this field.
                                      </div> --}}
                              </div>
                          </div>
                 
                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label for="part-BT">Part B</label>
                                  <input type="text" class="form-control" id="part-BT" name="part-BT"
                                      value="" required>
                                      {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                          Please fill in this field.
                                      </div> --}}
                              </div>
                          </div>

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label for="part-CT">Part C</label>
                                  <input type="text" class="form-control" id="part-CT" name="part-CT"
                                      value="" required>
                                      {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                          Please fill in this field.
                                      </div> --}}
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="totalResultTheory">Total Theory Exam</label>
                          <input type="text" class="form-control mt-1" name="totalResultTheory" id="totalResultTheory"
                            placeholder="Please fill in this field." autocomplete="off">
      
                          {{-- <div class="error-box-class" id="error-jenisCenderamata">
                              Please fill in this field.
                          </div> --}}
                      </div>


                      <div class="form-group">
                          <label for="statusResultTheory"></label>
                          <select class="form-control mt-1" name="statusResultTheory" id="statusResultTheory">
                              <option>Please Select </option>
                              <option value="Pass">Pass</option>
                              <option value="Fail">Fail</option>
                             
                          </select>
                            
      
                          {{-- <div class="error-box-class" id="error-jenisCenderamata">
                              Please fill in this field.
                          </div> --}}
                      </div>
  
                      {{-- <div class="error-box-class" id="error-jenisCenderamata">
                          Please fill in this field.
                      </div> --}}
                     
                    </div> <p> &nbsp;&nbsp;</p><p></p><p></p>
                    <span class="font-title">Practical Exam</span>&nbsp;&nbsp;<label for="name"><span style="color:#ff0000">Update the related field only*</span></label>
                    <p> </p> <hr>
                      <div class="row">
                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label for="name">Part A</label>
                                  <input type="text" class="form-control" id="partA" name="partA"
                                      value="" required>
                                      {{-- <div class="error-box-class" id="error-partA">
                                          Please fill in this field.
                                      </div> --}}
                              </div>
                          </div>
                     
                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label for="name">Part B</label>
                                  <input type="text" class="form-control" id="partB" name="partB"
                                      value="" required>
                                      {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                          Please fill in this field.
                                      </div> --}}
                              </div>
                          </div>

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label for="name">Part C</label>
                                  <input type="text" class="form-control" id="partC" name="partC"
                                      value="" required>
                                      {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                          Please fill in this field.
                                      </div> --}}
                              </div>
                          </div>


                      </div>

                      <div class="form-group">
                          <label for="totalResultPractical">Total Practical Exam</label>
                          <input type="text" class="form-control mt-1" name="totalResultPractical" id="totalResultPractical"
                            placeholder="Please fill in this field." autocomplete="off">
      
                          {{-- <div class="error-box-class" id="error-jenisCenderamata">
                              Please fill in this field.
                          </div> --}}
                      </div>

                    <div class="form-group">
                      <label for="statusResultPracticalExam">Result Practical Exam</label>
                      
                        <select class="form-control mt-1" name="statusResultPracticalExam" id="statusResultPracticalExam">
                          <option>Please Select </option>
                          <option value="Pass">Pass </option>
                          <option value="Fail">Fail </option>
                         
                        </select>

                      <span class="font-title">Final Result</span>
                      <p> </p><hr>
                     

                        <div class="form-group">
                            <label for="statusResultFinal">Final Verdict Status</label>
                            <select class="form-control mt-1" name="statusResultFinal" id="statusResultFinal">
                                <option>Please Select </option>
                                <option value="Pass">Pass</option>
                                <option value="Fail">Fail</option>
                               
                            </select>
                              
        
                            {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                Please fill in this field.
                            </div> --}}
                        </div>
              
                      <input type="hidden" class="form-control mt-1" name="id_participant_examresult" id="id_participant_examresult"
                       autocomplete="off">
                      <input type="hidden" class="form-control mt-1" name="id_training" id="id_training"
                       autocomplete="off">

                  </form>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                  <span class="d-none d-sm-block">Close</span>
                </button>
               
                <button type="button" class="btn btn-primary ml-1" id="hantarEditBtn">
                  <i class="bx bx-check d-block d-sm-none"></i>
                  <span id="spanSimpan" class="d-none d-sm-block">Save</span>
                </button>
              </div>


              
            </div>
          </div>
      </div>

      </section>
    


      
      <!--modal confirmation -->
      



      <!-- --->
    <section>
      <div class="row">
        <div class="col-12">
          <div class="modal fade" id="confirmationEditModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">UPDATE EXAM RESULT</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                  </button>
                </div>
                <div class="modal-body">
                  <p class="mb-0">
                    Are you sure you want to update / save this exam result?
                  </p>
                </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                      <span class="d-none d-sm-block">No</span>
                    </button>
      
                    <button type="button" class="btn btn-primary ml-1" data-dismiss="modal" id="submitKemaskiniBtn">
                      <i class="bx bx-check d-block d-sm-none"></i>
                      <span class="d-none d-sm-block">Yes</span>
                    </button>
                  </div>


                  
              </div>
            </div>
          </div>
        </div>
      </div> 

    </section>
    <section>
      <div class="modal" id="success-save-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
          
          <div class="modal-content">
            <div class="modal-header" style="padding:1.3rem;">
              <h4 class="modal-title text-3" id="myModalLabel33">
              </h4>
          
            </div>
  
            <div class="modal-body" style="padding: 1.3rem 1.3rem 0 1.3rem;">
              <div class="modal-content-border">
                <form id="formEndorseResult">
                  
                    <div class="row">
                        <div class="col-sm-12">
                          <p class="mb-0">
                            Successfully save the exam result.
                          </p>
                        </div>
                  
                </form>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                <span class="d-none d-sm-block">close</span>
              </button>
             
             
            </div>
  
  
            
          </div>
        </div>
      </div>
     </section>
</main>
  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    {{-- <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script>  --}}
    
@endsection



@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
    let batch_no_full = "{{ $batch_no_full }}";
</script>


<script src="{{ asset('js/examMgmt/updateExamResult.js') }}"></script>
<script src="{{ asset('js/examMgmt/submitUpdateExamResult.js') }}"></script>

@endsection
