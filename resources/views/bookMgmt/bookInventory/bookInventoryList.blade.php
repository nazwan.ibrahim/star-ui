@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Book Management')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.icon-editIcon:before {
  content: "\e916";
}

.header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .dataTables_length {
        display: none;
    }

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .btn-add-user:hover {
        background-color: #3e6be1;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }   

    .btn-actions {
        display: flex;
        gap: 10px;
    }

    .btn-edit,
    .btn-delete {
        background-color: #5A8DEE;
        color: #fff;
        border: none;
        border-radius: 5px;
        padding: 5px 10px;
        font-size: 14px;
        font-weight: 600;
        cursor: pointer;
        transition: background-color 0.3s ease-in-out;
    }

    .btn-edit:hover,
    .btn-delete:hover {
        background-color: #3e6be1;
    }

     /* Add styles for the table */
    .table {
    width: 100%;
    border-collapse: collapse;
    margin: 0;
    }

   

    .table th, .table td {
    padding: 10px;
    text-align: center;
    border: 1px solid #ddd;
    }

    /* Add hover effect on table rows */
    .table tbody tr:hover {
    background-color: #f5f5f5;
}
</style>
@endsection

@section('content')
<main style="padding:50px;margin:50px;">
    <section class="header-top-margin">
  
    <div class="text-center mb-3">
        <h2 class="page-title py-2" style="text-align: left !important;">Book Inventory List</h2>
    </div>
    <a href="{{ route('add-book') }}">
      <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40" fill="none">
        <g clip-path="url(#clip0_47_10118)">
        <circle cx="20" cy="20" r="20" fill="#3498DB"/>
        <path d="M10 10.6091C10 9.8291 10.3044 9.081 10.8461 8.52942C11.3879 7.97784 12.1227 7.66797 12.8889 7.66797H24.4444C25.2106 7.66797 25.9454 7.97784 26.4872 8.52942C27.029 9.081 27.3333 9.8291 27.3333 10.6091V18.2665C25.9726 17.8732 24.5334 17.8569 23.1644 18.2193C21.7954 18.5817 20.5463 19.3097 19.5461 20.328C18.5459 21.3463 17.8308 22.618 17.4749 24.0118C17.1189 25.4056 17.1349 26.8708 17.5212 28.2562H11.4444C11.4444 28.6462 11.5966 29.0203 11.8675 29.2961C12.1384 29.5719 12.5058 29.7268 12.8889 29.7268H18.0889C18.3532 30.2533 18.6724 30.7459 19.0379 31.1974H12.8889C12.1227 31.1974 11.3879 30.8875 10.8461 30.3359C10.3044 29.7844 10 29.0363 10 28.2562V10.6091ZM25.1667 32.668C26.8906 32.668 28.5439 31.9708 29.7629 30.7297C30.9818 29.4887 31.6667 27.8054 31.6667 26.0503C31.6667 24.2952 30.9818 22.612 29.7629 21.3709C28.5439 20.1299 26.8906 19.4327 25.1667 19.4327C23.4428 19.4327 21.7895 20.1299 20.5705 21.3709C19.3515 22.612 18.6667 24.2952 18.6667 26.0503C18.6667 27.8054 19.3515 29.4887 20.5705 30.7297C21.7895 31.9708 23.4428 32.668 25.1667 32.668ZM25.1667 22.3739C25.3582 22.3739 25.5419 22.4513 25.6774 22.5892C25.8128 22.7271 25.8889 22.9141 25.8889 23.1091V25.315H28.0556C28.2471 25.315 28.4308 25.3925 28.5662 25.5304C28.7017 25.6683 28.7778 25.8553 28.7778 26.0503C28.7778 26.2453 28.7017 26.4324 28.5662 26.5703C28.4308 26.7081 28.2471 26.7856 28.0556 26.7856H25.8889V28.9915C25.8889 29.1865 25.8128 29.3735 25.6774 29.5114C25.5419 29.6493 25.3582 29.7268 25.1667 29.7268C24.9751 29.7268 24.7914 29.6493 24.656 29.5114C24.5205 29.3735 24.4444 29.1865 24.4444 28.9915V26.7856H22.2778C22.0862 26.7856 21.9025 26.7081 21.7671 26.5703C21.6316 26.4324 21.5556 26.2453 21.5556 26.0503C21.5556 25.8553 21.6316 25.6683 21.7671 25.5304C21.9025 25.3925 22.0862 25.315 22.2778 25.315H24.4444V23.1091C24.4444 22.9141 24.5205 22.7271 24.656 22.5892C24.7914 22.4513 24.9751 22.3739 25.1667 22.3739ZM13.9722 10.6091C13.6849 10.6091 13.4094 10.7253 13.2062 10.9322C13.003 11.139 12.8889 11.4196 12.8889 11.7121V12.4474C12.8889 13.0562 13.3742 13.5503 13.9722 13.5503H23.3611C23.6484 13.5503 23.924 13.4341 24.1271 13.2273C24.3303 13.0204 24.4444 12.7399 24.4444 12.4474V11.7121C24.4444 11.4196 24.3303 11.139 24.1271 10.9322C23.924 10.7253 23.6484 10.6091 23.3611 10.6091H13.9722Z" fill="white"/>
        </g>
        <defs>
        <clipPath id="clip0_47_10118">
        <rect width="40" height="40" fill="white"/>
        </clipPath>
        </defs>
      </svg>
    </a>


    <div class="table-responsive">
        <div id="bookInventoryList_wrapper" class="dataTables_wrapper no-footer">
            <table class="table table-width-style dataTable no-footer" id="BookInventoryList" style="width: 100%">

                <thead class="table-header">
                    <tr>
                        <th class="header-1">No.</th>
                        <th class="header-1">Book Name</th>
                        <th class="header-1">Price(RM)</th>
                        <th class="header-1">Stock In</th>
                        <th class="header-1">Stock Out</th>
                        <th class="header-1">Available Balance</th>
                        <th class="header-1">Status</th>
                        <th class="header-1">Action</th>
                    </tr>
                </thead>

                <tbody>
                </tbody>

            </table>
        </div>
    </div>

         <!-- Delete Modal -->
         {{-- <div class="row"> --}}
          <div class="col-12">
              <div class="modal fade text-left" id="hapus-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModalTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                      <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title text-3" id="deleteModalTitle"></h4>
                              <button type="button" class="close model-remove-icon-bg"  data-dismiss="modal" aria-label="Close">
                                  <i class="bx bx-x modal-close-icon-1"></i>
                              </button>
                          </div>

                          <form id="">
                              <div class="modal-body">Are you confirm to detele this book?</div>
                                  <div class="modal-footer">
                                      <button type="button" class="btn btn-cancel-1" data-dismiss="modal">
                                          <i class="bx bx-x d-block d-sm-none"></i>
                                          <span class="d-none d-sm-block">No</span>
                                      </button>
                                      <button type="button" class="btn btn-primary-1 ml-1 hapusBtn" id="submitBtn">
                                          <i class="bx bx-check d-block d-sm-none"></i>
                                          <span class="d-none d-sm-block">Yes</span>
                                      </button>
                                  </div>
                          </form>

                      </div>
                   </div>
              </div>
          </div>
      {{-- </div> --}}


      <!--loading-->
<div class="row">
  <div class="col-12">
    <div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
          <div class="modal-body d-flex justify-content-center flex-column align-items-center">
            <div class="spinner-border" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--success-->

<div class="row">
  <div class="col-12">
      <div class="modal" id="successDeleteModal" tabindex="-1" role="dialog"
          aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable">
              <div class="modal-content">
                  <div class="modal-header" style="border-bottom: none;">
                      <h5 class="modal-title white" id="myModalLabel110"></h5>
                      <button type="button" class="close modal-close-icon-1 model-remove-icon-bg"
                          data-dismiss="modal" aria-label="Close">
                          <i class="bx bx-x"></i>
                      </button>
                  </div>
                  <div class="modal-body d-flex justify-content-center flex-column align-items-center">
                      <div class="d-flex justify-content-center align-items-center">
                          <p class="mb-0">
                             Deleting book success!
                          </p>
                      </div>
                      <div class="d-flex justify-content-center align-items-center mt-3">
                          <button type="button" class="btn btn-add-2 mr-1 refreshBtn">Close</button>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
   
    </section>
</main>
  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script> 
    
@endsection



@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>

<script src="{{ asset('js/bookMgmt/bookInventory/bookInventory.js') }}" ></script>

@endsection
