@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Admin - Book Management')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection
@section('page-styles')
<style>
  thead input {
    width: 100%;
  }

  .table thead {
    text-transform: capitalize;
  }

  .header-color {
    color: #2B6AB2 !important;
  }

  .btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}


</style>
@endsection

@section('content')
<main class="" style="padding-top:50px;padding-bottom:100px;">
  <section class="header-top-margin">

  <!-- breadcrumb -->
  
    <div class="card mt-7" style="padding:50px;margin:50px;background-color:#fbf9f9">
      {{-- <form method="POST" action="/submit">
          @csrf --}}
        <div class="form-group" style="margin:10px;">
            <label class="poppins-semibold-14 mb-1 font-title" for="jenisCenderamata-0"><b>UPDATE BOOK DETAILS</b></label>
        </div>
    

          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-body pr-1">
                  <div class="row mx-0 mb-2">
                    <div class="col-sm-9 card-title poppins-semibold-20 blue"><b>Book Details</b></div>
                  </div>

                  <div class="d-flex border rounded-lg p-2">
                    <div class="row">
                      <div class="col-2 mr-3" id="BookCoverImg"></div>

                      <div class="mt-1 ml-5">
                        <div class="d-flex">
                          <div class="text-1" style="width:190px;">BOOK TITLE</div>
                          <div class="mx-3">:</div>
                          <div class="text-2" id="booktitle"></div>
                        </div>

                        <div class="d-flex mt-1">
                          <div class="text-1" style="width:189px;">BOOK DETAILS</div>
                          <div class="mx-3">:</div>
                          <div class="text-2" id="bookdetails"></div>
                        </div>

                        <div class="d-flex mt-1">
                          <div class="text-1" style="width:190px;">STOCK IN</div>
                          <div class="mx-3">:</div>
                          <div class="text-2" id="stockin"></div>
                        </div>

                        <div class="d-flex mt-1">
                          <div class="text-1" style="width:190px;">STOCK OUT</div>
                          <div class="mx-3">:</div>
                          <div class="text-2" id="stockout"></div>
                        </div>    

                        <div class="d-flex mt-1">
                          <div class="text-1" style="width:190px;">AVAILABLE STOCK BALANCE</div>
                          <div class="mx-3">:</div>
                          <div class="text-2" id="stockbalance"></div>

                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
    
          <div class="form-group col-12" style="margin:40px 40px 40px 0;">
            <div id="maklumatDaftar">
              <label class="poppins-semibold-14 mb-1" for="pilihSemua"><b>Update Book Information</b></label>
                    <div class="">
                      <div class="form-group form-list">
                        <div class="border rounded-lg p-3">
                          <div class="rounded-lg pb-2" style="border: 2px dashed #DFE3E7;">
                          
                              <input style="visibility:hidden;" type="file" class="uploadImg" id="uploadCoverImg-0"
                                name="uploadCoverImg-0[]" accept=".jpg, .jpeg, .png, .img" onchange="onChangeUploadFile()" multiple>
                                <button onclick="uploadButton()" type="file" class="btn btn-block" id="btnuploadfileParentDiv-0">
                                    <div class="poppins-medium-14" style="color: #475F7B;" style="position: absolute; top: 25%; left: 120px;">Click here to upload book cover
                                      <img class="" alt="Default Image" width="20%" height="60%" style="float: left;" src="{{ asset('images/book.jpg') }}" id="previewImage">
                                        <i class="fa fa-camera" style="font-size: 25px;"></i> 
                                    </div>
                                    <div class="poppins-medium" style="font-size: 10px; color: #69809A">(Please choose your file to be uploaded)</div>
                                </button>
                                <div class="form-group row justify-content-center" id="parentDivUploadedFileDisplay-0" style="display: none"></div>
                              </div>
                          </div>
                        </div>
    
                        <div class="form-group" id="kemaskiniGambarLoadingIcon" style="display: none">
                          <div style="border: 1px solid #DFE3E6;text-align: center;padding: 50px;">
                            <div class="spinner-border" role="status">
                              <span class="sr-only">Loading...</span>
                            </div>
                          </div>
                        </div>
        
                            {{-- <div class="error-box-class" id="error-uploadedGambarBilik"> Sila isi medan ini.</div>
                        
                            <div class="error-box-class" id="error-uploadedMaxSizeLimitReached">Sila pilih gambar di bawah saiz 8mb.</div>
                            <div class="error-box-class" id="error-uploadedImageInvalidType">Sila muatnaik gambar cenderamata</div>
                          --}}
    
    
       
                          <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="name">Book Title<span style="color:#ff0000">*</span></label>
                                <input type="text" class="form-control" id="BookTitle" name="BookTitle"
                                    value="" required>
                                {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                                    *Sila lengkapkan medan ini
                                </div> --}}
                              </div>
                            </div>
            
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="name">Book Description<span style="color:#ff0000">*</span></label>
                                  <textarea rows="4" cols="50" class="form-control" id="BookDescription" name="BookDescription" value="" required maxlength="200"></textarea>
                                  {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                                      *Sila lengkapkan medan ini
                                  </div> --}}
                              </div>
                            </div>
                          </div>
    
                          <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                              <div class="form-group">
                                      <label for="name">Book Code<span style="color:#ff0000">*</span></label>(RM)
                                      <input type="text" class="form-control" id="BookCode" name="BookCode"
                                          value="" required>
                                      {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                                          *Sila lengkapkan medan ini
                                      </div> --}}
                              </div>
                            </div>
            
                          </div>
                       
    
    
    
                        <div class="row" style="margin:10px;">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label for="name">Stock In<span style="color:#ff0000">*</span></label>
                                <input type="text" class="form-control" id="StockIn" name="StockIn" value="" required>
                                    {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                                        *Sila lengkapkan medan ini
                                    </div> --}}
                            </div>
                          </div>
                         
                          
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label for="name">Stock Out<span style="color:#ff0000">*</span></label>
                                <input type="text" class="form-control" id="StockOut" name="StockOut" value="" required>
                                <input type="hidden" class="form-control" id="stockbalanceInput" name="stockbalanceInput" value="">
                                    {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                                        *Sila lengkapkan medan ini
                                    </div> --}}
                            </div>
                          </div>
                        </div>

                        <div class="row" style="margin:30px;">
                          <div class="form-group">
                              <label for="name"><b>Selling Price</b></label>     
                          </div>

                          {{-- <span>Sell Price</span> --}}
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label for="name">Online Price<span style="color:#ff0000">*</span></label>(RM)
                              <input type="text" class="form-control" id="SellPrice" name="SellPrice" value="" required>
                            </div>
                          </div>

                          <div class="col-sm-6">
                            <div class="form-group">
                              <label for="name">Walk In Price<span style="color:#ff0000">*</span></label>
                               (RM) <input type="text" class="form-control" id="WalkInPrice" name="WalkInPrice" value="" required>
                                    {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                                        *Sila lengkapkan medan ini
                                    </div> --}}
                            </div>
                          </div>


                       

                          <div class="col-sm-6">
                            <div class="form-group">
                              <label for="name">Special Price<span style="color:#ff0000">*</span></label>
                               (RM) <input type="text" class="form-control" id="SpecialPrice" name="SpecialPrice" value="" required>
                                    {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                                        *Sila lengkapkan medan ini
                                    </div> --}}
                            </div>
                          </div>
                        </div>

                        <div class="row" style="margin:30px;">
                          <div class="form-group">
                              <label for="name"><b>Book Reservation</b></label>     
                          </div>

                          {{-- <span>Sell Price</span> --}}
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label for="name">Quantity Reserve</label>
                              <input type="number" class="form-control" id="QuantityReserve" name="QuantityReserve" value="" required>
                            </div>
                          </div>

                          {{-- <div class="col-sm-6">
                            <div class="form-group">
                              <label for="name">Walk In Price<span style="color:#ff0000">*</span></label>
                               (RM) <input type="text" class="form-control" id="WalkInPrice" name="WalkInPrice" value="" required>
                                    {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                                        *Sila lengkapkan medan ini
                                    </div> --}}
                            {{-- </div>
                          </div> --}} 


                       

                          <div class="col-sm-6">
                            <div class="form-group">
                              <label for="name">Remark(reason for reservation)</label>
                              <textarea rows="4" cols="50" class="form-control" id="ReserveRemark" name="ReserveRemark" value="" required maxlength="200"></textarea>
                                    
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  
           
                        <div class="d-flex justify-content-end pt-3 pb-2">
                          <div style="margin:10px;">
                            <a href="{{ route('book-inventory') }}" class="btn btn-secondary-6 btn-block">Back</a>
                          </div>
                          <div style="margin:10px;">
                              <button  id="submitBorangDaftar" class="btn btn-secondary-6 btn-block">Save</button>
                          </div>
                        </div>
                        
              </div>
                 
            </div>
               
    </div>

      <!--loading-->
      <div class="row">
        <div class="col-12">
          <div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable">
              <div class="modal-content">
                <div class="modal-body d-flex justify-content-center flex-column align-items-center">
                  <div class="spinner-border" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>

<section>
  <div class="card mt-7" style="padding:50px;margin:50px;background-color:#fbf9f9">
    <h4 class="page-title py-2" style="text-align: left !important;">Book Transaction History</h4>

  {{-- <div class="table-responsive">
    <div id="BookHistory_wrapper" class="dataTables_wrapper no-footer"> --}}
        <table class="table table-width-style dataTable no-footer" id="BookHistory" style="width: 100%">

            <thead class="table-header">
                <tr>
                    <th class="header-1">No.</th>
                    <th class="header-1">Name</th>
                    <th class="header-1">Quantity</th>
                    <th class="header-1">Price Per Unit(RM)</th>
                    <th class="header-1">Total Price(RM)</th>
                    <th class="header-1">Date Purchased</th>
                    <th class="header-1">Status</th>
                </tr>
            </thead>

            <tbody>
            </tbody>

        </table>
    {{-- </div>
</div> --}}
  </div>
</section>

</main>


  @endsection

  @section('vendor-scripts')
  <script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
  <script src="{{asset('vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
  
  @endsection

  @section('page-scripts')
  <script>
    let userID = "{{ $userid }}";
    let host = "{{ env('API_SERVER') }}";
   
  </script>


  <script src="{{ asset('js/bookMgmt/bookInventory/bookInventoryUpdate.js') }}" ></script>
 
  @endsection

  