@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Book Management')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection
@section('page-styles')
<style>
  thead input {
    width: 100%;
  }

  .table thead {
    text-transform: capitalize;
  }

  .header-color {
    color: #2B6AB2 !important;
  }
</style>
@endsection

@section('content')
<section class="header-top-margin">

  <!-- breadcrumb -->
  

  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body pr-1">
          <div class="row mx-0 mb-2">
            <div class="col-sm-9 card-title poppins-semibold-20 blue">Update</div>
          </div>

          <div class="d-flex border rounded-lg p-2">
            <div class="row">
              <div class="col-2 mr-3" id="BookCoverImg"></div>

              <div class="mt-1 ml-5">
                <div class="d-flex">
                  <div class="text-1" style="width:190px;">JENIS CENDERAMATA</div>
                  <div class="mx-3">:</div>
                  <div class="text-2" id="jenisCenderamata"></div>
                </div>

                <div class="d-flex mt-1">
                  <div class="text-1" style="width:189px;">MAKLUMAT TERPERINCI</div>
                  <div class="mx-3">:</div>
                  <div class="text-2" id="maklumatTerperinci"></div>
                </div>

                <div class="d-flex mt-1">
                  <div class="text-1" style="width:190px;">JUMLAH STOK PEMBELIAN</div>
                  <div class="mx-3">:</div>
                  <div class="text-2" id="jumlahStok"></div>
                </div>

                <div class="d-flex mt-1">
                  <div class="text-1" style="width:190px;">STATUS STOK TERKINI</div>

                  <div class="d-flex">
                    <div class="mx-3">:</div>
                    <div class="text-2" id="statusStok">
                      <span class="bullet bullet-sm bullet-red mr-1"></span>
                      <span class="poppins-medium-14 mr-1">Dikeluarkan</span>
                      <span class="poppins-medium-14 mr-1" id="dikeluarkan"></span>

                      <span class="bullet bullet-sm bullet-soft-green mr-1"></span>
                      <span class="poppins-medium-14 mr-1">Baki</span>
                      <span class="poppins-medium-14 mr-1" id="baki"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="table-responsive mt-2">
            <table class="table grey" id="tableDirektori" style="width: 100%">
              <thead>
                <tr>
                  <th class="poppins-medium-14 grey">Bil.</th>
                  <th class="poppins-medium-14 grey">Nama Pembekal</th>
                  <th class="poppins-medium-14 grey">Tarikh Stok Diterima</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>

          <div class="row mt-3">
            <div class="col-12 d-flex justify-content-end align-items-end">
              <div>
                <a href="" class="btn btn-primary-2">Kembali ke Laman Cenderamata</a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  @endsection

  @section('vendor-scripts')
  <script src="{{asset('vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
  <script src="{{asset('vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
  
  @endsection

  @section('page-scripts')
  <script>
    let userID = "{{ $userid }}";
    let host = "{{ env('API_SERVER') }}";
   
  </script>


  <script src="{{ asset('js/bookMgmt/bookInventory/bookInventoryUpdate.js') }}" ></script>
 
  @endsection

  