@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Add Book Inventory')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   

    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.form-group {
    display: flex;
    align-items: center; /* Vertical alignment */
}

label {
    width: 100px; /* Adjust as needed for your design */
    text-align: left; /* Adjust for alignment (left, right, center) */
    /* margin-right: 10px; Spacing between label and input */
}

.book-cover-card-body {
  padding: 10px;
  border: 1px solid rgba(0, 0, 0, 0.125);
}

.book-cover-card-btn {
  color: #475f7b;
  text-align: center;
  padding: 10px;
  border: 1px solid rgba(0, 0, 0, 0.125);
}

.book-cover-card-btn:hover {
  color: white;
  background-color: #ff5b5c;
}
.nav_penyelenggaraan_tempahan:hover {
  background-color: #5a8dee;
  border: 1px solid #5a8dee;
  color: #ffffff !important;
}
</style>
@endsection

@section('content')
<main style="padding:50px;margin:50px;">
<section class="header-top-margin">
<div class="card mt-7" style="padding:50px;margin:50px;background-color:#fbf9f9">
{{-- <form method="POST" action="/submit">
    @csrf --}}
    <div class="form-group" style="margin:10px;">
        <label class="poppins-semibold-14 mb-1 font-title" for="jenisCenderamata-0">Add New Book</label>
    </div>


    <div class="form-group col-12  form-list">
        <div id="maklumatDaftar">
            <label class="poppins-semibold-14 mb-1" for="pilihSemua">Add Book Cover</label>
            <div class="form-group">
                <div class="border rounded-lg p-3">
                    <div class="rounded-lg pb-2" style="border: 2px dashed #DFE3E7;">
                
                        <input style="visibility:hidden;" type="file" class="uploadImg" id="uploadCoverImg-0"
                            name="uploadCoverImg-0[]" accept=".jpg, .jpeg, .png, .img" onchange="onChangeUploadFile()" multiple>
                            <button onclick="uploadButton()" type="file" class="btn btn-block" id="btnuploadfileParentDiv-0">
                                <div class="poppins-medium-14" style="color: #475F7B;" style="position: absolute; top: 25%; left: 120px;">Click here to upload book cover
                                    <img class="" alt="Default Image" width="20%" height="60%" style="float: left;" src="{{ asset('images/book.jpg') }}" id="previewImage">
                                    <i class="fa fa-camera" style="font-size: 25px;"></i> 
                                </div>
                                <div class="poppins-medium" style="font-size: 10px; color: #69809A">(Please choose your file to be uploaded)</div>
                            </button>
                        <div class="form-group row justify-content-center" id="parentDivUploadedFileDisplay-0" style="display: none"></div>
                    </div>
                </div>
            </div>

            <div class="form-group" id="kemaskiniGambarLoadingIcon" style="display: none">
                <div style="border: 1px solid #DFE3E6;text-align: center;padding: 50px;">
                    <div class="spinner-border" role="status">
                    <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>

            {{-- <div class="error-box-class" id="error-uploadedGambarBilik"> Sila isi medan ini.</div>
            <!-- <div class="error-box-class" id="error-uploadedBilanganGambarBilik">
                Sila pilih bilangan gambar di bawah had maksima 3.</div> -->
            <div class="error-box-class" id="error-uploadedMaxSizeLimitReached">Sila pilih gambar di bawah saiz 8mb.</div>
            <div class="error-box-class" id="error-uploadedImageInvalidType">Sila muatnaik gambar cenderamata</div> --}}
        


   
            <div class="row" style="margin:10px;">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Book Title<span style="color:#ff0000">*</span></label>
                        <input type="text" class="form-control" id="BookTitle" name="BookTitle"
                            value="" required>
                        {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                            *Sila lengkapkan medan ini
                        </div> --}}
                    </div>
                </div>
        
                <div class="col-sm-6">
                    <div class="form-group" style="margin-left:10px;">
                        <label for="name">Book Description<span style="color:#ff0000">*</span></label>
                        {{-- <input type="text" class="form-control" id="BookDescription" name="BookDescription"
                            value="" required> --}}
                            <textarea rows="4" cols="50" class="form-control" id="BookDescription" name="BookDescription" value="" required maxlength="200"></textarea>
                        {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                            *Sila lengkapkan medan ini
                        </div> --}}
                    </div>
                </div>
            </div>

            <div class="row" style="margin:10px;">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Book Code<span style="color:#ff0000">*</span></label>
                        <input type="text" class="form-control" id="BookCode" name="BookCode"
                            value="" required>
                        {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                            *Sila lengkapkan medan ini
                        </div> --}}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Stock In<span style="color:#ff0000">*</span></label>
                        <input type="text" class="form-control" id="StockIn" name="StockIn"
                            value="" required>
                        {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                            *Sila lengkapkan medan ini
                        </div> --}}
                    </div>
                </div>
        
               
            </div>

            <div class="row" style="margin:10px;">
               
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name"><b>Selling Price</b></label>  
                    </div>
                </div>
           
            </div>
            <div class="row" style="margin:10px;">
               
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Online Price<span style="color:#ff0000">*</span></label>
                        RM <input type="text" class="form-control" id="SellPrice" name="SellPrice"
                            value="" required>
                        {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                            *Sila lengkapkan medan ini
                        </div> --}}
                    </div>
                </div>
           
            </div>
            <div class="row" style="margin:10px;">
               
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Walkin Price<span style="color:#ff0000">*</span></label>
                       RM <input type="text" class="form-control" id="WalkinPrice" name="WalkinPrice"
                            value="" required>
                        {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                            *Sila lengkapkan medan ini
                        </div> --}}
                    </div>
                </div>
           
            </div>

            <div class="row" style="margin:10px;">
               
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Special Price<span style="color:#ff0000">*</span></label>
                        RM <input type="text" class="form-control" id="SpecialPrice" name="SpecialPrice"
                            value="" required>
                        {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                            *Sila lengkapkan medan ini
                        </div> --}}
                    </div>
                </div>
           
            </div>
        </div>

        </div>

     
            <div class="d-flex justify-content-end pt-3 pb-2">
                <div style="margin:10px;">
                <a href="{{ route('book-inventory') }}" class="btn btn-secondary-6 btn-block">Back</a>
                </div>
                <div style="margin:10px;">
                    <button  id="submitBorangDaftar" class="btn btn-secondary-6 btn-block">Add</button>
                </div>
            </div>
    </div>
</div>

<!--loading-->
<div class="row">
    <div class="col-12">
      <div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable">
          <div class="modal-content">
            <div class="modal-body d-flex justify-content-center flex-column align-items-center">
              <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
</section>
</main>
@endsection

@section('vendor-scripts')

@endsection

@section('page-scripts')
<script>
//  our own js 
let host = "{{ env('API_SERVER') }}";
let userID = "{{ $userid }}";
  
$(document).ready(function() {
    $('#saveBtn').on('click', function() {
        updateInfo();
     
        });
    });

    // document.getElementById('uploadCoverImg-0').addEventListener('change', function() {
    //         const selectedFile = this.files[0];
    //         const previewImage = document.getElementById('previewImage');

    //         if (selectedFile) {
    //             // Display the selected image in the preview element
    //             previewImage.src = URL.createObjectURL(selectedFile);
    //         }
    //     });
</script>

<script src="{{ asset('js/bookMgmt/bookInventory/bookInventory.js') }}" ></script>
@endsection
