@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Participant Dashboard')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
{{-- add css style here --}}
<style>

.layoutdetails_half{
display: flex;
width: 100%;
padding-left: 40px;
padding-top: 20px;
padding-bottom: 20px;
flex-direction: column;
align-items: flex-start;
gap: var(--spacing-24, 24px);
border-radius: 16px;
border: 1px solid var(--gray-400, #98A2B3);
background: #FFF;
}

.font-title{
    color: #656565;

font-family: Rajdhani;
font-size: 26px;
font-style: normal;
font-weight: 600;
line-height: 32px; /* 123.077% */
}

.normal_text{
    color: #454545;

font-family: Open Sans;
font-size: 16px;
font-style: normal;
font-weight: 400;
line-height: 28px; /* 175% */

}
.spanwidth{
    width: 95px;
}

.page-title {
    color: #656565;
    font-family: Rajdhani;
    font-size: 26px;
    font-style: normal;
    font-weight: 600;
}

.normal-text{
    color: #333;
    font-family: Open Sans;
    font-size: 18px;
    font-style: normal;
    font-weight: 400;
    margin-bottom: 1%;
}

.text-content{
    color: #454545;
    font-size: 18px;
    font-family: Open Sans;
    font-style: normal;
    font-weight: 600;
    margin-bottom: 1%;
}

.side-by-side {
        float: left;
        margin-right: 10px; /* Optional: Add margin between the divs */
    }
</style>
@endsection

@section('content')
<main style="padding:50px;margin:50px;">
    <section class="header-top-margin">
        <div class="row" style="margin:2px;">
        {{-- <form id="paymentForm" method="POST" action="{{ route('book-Payment-Invoice') }}">    --}}
        <form id="paymentForm" method="POST" action="{{ route('book_process_payment') }}">   
            @csrf
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @else
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                @endif     
       
            <div style="float:left;text-align:left;" class="">
                <table>
                    @foreach ($bookDetails as $book)
                        @if ($bookDetails)
                            <tr>
                                <td>
                                    <ul id="selected-items"></ul>
                                    <div style="display: flex; justify-content: space-between;margin-top:20px;">
                                        <div class="form-group card" style="display: flex; justify-content: space-between;background-color: #fbf9f9;width: 100%; height: 100%;">
                                            <div style="background-color: #fbf9f9;margin:2%;padding:2%;">
                                                <img src="data:image/{{$book->file_format}};base64,{{$book->file_cover}}" style="width: 290px; height: 293px;">
                                                <div>Product Title: {{ $book->book_title }}</div> 
                                                <div>Product Description: {{ $book->book_description }}</div><input type="hidden" name="inventory_id" id="inventory_id" value={{ $book->id }}>
                                                <div>Product Price: RM{{ $book->online_price_perunit }}</div><input type="hidden" name="online_price" id="online_price" value={{ $book->online_price_perunit }}>
                                                <div>Quantity: {{ $book->quantity }}</div><input type="hidden" name="quantity" id="quantity" value={{ $book->quantity }}>
                                                <div>Total Price: RM{{ $book->total_price}}</div><input type="hidden" name="total_price" id="total_price" value={{ $book->total_price }}>
                                            </div>
                                        </div>
                                    </div>
        
                                    
                                        
                                </td>
                            </tr>
                        @else
                            <tr><td>Product not found</td></tr>
                        @endif
                    @endforeach
                    <tr><td>
                    <!-- Display Grand Total -->
                    
                
                </td></tr>
                </table>
            </div>
            <div style="float:left;text-align:left;margin:50px 30px 0 80px;" class="col-sm-6">
                <div >
                    <div class="layoutdetails_half">
                        <span class="font-title" id="participant_details"></span>
                        <div class="normal-text" style="margin-top: -20px;"><span>Name</span> : <span class="text-content" id="participant_name"><?php echo $detailsPayment->fullname; ?></span></div>
                        <div class="normal-text" style="margin-top: -20px;"><span>Email Address</span> : <span class="text-content" id="email_address"><?php echo $detailsPayment->email; ?></span></div>
                        <div class="normal-text" style="margin-top: -20px;"><span>Phone Number</span> : <span class="text-content"  id="phone_number"><?php echo $detailsPayment->phone_no; ?></span></div>
                        <div class="normal-text" style="margin-top: -20px;"><span>Address </span> : <span class="text-content" id="address_1"><?php echo $detailsPayment->address_1; ?>&nbsp;&nbsp;</span><span id="address_2"><?php echo $detailsPayment->address_2; ?></span></div>
                        <div class="normal-text" style="margin-top: -20px;"><span>Country </span> : <span class="text-content" id="country"></span><?php echo $detailsPayment->country; ?></div>
                        <div class="normal-text" style="margin-top: -20px;"><span>Postcode</span> : <span class="text-content" id="postcode"><?php echo $detailsPayment->postcode; ?></span></div>
                        <div class="normal-text" style="margin-top: -20px;"><span>State</span> : <span class="text-content" id="state"></span><?php echo $detailsPayment->state; ?></div>
                        
                    </div>  
                </div>
          
                <div style="margin-top:30px;">
                    <div class="layoutdetails_half">
                        <span class="font-title" id="payment_summary">Payment Summary</span>
                        <div class="normal-text"><span>Grand Total</span> : RM<span class="text-content" id="payment_total">{{  $detailsPayment->grand_total_price  }}</span></div>
                    </div>
                
         
                    <div class="d-flex justify-content-end pt-3 pb-2">
                        <div style="margin:10px;">
                        <a href="{{ route('list-book') }}" class="btn btn-outline-danger">Cancel</a>
                        </div>
                        <div style="margin:10px;">
                    
                        <button class="btn btn-primary" id="payment_id">PAY NOW</button>

                    </div>
                
               
                </div>
            </div>
            </div>
           

       
   </form>
</div>
  
</section>
</main> 


@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
    {{-- <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>  --}}
    
@endsection

@section('page-scripts')
{{-- our own js --}}
{{-- <script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>
<script src="{{ asset('js/dashboard/ParticipantTrainingPayment.js') }}"></script> --}}

@endsection
