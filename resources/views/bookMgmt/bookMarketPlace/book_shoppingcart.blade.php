@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Book Marketplace')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />

{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .contentUpdate{
      color: #656565;
      font-size: 20px;
      font-family: Rajdhani;
      font-weight: 700;
      line-height: 28px;
      word-wrap: break-word;
    }

    .col-md-2-5 {
        flex: 0 0 23%;
        max-width: 23%;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
  color: var(--gray-950, #0C111D);
  width: 300px;
  font-family: Inter;
  font-size: 24px;
  font-style: normal;
  font-weight: 600;
  line-height: 32px; 
}

.form-group {
    display: flex;
    align-items: center; /* Vertical alignment */
}

.form-row {
    display: flex;
    align-items: center;
    margin-top: 5%;
}

label {
    width: 100%; /* Adjust as needed for your design */
    text-align: right; /* Adjust for alignment (left, right, center) */
}

.image-cap{
    max-width: 290px;
    max-height: 590px;
}

.btn-tertiary{
    background-color: #FFFFFF;
    color: #007BFF;
    border-color: #007BFF;
    font-family: Rajdhani;
}

.btn-primary{
    font-family: Rajdhani;
}

.cartno{
    color: #F04438;
    font-family: Open Sans;
    font-size: 13px;
    font-style: normal;
    font-weight: 600;
    line-height: -31px;
    margin-top: -10px;
    margin-left: 10px;
}

.product-card {
        display: flex;
        justify-content: space-between;
        background-color: #fbf9f9;
        width: 100%;
        padding: 2%;
        margin-bottom: 20px;
    }

    .product-details-container {
        display: flex;
        background-color: #fbf9f9;
        margin: 2%;
        padding: 2%;
        width: 100%;
    }

    .product-image img {
        float: left;
        text-align: center;
        max-width: 100%;
        max-height: 150px; /* Set the desired max height */
        height: auto;
    }

    .product-info {
        margin-left: 10px;
        width: 50%;
    }

    .product-title {
        font-size: 20px;
        font-weight: bold;
        color: #333;
        margin-bottom: 5px;
    }

    .product-description {
        font-size: 16px;
        color: #555;
        margin-bottom: 10px;
    }

    .product-price,
    .product-quantity,
    .product-total-price {
        font-size: 16px;
        color: #777;
        margin-bottom: 5px;
    }

    .quantity-controls {
        margin-left: 5px;
    }

    .quantity-controls span {
        margin-right: 5px;
        cursor: pointer;
    }
</style>

@endsection

@section('content')
<main class="col-md-12" style="padding: 50px;">
    <section class="header-top-margin">
        <h2 class="page-title" style="text-align: left;">Marketplace</h2>
        <div class="card mt-7" style="padding: 10px; margin: 10px;">
            <div>Book Cart</div>
            <form id="checkout-form" method="POST" action="{{ route('checkout-cart') }}">
                @csrf

                <table>
                    @foreach ($bookDetails as $index => $bookDetail)
                        <tr>
                            <td>
                                <div class="product-card">
                                    <div class="product-details-container">
                                        <div class="product-image">
                                            <img src="data:image/{{$bookDetail->file_format}};base64,{{$bookDetail->file_cover}}" alt="{{ $bookDetail->book_title }}">
                                        </div>
                                        <div class="product-info">
                                            <div class="product-title">{{ $bookDetail->book_title }}</div>
                                            <div class="product-description">{{ $bookDetail->book_description }}</div>
                                            <input type="hidden" name="inventory_id-{{ $bookDetail->id }}" id="inventory_id-{{ $bookDetail->id }}" value="{{ $bookDetail->id }}">
                                            <div class="product-price">Product Price: RM{{ $bookDetail->online_price }}</div>
                                            <input type="hidden" name="online_price-{{ $bookDetail->id }}" id="online_price-{{ $bookDetail->id }}" value="{{ $bookDetail->online_price }}">
                                            <div class="product-quantity">Quantity: <span id="quantity-{{ $bookDetail->id }}">{{ $productQuantities[$bookDetail->id] }}</span></div>
                                            <input type="hidden" name="quantityInput-{{ $bookDetail->id }}" id="quantityInput-{{ $bookDetail->id }}" value="{{ $productQuantities[$bookDetail->id] }}">
                                            <div class="product-total-price">Total Price: <span id="total_price-{{ $bookDetail->id }}">RM{{ $bookDetail->online_price * $productQuantities[$bookDetail->id] }}</span></div>
                                            <input type="hidden" name="total_priceInput-{{ $bookDetail->id }}" id="total_priceInput-{{ $bookDetail->id }}" value="{{ $bookDetail->online_price * $productQuantities[$bookDetail->id] }}">
                                            <div class="quantity-controls">
                                                <span><i class="fas fa-minus-circle" onclick="updateQuantity('{{ $bookDetail->id }}', -1)"></i></span>
                                                <span><i class="fas fa-plus-circle" onclick="updateQuantity('{{ $bookDetail->id }}', 1)"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            </td>
        </tr>
    @endforeach

    <!-- Display Grand Total -->
    <tr>
        <td>
            @if($grandTotal != 0)
            <div>
                    <div style="margin-left: 5px;">Grand Total: RM <span id="grand_price_hidden">{{ $grandTotal }}</span></div>
                    <input type="hidden" name="grand_price" id="grand_price" value={{ $grandTotal }}>
                </div>

                <div class="checkout-buttons">
                    <a href="{{ route('list-book') }}" class="btn btn-outline-danger">Back</a>
                    <button class="btn btn-primary" id="checkout-btn">Proceed To Checkout</button>
                </div>

                <!-- <button id="checkout-btn" class="btn btn-primary">Proceed To Checkout</button> -->
            @elseif($grandTotal == 0)
                <div>
                    <div style="color: crimson">Hi! Please add any book of your preference to proceed payment. Thank you.</div>
                    <div style="margin-left: 5px;">Grand Total: RM <span id="grand_price_hidden">{{ $grandTotal }}</span></div>
                    <input type="hidden" name="grand_price" id="grand_price" value={{ $grandTotal }}>
                </div>
                <a href="listOfBook" class="btn btn-primary">Back To Book Market Place</a>
            @endif
        </td>
    </tr>
</table>

    {{-- <button id="checkout-btn" class="btn btn-primary">Proceed to Checkout</button> --}}
   
</form>
  
</section>
</main>
@endsection

@section('vendor-scripts')

@endsection

@section('page-scripts')
<script>
let host = "{{ env('API_SERVER') }}";
let userID = "{{ $userid }}";
</script>
<script>
// function updateQuantity(productId, change) {
//     // Get the current quantity, total price, and grand price elements
//     var quantityElement = document.getElementById('quantity-' + productId);
//     var quantityElementInput = document.getElementById('quantityInput-' + productId);

//     var totalPriceElement = document.getElementById('total_price-' + productId);
//     var totalPriceElementInput = document.getElementById('total_priceInput-' + productId);



//     var grandPriceElementInput = document.getElementById('grand_price');
//     var grandPriceElementInput_hidden = document.getElementById('grand_price_hidden');
    

//     // Update the quantity
//     var newQuantity = parseInt(quantityElement.innerText) + change;
//     var newQuantityInput = parseInt(quantityElementInput.value) + change;

//     // Ensure the quantity doesn't go below 0
//     newQuantity = Math.max(0, newQuantity);

//     newQuantityInput = Math.max(0, newQuantity);

//     // Update the quantity element
//     quantityElement.innerText = newQuantity;
//     quantityElementInput.value = newQuantityInput;

//     // // Update the total price element
//     var sellPrice = parseFloat(document.getElementById('online_price-' + productId).value);
//     var newTotalPrice = sellPrice * newQuantity;
//     totalPriceElement.innerText = 'RM' + newTotalPrice.toFixed(2);


//     var sellPriceInput = parseFloat(document.getElementById('online_price-' + productId).value);
//     var newTotalPriceInput = sellPriceInput * newQuantityInput;
//     totalPriceElementInput.value = newTotalPriceInput.toFixed(2);




//     // // Update the grand price input
//     var grandPriceInput = parseFloat(grandPriceElementInput.value.replace('RM', ''));
//     var newGrandPriceInput = grandPriceInput + (change * sellPrice);
//     grandPriceElementInput.value = newGrandPriceInput.toFixed(2);


//     var grandPriceInput_hidden = parseFloat(grandPriceElementInput_hidden.innerText.replace('RM', ''));
//     var newGrandPriceInput_hidden = grandPriceInput_hidden + (change * sellPrice);
//     grandPriceElementInput_hidden.innerText= newGrandPriceInput_hidden.toFixed(2);
  
// }


// function updateQuantity(productId, change) {
//     // Get the current quantity, total price, and grand price elements
//     var quantityElement = document.getElementById('quantity-' + productId);
//     var quantityElementInput = document.getElementById('quantityInput-' + productId);

//     var totalPriceElement = document.getElementById('total_price-' + productId);
//     var totalPriceElementInput = document.getElementById('total_priceInput-' + productId);

//     var grandPriceElementInput = document.getElementById('grand_price');
//     var grandPriceElementInput_hidden = document.getElementById('grand_price_hidden');

//     // Update the quantity
//     var newQuantity = parseInt(quantityElement.innerText) + change;
//     var newQuantityInput = parseInt(quantityElementInput.value) + change;

//     // Ensure the quantity doesn't go below 0
//     newQuantity = Math.max(0, newQuantity);
//     newQuantityInput = Math.max(0, newQuantityInput);

//     // Update the quantity element
//     quantityElement.innerText = newQuantity;
//     quantityElementInput.value = newQuantityInput;

//     // Update the total price element
//     var sellPrice = parseFloat(document.getElementById('online_price-' + productId).value);
//     var newTotalPrice = sellPrice * newQuantity;
//     totalPriceElement.innerText = 'RM' + newTotalPrice.toFixed(2);

//     var newTotalPriceInput = sellPrice * newQuantityInput;
//     totalPriceElementInput.value = newTotalPriceInput.toFixed(2);

//     // Update the grand price input
//     var grandPriceInput = parseFloat(grandPriceElementInput.value.replace('RM', ''));
//     var newGrandPriceInput = grandPriceInput + (change * sellPrice);
//     newGrandPriceInput = Math.max(0, newGrandPriceInput); // Ensure grand price doesn't go below 0
//     grandPriceElementInput.value = newGrandPriceInput.toFixed(2);

//     var grandPriceInput_hidden = parseFloat(grandPriceElementInput_hidden.innerText.replace('RM', ''));
//     var newGrandPriceInput_hidden = grandPriceInput_hidden + (change * sellPrice);
//     newGrandPriceInput_hidden = Math.max(0, newGrandPriceInput_hidden); // Ensure grand price doesn't go below 0
//     grandPriceElementInput_hidden.innerText = 'RM' + newGrandPriceInput_hidden.toFixed(2);
// }


// function updateQuantity(productId, change) {
//     var quantityElement = document.getElementById('quantity-' + productId);
//     var quantityElementInput = document.getElementById('quantityInput-' + productId);
//     var totalPriceElement = document.getElementById('total_price-' + productId);
//     var totalPriceElementInput = document.getElementById('total_priceInput-' + productId);

//     var grandPriceElementInput = document.getElementById('grand_price');
//     var grandPriceElementInput_hidden = document.getElementById('grand_price_hidden');

//     var newQuantity = parseInt(quantityElement.innerText) + change;
//     var newQuantityInput = parseInt(quantityElementInput.value) + change;

//     newQuantity = Math.max(0, newQuantity);
//     newQuantityInput = Math.max(0, newQuantityInput);

//     quantityElement.innerText = newQuantity;
//     quantityElementInput.value = newQuantityInput;

//     var sellPrice = parseFloat(document.getElementById('online_price-' + productId).value);
//     var newTotalPrice = sellPrice * newQuantity;
//     totalPriceElement.innerText = 'RM' + newTotalPrice.toFixed(2);

//     var newTotalPriceInput = sellPrice * newQuantityInput;
//     totalPriceElementInput.value = newTotalPriceInput.toFixed(2);

//     // if (newQuantity > 0) {
//         var grandPriceInput = parseFloat(grandPriceElementInput.value.replace('RM', ''));
//         var newGrandPriceInput = grandPriceInput + (change * sellPrice);
//         newGrandPriceInput = Math.max(0, newGrandPriceInput);
//         grandPriceElementInput.value = newGrandPriceInput.toFixed(2);

//         var grandPriceInput_hidden = parseFloat(grandPriceElementInput_hidden.innerText.replace('RM', ''));
//         var newGrandPriceInput_hidden = grandPriceInput_hidden + (change * sellPrice);
//         newGrandPriceInput_hidden = Math.max(0, newGrandPriceInput_hidden);
//         grandPriceElementInput_hidden.innerText = 'RM' + newGrandPriceInput_hidden.toFixed(2);
//     // }
// }
// function updateQuantity(productId, change) {
//     var quantityElement = document.getElementById('quantity-' + productId);
//     var quantityElementInput = document.getElementById('quantityInput-' + productId);
//     var totalPriceElement = document.getElementById('total_price-' + productId);
//     var totalPriceElementInput = document.getElementById('total_priceInput-' + productId);

//     var grandPriceElementInput = document.getElementById('grand_price');
//     var grandPriceElementInput_hidden = document.getElementById('grand_price_hidden');

//     var currentQuantity = parseInt(quantityElement.innerText);
//     var newQuantity = currentQuantity + change;
//     var newQuantityInput = parseInt(quantityElementInput.value) + change;

//     newQuantity = Math.max(0, newQuantity);
//     newQuantityInput = Math.max(0, newQuantityInput);

//     quantityElement.innerText = newQuantity;
//     quantityElementInput.value = newQuantityInput;

//     var sellPrice = parseFloat(document.getElementById('online_price-' + productId).value);
//     var newTotalPrice = sellPrice * newQuantity;
//     totalPriceElement.innerText = 'RM' + newTotalPrice.toFixed(2);

//     var newTotalPriceInput = sellPrice * newQuantityInput;
//     totalPriceElementInput.value = newTotalPriceInput.toFixed(2);

//     if (change < 0 && newQuantity < currentQuantity) {
//         // If reducing quantity, deduct from grand_price_hidden
//         var grandPriceInput = parseFloat(grandPriceElementInput.value.replace('RM', ''));
//         var deduction = Math.abs(change * sellPrice);
//         var newGrandPriceInput = Math.max(0, grandPriceInput - deduction);
//         grandPriceElementInput.value = newGrandPriceInput.toFixed(2);

//         var grandPriceInput_hidden = parseFloat(grandPriceElementInput_hidden.innerText.replace('RM', ''));
//         var newGrandPriceInput_hidden = Math.max(0, grandPriceInput_hidden - deduction);
//         grandPriceElementInput_hidden.innerText = 'RM' + newGrandPriceInput_hidden.toFixed(2);
//     }
// }

function updateQuantity(productId, change) {
    var quantityElement = document.getElementById('quantity-' + productId);
    var quantityElementInput = document.getElementById('quantityInput-' + productId);
    var totalPriceElement = document.getElementById('total_price-' + productId);
    var totalPriceElementInput = document.getElementById('total_priceInput-' + productId);

    var grandPriceElementInput = document.getElementById('grand_price');
    var grandPriceElementInput_hidden = document.getElementById('grand_price_hidden');

    var currentQuantity = parseInt(quantityElement.innerText);
    var newQuantity = currentQuantity + change;
    var newQuantityInput = parseInt(quantityElementInput.value) + change;

    newQuantity = Math.max(0, newQuantity);
    newQuantityInput = Math.max(0, newQuantityInput);

    quantityElement.innerText = newQuantity;
    quantityElementInput.value = newQuantityInput;

    var sellPrice = parseFloat(document.getElementById('online_price-' + productId).value);
    var newTotalPrice = sellPrice * newQuantity;
    totalPriceElement.innerText = 'RM' + newTotalPrice.toFixed(2);

    var newTotalPriceInput = sellPrice * newQuantityInput;
    totalPriceElementInput.value = newTotalPriceInput.toFixed(2);

    // Deduct or add to grand_price_hidden based on change
    var deduction = Math.abs(change * sellPrice);
    if (change < 0 && newQuantity < currentQuantity) {
        // If reducing quantity, deduct from grand_price_hidden
        var grandPriceInput = parseFloat(grandPriceElementInput.value.replace('RM', ''));
        var newGrandPriceInput = Math.max(0, grandPriceInput - deduction);
        grandPriceElementInput.value = newGrandPriceInput.toFixed(2);

        var grandPriceInput_hidden = parseFloat(grandPriceElementInput_hidden.innerText.replace('RM', ''));
        var newGrandPriceInput_hidden = Math.max(0, grandPriceInput_hidden - deduction);
        grandPriceElementInput_hidden.innerText = 'RM' + newGrandPriceInput_hidden.toFixed(2);
    } else if (change > 0) {
        // If increasing quantity, add to grand_price_hidden
        var grandPriceInput = parseFloat(grandPriceElementInput.value.replace('RM', ''));
        var newGrandPriceInput = grandPriceInput + deduction;
        grandPriceElementInput.value = newGrandPriceInput.toFixed(2);

        var grandPriceInput_hidden = parseFloat(grandPriceElementInput_hidden.innerText.replace('RM', ''));
        var newGrandPriceInput_hidden = grandPriceInput_hidden + deduction;
        grandPriceElementInput_hidden.innerText = 'RM' + newGrandPriceInput_hidden.toFixed(2);
    }
}


</script>

<script src="{{ asset('js/bookMgmt/bookMarketPlace/bookMarket.js') }}" ></script>
@endsection