@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Book Marketplace')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .contentUpdate{
      color: #656565;
      font-size: 20px;
      font-family: Rajdhani;
      font-weight: 700;
      line-height: 28px;
      word-wrap: break-word;
    }

    .col-md-2-5 {
        flex: 0 0 23%;
        max-width: 23%;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
  color: var(--gray-950, #0C111D);
  width: 300px;
  font-family: Inter;
  font-size: 24px;
  font-style: normal;
  font-weight: 600;
  line-height: 32px; 
}

.form-group {
    display: flex;
    align-items: center; /* Vertical alignment */
}

.form-row {
    display: flex;
    align-items: center;
    margin-top: 5%;
}

label {
    width: 100%; /* Adjust as needed for your design */
    text-align: right; /* Adjust for alignment (left, right, center) */
}

.image-cap{
    max-width: 290px;
    max-height: 590px;
}

.btn-tertiary{
    background-color: #FFFFFF;
    color: #007BFF;
    border-color: #007BFF;
    font-family: Rajdhani;
}

.btn-primary{
    font-family: Rajdhani;
}
</style>
@endsection

@section('content')
<!-- Navbar -->
<!-- <nav class="navbar navbar-expand-lg"> -->


<!-- End Navbar -->

<main class="col-md-12" style="padding: 50px;">
<section class="header-top-margin">

  <h2 class="page-title" style="text-align: left;">Marketplace</h2>

    <div class="card mt-7" style="padding: 50px; margin: 50px;">
    <!-- <form method="POST" action="/submit">
    @csrf -->
      <div class="form-group" style="display: flex; justify-content: space-between; margin-bottom: 3%;">
          <div class="card col-md-2-5" style="padding: 30px; background-color: #fbf9f9;">
              <div>
                  <img class="image-cap" src="{{ asset('images/book_cover/book_cover1.png') }}" style="width: 100%; height: 100%;">
                  <label class="contentUpdate" style="margin-top: 5%;">RM 70.00</label>
                  <div class="form-row" style="justify-content: space-between;">
                      <button class="btn btn-tertiary">Details</button>
                      <button class="btn btn-primary">Add to Cart</button>
                  </div>
              </div>
          </div>

          <div class="card col-md-2-5" style="padding: 30px; background-color: #fbf9f9;">
              <div>
                  <img class="image-cap" src="{{ asset('images/book_cover/book_cover2.png') }}" style="width: 100%; height: 100%;">
                  <label class="contentUpdate" style="margin-top: 5%;">RM 70.00</label>
                  <div class="form-row" style="justify-content: space-between;">
                      <button class="btn btn-tertiary">Details</button>
                      <button class="btn btn-primary">Add to Cart</button>
                  </div>
              </div>
          </div>

          <div class="card col-md-2-5" style="padding: 30px; background-color: #fbf9f9;">
              <div>
                  <img class="image-cap" src="{{ asset('images/book_cover/book_cover3.png') }}" style="width: 100%; height: 100%;">
                  <label class="contentUpdate" style="margin-top: 5%;">RM 70.00</label>
                  <div class="form-row" style="justify-content: space-between;">
                      <button class="btn btn-tertiary">Details</button>
                      <button class="btn btn-primary">Add to Cart</button>
                  </div>
              </div>
          </div>

          <div class="card col-md-2-5" style="padding: 30px; background-color: #fbf9f9;">
              <div>
                  <img class="image-cap" src="{{ asset('images/book_cover/book_cover4.png') }}" style="width: 100%; height: 100%;">
                  <label class="contentUpdate" style="margin-top: 5%;">RM 70.00</label>
                  <div class="form-row" style="justify-content: space-between;">
                      <button class="btn btn-tertiary">Details</button>
                      <button class="btn btn-primary">Add to Cart</button>
                  </div>
              </div>
          </div>
      </div>

      <div class="form-group" style="display: flex; justify-content: space-between; margin-bottom: 3%;">
          <div class="card col-md-2-5" style="padding: 30px; background-color: #fbf9f9;">
              <div>
                  <img class="image-cap" src="{{ asset('images/book_cover/book_cover4.png') }}" style="width: 100%; height: 100%;">
                  <label class="contentUpdate" style="margin-top: 5%;">RM 70.00</label>
                  <div class="form-row" style="justify-content: space-between;">
                      <button class="btn btn-tertiary">Details</button>
                      <button class="btn btn-primary">Add to Cart</button>
                  </div>
              </div>
          </div>

          <div class="card col-md-2-5" style="padding: 30px; background-color: #fbf9f9;">
              <div>
                  <img class="image-cap" src="{{ asset('images/book_cover/book_cover3.png') }}" style="width: 100%; height: 100%;">
                  <label class="contentUpdate" style="margin-top: 5%;">RM 70.00</label>
                  <div class="form-row" style="justify-content: space-between;">
                      <button class="btn btn-tertiary">Details</button>
                      <button class="btn btn-primary">Add to Cart</button>
                  </div>
              </div>
          </div>

          <div class="card col-md-2-5" style="padding: 30px; background-color: #fbf9f9;">
              <div>
                  <img class="image-cap" src="{{ asset('images/book_cover/book_cover2.png') }}" style="width: 100%; height: 100%;">
                  <label class="contentUpdate" style="margin-top: 5%;">RM 70.00</label>
                  <div class="form-row" style="justify-content: space-between;">
                      <button class="btn btn-tertiary">Details</button>
                      <button class="btn btn-primary">Add to Cart</button>
                  </div>
              </div>
          </div>

          <div class="card col-md-2-5" style="padding: 30px; background-color: #fbf9f9;">
              <div>
                  <img class="image-cap" src="{{ asset('images/book_cover/book_cover1.png') }}" style="width: 100%; height: 100%;">
                  <label class="contentUpdate" style="margin-top: 5%;">RM 70.00</label>
                  <div class="form-row" style="justify-content: space-between;">
                      <button class="btn btn-tertiary">Details</button>
                      <button class="btn btn-primary">Add to Cart</button>
                  </div>
              </div>
          </div>
      </div>

      <div class="form-group" style="display: flex; justify-content: space-between; margin-bottom: 3%;">
          <div class="card col-md-2-5" style="padding: 30px; background-color: #fbf9f9;">
              <div>
                  <img class="image-cap" src="{{ asset('images/book_cover/book_cover1.png') }}" style="width: 100%; height: 100%;">
                  <label class="contentUpdate" style="margin-top: 5%;">RM 70.00</label>
                  <div class="form-row" style="justify-content: space-between;">
                      <button class="btn btn-tertiary">Details</button>
                      <button class="btn btn-primary">Add to Cart</button>
                  </div>
              </div>
          </div>

          <div class="card col-md-2-5" style="padding: 30px; background-color: #fbf9f9;">
              <div>
                  <img class="image-cap" src="{{ asset('images/book_cover/book_cover2.png') }}" style="width: 100%; height: 100%;">
                  <label class="contentUpdate" style="margin-top: 5%;">RM 70.00</label>
                  <div class="form-row" style="justify-content: space-between;">
                      <button class="btn btn-tertiary">Details</button>
                      <button class="btn btn-primary">Add to Cart</button>
                  </div>
              </div>
          </div>

          <div class="card col-md-2-5" style="padding: 30px; background-color: #fbf9f9;">
              <div>
                  <img class="image-cap" src="{{ asset('images/book_cover/book_cover3.png') }}" style="width: 100%; height: 100%;">
                  <label class="contentUpdate" style="margin-top: 5%;">RM 70.00</label>
                  <div class="form-row" style="justify-content: space-between;">
                      <button class="btn btn-tertiary">Details</button>
                      <button class="btn btn-primary">Add to Cart</button>
                  </div>
              </div>
          </div>

          <div class="card col-md-2-5" style="padding: 30px; background-color: #fbf9f9;">
              <div>
                  <img class="image-cap" src="{{ asset('images/book_cover/book_cover4.png') }}" style="width: 100%; height: 100%;">
                  <label class="contentUpdate" style="margin-top: 5%;">RM 70.00</label>
                  <div class="form-row" style="justify-content: space-between;">
                      <button class="btn btn-tertiary">Details</button>
                      <button class="btn btn-primary">Add to Cart</button>
                  </div>
              </div>
          </div>
      </div>

    </div>

</section>
</main>
@endsection

@section('vendor-scripts')

@endsection

@section('page-scripts')
<script>
let host = "{{ env('API_SERVER') }}";
let userID = "{{ $userid }}";

document.getElementById('profile_image').addEventListener('change', function() {
            const selectedFile = this.files[0];
            const previewImage = document.getElementById('previewImage');

            if (selectedFile) {
                // Display the selected image in the preview element
                previewImage.src = URL.createObjectURL(selectedFile);
            }
        });

</script>

<script src="{{ asset('js/bookMgmt/bookMarketPlace/bookMarket.js') }}" ></script>
@endsection
