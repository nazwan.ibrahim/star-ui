@extends('layouts.contentLayoutLogin')
{{-- page Title --}}
@section('title','Book Marketplace')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />

{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .contentUpdate{
      color: #656565;
      font-size: 20px;
      font-family: Rajdhani;
      font-weight: 700;
      line-height: 28px;
      word-wrap: break-word;
    }

    .col-md-2-5 {
        flex: 0 0 23%;
        max-width: 23%;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
  color: var(--gray-950, #0C111D);
  width: 300px;
  font-family: Inter;
  font-size: 24px;
  font-style: normal;
  font-weight: 600;
  line-height: 32px; 
}

.form-group {
    display: flex;
    align-items: center; /* Vertical alignment */
}

.form-row {
    display: flex;
    align-items: center;
    margin-top: 5%;
}

label {
    width: 100%; /* Adjust as needed for your design */
    text-align: right; /* Adjust for alignment (left, right, center) */
}

.image-cap{
    max-width: 290px;
    max-height: 590px;
}

.btn-tertiary{
    background-color: #FFFFFF;
    color: #007BFF;
    border-color: #007BFF;
    font-family: Rajdhani;
}

.btn-primary{
    font-family: Rajdhani;
}

.cartno{
    color: #F04438;
    font-family: Open Sans;
    font-size: 13px;
    font-style: normal;
    font-weight: 600;
    line-height: -31px;
    margin-top: -10px;
    margin-left: 10px;
}
</style>
@endsection

@section('content')
<!-- Navbar -->
<!-- <nav class="navbar navbar-expand-lg"> -->


<!-- End Navbar -->

<main class="col-md-12" style="padding: 50px;">
<section class="header-top-margin">

  <h2 class="page-title" style="text-align: left;">Marketplace</h2>
  <div id="" style="width: 28px; float:right;
  height: 28px;display: flex;
flex-direction: column;
justify-content: center;
flex-shrink: 0;
">


 
    {{-- <i class="fas fa-shopping-cart fa-lg btn btn-light" style="color: #598ee8; width:40px;height:80px;"></i>
   
    <span id="cart-count" class="cartno" style="">0</span></div> --}}
</div>
    {{-- <div class="card mt-7" style="padding: 50px; margin: 50px;"> --}}
        <div class="card" width="50%" style="padding: 10px; margin: 10px;">
    <!-- <form method="POST" action="/submit">
    @csrf -->







    {{-- <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28" fill="none">
        <path d="M0 2.625C0 2.39294 0.0921873 2.17038 0.256282 2.00628C0.420376 1.84219 0.642936 1.75 0.875 1.75H3.5C3.69518 1.75005 3.88474 1.81536 4.03853 1.93554C4.19232 2.05572 4.30152 2.22387 4.34875 2.41325L5.0575 5.25H25.375C25.5035 5.25012 25.6304 5.27853 25.7466 5.33321C25.8629 5.3879 25.9657 5.46751 26.0477 5.56641C26.1297 5.6653 26.1889 5.78104 26.2212 5.90541C26.2534 6.02977 26.2579 6.15971 26.2343 6.286L23.6093 20.286C23.5717 20.4865 23.4653 20.6676 23.3084 20.798C23.1515 20.9284 22.954 20.9998 22.75 21H7C6.79601 20.9998 6.59849 20.9284 6.44159 20.798C6.2847 20.6676 6.17829 20.4865 6.14075 20.286L3.5175 6.31225L2.8175 3.5H0.875C0.642936 3.5 0.420376 3.40781 0.256282 3.24372C0.0921873 3.07962 0 2.85706 0 2.625ZM8.75 21C7.82174 21 6.9315 21.3687 6.27513 22.0251C5.61875 22.6815 5.25 23.5717 5.25 24.5C5.25 25.4283 5.61875 26.3185 6.27513 26.9749C6.9315 27.6313 7.82174 28 8.75 28C9.67826 28 10.5685 27.6313 11.2249 26.9749C11.8813 26.3185 12.25 25.4283 12.25 24.5C12.25 23.5717 11.8813 22.6815 11.2249 22.0251C10.5685 21.3687 9.67826 21 8.75 21ZM21 21C20.0717 21 19.1815 21.3687 18.5251 22.0251C17.8688 22.6815 17.5 23.5717 17.5 24.5C17.5 25.4283 17.8688 26.3185 18.5251 26.9749C19.1815 27.6313 20.0717 28 21 28C21.9283 28 22.8185 27.6313 23.4749 26.9749C24.1313 26.3185 24.5 25.4283 24.5 24.5C24.5 23.5717 24.1313 22.6815 23.4749 22.0251C22.8185 21.3687 21.9283 21 21 21ZM8.75 22.75C9.21413 22.75 9.65925 22.9344 9.98744 23.2626C10.3156 23.5908 10.5 24.0359 10.5 24.5C10.5 24.9641 10.3156 25.4092 9.98744 25.7374C9.65925 26.0656 9.21413 26.25 8.75 26.25C8.28587 26.25 7.84075 26.0656 7.51256 25.7374C7.18438 25.4092 7 24.9641 7 24.5C7 24.0359 7.18438 23.5908 7.51256 23.2626C7.84075 22.9344 8.28587 22.75 8.75 22.75ZM21 22.75C21.4641 22.75 21.9093 22.9344 22.2374 23.2626C22.5656 23.5908 22.75 24.0359 22.75 24.5C22.75 24.9641 22.5656 25.4092 22.2374 25.7374C21.9093 26.0656 21.4641 26.25 21 26.25C20.5359 26.25 20.0908 26.0656 19.7626 25.7374C19.4344 25.4092 19.25 24.9641 19.25 24.5C19.25 24.0359 19.4344 23.5908 19.7626 23.2626C20.0908 22.9344 20.5359 22.75 21 22.75Z" fill="white"/>
        </svg> --}}
    


    <table width="70%">
         @foreach ($bookdetail as $product_d)
        <tr>
         
                <td>
                    
                  
                    <div style="display: flex; justify-content: space-between;margin-top:20px;">
                    <div class="form-group card col" style="display: flex; justify-content: space-between;background-color: #fbf9f9;;width: 30%; height: 30%;">
                        <div style="background-color: #fbf9f9;margin:2%;padding:2%;">
                            <img src="data:image/{{$product_d->file_format}};base64,{{$product_d->file_cover}}" width="50%">
                           
                        </div>
                    </div>
                    
                </div>
                </td>
                <td>
                    <div style="margin-top: 2px;margin-left:20px;margin-bottom:2px;margin-right:2px;font-size:30px;text-align:left">{{ $product_d->book_title }}</div>
                    <div style="margin-top: 2px;margin-left:20px;margin-bottom:2px;margin-right:2px;font-size:20px;text-align:left">{{ $product_d->book_description}}</div>
                    
                    
                    <div class="form-row">
                        <div class="contentUpdate" style="margin-top: 2px;margin-left:27px;font-size:20px;text-align:left">RM{{ $product_d->online_price }}</div>
                        
                        {{-- <div><button class="btn btn-primary">Add to Cart</button></div> --}}
                        {{-- <button class="add-to-cart btn btn-tertiary" data-product-id="{{ $product_d->id }}">Add to Cart</button> --}}
                        {{-- <button class="add-to-cart btn btn-primary">Buy</button> --}}
                    </div>
                    {{-- <div style=""><button class="btn btn-secondary">Back</button></div> --}}
                  
                    <div class="contentUpdate" style="margin-top: 2px;margin-left:27px;font-size:20px;text-align:left"><a href="{{route('list-book-public')}}" class="btn btn-secondary">Back</a></div>
                </td>
               
        
        </tr>
        @endforeach
    </table>

    
   
        
    {{-- </div> --}}

</section>
</main>
@endsection

@section('vendor-scripts')

@endsection

@section('page-scripts')
<script>
let host = "{{ env('API_SERVER') }}";


document.getElementById('profile_image').addEventListener('change', function() {
            const selectedFile = this.files[0];
            const previewImage = document.getElementById('previewImage');

            if (selectedFile) {
                // Display the selected image in the preview element
                previewImage.src = URL.createObjectURL(selectedFile);
            }
        });

</script>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        // Find all elements with the class 'add-to-cart'
        var addToCartButtons = document.querySelectorAll('.add-to-cart');

        // Attach a click event listener to each button
        addToCartButtons.forEach(function (button) {
            button.addEventListener('click', function () {
                // Get the product ID from the 'data-product-id' attribute
                var productId = this.getAttribute('data-product-id');

                // Call a function to add the product to the cart
                addToCart(productId);
            });
        });

        // Function to add the product to the cart
        function addToCart(productId) {
            // Perform the logic to add the product to the cart (could involve AJAX, local storage, etc.)
            console.log('Product added to cart with ID:', productId);
            // You can implement further logic here, such as updating a cart icon, showing a success message, etc.
        }
    });
</script>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var addToCartButtons = document.querySelectorAll('.add-to-cart');
        var cartCountElement = document.getElementById('cart-count');
        var cartCount = 0;

        addToCartButtons.forEach(function (button) {
            button.addEventListener('click', function () {
                var productId = this.getAttribute('data-product-id');
                addToCart(productId);
            });
        });

        function addToCart(productId) {
            // Your logic to add the product to the cart goes here
            // For this example, we're just incrementing the cart count
            cartCount++;
            updateCartCount();
        }

        function updateCartCount() {
            // Update the cart count in the UI
            cartCountElement.innerText = cartCount;
        }
    });
</script>

<script src="{{ asset('js/bookMgmt/bookMarketPlace/bookMarket.js') }}" ></script>
@endsection