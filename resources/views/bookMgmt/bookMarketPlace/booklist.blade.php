@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Book Marketplace')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .contentUpdate{
      color: #656565;
      font-size: 20px;
      font-family: Rajdhani;
      font-weight: 700;
      line-height: 28px;
      word-wrap: break-word;
    }

    .outOfStock{
      color: #eb1212;
      font-size: 20px;
      font-family: Rajdhani;
      font-weight: 700;
      line-height: 28px;
      word-wrap: break-word;
    }

    .col-md-2-5 {
        flex: 0 0 23%;
        max-width: 23%;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
  color: var(--gray-950, #0C111D);
  width: 300px;
  font-family: Inter;
  font-size: 24px;
  font-style: normal;
  font-weight: 600;
  line-height: 32px; 
}

.form-group {
    display: flex;
    align-items: center; /* Vertical alignment */
}

.form-row {
    display: flex;
    align-items: center;
    margin-top: 5%;
}

label {
    width: 100%; /* Adjust as needed for your design */
    text-align: right; /* Adjust for alignment (left, right, center) */
}

.image-cap{
    max-width: 290px;
    max-height: 590px;
}

.btn-tertiary{
    background-color: #FFFFFF;
    color: #007BFF;
    border-color: #007BFF;
    font-family: Rajdhani;
}

.btn-primary{
    font-family: Rajdhani;
}

.cartno{
    color: #F04438;
    font-family: Open Sans;
    font-size: 13px;
    font-style: normal;
    font-weight: 600;
    line-height: -31px;
    margin-top: -10px;
    margin-left: 10px;
}

.cartposition{
    position: right;
}
.details-container {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border: 1px solid #ccc;
    background-color: #fff;
    padding: 20px;
    z-index: 1000;
}

.popup {
    display: none;
    position: fixed;
    bottom: 10px;
    right: 10px;
    background-color: #dc3545;
    color: #fff6f6;
    padding: 16px;
    border: 1px solid #d4d4d4;
    border-radius: 5px;
}

.popup p {
    margin: 0;
}

.close {
    float: right;
    font-size: 20px;
    font-weight: bold;
    cursor: pointer;
}
</style>
@endsection

@section('content')
<!-- Navbar -->
<!-- <nav class="navbar navbar-expand-lg"> -->


<!-- End Navbar -->

<main class="col-md-12" style="padding: 50px;">
<section class="header-top-margin">

  <h2 class="page-title" style="text-align: left;">Marketplace</h2>

    {{-- <div class="card mt-7" style="padding: 50px; margin: 50px;"> --}}
        <div class="card mt-7" style="padding: 10px; margin: 10px;">
    <!-- <form method="POST" action="/submit">
    @csrf -->







    

  
    <table >
    <tr>
        <td colspan="4" style="text-align: right;">
            <div id="cart" style="display: flex; align-items: center; float: right;">
                <form id="cart-form" method="POST" action="{{ route('shopping-cart') }}">
                    @csrf
                    <button type="submit" class="btn btn-link" onclick="submitCartForm()" style="display: flex; align-items: center;">
                        <i class="fas fa-shopping-cart fa-lg" style="color: #598ee8;"></i>
                        <span id="cart-count" class="cartno">0</span>
                    </button>
                    <input type="hidden" name="cart_items" id="cartItemsInput" value="">

                     <!-- Add a reset icon button -->
                    <button type="button" class="btn btn-link" onclick="resetCart()" style="color: #598ee8;">
                        <i class="fas fa-sync">clear the cart</i>
                    </button>
                </form>
            </div>
        </td>
    </tr>
        <tr>
            @php $counter = 0; @endphp
            @foreach ($booklist as $product)
                @if ($counter === 4)
                    </tr><tr>
                    @php $counter = 0; @endphp
                @endif
                <td>
                    
                    
<div style="display: flex; justify-content: space-between; margin-top: 20px;">
    <div class="form-group card col" style="display: flex; justify-content: space-between; background-color: #fbf9f9; width: 100%; height: 100%;">
        @if ($product->stock_balance !== 0)
            <div style="background-color: #fbf9f9; margin: 2%; padding: 2%;">
                <img src="data:image/{{$product->file_format}};base64,{{$product->file_cover}}" style="width: 290px; height: 293px;">
                <div class="contentUpdate" style="margin-top: 2px; margin-left: -20px;">RM{{ $product->online_price }}</div>
                <span style="margin-top: 2px; margin-left: 2px; margin-bottom: 2px; margin-right: 2px; font-size: 12px; word-wrap: break-word;">{{ $product->book_title }}</span>
                <div class="form-row" style="justify-content: space-between; padding: 30px;">
                    <a href="{{route('book-detail',$product->id)}}" class="btn btn-tertiary">Details</a>
                    <button class="add-to-cart btn btn-primary" data-product-id="{{ $product->id }}">Add to Cart</button>
                </div>
            </div>
        @else
            <div style="background-color: #fbf9f9; margin: 2%; padding: 2%;">
                <img src="data:image/{{$product->file_format}};base64,{{$product->file_cover}}" style="width: 290px; height: 293px;">
                <div class="contentUpdate" style="margin-top: 2px; margin-left: -20px;">RM{{ $product->online_price }}</div>
                <span style="margin-top: 2px; margin-left: 2px; margin-bottom: 2px; margin-right: 2px; font-size: 12px; word-wrap: break-word;">{{ $product->book_title }}</span>
                <div class="form-row" style="justify-content: space-between; padding: 30px;">
                    <a href="{{route('book-detail',$product->id)}}" class="btn btn-tertiary">Details</a>
                    <div class="outOfStock" style="color:#F04438">Out of stock</div>
                </div>
            </div>
        @endif
    </div>
</div>


                </td>
                @php $counter++; @endphp
            @endforeach
        
        </tr>
    </table>

   


    <div id="popup" class="popup">
        <span class="close" onclick="closePopup()">&times;</span>
        <p>Item successfully added to the cart!</p>
    </div>

</section>
</main>
@endsection

@section('vendor-scripts')

@endsection

@section('page-scripts')
<script>
let host = "{{ env('API_SERVER') }}";
let userID = "{{ $userid }}";


</script>



<script>
    
    function submitCartForm() {
        // Retrieve cart data from localStorage
        var shoppingCart = JSON.parse(localStorage.getItem('shoppingCart')) || [];

        // Set the value of a hidden input in the form
        document.getElementById('cartItemsInput').value = JSON.stringify(shoppingCart);

        // Submit the form
        document.getElementById('cartForm').submit();
    }

    document.addEventListener('DOMContentLoaded', function () {
       // showPopup();
        var addToCartButtons = document.querySelectorAll('.add-to-cart');
        var cartCountElement = document.getElementById('cart-count');
        var cartCount = 0;

       
        // Retrieve cart data from localStorage on page load
        var savedCart = localStorage.getItem('shoppingCart');
        
        if (savedCart) {
            cartCount = JSON.parse(savedCart).length;
            updateCartCount();
        }

        addToCartButtons.forEach(function (button) {
            button.addEventListener('click', function () {
                var productId = this.getAttribute('data-product-id');
                addToCart(productId);
                showPopup();
            });
        });

       
            function addToCart(productId) {
            var shoppingCart = JSON.parse(localStorage.getItem('shoppingCart')) || [];
            shoppingCart.push(productId);

            //console.log('allsaveitem',shoppingCart);

            // Save the updated cart data to localStorage
            localStorage.setItem('shoppingCart', JSON.stringify(shoppingCart));
           // localStorage.setItem('lastAddToCartTime', Date.now());

            // Update the cart count and display
            cartCount = shoppingCart.length;
           
            updateCartCount();

          
        }

       

        // function updateCartCount() {
        //     // Update the cart count in the UI
        //     cartCountElement.innerText = cartCount;
        // }

        function updateCartCount() {
       
            cartCountElement.innerText = cartCount;
            //alert(cartCount);
            //    localStorage.removeItem('shoppingCart'); 

            if(cartCount!='' > 600000)// 60 000 = 1 minit
            {
                setTimeout(function () {
                location.reload();
                localStorage.removeItem('shoppingCart'); 
                    }, 600000);
                
            
            }


        }
    });

        function resetCart() {
            // Reset the cart count
            document.getElementById('cart-count').innerText = '0';

            // Reset the input field value
            document.getElementById('cartItemsInput').value = '';

            // Remove 'shoppingCart' from localStorage
            localStorage.removeItem('shoppingCart');
        }

    

function showPopup() {
    var popup = document.getElementById('popup');
    popup.style.display = 'block';

    // Set a timeout to hide the popup after 3000 milliseconds (adjust as needed)
    setTimeout(function () {
        popup.style.display = 'none';
    }, 1000);
}

function closePopup() {
    var popup = document.getElementById('popup');
    popup.style.display = 'none';
}

// Event listener for the button click
document.getElementById('viewDetailsBtn').addEventListener('click', openPopup);
</script>



<script src="{{ asset('js/bookMgmt/bookMarketPlace/bookMarket.js') }}" ></script>
@endsection
