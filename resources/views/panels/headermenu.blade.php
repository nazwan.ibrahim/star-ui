<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<style>
    /* .layout{
    width: 91px;
    height: 91px;
    } */
    .layout_2{       
        padding: 0px 40px;
        justify-content: space-between;
        align-self: stretch;
    }

    .position{
        position: absolute;
        left: 40px;
        bottom: -35px;
  
    }


    .leftmenu{
        color: white;
        font-family: Rajdhani;
        font-size: 18px;
        font-style: normal;
        /* font-weight: 500; */
        line-height: 24px; /* 133.333% */
        justify-content: left; 
        margin-right: 350px;
    
        padding-left:50px;
        text-align: left;
        position: left;
    }

    .leftmenuParticipant{
        color: white;
        font-family: Rajdhani;
        font-size: 18px;
        font-style: normal;
        /* font-weight: 500; */
        line-height: 24px; /* 133.333% */
        justify-content: left; 
        margin-right: 700px;
        padding-left:50px;
        text-align: left;
        position: left;
    }

    .leftmenuTP{
        color: white;
        font-family: Rajdhani;
        font-size: 18px;
        font-style: normal;
        /* font-weight: 500; */
        line-height: 24px; /* 133.333% */
        justify-content: left; 
        margin-right: 320px;
        padding-left:50px;
        text-align: left;
        position: left;
    }

    .leftmenuTrainer{
        color: white;
        font-family: Rajdhani;
        font-size: 18px;
        font-style: normal;
        /* font-weight: 500; */
        line-height: 24px; /* 133.333% */
        justify-content: left; 
        margin-right: 570px;
        padding-left:50px;
        text-align: left;
        position: left;
    }


    .menu{
       padding:10px;
       margin:10px;
      
    }

    .space_menu{
    padding:0 5px 0 5px;
    }

    .user-menu {
        font: 1em sans-serif;
        color: cyan !important;
        font-family: Rajdhani;
        font-weight:bold;
    }

   li {
      list-style-type: none;
    }
    
    .fontsize{
        font-size: 16px;
    }
.logout_location{
    margin-right:-20p
}

@media screen and (max-width: 600px) {
    .logout_location {
        width: 100%; /* Make buttons full width on small screens */
        margin: 10px 0; /* Adjust top and bottom margin */
    }

    .leftmenu{
        width: 100%; /* Make buttons full width on small screens */
        color: white;
        font-family: Rajdhani;
        font-size: 18px;
        font-style: normal;
        /* font-weight: 500; */
        line-height: 24px; /* 133.333% */
        justify-content: left; 
        margin-right: 350px;
    
        padding-left:50px;
        text-align: left;
        position: left;
    }

    .leftmenuTP{
        color: white;
        font-family: Rajdhani;
        font-size: 18px;
        font-style: normal;
        /* font-weight: 500; */
        line-height: 24px; /* 133.333% */
        justify-content: left; 
        margin-right: 320px;
        padding-left:50px;
        text-align: left;
        position: left;
    }

    .leftmenuTrainer{
        color: white;
        font-family: Rajdhani;
        font-size: 18px;
        font-style: normal;
        /* font-weight: 500; */
        line-height: 24px; /* 133.333% */
        justify-content: left; 
        margin-right: 570px;
        padding-left:50px;
        text-align: left;
        position: left;
    }

  
}
    </style>
    <header>
        <div class="container" style="height:50px;">
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-custom public-navbar">
            <!-- <div class="container" style="height:50px;"> -->
            <div class="navbar-star" >
                STAR
            </div>
    

    {{-- <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-custom public-navbar" style=" "> --}}
        {{-- <div class="container" style="height:50px;"> --}}
            <a class="navbar-brand" href="{{ route('admin.dashboard') }}">
                {{-- <div class="col-2 mr-3" id="IcCoverImg"><img src="data:image/" height="120" width="120""></div> --}}
                {{-- <div class="layout position"><img src="{{ asset('images/seda_logo.png') }}"  alt="STAR"></div> --}}
               
                    
            <div class="layout position layout_2">
                <svg width="80" height="80" viewBox="0 0 91 92" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <rect y="0.191406" width="91" height="91" fill="url(#pattern0)"/>
                    <defs>
                    <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                    <use xlink:href="#image0_1363_21406" transform="scale(0.0046729)"/>
                    </pattern>
                    <image id="image0_1363_21406" width="214" height="214" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANYAAADWCAYAAACt43wuAAAACXBIWXMAABcRAAAXEQHKJvM/AAAgAElEQVR4nO29CZSV1ZX2f0TGAoqpEKGYBwUkyAyKA5qoQLeKQxKSmIiZSPiTiMn32X6d7lZ7+ux83RE7LDokHcWOSUji2KbFWTRRESwFBESZh1KBYihQChD0v36n3n0577nnHe97a4B61rqL4o7vcJ6z9372Pvuc9umnn6om1B/mzV/QUSk1wjsA8++k2OI9wJbZs2Zuabqt9YcmYtUB5s1fMMkgTV/vwd8divzr1UqpFUqp/d6/WzzSLWkQF+YkRhOxMoZHohHG49wGeqhbPbLxgGgrZs+aub8BHNdJgSZiFQDPjZvkPSDRxY32ZGqx0iOZfjQRLT2aiJUQ8+YvGOERaVqWRKrc9ZGqOXJM/33oyDH9/zgoP6OtKmnVPPfOgb0y9S4h2qM8Zs+auSLLLz7Z0USsGPDINMMjU5803wFR9hw4XEugw8fUjl0faiLtiEmgpOjcobXqUtqq9t8OrTUBu5TW/psSWz1LBskeLcpBn0RoIlYACiHThu3VmjiQptL7tyGh5xltVfkZ7fS/Pc9ol8bKVXuWbG6TJXOjiVgGvJhphveILTpApPU8tu3X/zZGDOrVoZZkvTvov9sY7mUEsGRzPUvWJPF7aCLWCes0Ryl1Y5z37z1wRK1aX6VJtGr9nuIfYD0Acg0f1EUN6tUxifv4mGfFTnk5/5Qm1rz5C8Q6RYoQxEavrd6p1m/f3+Bcu2KDOO3cgV3U+GHd4pIMK3bH7FkzFzbqEy8ApySxPELdERU7iWWCUKcamYIgJJs0plx1Lm0V9fZqz02ce6pJ96cUseISCiKt2rDnpHXzsgLix6TR5dpljIjJTjmCnRLEikMorBOEeuH1ylw+qZjo0LtctWzTRu1+Z0MdXYXiAVJNGNYtjhU7ZQh2UhNr3vwF07wbGUgoYqcXKio1qYqFFiVtVJ+J41XXsweqssEDVYs2bXy/dGjPXrXhmRfVhmcaf8yP9bpkdHmUhH/Sx2AnJbE8lW9umCiBRP7Ey1vrRB6HUBfe+j3998c1Napq3Qa1f3ulJhyvdehVrl+remeDenXef6qPD9UU/ZiKDVRFLNjwgV3CfonKjjkno4p4UhHLy0Ph8t0c9J66JJQAt++zt9+qLdOTt96Z93qPkZ9Ro7/xFW3JNjz7olr124fz3lNS1lm17dJZ/w0p64J8HHf1tsqCvgOx46tTzoqyYI95BDtp8mAnDbG8OGpu0FIMXL6Hnt9Ybwnca395j/734W+4OQ+5Jsz+pv77yb+6Ux2q2pt7bfiXrlUDP+c3vm//95Pq7ccW678vuvV7mngVv/x1ZjEb1vTKn96lLezjs28r+PuwYFMn9gkjWLXnHs4t+McaAGKn1xsq5s1fwNqmhUFuH6IEFqqYMVQcMECxSBDAJI3gvTffUtXbK7Vb2GPk8Fy8hSXrc/447SbyXIuSEjX8S9eoIVdNVtXbdujPYcHKzh6ouo8ankesyT++XZV06awq7v2N2vrya7GPl2MArmNNAya0exat0gS7YerZLpEDxt0tucXGXirVrAEcQ2rMm79gjreeKI9UKHuLX9mm7lr4Rr2TCohLJe6cC5BEeTGZ8lwxSIUL+dKPf6pfhxxLf/qf+vUhV0/R/1atW6//7ejFagIEE0jF54VUfPd5s7+pLeiV8+7SxOV3bGBBwdaXl2V6HSDY7QuWqYef3xSkvlJK9ua8+QvuyPSH6xiN0mJ5sdSjQVaKOOpXi99Ve6sP19kxMYgHXnaxjo9c7thHe/aqMk2WnoHuGhZIeW6YMqzG24896Xsfn8cCiugh31fmEVIw5OrJvs9zjKO//mX99/tvvlWrVp4/TpMI4prxVHePWLvfWZ/+ooQAJXbp6p3qukv764oOB273VN1pjTH2anTE8i72QlcsxQz4wOJ36yWx22fiuNxAd0FcKiGNC7YgIRYIMgghgj4nbqQIDlgm01rhguJCgufu/HGOREK2oVdN0YqkMqwVny1UvAiD3C88igD3EOu1As+ksUnzjYpY8+YvmBuk+FEp8cAT7xY1uQspsCIMXpdyFwYhlu2umcCaKYNgQsKtryyLjHWwWhCr69mDNBnETRRrNfCySTrGw1KZ3wXpsGxioQCxmjJc02ID9xCXferE3rqSw74sSqn7vJYHcxpLYrlREMtz/Za4lnLUhZUyrYBYDgZnkoTuoara4wuzWD1y7letayeiBL8d9VvEWSiHHKtORJ89UAseZmylPBcPtQ8Lx+/wOU1kI910Ir7yix1SLYJbm5WoIeA+PvT8Jn0fA6wXKw9GIG40BmGjwYsX3ky1xUUqrNTtC5YXlVQE9+SgcKWYwZG5wfDp1ziD/qD4CZIoRxwkYDDzGrGTDGgRJYjdwghp/i7iSO8Lxuu/RY4H4qYunfefOlemv/dzF2uJ33RhOQ4sm+kGcp6oi1wHEt2T/+V2LfG7zr9QiPUKEJwYA0u8cKBBo0FbLE96vc/1GqoSAXDWkPIjmbVlQONK4f4xWHHnmPlR156748exkrVh7+G7TaFB3mtK8J+941Zd9oTAwTHhrmHFZPBLnAU5y7wqDhfJ+U5x8aTyo2zwoNzr8rd8FvJAIiEbKiGTDKIHRPvTj3+aeb2jeCGQDHHDKvDFNXxk3vwFd86eNbPBKoen33FHwzy2efMXEKzebj9PXurfF63S1iprMNAYRAgRLdq2UUeqD6rO/fvqX2nfvZvavORl9cnHx9TO1W+rMz8zRLU/s5sq7d5N7Vj2Rq20XdY5Z9Fc6DFquGrdoVR9XHNYdR7QV/UcP0qN/NoXVM9xo9TpLVqoVYseyXP5+O4uA/pqcnUbNkT/Du+H3Ps2bdVkEvCdEsNV3PtrTQRB18GD9PHx/oMf7NLPci78zfnwABwP12HdY4v1a5f86AeqdWmpjs2W/NPdmrD8zb+6/nHwIE34YoCk/tub96l+PUpVaduW9i9MWra8ot+y5RVLxo0dU3fyb0w0OIvlxVOQ6mr7NWT0nz+ytmgCBRUODGDEAqwTVsCcsRlIDHyeR0HDimC5sDhxIJYIN1KgXb9XlmlL6Ipb+AxSOMeBm4fAgYvoinPef2OVfg+v2VaE48aa6dKp3z7iTBbzGyiJHBNWDast/3/93l/73sv320qk8iYnHlnFYKyDI7EcIMtL3DWpoYkaDcpiGSJFXn5qSUWluu/xderY8U8y+z0GxLDrr9IzMBjzja9oy4F7IyTAavFgkGG1ZHbm9X0bt2iyYUnUabWDKsxiMdgO7dmnZ/ttLy9Tb//3YrXqt4/o349yJzkGLAifxRK53s/ruGrvO9Q8XmvRtkS7fpzLwMsnqTM9C0j8x/efPfUybaF3LH9Tf0fP8aP1+/k/ltNGr3GjtBWsXPZGzjqO/NoX1ZivfyV3nFmAe46HcvjIcTWkXyf7G89USk1Ztrxi6bixYz7I5AczQIMRL7yKdKfyh7+NYpQlcm6flyAFWCUGhD1omd2ZtZm9GYgCZm3cN8BrplvmAu/HMvHgO4uZI3IBK8ykgYXkt7FgpgAh1wHLpwwlMeg4IZX9f66n8lIHSP49DBm/UBBT/+JRp8ciokbavveZo0G4ggapfElfLiDxVJbL4hlIJEMZ5BCJQWa6TUGqHZYAFQ2RwXSjcLGk9KgxLPfYHSBqQIoSr9xKXudfrodLkZQEtDIUTyyVwHR3mXBwJbOYSFCA/716lfr+9OEuUWOJ5xbWuxxf7xarLkmlvDIh4iJuPFaIinAhhLguplUSyHsYTF0t8m37cy3RGvM6KtxUquqXGuvBRO5HzDEB0YjVlJe85v2SO1Ne5f3j37tNfx8uJXGYSbpCwZggzeLoFtyhoViueiVWEKkqvQtXjAYuWBghUL7LV1twilUyZ+laCf7E4LLFipKy2uzq/ghXsKEDcpnVFjqB/M4GPZkg1ODWMenwt4gaUoEiRCNHhqvLteX7EHm43rVV+9m5hUy8iBoNlVz1RqwwUnHBslL+pBJBYgluuAwGBocZJwjpZCBJPkuvd/KWXiijQFUgpJMi2pMJr3oJZYhBMpnKE4knX/JEHiYaIZqZlBbIhCUlW1lByLUhf41dvZOrXlTBuiAVRLrglu+qIVdN0QTpP2mizrlI0lXyOh1798zdePI6zNLkqBgokIqcEbknWc8kuaj3VryllTRmat4HId/81e8LPu6GBsnb4fJhvfZu2qrefeJZtfqhx/X5Y83P+943tZr67uLncvkwE4Mum6QV1SrPAgq4dvyf30gLFEOqNOhPTydfA62VUl9atrziyfpQC+tcvDAk9aKRShQ/WeqOS0JZEDEAzzPTEkxTmsNzWDQJ2AmwqaYgFhNrBqHsvIy4keLeEKudzNCpAkduityfNMdxWWzuBQ10lK76WJV7XhZwZnXtUI6BleuqN0GjTl3BuiCVMiq5CcRx+3DxIJPyauawYLqI1quZk/hAAGkgk0jjMqCk8cshIznLoOB3ToY2ZkkhaqjyEt1SFW9CiGfWHnL95XPKW7BpC0JpIEtQLAi5+tbltakzixVUoZ41qZRHAKkeUN4AEFUKl0bKhiAN8RFuHzfXFR8ob9aFrFg95eWDBFkurUA+7mm0cB7Uu2Po+/VWQIePeX9/VCf9EE2cO/1a/T+uKdCrnb2qf64Z19S2StwLc21ZjmBXTU7cPsAFyIVbaPXW4D+P1mWFRl26gnPrglQCcU+YHWWBn33jaoWMR/SNhjQ2sRgY9JbIvb+mRlunQslE3wfZt0r+jtGuORaopWTlNKTbW31E/1uMBjpcV6nGFxWQOkWul3nNlHfdsei41hd5beCQ4WWCg4QijLBiudByKMrebp4+3O4zf643sdeJoFEnxPIWKPp28oBMP3dn0QuGxD8oe+K6oW6ZCUoGBj4/RCMGcK134nVxUaTfRNJcFVZokLc1Tsq9qBIBgvKwf4dJjA0darcbqi74uks1PiVeQgTcbSy7XDOuFddVJ+MROWZ/U094kjA2r2WVl4ym1rFQYola6CIXxd2zZ82cUdAPxEDR25+5ln5klfyVZQ/kkSCB3BC7t4N9E+V1s4VYlpCNA+gKW2wipQVEq+1PX5XqPiDaYNEReuIAUpGmwMLRW9G8H9xH1nuJ2MRrJKcLjVu5D7fdONLVV/6WYrdZKyqxgmR16r0KXZyIr87NKjG6HpnNLsVamfkWZbh3WLG4a6niIMVWN4Fo27btzubNm+eWQrRp3brZGWd01dXH1QcONNu/v9pXiVxdXZ1q+1aB7KryQsV7RWnAI30RIZXdtEZZCqGJLDoDE7M6yp/ANcXc8rVoxPLEihV23/SsFihKvzwpfJVVsATSBMrmcg/l3SSeM12RLGrXIBIbAiS1TJCnrKzL0U4dO7bs17dPt1atWqmystB2zJGoqtqjjhw5ojZv2bpz3/79RyFgUtJJL3smvqzcdJnMXOKE6V1Q0CySPS4lFk7uZyHgHt0w5Sz7Gwg8RxSrA1QxY6yFNqn0bh4ZkErWDeHmSWchuUGmCoVFQrnqbix7N0tu0oLZ75Ix5fqGxRUdOnXqWNmnd+/mkKi8vAdPOXt+FQIhZnl5D993V1a+pz7YuWvf5s1bPty9e3fZ8U8+CVznj7VlENZcekwtqXhPtygr1IpJNb8dO5k9RFyLPLlvYX0Y40ISyFPO721+ooPXQq8oYkZRiOU10vQtVGQmlCReWkgALDV5Zm2ezIRCLildEuJlAdy9S0b30ISK2qO3ZcuW+/r363uof/9+5f376RRK9g0iYgIil5f36DR61Ai9mAmivfPu+qoNGzedfvTo0bwFTsqbPBiIPBiYT7yyrSCC2aRicpzwvdqW2mYKRFBSoPW2QTdkJg1rkwbEDLYUmpPpjxWjpMmLqx4xn9Mqze/eiu1aQCCWhFMms3fTCUvdf9IFqt+kibnl8hufWZJbZq685QksJCSw5j38HbVGKg4YZJdP6KW+NW2o6tujVLVo7s6rn96sWU3fvn32Tr78c+0uvOD8Nv379yvt1Ck8F1UfKC1tr/r161syetTINv379VOffPJJVfWBA0eOHz/utGSomWzNw6y/Y3fh+TKZIMWVf/nun+W9PvEH39H3Hw/DHAOFgGX+o4ecYU+KE5Ytr1g5buyYdVneikyJZSSBfaPpPx5c7apCDgSrUFmVSw2ZuXKVC1zStUuur4OsqDXBjZL3QDCp6UsLGvnfdOVg18rVHLp07rzzvPHjTp865Yo2Zw0a2K6kpCSrS1p0cKxCsrKyMrV3776dNTU17Vy/KwQ77bTTtJKYdjU3tZbUbuKa00fDrhVkUhXSrVr0cEG1hCY43g3b96sLRnS3X2IF8s+y7J2RtSuYt2si/dOTJCil3EUEBhsSyPIere5V7c0LiM1gN61AEdK8P4d+/fruHD92TLeysi6Zx0v1AVzW/v36djtw8KD6059f2bl58xbneeEeThrdQ6/qTtMXn3tG/EvC2I51dS/5XuW5Phvm67iPfLaQ+JgJAQHt2kv7m09LvBWveUkMZKYKev3/XjCfw0rddX9+r4QgyD5SQbKsCV8RZwalMALchBumnhW6YRqEuvCC87uVtm+fyW82VBw5clRVvPFm9cqVq1oGCR5Z9slHCZSVx1LhAvnM0ijlKbwrFz1ckKr7rWuGuu5xZvmtTIjlktbxw++6/83YF9xMErqIIoWypjUScsUhYhyQ0EURCxImThVC2YBgzz7/QqAF0zu7vLytIMXX3B9MFEIzZWLWfsqKgkLuOff4zplj7XudmQSfVXV7vgv4cjIVySx3sUnFTAaBiJvMlb2QjPfzuUI6s3JxmcEQJ1ykIoa65uor1V9MueKUIxVo1aqlPvevffXLSPpV9utcM1wrSohQTtNAGoWKQih1hZKHhFTcbx4QiuelCDgNpCmohQ5emqhgFCxeeC7gf5jP4R4seiZZOQryKnkNAltZkEjQKhUWzFh6mbcl2yJuyMLET44dcy60CwOZ+Zu/dK7q2z2fMKeffvrhSy6+6ONLL7m4I0raqQ6S2MPOGVrSpXPnQ9t3VB46fvy4j0WQimT5zr01+pEE3DcmSRZRKq9aA2WXXOWHH+zSC05F5UWMYoEqAldYu7kocIw9u7VT3Tr7xKa+WaiEBRNr2fKKR73ebhrMBPMfXJNYkq3tQ7dPdR08UHeYhSwfvr9TTfzBd7XsunzB/XkKINYLK/bO4mc1qdY8+Hii30ThwlK5rFR5eY+qL1x/XWn37t1aJPrSUwCdO3dqMeycoa0PHz5ctbuqyjcqSUWMHtxVK4dJq+rN1Mm5rOMqaaNenvuz3ORJqkVU3rOnfk5PwoUQS3kSPCqhlUIpWCUsiFjerntfNJ97/KUt+mDTgNmImavX+FG5Hn6QiotHe2cb42fOUOdcf6XaxdLxhDsPEktdNr5X/gVp1qxm8uWf+/j88yZ0aN789NTX5mQH1waZvrxHD7V+w8bDn376qW920tX83drpsZBGlh8+/Vpdz0kvezwXUi2MC8YE/2cdHa8X2t6aY8NyMRkYwBK3GTd2TGrWpo6xPMHCl7GWOrNCIEvjzcSuaxkBShElL7wvSXckrNNtN45y7iLYvn37vV/5yvQ2Awb0bzyJqHoGVR1fn/G11lh4+0hQ3dLGXTof2aVzLqZmXLBFLLGVKIf2TpdpQV2koyHNzYWsOk5tsTCVGA3zuYWPr9OV0oWCPAXmnw0HaEKit5ZpW5KLn2przL6SUwPjJoC5wf/f9cOc1edDhwyuuubqK7sQRzQhGbBeQwafXdKiRYvD27fv8FkuNjMg7sJyHfjo49jfi1XivsumE8rr+8iGEsRW0u9eGY2D+EzaShvc1kvyN70bOW7smFRiRipieWVLPsGCRCFFm1lBqi6kioJAloD14Acn4q5X7v5Z7AuJSPHDG0bkzZ7NmjU7dMnFFx0bN25Mw1w41YjQ/cwzm2vXcP2Gmk8//TQXm+q4a0jXROTivtIRi+Jp7juTqriEpsiBYkzPfaxby5I2qTcjRxMgLhzkX6WAkPHiuLFjEsvvqYi1bHnFIn7UPKj7/vhOUVYD6609vfo/rBelMBJ3xb2IQWtyiKe+cP21JX369G4SKDKCrkPs27fFlq3bqo4ePZpzqdOQS0QLyGVaLkQO5HjU4n7eeFAeGY9UH0i9GQNVGY5awkvGjR2TOGmcmFievO7bt+rZZTuKuqsiFwxFkBmMiwjZ4vbwCyIV8dQXvnBdgyySbeyg/nDI4MEl6zds3Hv06NFc4jEpucRrOb1lC7WNZSceYbBS475zo1aPlacoQzQmXt2BK2XxNUIGxoFCAQMdly2v2Dpu7JhE7dMSE2vZ8oqFedYq4+11XOCi4g606VCqa8jiFGaGkWr6F67v3LYRFcs2NnhxV5udu3ZVHTx4MLXlkoahukOxZaV0Zfzcn2m3kCr4IwcO1m7oN25Urqd8UiDATfjMmfaYGZnUaiUilsta/e6ZDWrL++mrxxEi2KuJvARJ4qC9n5S3RxQzWBxSEUsRUwWRimqCJhQXImrs2btv7759+1JbLuXthkK7BbFShALLFtyfE64k/iK3RTxevf291EJG5a4PbdU4sdVKKrf7WIgCmKa6WYDZZrNo+iHgR1OtTndapPRCAJm+7ShPaiJV/WDKFZd1HjCgvy/w4d58P4EUT8oFyR2yPHfnjwObAMmSoqjN0MOAQuiQ3xO5drGJ5Vkr306LrMpMC2YgVvtyoahkZkM0qawotMHLt68ZmiepN5GqfgG57FxX0AQYBEradI4zoPCWouyys/PbWaeBY2z38TqOxUISi+VLBmdhrQDxEnWBJHn1eps9e3PLtCFf0pmHigq7sUsTqRoGpk6+oox7YR6M7rExNa/RixNBE64U7MrSEunXXwgKtVqxiOVloH09LAqxVsow2TL7SL91s30z5Lvyp3fF/k78YruiAkn9mmlXNpGqAYB7wATXrl07n+WiQoOV2mnAuCH2EkvFkhNzzy5dIe+1CE8KRxVRH89zi0TcFcQ+pqIEFiqvf+RJp7KjBy2ecQXN9s09vAaPcdDT6y5k4/PXX9vmVFzq0VABuf5y6uSyPzz4cI25eJJVyeu37Y9duMu4oR+/ECqopR2vyzo/PKIkrdQY43hm1iryOV77iVBEWiyvJnCa+RwVFnGSwcwUQa6cXADkU9bVcOJmQAqpWKIdp0967Xqqc/Kev3TSxTWF9uprQvbgnlx15V/kDYyglQYuIHCZW7PasRfejrS6lt1OqtYl76zr8MyujlNDGMcVnGF3sl0aM7bi5JgtuAg2wfSu8cRVvcp1MW2Vsem03pHCWzEcpwX0dZf2z+tNQe3f0KGD00tDTSgqKN6deP4E37IMLWZcMzTWz+LuUShgK4Qy5mTnSQGWKk37BnQEhxGJbJcWmcfyypdy5Qn8UFzRYtj1V+q8AzNH/0tOJPUkDyUlK+QeMO19LhivLwy7MPJelujba7BskCW/6qJ+vmcJkCmojXWQTag3UFtY+d77vgQy8vvhI8cjc6NSlSF5LMYYNYOEFEzihBDsMFlx7691fNW2rEvqOkIKiWl7Z2DIuLFjQoP/UGJ5gdrN5nN0uIlbwc6JUF7SsXe5al1amiMYRKKYNpfwPXYs9x5e46Ks+NUfImcYZjiWJZiL1Ciq/eIXrittqlJvHOjfr1/JylVv+dZz9e3RXlWsq4pde8pkTL8M3fXJIxTJYyo2UBIp5C6kQJf1Wlble+uohHFoMxm2PDG334FQty9Id3DaEl092WeepfRfpFHpWRG3QQhiha0C4l6MHHFuusYLTagX0Jn3kcf8q7+RutmKJw6kj79rZ5ms4Ojq9NjsWTOnBX19oMXyRIufeaspNWgQk7Z8CRdQ76W0Z59qW9ZZWyYkd73jetcu+vUP398Ve20V5f3XXTrA9xyNTi777KWlgR9qQoMEFfEffviRb5k/LmHl7o9i9c6Q/aKf+Zt/zqy5pw28Iqs4d/Cy5RX3jxs7xrlDZJh4MS2taBEG3DsUHLPSgsQepUwIFiK/R8EmFS7g1ClXlBV8gE2oF0w8/7wymveYv809jqMS2hutFwMBIkagxYoiVg5sUpbleisUQFb/2gQ7z+stFwbXHlTnTRjXrClflQxbduxWfS+Yo07rd4PvMefvf1Xnx0J+6/LPXepbIoHSy64uUdDNO7dX5nKjxYIjdxtY4uSMsTw30NcRJmBH8lhApaGdldnNVFxDESgQNpDlCTDDRAtXo8VWrVpVf+sbM5pWACfEo09XqGtm3u380KebH6iXY1r0+werqqr25DyPpI1fiwmKEP7qxlH2L/RzNfgMslh5Ji5tpQWkMuu4BOSvyDVIyYlYsCglkBnMdg+mTr68iVQpsGJtYWVpxYDtznOvp/r3tao3sMLYoYg73cFYxCrEDSSwlC1LSeY9/I2ba5N6Xj84sueUpsQBF5lm/CboUutt5NaEkwC487TyNs9Eb/CXssNu1mBLWQtOdzAoMvQVGhZSF2j22ZYgU6+r2VapS/uxWFReILVHyewua3XZ5y7NbKeP/QcO5WbxjqUlasTQgrb2jfyNvj3LVN+eXSM/kwbET1t2VKlJE4YU5ftNyG+pjK4b/fE3b/Z7V1itOBsXpl3Lx1iMk+aBC5P8OS02r+s4e9ZMnzqYRywvKexzreISi5Oyt9XBzUOccOUWOBFiKhY6dj17UOSJ2daKma2QLXSIMR59+nU90Fe+vc35ng7tS/TgnHb5aDXt8jF64CTFwgdf0r+1ZOnbqvrgobxPnzukt/6NOV+fnJpoDG5+g9+yz8U8hxnXX5Tq++3f4cF1c50P6FNepgmW5rphtVgYuXHjppxEjNVi26Aoz0lqTJMCNTpOgS5FwhyDNcFPs3u+uyyWzw3c4H1RFCAQK4CV7rt9jS6eZV0V66zoWhqEqnXrNbGi4NqelL2pEl9BSvXnPqzm3vtk4KAwwXsee6ZCP+b8/QN68POIM1AY5PzW1so898EHiMDjnsxKMxUAACAASURBVPueUjded6Ga+3dfjT0QsYCoePc/9KdY58B5L/zXmYmtCoTiXMJ+xwTnzMO8bnfMib+JwcTzJ3TeuHGT7zk8lqjlStQQygYLAsal3sTOW07SoXdPPV7ZSBxj0MHbpLAi5rFhaKzChFjESu0GssCMA9Ttoc8fl9tiR3cv/dK1vrVWgu6jhuu/dr+zPvS77fU6xFZJrRWDY9q37w60TlFggN55z8PayoUNTgb7jP+1QA+qpGDgaovw81si3TgsIOcTZ4IQcO4j/+JH6r7/9+3Yn2GCgBxJfseEXDeOl/OKM2lIrGVuHYTHEkWs3UYxt6CjV+jNJI/nxOuy2QZjlTFKl924wGpZxMpbo+UTLzyZ/VzzufXbnYnlPMhm2k/eeqdeak+5kvJK9gFWSauDE8fnKt1lex5cxTA3kCoLu3r9ogsnJiIVg33S9H9KTSoTfAcDmu90/84/piKVgIF4yZf+SQ/oIPAa70k72G/63z/XAz0K/A7vTfs7Jl58rXYiiAtiLfOteCyu1uBREKLJMhLGIBscynasFCzYZAyDw9h08JrY5mBbLB/zcAF3JNg7WIAbyAOioQriCqL+yWO0+nLuvZAqamd7+2KStyov75FIYsddinLJkoDvwioxA5soxCLmH/MD2iralhGLxmAvFAz0MGDhOYYswW9C1jixHlYLz2TP3r25AUC76qT5VIQJ+r1DKDwkSf0w+SdZ+CiAF7RJs4oUJnmbL2qEEivpNiw2sGIIGTz0shD2F544LleIiwneHSBsCFyz1KiRIxI1MWSAhMUGF4+vFQ5GDO2dEw8IzJnRiUmCCIlV4j3ishGDhA1W4icGlLyf34AkQfEez0HULX8+0dKOc4HQQUComHH9hVowkN/hMxznwgf/FEkmE5xPmKXifPgd+7rxCPstjiWuiDJ+/NhuTyx+Kvd/+pkgvSdJGOsOT9srtTuovHG36rePFLS9Lp6cg1i5GxVKrA3bCiOWCU6OSnYemOTeF9Ru4h1VG2gVPmoMO2do8Bb2DoS5PHf/7Q2aVDbEUjAAGNxBg4TZlwHM4CWOcAHVzxWTyW/w+7iPLksHqRngEviHxTpBwgeDfsb1XfW5iGsXB5DeBcgbFAOa140JwDWhiTQfB2w4Tt8Scxk/VitJzxVCDlEKWUmMh1TotroYHUt293HHThD74qsduz4s6McJEDkpiGQSCCuFCX78e7dFrhC2iXVmt27bkzaGCbuRLlKZYJAyiBhMLkhOisHvAp9bsuhvQlU4foP3QEAXsGjK+62g2O3qy0Zr8kYJAwz4OMJFUGpAedcsTn6M4wm6bklw9tln+eKRuHEWsTwChWz7o1cch7RPSwJHiNTBXLKfs1iu7jOFuIIIFdKTQMBswUmhziCzUzQZ1qYKN9De2XzUqBH5u8UVAMSGqMFYO/B/5Iw3cLv4jiBXM64KxnsYiCh2NhjgWJogy8vg5bNxAbmwRmkEFvJTUZORCSaUJO6nC8M/M6xs7dsndi5FyKJuLyz+l3bUYqmo9InT5iEucEUdjWYQMHRmu7n1ZA6OnmqxgZWCVBAJ66T3t2rTRsdWPLSve9VkLVxQkRF4QS1rhUvQv1/fxH0swgY2okacQckAgVwuBKl3kviNC34Dy+Ma8JJgdp5DzLyaibl/d0MosThu3GRT+eQ3IGWS39p/ILn4ZYPmMy1bttx39OjRXAgwqFfHUGIxYcuKYqR0l+onW+2KDJ8UeHQOYrF1cDCxCnEDpW0ZZpcD7vPLe/SMgTqDSkjCDtJFdWCyidWzV098usQWK8wNw9LgYjE401YlBMUiab6PSgXXgH/ulTXqw4/cATufSQriLogfpmAmsUwuhFW0JMXAAf2Pm1YLdzBq91B2fGTMCckgEclhCU1ERKMXoTSJTQKUQcujynl9JrF8LZ3SyOwCrJWrjElqBDkpTi7qZKxNwNSAfv1SLWRk9sWFCVL3uPkE9DxQCHk/DwgZZ3YOqhJPUzPXsTR/t0kQRCrcwLS1eSh6WQ18E6bamRXOPmuQzx1EkSNUCKsKYnxhlVhAa6+uEIRtwhEF+iBO8Vfe54yTSSxfXVEx179s+/NrOUUwKMbq6V04EwMG9E/dzgy1LGjtkQniAR533lP7JLM6A7e21s5d8xZEWNeAl4JVsxiXf/k/ryXNtRVS8FrIZ+U8OHb+XrF2m3b7ikFU5bVLs9XBQb07RFYGEWdJrC+hSe0EvyO1CygIEDB0Qa4eua4GhIUIF64d9bBQEiFIGVNYX3Z8aBNt27bd2apVy9QFt7hLyNFxa90EUsfH57BoxECmKha2pklISAyWNIcUF2mKgu3jiwOz+LYY5xEHXbt2rfpg585cKIBHE0YsxpeQilZ6heStXMBaOgpysVpL5BkfsQrdoNuupEDmRLCgmw4WipMlBguTPQf29ruBvXqWF9wXAJFCV5EXUPcmxay4jCh+rrImE+TACilvikIxlraYqE1I/7zeyGSiX7++7T7YeaLqwp58bcj+AIQlWZNKQJxlbcKhidXM+E8OhbqBEksJ1v73Yk0kgsVc8/rfPhL6HXZ81b9/v+RrARxAUKCS4fabr9VxV1ow0OgXsWFrcHkNimNWpGpXUvcL/bDGI6b+KBWpcKFRFbNEn969fIUBdt8TG6IEFrJXVhT25HNFs725+R9BoYlhG5CM4lxpUxW1qIySFTu+yrIHOy4QlQw8pHSJB38niXGwev9vwf8Evs4ykKQwxQhZCMnxYhldlR1YlGKA76VoOcqySwzKccrxmqLPLf+QXa2hawwwAYeFLay4oACccjrTalG80LFXT+/f8sheK0FweHfaSMnotYpvjyf+ARPMEEIiXL9DVbV+MISKEyx2sSrZCVpL27cvyrRjlhUpb0ARiC9ZulaTLSoYf3fz+5G/4SKL/LYMwKh8V1BlR5LyIBth8WGYu4ylZ1JKu/CzEHTo0GFrdXV1zv/teUa7UGLltvT5+pd1+2nl9VuxQbFCGmI5jJDPYkW9OTaYGVjoKMtFXMAtfHz2bYGvD+rt951LO5SihmTiCkahdubtmssNQTSEh6A6QND89GaBm5s/suCWVHkmG0GxVCEWK4xYQS4sForyq7omlKD7md2aV1efIFLnDuGtxHVZ3efchCL2Qhlk8o9aDxiEmsN5ob/PYvliLMebY6G2zdk1uW1TTCmdEwwjmwl77VWvnj2L0940BiBZ7ew82lluFAYGYRakUp6lcwHXVfcHTLGkPyixHYY49YjFRKdOnXw/jsUKg3hIspuNyOyF7vgo2JPvCmpxoLn5H0HajkxI6pAHFTBojRXki6po72J15Cnr0uWMVAfkYcTUv3b2gaBEKa6qxvuC5Poga5XGTZOl9vZncVWDktwQJGmVBCmApMqo9LGIi2K0Vzuz2xk+ASOqexMEojNYGHTPy4njQr2oILiEPnJZzvZnaasuSrzgMqyighkkqrrYVns6dCgtKL5yxUkMqqSVAUmtghTPJoH0r5BEtTxoBxAUh3EeUbK/jaCYLQxJzz+NRYxC+1J/t2Pbu0kCJnmMQdsunbVBoP9Fmi1VHRjR3F5SXAjET9UES7DU2YatCNoXMymCauIYwLULHOPNwkHxDN9PKZJLlkYEiBvkI5YEJbD5DtxB1+tYsbjFxIB1UmlWU3N+cd1OrFWWJU0CVxvxOAsfIYwuBi9pE9jFSZohMY6TLC1xrCbW67F8SkEh/dk5GHxZ4izZkicpXKa90J7sYYobgyyOy8IgCRr0fH9QByKsFosYo0QGZvegfhC4YMRqTAAkpl3g2DiXMMslTW6SVp+YCOr1YZ9LHKk+LVCJzY/aKrINLahNv0bnUG1SaS3AqxRi7FIs3jKmFiBwcSZPFawsoPhWGsVgVmnWUW10xVGev4v0HrYOK+oipQFWKSinJF2LZJk5lgXLIDFO7ZL2l0JneJLOMuhdVovf6HfhLb7fkNyUqI5hSVjTErHcI0hEkQ5PsjRfeaIH54IryWuF9v3gXEiMm/0C5Vxql+Tn9zXMGu3at99lSu5RkKaxkIZ1gHaNIOMWOZ71WkmayoRgRNxd82MBUcKcEeRve8EjXZziqjLkLTj3Qo4L1+Xmm64ITdgyKNPM5HyvuJKUODHogmbqNL/B95sWl9+ioiEo8cpvc55pktMmwuoq+Y2016sY0F5OSC5Ltk4NWugoedYM0TFTYklPC9xAMafSPBHStfXWvxR7uxUXqG6Pk/BNAmKrO+Zcl/uErDTOyg2SHhY2sMBYh0IHNspo0HHi2mLhiuXOZQlbRbYRNd6wYGwnxb9pQFmTVS+oXcHMxAuBGfglNa3FbH5PYjOqa2xcYEkglS1KyEpjYplCSMz3u0glwD3EEoclrsMg6QZcN5d147sLnSTEmmfRqs2EnSSOQlSLMzNcSQNX0XpzW7xICvxXaWFmWiUX4hx81OxTCKSvBLFB2j6DxFHM5mGCCINpxRP/nKiVdZLvF8j7otquuX5DenEwOQRZcpkkUDaTfr95DlyDLD2Fli1bJlLYkNGlzXRUSZ09RtP2ySjIFYRU7FYu3UT1Bt5XRScqMbsZBYmpUBt4j86tdJUCXBcBJCkqmwokyeUwuHDbEA6k0NdFZgYivflEBEkCjovBL8JBEEk4D95r9jVU3mTDJBC0ZkzIJSKO6xykFrL2+y/Mu0biKdS3axlnswRbD6gXYukSEW9nRuWZ1CqDMPZBNjS4usxmDWnAUuguH1HgPMJcxyhEHaO0K0gD8RTqCxTXogbaQNTQ3XFHfkZXC6WNsVwoiFgQCUsloOIiTVOOJjShmKhN87gFDBoa6eX7gweq1+/9dUFL9U2Ebe5dFBCHFXPhWROakBS66UybNrlNE7JAc7ORe1ZAbmc9lvRtU57ZFR83LMZydL5pQhN82LVrd6YGQSZ6WqNFteSLC4gVb5+emAhrNaW83fKP1mRjbptwaqLm8OFEm2LYarVUXkgRLmNWeS5jGthtJFShMZYNDlI2m4P5/M1WKbQ70zs+Xj0ltOWZCzU1NaVZHmMTTj7g5YSBcRc22StvwmeFe1bIM6l2ZXkSSJUFLX1JyklxI24fRKOdtJTnB8EuaDTbCmeFAx83/GqCkwlZX+9jx44lSnbqpUoBih9GgMnf3Hw+A6zIY1FU55s4kPiJCgwzIBQ5vmNIPqGQDrxxwE2e++5D6u/OSS9NNyEZ7lzzX+rfRnwns6v20UcfJeov6dqit8jY3yxL8UIYL9XEWClansniMUSNNMtJKivfS31M927yrwlaumeteur91/Pe12TFsoF9HXcc2q0e2v4n3/P8/eD2ZAtAw5CkuSwTvV7pnqEy7TJGzWiHaz+Z1h2UhY4k3ZR0Zaqp0Wthrpx3l15KgisYVRRZyNIVGwc+/kj9cMXPcs+urd6qKmuq1Nx3Hjrx3IGt6ukP8snWhOTAGzDxwxULctdYeaT64iv/oHqWJO/RoVJOsrW9WK5V1/7yHnXhrd/Tjyt/epfeaioLid3Bl/1O2bJnSndQFjoKsGD4rsRa0kiG16NMsx1nbd4S0hUzAl/vP0U9/UGF+uIr/+ibNee++7CeTcHT77+unmoiVib4w/aX1NI9tWVRWKXX9pwokYJcE5+7WZW2aKsmdElXxXHk6FHf/+NsN0UCWDo1maAyCJKJh5UVZs+amYuxVtq7OaaFvd+VNOtkZkBmZzkJswfkCgoWMe1mGf6+/fuPOt8YA6UtStS/jZipvr38bj1TfqP/CeHk3s1P6lhre81u9cwHFXpApL3hTVDaCzj48SHtJYBfbvLX2TG5Ae5HWuzYUelbn3coYsU73hMEQrxgzEn8T0gy9KopupwJuX33rXemEi9cUrsyVEGfOxjVUioMOhHsiKNklweScEifYSbYbttbVbUn2d6oFi4/c4y67MzR6u0D29Sda36Ve3FNda17suNQbVHpD9/8WV6MYMdoTXDHSFgjvADluduA6y341vKfaNLdctZ1qd1A5UgOR4UNlCopbz8BsyiBschzKIJ4UxQ0ZASMVI5YW8zvbNM6veSO2cV3jUKLkuDmKnZjkKQqEPHSxGdvVn0f/4p+MFNiucBBgzjippzToXYC1LGXFSP8YfuLOZexCSeur8RMAiYlwdAOfXLuoIDr3r5FiVpzYIua8uL/8d0b+7vCsHffPt+sH9VclsRwdUgfQalej2rJFwSHEdJGShjkI1YhkjtZ7TJPdcG0So9s2WxOKt73b98R+B0ulYeglT2S4gALxQPX5Jebn9QEei1k9XVp8xMkx0J9vd/k3KzKrHv3uw9lKhc3dtz9zkM+q4P1Mq0TMZQLkAuFEJS3KVO3D/uqvk9xceDgwby8Zpz0jNSnZpinysFhhLTK3sz8T+5AUqiCUh4iOarP3nGrVmFQAmnUwTotIRUl+lHtpWwT/8HOXfuSHtOcs69Tiy/6ZzU+JG5itpxQNtT3HLGXCQaDPQObnz8ZEXReTFZYdhN2LEWcKu6gC1/vP1ktvvj/JiKVqg0JfP9H5Ipqe4b718KL610QBZsOuWngiLF8FssXY9nr9+NAWkwJZH9X2QcLS8a/SPJxSprWb9/vs5ybN2/5cPSoEYmrMJhZf3f+32h3DnUQV4SYioFzUEu//6h+MfYH2k0RN5E8l51AJkaApOZMzXcyW5+MyWasEtfFBC6gxFECroFprYhlsfp/78WyWCauGe720NI+mkzilifFpk2bK80e/nHyV6zFYjMEietZOwiJzLVYcfbDDoJDatdGSj87e9bMJfPmL/C9GqcJon0CelPvQzW5hWMrFz2curCRizZp9AkRZPfu3ek3s/IIxkwpgFiQCjJNf+Uf9QAQYtkzsvLcGE2ui/+v7ztcJGzsgCyopIgUQgKek5yUiR3WtcJS8VkwpLS3+t35f5uaSDa276j0jeIN26KJxfijxG7C976pJ3tz8lfexB/UDj0OHGGTDqtMhcVnu5P295OcFQcpNYI2qagRRNjgEVYvqHRhpf+isfes7QoUAmZPLBA3XwWQCZhuJDOzqRJKsvlkUw5FOTXdQZ4zhZ/zAtxruY7X9bowU1IRX9kiFl5NHOAOkvLRfQXpz1JTW1qHIshC3SRdb0248r3ksJRV3b7FzA+wlU7afYg5CRt2PwyJt4J6CuA/2617V721uurSSy5OZLkYHLhrSOuvWXES7l/PNuFfhwtjfo54wrR8AHFjQtkQTVYTxCPEeQ0VruPjWonFEYgFMzG0Q3hLA9zt4U9+K+96c42u0OLS6ESy+47tlSgPuTokxkeUcEFcxYQvGx3q9nwZ3ovyfEVwpfxhWizfmvpCi3GR3c1cFbs5KK8b6Z+8agzZCCwI9oy0ddv2RDviIThQVeEilfLcOzM+MCElTiiEJpiR7fInvseVA/tDhvVwWQOyvGpdEyYhM88nsMUc3GYRHoJKwYKuN7/x6p61msBJ0hhr317ncymidssHVFswmSOgUVKn1wpOHJ9aWrfhsFg5EbC560lVYJIYEGchWEhSTjb0FgvF1pScNOQLWk382uqdvjgLVwCXIG4vd9QpHnO8pCaD4A9WmU0QqNTAMs056zrtDvpKc6q36oFlzrgQ1Kyah9RSk+iyWriP1/e6KDNXyQXOl2O0Lanyavjs68DkYLp7Ys3XWArf53tdlIu54lxL5bmGV3hpkKQ4cuSoMnfLB6s2hBMLcWLVokf0+GIsogwiYMi6rGpvbNJkJq1w4eBINLHYHiWpgBEEmSGS+rKYepohmlu1rF69tvr888Ynli0ZwAxkHge8mXNp1drc6wxA/WhTlhswDH7ECcqgzAHETD/H4Q7ZOTDl1SQi59ulUjtqduuBz/EUC9Q/MphtYqHY2YTg2E3rrd1k7zzM95Z7ZJvy0l/nSMgExHkjZDDpSEkT+SyuUaFlYhs3bvK5gcoRg9vABTSbG2mCjRquBTZEDNpE8CA9lJZYDvU8n1izZ83cMm/+gmpzEzpMXVpiYZ1w/5A2OSGVsrX0qvVVPqu1es3aT84/b3yqYxJAMrFmLiDPi2SM1eFfU44XMGDt5yWZbA5m1ES+03wOKwBpC5Gfw8DEQf6tV5uuPiuBCyZiiwg3KiAX5cKBY4dykjvnjiQv74WIxai1XLnqrQMmsbBWSXfF2e3t6Ei9oLSPkNXtaeCqEURdl7/t6nZfnBVUYBgHbz/2pJ4ZaOgp5tc8CeT5OP2ycQdNkHkvZH1WXDALP3HxCdXQJpWAINzE016Qb5JF8mV2TAFpWQSYNQ54MZ8NLOT/MiRzqZCAhLYqekWAyybXAdfu5c/eU/SiZVz/PXv3+tRAe0wkBavbmfjDwpAoONzAleZ/7OwWxLpa/jOoV/ru05hgdnGQRY783zwJkeJ5HZIF5btc7uAbK1ZuLy/v0cv5gYRgsBMPEVDjxthiRnuHNeEzPJihKSqVMh1lBOhYJzM2s/NgQjyxKlmqh1Txy3kIeVx5KJHMl1b53ULcPXFRXRUnXBOO2zxv5aUmepaUaVJO6DI0E0uM6296UViqKOGCMQVpGFdBbp5uiR7SDj0KA3vnGR2fUXIRKweUQTLLaTej46RcJ8ZJ975gfM6SiT8chCWvV6prL+2fe3Xr1m29CGhbtUpf9M6Awf2xZWQbLkvF7H7Bc3Ny4gb/2vktkZXN+EQLHJ6YcU5pn9xv41pB0iziLRZ1mpODxIGQyj6Xy7vXWiXiPRPU8IWJE0HWW2oyIRzkw5oXWs2+cuUq302OY60IQYifEC2ksdH7b6zyjUUm+bB9BqLg8OZ8A9jnCnrJLV9UOHxQl9Q/bgKVhplk8o9v14vLhFTI7rLyOAhLHRez4o03UyXZcJOIme5c/V+h9Ww2GCi4P/86YmYugIdMLNxTRlCvPOugHG4ioCiYY7CFD1y0Qlcxc162FYHgS/WAz7dKEvOZqh9WB9GDiUM+w3Oc92WO8wkC5COXhZVOm0Bfu3ZdDYUB5nMvVESHAXS03fDsi5pUogYSkojkTmE47qC9djAuenoGx0KoxVL57mCHgnxa2zoJqrwdSphVyHlRsRGkGmIxOYbxw0642sxko0eNTGW17BIkXDdIZud2cGtw02xVD8vCYEGoYAAFDRxmashou4oQyBWbYCEWX9Qn1QzPd9rHAXlwx1j6YuOWANfTXAnA5/9t5HdyxyqK6lLPbV5zYGsud8fvYIWHejWB5jmk7Sfy8qtLj5qiBauF44hpjKNV2x7WQgUqoKiBpuTOhO4qZIiD4YPyigpW2i0uXMR61CSWtljpNlzQMvuFxtosToYiSHxfKeHv6i2PPnf6taEzyBOvbPMRi5ls9Zq1+5IW5rr8fgaCzN5zYn4P7l8twRZrK2S6R9sN1wpXCEHDfP0pT2a382O8J+0SlTtX5yd2P++5lk9b7i6CjLidB4wVvwIIBfFcrmmpdvGS5aPSxFpYqyNHjvj8rSdeTr6SQMKRCq/JUa7w1ovpKa3jeXMPgigMH5jnxT1qPxFksXLA5GH60rQl4+ApxmVJPoqgS4HRSbp3NugEclgzT2aqDdaS/TfeXNFs2DlDC4q14kJmatyqNV6uJqhqQy8zqXo7Nzgh198bFQ0iCDDwbReNzyaNS5DQXbWO/LZU8ZvACh3QljZ/UlBeDIlrykNqJRE6XPm4YsG2VghYUSV2hBtjvl7r6mG1IBSTuEBIpvfJ9ro0yfa+cRVCcruOqqQ8Yp326aef5n143vwFK8weGEsqKtVDz28q2kVk7RYnF7VvFm7p96f7l1CPGjkiVcI4LiABrpQdu7jQ3suPmYIIsz8JZlRH8/k/f3auJg8rnW1SYA2TVMyzItcmOS4olg/SmRL7nLOu1bkoKlCEUK7jDjtHJgQ7EZ4lsFbPL3nRF1s9sPjdyJDE1d4cL4kqH8QxvCQslCmWSfFC3FUYeE03TDnLfKp69qyZefJ50IrGJSax8CmLQSxpSyWbJUQlkJmxbKtVSKwVBuIt6ubiDjaEitvP+Zp2e/gsLh1klASzDaoUGJhYj+lekxXBq0ZFSBQOOOodOR6ORRlCijxvrqfi/9/oN1l3suK4sW6swwo7Z4kpeUBS+WyWcFkrm1SyQFEsEgSBVNI0hq7M1KIiqVM6x4OEMO/BQxJiJV3W5BDz8qyVCtnGZ6H5H3JIaVuiucBFgFAohDLDoOLEOUnbzybWevmVV5PveRoChABKdqJIRayCZSFRinWQAaYJM+I7atXkX+jBF5QLU16Fg10tD1HiBvy4pzbogiTHYsZ7YqGwoqh8b3F8Z1+Xey9xJpUUWFOOqTyi8h+SkjPLchX1K6++Vm3HVg89vzHvfUjqrEwXMElLDI/XQ03q47NvUxX3/ibXXlrGmixyTArCojjxlQpyBVWtO+hbRpKFO8jJcEHMnR65GFRpmL5wFDDFppABpn/helVWVnhqANcJceG8LkNDl0YElvxYdYgoZ/YqWwHExE1ECDCTumDR+X8TK54hL2ZaIanb4zz+EBB7SSIXxTOqnk/OJwicJ+dIPOkq9k0CcpP33ne/T2LHQ7lnUf5mBazpYxw9/r3bYvWyEJHMxMPfuDnR8V0yutyXTw1yA1XEbiMwMffLhbqDciEEeqHZy8sSEUpgK4TgmWef3/ml6Z9P1M3JBSnUTYKklfMQChLxIP5p3+JXemDjHopV0RYtBrFMi4SFqf74I52Dcv2ewNVcR+Is4idT8ZO6yiBkKWY88eRTVcc/+cRnJoOUQMKGMq/KIs4+wbJ1FO4gnpEsY0oCe8wFWSsVQay5JrFwB/Ev46yDcQE1BrVGAklx+yTOkm6kVes2RC7pRyFc/Mo23wZ11JO9uWLl4ZEjzi3etvsWwpQ1F8y8EMRhnZOICK7FhXEgPRGVp+aZXZCwhrI0xcy7uSDHwCNMbi8WqP+srHzPRyriqiAlEJUZ1444SoSJMEhpHSRkbCXdtDuuGigIdAWVQx3kRFFn0sDVfornsGT2buZkzMlphS0zwd+9bcYooHf0TAAAGhpJREFUXw1hs2bNDt3wleklcddrFQLUQgpdg5b0mwgbqGHWDmUvSHrn9xEaXJ8JWveUdCLAZSReK5b6J8AF/K8HfuOLrSgKuH3B8tByOvGCqr2N5E7scrND/x+rZk7gNOVM4yGB6y7t71tlgfGbPWtm36D3RxELn+Ju87lb//3V1LWDNphFaO4BkSgx4cJgvUTdiUraueT3srIuVdO/cH1BjWeiYEvYLuB+nVc2VJPJjj0gBbEJVR5YpihySk2iKI5xFhjy+xCCWNHVNoBzIAVAvi3s93ER7SUvWeOx//5j9fYdlT7B4uHnN6kXKsJXPiCCkaqRfQGCIJ3CsFJpq9l//P3z7DKme2bPmhlYTxDVQHChTawJw7pFnnBciPu36reP5AokIRj93bt7WfKwGQY3gbU5plJTVbWnDGWpWLktETdQA13iRqnX18GG7foFAULcPuxrmnhiWXDjkOB5Xlo1K8MS8pt2ka3EcOJi2q6hHUsG9U3kOKjqoDC3GOQiZ2WTCsEizhjDGjFeqANUXtsHwOJFaXUO6XhAwrSkIrZy1AbODftMqMVStVYLct0o/yencPuCZakO0IaYcjsxzAWhTwFVG1GtqVwuocpQJbQhy0XiAveLNVdxEswUuZqyPWoc7qZLUcTlk7xZ1HtNSO4q6TKVpOcdB3Td+v2DDx/65JNPcvkIvKG77n8z0QJb8XxQmPFy7JADRdB0C5Pi5unD7dXCL86eNXNS2NfE2X08L6eVtOJdN/MM6ETqAmYb8x1nkzpuxANPvJP3/B8eeuQwvnvWSDK4dA/5526OJBXW5Odjb9E5JDPZioWghZidT7IJKO9lrVdQ3kxw8OPaFcBUayTJP2VNKu7NH594ssokldI5q02JV60jXqD2kQxmsrY3lWPSTksq8reOJfgL3e8+gUhiecuNfasjLxmdbFdGpE065tgnHLZ6WJMrZq9tXEJUQhPHjx9vvej3D6a7mhmAagsa0oS5fZKofflz9wQWteptiEaeKMptr7clCi7SxRKRsI4iGJaNVc1Z7qyYBM8veXHvhx9+mKcCpl1JgUtIXI4Q5iJXWkzKH+uIFoUTy4PPn4TBSZbti5uXZKsUVMEkFcfkO+xNyA4ePNh58VPP1Dm5WGzoWkoCkVDaGPQs+4dQcSRt5HkhCX9HlRDxOgSjsgJLiPgx3vgOAaRHhKlrchEDb9y4ybfKkB6ShRYgaCXZI9d5XtxVCJDYHbmrSFKpGOKFBgydN3/BHWYlBj8Yt6EnAoSu17p6cu32qYdqPPkz2530fv7IWnXnzLG+QJMbWEwxwwaVECzTIAZCkZP2Y4XW09E96u2Pt6leCV0y1xIP3VrA66iEMgi5itUIxgZixRtvrvDdC+3OL36nYLWZccUiR4ndKcrFkqXFJaPzdrepjhItBEm2FYGpt8t/IBYVEHH8YfxbagFxB5FHSRJLGypiqaiGMnHBjfn3Rau0BG+SixvZsUOHmqFDB2e3o7MDDFhKhLAUWcIstE1SoBsEafUmdYqS3yo2sVxV68qrXE+zLMkFQgh6tV9Y4P7CjB9XpYVrz24X4rqCymOqz0RNNSofokDFsQSYWC/MNaRCas9y36IdAS4FN5Qbm9kPOWBvvJAVzCX7ECzrjfDEdSwmUACXvPSnPAkaUqWt5gkCoQfFt1F7XYfhkjHlLon9jrifP/2OO+K9d9zYMYeXLa+gXCgnM9IC6rU1u2KbcORzagQP7dmn/1616GHfZuBZodLr7GSrl5u3bG3Rrm27mq5dy1pk/qNFAtbka6/9i2p1egs1b/Rs9fh7S3XR6+frsNyoUECqPzz4cI2tACJU2KJTViDWOvjBrlTfBqFuunKwatHcZ3fujyNaCJJYLOWyWl/1L/qKhJT0m/WCxUCQwlQXlitLIIQgMlD9QKxEYlq69DYGUAMIqeymMIWUxxUblC8VYq1UUmJ5/mVBCmFdImjVaWMhF+ShcgJJXqoecDURRpDzG/puklzjRx57XDUmUgUogVirLe5PuJHUYilnrDWxeHVkhSKMXM+/8GKmCySzBKSh7yGyvC3Jk8eCbN9a9pOGeviBQkVDJpXyrJUDiayVilPS5IKrODdOT4L6hGtxJBgwoP/eSydd3LkuGtJkDXPHxYYEcod2nko1AlK5irppgDV71szExEpjsXAJ59o7QGK1HH5pgwE3dImjsJMBQIUGPcIbGxoaqShTWvT7B6saI6mUtlYD7Kdi561spCKWB1/JPDWESJTFAN89oGd7/WjT6vTUv4AM77q5VGj8+teLajZu3JSus2QTtPLHmipWF9hXgwmtUFJlNQaCQJmeYyHjHXHzVjZSuYKCefMXUEfo25bxX+5/I7NkH7j6ot7qwhEnXLjDR4+rx17cppa/nT48QobHNXRZ2KFDBifejvVUB5UtdjWFIIsQoRhjwATjwK7YiVrIGIVCLBaYYT/hMKepcfn4ct8FBa1bnq6+eFk/NWxAoga4PpCQpEKj0jEBrH17XdnC//p1VZYbiZ+swH3G9XORitwmk2zWpFLGGMB6ZYEbpjon2byxnQSxE8QujBs7Zv+y5RWnmUlj5MrDR46rLe8XHrPcdOUg1fx0N/fP6NxavfpW+gqEAx99rCrW7VbdupSobp39scrRo0dLVq9Zq44dO159xhlntG7ePHvXo7EDK/X0M8998tFHh/JIxYSl11QdOFLwWX7r6uA8aUmr5mrFu4XlQvFepp6fp2o/NnvWzLsK+d5CLZbygjufkDFlYm9NsELBzBSEHmWFB+7Mqr94ZK1eBu4CMzFxw6bNiVIYJzVI+GLRuTZ2JQWgkuKu+9/IpH1DlEXq0bWwMYCVuiG/wKG6UGulsiCWF9z5DoQDTlqRkRT42VmBZeC4LS7XkAYnTyx+Srs8jVE5zAqc+/8sfmonCV97HZXyVpbjXqfZuCAINUfC73Gh5A1wAVMLFiYKcgUF48aO2bJseQVBzwR5LguXcOzQroEK0DtbD8RyA/j8sePRAg2u4Z9Xvq9OO+00ZyXJoUM1JStXvaV2V+3ZeeaZ3dq1atXK+T0nG5DQn3rm2Z0vvvindvv378/bH1R5qt99j69TO/dmW8xy8NDH6qKRZwaGA+9srVarN6XjQIALyJL75Fu9OJAJscCy5RVLlVJfopeHPDekXyf11oY9etCmwXtVh9TYIfkCHdZq0TOb9YUPA67CD788TN+YjZXxCM4aM475zC4lTneWwXUqEAwL9ezzS3Y+9/ySQEJhpXCl/7zyA3Xs+CdFOY5d+w6rEWfl77zIGHjgyU2RVs0F7uus64fZRba4gJPRDbI47oLkdhvz5i9AxHjBfJqLf9fC9D43fjbKkMRUEOSxl7ap93aHp5ywVN+9brDvcwv/uD7RjaBSI6AgM4cunTvvvOjCid3Ky/MWxTVKEEOxxzPb0QYdP/dyScV7mbp9YUABZgx0al9bHbNm0349BtKKI7fdOMqVs7opSfV6FDIllqoll6+DLqBFGTNbXWL6Zf3UGMvaiaVbvXFf7COBVCS+J43uEUqwVq1aVY8aOeKTQYMGdKqLhqFZAnePTfzeWr3muCt+MoHb98TL2xJPlJAjyXUvFhyNN5WnAk7L8iczJ5ZydNBVMRswZgXyHsxwLmC5/uOhdYl/KS7BlGfFzh3+mdKevcrbNFSSQaaNGzfVrH17XdUHO3cGWicB+ai4K8ZN4DlMv6y/Oqd/R+3aL/zjhkxk+DQgrvrWtKH2JzG7I7IQLEwUi1hkrCGXTwXIuirDBeKqH3zpnMDXcSH+tCJ90jIJwUC7du2qBg7of7xf3z717i6S9N66bfu+de+8c2jfvv2R9WdYJZLpaQilvHtx018OyrlwKqXXkAVoY2a3bPAw0tvUPlMUhViqllyY1kfM59I0Y0wCZscf3XRuaP7rnxeuymTG1HsloSxN7JPXLDQMnTp1rOzVs+exnj3L+2DNitFUVHniA0TasaNy6/YdO5ofqD7Q2V4XFQSuz5LXK9XS1TtTx8aITlRHBOHp195TT79WNx4M94qmm4646havoDxzFI1YKiDeIlfEfkdZ9X83gVgxoPyE68XsiFghMyauyE9+sybz32U2xG+HaGkq/Nu2bbuzZcsWxyCc/r6e5X1atfQvY2lfWnteBw/41c0jR49q8vD3rl27m9UcPvxJdXV14gVyYp3CdvhIgjB3XJBGUEoDRydb5S1eLDgRHISiEksFFOoWQ8xw1ZTh9pk3N8gNROjAbcFFiVIbowC56CWflmR1Ccik+9+v36MfWU52WPG/nnFibROTGs/Z3gST3/yH1hV83YMQsA6PBrSTso6rTNQFsTraexqrjNfnoDjN+At/qysIhGtoKoO2G2hL8iqDGMwEiWYINqhXR5cbUi/AY1i/fX+OUMXED758Tu7aQqD7/rhex1wuVz3L6y5w7MCovHzViKRL7ZOi6MRSteQa4ZHLZ4+pKys0F4KlmXXdYN/NYnZE+TPjLdsNJD8WdJPJkyx6Jl3yMQhYL1zGQb1rSUaHqySxWRpggWqJVK127PpQrd9WnalVYmLqVNoq0NrY7uDvEC027cubzARZXnfH7vaCoogVNuqEWCogeawKXK/jsjjMjBCIWMsMns0ZMY7/v/B/NtSJcoVVa9O6uSYa58O/Akdc4IPZUhviMCD3VB/W4hDqazHiWOVd9wtHnKkuGtlN/+Y/3bfS+T7bHTRTHa48I9h38Ki2bIW4hiGkyjQJHIY6CwLYXGHe/AU3KaXuM5+XC5CGXGZFhoA4CXfPJs7r3qK4oBtqggFQV3KwCAXFdsuyAmTBxRNLz78ogK5Fh9wHPAW5R0x2fJ7nuU9YO1NsAghNeCBpFzLiFQSQ6pa6IpXKaNlIbHgndpP9/qBGL2HA6tgEwSJBCGZUEpICIRUDwvwM1o0Z0gYuSxAKWWB5MgBS2K7a5ROCU2LL1/rJwX0RctqkEshCRibBJMvwJVflwP3FktWDkFkRblyMGztmxbLlFfhoI8yPEORz01xLN2xI4tGEZPXBeZ85Qw3uc8KNYua7+uLeeS4js6JNTvIrxAEusKL5ukv66Irr9iUt1O59h4suFdcnuM4sKIUI+w6cmIBYtWBOMAx+Xuce2Pjw0DGfWtutcxvtQnZqfyK+5F6wAsGuYuf3B/ftoFcyRF3nkARwUWX1INQ5sVQtuR4thFxUUnODzuhUW30ucZUsD7n+0r564Av6dm/nu5EiZEyd2MsnIGC9fvfMJucyEwYSpAIMgD5nttMDprxrW7Xtg49OGoIh6lwxvlxNv7y/Pj/cPB5MOAK96sBa0oMg41L1ajwSyv3gMyaBaoWmd9Sb7+5Vfbq389038Pzr7+vlIWHQpUrXDG0wpFL1RSxVILkY+KzF4qZhmX7x2Lt6eYHyYoCp5/f0vd+8kdx8LBtWjYeJ3z3rzmPxnd+adpZzXRDk5jsbO7EY8F+dMlBfOyyFfa6sezKX6SSxWnyX6UEIcNEfeHKj/l4e3FOZMJnk/uPhdVopDAMhhKPPuqpPUqm6jrFseCd+v/08MVec7roM6L9d8IbauONENYIZW5nAqhE7oQ5ClCusuIAb6BIsGDAz/nJgaJlUUIlUMdp0FQOirgZdO9e54F7b8WlQrOUiB/cBAcOckPgbRZD79JPfrI5UBkPUv3ollapLVTAIXIB58xcocwNxMOX83poAUUlk21KMHRot4aIYmkTR8dZL7l0vbOWR70LGjtNzQ7pMoTLy28Ru5iRQl4AYWKIB5aWqc2lLrciJ2ONSV7E8qzfuVxsrD9T+3zHIn15a6UtpoOi5FEImHq6BKVaE5fDiqIFMvFPc20jVO6lUfVssgXch7rGfZ0a62R2QBsImGjfUnP1wX+yZ+aU3dzqtDoPEFjdIYNpEC4I0O2FAQTAGdbERNGAH9CxV3712sLp8fA99TuVdSzSpeL99jrUWY40ukmUi4OFydZNYrdctdbAQdRUr1ZBJpRqCxRLMnjVzjreOy5fnIkkKuX7+6NpYVfEkILEUDCBmZNMSMWvb+S0GhqvKGlLY1dl8n21xwirlbTlZZv+4qHVDT6if8tt8j/zNAB3Wv6MvJ+QqD7KPRdzeTqX+Yl8moiT5o7hWC2v9ReV/X9LFjyFV6qohkUo1FIslCMpzcSFvu3Fk7O2CIAqxl+3eQThzbZDyLJANBrRLzuf74rbcclkOU7KOg2H9O2lCyIPJgofZBwRiM0BN4hA/2jGR3UosqAkLlixJW7G4VguLZ8daw0JiOhvI6bfNcC6pV15FRYMhlWpoxFInyDXS3iqI2Yo8xSX5y6qdsF0XBotd/c6NdsU8MxyL8yRHZg/YwwFqoD04+Y6k68CC3CXzeVxc8nEmiB9ty2y7r2Ip7OvEZ1koSp0lgoY8+L6g48FqmRCrZcO0TlyPuJaRkIB775isquuyTCkJGhyxVC25VnjddfOK0KhWDshZhMIeaEGCBVbNdpsqdx9SrQMUvsoA5coWA4LeFwS7esQEg98kLgPUtgbETWKl8qyVMcAhpitOhBymtWRSYgWBrli3Jo24VksS75JHjBJypKFmQJ/9am/pR4MjlWqoxFJ+cj1mv8Z6J9yCJDtJ2jfRJVgwI+Nq2WBgMYszc0O8OLAHc1I1ELHB93mrfZtNflxae3DLZGKLJralQDGN2wCVCWO6Y2VwHKuFdaS4GVJFWW9cP+KpgFI3Jty+dVGlnhb1liCOAzYUHzd2zCK7P7zyZjMuOg0246x4ZWDyGNizVO07eEQnJk2EJYEFvMd2R+R7bUy7uLc/Mb1yZy6JHQeXjunuswzPvPaezxUjIfq6QRCS5hyHmfSmioHBTOWJVKlAvide3uE7AknOiiQfBb7XTgbHrcaIcw1w9+lSG9CmHJGC/n/F6e+QERo0sQTjxo5Zsmx5BbPUFDwh8zWs1rmDytSW9w9ENgZlMLzuuU1mbMFggFRm2ZPy1LUPD32sOndoFUi4p5dV5okSeq+w0d19zz3xyo5E1RmUFMlvQgaSqaa17OzloczyKwgi1SgCSMUkJAMeYYcSLBt8DkGjtlZyv1qxfm/uWvG9lHCZYEKzOxG7qjHsio0wQKRvXzNUTRzR3VVJwez53TS7K9YHGqwraGP2rJmPeuVPL9qvMTP+1Y2jYlVrMIBsN8SVIGVQMXAZ0Kw3IrcTlrMyYUvYKkKWt4EbaSawJSbKcwd75ufFOGYz3uJ7TCEmqkRIeXGX5K/4bSYYs1ZQf68j5jRjLa4fK7bjrqvCSqH8BqxBW9mQ4ykXGg2xVC25tsyeNROX8E7X6yQN6XKaZhd/M8ZgcJjCBmRk0AQt6LNhxzRx21sLkNldn7dzPkFyNfGWK2bCXUvboWpfzM/x29T4ybq4KGCliKWuDe44fI9HqgYbT7nQsLudBAB3YN78BVgwHj4zVe4tH2DhJFujxllFyyBo89I2PaBRs+4L6Bzk2lbGJUqUW3FK0tWwtqydxGIpo+aOSgv/96TrnaLbfF/sV1WDErtxRRrpzxhQQaE8128aC2STHm9DQKMklvJUQ6+Xxh12izXl5T6olF/88rZYHXjFKmWx/aYtACQhFp81XTesZ5ASyft4v+v7GeC4Y2a5UpyiYJS8vQePaNdY9v213WSs4esFXCfuTUQ/RpTgGcXsolRsNFpiqRN7c83xrNdcuxMUsyIuxqQx5eqh5zcWvPw9Tu5MN1ixqjsqHUspgmC7gXyXKwUgQHYPIu5eS1SJo/iNGVoWuLJXGe3K0iyTwUWHUCG9PLZ6hGqUVspEoyaWwLsRI+bNX3CHt5u/787VSulDdfMVukKlbUhpE8a19sg1eOPGJ0q7gfHLfJTnNga1DautTTxBSttFdUEfawCxcEV/FzN2MhGDUMqLm+c2Zitl4qQglsCLvRZ61utq+3VuLPFXWoLpItzKg+qiEd30gHbN2q7lJP8wc5R26bAsG3Yc0IR0xSJMAPbnzbgKYtjrwrAuWEnXsdhpAD4b9F6BbeWwUMRTuMhJk9wxCfWiZ6VOqv1o66z9WV3Da7eW5x6agGDEX2lcRAYo4oEdxMfpAiWwq9Dttmyulth2SzEV0artX78/1vd/FLswgvD9ki6AmGlUxJiEwu2b46VRTjqcVBbLhOEezvAEjrwkFzeeB4MHC5akzbJOqDoGM89jnWy30QW7gHegXcbkIADHan8/snsQscz2Y6Bz+1ZqowomFt+fVpJHlCAfFdH1t9rb57dOuybVNU5aYgm8pOLCMIIxS1PoWXPpMS3Tv1DxXuodUbBCPMwVu7IUI0zUcBXdBnWL2rjjgM8qBsnuyrM6JrE6Zdx9lzzUhGHd4mxrVO15ECdNHBWGk9YVDEIYwUzQzEbcxKw6yuoYCpKVlWgZ29wAz972htjmb372hvN7XL3qf/LbNU51kN/BSiGhZ9kWQNIZFERH4JQilOCUI5YgLsGUtzsKlqyY3WrN2EZ5LmWQjC4tnk1g3Yq1Y4cg4U4qW73r++ipRCjBKUssgSdyzHGpiC5AsmJse9MQAXkG9e6QdFsiVL65J6soERenPLEE3vauEGxaHCumPHcRoq3ftj+TzdoaAlgHNXxQmVb2ojZlMMDJL/QIdVLJ5mnRRCwHvG1eZ8S1YgLk+2JtmVMMmFsLQSTUvIQrsx/zXL1GU3VeV2giVgi8TfOmeY9EJFMiXVcfzpGt5vCxerNsEAgFj22CCtyfa6VnnRaeirFTXDQRKyYskk2yy6aSQDaEO+T9q2R/q8O1Fo7Xd8TYHEJ5hDGtDNZHeWJIlw6tNZky2ODuMW8lwZImVy8emoiVEp7oMckjWmB1RyPFSm8HziWnugiRFk3EyggG0SZ5K51TW7R6wIsekVZ4ZGpy8QpEE7GKBE9lHGE8+jYAy0ZuaYtBoi2NbWVuY0ETseoYHuHMR0drK6OLUx4R7ptYmhXe3/vl7yYC1SGUUv8/Qf/F6D9X50IAAAAASUVORK5CYII="/>
                    </defs>
                    </svg>
                    
                </a>
            </div>


        <div class="leftmenu" id="">
            <ul class="navbar-nav ms-auto mb-2 mb-md-0">

                @if(auth()->user()->user_type == '1') {{-- SU --}}
                <li class="nav-item mg-sm ">
                    <a class="nav-link" href="{{route('admin.dashboard')}}">HOME
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item mg-sm space_menu">
                    <a class="nav-link" href="{{ route('admin.user-management') }}">USER<br/>MANAGEMENT
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item dropdown space_menu">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        TRAINING<br/>
                    </a>
                    <ul class="dropdown-menu fontsize" style="background-color: #3B619F;font-size: 16px;">
                        <li><a class="dropdown-item" href="{{ route('admin.training-course-management') }}">TRAINING<br/>COURSE</a></li>
                        <li>
                            <hr class="dropdown-divider">
                          
                        <li><a class="dropdown-item" href="{{ route('TrainingManagement') }}">TRAINING<br/> APPLICATION</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="{{ route('admin.feedbackquestion') }}">TRAINING<br/>FEEDBACK<br/> QUESTION</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="{{ route('admin.training-list') }}">TRAINING<br/>MANAGEMENT</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="{{ route('admin.event-management') }}">EVENT</a></li>
                    </ul>
                </li>
                <li class="nav-item mg-sm space_menu">
                    <a class="nav-link" href="{{ route('exam-list-admin') }}">EXAM MANAGEMENT
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item mg-sm space_menu">
                    <a class="nav-link" href="{{ route('admin.questionbank-list') }}">QUESTION BANK
                        <span class="sr-only">(current)</span>
                    </a>
                </li>

                <li class="nav-item dropdown space_menu">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        BOOK MANAGEMENT
                    </a>
                    <ul class="dropdown-menu" style="background-color: #3B619F;font-size: 16px;">
                        <li><a class="dropdown-item" href="{{ route('book-inventory') }}">BOOK INVETORY</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="{{ route('list-book') }}">BOOK MARKET PLACE</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                    </ul>
                </li>

                <li class="nav-item dropdown space_menu">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    PARAMETER MANAGEMENT
                </a>
                <ul class="dropdown-menu" style="background-color: #3B619F;font-size: 16px;">
                    <li><a class="dropdown-item" href="{{ route('country') }}">COUNTRY</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('gender') }}">GENDER</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('certCode') }}">CERTIFICATE CODE</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('feeType') }}">FEE TYPE</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('trainingOption') }}">TRAINING OPTION</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('educationLevel') }}">EDUCATION LEVEL</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('trainingMode') }}">TRAINING MODE</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('trainingType') }}">TRAINING TYPE</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('admin.feedbackquestion') }}">FEEDBACK QUESTION</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('reType') }}">RENEWABLE ENERGY</a></li>
                </ul>
            </li>


                <li class="nav-item mg-sm user-menu dropdown space_menu">
                    <a class="nav-link dropdown-toggle" style="color: aqua" href="#" id="userDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        {{ Auth::user()->fullname ?? Auth::user()->company()->first()->name }}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="userDropdown" style="background-color: #3B619F;font-size: 16px;">
                        <li><a class="dropdown-item" href="{{ route('user-profile', ['id' => Auth::user()->id]) }}">User Information</a></li>
                        <li><a class="dropdown-item" href="{{ route('change-password') }}">Change Password</a></li>
                        <li><a class="dropdown-item" href="{{ route('trainer-attachment') }}">Trainer Attachment</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li class="nav-item mg-sm" >
                            <div style="margin-left:20px">
                                <a class="btn btn-danger dropdown-toggle" href="{{ route('user-logout') }}" id="accountDropdown" role="button">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="none">
                                    <path d="M15.8427 16.7449L15.8428 16.7433C15.856 16.6083 15.7574 16.4834 15.6131 16.4696L15.6107 16.4694C15.4889 16.4572 15.3521 16.5626 15.3382 16.6995C15.1871 18.3325 14.7162 19.5707 13.6953 20.3768C12.6927 21.1685 11.2583 21.4619 9.38047 21.4619H9.25047C7.18084 21.4619 5.63005 21.1011 4.61567 20.0867C3.60129 19.0723 3.24047 17.5215 3.24047 15.4519V8.93187C3.24047 6.86224 3.60129 5.31145 4.61567 4.29707C5.63005 3.28269 7.18084 2.92188 9.25047 2.92188H9.38047C11.2687 2.92188 12.7102 3.22032 13.7138 4.02721C14.7323 4.84617 15.1964 6.10222 15.3377 7.75845C15.3606 7.91834 15.4876 8.00608 15.6131 7.99413L15.6131 7.99399L15.6248 7.99315C15.7542 7.9839 15.8616 7.86821 15.852 7.72068C15.6884 5.81318 15.1003 4.52208 14.1004 3.69456C13.0893 2.85769 11.5704 2.42188 9.39047 2.42188H9.26047C6.863 2.42188 5.27045 2.9365 4.26778 3.93918C3.2651 4.94186 2.75047 6.5344 2.75047 8.93187V15.4519C2.75047 17.8494 3.2651 19.4419 4.26778 20.4446C5.27045 21.4473 6.863 21.9619 9.26047 21.9619H9.39047C11.556 21.9619 13.0664 21.5333 14.0755 20.7095C15.0736 19.8948 15.6653 18.6239 15.8427 16.7449Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                    <path d="M9.49988 12.4414H20.8799C21.0137 12.4414 21.1299 12.3253 21.1299 12.1914C21.1299 12.0575 21.0137 11.9414 20.8799 11.9414H9.49988C9.36602 11.9414 9.24988 12.0575 9.24988 12.1914C9.24988 12.3253 9.36602 12.4414 9.49988 12.4414Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                    <path d="M18.6496 16.2913C18.8396 16.2913 19.0296 16.2213 19.1796 16.0713L22.5296 12.7213C22.8196 12.4313 22.8196 11.9513 22.5296 11.6613L19.1796 8.31125C18.8896 8.02125 18.4096 8.02125 18.1196 8.31125C17.8296 8.60125 17.8296 9.08125 18.1196 9.37125L20.9396 12.1913L18.1196 15.0113C17.8296 15.3012 17.8296 15.7813 18.1196 16.0713C18.2596 16.2213 18.4596 16.2913 18.6496 16.2913Z" fill="#F9FAFB"/>
                                </svg>
                                  <span style="font-size:12px;"> LOG OUT </span>
                                </a>
                            </div>
                            
                        </li>        
                        {{-- <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">LOGOUT</a>
                </li> --}}
                    </ul>
                   
            </li>
            @elseif(auth()->user()->user_type == '2') {{-- SA --}}
            
            <li class="nav-item mg-sm space_menu">
                <a class="nav-link" href="{{route('admin.dashboard')}}">HOME
                    <span class="sr-only">(current)</span>
                </a>
            </li>


            <li class="nav-item mg-sm space_menu">
                <a class="nav-link" href="{{ route('admin.user-management') }}">USER<br>MANAGEMENT
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item dropdown space_menu">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    TRAINING<br>
                    ADMINISTRATION
                </a>
                <ul class="dropdown-menu space_menu" style="background-color: #3B619F;font-size: 16px;">
                    <li><a class="dropdown-item" href="{{ route('admin.training-course-management') }}">TRAINING<br> COURSE</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('TrainingManagement') }}">TRAINING<br> APPLICATION</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('admin.training-list') }}">TRAINING <br>MANAGEMENT</a></li>
                </ul>
            </li>
            <li class="nav-item mg-sm space_menu">
                <a class="nav-link" href="{{ route('admin.event-management') }}">EVENT
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item dropdown space_menu">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    EXAM<br>MANAGEMENT
                </a>
                <ul class="dropdown-menu" style="background-color: #3B619F;font-size: 16px;">
                    <li><a class="dropdown-item" href="{{ route('exam-list-admin') }}">EXAMINATION<br> LIST</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('admin.set-exam-result') }}">SET EXAM<br>TABLE</a></li>
                </ul>
            </li>
            <li class="nav-item mg-sm space_menu">
                <a class="nav-link" href="{{ route('admin.questionbank-list') }}">QUESTION<br> BANK
                    <span class="sr-only">(current)</span>
                </a>
            </li>


            <li class="nav-item dropdown space_menu">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    BOOK<br> MANAGEMENT
                </a>
                <ul class="dropdown-menu" style="background-color: #3B619F;font-size: 16px;">
                    <li><a class="dropdown-item" href="{{ route('book-inventory') }}">BOOK<br> INVENTORY</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('list-book') }}">BOOK MARKET<br> PLACE</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown space_menu">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REPORTING
                </a>
                <ul class="dropdown-menu" style="background-color: #3B619F;font-size: 16px;">
                    <li><a class="dropdown-item" href="{{ route('admin.training-report') }}">TRAINING<br>REPORT</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('admin.payment-report') }}">PAYMENT<br> REPORT</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('admin.access-log') }}">ACCESS<br> LOG</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('admin.audit-trail') }}">AUDIT<br> TRAIL</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown space_menu">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    PARAMETER <br>MANAGEMENT
                </a>
                <ul class="dropdown-menu" style="background-color: #3B619F;font-size: 16px;">
                    <li><a class="dropdown-item" href="{{ route('country') }}">COUNTRY</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    {{-- <li><a class="dropdown-item" href=" route('gender')">GENDER</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li> --}}
                    <li><a class="dropdown-item" href="{{ route('certCode') }}">CERTIFICATE<br> CODE</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('feeType') }}">FEE <br>TYPE</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('trainingOption') }}">TRAINING <br>OPTION</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('educationLevel') }}">EDUCATION LEVEL</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('trainingMode') }}">MODE<br> OF<br> TRAINING</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('trainingType') }}">TRAINING<br> TYPE</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('admin.feedbackquestion') }}">FEEDBACK<br> QUESTION</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('reType') }}">RENEWABLE<br> ENERGY</a></li>
                </ul>
            </li>

            <li class="nav-item mg-sm user-menu dropdown space_menu">
                <a class="nav-link dropdown-toggle" style="color: aqua" href="#" id="userDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    {{ Auth::user()->fullname ?? Auth::user()->company()->first()->name }}
                </a>
                <ul class="dropdown-menu" aria-labelledby="userDropdown" style="background-color: #3B619F;font-size: 16px;">
                    <li><a class="dropdown-item" href="{{ route('user-profile',['id' => Auth::user()->id]) }}">User<br> Information</a></li>
                    <li><a class="dropdown-item" href="{{ route('change-password') }}">Change <br>Password</a></li>
                    <li><a class="dropdown-item" href="{{ route('trainer-attachment') }}">Trainer <br>Attachment</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li class="nav-item mg-sm" >
                        <div style="margin-left:20px">
                            <a class="btn btn-danger dropdown-toggle" href="{{ route('user-logout') }}" id="accountDropdown" role="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="none">
                                <path d="M15.8427 16.7449L15.8428 16.7433C15.856 16.6083 15.7574 16.4834 15.6131 16.4696L15.6107 16.4694C15.4889 16.4572 15.3521 16.5626 15.3382 16.6995C15.1871 18.3325 14.7162 19.5707 13.6953 20.3768C12.6927 21.1685 11.2583 21.4619 9.38047 21.4619H9.25047C7.18084 21.4619 5.63005 21.1011 4.61567 20.0867C3.60129 19.0723 3.24047 17.5215 3.24047 15.4519V8.93187C3.24047 6.86224 3.60129 5.31145 4.61567 4.29707C5.63005 3.28269 7.18084 2.92188 9.25047 2.92188H9.38047C11.2687 2.92188 12.7102 3.22032 13.7138 4.02721C14.7323 4.84617 15.1964 6.10222 15.3377 7.75845C15.3606 7.91834 15.4876 8.00608 15.6131 7.99413L15.6131 7.99399L15.6248 7.99315C15.7542 7.9839 15.8616 7.86821 15.852 7.72068C15.6884 5.81318 15.1003 4.52208 14.1004 3.69456C13.0893 2.85769 11.5704 2.42188 9.39047 2.42188H9.26047C6.863 2.42188 5.27045 2.9365 4.26778 3.93918C3.2651 4.94186 2.75047 6.5344 2.75047 8.93187V15.4519C2.75047 17.8494 3.2651 19.4419 4.26778 20.4446C5.27045 21.4473 6.863 21.9619 9.26047 21.9619H9.39047C11.556 21.9619 13.0664 21.5333 14.0755 20.7095C15.0736 19.8948 15.6653 18.6239 15.8427 16.7449Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                <path d="M9.49988 12.4414H20.8799C21.0137 12.4414 21.1299 12.3253 21.1299 12.1914C21.1299 12.0575 21.0137 11.9414 20.8799 11.9414H9.49988C9.36602 11.9414 9.24988 12.0575 9.24988 12.1914C9.24988 12.3253 9.36602 12.4414 9.49988 12.4414Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                <path d="M18.6496 16.2913C18.8396 16.2913 19.0296 16.2213 19.1796 16.0713L22.5296 12.7213C22.8196 12.4313 22.8196 11.9513 22.5296 11.6613L19.1796 8.31125C18.8896 8.02125 18.4096 8.02125 18.1196 8.31125C17.8296 8.60125 17.8296 9.08125 18.1196 9.37125L20.9396 12.1913L18.1196 15.0113C17.8296 15.3012 17.8296 15.7813 18.1196 16.0713C18.2596 16.2213 18.4596 16.2913 18.6496 16.2913Z" fill="#F9FAFB"/>
                            </svg>
                              <span style="font-size:12px;"> LOG OUT </span>
                            </a>
                        </div>
                        
                    </li>        
                   
                </ul>
            </li>
      

            @elseif(auth()->user()->user_type == '3') {{-- TP --}}
            <li class="nav-item mg-sm space_menu">
                <a class="nav-link" href="{{route('TP-training-schedule-history')}}">HOME
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item mg-sm space_menu">
                <a class="nav-link" href="{{route('list-book')}}">MARKET<br> PLACE
                    <span class="sr-only">(current)</span>
                </a>
            </li>

            <li class="nav-item dropdown space_menu">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    TRAINING ADMIN
                </a>
                <ul class="dropdown-menu" style="background-color: #3B619F;font-size: 16px;">
                    <li><a class="dropdown-item" href="{{ route('TrainingManagementAppTP') }}">TRAINING<br> APPLICATION</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="{{ route('training-partner.training-list') }}">TRAINING<br> MGMT</a></li>
                </ul>
            </li>
            <li class="nav-item dropdown space_menu">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    EXAM<br>MGMT
                </a>
                <ul class="dropdown-menu" style="background-color: #3B619F;font-size: 16px;">
                    <li><a class="dropdown-item" href="{{ route('exam-list') }}">LIST<br>OF<br>TRAINING</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                </ul>
            </li>
            <li class="nav-item mg-sm space_menu">
                <a class="nav-link" href="{{ route('questionbankrequestlist') }}">QUESTION <br>BANK
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            {{--<li class="nav-item mg-sm ">
                        <a class="nav-link" href="{{ route('exam-list') }}">EXAM MANAGEMENT
            <span class="sr-only">(current)</span>
            </a>
            </li>

            </li>
            <li class="nav-item mg-sm ">
                <a class="nav-link" href="{{ route('TP-UpdateExamResult-List',TP-UpdateExamResult-List) }}">TRAINING REPORT
                    <span class="sr-only">(current)</span>
                </a>
            </li> --}}
            <li class="nav-item mg-sm user-menu dropdown space_menu leftmenuTP">
                <a class="nav-link dropdown-toggle" style="color: aqua" href="#" id="userDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    {{ Auth::user()->fullname ?? Auth::user()->email }}
                </a>
                <ul class="dropdown-menu" aria-labelledby="userDropdown" style="background-color: #3B619F;font-size: 16px;">
                    <li><a class="dropdown-item" href="{{ route('user-profile', ['id' => Auth::user()->id]) }}">User Information</a></li>
                    <li><a class="dropdown-item" href="{{ route('change-password') }}">Change Password</a></li>
                    <li><a class="dropdown-item" href="{{ route('trainer-attachment') }}">Trainer Attachment</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li class="nav-item mg-sm" >
                        <div style="margin-left:20px">
                            <a class="btn btn-danger dropdown-toggle" href="{{ route('user-logout') }}" id="accountDropdown" role="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="none">
                                <path d="M15.8427 16.7449L15.8428 16.7433C15.856 16.6083 15.7574 16.4834 15.6131 16.4696L15.6107 16.4694C15.4889 16.4572 15.3521 16.5626 15.3382 16.6995C15.1871 18.3325 14.7162 19.5707 13.6953 20.3768C12.6927 21.1685 11.2583 21.4619 9.38047 21.4619H9.25047C7.18084 21.4619 5.63005 21.1011 4.61567 20.0867C3.60129 19.0723 3.24047 17.5215 3.24047 15.4519V8.93187C3.24047 6.86224 3.60129 5.31145 4.61567 4.29707C5.63005 3.28269 7.18084 2.92188 9.25047 2.92188H9.38047C11.2687 2.92188 12.7102 3.22032 13.7138 4.02721C14.7323 4.84617 15.1964 6.10222 15.3377 7.75845C15.3606 7.91834 15.4876 8.00608 15.6131 7.99413L15.6131 7.99399L15.6248 7.99315C15.7542 7.9839 15.8616 7.86821 15.852 7.72068C15.6884 5.81318 15.1003 4.52208 14.1004 3.69456C13.0893 2.85769 11.5704 2.42188 9.39047 2.42188H9.26047C6.863 2.42188 5.27045 2.9365 4.26778 3.93918C3.2651 4.94186 2.75047 6.5344 2.75047 8.93187V15.4519C2.75047 17.8494 3.2651 19.4419 4.26778 20.4446C5.27045 21.4473 6.863 21.9619 9.26047 21.9619H9.39047C11.556 21.9619 13.0664 21.5333 14.0755 20.7095C15.0736 19.8948 15.6653 18.6239 15.8427 16.7449Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                <path d="M9.49988 12.4414H20.8799C21.0137 12.4414 21.1299 12.3253 21.1299 12.1914C21.1299 12.0575 21.0137 11.9414 20.8799 11.9414H9.49988C9.36602 11.9414 9.24988 12.0575 9.24988 12.1914C9.24988 12.3253 9.36602 12.4414 9.49988 12.4414Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                <path d="M18.6496 16.2913C18.8396 16.2913 19.0296 16.2213 19.1796 16.0713L22.5296 12.7213C22.8196 12.4313 22.8196 11.9513 22.5296 11.6613L19.1796 8.31125C18.8896 8.02125 18.4096 8.02125 18.1196 8.31125C17.8296 8.60125 17.8296 9.08125 18.1196 9.37125L20.9396 12.1913L18.1196 15.0113C17.8296 15.3012 17.8296 15.7813 18.1196 16.0713C18.2596 16.2213 18.4596 16.2913 18.6496 16.2913Z" fill="#F9FAFB"/>
                            </svg>
                              <span style="font-size:12px;"> LOG OUT </span>
                            </a>
                        </div>
                        
                    </li>        
                    {{-- <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">LOGOUT</a>
                </li> --}}
                </ul>
            </li>

            @elseif(auth()->user()->user_type == '4') {{-- Trainer --}}
            <li class="nav-item mg-sm space_menu">
                <a class="nav-link" href="{{route('trainer-training-schedule-history')}}">DASHBOARD
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item mg-sm space_menu">
                <a class="nav-link" href="{{route('list-book')}}">MARKET PLACE
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item mg-sm space_menu">
                <a class="nav-link" href="{{route('trainer.dashboard')}}">EVENT
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item mg-sm user-menu dropdown space_menu leftmenuTrainer">
                <a class="nav-link dropdown-toggle" style="color: aqua" href="#" id="userDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    {{ Auth::user()->fullname ?? Auth::user()->email }}
                </a>
                <ul class="dropdown-menu" aria-labelledby="userDropdown" style="background-color: #3B619F;font-size: 16px;">
                    <li><a class="dropdown-item" href="{{ route('user-profile') }}">User Information</a></li>
                    <li><a class="dropdown-item" href="{{ route('change-password') }}">Change Password</a></li>
                    <li><a class="dropdown-item" href="{{ route('trainer-attachment') }}">Trainer Attachment</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li class="nav-item mg-sm" >
                        <div style="margin-left:20px">
                            <a class="btn btn-danger dropdown-toggle" href="{{ route('user-logout') }}" id="accountDropdown" role="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="none">
                                <path d="M15.8427 16.7449L15.8428 16.7433C15.856 16.6083 15.7574 16.4834 15.6131 16.4696L15.6107 16.4694C15.4889 16.4572 15.3521 16.5626 15.3382 16.6995C15.1871 18.3325 14.7162 19.5707 13.6953 20.3768C12.6927 21.1685 11.2583 21.4619 9.38047 21.4619H9.25047C7.18084 21.4619 5.63005 21.1011 4.61567 20.0867C3.60129 19.0723 3.24047 17.5215 3.24047 15.4519V8.93187C3.24047 6.86224 3.60129 5.31145 4.61567 4.29707C5.63005 3.28269 7.18084 2.92188 9.25047 2.92188H9.38047C11.2687 2.92188 12.7102 3.22032 13.7138 4.02721C14.7323 4.84617 15.1964 6.10222 15.3377 7.75845C15.3606 7.91834 15.4876 8.00608 15.6131 7.99413L15.6131 7.99399L15.6248 7.99315C15.7542 7.9839 15.8616 7.86821 15.852 7.72068C15.6884 5.81318 15.1003 4.52208 14.1004 3.69456C13.0893 2.85769 11.5704 2.42188 9.39047 2.42188H9.26047C6.863 2.42188 5.27045 2.9365 4.26778 3.93918C3.2651 4.94186 2.75047 6.5344 2.75047 8.93187V15.4519C2.75047 17.8494 3.2651 19.4419 4.26778 20.4446C5.27045 21.4473 6.863 21.9619 9.26047 21.9619H9.39047C11.556 21.9619 13.0664 21.5333 14.0755 20.7095C15.0736 19.8948 15.6653 18.6239 15.8427 16.7449Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                <path d="M9.49988 12.4414H20.8799C21.0137 12.4414 21.1299 12.3253 21.1299 12.1914C21.1299 12.0575 21.0137 11.9414 20.8799 11.9414H9.49988C9.36602 11.9414 9.24988 12.0575 9.24988 12.1914C9.24988 12.3253 9.36602 12.4414 9.49988 12.4414Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                <path d="M18.6496 16.2913C18.8396 16.2913 19.0296 16.2213 19.1796 16.0713L22.5296 12.7213C22.8196 12.4313 22.8196 11.9513 22.5296 11.6613L19.1796 8.31125C18.8896 8.02125 18.4096 8.02125 18.1196 8.31125C17.8296 8.60125 17.8296 9.08125 18.1196 9.37125L20.9396 12.1913L18.1196 15.0113C17.8296 15.3012 17.8296 15.7813 18.1196 16.0713C18.2596 16.2213 18.4596 16.2913 18.6496 16.2913Z" fill="#F9FAFB"/>
                            </svg>
                              <span style="font-size:12px;"> LOG OUT </span>
                            </a>
                        </div>
                        
                    </li>        
                    {{-- <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">LOGOUT</a>
            </li> --}}
            </ul>
            </li>
            {{-- <li class="nav-item mg-sm ">
                        <a class="nav-link" href="{{ route('trainer-training-schedule-history') }}">TRAINING SCHEDULE & HISTORY
            <span class="sr-only">(current)</span>
            </a>
            </li>
            <li class="nav-item mg-sm ">
                <a class="nav-link" href="{{ route('trainer-AttendanceList') }}">LIST ATTENDANCE
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item mg-sm ">
                <a class="nav-link" href="{{ route('trainer-UpdateExamResult') }}">UPDATE EXAM RESULTS
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item mg-sm ">
                <a class="nav-link" href="{{ route('trainer-TrainingReport') }}">TRAINING REPORT
                    <span class="sr-only">(current)</span>
                </a>
            </li> --}}
            @elseif(auth()->user()->user_type == '5') {{-- Participant --}}
            <li class="nav-item mg-sm space_menu">
                <a class="nav-link" href="{{route('participant.dashboard')}}">HOME
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item mg-sm space_menu">
                <a class="nav-link" href="{{route('list-book')}}">MARKET PLACE
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item mg-sm user-menu dropdown space_menu leftmenuParticipant ">
               
                <a class="nav-link dropdown-toggle" style="color: aqua" href="#" id="userDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    {{ Auth::user()->fullname ?? Auth::user()->company()->first()->name }}
                </a>
               
                <ul class="dropdown-menu" aria-labelledby="userDropdown" style="background-color: #3B619F;font-size: 16px;">
                    <li><a class="dropdown-item" href="{{ route('user-profile',['id' => Auth::user()->id]) }}">User Information</a></li>
                    <li><a class="dropdown-item" href="{{ route('change-password') }}">Change Password</a></li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li class="nav-item mg-sm" >
                        <div style="margin-left:20px">
                            <a class="btn btn-danger dropdown-toggle" href="{{ route('user-logout') }}" id="accountDropdown" role="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="none">
                                <path d="M15.8427 16.7449L15.8428 16.7433C15.856 16.6083 15.7574 16.4834 15.6131 16.4696L15.6107 16.4694C15.4889 16.4572 15.3521 16.5626 15.3382 16.6995C15.1871 18.3325 14.7162 19.5707 13.6953 20.3768C12.6927 21.1685 11.2583 21.4619 9.38047 21.4619H9.25047C7.18084 21.4619 5.63005 21.1011 4.61567 20.0867C3.60129 19.0723 3.24047 17.5215 3.24047 15.4519V8.93187C3.24047 6.86224 3.60129 5.31145 4.61567 4.29707C5.63005 3.28269 7.18084 2.92188 9.25047 2.92188H9.38047C11.2687 2.92188 12.7102 3.22032 13.7138 4.02721C14.7323 4.84617 15.1964 6.10222 15.3377 7.75845C15.3606 7.91834 15.4876 8.00608 15.6131 7.99413L15.6131 7.99399L15.6248 7.99315C15.7542 7.9839 15.8616 7.86821 15.852 7.72068C15.6884 5.81318 15.1003 4.52208 14.1004 3.69456C13.0893 2.85769 11.5704 2.42188 9.39047 2.42188H9.26047C6.863 2.42188 5.27045 2.9365 4.26778 3.93918C3.2651 4.94186 2.75047 6.5344 2.75047 8.93187V15.4519C2.75047 17.8494 3.2651 19.4419 4.26778 20.4446C5.27045 21.4473 6.863 21.9619 9.26047 21.9619H9.39047C11.556 21.9619 13.0664 21.5333 14.0755 20.7095C15.0736 19.8948 15.6653 18.6239 15.8427 16.7449Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                <path d="M9.49988 12.4414H20.8799C21.0137 12.4414 21.1299 12.3253 21.1299 12.1914C21.1299 12.0575 21.0137 11.9414 20.8799 11.9414H9.49988C9.36602 11.9414 9.24988 12.0575 9.24988 12.1914C9.24988 12.3253 9.36602 12.4414 9.49988 12.4414Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                <path d="M18.6496 16.2913C18.8396 16.2913 19.0296 16.2213 19.1796 16.0713L22.5296 12.7213C22.8196 12.4313 22.8196 11.9513 22.5296 11.6613L19.1796 8.31125C18.8896 8.02125 18.4096 8.02125 18.1196 8.31125C17.8296 8.60125 17.8296 9.08125 18.1196 9.37125L20.9396 12.1913L18.1196 15.0113C17.8296 15.3012 17.8296 15.7813 18.1196 16.0713C18.2596 16.2213 18.4596 16.2913 18.6496 16.2913Z" fill="#F9FAFB"/>
                            </svg>
                              <span style="font-size:12px;"> LOG OUT </span>
                            </a>
                        </div>
                        
                    </li>        
                    {{-- <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">LOGOUT</a>
                    </li> --}}
                </ul>
               
            </li>
            {{-- -added menu --}}
         
            @endif

				<li class="nav-item dropdown active">
				
				</li>
				</ul>
			</div>
            {{-- style="color: aqua" --}}
            <div>
              
           
                {{-- <li class="nav-item mg-sm user-menu dropdown space_menu ">
                    <a class="nav-link dropdown-toggle" style="color: rgb(229, 235, 235);" href="#" id="userDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        {{ Auth::user()->fullname ?? Auth::user()->company()->first()->name }}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="userDropdown" style="background-color: #3B619F;font-size: 16px;">
                        <li  class="nav-item mg-sm space_menu"><a class="dropdown-item" href="{{ route('user-profile',['id' => Auth::user()->id]) }}">User<br> Information</a></li>
                        <li  class="nav-item mg-sm space_menu"><a class="dropdown-item" href="{{ route('change-password') }}">Change <br>Password</a></li>
                        <li  class="nav-item mg-sm space_menu"><a class="dropdown-item" href="{{ route('trainer-attachment') }}">Trainer <br>Attachment</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>

                        <li class="nav-item mg-sm" >
                            <div style="margin-left:20px">
                                <a class="btn btn-danger dropdown-toggle" href="{{ route('user-logout') }}" id="accountDropdown" role="button">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="none">
                                    <path d="M15.8427 16.7449L15.8428 16.7433C15.856 16.6083 15.7574 16.4834 15.6131 16.4696L15.6107 16.4694C15.4889 16.4572 15.3521 16.5626 15.3382 16.6995C15.1871 18.3325 14.7162 19.5707 13.6953 20.3768C12.6927 21.1685 11.2583 21.4619 9.38047 21.4619H9.25047C7.18084 21.4619 5.63005 21.1011 4.61567 20.0867C3.60129 19.0723 3.24047 17.5215 3.24047 15.4519V8.93187C3.24047 6.86224 3.60129 5.31145 4.61567 4.29707C5.63005 3.28269 7.18084 2.92188 9.25047 2.92188H9.38047C11.2687 2.92188 12.7102 3.22032 13.7138 4.02721C14.7323 4.84617 15.1964 6.10222 15.3377 7.75845C15.3606 7.91834 15.4876 8.00608 15.6131 7.99413L15.6131 7.99399L15.6248 7.99315C15.7542 7.9839 15.8616 7.86821 15.852 7.72068C15.6884 5.81318 15.1003 4.52208 14.1004 3.69456C13.0893 2.85769 11.5704 2.42188 9.39047 2.42188H9.26047C6.863 2.42188 5.27045 2.9365 4.26778 3.93918C3.2651 4.94186 2.75047 6.5344 2.75047 8.93187V15.4519C2.75047 17.8494 3.2651 19.4419 4.26778 20.4446C5.27045 21.4473 6.863 21.9619 9.26047 21.9619H9.39047C11.556 21.9619 13.0664 21.5333 14.0755 20.7095C15.0736 19.8948 15.6653 18.6239 15.8427 16.7449Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                    <path d="M9.49988 12.4414H20.8799C21.0137 12.4414 21.1299 12.3253 21.1299 12.1914C21.1299 12.0575 21.0137 11.9414 20.8799 11.9414H9.49988C9.36602 11.9414 9.24988 12.0575 9.24988 12.1914C9.24988 12.3253 9.36602 12.4414 9.49988 12.4414Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                    <path d="M18.6496 16.2913C18.8396 16.2913 19.0296 16.2213 19.1796 16.0713L22.5296 12.7213C22.8196 12.4313 22.8196 11.9513 22.5296 11.6613L19.1796 8.31125C18.8896 8.02125 18.4096 8.02125 18.1196 8.31125C17.8296 8.60125 17.8296 9.08125 18.1196 9.37125L20.9396 12.1913L18.1196 15.0113C17.8296 15.3012 17.8296 15.7813 18.1196 16.0713C18.2596 16.2213 18.4596 16.2913 18.6496 16.2913Z" fill="#F9FAFB"/>
                                </svg>
                                <span style="font-size:12px;"> LOG OUT </span>
                                </a>
                            </div>
                            
                        </li>        
                    
                    </ul>
                </li> --}}



                {{-- <li class="nav-item dropdown space_menu user-menu ">
                    
                    <a class="nav-link dropdown-toggle" style="color: rgb(229, 235, 235);" href="#" id="userDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        {{ Auth::user()->fullname ?? Auth::user()->company()->first()->name }}
                    </a>
                  
                    <ul class="dropdown-menu" style="background-color: #3B619F;font-size: 16px;">
                        <li  class="nav-item mg-sm space_menu"><a class="dropdown-item" href="{{ route('user-profile',['id' => Auth::user()->id]) }}">User<br> Information</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li  class="nav-item mg-sm space_menu"><a class="dropdown-item" href="{{ route('change-password') }}">Change <br>Password</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li  class="nav-item mg-sm space_menu"><a class="dropdown-item" href="{{ route('trainer-attachment') }}">Trainer <br>Attachment</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li class="nav-item mg-sm" >
                            <div style="margin-left:20px">
                                <a class="btn btn-danger dropdown-toggle" href="{{ route('user-logout') }}" id="accountDropdown" role="button">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="none">
                                    <path d="M15.8427 16.7449L15.8428 16.7433C15.856 16.6083 15.7574 16.4834 15.6131 16.4696L15.6107 16.4694C15.4889 16.4572 15.3521 16.5626 15.3382 16.6995C15.1871 18.3325 14.7162 19.5707 13.6953 20.3768C12.6927 21.1685 11.2583 21.4619 9.38047 21.4619H9.25047C7.18084 21.4619 5.63005 21.1011 4.61567 20.0867C3.60129 19.0723 3.24047 17.5215 3.24047 15.4519V8.93187C3.24047 6.86224 3.60129 5.31145 4.61567 4.29707C5.63005 3.28269 7.18084 2.92188 9.25047 2.92188H9.38047C11.2687 2.92188 12.7102 3.22032 13.7138 4.02721C14.7323 4.84617 15.1964 6.10222 15.3377 7.75845C15.3606 7.91834 15.4876 8.00608 15.6131 7.99413L15.6131 7.99399L15.6248 7.99315C15.7542 7.9839 15.8616 7.86821 15.852 7.72068C15.6884 5.81318 15.1003 4.52208 14.1004 3.69456C13.0893 2.85769 11.5704 2.42188 9.39047 2.42188H9.26047C6.863 2.42188 5.27045 2.9365 4.26778 3.93918C3.2651 4.94186 2.75047 6.5344 2.75047 8.93187V15.4519C2.75047 17.8494 3.2651 19.4419 4.26778 20.4446C5.27045 21.4473 6.863 21.9619 9.26047 21.9619H9.39047C11.556 21.9619 13.0664 21.5333 14.0755 20.7095C15.0736 19.8948 15.6653 18.6239 15.8427 16.7449Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                    <path d="M9.49988 12.4414H20.8799C21.0137 12.4414 21.1299 12.3253 21.1299 12.1914C21.1299 12.0575 21.0137 11.9414 20.8799 11.9414H9.49988C9.36602 11.9414 9.24988 12.0575 9.24988 12.1914C9.24988 12.3253 9.36602 12.4414 9.49988 12.4414Z" fill="#F9FAFB" stroke="#F9FAFB"/>
                                    <path d="M18.6496 16.2913C18.8396 16.2913 19.0296 16.2213 19.1796 16.0713L22.5296 12.7213C22.8196 12.4313 22.8196 11.9513 22.5296 11.6613L19.1796 8.31125C18.8896 8.02125 18.4096 8.02125 18.1196 8.31125C17.8296 8.60125 17.8296 9.08125 18.1196 9.37125L20.9396 12.1913L18.1196 15.0113C17.8296 15.3012 17.8296 15.7813 18.1196 16.0713C18.2596 16.2213 18.4596 16.2913 18.6496 16.2913Z" fill="#F9FAFB"/>
                                </svg>
                                <span style="font-size:12px;"> LOG OUT </span>
                                </a>
                            </div>
                            
                        </li>        
                    </ul>
                </li> --}}
          
          
            </div>
        </div>
    </nav>
</header>