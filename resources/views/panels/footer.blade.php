<meta name ="viewport" content ="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.SEDA-footer{
color: #FCFCFD;
 font-size: 24px;
 font-family: Inter;
 font-weight: 600;
 text-transform: capitalize;
 line-height: 32px;
 word-wrap: break-word
}

.copy-footer{
color: #FCFCFD;
 font-size: 14px;
 font-family: Inter;
 font-weight: 400;
 line-height: 20px;
 word-wrap: break-word
}

.custom-container {
      width: 100%;
      /* padding: 5px; Adjust padding as needed */
      background: #575656;
      margin-top: 20%;
      position: relative;
      bottom: 0;
      left: 50;
      display: block;
      justify-content: center;
      align-items: center;
    }
</style>


    <div class="custom-container">
    <div style="width: 100%; height: 100%; padding-left: 152px; padding-right: 152px; padding-top: 39px; padding-bottom: 39px; background: #0F172A; flex-direction: column; justify-content: flex-start; align-items: center; gap: 14px; display: inline-flex">
    <div class="SEDA-footer">
        <div style="color: #FCFCFD; font-size: 24px; font-family: Inter; font-weight: 600; text-transform: capitalize; line-height: 32px; word-wrap: break-word">sustainable Energy Development Authority (SEDA) Malaysia
        </div>
    </div>
    <div class="copy-footer">
        <div style="width: 1296px; height: 0px; border: 1px #1E293B solid"></div>
        <div style="text-align: center; color: #FCFCFD; font-size: 14px; font-family: Inter; font-weight: 400; line-height: 20px; word-wrap: break-word">Copyright © SEDA MALAYSIA 2023. ALL RIGHTS RESERVED.</div>
    </div>
    </div>
    </div>



  {{-- <script>
    var year = new Date().getFullYear();
    document.getElementById('copyrightYear').innerHTML = year;
  </script> --}}