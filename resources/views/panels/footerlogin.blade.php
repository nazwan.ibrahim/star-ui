<meta name ="viewport" content ="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    footer {
        position: fixed;
        bottom: 0;

        display: block;
        width: 100px;
        padding: 39px 152px;
        flex-direction: column;
        align-items: center;
        gap: 24px;

        /* background: var(--blue-gray-900, #0F172A); */
        background: #1c5997;
    }
    .fa {
        padding: 20px;
        font-size: 30px;
        width: 100px;
        text-align: center;
        text-decoration: none;
    }

    /* Add a hover effect if you want */
    .fa:hover {
    opacity: 0.7;
    }

    .custom-container {
      align-items: right;
      width: 100%;
      padding: 5px;
      margin-top: 15%;
      background: #575656;
      position: relative !important;
      bottom: 0;
      left: 0;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .button-container {
      width: 280px;
      height: 30px;
      position: relative;
    }
    
    .button-background {
      width: 280px;
      height: 42px;
      left: 0px;
      top: 0px;
      position: absolute;
      background: radial-gradient(70.71% 70.71% at 0.00% 120.71%, white 100%, black 100%);
    }

    .button-wrapper {
      height: 42px;
      padding-top: 12px;
      padding-bottom: 12px;
      padding-left: 90.10px;
      padding-right: 89.90px;
      left: 0px;
      top: 0px;
      position: absolute;
      background: #414040;
      border-radius: 4px;
      overflow: hidden;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }

    .button-content {
      text-align: center;
      color: #B7B7B7;
      font-size: 18px;
      font-family: Rajdhani;
      font-weight: 600;
      text-transform: uppercase;
      line-height: 18px;
      letter-spacing: 0.80px;
      word-wrap: break-word;
    }

    .button-link {
      text-decoration: none; /* Remove underline from the link */
      color: #B7B7B7;
    }

    .fa-icon {
      font-size: 36px; /* Adjust the font size as needed */
      color: #ABABAB; /* Grey color */
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      text-decoration: none; /* Remove underline from the link */
    }

    .icon-container{
        padding-left: 200px; 
        padding-right: 600px;
        align-items: center;
        justify-content: center;
        display: inline-block;

    }

</style>

<div class="custom-container">
<div style="width: 100%; height: 50%; padding-top: 10px; padding-bottom: 10px; padding-left: 20px; padding-right: 600px; background: #575656; justify-content: center; align-items: center; display: inline-block">
    <div style="flex: 1 1 0; align-self: stretch; padding-left: 10px; padding-right: 10.02px; justify-content: flex-start; align-items: flex-start; gap: 40px; display: inline-flex">
        <div style="width: 490.50px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 28px; display: inline-flex">
            <div style="align-self: stretch; height: 128px; flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 16px; display: inline-flex">
                <div style="align-self: stretch; color: #D3D3D3; font-size: 16px; font-family: Rajdhani; font-weight: 600; line-height: 24px; word-wrap: break-word; text-align: left;">SUSTAINABLE ENERGY DEVELOPMENT AUTHORITY <br/>( SEDA ) MALAYSIA</div>
                <div style="width: 171px; height: 0px; border: 2px #EAECF0 solid"></div>
                <div style="width: 324px; color: #D3D3D3; font-size: 14px; font-family: Rajdhani; font-weight: 500; line-height: 24px; letter-spacing: 0.06px; word-wrap: break-word; text-align : left">COPYRIGHT © SEDA MALAYSIA 2019. ALL RIGHTS RESERVED.</div>
            </div>
          
        </div>
        <div style="width: 180px; flex-direction: column; justify-content: flex-start; align-items: flex-start; display: inline-flex">
            <div style="align-self: stretch; height: 31.60px; flex-direction: column; justify-content: flex-start; align-items: flex-start; display: flex">
                <div style="justify-content: flex-start; align-items: flex-start; display: inline-flex">
                    <div style="width: 20px; height: 20px; flex-direction: column; justify-content: flex-start; align-items: flex-start; display: inline-flex">
                        <div style="align-self: stretch; height: 20px; padding-top: 5px; padding-bottom: 5px; padding-right: 10px; flex-direction: column; justify-content: flex-start; align-items: flex-start; display: flex">
                            <div style="justify-content: flex-start; align-items: flex-start; display: inline-flex">
                                <div style="width: 10px; height: 10px; position: relative">
                                    <div style="width: 5.62px; height: 10px; left: 0px; top: 0px; position: absolute; background: rgba(255, 255, 255, 0.40)"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="flex-direction: column; justify-content: flex-start; align-items: flex-start; display: inline-flex">
                        <div style="flex-direction: column; justify-content: flex-start; align-items: flex-start; display: flex">
                        <a href="{{ route('home') }}">
                        <div style="color: #E0E0E0; font-size: 14px; font-family: Rajdhani; font-weight: 600; text-transform: uppercase; line-height: 25.60px; letter-spacing: 0.80px; word-wrap: break-word">
                            Home
                        </div>
                        </a>
                        </div>
                    </div>

            </div>
            <div style="align-self: stretch; height: 36.59px; padding-top: 5px; flex-direction: column; justify-content: flex-start; align-items: flex-start; display: flex">
                <div style="justify-content: flex-start; align-items: flex-start; display: inline-flex">
                    <div style="width: 20px; height: 20px; flex-direction: column; justify-content: flex-start; align-items: flex-start; display: inline-flex">
                        <div style="align-self: stretch; height: 20px; padding-top: 5px; padding-bottom: 5px; padding-right: 10px; flex-direction: column; justify-content: flex-start; align-items: flex-start; display: flex">
                            <div style="justify-content: flex-start; align-items: flex-start; display: inline-flex">
                                <div style="width: 10px; height: 10px; position: relative">
                                    <div style="width: 5.62px; height: 10px; left: 0px; top: 0px; position: absolute; background: rgba(255, 255, 255, 0.40)"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="flex-direction: column; justify-content: flex-start; align-items: flex-start; display: inline-flex">
                        <div style="flex-direction: column; justify-content: flex-start; align-items: flex-start; display: flex">
                        <a href="{{ route('home') }}">
                        <div style="color: #E0E0E0; font-size: 14px; font-family: Rajdhani; font-weight: 600; text-transform: uppercase; line-height: 25.60px; letter-spacing: 0.80px; word-wrap: break-word">
                            About us
                        </div>
                        </a>
                        </div>
                    </div>
                </div>
                
            </div>
            <div style="align-self: stretch; height: 36.60px; padding-top: 5px; flex-direction: column; justify-content: flex-start; align-items: flex-start; display: flex">
                <div style="justify-content: flex-start; align-items: flex-start; display: inline-flex">
                    <div style="width: 20px; height: 20px; flex-direction: column; justify-content: flex-start; align-items: flex-start; display: inline-flex">
                        <div style="align-self: stretch; height: 20px; padding-top: 5px; padding-bottom: 5px; padding-right: 10px; flex-direction: column; justify-content: flex-start; align-items: flex-start; display: flex">
                            <div style="justify-content: flex-start; align-items: flex-start; display: inline-flex">
                                <div style="width: 10px; height: 10px; position: relative">
                                    <div style="width: 5.62px; height: 10px; left: 0px; top: 0px; position: absolute; background: rgba(255, 255, 255, 0.40)"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="flex-direction: column; justify-content: flex-start; align-items: flex-start; display: inline-flex">
                        <div style="padding-right: 0.70px; flex-direction: column; justify-content: flex-start; align-items: flex-start; display: flex">
                        <a href="{{ route('home') }}">
                        <div style="color: #E0E0E0; font-size: 14px; font-family: Rajdhani; font-weight: 600; text-transform: uppercase; line-height: 25.60px; letter-spacing: 0.80px; word-wrap: break-word">
                            Helpdesk
                        </div>
                        </a>
                        </div>
                    </div>

                <div class="icon-container">
                    <div style="align-self: stretch; justify-content: flex-start; align-items: flex-start; gap: 16px; display: inline-flex">
                <div style="width: 80px; height: 80px; position: relative">
                    <div style="width: 80px; height: 80px; left: 5px; top: 0px; position: absolute; background: rgba(11.63, 17.21, 29.17, 0.43); border-radius: 8px"></div>
                    <a href="https://www.facebook.com/SustainableEnergyDevelopmentAuthoritySEDAMalaysia/" class="fa fa-facebook fa-icon"></a>
                </div>
                <div style="width: 80px; height: 80px; position: relative">
                    <div style="width: 80px; height: 80px; left: 0px; top: 0px; position: absolute; background: rgba(11.63, 17.21, 29.17, 0.43); border-radius: 8px"></div>
                    <a href="https://twitter.com/SEDAMalaysia" class="fa fa-twitter fa-icon"></a>
                </div>
                <div style="width: 80px; height: 80px; position: relative">
                    <div style="width: 80px; height: 80px; left: 0px; top: 0px; position: absolute; background: rgba(11.63, 17.21, 29.17, 0.43); border-radius: 8px"></div>
                    <a href="https://www.youtube.com/channel/UCXJ-dWRiLgj_Kh84X0WYFoQ" class="fa fa-youtube fa-icon"></a>
                </div>
                <div style="width: 80px; height: 80px; position: relative">
                    <div style="width: 80px; height: 80px; left: 0px; top: 0px; position: absolute; background: rgba(11.63, 17.21, 29.17, 0.43); border-radius: 8px"></div>
                    <a href="https://www.instagram.com/sedamalaysia/" class="fa fa-instagram fa-icon"></a>
                </div>
</div>
    </div>
</div>
</div>



  {{-- <script>
    var year = new Date(getMonth).getFullYear();
    document.getElementById('copyrightYear').innerHTML = year;
  </script> --}}