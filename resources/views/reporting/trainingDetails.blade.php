@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Training Report Details')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .left-side {
        float: left;
        width: 33%;
    }

    .right-side {
        float: left;
        width: 67%;
    }

    .dataTables_length {
        display: none;
    }

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .filter-dropdown{
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }

    .form-row {  
        width: 80%;
    }

    .form-group label {
        display: block;
        text-align: left;
        margin-bottom: 5px;
    }
</style>
@endsection

@section('content')
<main class="header-top-margin col-md-10 mx-auto">
    <div class="text-center mb-3">
        <h2 class="page-title py-2" style="text-align: left !important; margin-top: 5%;">{{ $training->course_name }}</h2>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="company">Provider : </label>
        </div>

        <div class="form-group col-md-10 py-2">
            <label class="field-title" for="company">{{ $training->company_name }}</label>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="date_start">Start Date : </label>
        </div>

        <div class="form-group col-md-3 py-2">
            <label class="field-title" for="date_start">{{ $training->date_start }}</label>
        </div>
        
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="date_end">End Date : </label>
        </div>

        <div class="form-group col-md-3 py-2">
            <label class="field-title" for="date_end">{{ $training->date_end }}</label>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="start_time">Start Time : </label>
        </div>

        <div class="form-group col-md-3 py-2">
            <label class="field-title" for="start_time">{{ $training->start_time }}</label>
        </div>
        
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="end_time">End Time : </label>
        </div>

        <div class="form-group col-md-3 py-2">
            <label class="field-title" for="end_time">{{ $training->end_time }}</label>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="total_participant">Total Participant : </label>
        </div>

        <div class="form-group col-md-10 py-2">
            <label class="field-title" for="total_participant">{{ $training->total_participant }}</label>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="venue">Venue : </label>
        </div>

        <div class="form-group col-md-10 py-2">
            <label class="field-title" for="venue">{{ $training->venue }} , {{ $training->venue_address }} , {{ $training->state }}.</label>
        </div>
    </div>

    <div class="text-center mb-3 py-2">
        <h2 class="page-title" style="text-align: left !important; margin-top: 5%;">Training Report</h2>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="course_name">Training Name : </label>
        </div>

        <div class="form-group col-md-10 py-2">
            <label class="field-title" for="course_name">{{ $training->course_name }}</label>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="course_category">Course Category : </label>
        </div>

        <div class="form-group col-md-10 py-2">
            <label class="field-title" for="course_category">{{ $training->course_category }}</label>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="training_code">Training Code : </label>
        </div>

        <div class="form-group col-md-3 py-2">
            <label class="field-title" for="training_code">{{ $training->training_code }}</label>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="batch_no_full">Batch Number : </label>
        </div>

        <div class="form-group col-md-3 py-2">
            <label class="field-title" for="batch_no_full">{{ $training->batch_no_full }}</label>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="course_date">Course Date : </label>
        </div>

        <div class="form-group col-md-3 py-2">
            <label class="field-title" for="course_date">{{ $training->date_end }}</label>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="training_mode">Training Mode : </label>
        </div>

        <div class="form-group col-md-3 py-2">
            <label class="field-title" for="training_mode">{{ $training->mode_name }}</label>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="training_type">Training Type : </label>
        </div>

        <div class="form-group col-md-3 py-2">
            <label class="field-title" for="training_type">{{ $training->type_name }}</label>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="training_type">Competent Trainers :</label>
        </div>

        <div class="form-group col-md-3 py-2">
            @forelse ($trainerList as $trainer)
                <label class="field-title" for="trainer_list">{{ $trainer->fullname }}</label><br>
            @empty
                <label class="field-title" for="trainer_list">No trainers available</label>
            @endforelse
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="training_description">Training Description : </label>
        </div>

        <div class="form-group col-md-10 py-2">
            <label class="field-title" for="training_description">
                {{ $training->training_description ?? 'No training description entered.' }}
            </label>
        </div>
    </div>
</main>
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    
@endsection

@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>

<script src="{{ asset('js/reporting/reporting.js') }}"></script>
@endsection