@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Payment Report')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .dataTables_length {
        display: none;
    }

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .filter-dropdown{
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }

    .paid-status {
        border-radius: 8px;
        border: 1px solid #18eeaa;
        background: var(--success-25, #f6faf7);
        padding: 4px 8px;
        color: #18eeaa;
    }

    .failed-status {
        border-radius: 8px;
        border: 1px solid #F04438;
        background: var(--success-25, #FFFBFA);
        padding: 4px 8px;
        color: #F04438;
    }

    .hidden-column {
        display: none;
    }
</style>
@endsection

@section('content')
<main class="header-top-margin col-md-10 mx-auto">
    <div class="text-center mb-3">
        <h2 class="page-title py-2" style="text-align: left !important; margin-top: 5%;">Payment Report</h2>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="year_filter"><b>Year</b></label>
            <select class="form-control" id="year_filter">
                <option value="">All</option>
            </select>
        </div>

        <div class="form-group col-md-2 py-2">
            <label for="course_filter"><b>Course</b></label>
            <select class="form-control" id="course_filter" name="course_filter">
                <option value="">All</option>
            </select>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="state_filter"><b>State</b></label>
            <select class="form-control" id="state_filter">
                <option value="">All</option>
                <option value="Johor">Johor</option>
                <option value="Kedah">Kedah</option>
                <option value="Kelantan">Kelantan</option>
                <option value="Melaka">Melaka</option>
                <option value="Negeri Sembilan">Negeri Sembilan</option>
                <option value="Pahang">Pahang</option>
                <option value="Perak">Perak</option>
                <option value="Perlis">Perlis</option>
                <option value="Pulau Pinang">Pulau Pinang</option>
                <option value="Sabah">Sabah</option>
                <option value="Sarawak">Sarawak</option>
                <option value="Selangor">Selangor</option>
                <option value="Terengganu">Terengganu</option>
                <option value="Kuala Lumpur">Kuala Lumpur</option>
                <option value="Labuan">Labuan</option>
                <option value="Putrajaya">Putrajaya</option>
            </select>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="payment_filter"><b>Payment Made</b></label>
            <select class="form-control" id="payment_filter">
                <option value="">All</option>
                <option value="LO">LO</option>
                <option value="Credit">Credit</option>
            </select>
        </div>

        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="endDate_filter"><b>Training End Date</b></label>
            <input type="date" class="form-control" name="endDate_filter" id="endDate_filter">
        </div>
    </div>

    <div class="table-responsive">
        <div id="paymentReportList_wrapper" class="dataTables_wrapper no-footer">
            <table class="table table-bordered table-striped dataTable no-footer" id="paymentReportList" style="width: 100%;">
                <thead class="table-header">
                    <tr>
                        <th class="table-title text-center" width="40">No.</th>
                        <th class="table-title text-center">Programme</th>
                        <th class="table-title text-center">Start Date</th>
                        <th class="table-title text-center">End Date</th>
                        <th class="table-title text-center">Venue</th>
                        <th class="table-title text-center">Fee</th>
                        <th class="table-title text-center">Payment Made</th>
                        <th class="table-title text-center" style="display: none;">Year</th>
                        <th class="table-title text-center" style="display: none;">Region</th>
                        <th class="table-title text-center" style="display: none;">State</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</main>
@endsection


@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    
@endsection



@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>

<script src="{{ asset('js/reporting/reporting.js') }}"></script>

<script>
    const currentYear = new Date().getFullYear();
    const yearDropdown = document.getElementById("year_filter");

    for (let year = currentYear; year >= 1970; year--) {
        const option = document.createElement("option");
        option.value = year;
        option.text = year;
        yearDropdown.add(option);
    }
</script>
@endsection