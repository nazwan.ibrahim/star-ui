@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Training Report')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .dataTables_length {
        display: none;
    }

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .filter-dropdown{
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }

    .form-row {
        display: flex !important;
    }

    .form-group label {
        display: block;
        text-align: left;
        margin-bottom: 5px;
    }

    .hidden-column {
        display: none;
    }
</style>
@endsection

@section('content')
<main class="header-top-margin col-md-10 mx-auto">
    <div class="text-center mb-3">
        <h2 class="page-title py-2" style="text-align: left !important; margin-top: 5%;">Training Report</h2>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="year_filter"><b>Year</b></label>
            <select class="form-control" id="year_filter">
                <option value="">All</option>
            </select>
        </div>

        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="trainingType_filter"><b>Training Type</b></label>
            <select class="form-control" id="trainingType_filter">
                <option value="">All</option>
                <option value="1">Renewable Energy</option>
                <option value="2">Energy Efficiency</option>
                <option value="3">Sustainable Energy</option>
            </select>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="trainingOption_filter"><b>Training Option</b></label>
            <select class="form-control" id="trainingOption_filter">
                <option value="">All</option>
                <option value="1">Inhouse</option>
                <option value="2">Training Partner</option>
            </select>
        </div>
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="region_filter"><b>Region</b></label>
            <select class="form-control" id="region_filter">
                <option value="">All</option>
                <option value="Northern Region">Northern Region</option>
                <option value="Central Region">Central Region</option>
                <option value="Eastern Region">Eastern Region</option>
                <option value="Southern Region">Southern Region</option>
                <option value="East Malaysia">East Malaysia</option>
            </select>
        </div>
    </div>

    <div class="form-row col-md-12">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="status_filter"><b>Status</b></label>
            <select class="form-control" id="status_filter">
                <option value="">All</option>
                <option value="Active">Active</option>
                <option value="Completed">Completed</option>
                <option value="Cancelled">Cancelled</option>
            </select>
        </div>

        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="batch_filter"><b>Batch</b></label>
            <select class="form-control" id="batch_filter">
                <option value="">All</option>
            </select>
        </div>
    </div>

    <div class="table-responsive">
        <div id="trainingReportList_wrapper" class="dataTables_wrapper no-footer">
            <table class="table table-bordered table-striped dataTable no-footer" id="trainingReportList" style="width: 100%;">
                <thead class="table-header">
                    <tr>
                        <th class="table-title text-center" width="40">No.</th>
                        <th class="table-title text-center">Programme</th>
                        <th class="table-title text-center">Start Date</th>
                        <th class="table-title text-center">End Date</th>
                        <th class="table-title text-center">Venue</th>
                        <th class="table-title text-center">Participant</th>
                        <th class="table-title text-center">Fee</th>
                        <th class="table-title text-center">Status</th>
                        <th class="table-title text-center" style="display: none;">Region</th>
                        <th class="table-title text-center" style="display: none;">Batch</th>
                        <th class="table-title text-center" style="display: none;">Year</th>
                        <th class="table-title text-center" style="display: none;">Training Option</th>
                        <th class="table-title text-center" style="display: none;">Training Type</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</main>
@endsection


@section('vendor-scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/select/1.3.4/js/dataTables.select.min.js"></script>
@endsection


@section('page-scripts')
<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>

<script src="{{ asset('js/reporting/TPreporting.js') }}"></script>

<script>
    const currentYear = new Date().getFullYear();
    const yearDropdown = document.getElementById("year_filter");

    for (let year = currentYear; year >= 1970; year--) {
        const option = document.createElement("option");
        option.value = year;
        option.text = year;
        yearDropdown.add(option);
    }
</script>
@endsection