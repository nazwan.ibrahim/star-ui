@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','User Profile')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
        #sectionFormMain>div>div:nth-child(5)>div.col-4 {
            margin-top: 19px;
        }
    }


    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

    .btn-add-2:hover {
        color: #fff;
    }

    .btn-add-2:disabled,
    .btn-add-2[disabled],
    .btn-add-2:hover {
        color: #fff !important;
        opacity: 0.5;
    }

    .btn-secondary-6 {
        background-color: #fff;
        color: #5A8DEE;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        padding: 10px 20px;
        font-size: 16px;
        font-family: poppins-semibold, sans-serif;
        line-height: 20px;
        text-align: left;
    }

    .btn-secondary-6:hover {
        background-color: #5A8DEE;
        color: #fff;
    }

    .font-title {
        color: var(--gray-950, #0C111D);
        width: 300px;
        font-family: Inter;
        font-size: 24px;
        font-style: normal;
        font-weight: 600;
        line-height: 32px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 28px;
        font-style: normal;
        font-weight: 600;
    }

    .contentUpdate {
        color: #454545;
        font-family: Open Sans;
        font-size: 22px;
        font-style: normal;
        font-weight: 600;
        line-height: 28px;
        /* 155.556% */
        text-align: left;
    }

    .contentUpdate2 {
        color: #454545;
        font-family: Open Sans;
        font-size: 18px;
        font-style: normal;
        font-weight: 600;
        line-height: 28px;
        /* 155.556% */
        text-align: left;
    }

    /* .form-group {
    display: flex;
    align-items: center; /* Vertical alignment */
    /* } */
    */ label {
        width: 100px;
        /* Adjust as needed for your design */
        text-align: left;
        /* Adjust for alignment (left, right, center) */
        /* margin-right: 10px; Spacing between label and input */
    }

    .form-label {
        align-items: left;
        display: flex;
        align-items: center;
        gap: 8px;
        flex: 1 0 0;
        font-family: Open Sans;
    }

    ul {
        list-style-type: none;
        padding: 0;
    }

    li {
        text-align: left;
    }
</style>
@endsection

@section('content')
<!-- Navbar -->
<!-- <nav class="navbar navbar-expand-lg"> -->


<!-- End Navbar -->

<main style="padding:50px;margin:50px;">
    <section class="header-top-margin">
        <div class="card mt-7" style="padding:50px;margin:50px;background-color:#fbf9f9">
            <form method="POST" action="{{ route('save-user-profile', ['id' => $userInfo->id]) }}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label class="poppins-semibold-14 mb-1 page-title" for="updateProfile">Edit Profile</label>
                    <hr>
                </div>

                <div class="row">
                    <div>

                        <div class="row form-list">
                            <div class="form-group ">
                                <label class="poppins-semibold-14 mb-1 contentUpdate" for="userInfo">User Information</label>
                            </div>

                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Salutation<span style="color:#ff0000">*</span></label>
                                        <select class="form-control" id="salutation" name="salutation" required>
                                            <option value=""> -- Select Salutation -- </option>
                                            @foreach($salutation as $salute)
                                            <option value="{{ $salute->id }}" {{ $userInfo->salutation == $salute->salutation ? 'selected' : '' }}>{{ $salute->salutation }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div>
                                        <label for="name" class="form-label">Full Name<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="fullname" name="fullname" value="{{ $userInfo->fullname }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="identity" class="form-label">Identification Type<span style="color:#ff0000">*</span></label>
                                        <select class="form-control" id="identity" name="identity" required>
                                            <option value=""> -- Select Identification Type -- </option>
                                            @foreach($identity as $identity)
                                            <option value="{{ $identity->id_identity }}" {{ $userInfo->identity_type == $identity->identity_type ? 'selected' : '' }}>{{ $identity->identity_type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">MyKad Number/Passport Number<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="ic_no" name="ic_no" value="{{ $userInfo->ic_no }}" required>
                                    </div>
                                </div>

                                
                            </div>

                            <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                    <div>
                                        <label for="" class="form-label">Email Address<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="email" name="email" value="{{ $userInfo->email }}" required readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div>
                                        <label for="name" class="form-label">Phone Number<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="phone_no" name="phone_no" value="{{ $userInfo->phone_no }}" required>
                                    </div>
                                </div>

                               
                            </div>

                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Gender<span style="color:#ff0000">*</span></label>
                                        <select class="form-control" id="gender" name="gender" required>
                                            <option value=""> -- Select Gender -- </option>
                                            @foreach($genders as $gender)
                                            <option value="{{ $gender->id }}" {{ $userInfo->gender == $gender->name ? 'selected' : '' }}>{{ $gender->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Nationality<span style="color:#ff0000">*</span></label>
                                        <select class="form-control" id="nationality" name="nationality" required>
                                            <option value=""> -- Select Nationality -- </option>
                                            @foreach($nationality as $nation)
                                            <option value="{{ $nation->id_nationality }}" {{ $userInfo->nationality == $nation->nationality ? 'selected' : '' }}>{{ $nation->nationality }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div>
                                        <label for="name" class="form-label">Phone Number<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="phone_no" name="phone_no" value="{{ $userInfo->phone_no }}" required>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">IC No/Passport No<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="ic_no" name="ic_no" value="{{ $userInfo->ic_no }}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div>
                                        <label for="name" class="form-label">Certificate Number<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="certificate_no" name="certificate_no" value="{{ $userInfo->certificate_no }}" required readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div>
                                        <label for="name" class="form-label">Certificate Category<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="certificate_no" name="certificate_no" value="{{ $userInfo->certificate_category }}" required readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Address line 1<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="address_1" name="address_1" value="{{ $userInfo->address_1 }}" required>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Address line 2</label>
                                        <input type="text" class="form-control" id="address_2" name="address_2" value="{{ $userInfo->address_2 }}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Postcode<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="postcode" name="postcode" value="{{ $userInfo->postcode }}" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">City<span style="color:#ff0000">*</span></label>
                                        <select class="form-control" id="city" name="city" required>
                                            <option value=""> -- Select City -- </option>
                                            @foreach($cities as $city)
                                            <option value="{{ $city->id }}" {{ $userInfo->cityname == $city->cityname ? 'selected' : '' }}>{{ $city->cityname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">State<span style="color:#ff0000">*</span></label>
                                        <select class="form-control" id="state" name="state" required>
                                            <option value=""> -- Select State -- </option>
                                            @foreach($states as $state)
                                            <option value="{{ $state->id }}" {{ $userInfo->statename == $state->name ? 'selected' : '' }}>{{ $state->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Country<span style="color:#ff0000">*</span></label>
                                        <select class="form-control" id="country" name="country" required>
                                            <option value=""> -- Select Country -- </option>
                                            @foreach($countries as $country)
                                            <option value="{{ $country->id }}" {{ $userInfo->countryname == $country->name ? 'selected' : '' }}>{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <label class="poppins-semibold-14 mb-1 form-label" for="passportpic_name">Passport Photo<span style="color:#ff0000">*</span></label>
                                    <div class="form-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="passportpic_name" name="FailGambarPassport" onchange="previewImage(this)">
                                            <label class="custom-file-label" for="passportpic_name">
                                                Choose file
                                            </label>
                                            <ul>
                                            <li><small class="text-muted">Accepted file types: png, .jpeg, .jpg. Maximum file size: 5MB.</small></li>
                                            <li><small class="text-muted">Coloured passport-sized photograph (in the size of 3.5 cm x 5 cm)</small></li>
                                            <li><small class="text-muted">Passport picture size, Width: 35 mm, Height: 50 mm ; Resolution (DPI), 600</small></li>
                                            </ul>
                                        </div>
                                        <div class="form-group" style="margin-top:10px;">
                                            <div class="border rounded-lg p-3">
                                                <div class="rounded-lg pb-2" style="border: 2px dashed #DFE3E7;">
                                                    <div class="mt-2" style="display: flex; flex-direction: column; align-items: center; margin-left: 15px;">
                                                        @if($userInfo && $userInfo->passportpic_path)
                                                        <img id="imagePreview" src="{{ asset('storage/' . $userInfo->passportpic_path) }}" alt="Passport Photo" style="max-width: 100%; max-height: 150px;">
                                                        @else
                                                        <img id="imagePreview" src="#" alt="Passport Photo" style="max-width: 100%; max-height: 150px; display: none;">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="poppins-semibold-14 mb-1 form-label" for="ICpic_name">Copy of IC<span style="color:#ff0000">*</span></label>
                                    <div class="form-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="ICpic_name" name="FailGambarIC" onchange="updateFileName(this)">
                                            <label class="custom-file-label" for="ICpic_name">
                                                Choose file
                                            </label>
                                        </div>
                                        <div class="form-group" style="margin-top:10px;">
                                            <div class="border rounded-lg p-3">
                                                <div class="rounded-lg pb-2" style="border: 2px dashed #DFE3E7;">
                                                    <div class="mt-2" style="display: flex; flex-direction: column; align-items: center; margin-left: 15px;">
                                                        @if($userInfo && $userInfo->ICpic_path)
                                                        <a href="{{ asset('storage/' . $userInfo->ICpic_path) }}" target="_blank" style="margin-right: 6px;"><i class="fas fa-file-pdf"></i>
                                                            <i>{{ $userInfo->ICpic_name }}</i>
                                                        </a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="poppins-semibold-14 mb-1 contentUpdate" for="jenisCenderamata-0">Educational Information</label>
                            </div>
                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Higher Education Level<span style="color:#ff0000">*</span></label>
                                        <select class="form-control" id="education_level" name="education_level" required>
                                            <option value=""> -- Select a Higher Education Level -- </option>
                                            @foreach($education_level as $education_level)
                                            <option value="{{ $education_level->id }}" {{ $userInfo->education_level == $education_level->education_level ? 'selected' : '' }}>{{ $education_level->education_level }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Qualification<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="qualification" name="qualification" value="{{ $userInfo->qualification }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin:10px;">
                                <div class="col-4">
                                    <label class="poppins-semibold-14 mb-1 form-label" for="pilihSemua">Supporting Documents</label>
                                    <div class="form-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="edu_file" name="edu_file[]" onchange="updateFileName(this)" multiple>
                                            <label class="custom-file-label" for="edu_file">Choose File</label>
                                        </div>
                                        <div class="form-group" style="margin-top:10px;">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>File Name</th>
                                                        <th colspan='2'>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="fileList">
                                                    @if ($viewUploadedFileEdu)
                                                    @foreach ($viewUploadedFileEdu as $index => $file)
                                                    <tr data-file-id="{{ $file->file_id }}">
                                                        <td>{{ $index + 1 }}</td>
                                                        <td>{{ $file->file_name }}</td>
                                                        <td>
                                                            <a href="{{ asset('storage/eduFiles/' . $file->file_name) }}" target="_blank" title="View">
                                                                <i class="fas fa-file-pdf"></i>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0);" class="delete-file" title="Delete">
                                                                <i class="fas fa-trash-alt"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr>
                                                        <td colspan="4">No data available</td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="poppins-semibold-14 mb-1 contentUpdate" for="jenisCenderamata-0">Company Information</label>
                            </div>
                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Company Name<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="companyname" name="companyname" value="{{ $userInfo->companyname }}">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Position</label>
                                        <input type="text" class="form-control" id="position" name="position" value="{{ $userInfo->position }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Fax Number<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="fax_no" name="fax_no" value="{{ $userInfo->fax_no }}">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Phone Number<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="companyphone" name="companyphone" value="{{ $userInfo->companyphone }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Address Line 1<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="company_address_1" name="company_address_1" value="{{ $userInfo->company_address_1 }}">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Address Line 2</label>
                                        <input type="text" class="form-control" id="company_address_2" name="company_address_2" value="{{ $userInfo->company_address_2 }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Postcode<span style="color:#ff0000">*</span></label>
                                        <input type="text" class="form-control" id="companypostcode" name="companypostcode" value="{{ $userInfo->companypostcode }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">City<span style="color:#ff0000">*</span></label>
                                        <select class="form-control" id="companycity" name="companycity">
                                            <option value=""> -- Select City -- </option>
                                            @foreach($cities as $city)
                                            <option value="{{ $city->id }}" {{ $userInfo->companycity == $city->cityname ? 'selected' : '' }}>{{ $city->cityname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin:10px;">
                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">State<span style="color:#ff0000">*</span></label>
                                        <select class="form-control" id="companystate" name="companystate">
                                            <option value=""> -- Select State -- </option>
                                            @foreach($states as $state)
                                            <option value="{{ $state->id }}" {{ $userInfo->companystate == $state->name ? 'selected' : '' }}>{{ $state->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="">
                                        <label for="name" class="form-label">Country<span style="color:#ff0000">*</span></label>
                                        <select class="form-control" id="companycountry" name="companycountry">
                                            <option value=""> -- Select Country -- </option>
                                            @foreach($countries as $country)
                                            <option value="{{ $country->id }}" {{ $userInfo->companycountry == $country->name ? 'selected' : '' }}>{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex justify-content-end pt-3 pb-2">
                                <div style="margin:10px;">
                                    <a href="{{ route('user-profile') }}" class="btn btn-secondary-6 btn-block">Cancel</a>
                                </div>
                                <div style="margin:10px;">
                                    <button type="submit" class="btn btn-secondary-6 btn-block" id="hantarBtn">
                                        <span>Save</span>
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </form>
    </section>
</main>
@endsection

@section('vendor-scripts')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
@endsection

@section('page-scripts')
<script>
    let host = "{{ env('API_SERVER') }}";
</script>

<script>
    function previewImage(input) {
        var fileInput = input;
        var preview = document.getElementById('imagePreview');
        var label = document.querySelector('.custom-file-label');

        // Clear existing preview
        preview.style.display = 'none';
        preview.src = '';

        // Update label with the selected file name
        label.textContent = fileInput.files[0] ? fileInput.files[0].name : 'Choose file';

        // Show preview if an image file is selected
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                preview.src = e.target.result;
                preview.style.display = 'block';
            };

            reader.readAsDataURL(fileInput.files[0]);
        }
    }

    function updateFileName(input) {
        var label = input.nextElementSibling;
        var fileName = input.files[0] ? input.files[0].name : 'Choose File';
        label.textContent = fileName;
    }

    function updatePostcode() {
    var selectedPostcode = $('#postcode').val();

    $.ajax({
        url: '/get-postcode/' + selectedPostcode,
        type: 'GET',
        success: function (response) {
            console.log('RESPONSE', response);

            // Clear existing options
            $('#state').empty();
            $('#city').empty();
            $('#country').empty();

            // Append new options
            $('#state').append('<option value="' + response.state_id + '">' + response.state + '</option>');
            $('#city').append('<option value="' + response.city_id + '">' + response.cityname + '</option>');
            $('#country').append('<option value="' + response.country_id + '">' + response.country + '</option>');
        },
        error: function (error) {
            console.error('Error fetching postcode data:', error);
        }
    });
}
</script>
@endsection