@extends('layouts.contentLayoutMaster')

{{-- page Title --}}
@section('title','User Profile')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.form-group {
    display: flex;
    align-items: center; /* Vertical alignment */
}

label {
    width: 100px; /* Adjust as needed for your design */
    text-align: left; /* Adjust for alignment (left, right, center) */
    /* margin-right: 10px; Spacing between label and input */
}

.form-control{
    width: 400px; /* Adjust the width to your preference */
    height: 30px; /* Adjust the height to your preference */
    font-size: 14px; /* Adjust the font size to your preference */
}

.card {
  position: stretch;
  display: flex;
  flex-direction: column;
  width: 50%;
  word-wrap: break-word;
  background-color: #fff;
  background-clip: border-box;
  border: 1px solid rgba(0, 0, 0, 0.125);
  border-radius: 0.25rem;
}



</style>
@endsection

@section('content')
<main style="padding:50px;margin:50px;">
<section class="header-top-margin">
@if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
    @else
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
    @endif
<div class="card">

<label class="poppins-semibold-14 mb-1 font-title" >Change Password</label>

<div class="card-body">
<form id="changePasswordForm" action="{{ route('update-change-password') }}" method="post">
    @csrf


    <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
        <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
            <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Current Password</div>
            <div style="align-self: stretch; padding-left: 14px; padding-right: 14px; padding-top: 10px; padding-bottom: 10px; background: white; box-shadow: 0px 1px 2px rgba(16, 24, 40, 0.05); border-radius: 8px; overflow: hidden; border: 1px #D0D5DD solid; justify-content: flex-start; align-items: center; gap: 8px; display: inline-flex">
                <input type="password" placeholder="Password" name="current_password" id="current_password" style="width: 100%; padding: 8px; border: none; outline: none; border-radius: 4px;">
                <input type="checkbox" onclick="togglePassword('current_password')">
            </div>
        </div>
    </div>

   
    <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
        <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
            <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">New Passworrd</div>
            <div style="align-self: stretch; padding-left: 14px; padding-right: 14px; padding-top: 10px; padding-bottom: 10px; background: white; box-shadow: 0px 1px 2px rgba(16, 24, 40, 0.05); border-radius: 8px; overflow: hidden; border: 1px #D0D5DD solid; justify-content: flex-start; align-items: center; gap: 8px; display: inline-flex">
                <input type="password" placeholder="New Password" name="new_password" id="new_password" oninput="validatePassword()" style="width: 100%; padding: 8px; border: none; outline: none; border-radius: 4px;">
                <input type="checkbox" onclick="togglePassword('new_password')">
            </div>
        </div>
    </div>

    <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
        <div style="align-self: stretch; height: 84px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: flex">
            <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 600; line-height: 24px; word-wrap: break-word">Confirm Password</div>
            <div style="align-self: stretch; padding-left: 14px; padding-right: 14px; padding-top: 10px; padding-bottom: 10px; background: white; box-shadow: 0px 1px 2px rgba(16, 24, 40, 0.05); border-radius: 8px; overflow: hidden; border: 1px #D0D5DD solid; justify-content: flex-start; align-items: center; gap: 8px; display: inline-flex">
                <input type="password" placeholder="confirm Password" name="confirm_password" id="confirm_password" oninput="validatePassword()" style="width: 100%; padding: 8px; border: none; outline: none; border-radius: 4px;">
                <input type="checkbox" onclick="togglePassword('confirm_password')">
            </div>
        </div>
    </div>    
<span id="password-error" class="text-danger"></span>
        @if($errors->any('new_password'))
        <span class="text-danger">{{$errors->first('new_password')}}</span>
        @endif
        </br>

    <div class="button-container">
    <a href="{{ route('participant.dashboard') }}" class="btn btn-outline-danger">Back</a>
        <!-- <button type="cancel" class="btn btn-outline-danger"> Cancel </button> -->
        <button type="submit" class="btn btn-primary" id="btnSubmit"> Submit </button>
    </div>

    </form>
</div>
</section>
</main>
@endsection

@section('vendor-scripts')

@endsection

@section('page-scripts')
<script>

function validatePassword() {
        var passwordInput = document.getElementById('new_password');
        var otherPasswordInput = document.getElementById('confirm_password');
        var passwordError = document.getElementById('password-error');
        
        // Check password requirements
        var regex = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]+$/;
        if (passwordInput.value.length < 8 || !regex.test(passwordInput.value)) {
            passwordError.textContent = 'Password must be at least 8 characters and contains symbol, number, and uppercase letter.';
            passwordInput.setCustomValidity('Invalid password');
        } else if (passwordInput.value !== otherPasswordInput.value) {
            passwordError.textContent = 'Passwords do not match.';
            passwordInput.setCustomValidity('Passwords do not match');
        }
         else {
            passwordError.textContent = '';
            passwordInput.setCustomValidity('');
        }
    }

    function togglePassword(inputId) {
        var passwordInput = document.getElementById(inputId);
        if (passwordInput.type === "password") {
            passwordInput.type = "text";
            
        } else {
            passwordInput.type = "password";
        }
    }
  
</script>

<script src="{{ asset('js/userProfile/changePassword.js') }}"></script>
    @endsection