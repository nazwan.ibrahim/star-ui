@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','User Profile')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
        #sectionFormMain>div>div:nth-child(5)>div.col-4 {
            margin-top: 19px;
        }
    }


    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

    .btn-add-2:hover {
        color: #fff;
    }

    .btn-add-2:disabled,
    .btn-add-2[disabled],
    .btn-add-2:hover {
        color: #fff !important;
        opacity: 0.5;
    }

    .btn-secondary-6 {
        background-color: #fff;
        color: #5A8DEE;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        padding: 10px 20px;
        font-size: 16px;
        font-family: poppins-semibold, sans-serif;
        line-height: 20px;
        text-align: left;
    }

    .btn-secondary-6:hover {
        background-color: #5A8DEE;
        color: #fff;
    }

    ul {
        list-style-type: none;
        padding: 0;
    }

    li {
        text-align: left;
    }
    .font-title {
        color: var(--gray-950, #0C111D);
        width: 300px;
        font-family: Inter;
        font-size: 24px;
        font-style: normal;
        font-weight: 600;
        line-height: 32px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 28px;
        font-style: normal;
        font-weight: 600;
    }

    .contentUpdate {
        color: #454545;
        font-family: Open Sans;
        font-size: 22px;
        font-style: normal;
        font-weight: 600;
        line-height: 28px;
        /* 155.556% */
        text-align: left;
    }

    .contentUpdate2 {
        color: #454545;
        font-family: Open Sans;
        font-size: 18px;
        font-style: normal;
        font-weight: 600;
        line-height: 28px;
        /* 155.556% */
        text-align: left;
    }

    /* .form-group {
    display: flex;
    align-items: center; /* Vertical alignment */
    /* } */
    */ label {
        width: 100px;
        /* Adjust as needed for your design */
        text-align: left;
        /* Adjust for alignment (left, right, center) */
        /* margin-right: 10px; Spacing between label and input */
    }

    .form-label {
        align-items: left;
        display: flex;
        align-items: center;
        gap: 8px;
        flex: 1 0 0;
        font-family: Open Sans;
    }
</style>
@endsection

@section('content')
<!-- Navbar -->
<!-- <nav class="navbar navbar-expand-lg"> -->


<!-- End Navbar -->

<main style="padding:50px;margin:50px;">
    <section class="header-top-margin">
        <div class="card mt-7" style="padding:50px;margin:50px;background-color:#fbf9f9">

            <div class="form-group">
                <label class="poppins-semibold-14 mb-1 page-title" for="user Information">User Information</label>
            </div>
            @if ($message = Session::get('success'))
                <div style="align-content: center;text-align: center;" class="alert alert-success" id="successAlert">
                    <p>{{ $message }}</p>
                </div>
            @else
                @if ($message = Session::get('error'))
                    <div style="align-content: center;text-align: center;" class="alert alert-danger" id="errorAlert">
                        <p>{{ $message }}</p>
                    </div>
                @endif
            @endif  
            <div class="row">
                <div>

                    <div class="row form-list">

                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div>
                                    <label for="name" class="form-label">Role</label>
                                    <input type="text" class="form-control" id="FullName" name="FullName" value="{{ $userInfo->userType }}" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div>
                                    <label for="" class="form-label">Salutation</label>
                                    <input type="text" class="form-control" id="EmailAddress" name="EmailAddress" value="{{ $userInfo->salutation }}" disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div>
                                    <label for="name" class="form-label">Full Name</label>
                                    <input type="text" class="form-control" id="FullName" name="FullName" value="{{ $userInfo->fullname }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Identification Type</label>
                                    <input type="text" class="form-control" id="IcNo" name="IcNo" value="{{ $userInfo->identity_type }}" disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">MyKad No/Passport No</label>
                                    <input type="text" class="form-control" id="IcNo" name="IcNo" value="{{ $userInfo->ic_no }}" disabled>
                                </div>
                            </div>
                        </div>
                          

                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Gender</label>
                                    <input type="text" class="form-control" id="Gender" name="Gender" value="{{ $userInfo->gender }}" disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Nationality</label>
                                    <input type="text" class="form-control" id="Nationality" name="Nationality" value="{{ $userInfo->nationality }}" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div>
                                    <label for="" class="form-label">Email Address</label>
                                    <input type="text" class="form-control" id="EmailAddress" name="EmailAddress" value="{{ $userInfo->email }}" disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div>
                                    <label for="name" class="form-label">Phone Number</label>
                                    <input type="text" class="form-control number" pattern="[0-9]*" id="PhoneNumber" name="PhoneNumber" value="{{ $userInfo->phone_no }}" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Address line 1</label>
                                    <input type="text" class="form-control" id="AddressLine1" name="AddressLine1" value="{{ $userInfo->address_1 }}" disabled>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Address line 2</label>
                                    <input type="text" class="form-control" id="AddressLine2" name="AddressLine2" value="{{ $userInfo->address_2 }}" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Postcode</label>
                                    <input type="text" class="form-control number" pattern="[0-9]*" id="Postcode" name="Postcode" value="{{ $userInfo->postcode }}" disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">City</label>
                                    <input type="text" class="form-control" id="State" name="State" value="{{ $userInfo->statename }}" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">State</label>
                                    <input type="text" class="form-control" id="State" name="State" value="{{ $userInfo->statename }}" disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Country</label>
                                    <input type="text" class="form-control" id="Country" name="Country" value="{{ $userInfo->countryname }}" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <label class="poppins-semibold-14 mb-1 form-label" for="pilihSemua">Passport Photo</label>
                                <div class="col-6">
                                    <div class="border rounded-lg p-3">
                                        <div class="rounded-lg pb-2" style="border: 2px dashed #DFE3E7;">
                                            <div class="mt-2" style="display: flex; flex-direction: column; align-items: center; margin-left: 15px;">
                                                @if($userInfo && $userInfo->passportpic_path)
                                                <img id="imagePreview" src="{{ asset('storage/' . $userInfo->passportpic_path) }}" alt="Passport Photo" style="max-width: 100%; max-height: 150px;">
                                                @else
                                                <img id="imagePreview" src="#" alt="Passport Photo" style="max-width: 100%; max-height: 150px; display: none;">
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="poppins-semibold-14 mb-1 form-label" for="pilihSemua">Copy of IC</label>
                                <div class="col-6">
                                    <div class="border rounded-lg p-3">
                                        <div class="rounded-lg pb-2" style="border: 2px dashed #DFE3E7;">
                                            <div class="mt-2" style="display: flex; flex-direction: column; align-items: center; margin-left: 15px;">
                                                @if($userInfo && $userInfo->ICpic_path)
                                                <a href="{{ asset('storage/' . $userInfo->ICpic_path) }}" target="_blank" style="margin-right: 6px;"><i class="fas fa-file-pdf"></i>
                                                    <i>{{ $userInfo->ICpic_name }}</i>
                                                </a>
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="poppins-semibold-14 mb-1 contentUpdate" for="jenisCenderamata-0">Educational Information</label>
                        </div>
                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Higher Education Level</label>
                                    <input type="text" class="form-control" id="EducationLevel" name="EducationLevel" value="{{ $userInfo->education_level }}" disabled>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Qualification</label>
                                    <input type="text" class="form-control" id="Qualification" name="Qualification" value="{{ $userInfo->qualification }}" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin:10px;">
                            <div class="col-4">
                                <label class="poppins-semibold-14 mb-1 form-label" for="pilihSemua">Supporting Documents</label>
                                <div class="">
                                    <div class="form-group" style="margin-top:10px;">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>File Name</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="fileList">
                                                @if ($viewUploadedFileEdu)
                                                @foreach ($viewUploadedFileEdu as $index => $file)
                                                <tr>
                                                    <td>{{ $index + 1 }}</td>
                                                    <td>{{ $file->file_name }}</td>
                                                    <td>
                                                        <a href="{{ asset('storage/eduFiles/' . $file->file_name) }}" target="_blank" title="View">
                                                            <i class="fas fa-file-pdf"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="3">No data available</td>
                                                </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="poppins-semibold-14 mb-1 contentUpdate" for="jenisCenderamata-0">Company Information</label>
                        </div>
                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Company Name</label>
                                    <input type="text" class="form-control" id="CompanyName" name="CompanyName" value="{{ $userInfo->companyname }}" disabled>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Position</label>
                                    <input type="text" class="form-control" id="Position" name="Position" value="{{ $userInfo->position }}" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Fax Number</label>
                                    <input type="text" class="form-control" id="FaxNumber" name="FaxNumber" value="{{ $userInfo->fax_no }}" disabled>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Phone Number</label>
                                    <input type="text" class="form-control" id="CompanyPhoneNumber" name="CompanyPhoneNumber" value="{{ $userInfo->companyphone }}" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Address Line 1</label>
                                    <input type="text" class="form-control" id="CompanyAddressLine1" name="CompanyAddressLine1" value="{{ $userInfo->company_address_1 }}" disabled>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Address Line 2</label>
                                    <input type="text" class="form-control" id="CompanyAddressLine2" name="CompanyAddressLine2" value="{{ $userInfo->company_address_2 }}" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Postcode</label>
                                    <input type="text" class="form-control" id="Postcode" name="Postcode" value="{{ $userInfo->postcode }}" disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">City</label>
                                    <input type="text" class="form-control" id="State" name="State" value="{{ $userInfo->cityname }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin:10px;">
                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">State</label>
                                    <input type="text" class="form-control" id="State" name="State" value="{{ $userInfo->statename }}" disabled>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="">
                                    <label for="name" class="form-label">Country</label>
                                    <input type="text" class="form-control" id="Country" name="Country" value="{{ $userInfo->countryname }}" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex justify-content-end pt-3 pb-2">
                            <div style="margin:10px;">
                                <a href="{{ route('user-profile') }}" class="btn btn-secondary-6 btn-block">Back</a>
                            </div>

                            <div style="margin:10px;">
                                <a href="{{ route('update-user-profile') }}" class="btn btn-secondary-6 btn-block">Edit Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
    </section>
</main>
@endsection

@section('vendor-scripts')

@endsection

@section('page-scripts')
<script>
    function previewImage(input) {
        var fileInput = input;
        var preview = document.getElementById('imagePreview');
        var label = document.querySelector('.custom-file-label');

        // Clear existing preview
        preview.style.display = 'none';
        preview.src = '';

        // Update label with the selected file name
        label.textContent = fileInput.files[0] ? fileInput.files[0].name : 'Choose file';

        // Show preview if an image file is selected
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                preview.src = e.target.result;
                preview.style.display = 'block';
            };

            reader.readAsDataURL(fileInput.files[0]);
        }
    }


    setTimeout(function() {
        var successAlert = document.getElementById('successAlert');
        var errorAlert = document.getElementById('errorAlert');

        if (successAlert !== null) {
            successAlert.style.display = 'none';
        }

        if (errorAlert !== null) {
            errorAlert.style.display = 'none';
        }
    }, 5000);
</script>
</script>
@endsection