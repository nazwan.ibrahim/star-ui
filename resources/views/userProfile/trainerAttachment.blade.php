@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','TP-Schedule&History')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

.icon-editIcon:before {
  content: "\e916";
}



    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

  
    .tablecontent{
    color: #333;
    font-family: Open Sans;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: 20px; /* 125% */
    text-align: left;
}


</style>
@endsection

@section('content')
<main style="padding-top:20px;padding-left:50px;padding-right:50px; margin:5px;text-align: left">
    <section class="header-top-margin">
        
  
    <div class="row">
        <label class="page-title" for="" id="userType"></label>
    </div>
    <div class="row">
        <div class="col-lg-12 margin-tb page-title">
            <div class="pull-left">
                <h2>Trainer Attachment</h2>
            </div>
            
         </div>
    </div>
   
    
        <div class="table-responsive">
            
            <table class="table  dataTable no-footer" id="Trainer" style="width: 100%;">

                <thead class="table-header">
                    <tr >
                        <th class="header-1">No.</th>
                        <th class="header-1">Trainer Name</th>
                        <th class="header-1">QP No.</th>
                        <th class="header-1">State</th>
                        <th class="header-1">TP Attachment</th>
                        <th class="header-1">View Certificate</th>
                        <th class="header-1">Status</th>
                    </tr>
                </thead>

                <tbody class="tablecontent">



                </tbody>

            </table>
        </div>

        
       
    
   
       
        </section>
</main>
  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script> 
    
@endsection



@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
    let userType = "{{ $user_type }}";
    let companyID =  "{{ $company_id }}";
    //alert(userType);
</script>


<script src="{{ asset('js/userProfile/trainerAttachment.js') }}"></script>

@endsection
