@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','User Management')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .dataTables_length {
        display: none;
    }

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .btn-add-user:hover {
        background-color: #3e6be1;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }   

    .btn-actions {
        display: flex;
        gap: 10px;
    }

    .btn-edit,
    .btn-delete {
        background-color: #5A8DEE;
        color: #fff;
        border: none;
        border-radius: 5px;
        padding: 5px 10px;
        font-size: 14px;
        font-weight: 600;
        cursor: pointer;
        transition: background-color 0.3s ease-in-out;
    }

    .btn-edit:hover,
    .btn-delete:hover {
        background-color: #3e6be1;
    }

     /* Add styles for the table */
    .table {
    width: 100%;
    border-collapse: collapse;
    margin: 0;
    }

   

    .table th, .table td {
    padding: 25px;
    text-align: center;
    border: 1px solid #ddd;
    }

    /* Add hover effect on table rows */
    .table tbody tr:hover {
    background-color: #f5f5f5;
}
.active-status {
        border-radius: 8px;
        border: 1px solid #18eeaa;
        background: var(--success-25, #f6faf7);
        padding: 4px 8px;
        color: #18d647;
    }

    .inactive-status {
        border-radius: 8px;
        border: 1px solid #F04438;
        background: var(--success-25, #FFFBFA);
        padding: 4px 8px;
        color: #F04438;
    }

    .table-content.text-left {
    text-align: left !important;
}
</style>
@endsection

@section('content')
<main class="header-top-margin col-md-10 mx-auto">
    <div class="text-center mb-3">
        <h2 class="page-title py-2" style="text-align: left !important;">User Account</h2>
    </div>
    @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
    @else
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
    @endif
    <div class="table-responsive">
        <div id="userManagementList_wrapper" class="dataTables_wrapper no-footer">
            <table class="table table-striped dataTable no-footer" id="userManagementList" style="width: 100%;">
                <thead class="table-header">
                    <tr>
                        <th class="table-title text-center" width="40">No.</th>
                        <th class="table-title text-center">Name</th>
                        <th class="table-title text-center">Mykad No / Passport No.</th>
                        <th class="table-title text-center">Email</th>
                        <th class="table-title text-center">Role</th>
                        <th class="table-title text-center">Status</th>
                        <th class="table-title text-center">Action</th>
                    </tr>
                    <tr>
                    <th class="table-title text-center" width="40">Filter</th>
                    <th class="table-title text-center"> 
                        <input type="text" placeholder="Search Name" class="form-control" />
                    </th>
                    <th class="table-title text-center"> 
                        <input type="text" placeholder="Search Mykad No" class="form-control" />
                    </th>
                    <th class="table-title text-center">
                        <input type="text" placeholder="Search Email" class="form-control" />
                    </th>
                    <th class="table-title text-center">
                        <input type="text" placeholder="Search Role" class="form-control" />
                    </th>
                    <th class="table-title text-center">
                    <input type="text" placeholder="Search Status" class="form-control" />
                    </th>
                    <th class="table-title text-center">
                        <!-- Add a select dropdown for action filtering if needed -->
                    </th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</main>
@endsection


@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script> 
    
@endsection



@section('page-scripts')

{{-- our own js --}}

<script>
    
$(function(){

userList();


});


let userList = () => {

     let getList = `/api/user/userManagementList`;
    $.ajax({
      url: getList,
      type: "GET",
      dataType: "JSON",
    })
      .done((res) => {
        //createTable(res.data);
        tableUserList(res.userlist);
      })
      .catch((err) => {});
  };

  let tableUserList = (data) => {
    let senarai = $("#userManagementList").DataTable({
        data: data,
        dom: 'lfrtip', 
        ordering: false, 
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                $('input', column.header()).on('keyup change clear', function () {
                    if (column.search() !== this.value) {
                        // Use case-insensitive search for the "Status" column (index 5)
                        column.search(this.value, false, true).draw();
                    }
                });
            });
        },
      //  order: [[4, "desc"]],
      columnDefs: [
        {
          targets: 0,
          className: 'text-left', 
          data: null,
          searchable: false,
          orderable: false,
        },
        {
          targets: 1,
          className: 'text-left', 
          data: "id",
          render: function (data, type, full, meta) {
            if (data === null) {
              return `<td width="50%"  align="center"><p class="table-content">-</p></td>`;
            }
            if (data !== null && data !== "") {
              return `<td align="center" width="50%">
                        <a href="/admin/view-registered/${data}">
                          <p class="table-content" style="font-size: normal; color: #3498DB;">${full.fullname ?? full.companyname}</p>
                        </a>
                      </td>`;
            }
          },
        },
        {
            targets: 2,
            className: 'text-left', 
            data: "ic_no",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="50%" align="center"><p class="table-content">-</p></td>`;
              }
              if (data !== null || data !== "") {
                return `<td align="left" width="50%"><p class="table-content">${data}</p></td>`;
              }
            },
          },
        {
            targets: 3,
            className: 'text-left', 
            data: "email",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="50%" align="center"><p class="table-content">-</p></td>`;
              }
              if (data !== null || data !== "") {
                return `<td align="left" width="50%"><p class="table-content">${data}</p></td>`;
              }
            },
          },
          {
            targets: 4,
            className: 'text-left', 
            data: "user_type",
            render: function (data, type, full, meta) {
              if (data === 1) {
                return `<div class="dark-turquoise-status table-content">Super Admin</div>`;
              } else if (data === 2) {
                return `<div class="dark-turquoise-status table-content">SEDA Admin</div>`;
              }else if (data === 3) {
                return `<div class="dark-turquoise-status table-content">Training Partner</div>`;
              }else if (data === 4) {
                return `<div class="dark-turquoise-status table-content">Trainer</div>`;
              }else if (data === 5) {
                return `<div class="dark-turquoise-status table-content">Participant</div>`;
              }                  
              else {
                return "-";
              }
            },
          },
          {
            targets: 5,
            data: "status",
            render: function (data, type, full, meta) {
              if (data === 1) {
                return  `<td width="15%"><p class="active-status" style="text-align: center;">ACTIVE</p></td>`;
              } else if (data === 0 || data ===null) {
                return  `<td width="15%"><p class="inactive-status" style="text-align: center;">INACTIVE</p></td>`;
              } else {
                return "-";
              }
            },
          },
        {
          targets: 6,
          data: "id",
          render: function (data, type, full, meta) {
            // Assuming you have URLs for view, edit, and delete actions
            let resetPassword = "/admin/reset-password/" + data;
            let activate = "/admin/edit-registered/" + data;
            let deactivate = "/admin/user-management/" + data;
          
            if (data !== null && data !== "") {
              return `<div>
                        <a href="${resetPassword}" class="btn btn-sm">
                          <i class="fas fa-key" style="font-size: 18px; color: #3498DB;"></i>
                        </a>
                        <a href="${activate}" class="btn btn-sm">
                          <i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i>
                        </a>
                        <a href="${deactivate}" class="btn btn-sm">
                          <i class="fas fa-trash-alt" style="font-size: 18px; color: #3498DB;"></i>
                        </a>
                      </div>`;
            } else {
              return "-";
            }
          },
        }
      ],
      language: {
        info: " _START_ to _END_ from _TOTAL_ entry",
        infoEmpty: " 0 from 0 to 0 entry",
        lengthMenu: " _MENU_ Entry",
        paginate: {
          next: "Next",
          previous: "Previous",
        },
        zeroRecords: "No record found",
      },
    });

    var addUserButton = $('<button id="addUserButton" class="circular-button" style="color: white">' +
                        '<i class="fas fa-plus"></i>' +
                        '</button>');

    addUserButton.on('click', function () {
      window.location.href = "/admin/register-training-partner";
    });

    addUserButton.css({
      width: '37px',
      height: '37px',
      backgroundColor: '#3498DB',
      border: '2px solid #3498DB',
      borderRadius: '50%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      cursor: 'pointer',
    });
  
    addUserButton.css('margin-left', '10px');
    $('#userManagementList_wrapper .dataTables_filter').append(addUserButton);

    senarai
      .on("order.dt search.dt", function () {
        senarai
          .column(0, { search: "applied", order: "applied" })
          .nodes()
          .each(function (cell, i) {
            cell.innerHTML = i + 1;
          });
      })
      .draw();
  
    $("#filterStatus").on("change", (e) => {
      // console.log(e.target.value)
      senarai.column(5).search(e.target.value).draw();
    });
  
    $("#filterJenisKenderaan").on("change", (e) => {
      console.log(e.target.value);
      senarai.column(1).search(e.target.value).draw();
    });
  
    $("#search_input").on("keyup", () => {
      let searched = $("#search_input").val();
      // console.log(searched)
      senarai.search(searched).draw();
    });
  
    $("#resetFilter").on("click", () => {
      $("#filterStatus").val("").trigger("change");
      $("#filterJenisKenderaan").val("").trigger("change");
      $("#search_input").val("");
  
      senarai.search("").columns().search("").draw();
    });
  };
  

  //////

</script>
@endsection