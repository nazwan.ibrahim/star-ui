<!DOCTYPE html>
<html lang="ms">
  <!-- BEGIN: Head-->

    <head>
        <meta  charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') - SEDA Training & Registration (STAR)</title>
     
        <link href="{{ asset('css/vendor/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/pages/masterlayout.css') }}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
       

        @yield('vendor-styles')

    </head>
    @include('panels.headermenu')
    @include('layouts.pic.profile-navbar')
    @yield('page-styles')
    <body class="vertical-layout 1-column text-center navbar-sticky footer-static blank-page data-open=click " data-menu="vertical-menu-modern" data-col="1-column" data-framework="laravel">
   
            @yield('content')
       
        
        <!-- END: Content-->
  
        {{-- scripts --}}
        <!-- BEGIN: Page Vendor JS-->
            @yield('vendor-scripts')
            {{-- <script src="{{ asset('js/vendors/bootstrap.min.js') }}"></script>  --}}
            <script src="{{ asset('js/vendors/jquery-3.7.1.min.js') }}"></script> 
            <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script> 

            {{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> --}}
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> 
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
            <link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
            <link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <!-- END: Page Vendor JS-->
        <!-- BEGIN: Page JS-->
             @yield('page-scripts')
        <!-- END: Page JS-->
  
  <!-- BEGIN: Footer-->
  @include('panels.footer')
  <!-- END: Footer-->
    </body>
  </html>
    


