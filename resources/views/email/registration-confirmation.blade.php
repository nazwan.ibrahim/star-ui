<!DOCTYPE html>
<html>
<head>
    <title>STAR REGISTRATION CONFIRMATION</title>
</head>
<body>
    <p>Dear {{ $user->fullname }},</p>
    <p>Your registration was successful.</p> 

    <p>You are registered as a <strong>
    @if($user->user_type == 2)
        SEDA Admin
    @elseif($user->user_type == 3)
        Training Partner
    @elseif($user->user_type == 4) 
        Trainer
    @elseif($user->user_type == 5)
        Participant
    @else
        Unknown Type
    @endif
    </strong></p>
    <p>Here are your registration details:</p>
    <p><strong>Username:</strong> {{ $user->email }}</p>
    <p><strong>Password:</strong> {{ $plainTextPassword }}</p>

    <p>Thank you</p>
</body>
</html>
