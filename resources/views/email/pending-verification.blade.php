<!DOCTYPE html>
<html>
<head>
    <title>SUCCESS APPLYING TRAINING</title>
</head>
<body>
    <p>Hi {{ $user->fullname }},</p>
    <p></p>
    <p>Your application for training success. Here are your training application details:</p>
    <p><h3><strong>{{ $trainings->training_name }}</strong></h3></p>
    <p><strong>Provider:</strong> {{ $trainings->tpName }}</p>
    <p><strong>Start Date:</strong> {{ $trainings->date_end }}</p>
    <p><strong>End Date:</strong> {{ $trainings->date_end }}</p>
    <p><strong>Venue:</strong> {{ $trainings->venue }}</p>

    <p>Thank you for applying training with us.</p>
</body>
</html>
    