<!DOCTYPE html>
<html>
<head>
    <title>TRAINING APPLICATION PENDING APPROVAL</title>
</head>
<body>
    <p>Hi {{ $administrator->fullname }},</p>
    <p></p>
    <p>New Application for training. Here are details of training application </p>
    <p><h3><strong>{{ $trainings->training_name }}</strong></h3></p>
    <p><strong>Provider:</strong> {{ $trainings->companyname }}</p>
    <p><strong>Start Date:</strong> {{ $trainings->date_end }}</p>
    <p><strong>End Date:</strong> {{ $trainings->date_end }}</p>
    <p><strong>Venue:</strong> {{ $trainings->venue }}</p>

    <p>Thank you.</p>
</body>
</html>
    