@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Dashboard')
{{-- vendor css --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .doughnut-chart{
        height: 25%;
        width: 25%;
    }

    .chart-title{
        color: #454545;
        font-size: 24px;
        font-family: Open Sans;
        font-weight: 600;
        line-height: 32px;
    }

    .carousel-container {
        display: flex;
        flex-direction: row !important;
        align-items: center;
        justify-content: center;
        margin-top: 3%;
    }

    .carousel-button{
        width: 65px;
        height: 65px;
        background: #3498DB;
        border-radius: 8px;
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .training-cards{
        width: 364px;
        height: 460px;
        border-radius: 8px;
    }

    .card-title{
        color: #152536;
        font-size: 20px;
        font-family: Open Sans;
        font-weight: 600;
    }

    /* .card-location{
        color: #6C757D;
        font-size: 14px;
        font-family: Open Sans;
        font-weight: 400;
    } */

    .unconfirmed-details{
        border-radius: 8px;
        border: 1px solid var(--success-500, #F79009);
        background: var(--success-25, #FFFCF5);
        padding: 4px 25px;
        color: #F79009;
    }

    .contact-info-sets {
        display: flex;
        flex-direction: column;
        text-align: start;
        margin-bottom: 10%;
        margin-right: 50%;
        white-space: nowrap;
    }

    .contact-info-name{
        color: #454545;
        font-size: 20px;
        font-family: Open Sans;
        font-weight: 600;
        line-height: 30px;
    }

    .button-text{
        color: #F9F6FE;
        font-size: 16px;
        font-family: Rajdhani;
        font-weight: 700;
        text-transform: uppercase;
        line-height: 24px;
    }

    .logo-card{
        color: #FFFFFF;
    }

    .tr{
        background-color: white !important;
    }

    .training-cards {
    border: 1px solid;
    margin-left: 3%;
    margin-right: 3%;
    max-height: 1100px;
    overflow: hidden;
}

.content-container {
    display: flex;
    flex-direction: column;
}

.image-container {
    margin-bottom: 250px; /* Adjust the margin as needed */
    position: relative;
}

#imageErrorMessage {
    display: none;
    position: absolute;
    top: 700%;
    left: 50%;
    transform: translateX(-50%);
    color: red;
}

.image-container img {
    width: 100%;
    height: auto;
    object-fit: cover;
}

.card-title {
    /* margin-top: 10px; */
    font-weight: bold;
}

.card-location {
    margin-left: 5px;
}

.unconfirmed-details {
    display: block;
    margin-bottom: 5px;
}

</style>
@endsection

@section('content')
<main>

    <div class="col-md-12">
        <div class="text-center mb-3" style="margin-left: 3%; margin-top: 5%;">
            <h2 class="page-title py-2" style="text-align: left !important;">Training Calendar</h2>
        </div>
    </div>

    <div class="row col-md-12" style="margin-left: 20px">
       
      
     
        
    </div>


    <!-- <div class="form-row col-md-12" style="margin-left: 20px;margin-bottom:-30px;">
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="course_filter"><b>Programe</b></label>
            <select class="form-control" id="course_filter"  name="course_filter">
               <option value="">All </option>
            </select>
        </div>
        
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="venue_filter"><b>Venue</b></label>
            <select class="form-control" id="venue_filter" name="venue_filter">>
                <option value="">All</option>
            </select>
        </div>

        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="state_filter"><b>State</b></label>
            <select class="form-control" id="state_filter">
                <option value="">All</option>
               
            </select>
        </div>
        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="startDate_filter"><b>Start date</b></label>
            <select class="form-control" id="startDate_filter">
                <option value="">All</option>
              
            </select>
        </div>

        <div class="form-group col-md-2 py-2">
            <label class="field-title" for="endDate_filter"><b>End date</b></label>
            <select class="form-control" id="endDate_filter">
                <option value="">All</option>
            </select>
        </div>
    </div> -->

    <div class="col-md-12 py-4 mx-auto" style="margin-bottom: 3%;">
        <div id="AllList_wrapper" class="dataTables_wrapper no-footer">
            <table class="table table-bordered table-striped dataTable no-footer" id="AllList" style="width: 100%;">
                <thead class="table-header">
                    <tr>
                    <th class="table-title text-center" style="border-bottom: none;">No.</th>
                        <th class="table-title text-center" style="border-bottom: none;">Programme</th>
                        <th class="table-title text-center" style="border-bottom: none;">Course Category</th>
                        <th class="table-title text-center" style="border-bottom: none;">Training Type</th>
                        <th class="table-title text-center" style="border-bottom: none;">Batch</th>
                        <th class="table-title text-center" style="border-bottom: none;">Start Date</th>
                        <th class="table-title text-center" style="border-bottom: none;">End Date</th>
                        <th class="table-title text-center" style="border-bottom: none;">Status</th>
                        <!-- <th class="table-title text-center" style="border-bottom: none;">Action</th> -->
                    </tr>
                   
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-12">
        <div class="text-center mb-3" style="margin-left: 3%;">
            <h2 class="page-title py-2" style="text-align: left !important;">Upcoming Training</h2>
        </div>
    </div>

    <!-- Add the new carousel section -->
    <div class="col-md-12 carousel-container">
        <div class="col-md-1">
            <div class="carousel-button" id="carousel-button-left">
                <i class="fas fa-chevron-left" style="color: #FFFFFF;"></i>
            </div>
        </div>
        
        <!-- Dynamically generated training cards will be added here -->
        <div id="training-cards-container" class="carousel-container">
            <!-- Training cards will be dynamically added here -->
        </div>

        <div class="col-md-1">
            <div class="carousel-button" id="carousel-button-right">
                <i class="fas fa-chevron-right" style="color: #FFFFFF;"></i>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="text-center mb-3" style="margin-left: 3%; margin-top: 4%;">
            <h2 class="page-title py-2" style="text-align: left !important;">Cumulative Training Chart</h2>
        </div>
    </div>

    <!-- Dynamically generated doughnut charts -->
    <div class="col-md-10 py-4" style="display: flex; flex-direction: row; margin-bottom: 5%; margin-left: 5%;">
        <div class="doughnut-chart col-md-3 mx-auto">
            <label class="chart-title">Renewable Energy</label>
            <canvas id="doughnut-chart-1"></canvas>
            <div id="no-data-message" style="display: none; font-size: 20px;">
                <b>No data display.</b>
            </div>
        </div>
        <div class="doughnut-chart col-md-3 mx-auto">
            <label class="chart-title">Energy Efficiency</label>
            <canvas id="doughnut-chart-2"></canvas>
            <div id="no-data-message2" style="display: none; font-size: 20px;">
                <b>No data display.</b>
            </div>
        </div>
        <div class="doughnut-chart col-md-3 mx-auto">
            <label class="chart-title">Sustainable Energy</label>
            <canvas id="doughnut-chart-3"></canvas>
            <div id="no-data-message3" style="display: none; font-size: 20px;">
                <b>No data display.</b>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="text-center mb-3" style="margin-left: 3%;">
            <h2 class="page-title py-2" style="text-align: left !important;">Competency Training Stats</h2>
        </div>
    </div>
    <!-- <select id="minYear" onchange="updateChart()"> -->
        <!-- Options will be added dynamically using JavaScript -->
    <!-- </select> -->

    <div class="col-md-11 py-4" style="display: flex; flex-direction: row; margin-bottom: 5%; margin-left: 5%;">
        <div class="col-md-10 mx-auto">
            <canvas id="competency-chart"></canvas>
            <div id="no-data-message4" style="display: none; font-size: 20px;">
                <b>No data display.</b>
            </div>
        </div>
    </div>

    <div class="col-md-11 py-4" style="display: flex; flex-direction: row; margin-bottom: 5%; margin-left: 5%;">
        <div class="col-md-10 mx-auto">
            <table id="dataTable" class="display"></table>
        </div>
    </div>

   
    
</main>
@endsection

@section('vendor-scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment@1.1.2"></script>
    <script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
   
@endsection

@section('page-scripts')
<script src="{{ asset('js/dashboard/adminChart.js') }}"></script>
<script>
    let host = "{{ env('API_SERVER') }}";
</script>

<!-- Add the new carousel script -->
<script>
     
    </script>
@endsection
