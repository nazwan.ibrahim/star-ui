@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Trainer - Dashboard')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}
.doughnut-chart{
        height: 25%;
        width: 25%;
    }


.icon-editIcon:before {
  content: "\e916";
}

/* Add styles for the table */
.table {
        width: 100%;
        border-collapse: collapse;
        margin: 0;
        }

        .table th, .table td {
        padding: 10px;
        text-align: center;
        border: 1px solid #ddd;
        }

        /* Add hover effect on table rows */
        .table tbody tr:hover {
        background-color: #f5f5f5;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
        text-align: center;
    }

    .table-header th {
        padding: 10px;
        text-align: center;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .paid-status {
        border-radius: 8px;
        border: 1px solid #18eeaa;
        background: var(--success-25, #f6faf7);
        padding: 4px 8px;
        color: #18eeaa;
    }

    .failed-status {
        border-radius: 8px;
        border: 1px solid #F04438;
        background: var(--success-25, #FFFBFA);
        padding: 4px 8px;
        color: #F04438;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

</style>
@endsection

@section('content')
<main style="padding-top:20px;padding-left:50px;padding-right:50px; margin:5px;text-align: left">
    <section class="header-top-margin">
  
    <!-- <div class="row">
        <label class=" font-title" for="">Trainer</label>
    </div> -->
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Training Schedule</h2>
            </div>
            
         </div>
    </div>
   
    
    <div id="TrainingSchedule_wrapper" class="dataTables_wrapper no-footer">
        <table class="table table-width-style dataTable no-footer" id="TRSchedule" style="width: 100%">
            <thead class="table-header">
                <tr >
                    <th class="header-1">No.</th>
                    <th class="header-1">Partner Name</th>
                    <th class="header-1">Course Category</th>
                    <th class="header-1">Training Type</th>
                    <th class="header-1">Start Date</th>
                    <th class="header-1">End Date</th>
                    <th class="header-1">Status</th>
                </tr>
            </thead>

            <tbody>



            </tbody>

        </table>
   
    </div>
    <div>
        <p>
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Training History</h2>
                </div>
                
             </div>
        </div>
       
        
        <div id="TrainingHistory_wrapper" class="dataTables_wrapper no-footer">
            <table class="table table-width-style dataTable no-footer" id="TRHistory" style="width: 100%">
            <thead class="table-header">
                    <tr >
                        <th class="header-1">No.</th>
                        <th class="header-1">Partner Name</th>
                        <th class="header-1">Course Category</th>
                        <th class="header-1">Training Type</th>
                        <th class="header-1">Start Date</th>
                        <th class="header-1">End Date</th>
                        <th class="header-1">Status</th>
                        <th class="header-1">Action</th>
                    </tr>
                </thead>
    
                <tbody>
    
    
    
                </tbody>
    
            </table>
        </div>
        </p>
    </div>

</br>
<div class="col-md-12">
            <div class="text-center mb-3" style="margin-left: 3%;">
                <h2 class="page-title py-2" style="text-align: left !important;">Cumulative Training Chart</h2>
            </div>
        </div>

        <!-- Dynamically generated doughnut charts -->
        <div class="col-md-10 py-4" style="display: flex; flex-direction: row; margin-bottom: 5%; margin-left: 5%;">
            <div class="doughnut-chart col-md-3 mx-auto">
                <label class="chart-title">Renewable Energy</label>
                <canvas id="doughnut-chart-1"></canvas>
                <div id="no-data-message" style="display: none; font-size: 20px;">
                <b>No data display.</b>
            </div>
            </div>
            <div class="doughnut-chart col-md-3 mx-auto">
                <label class="chart-title">Energy Efficiency</label>
                <canvas id="doughnut-chart-2"></canvas>
                <div id="no-data-message2" style="display: none; font-size: 20px;">
                <b>No data display.</b>
            </div>
            </div>
            <div class="doughnut-chart col-md-3 mx-auto">
                <label class="chart-title">Sustainable Energy</label>
                <canvas id="doughnut-chart-3"></canvas>
                <div id="no-data-message3" style="display: none; font-size: 20px;">
                <b>No data display.</b>
            </div>
            </div>
        </div>
        <div class="col-md-12">
        <div class="text-center mb-3" style="margin-left: 3%;">
            <h2 class="page-title py-2" style="text-align: left !important;">Competency Training Stats</h2>
        </div>
    </div>

    <div class="col-md-11 py-4" style="display: flex; flex-direction: row; margin-bottom: 5%; margin-left: 5%;">
        <div class="col-md-10 mx-auto">
            <canvas id="competency-chart"></canvas>
            <div id="no-data-message4" style="display: none; font-size: 20px;">
                <b>No data display.</b>
            </div>
        </div>
    </div>
    <div class="col-md-11 py-4" style="display: flex; flex-direction: row; margin-bottom: 5%; margin-left: 5%;">
        <div class="col-md-10 mx-auto">
            <table id="dataTable" class="display"></table>
        </div>
    </div>

    
       
        </section>
        
</main>
  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment@1.1.2"></script>
    <script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="{{ asset('js/dashboard/trainerCharts.js') }}"></script>
@endsection



@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>


<script src="{{ asset('js/dashboard/TrainerTrainingSchedule.js') }}"></script>

@endsection
