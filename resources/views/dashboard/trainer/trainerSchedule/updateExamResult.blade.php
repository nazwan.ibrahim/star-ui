@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Exam Management')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

{{-- <link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet"> --}}
<link rel="stylesheet" 
    href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" 
    crossorigin="anonymous">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }
    
    .font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.font-title-2{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 18px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.icon-editIcon:before {
  content: "\e916";
}

.title{
    color: #656565;
    font-family: Rajdhani;
    font-size: 26px;
    font-style: normal;
    font-weight: 600;
    line-height: 38px; /* 146.154% */
}

.contentUpdate{
    color: #454545;
    font-family: Open Sans;
    font-size: 18px;
    font-style: normal;
    font-weight: 600;
    line-height: 28px; /* 155.556% */
}

.modal-dialog {
    max-width: 800px; /* Adjust the width as per your requirements */
}
</style>
@endsection

@section('content')
<main style="padding:50px;margin:50px;text-align: left">
  @php
  $lastParameter = request()->segment(count(request()->segments()));
@endphp
  <div class="container">
    <ol class="breadcrumb p-0 mb-0 poppins-semibold-12 light-grey">
              <li class="nav-item mg-sm ">
                  <a class="nav-link" href="{{ route('trainer-training-schedule-history') }}">Dashboard</a>
                      <span class="sr-only">(current)</span>
                  </a>
              </li>   
              <li class="nav-item mg-sm ">
                  <a class="nav-link" href="{{ route('trainer-AttendanceList',$lastParameter) }}">Attendance List</a>
                      <span class="sr-only">(current)</span>
                  </a>
              </li>          
              <li class="nav-item mg-sm ">
                  <a class="nav-link" href="{{ route('trainer-UpdateExamResult', $lastParameter) }}">Update Exam Result
                      <span class="sr-only">(current)</span>
                  </a>
              </li>
              <li class="nav-item mg-sm ">
                  <a class="nav-link" href="{{ route('trainer-TrainingReport',$lastParameter) }}">Training Report
                      <span class="sr-only">(current)</span>
                  </a>
              </li>
    </ol>
  </div>
    <section class="header-top-margin">
  
   
    <div class="row">
        <label class="poppins-semibold-14 mb-1 font-title-2" >Update Exam Result</label>
    </div>
    <div class="row" style="text-align: left">
        <div class="card mt-7" style="padding:50px;margin:50px;background-color:#fbf9f9">
            <span class="contentUpdate">Provider:</span><span class='title' id="provider_name">GCPV System Design</span>
            <span class="contentUpdate">Start Date :</span><span id="date_start">10/09/2023</span>
            <span class="contentUpdate">End Date :</span><span id="date_end">13/09/2023</span>
            <span class="contentUpdate">Venue :</span><span id="venue">KL</span>
        </div>   
    </div>

    <div class="row">
        
        <div class="table-responsive">
            <table border=1 class="table table-width-style dataTable no-footer" id="examResultUpdate" style="width: 100%">

                <thead>
                    {{-- <tr>
                        <th rowspan="3">No</th>
                        <th rowspan="3">Name</th>
                        <th rowspan="3">New / Res-sit</th>
                        <th colspan="5">Practical Exam</th>
                        <th colspan="5">Theory Exam</th>
                        <th rowspan="3">Final Verdict Pass/fail</th>
                        <th rowspan="3">Action</th>
                    </tr>
                    <tr>
                        <th rowspan="2">Part A</th>
                        <th rowspan="2">Part B</th>
                        <th rowspan="2">Part C</th>
                        <th colspan="2">Final</th>
                        <th rowspan="2">Part A</th>
                        <th rowspan="2">Part B</th>
                        <th rowspan="2">Part C</th>
                        <th colspan="2">Final</th>
                    </tr>
                    <tr>
                        <th rowspan="2">Total</th>
                        <th rowspan="2">Pass / Fail</th>
                        <th rowspan="2">Total</th>
                        <th rowspan="2">Pass / Fail</th>
                        
                    </tr>    --}}

                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>New / Res-sit</th>
                       
                       
                        <th>Part A</th>
                        <th>Part B</th>
                        <th>Part C</th>
                        <th>Total</th>
                        <th>Practical Final Result</th>
                        <th>Part A</th>
                        <th>Part B</th>
                        <th>Part C</th>
                        <th>Total</th>
                        <th>Theory Final Result</th>
                        <th>Final Verdict Result<br>(Pass / Fail)</th>
                        <th>Action</th>
                    </tr>   
                </thead>
                <tbody>
                   
                </tbody>
                {{-- <tbody>
                </tbody> --}}
            
            </table>
           
        </div>
    </div>

    <!-- modal -->
   
        <div class="modal fade text-left" id="edit-result-modal" tabindex="-1" role="dialog"
            aria-labelledby="myModalLabel33" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
              <div class="modal-content">
                <div class="modal-header" style="padding:1.3rem;" id="kemaskini-jenis-cenderamata">
                  <h4 class="modal-title text-3" id="myModalLabel33">UPDATE EXAM RESULT
                  </h4>
                  <button type="button" class="close model-remove-icon-bg" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x modal-close-icon-1"></i>
                  </button>
                </div>
                <div class="modal-body" style="padding: 1.3rem 1.3rem 0 1.3rem;">
                  <div class="modal-content-border">
                    <form id="formEditResult">
                        <span class="font-title">Practical Exam</span>
                      <p> </p> <hr>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="name">Part A<span style="color:#ff0000">*</span></label>
                                    <input type="text" class="form-control" id="partA" name="partA"
                                        value="" required>
                                        {{-- <div class="error-box-class" id="error-partA">
                                            Please fill in this field.
                                        </div> --}}
                                </div>
                            </div>
                       
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="name">Part B<span style="color:#ff0000">*</span></label>
                                    <input type="text" class="form-control" id="partB" name="partB"
                                        value="" required>
                                        {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                            Please fill in this field.
                                        </div> --}}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="name">Part C<span style="color:#ff0000">*</span></label>
                                    <input type="text" class="form-control" id="partC" name="partC"
                                        value="" required>
                                        {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                            Please fill in this field.
                                        </div> --}}
                                </div>
                            </div>


                        </div>

                        <div class="form-group">
                            <label for="totalResultPractical">Total Practical Exam</label>
                            <input type="text" class="form-control mt-1" name="totalResultPractical" id="totalResultPractical"
                              placeholder="Please fill in this field." autocomplete="off">
        
                            {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                Please fill in this field.
                            </div> --}}
                        </div>

                      <div class="form-group">
                        <label for="statusResultPracticalExam">Result Practical Exam</label>
                        
                          <select class="form-control mt-1" name="statusResultPracticalExam" id="statusResultPracticalExam">
                            <option>Please Select </option>
                            <option value="Pass">Pass </option>
                            <option value="Fail">Fail </option>
                           
                          </select>
                          
    
                        {{-- <div class="error-box-class" id="error-jenisCenderamata">
                            Please fill in this field.
                        </div> --}}
                       
                      </div> <p> &nbsp;&nbsp;</p><p></p><p></p>
                       <span class="font-title">Theory Exam</span>
                      <p> </p><hr>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="part-AT">Part A<span style="color:#ff0000">*</span></label>
                                    <input type="text" class="form-control" id="part-AT" name="part-AT"
                                        value="" required>
                                        {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                            Please fill in this field.
                                        </div> --}}
                                </div>
                            </div>
                   
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="part-BT">Part B<span style="color:#ff0000">*</span></label>
                                    <input type="text" class="form-control" id="part-BT" name="part-BT"
                                        value="" required>
                                        {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                            Please fill in this field.
                                        </div> --}}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="part-CT">Part C<span style="color:#ff0000">*</span></label>
                                    <input type="text" class="form-control" id="part-CT" name="part-CT"
                                        value="" required>
                                        {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                            Please fill in this field.
                                        </div> --}}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="totalResultTheory">Total Theory Exam</label>
                            <input type="text" class="form-control mt-1" name="totalResultTheory" id="totalResultTheory"
                              placeholder="Please fill in this field." autocomplete="off">
        
                            {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                Please fill in this field.
                            </div> --}}
                        </div>


                        <div class="form-group">
                            <label for="statusResultTheory">Result Total Theory Exam</label>
                            <select class="form-control mt-1" name="statusResultTheory" id="statusResultTheory">
                                <option>Please Select </option>
                                <option value="Pass">Pass </option>
                                <option value="Fail">Fail </option>
                               
                            </select>
                              
        
                            {{-- <div class="error-box-class" id="error-jenisCenderamata">
                                Please fill in this field.
                            </div> --}}
                        </div>
                      
                        <input type="hidden" class="form-control mt-1" name="exam_type_1" id="exam_type_1"
                        autocomplete="off">
                        <input type="hidden" class="form-control mt-1" name="exam_type_2" id="exam_type_2"
                        autocomplete="off">
                        <input type="hidden" class="form-control mt-1" name="idExamPractical" id="idExamPractical"
                         autocomplete="off">
                        <input type="hidden" class="form-control mt-1" name="totalTheory" id="idExamTheory"
                         autocomplete="off">
  
                    </form>
                  </div>
                </div>
                <div class="modal-footer" style="border-top: none;">
                  <button type="button" class="btn btn-cancel-1" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Close</span>
                  </button>
                  <button type="button" class="btn btn-primary ml-1" id="hantarEditBtn">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span id="spanSimpan" class="d-none d-sm-block">Save</span>
                  </button>
    
                </div>
              </div>
            </div>
        </div>




    

      <!--modal confirmation -->
      



      <!-- --->
      
      <div class="row">
        <div class="col-12">
          <div class="modal fade" id="confirmationEditModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">UPDATE EXAM RESULT</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                  </button>
                </div>
                <div class="modal-body">
                  <p class="mb-0">
                    Are you sure you want to update this exam result?
                  </p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-cancel-1" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">No</span>
                  </button>
    
                  <button type="button" class="btn btn-add-2 ml-1" data-dismiss="modal" id="submitKemaskiniBtn">
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Yes</span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 

      <div class="modal" id="confirmationEditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- Header content here -->
                </div>
                <div class="modal-body">
                    <!-- Body content here -->
                </div>
                <div class="modal-footer">
                    <!-- Footer content here -->
                </div>
            </div>
        </div>
    </div>




 

   
    </section>
</main>
  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    {{-- <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script>  --}}
    
@endsection



@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>


<script src="{{ asset('js/examMgmt/updateExamResult.js') }}"></script>

@endsection
