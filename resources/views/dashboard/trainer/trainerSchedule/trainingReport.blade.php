@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','TP - Training Report')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.form-group {
    display: flex;
    align-items: center; /* Vertical alignment */
}

label {
    width: 100px; /* Adjust as needed for your design */
    text-align: left; /* Adjust for alignment (left, right, center) */
    /* margin-right: 10px; Spacing between label and input */
}

</style>
@endsection

@section('content')
<!-- Navbar -->
<!-- <nav class="navbar navbar-expand-lg"> -->


<!-- End Navbar -->

<main style="padding:50px;margin:50px;text-align: left">
@php
  $lastParameter = request()->segment(count(request()->segments()));
@endphp
<div class="container">
    <ol class="breadcrumb p-0 mb-0 poppins-semibold-12 light-grey">
              <li class="nav-item mg-sm ">
                  <a class="nav-link" href="{{ route('trainer-training-schedule-history') }}">Dashboard</a>
                      <span class="sr-only">(current)</span>
                  </a>
              </li>   
              <li class="nav-item mg-sm ">
                  <a class="nav-link" href="{{ route('trainer-AttendanceList',$lastParameter) }}">Attendance List</a>
                      <span class="sr-only">(current)</span>
                  </a>
              </li>          
              <li class="nav-item mg-sm ">
                  <a class="nav-link" href="{{ route('trainer-UpdateExamResult', $lastParameter) }}">Update Exam Result
                      <span class="sr-only">(current)</span>
                  </a>
              </li>
              <li class="nav-item mg-sm ">
                  <a class="nav-link" href="{{ route('trainer-TrainingReport',$lastParameter) }}">Training Report
                      <span class="sr-only">(current)</span>
                  </a>
              </li>
    </ol>
  </div>
<section class="header-top-margin">
<div class="card mt-7" style="padding:50px;margin:50px;background-color:#fbf9f9">

{{-- <form method="POST" action="/submit">
    @csrf --}}
    
    <div class="form-group">
        <label class="poppins-semibold-14 mb-1 font-title" for="jenisCenderamata-0">Training Report</label>
    </div>

    <div class="row">
    <div>


    <div class="row">
            <div class="col-auto" style="position: relative;">
               
                <h4 class="user-name mb-0"><span id="">Energy Efficiency Management for Air-Conditioning and Mechanical Ventilation (ACMV) System Advance</span></h4>
                {{-- <h6 class="text-muted">Developer Team</h6> --}}
             </div>

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="name">Provider<span style="color:#ff0000">*</span></label>
                   <span id="provider">Universiti Kuala Lumpur - British Malaysia Institute (UniKL BMI)</span>
                    {{-- <div class="error-box-class" style="margin-top: 3px;" id="error-nama-peribadi">
                        *Sila lengkapkan medan ini
                    </div> --}}
                </div>
            </div>
     
           
        </div>

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="name">Start Date :</label>
                    <span id="dateStart">8 Mar 2023</span>
                </div>
            </div>
        </div>

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="name">End Date :</label>
                    <span id="dateStart">9 Mar 2023</span>
                </div>
            </div>
        </div>

        Training Report

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span id="dateStart">Training Name: </span>
                    <span id="dateStart">MASHRAE Secretariat, Kelana Business Center</span>
                </div>
            </div>
        </div>

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>Training Code : </span>
                    <span id="dateStart"> DD09</span>
                </div>
            </div>
        </div>

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>Course Date :</span>
                    <span id="dateStart"> 9 Mar 2023</span>
                </div>
            </div>
        </div>

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="">
                    <span>Competent Trainers :</span>
                    <span id="dateStart">Mohd</span>
                </div>
            </div>
        </div>


        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>Report Prepared by : Puan Ayu </span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>


        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>Synopsis :  </span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>Introduction :  </span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>Trainers :  Mohd</span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>

        Course Operation

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>Scheduled : </span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>


        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>Teaching Materials :  </span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>


        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>Discussion, group presentation and survey questionnaire :  </span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>

        Assessment

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>Practical exam : 100</span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>


        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>Theory Exam :  100 </span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>

        Results and Conclusion


        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span> Final Results : 100</span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>Conclusion : Extinction </span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>



        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>APPENDIX 1 - Detail of Results :</span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>
       

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>APPENDIX 2 - Schedule of Activities : </span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>APPENDIX 3 - Copy of Participant NRIC : </span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>

        <div class="row" style="margin:10px;">
            <div class="col-sm-12">
                <div class="form-group">
                    <span>APPENDIX 3 - Copy of Pasport size picture :  </span>
                    <span id="dateStart"></span>
                </div>
            </div>
        </div>
       
       
        

       


       

          <div class="d-flex justify-content-end pt-3 pb-2">
                <div style="margin:10px;">
                    <a href="" class="btn btn-secondary-6 btn-block">Cancel</a>
                </div>
    
                <div style="margin:10px;">
                    <a href="" class="btn btn-secondary-6 btn-block">Submit</a>
                </div>
          </div>
        </div>
    </div>
</section>
</main>
@endsection

@section('vendor-scripts')

@endsection

@section('page-scripts')
<script>
let host = "{{ env('API_SERVER') }}";
let userID = "{{ $userid }}";



</script>

<script src="{{ asset('js/userProfile/userProfile.js') }}"></script>
@endsection
