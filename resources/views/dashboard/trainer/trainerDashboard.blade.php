@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Hello')
{{-- vendor css --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .doughnut-chart{
        height: 25%;
        width: 25%;
    }

    .chart-title{
        color: #454545;
        font-size: 24px;
        font-family: Open Sans;
        font-weight: 600;
        line-height: 32px;
    }

    .carousel-container {
        display: flex;
        flex-direction: row !important;
        align-items: center;
        justify-content: center;
        margin-top: 3%;
    }

    .carousel-button{
        width: 65px;
        height: 65px;
        background: #3498DB;
        border-radius: 8px;
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .training-cards{
        width: 364px;
        height: 460px;
        border-radius: 8px;
    }

    .card-title{
        color: #152536;
        font-size: 20px;
        font-family: Open Sans;
        font-weight: 600;
    }

    .card-location{
        color: #6C757D;
        font-size: 14px;
        font-family: Open Sans;
        font-weight: 400;
    }

    .unconfirmed-details{
        border-radius: 8px;
        border: 1px solid var(--success-500, #F79009);
        background: var(--success-25, #FFFCF5);
        padding: 4px 25px;
        color: #F79009;
    }

    .contact-info-sets {
        display: flex;
        flex-direction: column;
        text-align: start;
        margin-bottom: 10%;
        margin-right: 50%;
        white-space: nowrap;
    }

    .contact-info-name{
        color: #454545;
        font-size: 20px;
        font-family: Open Sans;
        font-weight: 600;
        line-height: 30px;
    }

    .button-text{
        color: #F9F6FE;
        font-size: 16px;
        font-family: Rajdhani;
        font-weight: 700;
        text-transform: uppercase;
        line-height: 24px;
    }

    .logo-card{
        color: #FFFFFF;
    }

    .tr{
        background-color: white !important;
    }

    .text-center {
        text-align: center !important;
    }
</style>
@endsection

@section('content')
<main style="padding:50px;margin:50px;">
    <section class="header-top-margin">
        <div class="col-md-12">
            <div class="text-center mb-3" style="margin-left: 3%; margin-top: 3%;">
                <h2 class="page-title py-2" style="text-align: left !important;">Training Calendar</h2>
            </div>
        </div>

        <div class="col-md-10 py-4 mx-auto" style="margin-bottom: 3%;">
            <div id="TrainerList_wrapper" class="dataTables_wrapper no-footer">
                <table class="table table-bordered table-striped dataTable no-footer" id="TrainerList" style="width: 100%;">
                    <thead class="table-header">
                        <tr>
                            <th class="table-title text-center" width="40">No.</th>
                            <th class="table-title text-center">Programme</th>
                            <th class="table-title text-center">Start Date</th>
                            <th class="table-title text-center">End Date</th>
                            <th class="table-title text-center">Venue</th>
                            <th class="table-title text-center">Fee (RM)</th>
                            <th class="table-title text-center">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-12">
            <div class="text-center mb-3" style="margin-left: 3%;">
                <h2 class="page-title py-2" style="text-align: left !important;">Cumulative Training Chart</h2>
            </div>
        </div>

        <!-- Dynamically generated doughnut charts -->
        <div class="col-md-10 py-4" style="display: flex; flex-direction: row; margin-bottom: 5%; margin-left: 5%;">
            <div class="doughnut-chart col-md-3 mx-auto">
                <label class="chart-title">Renewable Energy</label>
                <canvas id="doughnut-chart-1"></canvas>
            </div>
            <div class="doughnut-chart col-md-3 mx-auto">
                <label class="chart-title">Energy Efficiency</label>
                <canvas id="doughnut-chart-2"></canvas>
            </div>
            <div class="doughnut-chart col-md-3 mx-auto">
                <label class="chart-title">Sustainable Energy</label>
                <canvas id="doughnut-chart-3"></canvas>
            </div>
        </div>

            <div class="form-group">
  
                <div class="card-body">
                    @csrf
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    <table id=traininglist class="table table-bordered datatable">
                <tr>
                    <th>No</th>
                    <th>Partner Name</th>
                    <th>Course Category</th>
                    <th>Training Type</th>
                    <th>Training Date (Start Date)</th>
                    <th>Training Date (End Date)</th>
                    <th>Status</th>
                    <th width="280px">Action</th>
                </tr>

                @foreach ($trainerdata as $trainerview)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $trainerview->partner }}</td>
                    <td>{{ $trainerview->course_name }}</td>
                    <td>{{ $trainerview->type }}</td>
                    <td>{{ $trainerview->date_start }}</td>
                    <td>{{ $trainerview->date_end }}</td>
                    <td>
                        @if ($trainerview->status == 1)
                            Active
                        @else
                            Not Active
                        @endif
                    </td>
                    <td>
                    <a class="btn btn-info" href="">View Details</a>

</td>
                </tr>
                 @endforeach
            </table>
            
    </section>
</main>  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment@1.1.2"></script>
    <script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="{{ asset('js/dashboard/trainerCharts.js') }}"></script>
@endsection

@section('page-scripts')
    <script>
        $(document).ready(function() {
            $('#traininglist').DataTable();
        });
    </script>
@endsection
