@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Participant Dashboard')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
{{-- add css style here --}}
<style>

.layoutdetails_half{
display: flex;
width: 100%;
padding-left: 40px;
padding-top: 20px;
padding-bottom: 20px;
flex-direction: column;
align-items: flex-start;
gap: var(--spacing-24, 24px);
border-radius: 16px;
border: 1px solid var(--gray-400, #98A2B3);
background: #FFF;
}

.font-title{
    color: #656565;

font-family: Rajdhani;
font-size: 26px;
font-style: normal;
font-weight: 600;
line-height: 32px; /* 123.077% */
}

.normal_text{
    color: #454545;

font-family: Open Sans;
font-size: 16px;
font-style: normal;
font-weight: 400;
line-height: 28px; /* 175% */

}
.spanwidth{
    width: 95px;
}

.page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 28px;
        font-style: normal;
        font-weight: 600;
    }

    .contentUpdate{
        color: #454545;
        font-family: Open Sans;
        font-size: 22px;
        font-style: normal;
        font-weight: 600;
        line-height: 28px; /* 155.556% */
        text-align: left;
    }

    .contentUpdate2{
        color: #454545;
        font-family: Open Sans;
        font-size: 18px;
        font-style: normal;
        font-weight: 600;
        line-height: 28px; /* 155.556% */
        text-align: left;
    }


</style>
@endsection

@section('content')
<main style="padding:50px;margin:50px;">
    <!-- Paid Section -->
    <section class="header-top-margin paid-section">
        <div class="contentUpdate2">
        Hi, <span class="font-title"><?php echo $salutation; ?> <?php echo $participant_name; ?></span>
            <div class="contentUpdate2"><span>Your application is <?php echo $status_name; ?>.
                <br>You may make payment in advance.</span></div>
        </div>
      
        <div class="row" style="margin:2px;">
            <div class="col-sm-12">
                <div class="layoutdetails_half">
                <h2 class="contentUpdate py-2" style="text-align: left !important; margin-bottom: 1%;" id="training_name"><?php echo $training_name; ?></h2>
                    <span class="font-title" id="training_name"></span>
                    <div class="contentUpdate2" style="margin-top: -20px;"><span >Provider</span>:<span><?php echo $training_provider; ?></span></div>
                    <div class="contentUpdate2" style="margin-top: -20px;"><span>Start Date</span>:<span><?php echo $training_startdate; ?></span></div>
                    <div class="contentUpdate2" style="margin-top: -20px;"><span>End Date</span>:<span ><?php echo $training_enddate; ?></span></div>
                    <div class="contentUpdate2" style="margin-top: -20px;"><span>Venue Address</span>:<span><?php echo $training_venue_address; ?></span></div>
                    <div class="contentUpdate2" style="margin-top: -20px;"><span>Fee</span>:RM<span></span><?php echo $training_fees; ?></div>
                </div>
            </div>
        </div>
        <div style="margin:10px;">
            {{-- <button  id="saveBtn" class="btn btn-secondary-6 btn-block">
              Submit
            </button> --}}
            @php
            $lastParameter = request()->segment(count(request()->segments()));
            @endphp 
            <a href="{{ route('participant-Payment-Details',$lastParameter) }}" class="btn btn-primary">Payment</a>
        </div>
    </section>
</main>
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
    {{-- <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>  --}}
    
@endsection

@section('page-scripts')
{{-- our own js --}}

@endsection
