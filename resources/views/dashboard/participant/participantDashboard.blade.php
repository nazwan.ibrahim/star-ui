@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Participant Dashboard-My Dashboard')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
{{-- add css style here --}}
<style>


.tablecontent{
color: #333;
font-family: Open Sans;
font-size: 16px;
font-style: normal;
font-weight: 400;
line-height: 20px; /* 125% */
text-align: left;
}


.header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .dataTables_length {
        display: none;
    }

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .paid-status {
        border-radius: 8px;
        border: 1px solid #18eeaa;
        background: var(--success-25, #f6faf7);
        padding: 4px 8px;
        color: #18eeaa;
    }

    .failed-status {
        border-radius: 8px;
        border: 1px solid #F04438;
        background: var(--success-25, #FFFBFA);
        padding: 4px 8px;
        color: #F04438;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

   
    .filter-dropdown{
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }

    .text-center {
        text-align: center !important;
    }
</style>
@endsection

@section('content')
<main class="header-top-margin col-md-10 mx-auto">
    <section class="header-top-margin">
        

               
    <div class="row" style="margin-top: 7%;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2 class="page-title">My Training List</h2>
            </div>
            
         </div>
    </div>
    @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
    @else
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
    @endif
  
                <div class="table-responsive">
                    <div id="upcomingList_wrapper" class="dataTables_wrapper no-footer">
                    <table class="table table-bordered table-striped dataTable no-footer" id="trainingList" style="width: 100%; border-bottom: white;">
                        <thead class="table-header">
                           <tr>
                            <th class="table-title text-center" style="border-bottom: none;" width=40; scope="col">No</th>
                            <th class="table-title text-center" style="border-bottom: none;" scope="col">Programme</th>
                            <th class="table-title text-center" style="border-bottom: none;" scope="col">Start Date</th>
                            <th class="table-title text-center" style="border-bottom: none;" scope="col">End Date</th>
                            <th class="table-title text-center" style="border-bottom: none;" scope="col">Venue</th>
                            <th class="table-title text-center" style="border-bottom: none;" scope="col">Status</th>
                            <th class="table-title text-center" style="border-bottom: none;" scope="col">Action</th>
                            
                        </tr>
                        </thead>
                        <tbody class="tablecontent">
                      
                        </tbody>
                    </table>
                    </div>
                </div>

                {{-- Training List Section --}}

                <div class="row" style="margin-top: 7%;">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2 class="page-title">Upcoming Trainings</h2>
                        </div>
                        
                    </div>
                </div>

                <div class="table-responsive">
                    <div id="upcomingList_wrapper" class="dataTables_wrapper no-footer">
                        <table class="table table-striped dataTable no-footer" id="upcomingList" style="width: 100%; border-bottom: white;">
                            <thead class="table-header">
                                <tr>
                                    <th class="table-title text-center" style="border-bottom: none;" width=40; scope="col">No.</th>
                                    <th class="table-title text-center" style="border-bottom: none;" scope="col">Programme</th>
                                    <th class="table-title text-center" style="border-bottom: none;" scope="col">Course Category</th>
                                    <th class="table-title text-center" style="border-bottom: none;" scope="col">Training Type</th>
                                    <th class="table-title text-center" style="border-bottom: none;" scope="col">Start Date</th>
                                    <th class="table-title text-center" style="border-bottom: none;" scope="col">End Date</th>
                                    <th class="table-title text-center" style="border-bottom: none;" scope="col">Status</th>
                                    <th class="table-title text-center" style="border-bottom: none;" scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                {{-- Training List Section --}}

                {{-- <div class="text-center mb-3" style="margin-left: 3%; margin-top: 15%;">
                    <h2 class="page-title py-2" style="text-align: left !important;">Cumulative Training Chart</h2>
                </div>

                <!-- Dynamically generated doughnut charts -->
                <div class="col-md-10 py-4" style="display: flex; flex-direction: row; margin-bottom: 5%; margin-left: 5%;">
                    <div class="doughnut-chart col-md-3 mx-auto">
                        <label class="chart-title">Renewable Energy</label>
                        <canvas id="doughnut-chart-1"></canvas>
                    </div>
                    <div class="doughnut-chart col-md-3 mx-auto">
                        <label class="chart-title">Energy Efficiency</label>
                        <canvas id="doughnut-chart-2"></canvas>
                    </div>
                    <div class="doughnut-chart col-md-3 mx-auto">
                        <label class="chart-title">Sustainable Energy</label>
                        <canvas id="doughnut-chart-3"></canvas>
                    </div>
                </div> --}}
           
        </div>

</section>
</main> 


@endsection

@section('vendor-scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment@1.1.2"></script>
    <script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
@endsection

@section('page-scripts')
{{-- our own js --}}
<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>

<script src="{{ asset('js/dashboard/ParticipantTraining.js') }}"></script>
@endsection
