<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Certificate of Attendance</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .logo {
            max-width: 100px;
            margin-bottom: 20px;
        }

        .certificate-title {
            font-family: 'Brush Script', cursive, sans-serif;
            font-size: 24px;
            font-weight: bold;
            text-align: center;
            margin-bottom: 20px;
        }

        .participant-details,
        .training-details {
            margin-bottom: 20px;
            text-align: center;
        }

        .signature {
            margin-top: 200px; /* Increase the margin to move it lower */
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="container">
        <img src="{{ asset('images/logo seda.png') }}" alt="Logo" class="logo">
        <h2 class="certificate-title" style="font-weight: bold;font-family: Rajdhani;font-size: 40px;">Certificate of Attendance</h2>
        <p class="participant-details">This is to certify that</p>

            <p class="participant-details" style="font-weight: bold; margin-bottom: 1px;">{{ $data->fullname }}</p>
            <p class="participant-details" style="font-weight: bold;">I / C No.: {{ $data->ic_no }}</p>
            <p class="participant-details">has attended</p> 
            <p class="training-details" style="font-weight: bold;">{{ $data->course_name }}</p>
            <p class="training-details" style="font-weight: bold;">at {{ $data->venue }}</p>
            <p class="training-details" style="font-weight: bold;">on {{ $data->date_start }} until {{ $data->date_end }} session</p>

        <p class="signature">(Dato ' Hamzah Bin Hussin)<br>Chief Executive Officer<br>Sustainable Energy Development Authority Malaysia</p>
    </div>
</body>
</html>
