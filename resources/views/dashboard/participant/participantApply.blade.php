@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Training Details')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .left-side {
        float: left;
        width: 33%;
    }

    .right-side {
        float: left;
        width: 67%;
    }

    .dataTables_length {
        display: none;
    }

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .filter-dropdown{
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }

    .side-by-side-buttons {
        display: inline-block;
        margin-right: 5px; /* Adjust the margin as needed */
    }

    .detail-row {
    margin-bottom: 15px;
}

.label {
    font-weight: bold;
    color: #333; /* Adjust color as needed */
}

.value {
    margin-left: 10px;
}

.trainer-list {
    list-style: none;
    padding: 0;
}

.trainer-list li {
    margin-bottom: 5px;
    font-family: 'Open Sans', sans-serif; /* Use your preferred font-family */
    font-size: 16px;
    color: #555; /* Adjust color as needed */
}

.detail-container {
        text-align: left;
    }

    .detail-row {
        display: flex;
        align-items: center;
        margin-bottom: 10px; /* Add margin for better spacing between rows if needed */
    }

    .detail-row i {
        margin-right: 10px; /* Adjust the margin for the icon */
    }
</style>
@endsection

@section('content')
<main class="header-top-margin col-md-12 mx-auto" style="margin-bottom: 85%;">
    <!-- left side -->
    <form action="{{ route('applyForTraining', ['id' => $training->id]) }}" method="POST">
    @csrf
    <div class="form-section" id="pagetraining">

    <div class="left-side py-4" style="border-right: 1px solid #98A2B3; display: flex; flex-direction: column; align-items: center;">

    <!-- Training Image Section -->
    <div style="padding: 20px 0;">
        <div style="width: 300px; height: 300px; background: #EAECF0; position: relative;">
            <img src="/storage/{{$training->training_image}}" style="width: 290px; height: 293px;" alt="Training Image">
        </div>
        <div style="color: #656565; font-size: 32px; font-family: Rajdhani; font-weight: 600; word-wrap: break-word; margin-top: 20px;">
            Training Course
        </div>
    </div>

    <!-- Details Section -->
    <div style="display: flex; flex-direction: column; gap: 10px; padding-bottom: 5%;">

    <div class="detail-container">
        <!-- Status -->
        <div class="detail-row">
        <i class="fas fa-users" style="font-size: 18px; color: #3498DB; align-item:left;"></i>
            <span class="label">Status :</span>
            <span class="value">{{ $train }}</span>
        </div>

        <!-- Mode of Training -->
        <div class="detail-row">
        <i class="fas fa-keyboard" style="font-size: 18px; color: #3498DB; align-item:left;"></i>
            <span class="label">Mode of Training :</span>
            <span class="value">{{ $training->mode }}</span>
        </div>

        <!-- Course Category -->
        <div class="detail-row">
        <i class="fas fa-newspaper" style="font-size: 18px; color: #3498DB; align-item:left;"></i>
            <span class="label">Course Category :</span>
            <span class="value">{{ $training->course_category }}</span>
        </div>

        <!-- Training Type -->
        <div class="detail-row">
        <i class="fas fa-dot-circle" style="font-size: 18px; color: #3498DB; align-item:left;"></i>
            <span class="label">Training Type :</span>
            <span class="value">{{ $training->training_type }}</span>
        </div>

        <!-- Location -->
        <div class="detail-row">
        <i class="fas fa-map-marker-alt" style="font-size: 18px; color: #3498DB; align-item:left;"></i>
            <span class="label">Location :</span>
            <span class="value">{{ $training->venue }} {{ $training->venue_address }}</span>
        </div>

        @if(count($trainer) > 0)
    <div class="detail-row" style="display: flex; align-items: center;">
        <i class="fas fa-user-alt" style="font-size: 18px; color: #3498DB; align-item:left;"></i>
        <span class="label" style="margin-right: 10px;">Trainer Name:</span>
        <span class="value">
            <ul class="trainer-list" style="list-style-type: none; padding: 0; display: flex; flex-wrap: wrap; margin: 0;">
                @foreach ($trainer as $train)
                    <li style="margin-right: 10px;">{{ $train->fullname }}</li>   
                @endforeach
            </ul>
        </span>
    </div>
@endif



        <!-- Start Time -->
        <div class="detail-row">
        <i class="fas fa-clock" style="font-size: 18px; color: #3498DB; align-item:left;"></i>
            <span class="label">Start Time :</span>
            <span class="value">{{ $training->start_time}}</span>
        </div>

        <!-- End Time -->
        <div class="detail-row">
        <i class="far fa-clock" style="font-size: 18px; color: #3498DB; align-item:left;"></i>
            <span class="label">End Time :</span>
            <span class="value">{{ $training->end_time}}</span>
        </div>

        <!-- Start Date -->
        <div class="detail-row">
        <i class="	fas fa-calendar-alt" style="font-size: 18px; color: #3498DB; align-item:left;"></i>
            <span class="label">Start Date :</span>
            <span class="value">{{ $training->date_start }}</span>
        </div>

        <!-- End Date -->
        <div class="detail-row">
        <i class="far fa-calendar-alt" style="font-size: 18px; color: #3498DB; align-item:left;"></i>
            <span class="label">End Date :</span>
            <span class="value">{{ $training->date_end }}</span>
        </div>

        <!-- No. of Participants -->
        <div class="detail-row">
        <i class="fas fa-users" style="font-size: 18px; color: #3498DB; align-item:left;"></i>
            <span class="label">No. of Participants :</span>
            <span class="value">{{ $training->total_participant }}</span>
        </div>

        <!-- Local Fee (RM) -->
        <div class="detail-row">
        <i class="fas fa-money-bill-wave" style="font-size: 18px; color: #3498DB; align-item:left;"></i>
            <span class="label">Local Fee (RM):</span>
            @foreach($feeList as $fee)
                @if($fee->fee_category === 'Local')
                    <span class="value">{{ $fee->fee_name}} {{ $fee->amount }}</span>
                @endif
            @endforeach
        </div>

        <!-- International Fee (RM) -->
        <div class="detail-row">
        <i class="fas fa-money-bill-wave-alt" style="font-size: 18px; color: #3498DB; align-item:left;"></i>
            <span class="label">International Fee (RM):</span>
            @foreach($feeList as $fee)
                @if($fee->fee_category === 'Int')
                    <span class="value">{{ $fee->fee_name}} {{ $fee->amount }}</span>
                @endif
            @endforeach
        </div>
    </div>
</div>
</div>

        <!-- right side -->
        <div class="right-side py-4">
    <!-- Training Name -->
    <div class="py-4" style="flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 33px; display: flex; padding-left: 5%">
        <div style="color: #656565; font-size: 32px; font-family: Rajdhani; font-weight: 600; word-wrap: break-word">Training Name</div>
        <div style="font-family: Open Sans; font-size: 16px; margin-right: 10%;">
            {{ $training->course_name }} 
        </div>
    </div>
    <hr class="dropdown-divider">
    
    <!-- Description -->
    <div class="py-4" style="flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 33px; display: flex; padding-left: 5%">
        <div style="color: #656565; font-size: 32px; font-family: Rajdhani; font-weight: 600; word-wrap: break-word">Description : </div>
        <ol style="font-family: Open Sans; font-size: 16px; margin-right: 10%; text-align: left;">
            {{ $training->training_description }} 
        </ol>
    </div>
    <hr class="dropdown-divider">
    
    <!-- Objectives -->
    <div class="py-4" style="flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 33px; display: flex; padding-left: 5%">
        <div style="color: #656565; font-size: 32px; font-family: Rajdhani; font-weight: 600; word-wrap: break-word">Objectives :</div>
        <ol style="font-family: Open Sans; font-size: 16px; margin-right: 20%; text-align: left;">
            @foreach ($objectives as $objective)
                <li>{{ $objective->objective }}</li>   
            @endforeach
        </ol>
    </div>
    <hr class="dropdown-divider">
    
    <!-- Person of Interest -->
    <div class="py-4" style="flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 33px; display: flex; padding-left: 5%">
        <div style="color: #656565; font-size: 32px; font-family: Rajdhani; font-weight: 600; word-wrap: break-word">Person of Interest :</div>
        <ol style="font-family: Open Sans; font-size: 16px; margin-right: 20%; text-align: left;">
            @foreach ($adminTP as $tp)
                <li>{{ $tp->name }} - {{ $tp->phone_no }} - {{ $tp->email }}</li>   
            @endforeach
        </ol>
    </div>
</div>

            <hr class="dropdown-divider">
            <p class="small fw-bold mt-2">
                <button id="applyBtn" class="btn btn-primary text-white fw-bold mt-3 ms-0" type="button">Apply Now</button>
            </p>

        </div>
    </div>


    <div id="terms" style="display: none;">
    <br>

        <div style="width: 100%; height: 100%; flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 24px; display: inline-flex">
        <div style="width: 1360px; color: #656565; font-size: 26px; font-family: Rajdhani; font-weight: 600; line-height: 32px; word-wrap: break-word; text-align: left;">
            {{ $training->course_name }} 
            </div>

            <div style="col-md-12">
                <div style="col-md-6">1. Do you have CIDB Green Card?</div>
                <div class="col-md-12">
                    <div style="margin-bottom: 5px;">
                    <br>
                        <div style="display: flex; align-items: center;">
                            <input type="radio" value="1" id="cidbYes" name="cidb" style="margin-right: 5px;">
                            <div style="color: #454545; font-size: 16px; font-family: 'Open Sans', sans-serif; font-weight: 400; line-height: 24px; cursor: pointer;">Yes</div>
                        </div>

                        <div style="display: flex; align-items: center;">
                            <input type="radio" value="0" id="cidbNo" name="cidb" style="margin-right: 5px;">
                            <div style="color: #454545; font-size: 16px; font-family: 'Open Sans', sans-serif; font-weight: 400; line-height: 24px; cursor: pointer;">No</div>
                        </div>
                    </div>
                </div>


            </div>
            
            <div style="flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 16px; display: flex">
                <div style="width: 749px; height: 128px; position: relative">
                    <div style="left: 0px; top: 0px; position: absolute; color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">2. Have you taken any Malaysian government recognized safety course before?</div>
                    <div style="width: 720px; height: 44px; left: 29px; top: 84px; position: absolute; justify-content: flex-start; align-items: center; gap: 16px; display: inline-flex">
                        <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">If yes, please state the course name</div>
                        <div style="width: 205px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: inline-flex">
                            <input type="text" class="form-control" id="safety_course_name" name="safety_course_name" placeholder="Enter Course Name Here">
                        </div>
                        <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">Year</div>
                        <div style="flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: inline-flex">
                            <input type="date" class="form-control" name="safety_date_start" id="safety_date_start" placeholder="Select Date">

                        </div>
                    </div>
                    <div style="width: 136px; height: 24px; left: 29px; top: 44px; position: absolute; justify-content: flex-start; align-items: flex-start; gap: 40px; display: inline-flex">

                        <div style="justify-content: flex-start; align-items: center; gap: 8px; display: flex">
                            <div style="width: 16px; height: 16px; position: relative">
                            <input type="radio" value="1" id="safety_course" name="safety_course">
                                <div style="width: 6px; height: 6px; left: 5px; top: 5px; position: absolute; background: white; border-radius: 9999px"></div>
                            </div>
                            <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 24px; word-wrap: break-word">Yes</div>
                        </div>

                        <div style="justify-content: flex-start; align-items: center; gap: 8px; display: flex">
                            <div style="width: 16px; height: 16px; position: relative">
                                <input type="radio" value="0" id="safety_course" name="safety_course">
                                    </div>
                                        <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 24px; word-wrap: break-word">No</div>
                                    </div>
                            </div>
                        </div>

                        
                    </div>


                    
            <div style="justify-content: flex-start; align-items: center; gap: 24px; display: inline-flex">
                <div style="width: 773px; height: 492px; position: relative">
                    <div style="left: 0px; top: 0px; position: absolute; color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">3. Have you taken any mathematical education before?</div>
                    <div style="width: 136px; height: 24px; left: 29px; top: 44px; position: absolute; justify-content: flex-start; align-items: flex-start; gap: 40px; display: inline-flex">
                        <div style="justify-content: flex-start; align-items: center; gap: 8px; display: flex">
                            <div style="width: 16px; height: 16px; position: relative">
                            <input type="radio" value="1" id="math_education" name="math_education">
                                <div style="width: 6px; height: 6px; left: 5px; top: 5px; position: absolute; background: white; border-radius: 9999px"></div>
                            </div>
                            <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 24px; word-wrap: break-word">Yes</div>
                        </div>
                        <div style="justify-content: flex-start; align-items: center; gap: 8px; display: flex">
                        <div style="width: 16px; height: 16px; position: relative">
                            <input type="radio" value="0" id="math_education" name="math_education">
                            </div>
                            <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 24px; word-wrap: break-word">No</div>
                        </div>
                    </div>
                    <div style="width: 744px; height: 408px; left: 29px; top: 84px; position: absolute; flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 16px; display: inline-flex">
                        <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">If yes, please select the highest education level</div>
                        <div style="flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 16px; display: flex">
                            <div style="justify-content: flex-start; align-items: center; gap: 8px; display: inline-flex">
                                <div style="width: 16px; height: 16px; position: relative">
                                <input type="radio" value="0" id="highest_education" name="highest_education">
                                    <div style="width: 6px; height: 6px; left: 5px; top: 5px; position: absolute; background: white; border-radius: 9999px"></div>
                                </div>
                                <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 24px; word-wrap: break-word">SRP/PMR/PT3</div>
                            </div>
                            <div style="justify-content: flex-start; align-items: center; gap: 8px; display: inline-flex">
                                <div style="width: 16px; height: 16px; position: relative">
                                <input type="radio" value="1" id="highest_education" name="highest_education">
                                </div>
                                <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 24px; word-wrap: break-word">MCE/SPM/SPMV/STVM/STAM/A-LEVEL</div>
                            </div>
                            <div style="justify-content: flex-start; align-items: center; gap: 8px; display: inline-flex">
                                <div style="width: 16px; height: 16px; position: relative">
                                <input type="radio" value="2" id="highest_education" name="highest_education">
                                </div>
                                <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 24px; word-wrap: break-word">STPM/DIPLOMA/MATRIKS</div>
                            </div>
                            <div style="justify-content: flex-start; align-items: center; gap: 8px; display: inline-flex">
                                <div style="width: 16px; height: 16px; position: relative">
                                <input type="radio" value="3" id="highest_education" name="highest_education">
                                </div>
                                <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 24px; word-wrap: break-word">HIGHER DIPLOMA/ADV DIPLOMA</div>
                            </div>
                            <div style="justify-content: flex-start; align-items: center; gap: 8px; display: inline-flex">
                                <div style="width: 16px; height: 16px; position: relative">
                                <input type="radio" value="4" id="highest_education" name="highest_education">
                                </div>
                                <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 24px; word-wrap: break-word">DEGREE</div>
                            </div>
                            <div style="justify-content: flex-start; align-items: center; gap: 8px; display: inline-flex">
                                <div style="width: 16px; height: 16px; position: relative">
                                <input type="radio" value="5" id="highest_education" name="highest_education">
                                </div>
                                <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 24px; word-wrap: break-word">MASTER</div>
                            </div>
                            <div style="justify-content: flex-start; align-items: center; gap: 8px; display: inline-flex">
                                <div style="width: 16px; height: 16px; position: relative">
                                <input type="radio" value="6" id="highest_education" name="highest_education">
                                </div>
                                <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 24px; word-wrap: break-word">PHD</div>
                            </div>
                            <div style="justify-content: flex-start; align-items: center; gap: 8px; display: inline-flex">
                                <div style="width: 16px; height: 16px; position: relative">
                                <input type="radio" value="7" id="highest_education" name="highest_education">
                                </div>
                                <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 24px; word-wrap: break-word">OTHERS (SPECIAL EXAM)</div>
                            </div>
                            <div style="justify-content: flex-start; align-items: center; gap: 24px; display: inline-flex">
                                <div style="width: 350px; color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">If Other, please state the education name</div>
                                <div style="width: 250px; flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: inline-flex">
                                    <input type="text" class="form-control" id="others_education" name="others_education" placeholder="Enter Education Name Here">

                                </div>
                                <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">Date</div>
                                <div style="flex-direction: column; justify-content: center; align-items: flex-start; gap: 16px; display: inline-flex">
                                    <input type="date" class="form-control" name="edu_date_start" id="edu_date_start" placeholder="Select Date">
                                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="width: 789.50px; height: 756px; position: relative">
                <div style="left: 0px; top: 0px; position: absolute; color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">4. Entry requirements (please check):</div>
                <div style="left: 27px; top: 44px; position: absolute; color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word">I am / have</div>
                
                <div style="width: 198px; height: 28px; left: 27px; top: 88px; position: relative; display: flex; align-items: center; gap: 8px;">
                    <div style="width: 26px; height: 26px; justify-content: center; align-items: center; display: flex">
                        <div style="width: 26px; height: 26px; position: relative">
                        <input type="checkbox" value="1" id="age" name="age">
                        </div>
                    </div>
                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word; cursor: pointer;">Above 21 years of age</div>
                </div>

                <div style="width: 2000px; height: 60px; left: 27px; top: 88px; position: relative; display: flex; align-items: center; gap: 8px;">
                    <div style="width: 26px; height: 26px; justify-content: center; align-items: center; display: flex">
                        <div style="width: 26px; height: 26px; position: relative">
                            <label for="qualification" style="cursor: pointer;">
                                <input type="checkbox" value="2" id="qualification" name="qualification">
                            </label>
                        </div>
                    </div>
                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word; cursor: pointer;">Minimum of diploma in Engineering/ Engineering Technology or Degree (BSc.) in science Physics / Applied Physics / industrial physics or equivalent as recognized by SEDA Malaysia</div>
                </div>

                <div style="width: 2000px; height: 30px; left: 27px; top: 88px; position: relative; display: flex; align-items: center; gap: 8px;">
                    <div style="width: 26px; height: 26px; justify-content: center; align-items: center; display: flex">
                        <div style="width: 26px; height: 26px; position: relative">
                            <label for="qualification" style="cursor: pointer;">
                            <input type="checkbox" value="1" id="safe_knowledge" name="safe_knowledge">
                            </label>
                        </div>
                    </div>
                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word; cursor: pointer;">Some knowledge of safe work practices</div>
                </div>

                <div style="width: 2000px; height: 50px; left: 27px; top: 88px; position: relative; display: flex; align-items: center; gap: 8px;">
                    <div style="width: 26px; height: 26px; justify-content: center; align-items: center; display: flex">
                        <div style="width: 26px; height: 26px; position: relative">
                            <label for="qualification" style="cursor: pointer;">
                            <input type="checkbox" value="1" id="read_knowledge" name="read_knowledge">
                            </label>
                        </div>
                    </div>
                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word; cursor: pointer;">Reading skills for comprehending technical subject matter</div>
                </div>

                <div style="width: 2000px; height: 40px; left: 27px; top: 88px; position: relative; display: flex; align-items: center; gap: 8px;">
                    <div style="width: 26px; height: 26px; justify-content: center; align-items: center; display: flex">
                        <div style="width: 26px; height: 26px; position: relative">
                            <label for="qualification" style="cursor: pointer;">
                            <input type="checkbox" value="1" id="understanding_knowledge" name="understanding_knowledge">
                            </label>
                        </div>
                    </div>
                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word; cursor: pointer;">Able to read and understand English</div>
                </div>

                <div style="width: 2000px; height: 40px; left: 27px; top: 88px; position: relative; display: flex; align-items: center; gap: 8px;">
                    <div style="width: 26px; height: 26px; justify-content: center; align-items: center; display: flex">
                        <div style="width: 26px; height: 26px; position: relative">
                            <label for="qualification" style="cursor: pointer;">
                            <input type="checkbox" value="1" id="health_insurance" name="health_insurance">
                            </label>
                        </div>
                    </div>
                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word; cursor: pointer;">Covered with proper recognized medical health insurance</div>
                </div>

                <div style="width: 2000px; height: 40px; left: 27px; top: 88px; position: relative; display: flex; align-items: center; gap: 8px;">
                    <div style="width: 26px; height: 26px; justify-content: center; align-items: center; display: flex">
                        <div style="width: 26px; height: 26px; position: relative">
                            <label for="qualification" style="cursor: pointer;">
                            <input type="checkbox" value="1" id="safety_priority" name="safety_priority">
                            </label>
                        </div>
                    </div>
                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word; cursor: pointer;">Will give safety as utmost priority</div>
                </div>

                <div style="width: 2000px; height: 40px; left: 27px; top: 88px; position: relative; display: flex; align-items: center; gap: 8px;">
                    <div style="width: 26px; height: 26px; justify-content: center; align-items: center; display: flex">
                        <div style="width: 26px; height: 26px; position: relative">
                            <label for="qualification" style="cursor: pointer;">
                            <input type="checkbox" value="1" id="rules" name="rules">
                            </label>
                        </div>
                    </div>
                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word; cursor: pointer;">Follow all rules and regulations from the organizer have knowledge and skills in :</div>
                </div>

                <div style="width: 2000px; height: 40px; left: 27px; top: 88px; position: relative; display: flex; align-items: center; gap: 8px;">
                    <div style="width: 26px; height: 26px; justify-content: center; align-items: center; display: flex">
                        <div style="width: 26px; height: 26px; position: relative">
                            <label for="qualification" style="cursor: pointer;">
                            <input type="checkbox" value="1" id="electric_formula" name="electric_formula">
                            </label>
                        </div>
                    </div>
                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word; cursor: pointer;">Electricity, electrical terms and common formula</div>
                </div>

                <div style="width: 2000px; height: 40px; left: 27px; top: 88px; position: relative; display: flex; align-items: center; gap: 8px;">
                    <div style="width: 26px; height: 26px; justify-content: center; align-items: center; display: flex">
                        <div style="width: 26px; height: 26px; position: relative">
                            <label for="qualification" style="cursor: pointer;">
                                <input type="checkbox" value="1" id="working_knowledge" name="working_knowledge">
                            </label>
                        </div>
                    </div>
                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word; cursor: pointer;">Working knowledge of tools and meters used in the installation and maintenance of electrical systems; and basic customer education and service practices</div>
                </div>

                <div style="width: 2000px; height: 40px; left: 27px; top: 88px; position: relative; display: flex; align-items: center; gap: 8px;">
                    <div style="width: 26px; height: 26px; justify-content: center; align-items: center; display: flex">
                        <div style="width: 26px; height: 26px; position: relative">
                            <label for="qualification" style="cursor: pointer;">
                                <input type="checkbox" value="1" id="declaration" name="declaration">
                            </label>
                        </div>
                    </div>
                    <div style="color: #454545; font-size: 16px; font-family: Open Sans; font-weight: 400; line-height: 28px; word-wrap: break-word; cursor: pointer;">
                        Declare all the information is correct and that I am fit and eligible to attend the course
                    </div>
                </div>

                <!-- Move the buttons closer to the checkbox -->
                <div style="margin-top: 110px; display: flex; justify-content: space-between; width: 150%;">
                    <button type="button" class="w-10 btn btn-outline-danger" id="prevButton">Previous</button>
                    <button type="submit" class="w-10 btn btn-primary" id="submitButton">Submit</button>
                </div>
        </div>
    </div>
</form>
</main>
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
@endsection

@section('page-scripts')
{{-- our own js --}}
    <script>
    // Get references to form sections and buttons
    const page1 = document.getElementById("pagetraining");
    const page2 = document.getElementById("terms");
    const nextButton = document.getElementById("applyBtn");
    const prevButton = document.getElementById("prevButton");
    const submitButton = document.getElementById("submitButton");
    const progressBullets = document.querySelectorAll("#progress-bullets li");

    let currentPage = 1;

    

    // Function to update progress bullets
    function updateProgressBullets() {
        progressBullets.forEach((bullet, index) => {
            if (index === currentPage - 1) {
                bullet.classList.add("active");
            } else {
                bullet.classList.remove("active");
            }
        });
    }
    

    function nextPage() {
        if (currentPage === 1) {
            // Check if page 1 is active
            // You can add additional validation here if needed
           
        
        }

        if (currentPage < 2) {
            currentPage++;
            page1.style.display = currentPage === 1 ? "block" : "none";
            page2.style.display = currentPage === 2 ? "block" : "none";
            nextButton.style.display = currentPage === 1 ? "block" : "none";
            prevButton.style.display = currentPage === 2 ? "block" : "none";
            submitButton.style.display = currentPage === 2 ? "block" : "none";

            updateProgressBullets();
        }
    }


    // Function to show the previous page and hide the current page
    function prevPage() {
        if (currentPage > 1) {
            currentPage--;
            page1.style.display = currentPage === 1 ? "block" : "none";
            page2.style.display = currentPage === 2 ? "block" : "none";
            nextButton.style.display = currentPage === 1 ? "block" : "none";
            prevButton.style.display = currentPage === 2 ? "block" : "none";
            submitButton.style.display = currentPage === 2 ? "block" : "none";

            updateProgressBullets();
        }
    }

    // Event listeners for the navigation buttons
    nextButton.addEventListener("click", nextPage);
    prevButton.addEventListener("click", prevPage);

</script>

@endsection