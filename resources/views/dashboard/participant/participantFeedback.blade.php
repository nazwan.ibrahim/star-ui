@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Feedback Question')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.icon-editIcon:before {
  content: "\e916";
}

.title{
    color: #656565;
    font-family: Rajdhani;
    font-size: 26px;
    font-style: normal;
    font-weight: 600;
    line-height: 38px; /* 146.154% */
}

.contentUpdate{
    color: #454545;
    font-family: Open Sans;
    font-size: 18px;
    font-style: normal;
    font-weight: 600;
    line-height: 28px; /* 155.556% */
    text-align: left;
}

.font-title-2{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 18px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

</style>
@endsection

@section('content')
<main style="padding:50px;margin:4px;">
    <section class="header-top-margin">
    @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
    @else
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
    @endif

    @csrf    

    <div class="row">
      <label class="poppins-semibold-14 mb-1 font-title" style="text-align: left;width:100%;">Feedback Question</label>
    </div>
    <div class="row">
        <div class="card mt-7" style="padding:50px;margin:50px;background-color:#fbf9f9;text-align: left;">
            <div >
                <span class="contentUpdate">Training Program:</span><span>&nbsp;{{$name}}</span>
            </div>
            <div>
            <input type="hidden" name="id_training" value="{{$id_training}}">
            </div>
            <div>
                <span class="contentUpdate">Type:</span><span>&nbsp;{{$category_name}}</span>
            </div>
        </div>
    </div>

    <div class="container mt-5">
    <h2>Feedback Question</h2>
    {{-- <form action="{{ route('add-participant-feedback') }}" method="POST" id="addFeedbackAnswer">
        @csrf --}}
    <table id= 'feedbackTable' class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Question</th>
                <th scope="col">Answer</th>
            </tr>
        </thead>
        <tbody style="text-align: left;width:100%;">
            @if ($feedbackquestion->isEmpty())
                <tr>
                    <td colspan="3">No data available</td>
                </tr>
            @else
                @foreach ($feedbackquestion as $feedback)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $feedback->question }}</td>
                        <td>
                        {{-- <form action="{{ route('add-participant-feedback') }}" method="POST" id="addFeedbackAnswer">
                            @csrf --}}
                        <input type="text" class="form-control" id="newFeedback{{ $loop->index + 1 }}" name="newFeedback{{ $loop->index + 1 }}"
                            placeholder="Enter your answer" value="{{ $feedback->feedback }}">
                            <input type="hidden" class="form-control" id="idFeedback{{ $loop->index + 1 }}" name="idFeedback{{ $loop->index + 1 }}"
                            placeholder="Enter your answer" value="{{ $feedback->questionid }}">
                        
                        
                        {{-- </form>    --}}
                        </td>
                    </tr>
                  
                @endforeach
            @endif
        </tbody>
    </table>

      <input type="submit" value="Submit" id="QueryBtn">    
                
              
</div>

    </section>
</main>

<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Confirm Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this feedback question?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <form id="deleteForm">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.delete-question').on('click', function () {
            var questionId = $(this).data('id');
            var action = "/admin/delete-feedback/" + questionId;

            var form = $('#deleteForm');
            form.attr('action', action);

            $('#deleteModal').modal('show');
        });

        $('#deleteModal').on('shown.bs.modal', function (e) {
            $('#deleteForm').submit(function (e) {
                e.preventDefault();

                $.ajax({
                    type: 'POST',
                    url: $('#deleteForm').attr('action'),
                    data: $('#deleteForm').serialize(),
                    success: function (data) {
                        $('#deleteModal').modal('hide');
                        location.reload(); // Reload the page or update as needed

                        // Optionally reset the form
                        $('#deleteForm')[0].reset();
                    },
                    error: function (error) {
                        console.error(error);
                    }
                });
            });
        });
    });
</script>



<section>
    <div class="modal" id="error-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        
        <div class="modal-content">
          <div class="modal-header" style="padding:1.3rem;">
            <h4 class="modal-title text-3" id="myModalLabel33">Success!
            </h4>
            <button type="button" class="close model-remove-icon-bg" data-dismiss="modal" aria-label="Close">
              <i class="bx bx-x modal-close-icon-1"></i>
            </button>
          </div>

          <div class="modal-body" style="padding: 1.3rem 1.3rem 0 1.3rem;">
            <div class="modal-content-border">
              <form id="formQueryResult">
              
                      
                  <div class="row">
                    <div class="col-sm-12">
                      <p class="mb-0">
                        <label for="error-message"></label>
                        <br>
                        <span id="error-message">
                          
                        </span>
                        <br>

                      </p>
                    </div>
                  </div>
                     

              </form>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
              <span class="d-none d-sm-block">close</span>
            </button>
           
           
          </div>


          
        </div>
      </div>
    </div>
   </section>

  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script> 
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <script>
    $(document).ready(function () {

let table = $('#feedbackTable').DataTable();

$('#addQuestionForm').on('submit', function (e) {
    e.preventDefault(); // Prevent the default form submission

    var newQuestion = $('#newQuestion').val();
    var newIdTraining = $('input[name="id_training"]').val();

    if (newQuestion.trim() !== '') {
        console.log('Triggered');

        $.ajax({
            type: 'POST',
            url: "{{ route('admin.addfeedbackquestion') }}",
            data: {
                '_token': $('input[name=_token]').val(),
                'newQuestion': newQuestion,
                'newIdTraining': newIdTraining
            },
            success: function (data) {
                console.log(data);
                $('#newQuestion').val('');

                location.reload(); // This will refresh the DataTable

      
            },
            error: function (error) {
                console.log(error);
            }
        });

    }
});

$('#addNewQuestionBtn').on('click', function (e) {
    e.preventDefault(); // Prevent the default button click behavior

    var newQuestion = $('#newQuestion').val();
    var newIdTraining = $('input[name="id_training"]').val();

    if (newQuestion.trim() !== '') {
        console.log('Triggered');

        $.ajax({
            type: 'POST',
            url: "{{ route('admin.addfeedbackquestion') }}",
            data: {
                '_token': $('input[name=_token]').val(),
                'newQuestion': newQuestion,
                'newIdTraining': newIdTraining
            },
            success: function (data) {
                console.log("response ", data);

                $('#newQuestion').val('');

                location.reload(); // This will refresh the DataTable



            },
            error: function (error) {
                console.log(error);
                // You can handle errors or display error messages here
            }
        });

    }
});
});

    $(document).ready(function () {
        $('.delete-question').on('click', function () {
            var questionId = $(this).data('id');
            var action = "/admin/delete-feedback/" + questionId;

            var form = $('#deleteForm');
            form.attr('action', action);

            $('#deleteModal').modal('show');
        });

        $('#deleteModal').on('shown.bs.modal', function (e) {
            $('#deleteForm').submit(function (e) {
                e.preventDefault();

                $.ajax({
                    type: 'POST',
                    url: $('#deleteForm').attr('action'),
                    data: $('#deleteForm').serialize(),
                    success: function (data) {
                        $('#deleteModal').modal('hide');
                        location.reload(); // Reload the page or update as needed

                        // Optionally reset the form
                        $('#deleteForm')[0].reset();
                    },
                    error: function (error) {
                        console.error(error);
                    }
                });
            });
        });
    });
</script>

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
    let id_training_participant = "{{ $id_t_p}}";
    let id_training = "{{ $id_training}}";
    //alert(id_training_participant);
</script>


@endsection



@section('page-scripts')

<script src="{{ asset('js/dashboard/ParticipantFeedback.js') }}"></script>



@endsection
