@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Participant Dashboard')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
{{-- add css style here --}}
<style>

.layoutdetails_half{
display: flex;
width: 100%;
padding-left: 40px;
padding-top: 20px;
padding-bottom: 20px;
flex-direction: column;
align-items: flex-start;
gap: var(--spacing-24, 24px);
border-radius: 16px;
border: 1px solid var(--gray-400, #98A2B3);
background: #FFF;
}

.font-title{
    color: #656565;

font-family: Rajdhani;
font-size: 26px;
font-style: normal;
font-weight: 600;
line-height: 32px; /* 123.077% */
}

.normal_text{
    color: #454545;

font-family: Open Sans;
font-size: 16px;
font-style: normal;
font-weight: 400;
line-height: 28px; /* 175% */

}
.spanwidth{
    width: 95px;
}

.page-title {
    color: #656565;
    font-family: Rajdhani;
    font-size: 26px;
    font-style: normal;
    font-weight: 600;
}

.normal-text{
    color: #333;
    font-family: Open Sans;
    font-size: 18px;
    font-style: normal;
    font-weight: 400;
    margin-bottom: 1%;
}

.text-content{
    color: #454545;
    font-size: 18px;
    font-family: Open Sans;
    font-style: normal;
    font-weight: 600;
    margin-bottom: 1%;
}
</style>
@endsection

@section('content')
<main style="padding:50px;margin:50px;">
    <section class="header-top-margin">
    @csrf
    <img src="{{ asset('images/receipt_image.svg') }}" style="margin-top: 3%;" title="en" alt="flag">
        <div class="page-title" style="margin-top: 2%;">
            Hi, <span class="page-title" id="participant_name"> </span>
           
          
            <div class="normal-text" style="margin-top: 2%;"><span>Thank you for your payment. Your application has been approve,
                <br>and you are now enrolled in the course. We look forward to helping you get started.</span></div>
           
            
        </div>
        
      
        <div class="row" style="margin:2px; margin-top: 2%;">
            <div class="col-sm-12">
                <div class="layoutdetails_half">
                    <span class="font-title" id="training_details"></span>
                    <div class="text-content" style="margin-top: -20px;"><span >Provider</span> : <span id="provider_name"></span></div>
                    <div class="text-content" style="margin-top: -20px;"><span>Start Date</span> : <span id="date_start"></span></div>
                    <div class="text-content" style="margin-top: -20px;"><span>End Date</span> : <span  id="date_end"></span></div>
                    <div class="text-content" style="margin-top: -20px;"><span>Venue Address</span> : <span id="venue_address"></span></div>
                    <div class="text-content" style="margin-top: -20px;"><span>Fee</span> : RM <span id="fee"></span></div>
                </div>
            </div>
        </div>
        <div style="margin:10px; margin-top: 2%;">
            {{-- <button  id="saveBtn" class="btn btn-secondary-6 btn-block">
              Submit
            </button> --}}
            @php
            $lastParameter = request()->segment(count(request()->segments()));
            @endphp 
            {{-- <a href="{{ route('participant-Payment-Invoice',$lastParameter) }}" class="btn btn-secondary-6 btn-block">Okay</a> --}}
            <a href="{{ route('participant.dashboard') }}" class="btn btn-primary">Return to Dashboard</a> 
        </div>
               
              
     

</section>
</main> 


@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
    {{-- <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>  --}}
    
@endsection

@section('page-scripts')
{{-- our own js --}}
<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>
<script src="{{ asset('js/dashboard/ParticipantTrainingPayment.js') }}"></script>

@endsection
