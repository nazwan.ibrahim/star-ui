@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Participant Dashboard')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
{{-- add css style here --}}
<style>

.layoutdetails_half{
display: flex;
width: 100%;
padding-left: 40px;
padding-top: 20px;
padding-bottom: 20px;
flex-direction: column;
align-items: flex-start;
gap: var(--spacing-24, 24px);
border-radius: 16px;
border: 1px solid var(--gray-400, #98A2B3);
background: #FFF;
}

.font-title{
    color: #656565;

font-family: Rajdhani;
font-size: 26px;
font-style: normal;
font-weight: 600;
line-height: 32px; /* 123.077% */
}

.normal_text{
    color: #454545;

font-family: Open Sans;
font-size: 16px;
font-style: normal;
font-weight: 400;
line-height: 28px; /* 175% */

}
.spanwidth{
    width: 95px;
}

.page-title {
    color: #656565;
    font-family: Rajdhani;
    font-size: 26px;
    font-style: normal;
    font-weight: 600;
}

.normal-text{
    color: #333;
    font-family: Open Sans;
    font-size: 18px;
    font-style: normal;
    font-weight: 400;
    margin-bottom: 1%;
}

.text-content{
    color: #454545;
    font-size: 18px;
    font-family: Open Sans;
    font-style: normal;
    font-weight: 600;
    margin-bottom: 1%;
}
</style>
@endsection

@section('content')
<main style="padding:50px;margin:50px;">
    <section class="header-top-margin">
    <form id="paymentForm" method="POST" action="{{ route('process_payment',[$id]) }}">   
        @csrf
        @if ($message = Session::get('success'))
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
        @else
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif
        @endif     
        <div class="row" style="margin:2px;">
            <div class="col-sm-6">
                <div class="layoutdetails_half">
                    <span class="font-title" id="participant_details"></span>
                    <div class="normal-text" style="margin-top: -20px;"><span>Name</span> : <span class="text-content" ><?php echo $detailsTrainingParticipant->participant_name; ?></span></div>
                    <div class="normal-text"  style="margin-top: -20px;"><span>Email Address</span> : <span class="text-content" ><?php echo $detailsTrainingParticipant->email; ?></span></div>
                    <div class="normal-text" style="margin-top: -20px;"><span>Phone Number</span> : <span class="text-content"><?php echo $detailsTrainingParticipant->phone_no; ?></span></div>
                    <div class="normal-text" style="margin-top: -20px;"><span>Address </span> : <span class="text-content" ><?php echo $detailsTrainingParticipant->address_1; ?>&nbsp;&nbsp;</span><span id="address_2"><?php echo $detailsTrainingParticipant->address_2; ?></span></div>
                    <div class="normal-text" style="margin-top: -20px;"><span>Country </span> : <span class="text-content" ></span><?php echo $detailsTrainingParticipant->country; ?></div>
                    <div class="normal-text" style="margin-top: -20px;"><span>Postcode</span> : <span class="text-content"><?php echo $detailsTrainingParticipant->postcode; ?></span></div>
                    <div class="normal-text" style="margin-top: -20px;"><span>State</span> : <span class="text-content" ></span><?php echo $detailsTrainingParticipant->state; ?></div>
                </div>


               
            </div>

            <div class="col-sm-6">
            <div class="layoutdetails_half">
                <span class="font-title" id="payment_summary">Payment Summary</span>
              
                <div class="normal-text"><span>Payment Total</span> : RM<span class="text-content" ><?php echo $detailsTrainingParticipant->training_fee; ?></span></div>
                <div class="normal-text"><span>Grand Total</span> : RM<span class="text-content"><?php echo $detailsTrainingParticipant->training_fee; ?></span></div>
                {{--<div><input type="text" class="form-control" id="remarks" placeholder="Please Put Remarks" name="remarks">--}}
                  
                </div>
            </div>
            <div class="d-flex justify-content-end pt-3 pb-2">
                <div style="margin:10px;">
                  <a href="{{ route('participant.dashboard') }}" class="btn btn-outline-danger">Cancel</a>
                </div>
                <div style="margin:10px;">
                {{-- <button  id="saveBtn" class="btn btn-secondary-6 btn-block">
                  Submit
                </button> --}}
                @php
                    $lastParameter = request()->segment(count(request()->segments()));
                @endphp
                <button class="btn btn-primary" id="payment_id">PAY NOW</button>

                {{--<a href="{{ route('participant-Payment-Invoice',$lastParameter) }}" class="btn btn-primary">Pay Now</a>  --}}
                {{-- <button class="btn btn-primary" id="payment_id">VIEW DETAILS</button>     --}}
            </div>
               
              </div>
            </div>
            </div>
           

        </div>
        <div class="row" style="margin:2px;">
            <div class="col-sm-6" style="margin-top: 2%;">
                <div class="layoutdetails_half">
                    <span class="font-title" id="training_details"></span>
                    <div class="normal-text" style="margin-top: -20px;"><span >Provider</span> : <span class="text-content"><?php echo $detailsTrainingParticipant->provider_name; ?></span></div>
                    <div class="normal-text" style="margin-top: -20px;"><span>Start Date</span> : <span class="text-content"><?php echo $detailsTrainingParticipant->date_start; ?></span></div>
                    <div class="normal-text" style="margin-top: -20px;"><span>End Date</span> : <span class="text-content" ><?php echo $detailsTrainingParticipant->date_end; ?></span></div>
                    <div class="normal-text" style="margin-top: -20px;"><span>Venue Address</span> : <span class="text-content" ><?php echo $detailsTrainingParticipant->venue_address; ?></span></div>
                    <div class="normal-text" style="margin-top: -20px;"><span>Fee</span> : RM <span class="text-content" ><?php echo $detailsTrainingParticipant->training_fee; ?></span></div>
                </div>
            </div>
        </div>
   </form>
              
     

</section>
</main> 


@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
    {{-- <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>  --}}
    
@endsection

@section('page-scripts')
{{-- our own js --}}    
<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";
</script>
<script src="{{ asset('js/dashboard/ParticipantTrainingPayment.js') }}"></script>
@endsection

