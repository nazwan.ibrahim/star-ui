@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Participant Dashboard')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}
@endsection

@section('page-styles')
<style>
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 20px;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    /* .dataTables_length {
        display: none;
    } */

    .dataTables_filter {
        display: flex;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    
    .dataTables_wrapper .dataTables_filter {
        float: right;
    }

    .circular-button {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        border: none;
        background-color: #3498DB;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
        border-bottom: none;
    }

    .table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
    }

    .table-row td {
        padding: 10px;
        border-bottom: none;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .filter-dropdown{
        border-radius: 9px;
        border: 1px solid var(--gray-400, #98A2B3);
        padding: 4px 8px;
        margin-right: 20px;
    }

    /* Add styles for the table */
    .table {
    width: 100%;
    border-collapse: collapse;
    margin: 0;
    }

   

    .table th, .table td {
    padding: 10px;
    text-align: center;
    border: 1px solid #ddd;
    }

    /* Add hover effect on table rows */
    .table tbody tr:hover {
    background-color: #f5f5f5;
}

</style>
@endsection

@section('content')
<main style="padding:50px;margin:50px;">
    <section class="header-top-margin">

         
        <div class="col-md-12 mx-auto">
            <div class="card mt-7" style="padding: 40px; margin-bottom: 2%; margin-top: 2%; background-color: #FFFFFF; text-align: left; border-radius: 16px; border: 1px #98A2B3 solid;">
                <h2 class="page-title py-2" style="text-align: left !important; margin-bottom: 1%;" id="training_name"><?php echo $course_name; ?></h2>
                    <div style="margin-top: 2%;">
                        <span class="contentUpdate">Provider : </span>&nbsp;<span class="contentUpdate2" id="trainingProvider"><?php echo $training_provider; ?></span>
                    </div>
                    <div style="margin-top: 2%;">
                        <span class="contentUpdate">Start Date : </span>&nbsp;<span class="contentUpdate2" id="date_start"><?php echo $training_startdate; ?></span>
                    </div>
                    <div style="margin-top: 2%;">
                        <span class="contentUpdate">End Date : </span>&nbsp;<span class="contentUpdate2" id="date_end"><?php echo $training_enddate; ?></span>
                    </div>
                    <div style="margin-top: 2%;">
                        <span class="contentUpdate">Venue : </span>&nbsp;<span class="contentUpdate2" id="venue_address"><?php echo $training_venue; ?></span>
                    </div>
            </div>
        </div>
   
        
               
                <div class="table-responsive col-md-12 mx-auto">
                    <table class="table table-striped dataTable no-footer" id="trainingDetails" style="width: 100%">  
                        <thead class="tableheader">   
                        <tr >
                            <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;" scope="col">Payment</th>
                            <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;" scope="col">Attendance</th>
                            <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;" scope="col">Feedback</th>
                            <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;" scope="col">Exam Result</th>
                            <th class="table-title" style="border-bottom: none; text-align: center; white-space: nowrap;" scope="col">Certificate</th>
                           
                        </tr>
                        </thead>
                        <tbody class="tablecontent">
                        @php $lastParameter = request()->segment(count(request()->segments()));@endphp 
                        @foreach($detailsTrainingParticipant as $detailTrainingParticipant)
                            <tr>
                           
                                <td> @if ($detailTrainingParticipant->status_payment == 0)
                                    <a id="testid" href="{{ route('participant-Payment-Details',$detailTrainingParticipant->id_training_participant) }}"><button class="btn btn-danger" id="payment_id">PAY NOW</button></a>
                                    @else
                                    <a id="testid" href="{{ route('participant-Payment-Invoice',$detailTrainingParticipant->id_training_participant) }}"><button class="btn btn-success" id="payment_id">PAID</button></a>
                                     @endif
                                </td>
                                <td>@if ($detailTrainingParticipant->status_attend == 1)
                                        <a><button class="btn btn-success" id="attend_id">COMPLETED</button></a>         

                                    @else
                                    <a><button class="btn btn-danger" id="attend_id">NOT COMPLETED</button></a>         
                                     @endif
                                </td>
                                <td>@if ($detailTrainingParticipant->status_feedback == 1)
                                    <a id="feedbackid" href="{{ route('participant-feedback',$lastParameter) }}"><button class="btn btn-success" id="payment_id">COMPLETED</button></a>         
                                    @else
                                       
                                        <a id="feedbackid" href="{{ route('participant-feedback',$lastParameter) }}"><button class="btn btn-primary" id="payment_id">ANSWER FEEDBACK</button></a>
                                     @endif
                                </td>
                                <td>@if ($detailTrainingParticipant->final_verdict_status == 'Pass')
                                        <b>PASS<b>
                                    @elseif($detailTrainingParticipant->final_verdict_status == 'Failed')
                                    <a id="testid" href="{{ route('participant-Payment-Invoice',$detailTrainingParticipant->id_training_participant) }}"><button class="btn btn-primary" id="payment_id">RE SIT</button></a>
                                    @else
                                        <b>-</b>
                                     @endif
                                </td>
                                <td>@if ($detailTrainingParticipant->status_certificate == 1)
                                    <a id="testid" href="{{ route('generateCertificate',$lastParameter)}}"><button class="btn btn-primary" id="payment_id">COA</button></a>
                                    <a id="testid" href="{{ route('generateCertificateQP',$lastParameter)}}"><button class="btn btn-success" id="payment_id">CQP</button></a>

                                    @else                                        
                                    <b>-</b>

                                     @endif
                                </td> 
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

               
        </div>

</section>
</main> 


@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
    {{-- <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>  --}}
    
@endsection

@section('page-scripts')
{{-- our own js
<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";

</script>
<script src="{{ asset('js/dashboard/ParticipantTraining.js') }}"></script>

--}}

@endsection
