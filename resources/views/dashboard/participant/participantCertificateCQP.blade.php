<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Certificate of Qualified Person</title>
    <style>
      body {
        font-family: Arial, sans-serif;
      }

      .container {
 width: 100%;
 height: 500px;
 margin: 0 auto;
 padding: 20px;
 box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
}

      .logo {
        max-width: 100px;
        margin-bottom: 20px;
      }

      .certificate-title {
        font-family: 'Brush Script', cursive, sans-serif;
        font-size: 24px;
        font-weight: bold;
        text-align: center;
        margin-bottom: 20px;
      }

      .participant-details {
        margin-bottom: 20px;
                text-align: center;

      }

      .training-details {
        margin-bottom: 20px;
                text-align: center;

      }

      .signature {
            margin-top: 200px; /* Increase the margin to move it lower */
            text-align: center;
        }
        
    </style>
  </head>
  <body>
    <div class="container">
   <img src="{{ asset('images/logo_seda.png') }}" width="20%" height="2.5%" style="padding: 0.05%; margin-right: 70px;">
      <h2 class="certificate-title" style="font-weight: bold;font-family: Rajdhani;font-size: 40px;">Certificate of Qualified Person</h2>
      <p class="participant-details">Certificate No:{{ $data->certificate_no}}</p>
      <p class="participant-details">This is to certify that</p>
      <p class="participant-details" style="font-weight: bold; margin-bottom: 1px;">{{ $data->fullname }}</p>
      <p class="participant-details">I / C No .: :{{ $data->ic_no }}</p>
      <p class="participant-details">has sucessfully completed the training program requirements for</p> 
      <p class="training-details" style="font-weight: bold;">{{ $data->course_name}}</p>
      <p class="training-details" style="font-weight: bold;">at {{ $data->venue }}</p>
      <p class="training-details" style="font-weight: bold;">on {{ $data->date_start }} until {{ $data->date_end }} session</p>

      <p class="signature">(Dato ' Hamzah Bin Hussin)<br>Chief Executive Officer<br>Sustainable Energy Development Authority Malaysia</p>
    </div>
  </body>
</html>