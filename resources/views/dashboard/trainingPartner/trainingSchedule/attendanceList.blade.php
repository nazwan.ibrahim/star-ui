@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Trainer-Attendance list')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.font-title{
color: var(--gray-950, #0C111D);
width: 300px;
font-family: Inter;
font-size: 24px;
font-style: normal;
font-weight: 600;
line-height: 32px; 
}

.icon-editIcon:before {
  content: "\e916";
}


</style>
@endsection

@section('content')
<main style="padding:50px;margin:50px;text-align: left">
  @php
  $lastParameter = request()->segment(count(request()->segments()));
@endphp
  <div class="container">
    <ol class="breadcrumb p-0 mb-0 poppins-semibold-12 light-grey">
              <li class="nav-item mg-sm ">
                  <a class="nav-link" href="{{ route('TP-training-schedule-history') }}">Dashboard</a>
                      <span class="sr-only">(current)</span>
                  </a>
              </li>   
              <li class="nav-item mg-sm ">
                  <a class="nav-link" href="{{ route('TP-Attendance-List',$lastParameter) }}">Attendance List</a>
                      <span class="sr-only">(current)</span>
                  </a>
              </li>          
              <li class="nav-item mg-sm ">
                  <a class="nav-link" href="{{ route('TP-UpdateExamResult-List', $lastParameter) }}">Update Exam Result
                      <span class="sr-only">(current)</span>
                  </a>
              </li>
              <li class="nav-item mg-sm ">
                  <a class="nav-link" href="{{ route('TP-TrainingReport',$lastParameter) }}">Training Report
                      <span class="sr-only">(current)</span>
                  </a>
              </li>
    </ol>
  </div>

    <section class="header-top-margin">
     

      {{-- <div class="row">
        <div class="col-12">
          <ol class="breadcrumb p-0 mb-0 poppins-semibold-12">
            <li class="breadcrumb-item">
                <a class="text-breadcrumb-2" href="{{ route('TP-training-schedule-history') }}">Dashboard</a>            
            </li>
            <li class="breadcrumb-item">
              <a class="text-breadcrumb-2" href="{{ route('TP-Attendance-List') }}">Attendance List</a>
            </li>
            
          </ol>
        </div>
      </div> --}}
      
        
  
  
    <div class="row">
        <label class=" font-title" for="">Training Partner</label>
    </div>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>List of Attendance</h2>
            </div>
            
         </div>
    </div>
   
    
    <div class="table-responsive">

        <table class="table table-width-style dataTable no-footer" id="TPattendanceList" style="width: 100%">

            <thead>
                <tr >
                    <th class="header-1">No.</th>
                    <th class="header-1">Participant</th>
                    <th class="header-1">Attend Check</th>
                    <th class="header-1">Attend date</th>
                    <th class="header-1">Remark</th>
                    
                </tr>
            </thead>

            <tbody>



            </tbody>

        </table>
   
    </section>
</main>
  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script> 
    
@endsection



@section('page-scripts')

{{-- our own js --}}

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";

</script>


<script src="{{ asset('js/dashboard/TPattendanceList.js') }}"></script>

@endsection
