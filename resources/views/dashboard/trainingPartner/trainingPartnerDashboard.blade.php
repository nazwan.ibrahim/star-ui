@extends('layouts.contentLayoutMain')
{{-- page Title --}}
@section('title','Dashboard')
{{-- vendor css --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@endsection

@section('page-styles')
<style>
    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .doughnut-chart{
        height: 25%;
        width: 25%;
    }

    .chart-title{
        color: #454545;
        font-size: 24px;
        font-family: Open Sans;
        font-weight: 600;
        line-height: 32px;
    }

    .carousel-container {
        display: flex;
        flex-direction: row !important;
        align-items: center;
        justify-content: center;
        margin-top: 3%;
    }

    .carousel-button{
        width: 65px;
        height: 65px;
        background: #3498DB;
        border-radius: 8px;
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .training-cards{
        width: 364px;
        height: 460px;
        border-radius: 8px;
    }

    .card-title{
        color: #152536;
        font-size: 20px;
        font-family: Open Sans;
        font-weight: 600;
    }

    .card-location{
        color: #6C757D;
        font-size: 14px;
        font-family: Open Sans;
        font-weight: 400;
    }

    .unconfirmed-details{
        border-radius: 8px;
        border: 1px solid var(--success-500, #F79009);
        background: var(--success-25, #FFFCF5);
        padding: 4px 25px;
        color: #F79009;
    }

    .contact-info-sets {
        display: flex;
        flex-direction: column;
        text-align: start;
        margin-bottom: 10%;
        margin-right: 50%;
        white-space: nowrap;
    }

    .contact-info-name{
        color: #454545;
        font-size: 20px;
        font-family: Open Sans;
        font-weight: 600;
        line-height: 30px;
    }

    .button-text{
        color: #F9F6FE;
        font-size: 16px;
        font-family: Rajdhani;
        font-weight: 700;
        text-transform: uppercase;
        line-height: 24px;
    }

    .logo-card{
        color: #FFFFFF;
    }

    .tr{
        background-color: white !important;
    }

    .text-center {
        text-align: center !important;
    }
</style>
@endsection

@section('content')
<main>
    <div class="py-4">
        <img src="{{ asset('images/temp-banner.png') }}" style="width: 100%; height: 60%;">
    </div>

    <div class="col-md-12">
        <div class="text-center mb-3" style="margin-left: 3%; margin-top: 3%;">
            <h2 class="page-title py-2" style="text-align: left !important;">Training Calendar</h2>
        </div>
    </div>

    <!-- <div class="col-md-12">
        <div class="text-center mb-4" style="margin-left: 3%; margin-top: 3%;">
            <h2 class="page-title py-2" style="text-align: left !important;">Upcoming Training</h2>
        </div>
    </div> -->

    <div class="col-md-10 py-4 mx-auto" style="margin-bottom: 3%;">
        <div id="TPList_wrapper" class="dataTables_wrapper no-footer">
            <table class="table table-bordered table-striped dataTable no-footer" id="TPList" style="width: 100%;">
                <thead class="table-header">
                    <tr>
                        <th class="table-title text-center" width="40">No.</th>
                        <th class="table-title text-center">Programme</th>
                        <th class="table-title text-center">Start Date</th>
                        <th class="table-title text-center">End Date</th>
                        <th class="table-title text-center">Venue</th>
                        <th class="table-title text-center">Fee (RM)</th>
                        <th class="table-title text-center">Status</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-12">
        <div class="text-center mb-3" style="margin-left: 3%;">
            <h2 class="page-title py-2" style="text-align: left !important;">Cumulative Training Chart</h2>
        </div>
    </div>

    <!-- Dynamically generated doughnut charts -->
    <div class="col-md-10 py-4" style="display: flex; flex-direction: row; margin-bottom: 5%; margin-left: 5%;">
        <div class="doughnut-chart col-md-3 mx-auto">
            <label class="chart-title">Renewable Energy</label>
            <canvas id="doughnut-chart-1"></canvas>
        </div>
        <div class="doughnut-chart col-md-3 mx-auto">
            <label class="chart-title">Energy Efficiency</label>
            <canvas id="doughnut-chart-2"></canvas>
        </div>
        <div class="doughnut-chart col-md-3 mx-auto">
            <label class="chart-title">Sustainable Energy</label>
            <canvas id="doughnut-chart-3"></canvas>
        </div>
    </div>

    <div class="col-md-12">
        <div class="text-center mb-3" style="margin-left: 3%;">
            <h2 class="page-title py-2" style="text-align: left !important;">Compentency Training Stats</h2>
        </div>
    </div>

    <div class="col-md-11 py-4" style="display: flex; flex-direction: row; margin-bottom: 5%; margin-left: 5%;">
        <div class="col-md-10 mx-auto">
            <canvas id="competency-chart"></canvas>
        </div>
    </div>

    <div class="col-md-11 py-4" style="display: flex; flex-direction: row; margin-bottom: 5%; margin-left: 5%;">
        <div class="col-md-10 mx-auto">
            <table id="dataTable" class="display"></table>
        </div>
    </div>

    <div class="col-md-12">
        <div class="text-center mb-3" style="margin-left: 3%;">
            <h2 class="page-title py-2" style="text-align: left !important;">Upcoming Training</h2>
        </div>
    </div>

    <!-- Add the new carousel section -->
    <div class="col-md-12 carousel-container">
        <div class="col-md-1">
            <div class="carousel-button" id="carousel-button-left">
                <i class="fas fa-chevron-left" style="color: #FFFFFF;"></i>
            </div>
        </div>
        
        <!-- Dynamically generated training cards will be added here -->
        <div class="col-md-9 row" id="training-cards-container">
            <!-- Training cards will be dynamically added here -->
        </div>

        <div class="col-md-1">
            <div class="carousel-button" id="carousel-button-right">
                <i class="fas fa-chevron-right" style="color: #FFFFFF;"></i>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="margin-top: 8%; display: flex; flex-direction: row; items-center: center; justify-content: center;">
        <div class="col-md-6">
            <div class="text-center mb-3" style="margin-left: 3%; margin-top: 3%;">
                <h2 class="page-title py-2" style="text-align: left !important; margin-bottom: 5%;">SEDA & Training Partners</h2>
                <img src="{{ asset('images/map_image.png') }}" style="width: 80%; height: 80%;">
            </div>
        </div>
        <div class="col-md-6">
            <div class="text-center mb-3" style="margin-left: 3%; margin-top: 3%;">
                <h2 class="page-title py-2" style="text-align: left !important; margin-bottom: 5%;">Need Support?</h2>
                <button class="button-text btn btn-primary" style="width: 90%;">
                    Contact Us
                    <i class="fas fa-phone mx-2"></i>
                </button>
            </div>
            <div class="col-md-6" style="margin-left: 2%; margin-top: 8%;">
                <h2 class="page-title py-2" style="text-align: left !important; margin-bottom: 5%; margin-top: 5%;">Contact</h2>
                <div style="display: flex; flex-direction: row;">
                    <div class="contact-info-sets">
                        <label class="contact-info-name">General</label>
                        <label class="contact-info-name" style="font-weight: 400 !important; font-size: 16px !important;">03-8870-5800</label>
                        <label class="contact-info-name" style="font-size: 18px !important; color: #3498DB;">training@seda.gov.my</label>
                    </div>
                    <div class="contact-info-sets">
                        <label class="contact-info-name">Pn. Sazlinda Ayu Binti Hj. Arshad</label>
                        <label class="contact-info-name" style="font-weight: 400 !important; font-size: 16px !important;">03-8870-5851</label>
                        <label class="contact-info-name" style="font-size: 18px !important; color: #3498DB;">training@seda.gov.my</label>
                    </div>
                </div>
                <div style="display: flex; flex-direction: row;">
                    <div class="contact-info-sets">
                        <label class="contact-info-name">En. Muhammad Syukri Ab Rahman</label>
                        <label class="contact-info-name" style="font-weight: 400 !important; font-size: 16px !important;">03-8870-5926</label>
                        <label class="contact-info-name" style="font-size: 18px !important; color: #3498DB;">training@seda.gov.my</label>
                    </div>
                </div>
            </div>
            <div style="display: flex; flex-direction: row;">
                <div class="mb-3" style="margin-left: 3%; margin-top: 3%; text-align: start; flex: 1;">
                    <h2 class="page-title py-2" style="text-align: left !important; margin-bottom: 2%;">Working Hours</h2>
                    <label class="card-location" style="color: #454545; font-size: 16px; font-weight: 400;">8:30 AM - 5:30PM (Monday - Friday)</label> 
                </div>
                <div class="mb-3" style="margin-left: 3%; margin-top: 3%; text-align: start; width: 60%;">
                    <h2 class="page-title py-2" style="text-align: left !important; margin-bottom: 2%;">Address</h2>
                    <label class="card-location" style="color: #454545; font-size: 16px; font-weight: 400;">
                        SUSTAINABLE ENERGY DEVELOPMENT AUTHORITY (SEDA) MALAYSIA Galeria PjH, Aras 9, Jalan P4W, Persiaran Perdana, Presint 4, 62100 Putrajaya, Malaysia.
                    </label> 
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 8%; display: flex; flex-direction: row; items-center: center; justify-content: center;">
        <div class="col-md-3 logo-card">
            <img src="{{ asset('images/nrecc_logo.png') }}">
        </div>
        <div class="col-md-3 logo-card">
            <img src="{{ asset('images/tnb_logo.png') }}">
        </div>
        <div class="col-md-3 logo-card">
            <img src="{{ asset('images/st_logo.png') }}">
        </div>
        <div class="col-md-3 logo-card">
            <img src="{{ asset('images/mida_logo.svg') }}">
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 7%;">
    </div>
</main>
@endsection

@section('vendor-scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment@1.1.2"></script>
    <script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="{{ asset('js/dashboard/TPCharts.js') }}"></script>
@endsection

@section('page-scripts')
{{-- our own js --}}
<script>
    let host = "{{ env('API_SERVER') }}";
</script>

<!-- Add the new carousel script -->
<script>
        /* document.addEventListener("DOMContentLoaded", function () {
            const trainingCardsData = ;

            const createTrainingCardElement = (training) => {
                return `
                    <div class="training-cards" style="border: 1px solid;">
                        <!-- Your existing card content -->
                        <img src="{{ asset('images/card_image.png') }}" style="width: 100%; height: 60%;">
                        <label class="card-title" style="margin-bottom: 2%; margin-top: 2%;">${training.course_name}</label>
                        <div class="mx-auto" style="display: flex; flex-direction: row; margin-bottom: 2%;">
                            <i class="fas fa-map-marker mx-2" style="color: #3498DB;"></i>
                            <label class="card-location">${training.training_venue}</label>
                        </div>
                        <div style="justify-content: start; text-align: start; margin: 5%; margin-bottom: 10%;">
                            <label class="unconfirmed-details">
                                <i class="fas fa-calendar-alt"> ${training.date_start}</i>
                            </label>
                            <label class="unconfirmed-details">
                                <i class="fas fa-yen-sign"> ${training.training_fee !== null ? training.training_fee : 'To be confirmed'}</i>
                            </label>
                        </div>
                    </div>`;
            }; */

           /*  const renderTrainingCards = (startIndex) => {
                const endIndex = startIndex + 3;
                const container = document.getElementById('training-cards-container');
                container.innerHTML = '';

                for (let i = startIndex; i < endIndex && i < trainingCardsData.length; i++) {
                    const cardElement = createTrainingCardElement(trainingCardsData[i]);
                    container.innerHTML += cardElement;
                }
            }; */

            /* renderTrainingCards(0);

            let currentIndex = 0;
            const totalCards = trainingCardsData.length;

            document.getElementById('carousel-button-left').addEventListener('click', function () {
                currentIndex = (currentIndex - 1 + totalCards) % totalCards;
                renderTrainingCards(currentIndex);
            });

            document.getElementById('carousel-button-right').addEventListener('click', function () {
                currentIndex = (currentIndex + 1) % totalCards;
                renderTrainingCards(currentIndex);
            });
        }); */
    </script>
@endsection
