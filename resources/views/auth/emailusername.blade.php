
@extends('layouts.contentLayoutLogin')
{{-- page Title --}}
@section('title','User Name')
{{-- vendor css --}}
@section('page-styles')
<style>

a,
a:hover {
    color: #2c2c2c;
    text-decoration: none;
}

a:hover {
    color: #5b5c5c;
    text-decoration: none;
}
.noborderlogin{
    display: block;
    width: 500px;
    margin-top:160px;
    flex-direction: column;
    align-items: center;
    gap: var(--spacing-32, 32px);
    
    background: #fff; 
}

.borderlogin{
    display: block;
    width: 500px;
    margin-top:70px;
    margin-bottom:20px;
    flex-direction: column;
    align-items: center;
    gap: var(--spacing-32, 32px);
    border-radius: 17px;
    border: 1px solid var(--primary-500-base-color, #4B94D8);
    background: #fff; 
}

.title_header{
    color: #656565;

    text-align: center;
    font-family: Rajdhani;
    font-size: 32px;
    font-style: normal;
    font-weight: 700;
    line-height: 38px; /* 118.75% */
}
    .title{
        color: #656565;

    text-align: center;
    font-family: Rajdhani;
    font-size: 32px;
    font-style: normal;
    font-weight: 700;
    line-height: 38px; /* 118.75% */
    }
</style>

@endsection


@section('content')

<main class="form-signin" style="over-flow:auto; max-width: 600px !important;">
        
    <div class="noborderlogin"><p class="mt-3 mb-3 fw-bold title_header">SEDA Training and Registration System (STAR)</p></div>
            
        <label for="floatingInput" style="font-weight: bold; font-size: larger;">
            
            Please contact <a href="tel:07333000333">07-333000333</a> for assistance.
            
        </label>



        <hr>
            <p class="small fw-bold mt-2">
                <a class="" href="{{ route('home') }}">
                    <button class="w-100 btn btn-primary text-white fw-bold mt-2">Return to Home</button>
               </a>
            </p>
    </div>
</main>


   
    @endsection

    @section('vendor-scripts')

   
    
    @endsection

    
    @section('page-scripts')
   

    {{-- <script type="text/javascript">
        $(document).ready(function() {
            $("#btnSend").click(function(e) {
                $('#page_loader').fadeIn();
                $(".loader-text").text("Pengesahan pengguna..");
                e.preventDefault();
    
                $.ajax({
                    url: "https://mygovevent.mampu.gov.my/ms/auth/valid-account",
                    type: 'POST',
                    data: $('#form_forgot_pass').serialize(),
                    dataType: 'json',
                    success: function(data) {
                        if (data.success) {
                            $('#page_loader').fadeOut();
                            
                            Swal.fire({
                                icon: 'success',
                                title: 'Berjaya',
                                text: data.message,
                                showClass: { popup: 'animated fadeInDown faster' },
                                hideClass: { popup: 'animated fadeOutUp faster' },
                                allowOutsideClick: false,
                                footer: 'MyGovEvent'
                            }).then((result) => {
                                if(result.value) {
                                    window.location = "https://mygovevent.mampu.gov.my/ms/auth/login/identifier";
                                }
                            })
    
                        } else {
                            $('#page_loader').fadeOut();
    
                            Swal.fire({
                                icon: 'error',
                                title: 'Ralat',
                                text: data.message,
                                showClass: { popup: 'animated fadeInDown faster' },
                                hideClass: { popup: 'animated fadeOutUp faster' },
                                allowOutsideClick: false,
                                footer: 'MyGovEvent'
                            });
    
                            removeError();
                        }
                    },
                    error: function(xhr) {
                        $('#page_loader').fadeOut();
                        if (xhr.status === 422) inputError(xhr);
                    }
                });
            });
        });
    </script> --}}
    
    <script>
    
      let host = "{{env('API_SERVER')}}";
   
    $(document).ready(function() {
    $('#btnSubmit').on('click', function() {
        resetPassword();
        //console.log('here');
      //  console.log('here');
     //  alert('k')
       // 
        // $("#confirmationModal").modal("hide");
        // $("#loadingModal").modal("show");
    });
    });
    // $('#btnSubmit').on('click', function() {
    //     $('#confirmationModal').modal('show');
    // });
    
      
    </script>

    <script src="{{ asset('js/login/resetPassword.js') }}"></script>
    @endsection


  


