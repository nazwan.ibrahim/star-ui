<!DOCTYPE html>
<html>
<head>
    <title>FORGOT PASSWORD STAR</title>
</head>
<body>
    <p>Hi,</p>
    <p>You can reset your password using the link below:</p>
    <a href="{{ route('reset-password-get', ['token' => $token, 'encoded_email' => urlencode($email)]) }}">Reset Password</a>

    <p>Please note that the link will expire after 5 minutes.</p>
    
    <p>If you encounter any issues or the link has expired, please request a new password reset <a href="{{ route('forgot-password') }}">here</a>.</p>

    <p>Thank you.</p>
</body>
</html>
