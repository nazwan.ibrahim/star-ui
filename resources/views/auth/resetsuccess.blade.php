
@extends('layouts.contentLayoutLogin')
{{-- page Title --}}
@section('title','Reset Success')
{{-- vendor css --}}
<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
@section('page-styles')
<style>

a,
a:hover {
    color: #2c2c2c;
    text-decoration: none;
}

a:hover {
    color: #5b5c5c;
    text-decoration: none;
}

.page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

    .header-top-margin {
        margin-top: 75px;
        padding-bottom: 55px;
        margin-bottom: 55px;
    }

    .contentUpdate{
        color: #454545;
        font-family: Open Sans;
        font-size: 18px;
        font-style: normal;
        font-weight: 600;
        line-height: 28px; /* 155.556% */
        text-align: left;
    }
</style>

@endsection


@section('content')

    <main class="form-signin header-top-margin">
        {{-- <form id="form_forgot_pass" action="{{ route('reset-password') }}" method="post" class="user-form"> --}}
            <input type="hidden" name="_token" value="U2S8yLDy3Flbc1MNhLCtcuCZfpmu77aRePah783F">            
            
            <!-- <a href="#"><img src="{{ asset('images/Big_title.png') }}"  alt="STAR" width="100%" height="100%"></a> -->

            <h2 class="mt-3 mb-3 page-title">Your Password Has Been Reset!</h2>

           
            <label class="contentUpdate" for="floatingInput" style="margin-bottom: 5%; margin-top: 5%;">Sign back in with your new password.</label>
            
            <div>
                <hr>
                <p class="small fw-bold mt-2">
                    <a class="" href="{{ route('login') }}">
                        <button class="w-50 btn btn-primary text-white fw-bold mt-2">Return to Log In</button>
                   </a>
                </p>
                <br/>
               
            </div>
        {{-- </form> --}}
    </main>


   
    @endsection

    @section('vendor-scripts')

   
    
    @endsection

    
    @section('page-scripts')
   
    
    <script>
    
      let host = "{{env('API_SERVER')}}";
   
    $(document).ready(function() {
    $('#btnSubmit').on('click', function() {
        resetPassword();
        //console.log('here');
      //  console.log('here');
     //  alert('k')
       // 
        // $("#confirmationModal").modal("hide");
        // $("#loadingModal").modal("show");
    });
    });
    // $('#btnSubmit').on('click', function() {
    //     $('#confirmationModal').modal('show');
    // });
    
      
    </script>

    <script src="{{ asset('js/login/resetPassword.js') }}"></script>
    @endsection


  


