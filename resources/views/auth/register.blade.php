
<!DOCTYPE html>
<html lang="ms">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="PjwUFBULkEBFHDwVncKTZ6k8oM55Zg8PAAtNUPjD">

    <meta name="description" content="MyGovEvent">
    <meta name="author" content="Unit Sistem Dalaman | Seksyen Pembangunan Aplikasi Bersepadu | BPA | MAMPU">

    <title>STAR</title>
    
    <link rel="icon" href="#">

    <!-- Styles -->
    <link href="https://mygovevent.mampu.gov.my/css/app.css?id=4991aee89dd576353254afbfc6a22fe1" rel="stylesheet">

   
    <style>
        html,
        body {
            height: 100%;
        }

        body {
            display: flex;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f8f8f8;
        }
        a,
        a:hover {
            color: #2c2c2c;
            text-decoration: none;
        }

        a:hover {
            color: #5b5c5c;
            text-decoration: none;
        }
    </style>

</head>

<body class="text-center">

    <div class="loading-overlay" id="page_loader">
        <div class="loader"></div>
        <div class="loader-text"></div>
    </div>

    <main class="form-signin">
        <form id="form_login">
            <input type="hidden" name="_token" value="PjwUFBULkEBFHDwVncKTZ6k8oM55Zg8PAAtNUPjD">            <input type="hidden" id="event_id" value="">
            
            <a href="#"><img src="{{ asset('images/Big_title.png') }}"  alt="STAR" width="100%" height="100%"></a>
            <div class="col-lg-8" style="margin: auto">
           
            <h4 class="mt-3 mb-3 fw-bold card-title">Pendaftaran Pengguna</h4>
            
                
                
                
                
            </div>

            <div class="form-floating">
                <input type="text" class="form-control" id="user_id" name="user_id"  placeholder="MyKad atau E-mel">
                <label for="floatingInput">Nama: </label>
            </div>
            
            <div class="form-floating">
                <input type="text" class="form-control" id="user_id" name="user_id"  placeholder="MyKad atau E-mel">
                <label for="floatingInput">Emel:</label>
            </div>

            <div class="form-floating">
                <input type="text" class="form-control" id="user_id" name="user_id"  placeholder="MyKad atau E-mel">
                <label for="floatingInput">MyKad:</label>
            </div>

            <div class="form-floating">
                <input type="text" class="form-control" id="user_id" name="user_id"  placeholder="MyKad atau E-mel">
                <label for="floatingInput">Alamat: </label>
            </div>

            <div class="form-floating">
                <input type="text" class="form-control" id="user_id" name="user_id"  placeholder="MyKad atau E-mel">
                <label for="floatingInput">Nama Training</label>
            </div>

            <div class="form-group">
                <label for="captcha" class="col-lg-12 col-form-label">CAPTCHA <span class="star">*</span></label>
                <div class="col-md-4 col-lg-4 captcha">
                    <span><img src="#" ></span>
                    <button type="button" class="btn btn-light" class="reload" id="btnReload">
                        <i class="fas fa-sync-alt fa-2x text-primary" title="Set Semula"></i>
                    </button>
                </div>
            </div>
           
            <button class=" w-75 fw-bold btn btn-primary btn-round text-white" id="btnRegister">Daftar</button>

            <div>
                <hr>
                <p class="mt-2">
                    <div class="small fw-bold float-start">
                        <a href="{{ route('auth-forgot-password') }}">Lupa Kata Laluan? </a> 
                    </div>    
                    <div class="small fw-bold float-start">
                        <a class="float-start" href="{{ route('auth-login') }}">Log Masuk</a>   
                    </div>    
                              

                    
                    <div class="float-end mt-n1">
                                                    <a href="#">
                                <img src="https://mygovevent.mampu.gov.my/images/en.png" class="nav-flag ms-1" title="en" alt="flag">
                            </a>
                                                    <a href="#">
                                <img src="https://mygovevent.mampu.gov.my/images/ms.png" class="nav-flag ms-1" title="ms" alt="flag">
                            </a>
                                            </div>
                </p>
                <br/>
                <div class="mt-3" style="border: 2px dashed #2c2c2c; border-radius: 5px; padding: 5px;">
                    Pengguna Baharu? Sila klik untuk <a href="{{ route('auth-register') }}" class="fw-bold">Daftar</a>
                    
                </div>
                <p class="m-0 text-center mt-5">Hakcipta &copy; Star 2023</p>
                <p class="small text-center mt-2"><a href="#">Penafian</a> | <a href="#">Privasi Pengguna</a></p>
            </div>
        </form>
    </main>

    <script src="/js/app.js?id=319770293445a627f795c156d2a96506"></script>

    
    
    <script type="text/javascript">
    function btnSubmit(){var e=$("#event_id").val();$.ajax({url:"https://mygovevent.mampu.gov.my/ms/auth/identify-user",type:"POST",data:$("#form_login").serialize(),dataType:"json",success:function(t){t.success?!0===t.result.route&&(window.location=e?"https://mygovevent.mampu.gov.my/ms/auth/user-login/challenge":"https://mygovevent.mampu.gov.my/ms/auth/login/challenge"):($("#page_loader").fadeOut(),Swal.fire({icon:"error",title:"Ralat",html:t.message,showClass:{popup:"animated fadeInDown faster"},hideClass:{popup:"animated fadeOutUp faster"},allowOutsideClick:!1,footer:"MyGovEvent"}),removeError())},error:function(e,t){$("#page_loader").fadeOut(),422===e.status&&inputError(e),419===e.status&&Swal.fire({icon:"error",title:"Ralat",html:"Token pengesahan anda telah tamat. Sila muat semula halaman ini.",showClass:{popup:"animated fadeInDown faster"},hideClass:{popup:"animated fadeOutUp faster"},allowOutsideClick:!1,footer:"MyGovEvent"})}})}function hasClass(e,t){return e.className.match(new RegExp("(\\s|^)"+t+"(\\s|$)"))}function addClass(e,t){hasClass(e,t)||(e.className+=" "+t)}function removeClass(e,t){if(hasClass(e,t)){var a=new RegExp("(\\s|^)"+t+"(\\s|$)");e.className=e.className.replace(a," ").trim().replace(/\s{2,}/g," ")}}function changeClass(e,t){hasClass(e,t)||addClass(e,t)}document.onreadystatechange=function(){var e=document.readyState;"interactive"==e?(changeClass(document.getElementById("btnNext"),"btn-dark"),$("#btnNext").text("Sila tunggu.."),$("#btnNext").attr("disabled",!0)):"complete"==e&&setTimeout(function(){document.getElementById("interactive"),$("#btnNext").attr("disabled",!1),$("#btnNext").text("Seterusnya"),removeClass(document.getElementById("btnNext"),"btn-dark"),changeClass(document.getElementById("btnNext"),"btn-primary")},3e3)},$(function(){"use strict";$("#btnNext").click(function(e){e.preventDefault(),$(this).attr("disabled",!0),$(this).text("Pengesahan pengguna.."),setTimeout(()=>{$(this).attr("disabled",!1),$(this).text("Seterusnya"),btnSubmit()},2e3)})});
    </script>  
    <script src="https://mygovevent.mampu.gov.my/js/myevent/err_handler.min.js"></script>

</body>

</html>
