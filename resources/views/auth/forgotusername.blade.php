
@extends('layouts.contentLayoutMain')
{{-- page Title --}}
@section('title','Login')
{{-- vendor css --}}
@section('page-styles')
<style>

a,
a:hover {
    color: #2c2c2c;
    text-decoration: none;
}

a:hover {
    color: #5b5c5c;
    text-decoration: none;
}
</style>

@endsection


@section('content')

    <main class="form-signin">
        {{-- <form id="form_forgot_pass"> --}}
            <input type="hidden" name="_token" value="U2S8yLDy3Flbc1MNhLCtcuCZfpmu77aRePah783F">            <a href="#"><img src="{{ asset('images/Big_title.png') }}"  alt="STAR" width="100%" height="100%"></a>

            <p class="mt-3 mb-3 fw-bold">Forgot Username</p>
            Enter your email to receive username.

            <div class="form-floating">
                <input type="text" class="form-control" id="user_id" name="user_id" placeholder="E-mail">
                <label for="floatingInput">Email</label>
            </div>
            <button class="w-50 btn btn-primary text-white fw-bold mt-2" id="btnSend">Submit</button>

            <div>
                <hr>
                <p class="small fw-bold mt-2">
                    <a class="float-start" href="{{ route('login') }}">Log in</a>
    
                </p>
                <br/>
                <p class="m-0 text-center mt-5">All rights reserved&copy; Star 2023</p>
                <p class="small text-center mt-2">Sustainable Energy Development Authority Malaysia <br>(SEDA Malaysia)</a></p>
            </div>
        {{-- </form> --}}
    </main>

      <!-- Confirmation Modal -->
     
    @endsection

    @section('vendor-scripts')
    <script src="/js/app.js?id=319770293445a627f795c156d2a96506"></script>
    <script src="{{asset('vendors/js/extensions/jquery.rateyo.min.js')}}"></script>
   
    @endsection
    @section('page-scripts')
    <script src="/js/app.js?id=319770293445a627f795c156d2a96506"></script>

    <script type="text/javascript">
      
      let host = "{{env('API_SERVER')}}";
   
    $(document).ready(function() {
    $('#btnSend').on('click', function() {
       // resetPassword();
        window.location.href = `${host}/auth/username-success`;
        });
    });
    </script>
   
    
    @endsection


  


