
@extends('layouts.contentLayoutLogin')
{{-- page Title --}}
@section('title','Forgot Password')
{{-- vendor css --}}
<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Rajdhani' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@section('page-styles')
<style>

a,
a:hover {
    color: #2c2c2c;
    text-decoration: none;
}

a:hover {
    color: #5b5c5c;
    text-decoration: none;
}

.noborderlogin{
    display: block;
    width: 500px;
    margin-top:100px;
    flex-direction: column;
    align-items: center;
    gap: var(--spacing-32, 32px);
    background: #fff; 
}

.borderlogin{
    display: block;
    width: 500px;
    margin-top:30px;
    margin-bottom:20px;
    flex-direction: column;
    align-items: center;
    gap: var(--spacing-32, 32px);
    border-radius: 17px;
    border: 1px solid var(--primary-500-base-color, #4B94D8);
    background: #fff; 
}

.title_header{
    color: #656565;

    text-align: center;
    font-family: Rajdhani;
    font-size: 32px;
    font-style: normal;
    font-weight: 700;
    line-height: 38px; /* 118.75% */
}
    .title{
        color: #656565;

    text-align: center;
    font-family: Rajdhani;
    font-size: 32px;
    font-style: normal;
    font-weight: 700;
    line-height: 38px; /* 118.75% */
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 28px;
        font-style: normal;
        font-weight: 600;
    }

    .contentUpdate{
        color: #454545;
        font-family: Open Sans;
        font-size: 22px;
        font-style: normal;
        font-weight: 600;
        line-height: 28px; /* 155.556% */
        text-align: center;
    }
</style>

@endsection


@section('content')

<main class="form-signin" style="over-flow:auto; max-width: 600px !important;">
    <div class="noborderlogin"><p class="mt-3 mb-3 fw-bold page-title">SEDA Training and Registration System (STAR)</p></div>
        <div class=" borderlogin">
            <div style="margin:30px;">
                <form id="form_forgot_pass" action="{{ route('reset-password-post') }}" method="post" class="user-form">
                @csrf
                @if ($message = Session::get('success'))
                      <div class="alert alert-success">
                          <p>{{ $message }}</p>
                      </div>
                    @else
                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                    @endif 
                        {{-- <a href="#"><img src="{{ asset('images/Big_title.png') }}"  alt="STAR" width="100%" height="100%"></a> --}}

                        <p class="mt-3 mb-3 fw-bold contentUpdate">Reset Password</p>
                            <input type="hidden" name="token" value="{{ $token }}">
                            <input type="hidden" class="form-control" style="margin-bottom: 2%;" id="email" name="email" placeholder="Your Email" value="{{ $email }}" readonly>
                            
                            
                            <span id="password-error" class="text-danger"></span>
                                    @if($errors->any('new_password'))
                                    <p>{{$errors->first('new_password')}}</p>
                                    @endif
                            <div class="">
                                <label for="newpassword" style="margin-bottom: 2%;">New Password</label>
                                <input type="password" name="newpassword" class="form-control" style="margin-bottom: 2%;" id="newpassword" oninput="validatePassword()" placeholder="New Password" required>
                            </div>

                            <div class="">
                                <label for="confirmpassword" style="margin-bottom: 2%;">Confirm Password</label>
                                <input type="password" class="form-control" style="margin-bottom: 2%;" id="confirmpassword" name="confirmpassword" oninput="validatePassword()" placeholder="Confirm Password">
                            </div>

                                <div class="small float-start font-size: 12px;">
                                    <label>
                                        <input type="checkbox"  id="togglePassword" onclick="showPassword()" > Show Password
                                    </label>         
                                </div>    
           
       
                           
                            <br>
                                <div style="align-content: center"><button id="btnSubmit" class="w-75 btn btn-primary text-white fw-bold btn-round mt-2">Submit</button>  </div>           
                              
                            </form>
                                <hr>
                                <p class="small fw-bold mt-2">
                                    {{-- <a class="float-start" href="{{ route('login') }}">Log in</a> --}}
                                    <div class="float-end mt-n1">
                                        <a href="{{ route('username-success') }}">I forgot my username</a> 
                                    </div>    
                                </p>
                                <br/>
                           
                        <div class="mt-3" style="border: 2px dashed #2c2c2c; border-radius: 5px; padding: 5px;">
                            Don't have an account? <a href="{{ route('user-register') }}" class="fw-bold">Sign Up</a>   
                        </div>
                    
            </div>
        </div>
    </div>
        
</main>


   
    @endsection

@section('vendor-scripts')

@endsection

@section('page-scripts')
    <script>
    
        function showPassword() {
        const togglePassword = document.querySelector("#togglePassword");
        const x = document.getElementById("newpassword");
        const x2 = document.getElementById("confirmpassword");
    
        if ((x.type === "password") && (x2.type === "password")) {
            x.type = "text";
            x2.type = "text";
            togglePassword.classList.toggle("bxs-show");
        } else {
            x.type = "password";
            x2.type = "password";
        }
        }
    

        function validatePassword() {
        var passwordInput = document.getElementById('newpassword');
        var otherPasswordInput = document.getElementById('confirmpassword');
        var passwordError = document.getElementById('password-error');
        
        // Check password requirements
        var regex = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]+$/;
        if (passwordInput.value.length < 8 || !regex.test(passwordInput.value)) {
            passwordError.textContent = 'Password must be at least 8 characters and contains symbol, number, and uppercase letter.';
            passwordInput.setCustomValidity('Invalid password');
        } else if (passwordInput.value !== otherPasswordInput.value) {
            passwordError.textContent = 'Passwords do not match.';
            passwordInput.setCustomValidity('Passwords do not match');
        }
         else {
            passwordError.textContent = '';
            passwordInput.setCustomValidity('');
        }
    }

    </script>
    @endsection


  


