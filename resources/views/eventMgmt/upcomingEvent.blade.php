@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Event Management')
{{-- vendor css --}}
@section('vendor-styles')
{{-- <link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}"> --}}
{{-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --}}

<link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('page-styles')
<style>

    @media only screen and (max-width: 792px) {
      #sectionFormMain>div>div:nth-child(5)>div.col-4 {
        margin-top: 19px;
      }
    }
  
   
    .header-top-margin {
        margin-top: 55px;
        padding-bottom: 200px;
        margin-bottom: 55px;
    }

    .btn-block {
        display: block;
        width: 100%;
    }

    .btn-add-2 {
        background-color: #5A8DEE;
        color: #fff;
        border: 1px solid #5A8DEE;
        border-radius: 5px;
        font-size: 14px;
        font-family: poppins-semibold, sans-serif;
        padding: 10px 20px;
    }

.btn-add-2:hover {
  color: #fff;
}

.btn-add-2:disabled, .btn-add-2[disabled], .btn-add-2:hover {
  color: #fff !important;
  opacity: 0.5;
}  
.btn-secondary-6 {
  background-color: #fff;
  color: #5A8DEE;
  border: 1px solid #5A8DEE;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 16px;
  font-family: poppins-semibold, sans-serif;
  line-height: 20px;
  text-align: left;
}

.btn-secondary-6:hover {
  background-color: #5A8DEE;
  color: #fff;
}

.table-row td {
        padding: 10px;
        border-bottom: none;
    }

    .table-striped > tbody > tr:nth-of-type(odd) > * {
        --bs-table-accent-bg: #fff;
        color: var(--bs-table-striped-color);
    }

    .table-striped>tbody>tr:nth-of-type(odd)>* {
        --bs-table-color-type: var(--bs-table-striped-color);
        --bs-table-bg-type: #fff;
    }

    .page-title {
        color: #656565;
        font-family: Rajdhani;
        font-size: 26px;
        font-style: normal;
        font-weight: 600;
    }

.table-title {
        color: #333;
        font-family: Open Sans;
        font-size: 16px;
        font-style: normal;
        font-weight: 600;
    }

.icon-editIcon:before {
  content: "\e916";
}

.fully-booked-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .closed-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .open-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .table-header {
        background-color: #f4f4f4;
        font-weight: 600;
    }

    .table-header th {
        padding: 10px;
        border-bottom: none;
    }

     /* Add styles for the table */
     .table {
    width: 100%;
    border-collapse: collapse;
    margin: 0;
    }

   

    .table th, .table td {
    padding: 10px;
    text-align: center;
    border: 1px solid #ddd;
    }

    /* Add hover effect on table rows */
    .table tbody tr:hover {
    background-color: #f5f5f5;
}

.page-title {
      font-size: 24px;
      font-weight: bold;
    }

    .null-value {
        border-radius: 8px;
        border: 1px solid var(--warning-500, #F79009);
        background: var(--warning-25, #FFFCF5);
        padding: 4px 8px;
        color: #F79009;
    }

    .success-status {
        border-radius: 8px;
        border: 1px solid #3498DB;
        background: var(--success-25, #F6FEF9);
        padding: 4px 8px;
        color: #3498DB;
    }

    .active-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #17B26A);
        background: var(--success-25, #F6FEF9);
        padding: 4px 25px;
        color: #17B26A;
    }

    .failed-status {
        border-radius: 8px;
        border: 1px solid var(--success-500, #FF0000);
        background: var(--success-25, #F3DEDE);
        padding: 4px 25px;
        color: #FF0000;
    }
    .table-content {
        color: #333;
        font-family: Open Sans;
        font-size: 14px;
        font-style: normal;
        font-weight: 400;
        line-height: 20px;
        text-align: left;
    }
</style>
@endsection

@section('content')
<main style="padding:50px;margin:4px;">
    <section class="header-top-margin">
    @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
    @else
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
    @endif

    
    @csrf
    @method('DELETE')
    <div class="row">
      <h2 class="page-title py-2" style="text-align: left !important;">List of Training</h2>
    </div>
  </br>

  <div>
    <div class="table-responsive col-md-12 mx-auto">

        <table class="table table-striped dataTable no-footer" id="listoftraining" style="width: 100%; border-bottom: white;">

            <thead class="table-header">
                <tr>
                    <th class="table-title" style="border-bottom: none; text-align: center;" width=40;>No.</th>
                    <th class="table-title" style="border-bottom: none; text-align: center;">Programme</th>
                    <th class="table-title" style="border-bottom: none; text-align: center;">Start Date</th>
                    <th class="table-title" style="border-bottom: none; text-align: center;">End Date</th>
                    <th class="table-title" style="border-bottom: none; text-align: center;">Venue</th>
                    <th class="table-title" style="border-bottom: none; text-align: center;">Fee (RM)</th>
                    <th class="table-title" style="border-bottom: none; text-align: center;">Status</th>
                </tr>
            </thead>

            <tbody class="table-content">

            </tbody>

        </table>
    </div>
  </div>

  <div class="row" style="margin-top: 7%;">
    <h2 class="page-title py-2" style="text-align: left !important;">Upcoming Training</h2>
  </div>
</br>

<div>
    <div class="table-responsive col-md-12 mx-auto">

        <table class="table table-striped dataTable no-footer" id="upcomingevent" style="width: 100%; border-bottom: white;">

        <thead class="table-header">
                <tr>
                    <th class="table-title text-center" style="border-bottom: none;">No.</th>
                        <th class="table-title text-center" style="border-bottom: none;">Programme</th>
                        <th class="table-title text-center" style="border-bottom: none;">Course Category</th>
                        <th class="table-title text-center" style="border-bottom: none;">Training Type</th>
                        <th class="table-title text-center" style="border-bottom: none;">Batch</th>
                        <th class="table-title text-center" style="border-bottom: none;">Start Date</th>
                        <th class="table-title text-center" style="border-bottom: none;">End Date</th>
                        <th class="table-title text-center" style="border-bottom: none;">Status</th>
                </tr>
            </thead>

            <tbody class="table-content">



            </tbody>

        </table>
    </div>
</div>
   
    </section>
</main>
  
  
@endsection

@section('vendor-scripts')
{{-- js from ny downloaded a plugin --}}
   
    <script src="{{ asset('js/vendors/jquery.dataTables.min.js') }}"></script> 
    
@endsection



@section('page-scripts')

<script>
    let host = "{{ env('API_SERVER') }}";
    let userID = "{{ $userid }}";

</script>


<script src="{{ asset('js/eventMgmt/eventManagement.js') }}"></script>

@endsection
