
$(function(){

  ScheduleList();
  HistoryList();
  //TrainingDetails();


});




  let ScheduleList = () => {
    userid = userID;
       let getList = `${host}/api/dashboard/trainingPartner/${userid}/TPTrainingSchedule`;

   
      $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
      })
        .done((res) => {
          //createTable(res.data);
          tableScheduleList(res.TrainingSchedule);
          console.log(res.TrainingSchedule);
        })
        .catch((err) => {});
    };


    let tableScheduleList = (data) => {
      let senarai = $("#TPSchedule").DataTable({
        data: data,
        //  order: [[4, "desc"]],
        columnDefs: [
          {
            targets: 0,
            data: null,
            searchable: false,
            orderable: false,
          },
          {
            targets: 1,
            data: "training_name",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
              }
              if (data !== null || data !== "") {
                return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
              }
            },
          },
          {
            targets: 2,
            data: "course_category",
            render: function (data, type, full, meta) {
              return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
            },
          },
          {
              targets: 3,
              data: "training_type",
              render: function (data, type, full, meta) {
                if (data && data !== '' && data !== null) {
                  return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                
                } else {
                  return '<td width="15%"><p class="poppins-semibold-14">-</p></td>';
                }
              },
            },
          {
            targets: 4,
            data: "date_start",
            render: function (data, type, full, meta) {
              if(data && data != '' && data != null) {
              
                return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
              } else {
                return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
              }
              
            },
          },

          {
              targets: 5,
              data: "date_end",
              render: function (data, type, full, meta) {
                if(data && data != '' && data != null) {
                
                  return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                } else {
                  return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
                }
                
              },
            },
         
          {
            targets: 6,
            data: "status",
            //visible: false,
            render: function (data, type, full, meta) {

               let link = "";
              // if (data == null || data === "") {
              //     return `<a href="view-exam-result" class="btn btn-sm">
              //                   <i class="icon-editIcon" style="font-size: 18px;"></i>
              //               </a>`;
              //   }
                if (data != null || data !== "") {
                  return  `<td width="15%"><p class="poppins-semibold-14">Active</p></td>`;
                }
              //return data;
            },
          },
        ],
        language: {
          info: "view _START_ to _END_ from _TOTAL_ entry",
          infoEmpty: "view 0 from 0 to 0 entry",
          lengthMenu: "view _MENU_ Entry",
          paginate: {
            next: "Next",
            previous: "Previous",
          },
          zeroRecords: "No record found",
        },
      });
    
      senarai
        .on("order.dt search.dt", function () {
          senarai
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
              cell.innerHTML = i + 1;
            });
        })
        .draw();
    
      $("#filterStatus").on("change", (e) => {
        // console.log(e.target.value)
        senarai.column(5).search(e.target.value).draw();
      });
    
      $("#filterJenisKenderaan").on("change", (e) => {
        console.log(e.target.value);
        senarai.column(1).search(e.target.value).draw();
      });
    
      $("#search_input").on("keyup", () => {
        let searched = $("#search_input").val();
        // console.log(searched)
        senarai.search(searched).draw();
      });
    
      $("#resetFilter").on("click", () => {
        $("#filterStatus").val("").trigger("change");
        $("#filterJenisKenderaan").val("").trigger("change");
        $("#search_input").val("");
    
        senarai.search("").columns().search("").draw();
      });
    };
    

    //////History////////////////////
    let HistoryList = () => {
      userid = userID;

      let getList = `${host}/api/dashboard/trainingPartner/${userid}/TPTrainingHistory`;
     
     $.ajax({
       url: getList,
       type: "GET",
       dataType: "JSON",
     })
       .done((res) => {
         //createTable(res.data);
         tableHistoryList(res.TrainingHistory);
         console.log(res.TrainingHistory);
       })
       .catch((err) => {});
   };


   let tableHistoryList = (data) => {
     let senarai = $("#TPHistory").DataTable({
       data: data,
       //  order: [[4, "desc"]],
       columnDefs: [
         {
           targets: 0,
           data: null,
           searchable: false,
           orderable: false,
         },
         {
           targets: 1,
           data: "training_name",
           render: function (data, type, full, meta) {
             if (data === null) {
               return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
             }
             if (data !== null || data !== "") {
               return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
             }
           },
         },
         {
           targets: 2,
           data: "course_category",
           render: function (data, type, full, meta) {
             return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
           },
         },
         {
             targets: 3,
             data: "training_type",
             render: function (data, type, full, meta) {
               if (data && data !== '' && data !== null) {
                 return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
               
               } else {
                 return '<td width="15%"><p class="poppins-semibold-14">-</p></td>';
               }
             },
           },
           {
              targets: 4,
              data: "date_start",
              render: function (data, type, full, meta) {
                if(data && data != '' && data != null) {
                
                  return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                } else {
                  return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
                }
                
              },
            },

            {
                targets: 5,
                data: "date_end",
                render: function (data, type, full, meta) {
                  if(data && data != '' && data != null) {
                  
                    return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                  } else {
                    return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
                  }
                  
                },
              },
              {
                targets: 6,
                data: "status",
                render: function (data, type, full, meta) {
                  
                  
                    return  `<td width="15%"><p class="poppins-semibold-14">Complete</p></td>`;
                 
                  
                },
              },
           
            {
              targets:7,
              data: "id",
              //visible: false,
              render: function (data, type, full, meta) {

                 let link = "";
               
                  // if (data != null || data !== "") {

                 
            
                   return  `
                              <a href="${host}/dashboard/training-partner/attendance-list/${full.batch_no}/${data}" class="btn btn-sm">
                              <button class="btn btn-sm icon-style" data-id="${data}" 
                              <i class="icon-sunting" data-id="${data}">View Details</i>
                              </button></a>
                              
                             `;
                  // }
                 
                
              },
            },
       ],
       language: {
         info: "view _START_ to _END_ from _TOTAL_ entry",
         infoEmpty: "view 0 from 0 to 0 entry",
         lengthMenu: "view _MENU_ Entry",
         paginate: {
           next: "Next",
           previous: "Previous",
         },
         zeroRecords: "No record found",
       },
     });
   
     senarai
       .on("order.dt search.dt", function () {
         senarai
           .column(0, { search: "applied", order: "applied" })
           .nodes()
           .each(function (cell, i) {
             cell.innerHTML = i + 1;
           });
       })
       .draw();
   
     $("#filterStatus").on("change", (e) => {
       // console.log(e.target.value)
       senarai.column(5).search(e.target.value).draw();
     });
   
     $("#filterJenisKenderaan").on("change", (e) => {
       console.log(e.target.value);
       senarai.column(1).search(e.target.value).draw();
     });
   
     $("#search_input").on("keyup", () => {
       let searched = $("#search_input").val();
       // console.log(searched)
       senarai.search(searched).draw();
     });
   
     $("#resetFilter").on("click", () => {
       $("#filterStatus").val("").trigger("change");
       $("#filterJenisKenderaan").val("").trigger("change");
       $("#search_input").val("");
   
       senarai.search("").columns().search("").draw();
     });
   };
   

