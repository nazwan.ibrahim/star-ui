
$(function(){
  TrainingList();
  TrainingDetails();
  TrainingDetailsTable();
  upcomingList();
});

  let TrainingList = () => {

      ID =   userID;
    //  alert(ID);
       let getList = `${host}/api/dashboard/participant/${ID}/ParticipantTrainingList`;
   
      $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
      }).done((res) => {
          //createTable(res.data);
          tableList(res.participantTraining);
          //console.log(res.participantTraining);
        })
        .catch((err) => {});
  };


  let tableList = (data) => {
      let senarai = $("#trainingList").DataTable({
        data: data,
        //  order: [[4, "desc"]],
        columnDefs: [
          {
            targets: 0,
            data: null,
            searchable: false,
            orderable: false,
          },
          {
            targets: 1,
            data: "training_name",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
              }
              if (data !== null || data !== "") {
                return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
              }
            },
          },
          {
            targets: 2,
            data: "date_start",
            render: function (data, type, full, meta) {
              if(data && data != '' && data != null) {
              
                return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
              } else {
                return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
              }
              
            },
          },

          {
              targets: 3,
              data: "date_end",
              render: function (data, type, full, meta) {
                if(data && data != '' && data != null) {
                
                  return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                } else {
                  return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
                }
                
              },
            },
            {
              targets: 4,
              data: "venue",
              render: function (data, type, full, meta) {
                if(data && data != '' && data != null) {
                
                  return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
                } else {
                  return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
                }
                
              },
            },
            {
              targets: 5,
              data: "id_approval_status",
              render: function (data, type, full, meta) {
                if(data == '1' || data == '0'){
                  return  `<td width="15%"><p class="null-value" style="text-align: center;">Pending Approval</p></td>`;
                }
                else if(data == '2'){
                  return  `<td width="15%"><p class="active-status" style="text-align: center;">Approved</p></td>`;
                }
                else if(data == '3'){
                  return  `<td width="15%"><p class="failed-status" style="text-align: center;">Rejected</p></td>`;
                }
              },
            },
         
          {
            targets: 6,
            data: "training_status",
            render: function (data, type, full, meta) {
              if(data == '1'){
                return  ` 
                <td width="50%" align="center">
                  <div>
                      <a href="participant/participantToMakePayment/${full.id_training}" class="btn btn-sm failed-status">Please Pay Now</a>
                  </div>
                </td>`;
  

              }else if (data == '2') {
                return  `<td width="=50%">
                <a id="testid" href="participant/trainingDetails/${full.id_training}" class="btn btn-sm">
                  <button class="btn btn-sm icon-style null-value" data-id="${data}">
                    <i class="icon-sunting" data-id="${data}">VIEW DETAILS</i>
                  </button>
                </a>
              </td>`;
               }else {
                  return  `<td width="=50%">
                  <a id="testid" href="participant/trainingDetails/${full.id_training}" class="btn btn-sm">
                   -
                  </a>
                </td>`;
                  }  
            },
          },
        ],
        language: {
          info: "view _START_ to _END_ from _TOTAL_ entry",
          infoEmpty: "view 0 from 0 to 0 entry",
          lengthMenu: "view _MENU_ Entry",
          paginate: {
            next: "Next",
            previous: "Previous",
          },
          zeroRecords: "No record found",
        },
      });
    
      senarai
        .on("order.dt search.dt", function () {
          senarai
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
              cell.innerHTML = i + 1;
            });
        })
        .draw();
    
    
   
  };
    


  let TrainingDetails = () => {

   
      //ID =   $(this).data("id");
      let url = window.location.pathname;
      let ID = url.substring(url.lastIndexOf('/') + 1);
     //   alert(ID);
      let getDetails = `${host}/api/dashboard/participant/${ID}/ParticipantTrainingDetails`;
   
      $.ajax({
        url: getDetails,
        type: "GET",
        dataType: "JSON",
      }).done((res) => {
          //createTable(res.data);
         data = res.details;
          console.log(res.details);
          $.each(data, function(index, value) {

 
              (value.training_name == null || value.training_name == '') ? $('#training_name').text('n/a') : $('#training_name').text(value.training_name);
              (value.provider_name == null || value.provider_name == '') ? $('#provider_name').text('n/a') : $('#provider_name').text(value.provider_name);
              (value.date_start == null || value.date_start == '') ? $('#date_start').text('n/a') : $('#date_start').text(value.date_start);
              (value.date_end == null || value.date_end == '') ? $('#date_end').text('n/a') : $('#date_end').text(value.date_end);
              (value.venue_address == null || value.venue_address == '') ? $('#venue_address').text('n/a') : $('#venue_address').text(value.venue_address);
             
              
              });
          
      

        })
        .catch((err) => {});
  };


  

  let TrainingDetailsTable = () => {

   
      //ID =   $(this).data("id");
      let url = window.location.pathname;
      let ID = url.substring(url.lastIndexOf('/') + 1);
      //  alert(ID);
      let getDetails = `${host}/api/dashboard/participant/${ID}/ParticipantTrainingDetails`;
   
      $.ajax({
        url: getDetails,
        type: "GET",
        dataType: "JSON",
      }).done((res) => {
          //createTable(res.data);
         // let dataArray = res.detailsParticipant;
          //let dataArray = Object.values(res.detailsParticipant);
          tableDetails(res.detailsParticipant);
       
          console.log('detailsParticipant',res.detailsParticipant);
        
          
      

        })
        .catch((err) => {});
  };


  let tableDetails = (data) => {
    let senarai = $("#trainingDetails").DataTable({
      data: data,
      columns: [
        {
          //data: "payment_id",
          //data:is_paid,
         // render: function (data, type, full, meta) {

            render: function (data, type, full, meta) {
              if (full.status_payment == 1) {
                return `<a id="testid" href="${host}/dashboard/participant/paymentInvoice/${full.id_users}"><button class="btn btn-primary" id="payment_id">VIEW PAYMENT RECEIPT</button></a>`;
              //  return full.KuantitiMohon;
              }
             
          
            },

          
        },
        {
          //data: "date_attend",
            render: function (data, type, full, meta) {
              if (full.status_attend == 1) {
                return `Date & Time:  ${full.date_attend}`;
              
              }else{
                return `Date & Time:  <b>Training Not Started Yet</b>`;
              }
             
            },
            
          
        },
        {
          data: "status_feedback",
          render: function (data, type, full, meta) {
            return data;
          },
        },
        {
          data: "final_verdict_status",
          render: function (data, type, full, meta) {
            if(data == "Pass"){
            return data;
            }else if(data == "Failed"){
              return `${data}<BR><a id="testid" href="${host}/dashboard/participant/paymentInvoice/${full.id_users}"><button class="btn btn-primary" id="payment_id">RE SIT</button></a>`;
            }else{
              return `<b>Training Not Start Yet</b>`;
            }
          },
        },
        {
          data: "certificate_no",
          render: function (data, type, full, meta) {
            return data;
          },
        },
        // Add more columns as needed
      ],
    });
  };

  let upcomingList = () => {

    console.log('upcomingList function is called');
    ID =   userID;
    let getList = `${host}/api/dashboard/participant/${ID}/ParticipantAvailableList`;
    $.ajax({
        url: getList,
        type: "GET",
        dataType: "JSON",
    })
    .done((res) => {
        console.log('API Response:', res);
        if (res.status === 'success') {
            tableTrainingList(res.upcomingList);
            console.log('Upcoming List:', res.upcomingList);
        } else {
            console.error('Error:', res.message);
        }
    })
    .fail((err) => {
        console.error('AJAX request failed:', err);
    });
  };

  let tableTrainingList = (data) => {
    let dataTable = $("#upcomingList").DataTable({
        data: data,
        responsive: true,
        columns: [
            {
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            {
                data: "course_name",
                searchable: true,
                targets: [1],
                render: function (data, type, full, meta) {
                    return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
                }
            },
            {
              data: "course_category",
              searchable: true,
              targets: [1],
              render: function (data, type, full, meta) {
                  return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
              }
          },
            {
                data: "training_type",
                render: function (data, type, full, meta) {
                    if (data) {
                        return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                    } else {
                      return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                  }
              }
          },
          {
              data: "date_start",
              render: function (data, type, full, meta) {
                  if (data) {
                      return `<td width="50%" align="center"><p class="table-content">${data}</p></td>`;
                  } else {
                      return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                  }
              }
          },
          {
              data: "date_end",
              render: function (data, type, full, meta) {
                  return data ? `<td width="50%" align="center"><p class="table-content">${data}</p></td>` : '-';
              }
          },
          {
            data: "status_id",
            render: function (data, type, full, meta) {
                if (full.status_name) {
                    return `<td width="50%" align="center"><p class="table-content">${full.status_name}</p></td>`;
                } else {
                    return '<td width="50%" align="center"><div class="null-value">TBC</div></td>';
                }
            }
          },
          {
              data: "id",
              render: function (data, type, full, meta) {
          
                let applyLink = `${host}/dashboard/participant/apply/${data}`;
                if (data !== null && data !== "") {
                  return `
                        <td width="50%" align="center">
                          <div>
                              <a href="${applyLink}" class="btn btn-sm btn-primary">Apply Now</a>
                          </div>
                        </td>`;
                } else {
                    return "-";
                }

              }
          },
      ],
      lengthMenu: [5, 10, 15, 20],
      pageLength: 5,
      language: {
          info: "_START_ to _END_ from _TOTAL_ entry",
          infoEmpty: "0 from 0 to 0 entry",
          lengthMenu: " _MENU_ Entry",
          paginate: {
              next: "Next",
              previous: "Previous",
          },
          zeroRecords: "No record found",
      },
  });

  $('#trainingList').on('click', '.delete-training-button', function (e) {
      e.preventDefault();
      let trainingId = $(this).data('training-id');

      $.ajax({
          url: `/api/training/softDelete/${trainingId}`,
          type: "GET",
          dataType: "JSON",
      })
      .done((res) => {
        if (res.status === 'success') {
          dataTable.row($(this).closest('tr')).remove().draw();
          console.log('Training record marked as deleted successfully.');
  
          window.location.href = '/admin/training-management';
      } else {
          console.error('Error:', res.message);
      }
  })
  .fail((err) => {
      console.error('AJAX request failed:', err);
  });
});

var viewReportButton = $('<button id="viewReportButton" class="circular-button" style="color: white"><i class="fas fa-plus"></i></button>');
viewReportButton.on('click', function () {
  window.location.href = "/admin/training-registration";
});

viewReportButton.css({
  width: '37px',
  height: '37px',
  backgroundColor: '#3498DB',
  border: '2px solid #3498DB',
  borderRadius: '50%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  cursor: 'pointer',
  display: 'none',
});

  /* viewReportButton.css('margin-left', '10px');
  $('.dataTables_filter').append(viewReportButton);

  let dropdown = $('<select id="dropdownList" class="filter-dropdown" style="width: auto;">' +
              '<option value="" disabled selected>Filter</option>' +
              '<option value="option1">Option 1</option>' +
              '<option value="option2">Option 2</option>' +
              '<option value="option3">Option 3</option>' +
              '</select>');
  $('.dataTables_filter').prepend(dropdown); */

  dataTable.on("order.dt search.dt", function () {
    dataTable
        .column(0, { search: "applied", order: "applied" })
        .nodes()
        .each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    })
    .draw();
}

  