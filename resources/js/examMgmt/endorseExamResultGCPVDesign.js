let senarai;
let findheader;
$(function(){

    ExamList();
   
    // endorseExamResult();
    queryExamResult();
   
});


// $(document).ready(function () {
//     updateExamResult();
// });

// $(document).on('click', '#editResult', function() {
   let url = window.location.pathname;
   let batch_full = url.substring(url.lastIndexOf('/') + 1);
   let segments = url.split('/');
   let ID = segments[segments.length - 2];
  
  let getDetails = `${host}/api/exam/${ID}/${batch_full}/examDetailsAdmin2`;
  //console.log('examDetails',ID);

  $.ajax({
    url: getDetails,
    type: 'GET',
   // contentType: "application/json",
    dataType: "JSON",
}).done((res) => {
    let data = res.trainingDetails;
    console.log('sss',data);

    $.each(data, function(index, value) {

   
    (value.training_name == null || value.training_name == '') ? $('#provider_name').text('n/a') : $('#provider_name').text(value.training_name);
    (value.date_start == null || value.date_start == '') ? $('#date_start').text('n/a') : $('#date_start').text(value.date_start);
    (value.date_end == null || value.date_end == '') ? $('#date_end').text('n/a') : $('#date_end').text(value.date_end);
    (value.venue == null || value.venue == '') ? $('#venue').text('n/a') : $('#venue').text(value.venue);
    (value.batch_no_full == null || value.batch_no_full == '') ? $('#batch_no').text('n/a') : $('#batch_no').text(value.batch_no_full);
    
    (value.batch_no_full == null || value.batch_no_full == '') ? $('#batch_no_query').val('n/a') : $('#batch_no_query').val(value.batch_no_full);
    (value.id_training_course == null || value.id_training_course == '') ? $('#id_training_course').val('n/a') : $('#id_training_course').val(value.id_training_course);
    
    });


}).catch((err) => {
    //console.log(err);
});
 

// });



  $(document).on('click', '#editResult', function() {
    
     ID =   $(this).data("id");
     console.log('s',ID);

    let url = window.location.pathname;
    let batch_full = url.substring(url.lastIndexOf('/') + 1);
    
    
//alert(batch_full);
    let getExamResult = `${host}/api/exam/${ID}/${batch_full}/getExamResult`;


      $.ajax({
            url: getExamResult,
            type: 'GET',
            //contentType: "application/json",
            dataType: "JSON",
        }).done((res) => {
            let data = res.exam;
            console.log('exam',data);
      
            $.each(data, function(index, value) {

           

            (value.part_A == null || value.part_A == '') ? $('#partA').val('n/a') : $('#partA').val(value.part_A);
            (value.part_B == null || value.part_B == '') ? $('#partB').val('n/a') : $('#partB').val(value.part_B);
            (value.part_C == null || value.part_C == '') ? $('#partC').val('n/a') : $('#partC').val(value.part_C);
            (value.total_mark_practical == null || value.total_mark_practical == '') ? $('#totalResultPractical').val('n/a') : $('#totalResultPractical').val(value.total_mark_practical);
           
            (value.result_status_practical == null || value.result_status_practical == '') ? $('#statusResultPracticalExam').val('n/a') : $('#statusResultPracticalExam').val(value.result_status_practical);
        

            (value.part_A_theory == null || value.part_A_theory == '') ? $('#part-AT').val('n/a') : $('#part-AT').val(value.part_A_theory);
            (value.part_B_theory == null || value.part_B_theory == '') ? $('#part-BT').val('n/a') : $('#part-BT').val(value.part_B_theory);
            (value.part_C_theory == null || value.part_C_theory == '') ? $('#part-CT').val('n/a') : $('#part-CT').val(value.part_C_theory);
             (value.total_mark_theory == null || value.total_mark_theory == '') ? $('#totalResultTheory').val('n/a') : $('#totalResultTheory').val(value.total_mark_theory);
             
            (value.result_status_theory == null || value.result_status_theory == '') ? $('#statusResultTheory').val('n/a') : $('#statusResultTheory').val(value.result_status_theory);

            (value.final_verdict_status == null || value.final_verdict_status == '') ? $('#statusResultFinal').val('n/a') : $('#statusResultFinal').val(value.final_verdict_status);
        
            (value.id_participant_examresult == null || value.id_participant_examresult == '') ? $('#id_participant_examresult').val('n/a') : $('#id_participant_examresult').val(value.id_participant_examresult);
            (value.id_training == null || value.id_training == '') ? $('#id_training').val('n/a') : $('#id_training').val(value.id_training);
            });
        

        }).catch((err) => {
            //console.log(err);
        });

    $('#edit-result-modal').modal('show');

});






    let ExamList = () => {

        let url = window.location.pathname;
        let batch_full = url.substring(url.lastIndexOf('/') + 1);
        let segments = url.split('/');
        let ID = segments[segments.length - 2];
       // batch_full = batch_no_full;
 //alert(batch_full);alert(ID);

 
    let getExamList = `${host}/api/exam/${batch_full}/${ID}/examNameList_GCPVDESIGN`;
     //   let getExamList = `${host}/api/exam/${batch_full}/${ID}/examNameListAdminCGPV`;
     
        $.ajax({
          url: getExamList,
          type: "GET",
          dataType: "JSON",
        })
          .done((res) => {
            findheader = res.headerName;
         
         let resultList = res.examlist;
         console.log('findheader.fundamental_exam_1',findheader.fundamental_exam_1);
         console.log('findheader.fundamental_exam_2',findheader.fundamental_exam_2);
         console.log('findheader.fundamental_exam_3',findheader.fundamental_exam_3);

         console.log('findheader.design_exam_1',findheader.design_exam_1);
         console.log('findheader.design_exam_2',findheader.design_exam_2);
         console.log('findheader.design_exam_3',findheader.design_exam_3);

         console.log('findheader.practical_exam_1',findheader.practical_exam_1);
         console.log('findheader.practical_exam_2',findheader.practical_exam_2);
         console.log('findheader.practical_exam_3',findheader.practical_exam_3);
        

         //console.log('findheaderd',findheader);


         console.log(resultList);
           
            
         tableExamNameList(resultList,findheader);
         tableExamNameList2(resultList,findheader);
         tableExamNameList3(resultList,findheader);
      

         


          })
          .catch((err) => {});
      };
 
      var table = document.getElementById("examResultUpdate");

// Get the first row of the table (assuming the headers are in the first row)
var headerRow = table.getElementsByTagName("thead")[0].getElementsByTagName("tr")[0];

// Count the number of cells (columns) in the header row
var numColumns = headerRow.cells.length;
console.log("Number of columns: " + numColumns);

var columnNames = [];
for (var i = 0; i < headerRow.cells.length; i++) {
    var columnName = headerRow.cells[i].textContent || headerRow.cells[i].innerText;
    columnNames.push(columnName.trim());
}

console.log("Column names: " + columnNames.join(', '));


console.log("Column 0: " + columnNames[0]);
console.log("Column 1: " + columnNames[1]);
console.log("Column 2: " + columnNames[2]);
console.log("Column 3: " + columnNames[3]);
console.log("Column 4: " + columnNames[4]);
console.log("Column 5: " + columnNames[5]);
console.log("Column 6: " + columnNames[6]);
console.log("Column 7: " + columnNames[7]);
console.log("Column 8: " + columnNames[8]);
console.log("Column 9: " + columnNames[9]);

console.log("Column 10: " + columnNames[10]);
console.log("Column 11: " + columnNames[11]);
console.log("Column 12: " + columnNames[12]);
console.log("Column 13: " + columnNames[13]);
console.log("Column 14: " + columnNames[14]);
console.log("Column 15: " + columnNames[15]);
console.log("Column 16: " + columnNames[16]);

console.log("Column 17: " + columnNames[17]);
console.log("Column 18: " + columnNames[18]);
console.log("Column 19: " + columnNames[19]);

console.log("Column 20: " + columnNames[20]);
console.log("Column 21: " + columnNames[21]);
console.log("Column 22: " + columnNames[22]);



let tableExamNameList = (data) => {
        
  if (numColumns === 19) //template 1
    {
      if(
        (columnNames[3] === findheader.fundamental_exam_1 && columnNames[4] === findheader.fundamental_exam_2 
          && columnNames[6] === "Fundamental Result")
          && columnNames[7] === findheader.design_exam_1 && columnNames[8] === findheader.design_exam_2 
          && columnNames[10] === "Design Result"
          && columnNames[11] === findheader.practical_exam_1 && columnNames[12] === findheader.practical_exam_2 
          && columnNames[13] === findheader.practical_exam_3 && columnNames[15]  === "Practical Result")
        {  console.log('template 1');
           senarai = $("#examResultUpdate").DataTable
          ({
            data: data,
            // data:dataArray,
            //  order: [[4, "desc"]],
            columnDefs: 
            [
              {
                targets: 0,
                data: function (row, type, full, meta) {
                // Use meta.row to get the row index (starting from 0)
                return meta.row + 1;
                },
                searchable: false,
                orderable: false,
              },
              {
                targets: 1,
                data: "fullname",
              //data: "id_training_participant",
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                  
                },
              },
              {
                targets: 2,
                data: "exam_type",
              //data:"exam_type",
                render: function (data, type, full, meta) {
                  
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                  <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                },
              },
              {
                  targets: 3,
                  data: "part_A_fundamental",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index    
                    if((data === '') || (data === null)) {                   
                    return `<input type="text" value="" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){     
                      return `<input type="text" value="${data}" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                  },
                },
              {
                targets: 4,
                data: "part_B_fundamental",
                render: function (data, type, full, meta) {

                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {         
                  return `<input type="text" value="" id="part_b_fundamental${rowIndex}" name="part_b_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){   
                  return `<input type="text" value="${data}" id="part_b_fundamental${rowIndex}" name="part_b_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },
             
              {
                targets: 5,
                data: "total_mark_fundamental",
                className: "text-center",
                render: function (data, type, full, meta) {
                 
                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                 }
                },
              },
              {
                targets: 6,
                data: "result_status_fundamental",
                //visible: false,
                render: function (data, type, full, meta) {

                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                 }
                },
              },
          
              {
                  targets: 7,
                  data: "part_A_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
    
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },

              {
                targets: 8,
                data: "part_B_design",
                //visible: false,
                render: function (data, type, full, meta) {
  
                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_b_design${rowIndex}" name="part_b_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_b_design${rowIndex}" name="part_b_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
            },
             
              {
                  targets: 9,
                  data: "total_mark_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                  targets: 10,
                  data: "result_status_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                targets: 11,
                data: "part_A_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 12,
                data: "part_B_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_b_practical${rowIndex}" name="part_b_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_b_practical${rowIndex}" name="part_b_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 13,
                data: "part_C_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 14,
                data: "total_mark_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 15,
                data: "result_status_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },
              
              {
                  targets: 16,
                  data: "final_verdict_status",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                    return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                targets: 17,
                //data: "submitted_status",
                data:"endorse_exam",
                //visible: false,
                render: function (data, type, full, meta) {

                
                if (data === null || data === '') {
                  return `Not Submit`;
                
                }else if (data !== null || data !== '') {
                  return `Submitted to SEDA`;
                }
                },
              },
              {
                  targets: 18,
                  data: "id",
                  //visible: false,
                  render: function (data, type, full, meta) {

                      if (data !== null || data !== '') {
                          return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}" title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                        
                      }
                 //return data;
                  },
              },
            ],
                language: {
                info: "view _START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "view 0 from 0 to 0 entry",
                lengthMenu: "view _MENU_ Entry",
                paginate: {
                  next: "Next",
                  previous: "Previous",
                },
                zeroRecords: "No record found",
              },
          });
       
      
      } else {
      // Your code for the second condition
      console.log("Condition 2 matched");
            } 
    } else if (numColumns === 18) //template 2
    {
      if(
      (columnNames[3] === findheader.fundamental_exam_1 && columnNames[4] === findheader.fundamental_exam_2 && columnNames[6] === "Fundamental Result")
      && columnNames[7] === findheader.design_exam_1 && columnNames[9] === "Design Result"
      && columnNames[10] === findheader.practical_exam_1 && columnNames[11] === findheader.practical_exam_2 
      && columnNames[12] === findheader.practical_exam_3 && columnNames[14]  === "Practical Result")  
        {  console.log('template 2')
          let senarai = $("#examResultUpdate").DataTable
          ({
            data: data,
            // data:dataArray,
            //  order: [[4, "desc"]],
            columnDefs: 
            [
              {
                targets: 0,
                data: function (row, type, full, meta) {
                // Use meta.row to get the row index (starting from 0)
                return meta.row + 1;
                },
                searchable: false,
                orderable: false,
              },
              {
                targets: 1,
                data: "fullname",
              //data: "id_training_participant",
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                  
                },
              },
              {
                targets: 2,
                data: "exam_type",
              //data:"exam_type",
                render: function (data, type, full, meta) {
                  
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                  <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                },
              },
              {
                  targets: 3,
                  data: "part_A_fundamental",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index    
                    if((data === '') || (data === null)) {                   
                    return `<input type="text" value="" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){     
                      return `<input type="text" value="${data}" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                  },
                },
              {
                targets: 4,
                data: "part_B_fundamental",
                render: function (data, type, full, meta) {

                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {         
                  return `<input type="text" value="" id="part_b_fundamental${rowIndex}" name="part_b_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){   
                  return `<input type="text" value="${data}" id="part_b_fundamental${rowIndex}" name="part_b_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },
             
              {
                targets: 5,
                data: "total_mark_fundamental",
                className: "text-center",
                render: function (data, type, full, meta) {
                 
                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                 }
                },
              },
              {
                targets: 6,
                data: "result_status_fundamental",
                //visible: false,
                render: function (data, type, full, meta) {

                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                 }
                },
              },
          
              {
                  targets: 7,
                  data: "part_A_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
    
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
             
              {
                  targets: 8,
                  data: "total_mark_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                  targets: 9,
                  data: "result_status_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                targets: 10,
                data: "part_A_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 11,
                data: "part_B_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 12,
                data: "part_C_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 13,
                data: "total_mark_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 14,
                data: "result_status_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },
              
              {
                  targets: 15,
                  data: "final_verdict_status",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                    return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                targets: 16,
                //data: "submitted_status",
                data:"endorse_exam",
                //visible: false,
                render: function (data, type, full, meta) {

                
                if (data === null || data === '') {
                  return `Not Submit`;
                
                }else if (data !== null || data !== '') {
                  return `Submitted to SEDA`;
                }
                },
              },
              {
                  targets: 17,
                  data: "id",
                  //visible: false,
                  render: function (data, type, full, meta) {

                      if (data !== null || data !== '') {
                          return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}" title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                        
                      }
                 //return data;
                  },
              },
            ],
                language: {
                info: "view _START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "view 0 from 0 to 0 entry",
                lengthMenu: "view _MENU_ Entry",
                paginate: {
                  next: "Next",
                  previous: "Previous",
                },
                zeroRecords: "No record found",
              },
          });
      }
    }else if (numColumns === 16) // template 3
      {
        if(
          (columnNames[3] === findheader.fundamental_exam_1 && columnNames[4] === findheader.fundamental_exam_2 
            && columnNames[6] === "Fundamental Result")
            && columnNames[7] === findheader.design_exam_1 && columnNames[9] === "Design Result"
            && columnNames[10] === findheader.practical_exam_1 && columnNames[12]  === "Practical Result") 
          {
            console.log('template 3 ')
            let senarai = $("#examResultUpdate").DataTable
            ({
              data: data,
              // data:dataArray,
              //  order: [[4, "desc"]],
              columnDefs: 
              [
                {
                  targets: 0,
                  data: function (row, type, full, meta) {
                  // Use meta.row to get the row index (starting from 0)
                  return meta.row + 1;
                  },
                  searchable: false,
                  orderable: false,
                },
                {
                  targets: 1,
                  data: "fullname",
                //data: "id_training_participant",
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                    
                    
                  },
                },
                {
                  targets: 2,
                  data: "exam_type",
                //data:"exam_type",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                    <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                    
                  },
                },
                {
                    targets: 3,
                    data: "part_A_fundamental",
                    render: function (data, type, full, meta) {
                      
                      var rowIndex = meta.row; // Get the row index    
                      if((data === '') || (data === null)) {                   
                      return `<input type="text" value="" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }else if(data !=''){     
                        return `<input type="text" value="${data}" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }
                    },
                  },
                {
                  targets: 4,
                  data: "part_B_fundamental",
                  render: function (data, type, full, meta) {
  
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {         
                    return `<input type="text" value="" id="part_b_fundamental${rowIndex}" name="part_b_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){   
                    return `<input type="text" value="${data}" id="part_b_fundamental${rowIndex}" name="part_b_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
               
                {
                  targets: 5,
                  data: "total_mark_fundamental",
                  className: "text-center",
                  render: function (data, type, full, meta) {
                   
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                   }
                  },
                },
                {
                  targets: 6,
                  data: "result_status_fundamental",
                  //visible: false,
                  render: function (data, type, full, meta) {
  
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                   }
                  },
                },
            
                {
                    targets: 7,
                    data: "part_A_design",
                    //visible: false,
                    render: function (data, type, full, meta) {
      
                      var rowIndex = meta.row; // Get the row index 
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
               
                {
                    targets: 8,
                    data: "total_mark_design",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
                {
                    targets: 9,
                    data: "result_status_design",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
                {
                  targets: 10,
                  data: "part_A_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
  
                {
                  targets: 11,
                  data: "total_mark_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
  
                {
                  targets: 12,
                  data: "result_status_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
                
                {
                    targets: 13,
                    data: "final_verdict_status",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }else if(data !=''){
                      return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
                {
                  targets: 14,
                  //data: "submitted_status",
                  data:"endorse_exam",
                  //visible: false,
                  render: function (data, type, full, meta) {
  
                  
                  if (data === null || data === '') {
                    return `Not Submit`;
                  
                  }else if (data !== null || data !== '') {
                    return `Submitted to SEDA`;
                  }
                  },
                },
                {
                    targets: 15,
                    data: "id",
                    //visible: false,
                    render: function (data, type, full, meta) {
  
                        if (data !== null || data !== '') {
                            return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}" title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                          
                        }
                   //return data;
                    },
                },
              ],
                  language: {
                  info: "view _START_ to _END_ from _TOTAL_ entry",
                  infoEmpty: "view 0 from 0 to 0 entry",
                  lengthMenu: "view _MENU_ Entry",
                  paginate: {
                    next: "Next",
                    previous: "Previous",
                  },
                  zeroRecords: "No record found",
                },
            });
          }
      }//endif
      else if (numColumns === 17) //template 4
      {
        if(
          (columnNames[3] === findheader.fundamental_exam_1 && columnNames[4] === findheader.fundamental_exam_2 
            && columnNames[5] === findheader.fundamental_exam_3
            && columnNames[7] === "Fundamental Result")
            && columnNames[8] === findheader.design_exam_1 && columnNames[10] === "Design Result"
            && columnNames[11] === findheader.practical_exam_1 && columnNames[13]  === "Practical Result") 
          {  console.log('template 4')
            let senarai = $("#examResultUpdate").DataTable
            ({
              data: data,
              // data:dataArray,
              //  order: [[4, "desc"]],
              columnDefs: 
              [
                {
                  targets: 0,
                  data: function (row, type, full, meta) {
                  // Use meta.row to get the row index (starting from 0)
                  return meta.row + 1;
                  },
                  searchable: false,
                  orderable: false,
                },
                {
                  targets: 1,
                  data: "fullname",
                //data: "id_training_participant",
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                    
                    
                  },
                },
                {
                  targets: 2,
                  data: "exam_type",
                //data:"exam_type",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                    <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                    
                  },
                },
                {
                    targets: 3,
                    data: "part_A_fundamental",
                    render: function (data, type, full, meta) {
                      
                      var rowIndex = meta.row; // Get the row index    
                      if((data === '') || (data === null)) {                   
                      return `<input type="text" value="" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }else if(data !=''){     
                        return `<input type="text" value="${data}" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }
                    },
                  },
                {
                  targets: 4,
                  data: "part_B_fundamental",
                  render: function (data, type, full, meta) {
  
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {         
                    return `<input type="text" value="" id="part_b_fundamental${rowIndex}" name="part_b_theory${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){   
                    return `<input type="text" value="${data}" id="part_b_fundamental${rowIndex}" name="part_b_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },

                {
                  targets: 5,
                  data: "part_C_fundamental",
                  render: function (data, type, full, meta) {
  
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {         
                    return `<input type="text" value="" id="part_c_fundamental${rowIndex}" name="part_c_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){   
                    return `<input type="text" value="${data}" id="part_c_fundamental${rowIndex}" name="part_c_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
               
                {
                  targets: 6,
                  data: "total_mark_fundamental",
                  className: "text-center",
                  render: function (data, type, full, meta) {
                   
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                   }
                  },
                },
                {
                  targets: 7,
                  data: "result_status_fundamental",
                  //visible: false,
                  render: function (data, type, full, meta) {
  
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                   }
                  },
                },
            
                {
                    targets: 8,
                    data: "part_A_design",
                    //visible: false,
                    render: function (data, type, full, meta) {
      
                      var rowIndex = meta.row; // Get the row index 
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
               
                {
                    targets: 9,
                    data: "total_mark_design",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
                {
                    targets: 10,
                    data: "result_status_design",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
                {
                  targets: 11,
                  data: "part_A_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
  
                {
                  targets: 12,
                  data: "total_mark_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
  
                {
                  targets: 13,
                  data: "result_status_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
                
                {
                    targets: 14,
                    data: "final_verdict_status",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }else if(data !=''){
                      return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
                {
                  targets: 15,
                  //data: "submitted_status",
                  data:"endorse_exam",
                  //visible: false,
                  render: function (data, type, full, meta) {
  
                  
                  if (data === null || data === '') {
                    return `Not Submit`;
                  
                  }else if (data !== null || data !== '') {
                    return `Submitted to SEDA`;
                  }
                  },
                },
                {
                    targets: 16,
                    data: "id",
                    //visible: false,
                    render: function (data, type, full, meta) {
  
                        if (data !== null || data !== '') {
                            return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}" title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                          
                        }
                   //return data;
                    },
                },
              ],
                  language: {
                  info: "view _START_ to _END_ from _TOTAL_ entry",
                  infoEmpty: "view 0 from 0 to 0 entry",
                  lengthMenu: "view _MENU_ Entry",
                  paginate: {
                    next: "Next",
                    previous: "Previous",
                  },
                  zeroRecords: "No record found",
                },
            });

          }
      }else if (numColumns === 18) //template 5
      {
        if(
          (columnNames[3] === findheader.fundamental_exam_1 && columnNames[4] === findheader.fundamental_exam_2 
            && columnNames[5] === findheader.fundamental_exam_3
            && columnNames[7] === "Fundamental Result")
            && columnNames[8] === findheader.design_exam_1 && columnNames[9] === findheader.design_exam_2 && columnNames[11] === "Design Result"
            && columnNames[12] === findheader.practical_exam_1 && columnNames[14]  === "Practical Result")  
            {  console.log('template 5')
            let senarai = $("#examResultUpdate").DataTable
            ({
              data: data,
              // data:dataArray,
              //  order: [[4, "desc"]],
              columnDefs: 
              [
                {
                  targets: 0,
                  data: function (row, type, full, meta) {
                  // Use meta.row to get the row index (starting from 0)
                  return meta.row + 1;
                  },
                  searchable: false,
                  orderable: false,
                },
                {
                  targets: 1,
                  data: "fullname",
                //data: "id_training_participant",
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                    
                    
                  },
                },
                {
                  targets: 2,
                  data: "exam_type",
                //data:"exam_type",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                    <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                    
                  },
                },
                {
                    targets: 3,
                    data: "part_A_fundamental",
                    render: function (data, type, full, meta) {
                      
                      var rowIndex = meta.row; // Get the row index    
                      if((data === '') || (data === null)) {                   
                      return `<input type="text" value="" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }else if(data !=''){     
                        return `<input type="text" value="${data}" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }
                    },
                  },
                {
                  targets: 4,
                  data: "part_B_fundamental",
                  render: function (data, type, full, meta) {
  
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {         
                    return `<input type="text" value="" id="part_b_fundamental${rowIndex}" name="part_b_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){   
                    return `<input type="text" value="${data}" id="part_b_fundamental${rowIndex}" name="part_b_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
                {
                  targets: 5,
                  data: "part_C_fundamental",
                  render: function (data, type, full, meta) {
  
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {         
                    return `<input type="text" value="" id="part_c_fundamental${rowIndex}" name="part_c_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){   
                    return `<input type="text" value="${data}" id="part_c_fundamental${rowIndex}" name="part_c_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
               
                {
                  targets: 6,
                  data: "total_mark_fundamental",
                  className: "text-center",
                  render: function (data, type, full, meta) {
                   
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                   }
                  },
                },
                {
                  targets: 7,
                  data: "result_status_fundamental",
                  //visible: false,
                  render: function (data, type, full, meta) {
  
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                   }
                  },
                },
            
                {
                    targets: 8,
                    data: "part_A_design",
                    //visible: false,
                    render: function (data, type, full, meta) {
      
                      var rowIndex = meta.row; // Get the row index 
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },

                {
                  targets: 9,
                  data: "part_B_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
    
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_b_design${rowIndex}" name="part_b_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_b_design${rowIndex}" name="part_b_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
               
                {
                    targets: 10,
                    data: "total_mark_design",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
                {
                    targets: 11,
                    data: "result_status_design",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
                {
                  targets: 12,
                  data: "part_A_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
  
                {
                  targets: 13,
                  data: "total_mark_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
  
                {
                  targets: 14,
                  data: "result_status_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
                
                {
                    targets: 15,
                    data: "final_verdict_status",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }else if(data !=''){
                      return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
                {
                  targets: 16,
                  //data: "submitted_status",
                  data:"endorse_exam",
                  //visible: false,
                  render: function (data, type, full, meta) {
  
                  
                  if (data === null || data === '') {
                    return `Not Submit`;
                  
                  }else if (data !== null || data !== '') {
                    return `Submitted to SEDA`;
                  }
                  },
                },
                {
                    targets: 17,
                    data: "id",
                    //visible: false,
                    render: function (data, type, full, meta) {
  
                        if (data !== null || data !== '') {
                            return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}" title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                          
                        }
                   //return data;
                    },
                },
              ],
                  language: {
                  info: "view _START_ to _END_ from _TOTAL_ entry",
                  infoEmpty: "view 0 from 0 to 0 entry",
                  lengthMenu: "view _MENU_ Entry",
                  paginate: {
                    next: "Next",
                    previous: "Previous",
                  },
                  zeroRecords: "No record found",
                },
            });
          }
        }//endifelse if (numColumns === 17) 
        else if (numColumns === 21) //template 6
        { 
          if(
            (columnNames[3] === findheader.fundamental_exam_1 && columnNames[4] === findheader.fundamental_exam_2 
              && columnNames[5] === findheader.fundamental_exam_3 && columnNames[7] === "Fundamental Result")
              && columnNames[8] === findheader.design_exam_1 
              && columnNames[9] === findheader.design_exam_2
              && columnNames[10] === findheader.design_exam_3
              && columnNames[12] === "Design Result"
              && columnNames[13] === findheader.practical_exam_1 && columnNames[14] === findheader.practical_exam_2 
              && columnNames[15] === findheader.practical_exam_3 && columnNames[17]  === "Practical Result")  
            { console.log('template 6');
              let senarai = $("#examResultUpdate").DataTable
              ({
                data: data,
                // data:dataArray,
                //  order: [[4, "desc"]],
                columnDefs: 
                [
                  {
                    targets: 0,
                    data: function (row, type, full, meta) {
                    // Use meta.row to get the row index (starting from 0)
                    return meta.row + 1;
                    },
                    searchable: false,
                    orderable: false,
                  },
                  {
                    targets: 1,
                    data: "fullname",
                  //data: "id_training_participant",
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row; // Get the row index  
                      return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                      
                      
                    },
                  },
                  {
                    targets: 2,
                    data: "exam_type",
                  //data:"exam_type",
                    render: function (data, type, full, meta) {
                      
                      var rowIndex = meta.row; // Get the row index  
                      return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                      <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                      
                    },
                  },
                  {
                      targets: 3,
                      data: "part_A_fundamental",
                      render: function (data, type, full, meta) {
                        
                        var rowIndex = meta.row; // Get the row index    
                        if((data === '') || (data === null)) {                   
                        return `<input type="text" value="" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                        }else if(data !=''){     
                          return `<input type="text" value="${data}" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                        }
                      },
                    },
                  {
                    targets: 4,
                    data: "part_B_fundamental",
                    render: function (data, type, full, meta) {
    
                      var rowIndex = meta.row; // Get the row index 
                      if((data === '') || (data === null)) {         
                      return `<input type="text" value="" id="part_b_fundamental${rowIndex}" name="part_b_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){   
                      return `<input type="text" value="${data}" id="part_b_fundamental${rowIndex}" name="part_b_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                  },

                  {
                    targets: 5,
                    data: "part_C_fundamental",
                    render: function (data, type, full, meta) {
    
                      var rowIndex = meta.row; // Get the row index 
                      if((data === '') || (data === null)) {         
                      return `<input type="text" value="" id="part_c_fundamental${rowIndex}" name="part_c_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){   
                      return `<input type="text" value="${data}" id="part_c_fundamental${rowIndex}" name="part_c_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                  },
                 
                  {
                    targets: 6,
                    data: "total_mark_fundamental",
                    className: "text-center",
                    render: function (data, type, full, meta) {
                     
                      var rowIndex = meta.row; // Get the row index 
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                     }
                    },
                  },
                  {
                    targets: 7,
                    data: "result_status_fundamental",
                    //visible: false,
                    render: function (data, type, full, meta) {
    
                      var rowIndex = meta.row; // Get the row index 
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }else if(data !=''){
                        return `<input type="text" value="${data}" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                     }
                    },
                  },
              
                  {
                      targets: 8,
                      data: "part_A_design",
                      //visible: false,
                      render: function (data, type, full, meta) {
        
                        var rowIndex = meta.row; // Get the row index 
                        if((data === '') || (data === null)) {
                        return `<input type="text" value="" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                        oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }else if(data !=''){
                        return `<input type="text" value="${data}" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                        oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }
                      },
                  },

                  {
                    targets: 9,
                    data: "part_B_design",
                    //visible: false,
                    render: function (data, type, full, meta) {
      
                      var rowIndex = meta.row; // Get the row index 
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="part_b_design${rowIndex}" name="part_b_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="part_b_design${rowIndex}" name="part_b_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },

                {
                  targets: 10,
                  data: "part_C_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
    
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_c_design${rowIndex}" name="part_c_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_c_design${rowIndex}" name="part_c_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
                 
                  {
                      targets: 11,
                      data: "total_mark_design",
                      //visible: false,
                      render: function (data, type, full, meta) {
                        var rowIndex = meta.row;
                        if((data === '') || (data === null)) {
                        return `<input type="text" value="" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                        oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }else if(data !=''){
                        return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                        oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }
                      },
                  },
                  {
                      targets: 12,
                      data: "result_status_design",
                      //visible: false,
                      render: function (data, type, full, meta) {
                        var rowIndex = meta.row;
                        if((data === '') || (data === null)) {
                        return `<input type="text" value="" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }else if(data !=''){
                        return `<input type="text" value="${data}" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }
                      },
                  },
                  {
                    targets: 13,
                    data: "part_A_practical",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                  },
    
                  {
                    targets: 14,
                    data: "part_B_practical",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="part_b_practical${rowIndex}" name="part_b_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="part_b_practical${rowIndex}" name="part_b_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                  },
    
                  {
                    targets: 15,
                    data: "part_C_practical",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                  },
    
                  {
                    targets: 16,
                    data: "total_mark_practical",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                  },
    
                  {
                    targets: 17,
                    data: "result_status_practical",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                  },
                  
                  {
                      targets: 18,
                      data: "final_verdict_status",
                      //visible: false,
                      render: function (data, type, full, meta) {
                        var rowIndex = meta.row;
                        if((data === '') || (data === null)) {
                        return `<input type="text" value="" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                        }else if(data !=''){
                        return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }
                      },
                  },
                  {
                    targets: 19,
                    //data: "submitted_status",
                    data:"endorse_exam",
                    //visible: false,
                    render: function (data, type, full, meta) {
    
                    
                    if (data === null || data === '') {
                      return `Not Submit`;
                    
                    }else if (data !== null || data !== '') {
                      return `Submitted to SEDA`;
                    }
                    },
                  },
                  {
                      targets: 20,
                      data: "id",
                      //visible: false,
                      render: function (data, type, full, meta) {
    
                          if (data !== null || data !== '') {
                              return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}" title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                            
                          }
                     //return data;
                      },
                  },
                ],
                    language: {
                    info: "view _START_ to _END_ from _TOTAL_ entry",
                    infoEmpty: "view 0 from 0 to 0 entry",
                    lengthMenu: "view _MENU_ Entry",
                    paginate: {
                      next: "Next",
                      previous: "Previous",
                    },
                    zeroRecords: "No record found",
                  },
              });
            
            }
        }
          
};

    

       

      /////////////Scenario
     

       ////////
      
//have to start new 
       let tableExamNameList2 = (data) => {
  
       if (numColumns === 19) //template 7
       { 
         if(
         (columnNames[3] === findheader.fundamental_exam_1 && columnNames[5] === "Fundamental Result")
          && columnNames[6] === findheader.design_exam_1 
          && columnNames[7] === findheader.design_exam_2
          && columnNames[8] === findheader.design_exam_3
          && columnNames[10] === "Design Result"
          && columnNames[11] === findheader.practical_exam_1 && columnNames[12] === findheader.practical_exam_2 
          && columnNames[13] === findheader.practical_exam_3 && columnNames[15]  === "Practical Result") 
           { console.log('template 7');
             let senarai = $("#examResultUpdate").DataTable
             ({
              data: data,
              // data:dataArray,
              //  order: [[4, "desc"]],
              columnDefs: 
              [
                {
                  targets: 0,
                  data: function (row, type, full, meta) {
                  // Use meta.row to get the row index (starting from 0)
                  return meta.row + 1;
                  },
                  searchable: false,
                  orderable: false,
                },
                {
                  targets: 1,
                  data: "fullname",
                //data: "id_training_participant",
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                    
                    
                  },
                },
                {
                  targets: 2,
                  data: "exam_type",
                //data:"exam_type",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index  
                    return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                    <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                    
                  },
                },
                {
                    targets: 3,
                    data: "part_A_fundamental",
                    render: function (data, type, full, meta) {
                      
                      var rowIndex = meta.row; // Get the row index    
                      if((data === '') || (data === null)) {                   
                      return `<input type="text" value="" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }else if(data !=''){     
                        return `<input type="text" value="${data}" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }
                    },
                  },
              
                {
                  targets: 4,
                  data: "total_mark_fundamental",
                  className: "text-center",
                  render: function (data, type, full, meta) {
                   
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                   }
                  },
                },
                {
                  targets: 5,
                  data: "result_status_fundamental",
                  //visible: false,
                  render: function (data, type, full, meta) {
  
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                   }
                  },
                },
            
                {
                    targets: 6,
                    data: "part_A_design",
                    //visible: false,
                    render: function (data, type, full, meta) {
      
                      var rowIndex = meta.row; // Get the row index 
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },

                {
                  targets: 7,
                  data: "part_B_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
    
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_b_design${rowIndex}" name="part_b_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_b_design${rowIndex}" name="part_b_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },

              {
                targets: 8,
                data: "part_C_design",
                //visible: false,
                render: function (data, type, full, meta) {
  
                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_c_design${rowIndex}" name="part_c_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_c_design${rowIndex}" name="part_c_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
            },
               
                {
                    targets: 9,
                    data: "total_mark_design",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                      oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
                {
                    targets: 10,
                    data: "result_status_design",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                      return `<input type="text" value="${data}" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
                {
                  targets: 11,
                  data: "part_A_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
  
                {
                  targets: 12,
                  data: "part_B_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_b_practical${rowIndex}" name="part_b_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_b_practical${rowIndex}" name="part_b_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
  
                {
                  targets: 13,
                  data: "part_C_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
  
                {
                  targets: 14,
                  data: "total_mark_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
  
                {
                  targets: 15,
                  data: "result_status_practical",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
                },
                
                {
                    targets: 16,
                    data: "final_verdict_status",
                    //visible: false,
                    render: function (data, type, full, meta) {
                      var rowIndex = meta.row;
                      if((data === '') || (data === null)) {
                      return `<input type="text" value="" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                      }else if(data !=''){
                      return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                    },
                },
                {
                  targets: 17,
                  //data: "submitted_status",
                  data:"endorse_exam",
                  //visible: false,
                  render: function (data, type, full, meta) {
  
                  
                  if (data === null || data === '') {
                    return `Not Submit`;
                  
                  }else if (data !== null || data !== '') {
                    return `Submitted to SEDA`;
                  }
                  },
                },
                {
                    targets: 18,
                    data: "id",
                    //visible: false,
                    render: function (data, type, full, meta) {
  
                        if (data !== null || data !== '') {
                            return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}" title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                          
                        }
                   //return data;
                    },
                },
              ],
                  language: {
                  info: "view _START_ to _END_ from _TOTAL_ entry",
                  infoEmpty: "view 0 from 0 to 0 entry",
                  lengthMenu: "view _MENU_ Entry",
                  paginate: {
                    next: "Next",
                    previous: "Previous",
                  },
                  zeroRecords: "No record found",
                },
            });
           
          }
       } else if (numColumns === 20) //template 8 
       {
       if(
        (columnNames[3] === findheader.fundamental_exam_1 && columnNames[4] === findheader.fundamental_exam_2 	&& columnNames[6] === "Fundamental Result")
          && columnNames[7] === findheader.design_exam_1 
 	        && columnNames[8] === findheader.design_exam_2
	        && columnNames[9] === findheader.design_exam_3
	        && columnNames[11] === "Design Result"
          && columnNames[12] === findheader.practical_exam_1 && columnNames[12] === findheader.practical_exam_2 
          && columnNames[13] === findheader.practical_exam_3 && columnNames[16]  === "Practical Result")  
         { console.log('template 8');
          let senarai = $("#examResultUpdate").DataTable
          ({
            data: data,
            // data:dataArray,
            //  order: [[4, "desc"]],
            columnDefs: 
            [
              {
                targets: 0,
                data: function (row, type, full, meta) {
                // Use meta.row to get the row index (starting from 0)
                return meta.row + 1;
                },
                searchable: false,
                orderable: false,
              },
              {
                targets: 1,
                data: "fullname",
              //data: "id_training_participant",
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                  
                },
              },
              {
                targets: 2,
                data: "exam_type",
              //data:"exam_type",
                render: function (data, type, full, meta) {
                  
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                  <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                },
              },
              {
                  targets: 3,
                  data: "part_A_fundamental",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index    
                    if((data === '') || (data === null)) {                   
                    return `<input type="text" value="" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){     
                      return `<input type="text" value="${data}" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                  },
                },
              {
                targets: 4,
                data: "part_B_fundamental",
                render: function (data, type, full, meta) {

                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {         
                  return `<input type="text" value="" id="part_b_fundamental${rowIndex}" name="part_b_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){   
                  return `<input type="text" value="${data}" id="part_b_fundamental${rowIndex}" name="part_b_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },
             
              {
                targets: 5,
                data: "total_mark_fundamental",
                className: "text-center",
                render: function (data, type, full, meta) {
                 
                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                 }
                },
              },
              {
                targets: 6,
                data: "result_status_fundamental",
                //visible: false,
                render: function (data, type, full, meta) {

                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                 }
                },
              },
          
              {
                  targets: 7,
                  data: "part_A_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
    
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },

              {
                targets: 8,
                data: "part_B_design",
                //visible: false,
                render: function (data, type, full, meta) {
  
                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_b_design${rowIndex}" name="part_b_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_b_design${rowIndex}" name="part_b_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
            },

            {
              targets: 9,
              data: "part_C_design",
              //visible: false,
              render: function (data, type, full, meta) {

                var rowIndex = meta.row; // Get the row index 
                if((data === '') || (data === null)) {
                return `<input type="text" value="" id="part_c_design${rowIndex}" name="part_c_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
              }else if(data !=''){
                return `<input type="text" value="${data}" id="part_c_design${rowIndex}" name="part_c_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
              }
              },
          },
             
              {
                  targets: 10,
                  data: "total_mark_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                  targets: 11,
                  data: "result_status_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                targets: 12,
                data: "part_A_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 13,
                data: "part_B_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_b_practical${rowIndex}" name="part_b_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_b_practical${rowIndex}" name="part_b_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 14,
                data: "part_C_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 15,
                data: "total_mark_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 16,
                data: "result_status_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },
              
              {
                  targets: 17,
                  data: "final_verdict_status",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                    return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                targets: 18,
                //data: "submitted_status",
                data:"endorse_exam",
                //visible: false,
                render: function (data, type, full, meta) {

                
                if (data === null || data === '') {
                  return `Not Submit`;
                
                }else if (data !== null || data !== '') {
                  return `Submitted to SEDA`;
                }
                },
              },
              {
                  targets: 19,
                  data: "id",
                  //visible: false,
                  render: function (data, type, full, meta) {

                      if (data !== null || data !== '') {
                          return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}" title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                        
                      }
                 //return data;
                  },
              },
            ],
                language: {
                info: "view _START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "view 0 from 0 to 0 entry",
                lengthMenu: "view _MENU_ Entry",
                paginate: {
                  next: "Next",
                  previous: "Previous",
                },
                zeroRecords: "No record found",
              },
          });
        
        }
       }//endif
      
      }


//have to start new
let tableExamNameList3 = (data) => {

      if (numColumns === 17) //template 9 
      {
        if(
          (columnNames[3] === findheader.fundamental_exam_1 &&  columnNames[5] === "Fundamental Result")
          && columnNames[6] === findheader.design_exam_1 
          && columnNames[8] === "Design Result"
          && columnNames[9] === findheader.practical_exam_1 && columnNames[10] === findheader.practical_exam_2
          && columnNames[11] === findheader.practical_exam_3 && columnNames[13]  === "Practical Result")  
        { console.log('template 9');
         let senarai = $("#examResultUpdate").DataTable
         ({
          data: data,
          // data:dataArray,
          //  order: [[4, "desc"]],
          columnDefs: 
          [
            {
              targets: 0,
              data: function (row, type, full, meta) {
              // Use meta.row to get the row index (starting from 0)
              return meta.row + 1;
              },
              searchable: false,
              orderable: false,
            },
            {
              targets: 1,
              data: "fullname",
            //data: "id_training_participant",
              render: function (data, type, full, meta) {
                var rowIndex = meta.row; // Get the row index  
                return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                
                
              },
            },
            {
              targets: 2,
              data: "exam_type",
            //data:"exam_type",
              render: function (data, type, full, meta) {
                
                var rowIndex = meta.row; // Get the row index  
                return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                
              },
            },
            {
                targets: 3,
                data: "part_A_fundamental",
                render: function (data, type, full, meta) {
                  
                  var rowIndex = meta.row; // Get the row index    
                  if((data === '') || (data === null)) {                   
                  return `<input type="text" value="" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){     
                    return `<input type="text" value="${data}" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                },
            },
          
            {
              targets: 4,
              data: "total_mark_fundamental",
              className: "text-center",
              render: function (data, type, full, meta) {
               
                var rowIndex = meta.row; // Get the row index 
                if((data === '') || (data === null)) {
                return `<input type="text" value="" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
              }else if(data !=''){
                return `<input type="text" value="${data}" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
               }
              },
            },
            {
              targets: 5,
              data: "result_status_fundamental",
              //visible: false,
              render: function (data, type, full, meta) {

                var rowIndex = meta.row; // Get the row index 
                if((data === '') || (data === null)) {
                return `<input type="text" value="" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
               }
              },
            },
        
            {
                targets: 6,
                data: "part_A_design",
                //visible: false,
                render: function (data, type, full, meta) {
  
                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
            },
           
            {
                targets: 7,
                data: "total_mark_design",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
            },
            {
                targets: 8,
                data: "result_status_design",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
            },
            {
              targets: 9,
              data: "part_A_practical",
              //visible: false,
              render: function (data, type, full, meta) {
                var rowIndex = meta.row;
                if((data === '') || (data === null)) {
                return `<input type="text" value="" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
              }else if(data !=''){
                return `<input type="text" value="${data}" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
              }
              },
            },

            {
              targets: 10,
              data: "part_B_practical",
              //visible: false,
              render: function (data, type, full, meta) {
                var rowIndex = meta.row;
                if((data === '') || (data === null)) {
                return `<input type="text" value="" id="part_b_practical${rowIndex}" name="part_b_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
              }else if(data !=''){
                return `<input type="text" value="${data}" id="part_b_practical${rowIndex}" name="part_b_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
              }
              },
            },

            {
              targets: 11,
              data: "part_C_practical",
              //visible: false,
              render: function (data, type, full, meta) {
                var rowIndex = meta.row;
                if((data === '') || (data === null)) {
                return `<input type="text" value="" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
              }else if(data !=''){
                return `<input type="text" value="${data}" id="part_c_practical${rowIndex}" name="part_c_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
              }
              },
            },

            {
              targets: 12,
              data: "total_mark_practical",
              //visible: false,
              render: function (data, type, full, meta) {
                var rowIndex = meta.row;
                if((data === '') || (data === null)) {
                return `<input type="text" value="" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
              }else if(data !=''){
                return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
              }
              },
            },

            {
              targets: 13,
              data: "result_status_practical",
              //visible: false,
              render: function (data, type, full, meta) {
                var rowIndex = meta.row;
                if((data === '') || (data === null)) {
                return `<input type="text" value="" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
              }else if(data !=''){
                return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
              }
              },
            },
            
            {
                targets: 14,
                data: "final_verdict_status",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                  return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
            },
            {
              targets: 15,
              //data: "submitted_status",
              data:"endorse_exam",
              //visible: false,
              render: function (data, type, full, meta) {

              
              if (data === null || data === '') {
                return `Not Submit`;
              
              }else if (data !== null || data !== '') {
                return `Submitted to SEDA`;
              }
              },
            },
            {
                targets: 16,
                data: "id",
                //visible: false,
                render: function (data, type, full, meta) {

                    if (data !== null || data !== '') {
                        return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}" title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                      
                    }
               //return data;
                },
            },
          ],
              language: {
              info: "view _START_ to _END_ from _TOTAL_ entry",
              infoEmpty: "view 0 from 0 to 0 entry",
              lengthMenu: "view _MENU_ Entry",
              paginate: {
                next: "Next",
                previous: "Previous",
              },
              zeroRecords: "No record found",
            },
        });
       
       }
      }  if (numColumns === 16) //template 10 
      {
        if(
          (columnNames[3] === findheader.fundamental_exam_1 &&  columnNames[5] === "Fundamental Result")
          && columnNames[6] === findheader.design_exam_1 
          && columnNames[8] === "Design Result"
          && columnNames[9] === findheader.practical_exam_1 && columnNames[10] === findheader.practical_exam_2
          && columnNames[12]  === "Practical Result")  
        {  console.log('template 10')
          let senarai = $("#examResultUpdate").DataTable
          ({
            data: data,
            // data:dataArray,
            //  order: [[4, "desc"]],
            columnDefs: 
            [
              {
                targets: 0,
                data: function (row, type, full, meta) {
                // Use meta.row to get the row index (starting from 0)
                return meta.row + 1;
                },
                searchable: false,
                orderable: false,
              },
              {
                targets: 1,
                data: "fullname",
              //data: "id_training_participant",
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                  
                },
              },
              {
                targets: 2,
                data: "exam_type",
              //data:"exam_type",
                render: function (data, type, full, meta) {
                  
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                  <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                },
              },
              {
                  targets: 3,
                  data: "part_A_fundamental",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index    
                    if((data === '') || (data === null)) {                   
                    return `<input type="text" value="" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){     
                      return `<input type="text" value="${data}" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                  },
                },
             
              {
                targets: 4,
                data: "total_mark_fundamental",
                className: "text-center",
                render: function (data, type, full, meta) {
                 
                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                 }
                },
              },
              {
                targets: 5,
                data: "result_status_fundamental",
                //visible: false,
                render: function (data, type, full, meta) {

                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                 }
                },
              },
          
              {
                  targets: 6,
                  data: "part_A_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
    
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
             
              {
                  targets: 7,
                  data: "total_mark_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                  targets: 8,
                  data: "result_status_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                targets: 9,
                data: "part_A_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 10,
                data: "part_B_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_b_practical${rowIndex}" name="part_b_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_b_practical${rowIndex}" name="part_b_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 12,
                data: "total_mark_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 13,
                data: "result_status_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },
              
              {
                  targets: 14,
                  data: "final_verdict_status",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                    return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                targets: 15,
                //data: "submitted_status",
                data:"endorse_exam",
                //visible: false,
                render: function (data, type, full, meta) {

                
                if (data === null || data === '') {
                  return `Not Submit`;
                
                }else if (data !== null || data !== '') {
                  return `Submitted to SEDA`;
                }
                },
              },
              {
                  targets: 16,
                  data: "id",
                  //visible: false,
                  render: function (data, type, full, meta) {

                      if (data !== null || data !== '') {
                          return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}" title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                        
                      }
                 //return data;
                  },
              },
            ],
                language: {
                info: "view _START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "view 0 from 0 to 0 entry",
                lengthMenu: "view _MENU_ Entry",
                paginate: {
                  next: "Next",
                  previous: "Previous",
                },
                zeroRecords: "No record found",
              },
          });
        }

      }if (numColumns === 15) //template 11 
      {
        if(
          (columnNames[3] === findheader.fundamental_exam_1 &&  columnNames[5] === "Fundamental Result")
          && columnNames[6] === findheader.design_exam_1 
          && columnNames[8] === "Design Result"
          && columnNames[9] === findheader.practical_exam_1 
          && columnNames[11]  === "Practical Result") 
        { console.log('template 11');
          let senarai = $("#examResultUpdate").DataTable
          ({
            data: data,
            // data:dataArray,
            //  order: [[4, "desc"]],
            columnDefs: 
            [
              {
                targets: 0,
                data: function (row, type, full, meta) {
                // Use meta.row to get the row index (starting from 0)
                return meta.row + 1;
                },
                searchable: false,
                orderable: false,
              },
              {
                targets: 1,
                data: "fullname",
              //data: "id_training_participant",
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training_participant}" id="id_training_participant${rowIndex}" name="id_training_participant${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                  
                },
              },
              {
                targets: 2,
                data: "exam_type",
              //data:"exam_type",
                render: function (data, type, full, meta) {
                  
                  var rowIndex = meta.row; // Get the row index  
                  return `${data}<input type="hidden" value="${full.id_training}" id="id_training${rowIndex}" name="id_training${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">
                  <input type="hidden" value="${full.id_participant_examresult}" id="id_participant_examresult${rowIndex}" name="id_participant_examresult${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})">`;
                  
                },
              },
              {
                  targets: 3,
                  data: "part_A_fundamental",
                  render: function (data, type, full, meta) {
                    
                    var rowIndex = meta.row; // Get the row index    
                    if((data === '') || (data === null)) {                   
                    return `<input type="text" value="" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){     
                      return `<input type="text" value="${data}" id="part_a_fundamental${rowIndex}" name="part_a_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center" oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }
                  },
                },
             
              {
                targets: 4,
                data: "total_mark_fundamental",
                className: "text-center",
                render: function (data, type, full, meta) {
                 
                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="total_mark_fundamental${rowIndex}" name="total_mark_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"
                  oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                 }
                },
              },
              {
                targets: 5,
                data: "result_status_fundamental",
                //visible: false,
                render: function (data, type, full, meta) {

                  var rowIndex = meta.row; // Get the row index 
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_fundamental${rowIndex}" name="result_status_fundamental${rowIndex}" style="width: 60px;height:40px;text-align:center"  onblur="MarkAutoSave(${rowIndex})" disabled>`;
                 }
                },
              },
          
              {
                  targets: 6,
                  data: "part_A_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
    
                    var rowIndex = meta.row; // Get the row index 
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="part_a_design${rowIndex}" name="part_a_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
             
              {
                  targets: 7,
                  data: "total_mark_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="total_mark_design${rowIndex}" name="total_mark_design${rowIndex}" style="width: 60px;height:40px;text-align:center" 
                    oninput="validateInput(this)" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                  targets: 8,
                  data: "result_status_design",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }else if(data !=''){
                    return `<input type="text" value="${data}" id="result_status_design${rowIndex}" name="result_status_design${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                targets: 9,
                data: "part_A_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="part_a_practical${rowIndex}" name="part_a_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 10,
                data: "total_mark_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="total_mark_practical${rowIndex}" name="total_mark_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },

              {
                targets: 11,
                data: "result_status_practical",
                //visible: false,
                render: function (data, type, full, meta) {
                  var rowIndex = meta.row;
                  if((data === '') || (data === null)) {
                  return `<input type="text" value="" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }else if(data !=''){
                  return `<input type="text" value="${data}" id="result_status_practical${rowIndex}" name="result_status_practical${rowIndex}" style="width: 60px;height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                }
                },
              },
              
              {
                  targets: 12,
                  data: "final_verdict_status",
                  //visible: false,
                  render: function (data, type, full, meta) {
                    var rowIndex = meta.row;
                    if((data === '') || (data === null)) {
                    return `<input type="text" value="" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                    }else if(data !=''){
                    return `<input type="text" value="${data}" id="final_verdict_status${rowIndex}" name="final_verdict_status${rowIndex}" style="width: 60px; height:40px;text-align:center" onblur="MarkAutoSave(${rowIndex})" disabled>`;
                  }
                  },
              },
              {
                targets: 13,
                //data: "submitted_status",
                data:"endorse_exam",
                //visible: false,
                render: function (data, type, full, meta) {

                
                if (data === null || data === '') {
                  return `Not Submit`;
                
                }else if (data !== null || data !== '') {
                  return `Submitted to SEDA`;
                }
                },
              },
              {
                  targets: 14,
                  data: "id",
                  //visible: false,
                  render: function (data, type, full, meta) {

                      if (data !== null || data !== '') {
                          return `<a class="btn btn-sm" style="display: inline-table;"  id="editResult" data-id="${data}" title="Update result"><i class="fas fa-edit" style="font-size: 18px; color: #3498DB;"></i></a>`;    
                        
                      }
                 //return data;
                  },
              },
            ],
                language: {
                info: "view _START_ to _END_ from _TOTAL_ entry",
                infoEmpty: "view 0 from 0 to 0 entry",
                lengthMenu: "view _MENU_ Entry",
                paginate: {
                  next: "Next",
                  previous: "Previous",
                },
                zeroRecords: "No record found",
              },
          });
        }

      }
    }
//////////////////////////

       
let queryExamResult = () => {

    $("#QueryBtn").on("click", () => {
        $("#confirmationQueryModal").modal('show');
    });
      

    // $(document).on('click', '#EndorseBtn', function() {
    //   //  alert('k');
    

    //     //$('#edit-result-modal').modal('hide');
    //     $('#confirmationEndorseModal').modal('show');
    
    // });
   
    $("#submitQuery").on("click", () => {
        $("#confirmationQueryModal").modal("hide");
       
        let editExamResult = host + '/api/exam/editExamResultQuery';
        
  
      const data = {

        Batch_No : $('#formQueryResult input[name="batch_no_query"]').val(),
        Id_course : $('#formQueryResult input[name="id_training_course"]').val(),
        
          };
        //console.log(data);
        let postData = $.ajax({
          url: editExamResult,
          method: "PUT",
          data: data,
        }) .done((res) => {
         console.log('data',res);
  
        //   setTimeout(() => {
  
        //     $("#loadingModal").modal("hide");
        //     $("#berjaya-simpan-modal").modal({
        //         backdrop: 'static',
        //         keyboard: false,
        //     });
        //     $("#loadingModal").modal("hide");
  
        // }, 2000);
  
  
        })
        .catch((err) => {
          //console.log(err);
        });
        $.when(postData).done( (res) => {
        //  $('#berjaya-kemaskini-modal').modal('show');
          let url = window.location.pathname;
          let ID = url.substring(url.lastIndexOf('/') + 1);
         //  window.location.href = `${host}/exam/endorseExamResult/${ID}`;
  
       })
  
  
      });
  
 }


