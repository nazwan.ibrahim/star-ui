let url = window.location.pathname;
let inventoryID = url.substring(url.lastIndexOf('/') + 1);
$(function () {
  getTableDirektori();
})

function getTableDirektori() {
   // inventoryID =   $(this).data("id");
    

    let getInventoryList = `${host}/api/book/${inventoryID}/bookInfo`;
  $.ajax({
      type: 'GET',
      url: getInventoryList,
    })
    .done(res => {
      let dataBook = res.bookInventory;

      $.each(dataBook, function(index, value) {

        (value.book_title == null || value.book_title == '') ? $('#BookTitle').val('n/a') : $('#BookTitle').val(value.book_title);
        (value.book_description == null || value.book_description == '') ? $('#BookDescription').val('n/a') : $('#BookDescription').val(value.book_description);
        (value.book_code == null || value.book_code == '') ? $('#BookCode').val('n/a') : $('#BookCode').val(value.book_code);
        (value.online_price == null || value.online_price == '') ? $('#SellPrice').val('n/a') : $('#SellPrice').val(value.online_price);
        (value.walkin_price == null || value.walkin_price == '') ? $('#WalkInPrice').val('n/a') : $('#WalkInPrice').val(value.walkin_price);
        (value.special_price == null || value.special_price == '') ? $('#SpecialPrice').val('n/a') : $('#SpecialPrice').val(value.special_price);
        (value.stock_in == null || value.stock_in == '') ? $('#StockIn').val('n/a') : $('#StockIn').val(value.stock_in);
        (value.stock_out == null || value.stock_out == '') ? $('#StockOut').val('n/a') : $('#StockOut').val(value.stock_out);
        (value.stock_reserve == null || value.stock_reserve == '') ? $('#QuantityReserve').val('n/a') : $('#QuantityReserve').val(value.stock_reserve);
        (value.reserve_remark == null || value.reserve_remark == '') ? $('#ReserveRemark').val('n/a') : $('#ReserveRemark').val(value.reserve_remark);
      
     // tableMaklumatDirektori(res);

      })

      dataBook.forEach((el) => {
        getBookInfo(el);
      });

    }).catch(err => {
      console.log(err);
    });
}


function getBookInfo(data) {
          
          let stock_balance;
          if((data.stock_reserve =='')||(data.stock_reserve == 0)){
            stock_balance = (data.stock_in - data.stock_out);
          }
            if(data.stock_reserve !==''){
            stock_balance = (data.stock_in - (data.stock_out + data.stock_reserve));
            }

            //console.log(stock_balance);
            if (!data.cover_name) {
                $('#BookCoverImg').empty().append('<div class="text-center mt-2">No Book Cover Yet</div>');
            } else {
                $('#BookCoverImg').html(`<img src="data:image/${data.file_format};base64,${data.file_cover}" height="120" width="120"">`);
            }

            if (!data.book_title) {
                $('#booktitle').html('-');
            } else {
                $('#booktitle').html(data.book_title);
            }

            if (!data.book_description) {
                $('#bookdetails').html('-');
            } else {
                $('#bookdetails').html(data.book_description);
            }

            if (!data.stock_in) {
                $('#stockin').html('-');
            } else {
                $('#stockin').html(data.stock_in);
            }

            if (!data.stock_out) {
                $('#stockout').html('-');
            } else {
                $('#stockout').html(data.stock_out);
            }

            if (!stock_balance) {
                $('#stockbalance').html('0');
                $('#stockbalanceInput').val(0);
            } else {
                $('#stockbalance').html(stock_balance);
                $('#stockbalanceInput').val(stock_balance);
                
            }
}


var maxFileSize = 8000000; //8mb
var isErrorFileType = false;
var isErrorFileSize = false;
var uploadedFilesArray = [];
var uploadedFilesArray1 = [];
var uploadedEditedFilesArray = [];
var dataForm = new FormData();


indexCount = 0;
let jenisCenderamataData;
let dataPembekal;

$(function () {

  $('.deleteBtn').hide();
})

function uploadButton() {
  $('#uploadCoverImg-0').val(null);
  $('#uploadCoverImg-0').trigger('click');
}

function onChangeUploadFile() {
  // reset
  isErrorFileType = false;
  isErrorFileSize = false;
  uploadedFilesArray = [];
  var allowedExtensions = ["jpg", "jpeg", "png", "img"];

  $.each($('#uploadCoverImg-0')[0].files, function (index, val) {
    uploadedFilesArray.push(this);

    var fileExtension = val.name.split('.').pop().toLowerCase();
    var isValidExtension = allowedExtensions.includes(fileExtension);

    // if (val.type != "image/jpeg" && val.type != "image/jpg" && val.type != "image/png") {
    //   isErrorFileType = true;
    // }

    if (!isValidExtension) {
      isErrorFileType = true;
    }

    if (val.size > maxFileSize) {
      isErrorFileSize = true;
    }
  });

  if (isErrorFileType) {
    $('#error-uploadedImageInvalidType').show();
  } else {
    $('#error-uploadedImageInvalidType').hide();
  }

  if (isErrorFileSize) {
    $('#error-uploadedMaxSizeLimitReached').show();
  } else {
    $('#error-uploadedMaxSizeLimitReached').hide();
  }

  if ($('#uploadCoverImg-0')[0].files[0]) {
    $('#error-uploadCoverImg').hide();
  }

  processUploadedImage();
}


function padamFail(fileIndex) {
  uploadedFilesArray.splice(fileIndex, 1);
  processUploadedImage();
}

function processUploadedImage() {
  if (!uploadedFilesArray.length) {

    $('#error-uploadedGambarBilik').show();
    $('#parentDivUploadedFileDisplay-0').html('');
    $('#parentDivUploadedFileDisplay-0').hide();
    $('#btnuploadfileParentDiv-0').show();
  } else {

    $('#btnuploadfileParentDiv-0').hide();
    $('#parentDivUploadedFileDisplay-0').show();
    $('#parentDivUploadedFileDisplay-0').html('');

    for (var i = 0; i < uploadedFilesArray.length; i++) {
      $('#parentDivUploadedFileDisplay-0').append(`
        <div class="book-cover-card-body">
          <img id="lampiranAduan" class="" src="` + URL.createObjectURL(uploadedFilesArray[i]) + `" alt="book cover" height="160">
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray[i].name}</p>
          </div>
          <a  onclick="padamFail(${i})" class="book-cover-card-btn cursor-pointer">Remove File</a>
        </div>`);
    }
  }
  if (uploadedFilesArray.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}





function uploadButton1(index) {
  $('#uploadCoverImg-' + index).val(null);
  $('#uploadCoverImg-' + index).trigger('click');
}

function onChangeUploadFile1(index) {
  // reset
  isErrorFileType = false;
  isErrorFileSize = false;
  uploadedFilesArray1 = [];
  $.each($('#uploadCoverImg-' + index)[0].files, function (index, val) {
    uploadedFilesArray1.push(this);
    if (val.type != "image/jpeg" && val.type != "image/jpg" && val.type != "image/png") {
      isErrorFileType = true;
    }
    if (val.size > maxFileSize) {
      isErrorFileSize = true;
    }
  });
  if (isErrorFileType) {
    $('#error-uploadedImageInvalidType').show();
  } else {
    $('#error-uploadedImageInvalidType').hide();
  }
  if (isErrorFileSize) {
    $('#error-uploadedMaxSizeLimitReached').show();
  } else {
    $('#error-uploadedMaxSizeLimitReached').hide();
  }

  if ($('#uploadCoverImg-' + index)[0].files[0]) {
    $('#error-uploadCoverImg').hide();
  }

  processUploadedImage1(index);
}

function padamFail1(fileIndex, index) {
  uploadedFilesArray1.splice(fileIndex, 1);
  processUploadedImage1(index);
}

function processUploadedImage1(index) {
  if (!uploadedFilesArray1.length) {

    $('#error-uploadedGambarBilik').show();
    $('#parentDivUploadedFileDisplay-' + index).html('');
    $('#parentDivUploadedFileDisplay-' + index).hide();
    $('#btnuploadfileParentDiv-' + index).show();
  } else {

    $('#btnuploadfileParentDiv-' + index).hide();
    $('#parentDivUploadedFileDisplay-' + index).show();
    $('#parentDivUploadedFileDisplay-' + index).html('');

    for (var i = 0; i < uploadedFilesArray1.length; i++) {
      $('#parentDivUploadedFileDisplay-' + index).append(`
        <div class="card col-2 border rounded-lg p-2 mx-2">
          <img id="lampiranAduan" class="card-img-top" src="` + URL.createObjectURL(uploadedFilesArray1[i]) + `" alt="book cover" height="160">
          <div class="book-cover-card-body">
            <p style="margin-bottom: 0;">${uploadedFilesArray1[i].name}</p>
          </div>
          <a  onclick="padamFail1(${i}, ${index})" class="book-cover-card-btn cursor-pointer">Padam Fail</a>
        </div>`);
    }
  }
  if (uploadedFilesArray1.length != 0) {
    //reset data
    dataForm = new FormData();

    $.each(uploadedFilesArray1, function (index, val) {
      let file = val;
      dataForm.append('file' + index, file);
    });
  }
}

$("#hantarBtn").on("click", () => {
  validateBorang();

  if(validateBorang() == false){
    $("#confirmationModal").modal("show");
  }
  else{
    $('html, body').animate({
      scrollTop: $('#maklumatDaftar').offset().top + 'px'
  }, 1000);
  }
});

function validateBorang() {
  let IsError = false;

  if ($("#filterJenisCenderamata-0").val()) {
    $("#error-jenisCenderamata").hide();
    } else {
    $("#error-jenisCenderamata").show();
    IsError = true
  }

  if ($("#harga-0").val()) {
    $("#error-harga").hide();
    } else {
    $("#error-harga").show();
    IsError = true
  }

  if ($("#kuantiti-0").val()) {
    $("#error-kuantiti").hide();
    } else {
    $("#error-kuantiti").show();
    IsError = true
  }

  if ($("#tarikhPenerimaan-0").val()) {
    $("#error-tarikh").hide();
    } else {
    $("#error-tarikh").show();
    IsError = true
  }

  if (uploadedFilesArray.length > 0) {
    $('#error-uploadedGambarBilik').hide();
  } else {
    $('#error-uploadedGambarBilik').show();
    IsError = true;
  }

return IsError;

}

var frm = $('.form-l');
var countImg = $('.card-img-top').length;
var refnum;
// submit form
$("#submitBorangDaftar").on("click", () => {
  $("#loadingModal").modal({
    backdrop: "static",
    keyboard: false,
    show: true,
  });


  let putAPIBook = `${host}/api/book/${inventoryID}/updateBookInformation`;
  //let putAPIBook = `${host}/api/book/addBookInformation`;

  $(".form-list").each(function (i) {
    let form = new FormData();
   
    
    form.append("BookTitle", $("#BookTitle").val());
    form.append("BookDescription", $("#BookDescription").val());
    form.append("BookCode", $("#BookCode").val());
    form.append("SellPrice", $("#SellPrice").val());
    form.append("WalkInPrice", $("#WalkInPrice").val());
    form.append("SpecialPrice", $("#SpecialPrice").val());
    form.append("StockIn", $("#StockIn").val());
    form.append("StockOut", $("#StockOut").val());stockbalanceInput
    form.append("StockbalanceInput", $("#stockbalanceInput").val());
    form.append("QuantityReserve", $("#QuantityReserve").val());
    form.append("ReserveRemark", $("#ReserveRemark").val());
    form.append("UpdatedBy", userID);
    

   
    // code daus v2
    let fileInputImages = $("#uploadCoverImg-" + i)[0];
    for (j = 0; j < fileInputImages.files.length; j++) {
      form.append("FailGambar[]", $("#uploadCoverImg-" + i)[0].files[j]);
    }
  // console.log(...form);

    let postData = $.ajax({
        url: putAPIBook,
        type: "post",
        mimeType: "multipart/form-data",
        processData: false,
        contentType: false,
        async: false,
        data: form,
      }).done((res) => {
        // Handle success here
        console.log("Operation successful");
      }).fail((err) => {
        // Handle failure here
        console.error(err);
      }).always(() => {
        // This part is executed regardless of success or failure
        $("#loadingModal").modal("hide"); // Hide the loading modal
      //  $("#loadingModal").modal("hide");
      //   setTimeout(() => {
      //     // $("#berjaya-simpan-modal").modal({
      //     //   backdrop: 'static',
      //     //   keyboard: false,
      //     // });
      //   }, 1000);
        location.replace(
              `${host}/book/bookInventory`
            );

      })//
   
  })

});




      let HistoryList = () => {

          let getHistoryList = `${host}/api/book/${userID}/${inventoryID}/bookTransactionList`;
            $.ajax({
              url: getHistoryList,
              type: "GET",
              dataType: "JSON",
            })
            .done((res) => {
              //createTable(res.data);
              tableHistoryList(res.bookTransactionList);
              console.log(res.bookTransactionList);
            })
            .catch((err) => {});
        };


      let tableHistoryList = (data) => {
      let senarai = $("#BookHistory").DataTable({
        data: data,
        //  order: [[4, "desc"]],
        columnDefs: [
          {
            targets: 0,
            data: null,
            searchable: false,
            orderable: false,
          },
          {
            targets: 1,
            data: "fullname",
            render: function (data, type, full, meta) {
              if (data === null) {
                return `<td width="40%" align="left"><p class="poppins-semibold-14">-</p></td>`;
              }
              if (data !== null || data !== "") {
                return `<td align="left" width="40%"><p class="poppins-semibold-14">${data}</p></td>`;
              }
            },
          },
          {
            targets: 2,
            data: "quantity",
            render: function (data, type, full, meta) {
              return `<td align="left" width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
            },
          },
          
          {
            targets: 3,
            data: "online_price_perunit",
            render: function (data, type, full, meta) {
              if(data && data != '' && data != null) {
               
                return  `<td width="15%"><p class="poppins-semibold-14">${data}</p></td>`;
              } else {
                return `<td width="15%"><p class="poppins-semibold-14">-</p></td>`;
              }
              
            },
          },
          {
            targets: 4,
            data: "total_price",
            render: function (data, type, full, meta) {
            
              if(data && data != '' && data != null) {
                  return `<div class="dark-turquoise-status poppins-medium-12">${data}</div>`;
              }else
               {
                return "-";
              }
            },
          },
          {
            targets: 5,
            data: "date_purchase",
            render: function (data, type, full, meta) {
            
              if(data && data != '' && data != null) {
                  return `<div class="dark-turquoise-status poppins-medium-12">${data}</div>`;
              }else if(data == '0' || data == 0) {
                return `<div class="dark-turquoise-status poppins-medium-12">${data}</div>`;
              }else
               {
                return "-";
              }
            },
          },
          {
            targets: 6,
            data: "status_payment",
            className: "text-center",
            render: function (data, type, full, meta) {
              
    
              if(data == '1') {
                  return `<div class="dark-turquoise-status poppins-medium-12">Available</div>`;
              }else if(data == '2'){
                return `<div class="dark-turquoise-status poppins-medium-12">Reserved</div>`;
              }else if(data == '3'){
                return `<div class="dark-turquoise-status poppins-medium-12">Out of stock</div>`;
              }else
               {
                return "-";
              }


            },
          },
       
        ],
        language: {
          info: "view _START_ to _END_ from _TOTAL_ entry",
          infoEmpty: "view 0 from 0 to 0 entry",
          lengthMenu: "view _MENU_ Entry",
          paginate: {
            next: "Next",
            previous: "Previous",
          },
          zeroRecords: "No record found",
        },
      });
    
      senarai
        .on("order.dt search.dt", function () {
          senarai
            .column(0, { search: "applied", order: "applied" })
            .nodes()
            .each(function (cell, i) {
              cell.innerHTML = i + 1;
            });
        })
        .draw();
    
  };
    

  // table History
// $(function(){

// let tableHistoryList = (data) => {
//   let senarai = $("#BookHistoryList").DataTable({
//       data: data,
//       columns: [
//           { data: "fullname" },
//           { data: "quantity" },
//           { data: "online_price_perunit" },
//           { data: "total_price" },
//           { data: "date_purchase" },
//           { data: "status_payment" }
//       ],
//       language: {
//           info: "view _START_ to _END_ from _TOTAL_ entry",
//           infoEmpty: "view 0 from 0 to 0 entry",
//           lengthMenu: "view _MENU_ Entry",
//           paginate: {
//               next: "Next",
//               previous: "Previous",
//           },
//           zeroRecords: "No record found",
//       },
//   });

//   senarai
//       .on("order.dt search.dt", function () {
//           senarai
//               .column(0, { search: "applied", order: "applied" })
//               .nodes()
//               .each(function (cell, i) {
//                   cell.innerHTML = i + 1;
//               });
//       })
//       .draw();
// };

  HistoryList();
 
// });