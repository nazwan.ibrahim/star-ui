<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class QuestionBankFile extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'question_bank_file';
    protected $fillable = [
        'id_question_bank',
        'file_name',  // Add 'file_name' to the fillable array
        'file_path',
        'file_format',
        'status',
        'type',
        'category',
    ];
    public function questionBank()
    {
        return $this->belongsTo(QuestionBank::class, 'id_question_bank');
    }
}
