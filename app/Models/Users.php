<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Casts\Attribute;


class Users extends Model
{
    use HasApiTokens, HasFactory, Notifiable;


    protected $fillable = ['fullname', 'ic_no','certificate_no','certificate_category', 'phone_no','email', 'password','password_confirmation',
     'agency','user_type','terms','status','id_company','created_by','updated_by'];


    protected $hidden = [
        'password',
        'remember_token',
    ];
    
    
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];    
}
