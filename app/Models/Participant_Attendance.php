<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participant_Attendance extends Model
{
    use HasFactory;

    protected $table = 'participant_attendance';

}
