<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class QuestionBankRequest extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'question_bank_request';

    protected $fillable = [
        'id_company',
        'id_training_course',
        'id_request_status',       
        'status',
        'created_by',
        'created_at'
    ];
    
}
