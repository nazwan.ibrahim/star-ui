<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrainingRegistration extends Model
{
    use SoftDeletes;
    protected $table = 'trainings';
    
    public function trainingCourse()
    {
        return $this->belongsTo(TrainingCourse::class, 'id_training_course', 'id_training_course');
    }
}