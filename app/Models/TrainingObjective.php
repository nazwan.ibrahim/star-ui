<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingObjective extends Model
{
    protected $table = 'training_objective';    
    protected $fillable = ['id_training', 'objective'];

}