<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Upload_Document extends Model
{
    use HasFactory;
    protected $fillable = [
        'file_name',
        'file_path',
        'docType',
        'status',
        'created_at',
        'created_by'
        
    ];

    protected $table = 'upload_documents';

}
