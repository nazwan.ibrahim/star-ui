<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
class Question_bank_file extends Model
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $fillable = [
        'file_name',
        'file_path',
        'type',
        'status','category'
    ];
    protected $table = 'question_bank_file';

}
