<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
class QuestionBank extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'question_bank';

    protected $fillable = [
        'id_company',
        'id_trainings',
        'id_training_course',
        'id_approval_status',       
        'status',
        'created_by',
        'created_at'
    ];

    public function files()
    {
        return $this->hasMany(QuestionBankFile::class, 'id_question_bank');
    }
    

}
