<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Company extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name','email','phone_no','id_country','id_state','status','created_by','created_at'
   ];
    protected $table = 'company';

    public function user()
    {
        return $this->hasOne(Company::class);
    }

}