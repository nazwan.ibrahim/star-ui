<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
class Trainer extends Model
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table = 'trainer'; 
    protected $fillable = [
        'id_users',
        'id_company',
        'expertise_area',
        'status',
        'created_at',
        'created_by',
    ];
}
