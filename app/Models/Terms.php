<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
class Terms extends Model
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table = 'training_terms'; 
    protected $fillable = [
        'id_training_participant',
        'cidb',
        'safety_course',
        'safety_course_name',
        'safety_start_date',
        'math_education',
        'highest_education',
        'others_education',
        'edu_start_date',
        'age',
        'payment_proof',
        'qualification',
        'safe_knowledge',
        'math_knowledge',
        'read_knowledge',
        'understanding_knowledge',
        'health_insurance',
        'safety_priority',
        'rules',
        'electric_formula',
        'working_knowledge',
        'declaration',
        'status',
        'created_at',
        'created_by'

    ];
}
