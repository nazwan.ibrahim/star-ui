<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Application extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'training_participant'; 
    protected $fillable = [
        'id_users',
        'id_training',
        'id_approval_status',
        'status_payment',
        'status_attend',
        'status_exam',
        'status_certificate',
        'status_feedback',
        'training_status',
        'created_at',
        'created_by',
    ];
}