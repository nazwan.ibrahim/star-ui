<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question_bank_request extends Model
{
    use HasFactory;

    protected $table = 'question_bank_request';

}
