<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Training extends Model
{
    use SoftDeletes;
    
    public function courseCategory()
    {
        return $this->belongsTo(CourseCategory::class, 'id_course_category');
    }

    public function trainingType()
    {
        return $this->belongsTo(TrainingType::class, 'id_training_type');
    }

    public function trainingOption()
    {
        return $this->belongsTo(TrainingOption::class, 'id_training_option');
    }

    public function trainingCourse()
    {
        return $this->belongsTo(TrainingCourse::class, 'id_training_course', 'id_training_course');
    }

    public function uploadDocuments()
    {
        return $this->hasMany(UploadDocuments::class, 'id');
    }
}

class UploadDocuments extends Model
{
    protected $table = 'upload_documents';

    public function trainings()
    {
        return $this->hasMany(Training::class, 'id_training');
    }

}

class CourseCategory extends Model
{
    protected $table = 'course_category';

    public function trainings()
    {
        return $this->hasMany(Training::class, 'id_course_category');
    }
}

class TrainingType extends Model
{
    protected $table = 'training_type';

    public function trainings()
    {
        return $this->hasMany(Training::class, 'id_training_type');
    }
}

class TrainingOption extends Model
{
    protected $table = 'training_option';

    public function trainings()
    {
        return $this->hasMany(Training::class, 'id_training_option');
    }
}

class TrainingCourse extends Model
{
    protected $table = 'training_course';

    public function trainings()
    {
        return $this->hasMany(Training::class, 'id_training_course');
    }

    public function editTrainingRegistration($id)
    {
        $userid = auth()->user()->id;
        $training = Training::select('id', 'other_columns')
            ->with('trainingCourse', 'courseCategory', 'trainingType','trainingOption')
            ->where('id', $id)
            ->first();

        return view('trainingMgmt.editTrainingRegistration', compact('userid', 'training'));
    }

    public function updateTrainingRegistration(Request $request, $id)
    {
        $this->validate($request, [
            /* 'training_option' => 'string', */
            'training_mode' => 'string',
            'venue' => 'string',
            'venue_address' => 'string',
            'state' => 'string',
            'id_course_category' => '',
            'id_training_type' => '',
            'trainingCourse' => 'string',
            'total_participant' => '',
            'training_code' => 'string',
            'date_start' => '',
            'date_end' => '',
            'start_time' => '',
            'end_time' => '',
            'advert_start' => '',
            'advert_end' => '',
            'fee' => '',
            'hrdf_claim' => '',
            'document_description' => 'string',
            'training_description' => 'string',
            'batch_no' =>'',
            'id_company'=>'',
            'training_image'=>'',
        ]);

        $training = Training::find($id);

        if (!$training) {
            return redirect()->route('editTrainingRegistration', ['id' => $id])->with('error', 'Training not found');
        }

        $training->update([
            /* 'training_option' => $request->input('training_option'), */
            'training_mode' => $request->input('training_mode'),
            'venue' => $request->input('venue'),
            'venue_address' => $request->input('venue_address'),
            'state' => $request->input('state'),
            'id_course_category' => $request->input('id_course_category'),
            'id_training_type' => $request->input('id_training_type'),
            'trainingCourse' => $request->input('trainingCourse'),
            'total_participant' => $request->input('total_participant'),
            'training_code' => $request->input('training_code'),
            'date_start' => $request->input('date_start'),
            'date_end' => $request->input('date_end'),
            'start_time' => $request->input('start_time'),
            'end_time' => $request->input('end_time'),
            'advert_start' => $request->input('advert_start'),
            'advert_end' => $request->input('advert_end'),
            'fee' => $request->input('fee'),
            'hrdf_claim' => $request->input('hrdf_claim'),
            'document_description' => $request->input('document_description'),
            'training_description' => $request->input('training_description'),
            'training_image' => $request->input('training_image'),
            'batch_no' => $request->input('batch_no'),
            'id_company' => $request->input('partner'),
            'attendance_status' => 0,
            'results_status' => 0,
            'report_status' => 0,
            'status' => 1,
            'id_training_status' => 1,
            'id_approval_status' => 2,
            'id_training_app_status' => 1,
            'updated_at' => now(),
        ]);

        return redirect()->route('editTrainingRegistration', ['id' => $id])->with('success', 'Training registration updated successfully');
    }

    public function insertTraining(Request $request) {
        $this->validate($request, [
            'id_training_option' => 'nullable|string',
            'course_name' => 'nullable',
            'training_mode' => 'nullable|string',
            'venue' => 'nullable|string',
            'venue_address' => 'nullable|string',
            'state' => 'nullable|string',
            'id_course_category' => 'nullable',
            'id_training_type' => 'nullable',
            'id_training_course' => 'nullable',
            'total_participant' => 'nullable',
            'training_code' => 'nullable|string',
            'date_start' => 'nullable|date',
            'date_end' => 'nullable|date',
            'start_time' => 'nullable',
            'end_time' => 'nullable',
            'advert_start' => 'nullable',
            'advert_end' => 'nullable',
            'is_paid' => 'nullable|boolean',
            'fee' => 'nullable|numeric',
            'hrdf_claim' => 'nullable|boolean',
            'document_description' => 'nullable|string',
            'training_description' => 'nullable|string',
            'batch_no' =>'nullable|string',
            'id_company' => 'nullable'
        ]);
    
        $stateToRegion = [
            'Perlis' => 'Northern Region',
            'Kedah' => 'Northern Region',
            'Pulau Pinang' => 'Northern Region',
            'Perak' => 'Northern Region',
            'Selangor' => 'Central Region',
            'Kuala Lumpur' => 'Central Region',
            'Labuan' => 'Central Region',
            'Putrajaya' => 'Central Region',
            'Kelantan' => 'Eastern Region',
            'Terengganu' => 'Eastern Region',
            'Pahang' => 'Eastern Region',
            'Negeri Sembilan' => 'Southern Region',
            'Melaka' => 'Southern Region',
            'Johor' => 'Southern Region',
            'Sabah' => 'East Malaysia',
            'Sarawak' => 'East Malaysia',
        ];

        $selectedState = $request['state'];
    
        $region = $stateToRegion[$selectedState];

        $fee = $request->input('fee');
        $isPaid = $fee > 0 ? 1 : 0;
    
        $training = new Training();
        $training->fill($request->all());
        $training->region = $region;
        $training->is_paid = $isPaid;
        $training->save();
    
        return redirect('/admin/training-management');
    }
}