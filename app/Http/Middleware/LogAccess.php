<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\AccessLog;
use Illuminate\Support\Facades\Auth;

class LogAccess
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($user) {
            $username = $user->username;
        } else {
            $username = 'Guest';
        }

        $activity = $request->is('logout') ? 'logout' : 'login';

        AccessLog::create([
            'username' => $username,
            'activity' => $activity,
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $request->header('User-Agent'),
        ]);
        return $next($request);
    }
}