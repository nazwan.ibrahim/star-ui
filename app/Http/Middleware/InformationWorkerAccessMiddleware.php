<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TrainingPartnerAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next)
    {
        $userType = auth()->user()->user_type;

        if ($userType == 6) {
            return $next($request);
        }

         // Redirect to the custom error page for 403 Forbidden
         return abort(403);
    }
}
