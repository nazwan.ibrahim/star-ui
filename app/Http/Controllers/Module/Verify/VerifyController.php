<?php

namespace App\Http\Controllers\Module\Verify;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class VerifyController extends Controller
{
    public function verifyPage()
    {
        return view('verifyCert.verifycertificate');
    }

    public function verifyCertificate(Request $request)
    {
        $data = [
            'mykad_no' => $request['ic_no'],
            'certificate_no' => $request['certificate_no'],
        ];

        $response = Http::withOptions([
            'verify' => false,
        ])->withHeaders([
            'Authorization' => 'Bearer ' . env('ACCESS_TOKEN'),
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ])->post('https://spqp-staging.seda.gov.my/api/qualified_person/certificate/verify', $data);

        $jsonData = $response->json();

        return response()->json($jsonData);


        // Check if any key in the JSON data is null
        // if ($jsonData['status'] == 'OK' && !in_array(null, $jsonData, true)) {
        //     return view('verifyCert.verifycertificate', ['jsonData' => $jsonData]);
        // } else {
        //     return redirect()->back()->with('error', 'Record Not Found.');
        // }
    }


}