<?php

namespace App\Http\Controllers\Module\BookMgmt;

use Illuminate\Support\Facades\Http;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Mail\PendingMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;


class BookMgmtListController extends Controller
{
    public function bookListPublic() {

       

        $booklist = DB::table('book_inventory')
        ->where('book_inventory.deleted_status','=',0)
        ->orWhere('book_inventory.deleted_status','=','')
        ->leftJoin('book_cover','book_cover.id_book_inventory', '=', 'book_inventory.id')
        ->leftJoin('book_details', 'book_details.id_book_inventory', '=', 'book_inventory.id')
        ->select('book_inventory.id','book_inventory.book_code','book_inventory.book_title','book_inventory.book_description',
        'book_cover.cover_name', 'book_cover.file_cover','book_cover.file_format',
        'book_cover.file_size','book_details.online_price','book_details.sell_type','book_details.stock_in',
        'book_details.stock_out','book_inventory.deleted_status')
        ->get();


        return view('bookMgmt.bookMarketPlace.booklistPublic', compact('booklist'));

      }

      public function bookList() {

        // if (!auth()->check() ) {
        //     return redirect()->route('login');
        // } else {
        //     $userid = auth()->user() ? auth()->user()->id : null;
        $userid = auth()->user()->id;

        $booklist = DB::table('book_inventory')
        ->where('book_inventory.deleted_status','=',0)
        ->orWhere('book_inventory.deleted_status','=','')
        ->leftJoin('book_cover','book_cover.id_book_inventory', '=', 'book_inventory.id')
        ->leftJoin('book_details', 'book_details.id_book_inventory', '=', 'book_inventory.id')
        ->select('book_inventory.id','book_inventory.book_code','book_inventory.book_title','book_inventory.book_description',
        'book_cover.cover_name', 'book_cover.file_cover','book_cover.file_format',
        'book_cover.file_size','book_details.online_price','book_details.sell_type','book_details.stock_in',
        'book_details.stock_out','book_details.stock_balance','book_inventory.deleted_status')
        ->get();


        return view('bookMgmt.bookMarketPlace.booklist', compact('userid','booklist'));
       // }

      }


      
      public function bookDetail($bookid) {

        $userid = auth()->user()->id;

        $bookdetail = DB::table('book_inventory')
        ->where('book_inventory.id','=',$bookid)
        ->leftJoin('book_cover','book_cover.id_book_inventory', '=', 'book_inventory.id')
        ->leftJoin('book_details', 'book_details.id_book_inventory', '=', 'book_inventory.id')
        ->select('book_inventory.id','book_inventory.book_code','book_inventory.book_title','book_inventory.book_description',
        'book_cover.cover_name', 'book_cover.file_cover','book_cover.file_format',
        'book_cover.file_size','book_details.online_price','book_details.sell_type','book_details.stock_in',
        'book_details.stock_out','book_inventory.deleted_status')
        ->get();


        return view('bookMgmt.bookMarketPlace.bookdetail', compact('userid','bookdetail'));

      }


      public function bookDetailPublic($bookid) {

        $bookdetail = DB::table('book_inventory')
        ->where('book_inventory.id','=',$bookid)
        ->leftJoin('book_cover','book_cover.id_book_inventory', '=', 'book_inventory.id')
        ->leftJoin('book_details', 'book_details.id_book_inventory', '=', 'book_inventory.id')
        ->select('book_inventory.id','book_inventory.book_code','book_inventory.book_title','book_inventory.book_description',
        'book_cover.cover_name', 'book_cover.file_cover','book_cover.file_format',
        'book_cover.file_size','book_details.online_price','book_details.sell_type','book_details.stock_in',
        'book_details.stock_out','book_inventory.deleted_status')
        ->get();


        return view('bookMgmt.bookMarketPlace.bookdetailpublic', compact('bookdetail'));

      }
      
   
      public function storeSelectedItems(Request $request)
      {
        //   if (auth()->user()->id == ''){
        //     return redirect()->route('login.login');
        //   }
        // if (!auth()->check() ) {
        //     return redirect()->route('login');
        // } else {
        //     $userid = auth()->user() ? auth()->user()->id : null;
      
          $userid = auth()->user()->id;
          $selectedItems = json_decode($request->input('cart_items'));
      
          if (!is_array($selectedItems)) {
              $selectedItems = [];
          }
      
          // Use the correct session variable name
          session(['selectedItems' => $selectedItems]);
      
          return redirect()->route('show-Selected-Items');
      //}
    }
      
    public function showSelectedItems()
    {
        $productTotal = '';
        // if (!auth()->check() ) {
        //     return redirect()->route('login');
        // } else {
        //     $userid = auth()->user() ? auth()->user()->id : null;
        $userid = auth()->user()->id;
        $selectedItems = session('selectedItems');

        $currentTime = now();

        $bookDetails = []; // Initialize an empty array to store details
        //dd($selectedItems);
         $productQuantities = [];

        if (is_null($selectedItems) && is_array($selectedItems)) {
            
        }

        if (!is_null($selectedItems) && is_array($selectedItems)) {
            foreach ($selectedItems as $item) {
                // Check if the product ID is already in the array
                if (isset($productQuantities[$item])) {
                    // Increment the quantity if it already exists
                    $productQuantities[$item]++;
                } else {
                    // Set the quantity to 1 if it doesn't exist
                    $productQuantities[$item] = 1;

                    // Fetch details for the product only once
                    $bookdetail = DB::table('book_inventory')
                        ->where('book_inventory.id', '=', $item)
                        ->leftJoin('book_cover', 'book_cover.id_book_inventory', '=', 'book_inventory.id')
                        ->leftJoin('book_details', 'book_details.id_book_inventory', '=', 'book_inventory.id')
                        ->select('book_inventory.id', 'book_inventory.book_code', 'book_inventory.book_title', 'book_inventory.book_description',
                            'book_cover.cover_name', 'book_cover.file_cover', 'book_cover.file_format',
                            'book_cover.file_size', 'book_details.online_price', 'book_details.sell_type', 'book_details.stock_in',
                            'book_details.stock_out', 'book_inventory.deleted_status')
                        ->first();

                    // Add the details to the array
                    $bookDetails[] = $bookdetail;
                    // Calculate the total for each product
                $productTotal = $bookdetail->online_price; 

            

                $productTotals[] = $productTotal;
                }
            }

            if($productTotal == ''){
            // dd('no data');
            $grandTotal = 0 ;
                return view('bookMgmt.bookMarketPlace.book_shoppingcart', compact('userid', 'selectedItems', 'bookDetails','productQuantities','grandTotal'));
            }
            $grandTotal = array_sum($productTotals);
        }

            return view('bookMgmt.bookMarketPlace.book_shoppingcart', compact('userid', 'selectedItems', 'bookDetails','productQuantities','grandTotal'));

       // }
    }



    public function bookCheckout(Request $request){   

        // if (!auth()->check() ) {
        //     return redirect()->route('login');
        // } else {
        //     $userid = auth()->user() ? auth()->user()->id : null;

        $userid = auth()->user()->id;
        // Get all form input data
        $formData = $request->all();
       
        // Remove the token and grand_price from the data
        unset($formData['_token'], $formData['grand_price']);


        $dateComponent = date('Ymd');
        $randomComponent = mt_rand(10000, 99999);
        $orderNumber = $dateComponent. $randomComponent;

        foreach ($formData as $key => $value) {
            // Check if the key follows the pattern "inventory_id-*"
            if (preg_match('/^inventory_id-\d+$/', $key)) {
                // Extract the book ID from the field name (e.g., "inventory_id-4" becomes "4")
                $bookId = explode('-', $key)[1];
        
                // Insert into the database
                $bookOrder = DB::table('book_order_item')->insertGetId([
                    'user_id' => $userid,
                    'order_no' => $orderNumber,
                    'id_book_inventory' => $bookId,
                    'quantity' => $formData["quantityInput-$bookId"],
                    'online_price_perunit' => $formData["online_price-$bookId"],
                    'total_price' => $formData["total_priceInput-$bookId"],
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }
        }

        $bookPayment = DB::table('book_payment')->insert([
            'order_no' => $orderNumber,
            'user_id' => $userid,
            'grand_total_price' => $request->input('grand_price'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $gprice = DB::table('book_payment')
        ->where('book_payment.order_no', '=',$orderNumber)
        ->select(
            'order_no as order_id',
            'user_id',
            'grand_total_price'
        )
        ->first();
    
      
        foreach ($formData as $key => $value) {
            // Extract the book ID from the field name (e.g., "inventory_id-4" becomes "4")
            $bookId = explode('-', $key)[1];
    
       
        $bookDetails = DB::table('book_inventory')
        //->where('book_order_item.updated_at', '=', DB::raw('(SELECT MAX(updated_at) FROM book_order_item WHERE book_order_item.id_order = book_order_item.id_order)'))
            ->where('book_order_item.user_id', '=', $userid)  // Assuming 'id_book_inventory' is the inventory ID
            ->where('book_order_item.order_no', '=', $orderNumber) 
            ->leftJoin('book_cover', 'book_cover.id_book_inventory', '=', 'book_inventory.id')
            ->leftJoin('book_details', 'book_details.id_book_inventory', '=', 'book_inventory.id')
            ->leftJoin('book_order_item', 'book_order_item.id_book_inventory', '=', 'book_inventory.id')
            ->leftJoin('book_payment','book_payment.order_no','=','book_order_item.order_no')
            ->leftJoin('users','users.id','=','book_order_item.user_id')
            ->select('book_inventory.id', 'book_inventory.book_code', 'book_inventory.book_title', 'book_inventory.book_description',
                'book_cover.cover_name', 'book_cover.file_cover', 'book_cover.file_format',
                'book_cover.file_size', 'book_details.sell_type', 'book_details.stock_in',
                'book_details.stock_out', 'book_inventory.deleted_status','book_order_item.quantity','book_order_item.online_price_perunit','book_order_item.total_price')
            ->orderby('book_order_item.quantity')
            ->groupby('book_inventory.id')
            ->get();
        }


        $detailsPayment =  DB::table('users')
        ->select(
            'users.fullname',
            'users.id',
            'users.email',
            'users.phone_no',
            'users.address_1','users.address_2','users.postcode',
            'state.name as state',
            'country.nicename as country','book_payment.grand_total_price'
        
        )
   
            ->leftJoin('country', 'country.id', '=', 'users.id_country')
            ->leftJoin('state','state.id','=','users.id_state')
            ->leftJoin('book_payment','book_payment.user_id','=','users.id')
            ->leftJoin('book_order_item','book_order_item.order_no','=','book_payment.order_no')
        
            ->where('users.id', '=', $userid)
            ->orderby('book_payment.id','desc')
            ->first();

        

        return view('bookMgmt.bookMarketPlace.paymentDetails',compact('userid','detailsPayment','bookDetails','gprice'));

     // }
    }




    public function processPayment(){

        // if (!auth()->check() ) {
        //     return redirect()->route('login');
        // } else {
        //     $userid = auth()->user() ? auth()->user()->id : null;
        $userid = auth()->user()->id;
        $remarks = request()->input('remarks');

        $detailsPayment =  DB::table('users')
        ->select(
            'users.fullname',
            'users.id',
            'users.email',
            'users.phone_no',
            'users.address_1','users.address_2','users.postcode',
            'state.name as state',
            'country.nicename as country','book_payment.grand_total_price'
           
        )
       
        ->leftJoin('country', 'country.id', '=', 'users.id_country')
        ->leftJoin('state','state.id','=','users.id_state')
        ->leftJoin('book_payment','book_payment.user_id','=','users.id')
        ->leftJoin('book_order_item','book_order_item.order_no','=','book_payment.order_no')
      
        ->where('users.id', '=', $userid)
        ->orderby('book_payment.id','desc')
        ->first();

     
        if($detailsPayment->country && $detailsPayment->address_1){
    
        $DATA = [
            'merchant_acc_no' => env('MERCHANT_ACC'),
        ];

        $response = Http::withOptions([
            'verify' => false,
        ])->withHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded',
        ])->asForm()->post('https://efit.seda.gov.my/pgwy/transid.php', $DATA);
        //dd($response->successful(env('MERCHANT_ACC')));
        if ($response->successful()) {     
            $responseData = $response->body();        
           
                if (isset($responseData)) {            
                    $transaction = [
                        "internal_merchant_account" => env('MERCHANT_ACC'),
                        "req_merchant_tranid" => $responseData, 
                        "internal_module_id" => "star-dev",
                        "req_amount" => $detailsPayment->grand_total_price,
                        "req_settlement_remarks" => "Transaction",
                        "req_txn_desc" => 'Book',
                        "req_customer_id" => $detailsPayment->id,
                        "req_fr_highrisk_email" => $detailsPayment->email,
                        "req_fr_highrisk_country" => $detailsPayment->country,
                        "req_fr_billing_address" => $detailsPayment->address_1 . ', ' . $detailsPayment->address_2,
                        "req_fr_shipping_address" => $detailsPayment->address_1 . ', ' . $detailsPayment->address_2,
                        "req_fr_shipping_cost" => "0.00",
                        "req_fr_customer_ip" => "202.188.0.1"
                    ];

                    $response2 = Http::withOptions([
                        'verify' => false,
                    ])->withHeaders([
                        'Content-Type' => 'application/x-www-form-urlencoded',
                    ])->asForm()->post('https://efit.seda.gov.my/pgwy/index.php', $transaction);
                    
                    if ($response2->successful()) {  
                        $responseData2 = $response2->body();   
                       // dd($responseData2);
                        // session(['payment_token' => $responseData2['SECURE_SIGNATURE']]);
                        if (isset($responseData)) {  
                            return $responseData2;
                           

                        }else{
                        
                            return redirect()->route('book-Payment-Invoice');
                        }
                    } else {
                        return redirect()->back()->with('error', 'Failed To Make Payment.');
                    }
                } else {
                    return redirect()->back()->with('error', 'Failed To Make Payment.');
                }
        } else {
            return redirect()->back()->with('error', 'Failed To Make Payment.');
        }
        }else{
            return redirect()->back()->with('error', 'Please Update Your Profile');
        }
    
   // }
            
    }

    //Route::post('/book/return', [BookMgmtListController::class, 'bookPaymentInvoice'])->name('book-Payment-Invoice');
    //public function bookPaymentInvoice(Request $request,$id)
    public function bookPaymentInvoice(Request $request)
    {   

        // if (!auth()->check() ) {
        //     return redirect()->route('login');
        // } else {
        // $userid = auth()->user() ? auth()->user()->id : null;
        $paymentToken = session('payment_token');

        $userid = auth()->user()->id;
        $detailsPayment =  DB::table('users')
        ->select([
            'users.fullname',
            'users.id',
            'users.email',
            'users.phone_no',
            'users.address_1','users.address_2','users.postcode',
            'state.name as state',
            'country.nicename as country','book_payment.grand_total_price'
           
        ])
       
        ->leftJoin('country', 'country.id', '=', 'users.id_country')
        ->leftJoin('state','state.id','=','users.id_state')
        ->leftJoin('book_payment','book_payment.user_id','=','users.id')
        ->leftJoin('book_order_item','book_order_item.order_no','=','book_payment.order_no')
      
        ->where('users.id', '=', $userid)
        //->where('book_payment.id_order', '=', $userid)
        ->first();

       // dd($request->all());
        // "inventory_id" => "6"
        // "online_price" => "100.99"
        // "quantity" => "3"
        // "total_price" => "302.97"

        // Log the incoming request for debugging
       // Log::info('Payment Return Request: ' . $request->all());
           // Retrieve the payment token from the query parameters
        $token = $request->query('SECURE_SIGNATURE');
       // $userid = auth()->user()->id;
        $details = DB::table('training_participant')
        ->join('trainings','trainings.id','=','training_participant.id_training')
        ->leftjoin('users','users.id','=','training_participant.id_users')
        ->leftjoin('country','country.id','=','users.id_country')
        ->where('users.id','=',$userid)
        ->select(
            'users.fullname',
            'users.email',
            'users.address_1',
            'users.address_2',
            'country.name as countryname',
            'users.id',
            DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
            DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
            'trainings.venue',
            'trainings.fee',
        )
        ->first();
        $DATA = [   
            "username" => 'api@seda.gov.my',
            "password" => '123456',
            "grant_type" => 'password',
            "userassignmentid" => '100'
        ];

        $response = Http::withOptions([
            'verify' => false,
        ])->withHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Authorization' => 'Bearer ' . $paymentToken,
        ])->asForm()->post('https://uat.officecentralcloud.com/token', $DATA);
        $responseData = json_decode($response->body(), true); // Decode the JSON into an array

        if (isset($responseData['access_token'])) {
            $accessToken = $responseData['access_token'];
                
        
        $resit = [
            "invoiceno" => "INV/2015/1",
            "receiptdate" => "2023-01-01",
            "subject" => "Baja Tumbuh-tumbuhan",
            "accountingaccountcode" => "SF-A0120201",
            "accountname" => "Customer A",
            "accountbrn" => "123456-I",
            "accountgstno" => "",
            "currencycode" => "MYR",
            "accountstreet1" => "No 1, Jalan 1/3P",
            "accountstreet2" => "Seksyen 1 Tambahan",
            "accountpostcode" => "43650",
            "accountcity" => "Bandar Baru Bangi",
            "accountstate" => "Selangor",
            "accountcountry" => "Malaysia",
            "accountinginvoicedetails" => [
                [
                    "msiccode" => "", // optional
                    "itemcode" => "",
                    "itemdescription" => "Baja",
                    "quantity" => 1,
                    "priceperunit" => 300.00,
                    "amountexcltax" => 300.00,
                    "taxamount" => 18.00,
                    "amountincltax" => 318.00,
                    "taxcode" => ""
                ]
            ],
            "accountingtransactiondetails" => [
                [
                    "accountingaccountcode" => "SF-H1110601", // DEBIT bank glcode
                    "debit" => 318.00, // inclusive of tax if any
                    "credit" => 0
                ],
                [
                    "accountingaccountcode" => "SF-H1110601", // Credit item glcode
                    "debit" => 0,
                    "credit" => 18.00 // inclusive of tax if any
                ],
                [
                    "accountingaccountcode" => "SF-H1110601",
                    "debit" => 0,
                    "credit" => 300.00 // inclusive of tax if any
                ]
            ]
        ];

        $response2 = Http::withOptions([
            'verify' => false,
        ])->withHeaders([
            'Authorization' => 'Bearer '. $accessToken,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ])->post('https://uat.officecentralcloud.com/api/accountingreceipts/create', $resit);

        $responseData2 = json_decode($response2->body(), true); // Decode the JSON into an array
            // dd($responseData2);
        if($responseData2['isSuccess']==true){

            //insert code into payment here
            //status_payment = 'success',
                        return view('bookMgmt.bookMarketPlace.bookPaymentReceipt',compact('userid','detailsPayment'));
        }else{
            dd("Failed to send receipt.");
             //status_payment = 'failed',
        }

    }else{
        dd("Access token not found in the response.");
    }

        
        // return view('dashboard.participant.participantPaymentInvoice',compact('userid'));
     //}

    }


    
}


