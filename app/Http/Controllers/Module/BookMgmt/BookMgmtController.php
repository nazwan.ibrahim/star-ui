<?php

namespace App\Http\Controllers\Module\BookMgmt;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BookMgmtController extends Controller
{
    public function addBook()
    {
        
        $userid = auth()->user()->id;
        return view('bookMgmt.bookInventory.addBook', compact('userid'));
    }


    public function bookInventory()
    {
        
        $userid = auth()->user()->id;
        return view('bookMgmt.bookInventory.bookInventoryList', compact('userid'));
    }

    public function bookInventoryUpdate()
    {
        
        $userid = auth()->user()->id;
        return view('bookMgmt.bookInventory.bookInventoryUpdate', compact('userid'));
    }

}
