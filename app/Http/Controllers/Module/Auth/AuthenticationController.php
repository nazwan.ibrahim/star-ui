<?php
namespace App\Http\Controllers\Module\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AuthenticationController extends Controller
{
  //Login page
  public function loginPage(){
    $pageConfigs = ['bodyCustomClass'=> 'bg-full-screen-image'];
    return view('auth.login',['pageConfigs' => $pageConfigs]);
  }
  //Register page
  public function registerPage(){
    $pageConfigs = ['bodyCustomClass'=> 'bg-full-screen-image'];
    return view('auth.register',['pageConfigs' => $pageConfigs]);
  }
   //forget Password page
   public function forgetPasswordPage(){
    $pageConfigs = ['bodyCustomClass'=> 'bg-full-screen-image'];
    return view('auth.forgotpassword',['pageConfigs' => $pageConfigs]);
  }
   
   public function loginSuccessPage(){
    $pageConfigs = ['bodyCustomClass'=> 'bg-full-screen-image'];
    return view('auth.loginsuccess',['pageConfigs' => $pageConfigs]);
  }
  
}
