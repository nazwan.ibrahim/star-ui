<?php

namespace App\Http\Controllers\Module\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Users;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    //  public function __construct()
    //  {
    //      $this->middleware('guest')->except('logout');
    //  }


     public function __construct()
     {
         $this->middleware('guest')->except([
             'logout', 'dashboard'
         ]);
     }

    public function email()
    {
        return 'email';
    }

    public function loginPage()
    {
        return view('login.login');
    }

    //forget Password page
   public function forgetPasswordPage(){
    $pageConfigs = ['bodyCustomClass'=> 'bg-full-screen-image'];
    return view('auth.forgotpassword',['pageConfigs' => $pageConfigs]);
  }
   

    public function authenticate(Request $request): RedirectResponse
    {
        // Validate the request data
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $remember_me = $request->has('remember_me') ? true : false; 


        // Get the user credentials from the request
        $credentials = $request->only('email', 'password');

        // Attempt to log in the user
        if (Auth::attempt($credentials)) {
            // $user = Auth::user();
            return redirect()->intended('dashboard')
            ->withSuccess('You have Successfully Login');

            // Store the username in the session
            // $request->session()->put('username', $user->username);

            // Check the user's type and redirect accordingly
            // switch ($user->id_usertype) {
            //     case '1':
            //         return redirect()->intended('admin.dashboard');
            //     case '2':
            //         return redirect()->intended('participant.dashboard');
            //     case '3':
            //         return redirect()->intended('trainingPartner.dashboard');
            //     case '4':
            //         return redirect()->intended('trainer.dashboard');
            //     default:
            //         return redirect()->intended('home');
            // }
        } else {
            return redirect()->back()
                ->withInput($request->except('password'))
                ->withErrors(['email' => 'ID pengguna atau kata laluan tidak sah.']);
        }
    }

   
    public function dashboard()
    {
        if(Auth::check())
        {
            return view('dashboard.participant.participantDashboard');
        }
        
        return redirect("login")->withSuccess('You are not allowed to access');
    } 

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
       // session()->forget('selectedItems');
        $request->session()->forget('selectedItems');
        return redirect()->route('login')
            ->withSuccess('You have logged out successfully!');;
    }    


    // Login
    // public function showLoginForm()
    // {
    //     $pageConfigs = ['bodyCustomClass' => 'bg-full-screen-image'];

    //     return view(
    //         'auth.login',
    //         [
    //             'pageConfigs' => $pageConfigs
    //         ]
    //     );
    // }


    // public function login(Request $request)
    // {
    //     $request->validate(
    //         [
    //             'email' => 'required',
    //             'password' => 'required',
    //         ]
    //     );

    //     $credentials = $request->only('email', 'password');

    //     if (Auth::attempt($credentials)) {

    //         $user = Auth::user();
    //         $request->session()->put('user', $user);
    //         return redirect()->route('index-laman-utama');

    //     }

    //     return redirect()->back()->withErrors(['Wrong Credentials!']);

    //     // dd($request);
    // }
}
