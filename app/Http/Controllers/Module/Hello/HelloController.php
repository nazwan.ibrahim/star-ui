<?php

namespace App\Http\Controllers\Module\Hello;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class HelloController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

  //  use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

     public function __construct()
     {
         $this->middleware('guest')->except('logout');
     }

    public function username()
    {
        return 'username';
    }

    public function helloPage(){
        $pageConfigs = ['bodyCustomClass'=> 'bg-full-screen-image'];
        return view('hello.hello',['pageConfigs' => $pageConfigs]);
      }
  

    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $date = Carbon::now()->toDateTimeString();
        $profilPeribadi = ProfilPeribadi::where('ADID', '=', $request->username);
        $profilPeribadi->update(['LogTerakhir' => $date]);

        $AD_Access = $this->credentials($request);

        // Check if the user exists in the database
        $user = ProfilPeribadi::where('ADID', $request->username)->first();

        // if (!$user || !Hash::check($request->password, $user->password)) {
        //     return redirect()->back()
        //         ->withInput($request->except('password'))
        //         ->withErrors(['username' => 'Invalid username or password.']);
        // }

        // Attempt to log in the user
        if (Auth::attempt($AD_Access)) {
            $user = Auth::user();
            $request->session()->put($request->username);
            // return $AD_Access;
        }

        return redirect()->back()
            ->withInput($request->except('password'))
            ->withErrors(['username' => 'ID pengguna atau kata laluan tidak sah.']);
    }



    // Login
    // public function showLoginForm()
    // {
    //     $pageConfigs = ['bodyCustomClass' => 'bg-full-screen-image'];

    //     return view(
    //         'auth.login',
    //         [
    //             'pageConfigs' => $pageConfigs
    //         ]
    //     );
    // }


    // public function login(Request $request)
    // {
    //     $request->validate(
    //         [
    //             'email' => 'required',
    //             'password' => 'required',
    //         ]
    //     );

    //     $credentials = $request->only('email', 'password');

    //     if (Auth::attempt($credentials)) {

    //         $user = Auth::user();
    //         $request->session()->put('user', $user);
    //         return redirect()->route('index-laman-utama');

    //     }

    //     return redirect()->back()->withErrors(['Wrong Credentials!']);

    //     // dd($request);
    // }
}
