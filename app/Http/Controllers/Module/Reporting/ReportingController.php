<?php

namespace App\Http\Controllers\Module\Reporting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\courseReType;

class ReportingController extends Controller
{
    public function trainingReporting()
    {
        $userid = auth()->user()->id;

        return view('reporting.trainingReport' , compact('userid'));
    }

    public function paymentReporting()
    {
        $userid = auth()->user()->id;

        return view('reporting.paymentReport' , compact('userid'));
    }

    public function accessLog()
    {
        $userid = auth()->user()->id;

        return view('reporting.accessLog' , compact('userid'));
    }

    public function auditTrail()
    {
        $userid = auth()->user()->id;

        return view('reporting.auditTrail' , compact('userid'));
    }

    public function paymentDetails()
    {
        $userid = auth()->user()->id;

        return view('reporting.paymentDetails' , compact('userid'));
    }

    public function trainingDetails($id)
    {
        $userid = auth()->user()->id;
        $trainerList = DB::table('trainings')
        ->where('trainings.id', '=', $id)
        ->join('training_trainer', 'training_trainer.id_training', '=', 'trainings.id')
        ->join('trainer', 'trainer.id', '=', 'training_trainer.id_trainer')
        ->join('users', 'users.id', '=', 'trainer.id_users')
        ->select(
            'users.fullname',
        )
        ->get();

        $training = DB::table('trainings')
        ->where('trainings.id', '=', $id)
        ->join('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->join('training_type','training_type.id','=','training_course.id_training_type')
        ->join('course_category','course_category.id','=','training_course.id_course_category')
        ->join('company', 'company.id', '=', 'trainings.id_company')
        ->join('training_mode', 'training_mode.id', '=', 'trainings.training_mode')
        ->select(
            'trainings.id',
            'training_course.course_name as course_name',
            DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
            DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
            'trainings.training_code',
            'trainings.venue',
            'trainings.total_participant',
            'trainings.training_mode',
            'trainings.training_description',
            'trainings.state',
            'trainings.venue_address',
            'training_type.name as type_name',
            'trainings.start_time',
            'trainings.end_time',
            'company.name as company_name',
            'trainings.batch_no_full',
            'training_mode.mode as mode_name',
            'trainings.start_time',
            'trainings.end_time',
            'course_category.course_name as course_category',
        )
        ->first();

        return view('reporting.trainingDetails' , compact('userid' , 'training', 'trainerList', ));
    }

    public function trainingReportingTP()
    {
        $userid = auth()->user()->id;

        return view('reporting.TPtrainingReport' , compact('userid'));
    }
}
