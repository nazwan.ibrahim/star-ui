<?php

namespace App\Http\Controllers\Module\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\AuditTrail;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Training;
use App\Models\Application;
use App\Models\Terms;
use Illuminate\Support\Facades\DB;
use App\Models\TrainingCourse;
use App\Models\courseReType;
use App\Models\Participant;
use App\Models\Participant_Attendance;
use App\Models\Feedback_question;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use App\Mail\PendingVerification;
use App\Mail\PendingApplication;
use App\Mail\PendingApplicationTP;
use Exception;
use Illuminate\Mail\PendingMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth')->except('publicDashboard','returnPage');
    }

    public function paymentError()
    {
        $userid = auth()->user()->id;
        
        return view('errors.paymentError',compact('userid'));
    } 

    public function returnPage(Request $request) 
    {
        $transactionId = $request->get('transid');
        $status = $request->get('status');
        $reason = $request->get('reson');

        $data = $request->session()->all();

// dd($data);


        try {
           
            $merchant =  DB::table('payment_merchant')
            ->select(
                'merchant_tranid',
                'pay_type',
                'id_users',
                'amount'
            )
            ->where('merchant_tranid', '=', $transactionId)
            ->first();

            // dd($merchant->id_users);

            if ($merchant->pay_type == 'Training') {
                return redirect()->route('participant-Payment-Invoice',$merchant->id_users );
            } else if ($merchant->pay_type == 'Book') {
                return redirect()->route('book-Payment-Invoice');
            } 

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {

            return view('errors.returnPage');
        }
    }
    

    public function participantDashboard()
    {
        $userid = auth()->user()->id;
        
        return view('dashboard.participant.participantDashboard',compact('userid'));
    } 

    public function participantTrainingDetails($id){
        $userid = auth()->user()->id;
        
        $detailsTraining =  DB::table('trainings')
        ->select([
            'training_course.course_name',
            'trainings.id',
            DB::raw('DATE_FORMAT(trainings.date_start, "%d/%m/%Y") as date_start'),
            DB::raw('DATE_FORMAT(trainings.date_end, "%d/%m/%Y") as date_end'),
            'venue_address',
            'company.name as provider_name',
            'training_course.new_fee as training_fee',
            'training_course.new_fee_int as training_fee_int',
            'trainings.is_paid',
            'trainings.is_resit',

        ])
        ->leftJoin('training_course','trainings.id_training_course','=','training_course.id_training_course')
        ->leftJoin('training_partner_course','training_partner_course.id_training_course','=','training_course.id_training_course')
        ->leftJoin('company', 'company.id', '=', 'training_partner_course.id_company')
       // ->leftJoin('users', 'users.id', '=', 'training_participant.id_users')
        ->where('trainings.id', '=', $id)
        ->get();


        $detailsTrainingParticipant =  DB::table('training_participant')
        ->where('training_participant.id_training', '=', $id)
        ->where('training_participant.id_users', '=', $userid)
        ->select([
            'training_participant.id as id_training_participant',
            'training_participant.id_training',
            'training_participant.id_users',
            'training_participant.status_payment',
            'training_participant.status_attend',
            'training_participant.status_feedback',
            'training_participant.status_certificate',
            'trainings.id',
            'training_course.course_name',
            'trainings.is_paid',
            DB::raw('DATE_FORMAT(trainings.date_start, "%d/%m/%Y") as date_attend'),
            DB::raw('DATE_FORMAT(trainings.start_time, "%d/%m/%Y") as time_attend'),
            'participant_payment.id as payment_id',
            'participant_examresult.final_verdict_status', 
            'participant_certificate.certificate_no'
        ])
        
        
        ->leftJoin('participant_attendance', 'participant_attendance.id_training_participant', '=', 'training_participant.id')
        ->leftJoin('participant_examresult', 'participant_examresult.id_training_participant', '=', 'training_participant.id') 
        ->leftJoin('trainings','training_participant.id_training','=','trainings.id')
        ->leftJoin('training_course','trainings.id_training_course','=','training_course.id_training_course')
        ->leftJoin('users','users.id','=','training_participant.id_users')
        ->leftJoin('participant_payment', 'participant_payment.id_training_participant', '=', 'training_participant.id') 
        ->leftJoin('participant_certificate','participant_certificate.id_training_participant','=','training_participant.id')
        ->groupby('trainings.id')
        ->groupby('training_participant.id_users')
        ->get();

        // dd($detailsTraining);
        foreach($detailsTraining as $detailTraining){
            $course_name = $detailTraining->course_name;
            $training_startdate = $detailTraining->date_start;
            $training_enddate = $detailTraining->date_end;
            $training_venue = $detailTraining->venue_address;
            $training_provider = $detailTraining->provider_name;
            $training_fee = $detailTraining->training_fee;
            $training_fee_int = $detailTraining->training_fee_int;
            $is_paid = $detailTraining->is_paid;
            $is_resit = $detailTraining->is_resit;

        }

        if($is_paid == 1){
            $training_fee;
        }else{
            $training_fee = 0;
        }


        return view('dashboard.participant.participantTrainingDetails',compact('userid','course_name','training_provider','training_startdate',
        'training_enddate','training_venue','detailsTrainingParticipant','training_fee','training_fee_int'));
    }
    

    public function participantToMakePayment($id){

        $userid = auth()->user()->id;

        $detailsTraining =  DB::table('trainings')
        ->select([
            'training_course.course_name as training_name',
            'trainings.id',
            DB::raw('DATE_FORMAT(trainings.date_start, "%d/%m/%Y") as date_start'),
            DB::raw('DATE_FORMAT(trainings.date_end, "%d/%m/%Y") as date_end'),
            'venue_address',
            'company.name as provider_name',
            'training_course.new_fee as training_fee',
            'training_course.new_fee_int as training_fee_int',
            'users.fullname as participant_name',
            'training_option.option',
            'status_approval.status_name',
            'salutation.salutation',
            'nationality.nationality'

        ])
        ->leftJoin('company', 'company.id', '=', 'trainings.id_company')
        ->leftJoin('training_participant', 'trainings.id', '=', 'training_participant.id_training')
        ->leftJoin('training_course','trainings.id_training_course','=','training_course.id_training_course')
        ->leftJoin('training_option','training_course.id_training_option','=','training_option.id')
        ->leftJoin('training_type','training_course.id_training_type','=','training_type.id')
        ->leftJoin('users', 'users.id', '=', 'training_participant.id_users')
        ->leftJoin('salutation', 'users.id_salutation', '=', 'salutation.id')
        ->leftJoin('nationality', 'users.id_nationality', '=', 'nationality.id_nationality')
        ->leftJoin('status_approval', 'training_participant.id_approval_status', '=', 'status_approval.id')
        ->where('trainings.id', '=', $id)
        ->where('training_participant.id_users', '=', $userid)
        ->get();
        
        
        foreach($detailsTraining as $detailTraining){
            $participant_name = $detailTraining->participant_name;
            $training_name = $detailTraining->training_name;
            $training_startdate = $detailTraining->date_start;
            $training_enddate = $detailTraining->date_end;
            $training_venue_address = $detailTraining->venue_address;
            $training_provider = $detailTraining->provider_name;
            $training_fee = $detailTraining->training_fee;
            $training_fee_int = $detailTraining->training_fee_int;
            $participant_name = $detailTraining->participant_name;
            $training_option = $detailTraining->option;
            $status_name = $detailTraining->status_name;
            $salutation = $detailTraining->salutation;
            $nationality = $detailTraining->nationality;

        }

        if($nationality == 'MALAYSIAN'){
            $training_fees = $training_fee;
          
        }else{
            $training_fees = $training_fee_int;
        }

        if($training_option == 'Training Partner'){
            return view('dashboard.participant.participantToTP',compact('userid','training_name','training_provider','training_startdate',
            'training_enddate','training_venue_address','training_fees','participant_name','status_name','salutation'));

        }else{
            return view('dashboard.participant.participantToMakePayment',compact('userid','training_name','training_provider','training_startdate',
            'training_enddate','training_venue_address','training_fees','participant_name','status_name','salutation'));
        }

        
    }

    public function participantPaymentDetails($id){

        // dd($id);
        $userid = auth()->user()->id;
        // dd($userid);
        $detailsTrainingParticipant =  DB::table('training_participant')
        ->select([
            'training_participant.id as id_training_participant',
            'training_participant.id_training',
            'training_participant.id_users',
            'users.fullname as participant_name',
            'users.email',
            'users.phone_no',
            'users.address_1','users.address_2','users.postcode',
            'state.name as state',
            'country.nicename as country',
            'trainings.id',
            'training_course.course_name as training_name',
            DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
            DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
            'trainings.training_code',
            'trainings.venue',
            'trainings.fee as training_fee',
            'trainings.total_participant',
            'trainings.training_mode',
            'trainings.training_description',
            'trainings.venue_address',
            'training_type.name as training_type',
            'trainings.start_time',
            'trainings.end_time',
            'trainings.fee',
            'company.name as provider_name',
            'training_course.new_fee as training_fee',
            'training_course.new_fee_int as training_fee_int'

        ])
        ->leftJoin('trainings', 'trainings.id', '=', 'training_participant.id_training')
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        ->leftJoin('company', 'company.id', '=', 'trainings.id_company')
        ->leftJoin('users', 'users.id', '=', 'training_participant.id_users')
        ->leftJoin('country', 'country.id', '=', 'users.id_country')
        ->leftJoin('state','state.id','=','users.id_state')
        ->where('training_participant.id_training', '=', $id)
        ->where('training_participant.id_users', '=', $userid)
        ->first();
            // dd($detailsTrainingParticipant );

        return view('dashboard.participant.participantPaymentDetails',compact('userid','detailsTrainingParticipant','id'));
    }
    
    public function processPayment($id){
        $userid = auth()->user()->id;
        $remarks = request()->input('remarks');

        $details =  DB::table('training_participant')
        ->select([
            'training_participant.id as id_training_participant',
            'training_participant.id_training',
            'training_participant.id_users',
            'users.fullname',
            'users.email',
            'users.phone_no',
            'users.address_1','users.address_2','users.postcode',
            'state.name as state',
            'country.nicename as country',
            'trainings.id',
            DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
            DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
            'trainings.training_code',
            'trainings.venue',
            'training_course.new_fee as training_fee',
            'trainings.total_participant',
            'trainings.training_mode',
            'trainings.training_description',
            'trainings.venue_address',
            'training_type.name as training_type',
            'trainings.start_time',
            'trainings.end_time',
            'training_course.course_name as training_name',
            'trainings.fee',
            'company.name as provider_name',
        ])
        ->leftJoin('trainings', 'trainings.id', '=', 'training_participant.id_training')
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        ->leftJoin('company', 'company.id', '=', 'trainings.id_company')
        ->leftJoin('users', 'users.id', '=', 'training_participant.id_users')
        ->leftJoin('country', 'country.id', '=', 'users.id_country')
        ->leftJoin('state','state.id','=','users.id_state')
        ->where('training_participant.id_training', '=', $id)
        ->where('training_participant.id_users', '=', $userid)
        ->first();

        try {
            if ($details->country && $details->address_1) {
    
                $DATA = [
                    'merchant_acc_no' => env('MERCHANT_ACC'),
                ];
    
                $response = Http::withOptions([
                    'verify' => false,
                ])->withHeaders([
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ])->timeout(60)->asForm()->post('https://efit.seda.gov.my/pgwy/transid.php', $DATA);
    
                $response->throw(); // Throw an exception for non-successful responses
                    
                //create db transaction merchant_tranid
                $merchant = DB::table('payment_merchant')
                ->insert([
                        'merchant_tranid' => $response,
                        'pay_type' => 'Training',
                        'amount' => $details->training_fee,
                        'id_users' => $details->id_users,
                        'created_by' => $userid,
                        'created_at' => Carbon::now(),

                ]);

                $responseData = $response->body();
    
                if (isset($responseData)) {
                    $transaction = [
                            "internal_merchant_account" => env('MERCHANT_ACC'),
                            "req_merchant_tranid" => $responseData, 
                            "internal_module_id" => "star-dev",
                            "req_amount" => $details->training_fee,
                            "req_settlement_remarks" => "Transaction",
                            "req_txn_desc" => 'Training',
                            "req_customer_id" => $details->id,
                            "req_fr_highrisk_email" => $details->email,
                            "req_fr_highrisk_country" => $details->country,
                            "req_fr_billing_address" => $details->address_1 . ', ' . $details->address_2,
                            "req_fr_shipping_address" => $details->address_1 . ', ' . $details->address_2,
                            "req_fr_shipping_cost" => "0.00",
                            "req_fr_customer_ip" => "202.188.0.1"
                        ];

                        $response2 = Http::withOptions([
                            'verify' => false,
                        ])->withHeaders([
                            'Content-Type' => 'application/x-www-form-urlencoded',
                        ])->timeout(30)->asForm()->post('https://efit.seda.gov.my/pgwy/index.php', $transaction);
        
                        $response2->throw(); // Throw an exception for non-successful responses
        
                        $responseData2 = $response2->body();
        
                        return $responseData2;
                    } else {
                        return redirect()->back()->with('error', 'Failed To Make Payment.');
                    }
                } else {
                    return redirect()->back()->with('error', 'Please Update Your Profile');
                }
            } catch (ConnectionException $e) {
                return redirect()->back()->with('error', 'Request Timed Out: ' . $e->getMessage());
            } catch (\Illuminate\Http\Client\RequestException $e) {
                return redirect()->back()->with('error', 'Failed To Make Payment: ' . $e->getMessage());
            }
    }
   
    public function participantPaymentInvoice(Request $request,$id)
    {   

      
           // Retrieve the payment token from the query parameters
        // $token = $request->query('SECURE_SIGNATURE');

        // $paymentData = session('paymentData');

        // dd($paymentData);

        $userid = auth()->user()->id;
        $details = DB::table('training_participant')
        ->join('trainings','trainings.id','=','training_participant.id_training')
        ->leftjoin('users','users.id','=','training_participant.id_users')
        ->leftjoin('country','country.id','=','users.id_country')
        ->where('users.id','=',$userid)
        ->select(
            'users.fullname',
            'users.email',
            'users.address_1',
            'users.address_2',
            'country.name as countryname',
            'users.id',
            DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
            DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
            'trainings.venue',
            'trainings.fee',
        )
        ->first();
        // $DATA = [   
        //     "username" => 'api@seda.gov.my',
        //     "password" => '123456',
        //     "grant_type" => 'password',
        //     "userassignmentid" => '100'
        // ];

        // $response = Http::withOptions([
        //     'verify' => false,
        // ])->withHeaders([
        //     'Content-Type' => 'application/x-www-form-urlencoded',
        //     'Authorization' => 'Bearer ' . $paymentToken,
        // ])->asForm()->post('https://uat.officecentralcloud.com/token', $DATA);
        // $responseData = json_decode($response->body(), true); // Decode the JSON into an array

        // if (isset($responseData['access_token'])) {
        //     $accessToken = $responseData['access_token'];
                
        
        // $resit = [
        //     "invoiceno" => "INV/2015/1",
        //     "receiptdate" => "2023-01-01",
        //     "subject" => "Baja Tumbuh-tumbuhan",
        //     "accountingaccountcode" => "SF-A0120201",
        //     "accountname" => "Customer A",
        //     "accountbrn" => "123456-I",
        //     "accountgstno" => "",
        //     "currencycode" => "MYR",
        //     "accountstreet1" => "No 1, Jalan 1/3P",
        //     "accountstreet2" => "Seksyen 1 Tambahan",
        //     "accountpostcode" => "43650",
        //     "accountcity" => "Bandar Baru Bangi",
        //     "accountstate" => "Selangor",
        //     "accountcountry" => "Malaysia",
        //     "accountinginvoicedetails" => [
        //         [
        //             "msiccode" => "", // optional
        //             "itemcode" => "",
        //             "itemdescription" => "Baja",
        //             "quantity" => 1,
        //             "priceperunit" => 300.00,
        //             "amountexcltax" => 300.00,
        //             "taxamount" => 18.00,
        //             "amountincltax" => 318.00,
        //             "taxcode" => ""
        //         ]
        //     ],
        //     "accountingtransactiondetails" => [
        //         [
        //             "accountingaccountcode" => "SF-H1110601", // DEBIT bank glcode
        //             "debit" => 318.00, // inclusive of tax if any
        //             "credit" => 0
        //         ],
        //         [
        //             "accountingaccountcode" => "SF-H1110601", // Credit item glcode
        //             "debit" => 0,
        //             "credit" => 18.00 // inclusive of tax if any
        //         ],
        //         [
        //             "accountingaccountcode" => "SF-H1110601",
        //             "debit" => 0,
        //             "credit" => 300.00 // inclusive of tax if any
        //         ]
        //     ]
        // ];

        // $response2 = Http::withOptions([
        //     'verify' => false,
        // ])->withHeaders([
        //     'Authorization' => 'Bearer '. $accessToken,
        //     'Content-Type' => 'application/json',
        //     'Accept' => 'application/json',
        // ])->post('https://uat.officecentralcloud.com/api/accountingreceipts/create', $resit);

        // $responseData2 = json_decode($response2->body(), true); // Decode the JSON into an array
        //     // dd($responseData2);
        // if($responseData2['isSuccess']==true){
            return view('dashboard.participant.participantPaymentInvoice',compact('userid'));
        // }else{
        //     dd("Failed to send receipt.");
        // }

    // }else{
    //     dd("Access token not found in the response.");
    // }

        
        // return view('dashboard.participant.participantPaymentInvoice',compact('userid'));


    }

    public function participantApply($id)
    {

        // dd(now());
        $userid = auth()->user()->id;

        $training = DB::table('trainings')
        ->where('trainings.id', '=', $id)
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        ->leftjoin('training_mode','training_mode.id','=','trainings.training_mode')
        ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        ->leftjoin('company','company.id','=','trainings.id_company')
        ->select(
            'trainings.id',
            'training_course.course_name',
            'course_category.course_name as course_category',
            DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
            DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
            'trainings.training_code',
            'trainings.venue',
            'trainings.total_participant',
            'training_mode.mode',
            'trainings.training_description',
            'trainings.venue_address',
            'training_type.name as training_type',
            'trainings.id_training_app_status',
            'trainings.start_time',
            'trainings.end_time',
            'company.name as companyname',
            'trainings.training_image'
        )
        ->first();

        $feeList = DB::table('trainings')
        ->where('trainings.id', '=', $id)
        ->join('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->join('resit_fee', 'resit_fee.id_training_course', '=', 'training_course.id_training_course')
        ->join('fee_type','fee_type.id_fee','=','resit_fee.id_fee')
        ->select(
            'resit_fee.amount',
            'fee_type.fee_name',
            'resit_fee.fee_category',
            )
        ->get();

        if($training->id_training_app_status == 4){
            $train = 'Not Started';
        }elseif($training->id_training_app_status == 1){
            $train = 'Open';

        }elseif($training->id_training_app_status == 2){
            $train = 'Closed';

        }elseif($training->id_training_app_status == 3){
            $train = 'Fully Booked';
        }

        // dd( $training);
        $trainer = DB::table('training_trainer')
        ->where('training_trainer.id_training', '=', $id)
        ->leftjoin('trainer','trainer.id','=','training_trainer.id_trainer')
        ->leftjoin('trainings','trainings.id','=','training_trainer.id_training')
        ->leftjoin('users','users.id','=','trainer.id_users')
        ->select(
            'users.fullname',
        )
        ->groupBy('trainer.id_users')
        ->get();
        // dd($trainer);

        $adminTP = DB::table('trainings')
        ->where('trainings.id', '=', $id)
        ->join('company','company.id','=','trainings.id_company')
        ->select(
            'company.*',
        )
        ->get();

        $objectives = DB::table('trainings')
        ->where('trainings.id', '=', $id)
        ->join('training_objective','training_objective.id_training','=','trainings.id')
        ->select(
            'training_objective.objective',
        )
        ->get();

        $detailsParticipant =  DB::table('users')
        ->select(
            'users.fullname',
            'users.id',
            'users.email',
            'users.phone_no',
            'users.address_1','users.address_2','users.postcode',
            'state.name as state',
            'country.nicename as country',
            'education.qualification',
            'education_level.education_level',
           
        )

        ->leftJoin('country', 'country.id', '=', 'users.id_country')
        ->leftJoin('state','state.id','=','users.id_state')  
        ->leftJoin('education', 'education.id_users', '=', 'users.id') 
        ->leftJoin('education_level', 'education.id_education_level', '=', 'education.id')       
        ->where('users.id', '=', $userid)
        ->first();

        $participantT = DB::table('status_approval')
        ->select(
            'id',
            'status_name',
            'status_code'

        )
        ->where('status_approval.status_code', '=', 'approved')
        ->first();

        $countParticipant = DB::table('training_participant')
        ->leftJoin('trainings', 'trainings.id', '=', 'training_participant.id_training')
        ->leftJoin('users', 'users.id', '=', 'training_participant.id_users')
        ->where('trainings.id', '=', $id)
        ->count('training_participant.id');

        $trainingapplied = DB::table('training_participant')
        ->select(
            'training_participant.id_approval_status',
            'training_participant.id_training',
            'training_participant.id_users'

        )
        ->leftJoin('trainings', 'trainings.id', '=', 'training_participant.id_training')
        ->leftJoin('users', 'users.id', '=', 'training_participant.id_users')
        ->where('training_participant.id_users', '=', $userid)
        ->where('training_participant.id_training', '=', $id)
        ->first();

        $now = now();

        if($trainingapplied != null){
            return redirect()->route('participant.dashboard')->with('error', 'You have been applied this training, Please apply another training');
        }
      
        if($detailsParticipant->fullname && $detailsParticipant->email && $detailsParticipant->phone_no && $detailsParticipant->address_1 && $detailsParticipant->qualification){
            return view('dashboard.participant.participantApply',compact('userid' , 'training','train','trainer','adminTP','objectives', 'feeList',));
          
        }else{
            return redirect()->route('user-profile', ['id' => Auth::user()->id])->with('error', 'Please Update Your Profile');
        }
    }

    public function applyForTraining(Request $request,$id)
    {
        $userid = auth()->user()->id;

        $users = DB::table('users')
        ->where('users.id', '=', $userid)
        ->select(
            'users.name',
            'users.email',
        )
        ->first();

        $trainings = DB::table('trainings')
        ->where('trainings.id', '=', $id)
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        ->leftjoin('training_option','training_option.id','=','training_course.id_training_option')
        ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        ->leftjoin('company','company.id','=','trainings.id_company')
        ->select(
            'trainings.id',
            'training_course.course_name as course_name',
            DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
            DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
            'trainings.training_code',
            'trainings.venue',
            'trainings.fee',
            'trainings.total_participant',
            'trainings.training_mode',
            'trainings.training_description',
            'trainings.venue_address',
            'training_option.option',
            'training_type.name as training_type',
            'trainings.start_time',
            'trainings.end_time',
            'company.name as tpName',
            'company.email as companyemail'

        )
        ->get();

        // dd($trainings);
        // if(){

        // }
        // dd($userid);
    try{

        $username = auth()->user()->fullname;
      $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Applied for Training ' . $id,
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

        
            $application = Application::create([
                'id_users' => $userid,
                'id_training' => $id,
                'id_approval_status' => 1,
                'training_status' => 10,
                'created_at' => now(),
                'created_by' => $userid
            ]);
    
            $applicationId = $application->id;
            $terms = Terms::create([
                'id_training_participant' => $applicationId,
                'cidb'=> $request->input('cidb'),
                'age'=>$request->input('age'),
                'payment_proof' =>$request->input('payment_proof'),
                'safety_course'=> $request->input('safety_course'),
                'safety_course_name'=> $request->input('safety_course_name'),
                'safety_start_date'=> $request->input('safety_start_date'),
                'math_education'=> $request->input('math_education'),
                'highest_education'=> $request->input('highest_education'),
                'others_education'=>$request->input('other_education'),
                'edu_start_date'=>$request->input('edu_start_date'),
                'qualification'=>$request->input('qualification'),
                'safe_knowledge'=>$request->input('safe_knowledge'),
                'math_knowledge'=>$request->input('math_knowledge'),
                'read_knowledge'=>$request->input('read_knowledge'),
                'understanding_knowledge'=>$request->input('understanding_knowledge'),
                'health_insurance'=>$request->input('health_insurance'),
                'safety_priority'=>$request->input('safety_priority'),
                'rules'=>$request->input('rules'),
                'electric_formula'=>$request->input('electric_formula'),
                'working_knowledge'=>$request->input('working_knowledge'),
                'declaration'=>$request->input('declaration'),
                'status'=>1,
                'created_at' => now(),
                'created_by' => $userid ,
                'updated_at' => now(),
            ]);

            Mail::to($trainings->email)->send(new PendingVerification($trainings));

            return redirect()->route('participant.dashboard')->with('success', 'Success to apply');

        
        // dd($terms);

        // if($trainings->training_mode == 'Inhouse'){
        //     Mail::to($user->email)->send(new PendingVerification($user,$trainings));
        //     Mail::to($administrator->email)->send(new PendingApplication($administrator,$trainings));

        // }else{
        //     Mail::to($user->email)->send(new PendingVerification($user,$trainings));
        //     Mail::to($trainings->companyemail)->send(new PendingApplicationTP($trainings));

        // }



        }catch (\Exception $e) {
            return redirect()->route('participant.dashboard')->with('error', 'Please try again later');
        }
    }


    
    public function participantFeedback($id)
    {
        $userid = auth()->user()->id;

        $tngparticipant = DB::table('training_participant')
        ->where('training_participant.id_users', '=', $userid)
        ->where('training_participant.id_training', '=', $id)
        ->select('training_participant.id AS id_training_participant','training_participant.id AS id_training',
       )
       ->get();
        
       $id_t_p = $tngparticipant[0]->id_training_participant;
       $id_training  = $tngparticipant[0]->id_training;

        $training = DB::table('trainings')
        ->where('trainings.id', '=', $id)
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        ->select('trainings.id','training_course.course_name as training_name',
        'course_category.course_name as category_name',
        'training_type.name as type_name',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
        'trainings.id_training_status as training_status',
        )
        ->whereNull('trainings.deleted_at')
        ->get();

        foreach($training as $train){
            $id_training = $train->id;
            $name = $train->training_name;
            $category_name = $train->category_name;
            $type_name = $train->type_name;
        }

        $feedbackquestion = DB::table('feedback_question')
        //->leftjoin('trainings','trainings.id','=','feedback_question.id_training')
        //->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        
        ->leftjoin('participant_feedback','participant_feedback.id_feedback_question','=','feedback_question.id')
        ->leftjoin('training_participant','training_participant.id','=','participant_feedback.id_training_participant')
        ->select(
        //'trainings.id','training_course.course_name as training_name',
        //'training_participant.id_training',
      //  DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
       // DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
       // 'trainings.id_training_status as training_status',
        'feedback_question.id as questionid','feedback_question.question',
        'participant_feedback.feedback as feedback'
        )
       // ->where('trainings.id', '=', $id)
        ->whereNull('feedback_question.deleted_at')
        ->groupby('feedback_question.id')
        ->get();



        return view('dashboard.participant.participantFeedback',compact('id_training','id_t_p','userid','feedbackquestion','training','name','category_name','type_name','id_training'));
    }

    public function postParticipantFeedback(Request $request)
    {
        
    }
    
    public function adminDashboard()
    {
        return view('dashboard.admin.adminDashboard');
    }

    public function trainingPartnerDashboard()
    {
        return view('dashboard.trainingPartner.trainingPartnerDashboard');
    }

    public function publicDashboard()
    {
        $dashboardList = DB::table('trainings')
        ->whereNotNull('date_start')
        ->whereNotNull('date_end')
        ->join('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->join('training_type','training_type.id','=','training_course.id_training_type')
        ->select(
            'training_course.course_name',
            'training_type.name',
            DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
            DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
            'trainings.venue',
            'trainings.fee',
        )
        ->get();

        return view('dashboard.public.publicDashboard' , compact('dashboardList'));
    }

    public function trainerDashboard()
    {

        $trainerdata = Training::join('users', 'users.id', '=', 'trainings.id_company')
                        ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
                        ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
                        ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
                        ->leftjoin('status_training', 'status_training.id', '=', 'trainings.id_training_status')
                        ->leftjoin('company', 'company.id', '=', 'users.id_company')
                        ->get([ 'company.name as partner','training_course.course_name as training_name','trainings.date_start','trainings.date_end','status_training.status_name','training_type.name as type','course_category.course_name']);
                        
                        // dd($trainerdata);
        return view('dashboard.trainer.trainerDashboard',compact('trainerdata'))->with('i', (request()->input('page', 1) - 1) * 10);        
    }

    public function TPAttendanceList()
    {
        $userid = auth()->user()->id;
        return view('dashboard.trainingPartner.trainingSchedule.attendanceList', compact('userid'));
    } 

    public function TPUpdateExamResult()
    {
        $userid = auth()->user()->id;
        return view('dashboard.trainingPartner.trainingSchedule.updateExamResult', compact('userid'));
    } 

    public function TPTrainingReport()
    {
        $userid = auth()->user()->id;
        return view('dashboard.trainingPartner.trainingSchedule.trainingReport', compact('userid'));
    }


    public function TPTrainingScheduleHistory()
    {
        $userid = auth()->user()->id;
        $user_type= auth()->user()->user_type;
        return view('dashboard.trainingPartner.trainingSchedule.trainingScheduleAndHistory', compact('userid','user_type'));             
    }
    
    public function trainerAttendanceList()
    {
        $userid = auth()->user()->id;
        return view('dashboard.trainer.trainerSchedule.attendanceList', compact('userid'));
               
    } 

    public function trainerUpdateExamResult()
    {
        $userid = auth()->user()->id;
        return view('dashboard.trainer.trainerSchedule.updateExamResult', compact('userid'));
                     
    } 

    public function trainerTrainingReport()
    {
        $userid = auth()->user()->id;
        return view('dashboard.trainer.trainerSchedule.trainingReport', compact('userid'));                  
    } 

    public function trainerTrainingScheduleHistory()
    {
        $userid = auth()->user()->id;
        return view('dashboard.trainer.trainerSchedule.trainingScheduleAndHistory', compact('userid'));  
         
    }

    
}
