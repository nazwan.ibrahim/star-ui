<?php

namespace App\Http\Controllers\Module\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\View\View;
use App\Models\Country;
use App\Models\State;
use App\Models\Upload_Document;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationConfirmation;
use App\Models\Company;
use App\Models\ExamConfiguration;
use App\Models\ExamResultHeader;
use App\Models\ExamTraining;
use App\Models\Loa;
use Exception;
use App\Models\UploadDocuments;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Http;

class ParameterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function trainingCertificateCode(){
        $userid = auth()->user()->id;

        $certificateCode = DB::table('training_cert_code')
        ->get();
        return view('admin.parameter.trainingCertificateCode',compact('userid','certificateCode'));
    }

    public function addTrainingCertificateCode(){
        $userid = auth()->user()->id;
        return view('admin.parameter.trainingCertificateCode',compact('userid'));
    }

}