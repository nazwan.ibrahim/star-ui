<?php

namespace App\Http\Controllers\Module\Admin;

use App\Http\Controllers\Controller;
use App\Imports\UsersImport;
use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\View\View;
use App\Models\Country;
use App\Models\State;
use App\Models\Upload_Document;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationConfirmation;
use App\Models\Company;
use App\Models\ExamConfiguration;
use App\Models\ExamResultHeader;
use App\Models\ExamGCPVResultHeader;
use App\Models\ExamTraining;
use App\Models\Loa;
use App\Models\Postcode;
use Exception;
use App\Models\UploadDocuments;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Http;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Trainer;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function userRegistrationManagement(): View
    {

        $userid = auth()->user()->id;
        return view('userRegistration.userRegistrationManagement', compact('userid'));
    }

    public function UploadBulkRegistration(Request $request)
    {
        $userid = auth()->user()->id;

        $request->validate([
            'uploadBulk' => 'required|mimes:xlsx,xls,csv|max:2048'
        ]);

        $relatedModel = new Upload_Document();
        if ($request->hasFile('uploadBulk')) {
            $uploadedFile = $request->file('uploadBulk');

            $fileName = $uploadedFile->getClientOriginalName();
            $extension = $uploadedFile->getClientOriginalExtension();
            $filePath = $uploadedFile->storeAs('uploads', $fileName, 'public');
            

            $relatedModel::create([
                'file_name' => $fileName,
                'file_path' => $filePath,
                'file_format' => $extension,
                'docType' => 'uploadBulk',
                'id_users' => $userid,
                'status' => 1,
                'created_at' => now(),
                'created_by' => $userid
            ]);
        }

        return redirect()->route('admin.user-management')->with('success', 'Success to upload');
    }


    public function registerTrainingPartner()
    {

        $userid = auth()->user()->id;
        // dd($userid);
        $userType = auth()->user()->user_type;


        $roles = DB::table('user_role')->get();
        $companies = DB::table('company')->get();

        $countries = DB::table('country')->get();
        $states = DB::table('state')->get();
        // dd($states);
        return view('userRegistration.registerTrainingPartner', compact('userid', 'countries', 'states', 'roles', 'companies', 'userType'));
    }

    // public function getStates(Request $request, $countryId)
    // {
    //     $states = State::where('id_country', $countryId)->get();

    //     return response()->json($states);
    // }

    public function getPostcode(Request $request, $postcode)
    {
        $postcode = DB::table('postcode')
        ->where('postcode','=', $postcode)
        ->leftJoin('city', 'city.id', '=', 'postcode.id_city')
        ->leftJoin('state', 'state.id', '=', 'postcode.id_state')
        ->leftJoin('country', 'country.id', '=', 'postcode.id_country')
        ->select(
            'postcode.id_postcode',
            'postcode.postcode',
            'postcode.id_city', 
            'city.id as city_id',
            'city.cityname',
            'postcode.id_state',
            'state.id as state_id',
            'state.name as state',
            'postcode.id_country', 
            'country.id as country_id',
            'country.name as country'
        )
        ->first();
    

        // dd($postcode);
    

        return response()->json($postcode);
    }

    public function getCountry(Request $request, $countryId)
    {
        $postcode = DB::table('postcode')
        ->where('id_country','=', $countryId)
        ->leftJoin('city', 'city.id', '=', 'postcode.id_city')
        ->leftJoin('state', 'state.id', '=', 'postcode.id_state')
        ->leftJoin('country', 'country.id', '=', 'postcode.id_country')
        ->select(
            'postcode.id_postcode',
            'postcode.postcode',
            'postcode.id_city', 
            'city.id as city_id',
            'city.cityname',
            'postcode.id_state',
            'state.id as state_id',
            'state.name as state',
            'postcode.id_country', 
            'country.id as country_id',
            'country.name as country'
        )
        ->get();
    

        // dd($postcode);
    

        return response()->json($postcode);
    }

    public function getCity(Request $request, $citiesId)
    {
        $postcode = DB::table('postcode')
        ->where('id_city','=', $citiesId)
        ->leftJoin('city', 'city.id', '=', 'postcode.id_city')
        ->leftJoin('state', 'state.id', '=', 'postcode.id_state')
        ->leftJoin('country', 'country.id', '=', 'postcode.id_country')
        ->select(
            'postcode.id_postcode',
            'postcode.postcode',
            'postcode.id_city', 
            'city.id as city_id',
            'city.cityname',
            'postcode.id_state',
            'state.id as state_id',
            'state.name as state',
            'postcode.id_country', 
            'country.id as country_id',
            'country.name as country'
        )
        ->get();
    

        // dd($postcode);
    

        return response()->json($postcode);
    }

    public function getStates(Request $request, $statesId)
    {
        $postcode = DB::table('postcode')
        ->where('id_state','=', $statesId)
        ->leftJoin('city', 'city.id', '=', 'postcode.id_city')
        ->leftJoin('state', 'state.id', '=', 'postcode.id_state')
        ->leftJoin('country', 'country.id', '=', 'postcode.id_country')
        ->select(
            'postcode.id_postcode',
            'postcode.postcode',
            'postcode.id_city', 
            'city.id as city_id',
            'city.cityname',
            'postcode.id_state',
            'state.id as state_id',
            'state.name as state',
            'postcode.id_country', 
            'country.id as country_id',
            'country.name as country'
        )
        ->get();
    

        // dd($postcode);
    

        return response()->json($postcode);
    }


    private function generateRandomPassword($length = 8)
    {
        $uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $lowercase = 'abcdefghijklmnopqrstuvwxyz';
        $numbers = '0123456789';
        $symbols = '!@#$%^&*()_+';

        $password =
            Str::random(1, $uppercase) .
            Str::random(1, $lowercase) .
            Str::random(1, $numbers) .
            Str::random(1, $symbols) .
            Str::random($length - 4);

        return str_shuffle($password);
    }

    public function getQPperson(Request $request)
    {
        $userid = auth()->user()->id;

        $data = [
            'mykad_no' => $request['mykad_no'],
            'certificate_no' => $request['certificate_no'],
        ];

        $response = Http::withOptions([
            'verify' => false,
        ])->withHeaders([
            'Authorization' => 'Bearer ' . env('ACCESS_TOKEN'),
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ])->post('https://spqp-staging.seda.gov.my/api/qualified_person/certificate/verify', $data);


        $jsonData = $response->json();

        return response()->json($jsonData);
        // dd($jsonData);
        // if ($jsonData['status'] == 'OK' && !in_array(null, $jsonData, true)) {
        //     return view('userRegistration.registerTrainingPartner',compact('userid'), ['jsonData' => $jsonData]);
        // }else{
        //     return redirect()->back()->with('error', 'Record Not Found.');
        // }

    }

    public function createRegistration(Request $request)
    {

        $userid = auth()->user()->id;
        $randomPassword = $this->generateRandomPassword();
        $hashedPassword = bcrypt($randomPassword);
        $plainTextPassword = $randomPassword;

        $userType = $request->input('type_user');
        // $commonRules = [
        //     'fullname' => 'required',
        //     'email' => 'required|email|unique:users',
        // ];
        // $request->validate($commonRules);
        if ($userType == 2) {
            $request->validate([
                'email' => 'required|email|unique:users',
                'ic_no' => 'required|unique:users',
            ]);
            $user = Users::create([
                'fullname' => $request['fullname'],
                'staffno' => $request['staffno'],
                'ic_no' => $request['ic_no'],
                'phone_no' => $request['phone_no'],
                'email' => $request['email'],
                'id_country' => $request['country'],
                'id_state' => $request['state'], // Change to 'state' if it's consistent
                'password' => $hashedPassword,
                'password_confirmation' => $hashedPassword,
                'user_type' => $userType,
                'status' => 1,
                'created_by' => $userid,
                'created_at' => now(),
            ]);
        } elseif ($userType == 3) {
            $request->validate([
                'email' => 'required|email|unique:users',
            ]);
            $company = Company::create([
                'name' => $request['companyName'],
                'email' => $request['email'],
                'phone_no' => $request['phone_no'],
                'id_country' => $request['country'],
                'id_state' => $request['state'], // Change to 'state' if it's consistent
                'status' => 1,
                'created_by' => $userid,
                'created_at' => now(),
            ]);
            $user = Users::create([
                'email' => $request['email'],
                'password' => $hashedPassword,
                'password_confirmation' => $hashedPassword,
                'user_type' => $userType,
                'status' => 1,
                'created_by' => $userid,
                'created_at' => now(),
                'id_company' => $company->id,
            ]);
        } elseif ($userType == 4) {
            $request->validate([
                'email' => 'required|email|unique:users',
                'ic_no' => 'unique:users',
            ]);
            $user = Users::create([
                'fullname' => $request['fullname'],
                'certificate_no' => $request['certificate_no1'],
                'certificate_category' => $request['certificate_category'],
                'phone_no' => $request['phone_no'],
                'ic_no' => $request['ic_no'],
                'email' => $request['email'],
                'password' => $hashedPassword,
                'password_confirmation' => $hashedPassword,
                'user_type' => $userType,
                'status' => 1,
                'created_by' => $userid,
                'created_at' => now(),
            ]);
            // $trainer = Trainer::create([
            //     'id_users' =>  $user->id,
            //     'id_company' => $request['training_partner'],
            //     'status' => 1,
            //     'created_by' => $userid,
            //     'created_at' => now(),
            // ]);
        } elseif ($userType == 5) {
            $request->validate([
                'fullname' => 'required',
                'email' => 'required|email|unique:users',
            ]);
            $user = Users::create([
                'fullname' => $request['fullname'],
                'email' => $request['email'],
                'agency' => $request['company'],
                'phone_no' => $request['phone_no'],
                'password' => $hashedPassword,
                'password_confirmation' => $hashedPassword,
                'user_type' => $userType,
                'status' => 1,
                'created_by' => $userid,
                'created_at' => Carbon::now()
            ]);
        }
        if (!$user) {
            return redirect()->back()->with('error', 'Failed to register');
        } else {
            Mail::to($user->email)->send(new RegistrationConfirmation($user, $plainTextPassword));
            return redirect()->route('admin.user-management')->with('success', 'Success to register');
        }
    }

    public function deleteAttachment(Request $request)
    {
        $userid = auth()->user()->id;
        $fileId = $request->input('fileId');

        $checkFile = DB::table('upload_documents')
            ->where('id', $fileId)
            ->get();

        if (!$checkFile) {
            return response()->json(['success' => false, 'message' => 'MOU not found.']);
        }

        DB::beginTransaction();
        try {

            //Delete Attachment
            DB::table('upload_documents')
                ->where('id', $fileId)
                ->delete();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'Attachment deleted successfully.']);
        } catch (Exception $ex) {
            // dd($ex->getMessage());
            DB::rollback();

            return response()->json(['success' => false, 'message' => 'Failed to delete.']);
        }
    }

    public function removeMou(Request $request)
    {
        $userid = auth()->user()->id;
        $mouId = $request->input('mouId');

        $checkMou = DB::table('list_mou')
            ->where('id', $mouId)
            ->get();

        if (!$checkMou) {
            return response()->json(['success' => false, 'message' => 'MOU not found.']);
        }

        DB::beginTransaction();
        try {

            //Remove MOU
            DB::table('list_mou')
                ->where('id', $mouId)
                ->delete();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'MOU deleted successfully.']);
        } catch (Exception $ex) {
            // dd($ex->getMessage());
            DB::rollback();

            return response()->json(['success' => false, 'message' => 'Failed to delete.']);
        }
    }

    public function removeLoa(Request $request)
    {
        $userid = auth()->user()->id;
        $loaId = $request->input('loaId');

        $checkMou = DB::table('list_loa')
            ->where('id', $loaId)
            ->get();

        if (!$checkMou) {
            return response()->json(['success' => false, 'message' => 'LOA not found.']);
        }

        DB::beginTransaction();
        try {

            //Remove MOU
            DB::table('list_loa')
                ->where('id', $loaId)
                ->delete();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'LOA deleted successfully.']);
        } catch (Exception $ex) {
            // dd($ex->getMessage());
            DB::rollback();

            return response()->json(['success' => false, 'message' => 'Failed to delete.']);
        }
    }

    public function removeTrainer(Request $request)
    {
        $userid = auth()->user()->id;
        $userID = $request->input('userID');
        $trainerId = $request->input('trainerId');

        $id_company = DB::table('users')->where('id', $userID)->value('id_company');

        $trainer = DB::table('trainer')
            ->where('id_users', $trainerId)
            ->where('id_company', $id_company)
            ->get();

        if (!$trainer) {
            return response()->json(['success' => false, 'message' => 'Trainer not found.']);
        }

        DB::beginTransaction();
        try {

            // Delete the trainer
            DB::table('trainer')
                ->where('id_users', $trainerId)
                ->where('id_company', $id_company)
                ->update([
                    'status' => 0,
                    'deleted_by' => $userid,
                    'deleted_at' => Carbon::now()
                ]);

            DB::commit();
            return response()->json(['success' => true, 'message' => 'Trainer deleted successfully.']);
        } catch (Exception $ex) {
            // dd($ex->getMessage());
            DB::rollback();

            return response()->json(['success' => false, 'message' => 'Failed to delete.']);
        }
    }

    public function saveTrainer(Request $request)
    {
        $userid = auth()->user()->id;

        $trainerIds = $request->input('trainerIds');
        $userID = $request->input('userID');

        $id_company = DB::table('users')->where('id', $userID)->value('id_company');

        DB::beginTransaction();
        try {

            foreach ($trainerIds as $trainerId) {
                DB::table('trainer')->insert([
                    'id_users' => $trainerId,
                    'id_company' => $id_company,
                    'status' => 1,
                    'created_at' => Carbon::now(),
                    'created_by' => $userid,
                ]);
            }

            DB::commit();
            return response()->json(['success' => true, 'message' => 'Trainer saved successfully.']);
        } catch (Exception $ex) {
            // dd($ex->getMessage());
            DB::rollback();

            return response()->json(['success' => false, 'message' => 'Failed to save.']);
        }
    }

    public function viewUserRegistration($id)
    {

        $userid = auth()->user()->id;

        $viewUser = DB::table('users')
            ->where('users.id', '=', $id)
            ->leftJoin('gender AS user_gender', 'user_gender.id', '=', 'users.id_gender')
            ->leftjoin('state AS user_state', 'user_state.id', '=', 'users.id_state')
            ->leftjoin('country AS user_country', 'user_country.id', '=', 'users.id_country')
            ->leftjoin('education', 'education.id_users', '=', 'users.id')
            ->leftJoin('education_level', 'education_level.id', '=', 'education.id_education_level')
            ->leftjoin('user_profile_uploads', 'user_profile_uploads.id_users', '=', 'users.id')
            ->leftjoin('nationality', 'nationality.id_nationality', '=', 'users.id_nationality')
            ->leftjoin('company', 'company.id', '=', 'users.id_company')
            ->leftjoin('state AS company_state', 'company_state.id', '=', 'company.id_state')
            ->leftjoin('country AS company_country', 'company_country.id', '=', 'company.id_country')
            ->leftjoin('salutation', 'salutation.id', '=', 'users.id_salutation')
            ->leftjoin('user_role', 'users.user_type', '=', 'user_role.code')
            ->leftjoin('identification_type', 'identification_type.id_identity', '=', 'users.id_identity_type')

            ->select(
                'users.id_salutation',
                'salutation.salutation',
                'identification_type.identity_type',
                'users.fullname',
                'users.ic_no',
                'users.staffno',
                'users.id_gender',
                'user_gender.name AS user_gendername',
                'users.phone_no',
                'users.certificate_no',
                'users.certificate_category',
                'users.address_1',
                'users.address_2',
                'users.postcode',
                'users.email',
                'users.user_type',
                'users.position',
                'user_state.name AS user_statename',
                'user_state.id AS user_state_id',
                'user_country.name AS user_countryname',
                'user_country.id AS user_country_id',
                'users.id_company',
                'education.id AS id_edu',
                'education.qualification',
                'education.id_education_level',
                'education_level.education_level',
                'company.name AS companyname',
                'company.phone_no AS companyphone_no',
                'company.fax_no',
                'company.postcode',
                'company.address_1 AS company_address_1',
                'company.address_2 AS company_address_2',
                'company_state.name as company_state',
                'company_country.name AS company_countryname',
                'users.id_nationality as id_nationality',
                'nationality.nationality',
                'user_profile_uploads.ICpic_name',
                'user_profile_uploads.ICpic_format',
                'user_profile_uploads.ICpic_path',
                'user_profile_uploads.passportpic_name',
                'user_profile_uploads.passportpic_format',
                'user_profile_uploads.passportpic_path',
            )
            ->first();
        // dd($viewUser);

        $viewUploadedFile = DB::table('upload_documents')
            ->select('id AS file_id', 'file_name')
            ->where('id_users', '=', $id)
            ->where('id_section', '=', $viewUser->id_edu)
            ->where('docType', '=', 'edu')
            ->get()
            ->toArray();

        if ($viewUser->id_company !== null) {
            $viewMou = DB::table('list_mou as mou')
                ->leftjoin('company', 'mou.id_company', '=', 'company.id')
                ->leftJoin('upload_documents', 'upload_documents.id', '=', 'mou.id_upload')
                ->select(
                    'mou.id AS mou_id',
                    'mou.mou_name',
                    DB::raw('DATE_FORMAT(mou.start_date, "%d/%m/%Y") as mou_start_date'),
                    DB::raw('DATE_FORMAT(mou.end_date, "%d/%m/%Y") as mou_end_date'),
                    'mou.remarks as mou_remarks',
                    'upload_documents.file_name as mou_file'

                )
                ->where('company.id', $viewUser->id_company)
                ->get();

            $mouIds = $viewMou->pluck('mou_id')->toArray();

            //Attachments MOU
            $viewUploadedFileMou = DB::table('upload_documents')
                ->join('list_mou', 'upload_documents.id_section', '=', 'list_mou.id')
                ->whereIn('list_mou.id', $mouIds)
                ->where('upload_documents.id_users', '=', $id)
                ->where('upload_documents.docType', '=', 'mou')
                ->get(['upload_documents.id AS file_id', 'upload_documents.file_name', 'upload_documents.file_path', 'list_mou.id']);
        }

        // dd($viewMou);
        $viewLoa = null; // Initialize $viewMou to null
        if ($viewUser->id_company !== null) {
            $viewLoa = DB::table('list_loa as loa')
                ->leftjoin('company', 'loa.id_company', '=', 'company.id')
                ->leftJoin('upload_documents', 'upload_documents.id', '=', 'loa.id_upload')
                ->select(
                    'loa.id AS loa_id',
                    'loa.loa_name',
                    'loa.course as loa_course',
                    'loa.remarks as loa_remarks',
                    DB::raw('DATE_FORMAT(loa.start_date, "%d/%m/%Y") as loa_start_date'),
                    DB::raw('DATE_FORMAT(loa.end_date, "%d/%m/%Y") as loa_end_date'),
                    'loa.audit_facilities',
                    'upload_documents.file_name as loa_file'

                )
                ->where('company.id', $viewUser->id_company)
                ->get();

            $loaIds = $viewLoa->pluck('loa_id')->toArray();

            //Attachments LOA
            $viewUploadedFileLoa = DB::table('upload_documents')
                ->join('list_loa', 'upload_documents.id_section', '=', 'list_loa.id')
                ->whereIn('list_loa.id', $loaIds)
                ->where('upload_documents.id_users', '=', $id)
                ->where('upload_documents.docType', '=', 'loa')
                ->get(['upload_documents.id AS file_id', 'upload_documents.file_name', 'upload_documents.file_path', 'list_loa.id']);
        }

        $viewTrainer = null; // Initialize $viewMou to null
        if ($viewUser->id_company !== null) {
            $viewTrainer = DB::table('trainer as trainer')
                ->leftjoin('company', 'trainer.id_company', '=', 'company.id')
                ->leftjoin('users', 'trainer.id_users', '=', 'users.id')
                ->leftjoin('user_role', 'users.user_type', '=', 'user_role.id')
                ->select(
                    'trainer.id_users as trainer_id',
                    'users.fullname as trainer_name',
                    'users.email as trainer_email',
                    'users.position as trainer_position',
                    'user_role.role as trainer_role',
                    'users.status as trainer_status',
                )
                ->where('company.id', $viewUser->id_company)
                ->where('trainer.status', '=', '1')
                ->get();
        }

        if (!$viewUser) {
            return redirect()->route('admin.user-registration')
                ->withSuccess('No users found');
        }

        if ($viewUser->user_type == 1 || $viewUser->user_type == 2 || $viewUser->user_type == 5) {
            return view('userRegistration.viewUserRegistration', [
                'viewUser' => $viewUser,
                'viewUploadedFile' => $viewUploadedFile,
                'id' => $id
            ]);
        } elseif ($viewUser->user_type == 3) {
            return view('userRegistration.viewUserRegistrationTP', [
                'viewUser' => $viewUser,
                'viewMou' => $viewMou,
                'viewLoa' => $viewLoa,
                'viewTrainer' => $viewTrainer,
                'viewUploadedFileMou' => $viewUploadedFileMou,
                'viewUploadedFileLoa' => $viewUploadedFileLoa,
                'id' => $id
            ]);
        } elseif ($viewUser->user_type == 4) {
            return view('userRegistration.viewUserRegistrationTrainer', [
                'viewUser' => $viewUser,
                'viewUploadedFile' => $viewUploadedFile,
                'id' => $id
            ]);
        }
    }
    public function editUserRegistration($id)
    {
        $userid = auth()->user()->id;
        $countries = DB::table('country')->orderBy('country.name', 'asc')->get();
        $bumiputra = DB::table('bumiputra')->get();
        $genders = DB::table('gender')->get();
        $cities = DB::table('city')->orderBy('city.cityname', 'asc')->get();
        $states = DB::table('state')->orderBy('state.name', 'asc')->get();
        $trainers = DB::table('users')->where('user_type', '=', '4')->get();
        $salutation = DB::table('salutation')->orderBy('salutation.salutation', 'asc')->get();
        $education_level = DB::table('education_level')->orderBy('education_level_code', 'asc')->get();
        $nationality = DB::table('nationality')->orderBy('nationality', 'asc')->get();
        $identity = DB::table('identification_type')->orderBy('id_identity', 'asc')->get();

        $viewUser = DB::table('users')
            ->where('users.id', '=', $id)
            ->leftJoin('gender AS user_gender', 'user_gender.id', '=', 'users.id_gender')
            ->leftjoin('city AS user_city', 'user_city.id', '=', 'users.id_city')
            ->leftjoin('state AS user_state', 'user_state.id', '=', 'users.id_state')
            ->leftjoin('country AS user_country', 'user_country.id', '=', 'users.id_country')
            ->leftjoin('nationality', 'nationality.id_nationality', '=', 'users.id_nationality')
            ->leftjoin('education', 'education.id_users', '=', 'users.id')
            ->leftJoin('education_level', 'education_level.id', '=', 'education.id_education_level')
            ->leftjoin('user_profile_uploads', 'user_profile_uploads.id_users', '=', 'users.id')
            ->leftjoin('company', 'company.id', '=', 'users.id_company')
            ->leftjoin('state AS company_state', 'company_state.id', '=', 'company.id_state')
            ->leftjoin('country AS company_country', 'company_country.id', '=', 'company.id_country')
            ->leftjoin('city AS company_city', 'company_city.id', '=', 'company.id_city')
            ->leftjoin('salutation', 'salutation.id', '=', 'users.id_salutation')
            ->leftjoin('user_role', 'users.user_type', '=', 'user_role.code')
            ->leftjoin('identification_type', 'identification_type.id_identity', '=', 'users.id_identity_type')
            ->select(
                'users.id_salutation',
                'salutation.salutation',
                'identification_type.identity_type',
                'users.id as user_id',
                'users.fullname',
                'users.ic_no',
                'users.certificate_no',
                'users.certificate_category',
                'users.id_gender',
                'user_gender.name AS user_gendername',
                'users.phone_no',
                'users.address_1',
                'users.address_2',
                'users.postcode',
                'users.email',
                'users.position',
                'users.user_type',
                'user_city.cityname AS user_cityname',
                'user_state.name AS user_statename',
                'user_state.id AS user_state_id',
                'user_country.name AS user_countryname',
                'user_country.id AS user_country_id',
                'education.id AS id_edu',
                'education.qualification AS user_qualification',
                'education.id_education_level',
                'education_level.education_level',
                'nationality.nationality',
                'users.id_company',
                'company.name AS companyname',
                'company.phone_no AS companyphone_no',
                'company.fax_no as companyfax_no',
                'company.postcode as company_postcode',
                'company.address_1 AS company_address_1',
                'company.address_2 AS company_address_2',
                'company_state.name as company_state',
                'company_country.name AS company_country',
                'company_city.cityname AS company_city',
                'user_profile_uploads.ICpic_name AS user_ICpic_name',
                'user_profile_uploads.ICpic_format AS user_ICpic_format',
                'user_profile_uploads.ICpic_path AS user_ICpic_path',
                'user_profile_uploads.passportpic_name AS user_passportpic_name',
                'user_profile_uploads.passportpic_path AS user_passportpic_path',
                'user_profile_uploads.passportpic_format AS user_passportpic_format',
            )
            ->first();
        // dd($viewUser);

        $viewUploadedFile = DB::table('upload_documents')
            ->select('id AS file_id', 'file_name')
            ->where('id_users', '=', $id)
            ->where('id_section', '=', $viewUser->id_edu)
            ->where('docType', '=', 'edu')
            ->get()
            ->toArray();

        if ($viewUser->id_company !== null) {
            $viewMou = DB::table('list_mou as mou')
                ->leftJoin('upload_documents', 'upload_documents.id', '=', 'mou.id_upload')
                ->leftjoin('company', 'mou.id_company', '=', 'company.id')
                ->select(
                    'mou.id AS mou_id',
                    'mou.mou_name',
                    'mou.start_date AS mou_start_date',
                    'mou.end_date AS mou_end_date',
                    'mou.remarks as mou_remarks',
                    'upload_documents.file_name as mou_file'

                )
                ->where('company.id', $viewUser->id_company)
                ->get();

            $mouIds = $viewMou->pluck('mou_id')->toArray();

            //Attachments MOU
            $viewUploadedFileMou = DB::table('upload_documents')
                ->join('list_mou', 'upload_documents.id_section', '=', 'list_mou.id')
                ->whereIn('list_mou.id', $mouIds)
                ->where('upload_documents.id_users', '=', $id)
                ->where('upload_documents.docType', '=', 'mou')
                ->get(['upload_documents.id AS file_id', 'upload_documents.file_name', 'upload_documents.file_path', 'list_mou.id']);
        }

        if ($viewUser->id_company !== null) {
            $viewLoa = DB::table('list_loa as loa')
                ->leftjoin('company', 'loa.id_company', '=', 'company.id')
                ->leftJoin('upload_documents', 'upload_documents.id', '=', 'loa.id_upload')
                ->select(
                    'loa.id AS loa_id',
                    'loa.loa_name',
                    'loa.course as loa_course',
                    'loa.remarks as loa_remarks',
                    'loa.start_date as loa_start_date',
                    'loa.end_date as loa_end_date',
                    'loa.audit_facilities AS loa_audit_facilities',
                    'upload_documents.file_name as loa_file'

                )
                ->where('company.id', $viewUser->id_company)
                ->get();

            $loaIds = $viewLoa->pluck('loa_id')->toArray();

            //Attachments LOA
            $viewUploadedFileLoa = DB::table('upload_documents')
                ->join('list_loa', 'upload_documents.id_section', '=', 'list_loa.id')
                ->whereIn('list_loa.id', $loaIds)
                ->where('upload_documents.id_users', '=', $id)
                ->where('upload_documents.docType', '=', 'loa')
                ->get(['upload_documents.id AS file_id', 'upload_documents.file_name', 'upload_documents.file_path', 'list_loa.id']);
        }

        if ($viewUser->id_company !== null) {
            $viewTrainer = DB::table('trainer as trainer')
                ->leftjoin('company', 'trainer.id_company', '=', 'company.id')
                ->leftjoin('users', 'trainer.id_users', '=', 'users.id')
                ->leftjoin('user_role', 'users.user_type', '=', 'user_role.id')
                ->select(
                    'trainer.id_users as trainer_id',
                    'users.fullname as trainer_name',
                    'users.email as trainer_email',
                    'users.position as trainer_position',
                    'user_role.role as trainer_role',
                    'users.status as trainer_status',
                )
                ->where('company.id', $viewUser->id_company)
                ->where('trainer.status', '=', '1')
                ->get();
        }

        if (!$viewUser) {
            return redirect()->route('admin.user-registration')
                ->withSuccess('No users found');
        }

        if ($viewUser->user_type == 1 || $viewUser->user_type == 2 || $viewUser->user_type == 5) {
            return view('userRegistration.editUserRegistration', [
                'viewUser' => $viewUser,
                'viewUploadedFile' => $viewUploadedFile,
                'genders' => $genders,
                'countries' => $countries,
                'states' => $states,
                'cities' => $cities,
                'bumiputra' => $bumiputra,
                'salutation' => $salutation,
                'education_level' => $education_level,
                'nationality' => $nationality,
                'id' => $id,
                'identity' => $identity
            ]);
        } elseif ($viewUser->user_type == 3) {
            return view('userRegistration.editUserRegistrationTP', [
                'viewUser' => $viewUser,
                'viewMou' => $viewMou,
                'viewLoa' => $viewLoa,
                'viewUploadedFileMou' => $viewUploadedFileMou,
                'viewUploadedFileLoa' => $viewUploadedFileLoa,
                'viewTrainer' => $viewTrainer,
                'trainers' => $trainers,
                'genders' => $genders,
                'countries' => $countries,
                'cities' => $cities,
                'states' => $states,
                'bumiputra' => $bumiputra,
                'salutation' => $salutation,
                'nationality' => $nationality,
                'id' => $id,
                'identity' => $identity

            ]);
        } elseif ($viewUser->user_type == 4) {
            return view('userRegistration.editUserRegistrationTrainer', [
                'viewUploadedFile' => $viewUploadedFile,
                'viewUser' => $viewUser,
                'genders' => $genders,
                'countries' => $countries,
                'states' => $states,
                'cities' => $cities,
                'bumiputra' => $bumiputra,
                'salutation' => $salutation,
                'education_level' => $education_level,
                'nationality' => $nationality,
                'identity' => $identity,
                'id' => $id
            ]);
        }
    }

    public function updateUserRegistration(Request $request, $id)
    {
        $userid = auth()->user()->id;

        // dd($request->all());
        $user = Users::find($id);
        $id_edu = DB::table('education')->where('id_users', $id)->value('id');
        $id_company = DB::table('users')->where('id', $id)->value('id_company');

        // Check if the user exists
        if (!$user) {
            return redirect()->route('admin.user-registration')
                ->with('error', 'User not found.');
        }

        DB::beginTransaction();
        try {

            //Update user information
            DB::table('users')
                ->where('id', '=', $id)
                ->update([
                    'users.id_salutation' => $request->salutation,
                    'users.id_identity_type' => $request->identity,
                    'users.fullname' => $request->fullname,
                    'users.ic_no' => $request->ic_no,
                    'certificate_no' => $request->certificate_no,
                    'certificate_category' => $request->certificate_category,
                    'users.id_gender' => $request->gender,
                    'users.email' => $request->email,
                    'users.phone_no' => $request->phone_no,
                    'users.address_1' => $request->address_1,
                    'users.address_2' => $request->address_2,
                    'users.postcode' => $request->postcode,
                    'users.id_state' => $request->state,
                    'users.id_city' => $request->city,
                    'users.id_country' => $request->country,
                    'users.id_nationality' => $request->nationality,
                    'users.position' => $request->position,
                    'users.updated_by' => $userid,
                    'users.updated_at' => now(),
                    'users.status' => 1
                ]);

            //Update company information
            if ($request->has('companyname')) {
                if (!$id_company) {
                    $companyID = DB::table('company')
                        ->insertGetId([
                            'name' => $request->companyname,
                            'phone_no' => $request->companyphone_no,
                            'fax_no' => $request->companyfax_no,
                            'address_1' => $request->company_address_1,
                            'address_2' => $request->company_address_2,
                            'postcode' => $request->company_postcode,
                            'id_city' => $request->company_city,
                            'id_state' => $request->company_state,
                            'id_country' => $request->company_country,
                            'created_by' => $userid,
                            'created_at' => now(),
                        ]);

                    DB::table('users')
                        ->where('id', '=', $id)
                        ->update([
                            'users.id_company' => $companyID
                        ]);
                } else {
                    DB::table('company')
                        ->leftJoin('users', 'company.id', '=', 'users.id_company')
                        ->where('users.id', '=', $id)
                        ->update([
                            'company.name' => $request->companyname,
                            'company.phone_no' => $request->companyphone_no,
                            'company.fax_no' => $request->companyfax_no,
                            'company.address_1' => $request->company_address_1,
                            'company.address_2' => $request->company_address_2,
                            'company.id_city' => $request->company_city,
                            'company.id_state' => $request->company_state,
                            'company.postcode' => $request->company_postcode,
                            'company.id_country' => $request->company_country,
                            'company.updated_by' => $userid,
                            'company.updated_at' => now(),
                        ]);
                }
            }

            //Update educational information
            if ($request->has('education_level')) {

                $eduID = null;

                $id_edu = DB::table('education')->where('id_users', $id)->value('id');

                if (!$id_edu) {
                    $eduID = DB::table('education')
                        ->insertGetId([
                            'qualification' => $request->qualification,
                            'id_education_level' => $request->education_level,
                            'status' => 1,
                            'id_users' => $id,
                            'created_at' => Carbon::now(),
                            'created_by' => $userid,
                        ]);

                    //Upload supporting documents - educational information
                    if ($request->hasFile('edu_file')) {

                        $files = $request->file('edu_file');

                        foreach ($files as $file) {
                            $extension = $file->getClientOriginalExtension();
                            $filename = $file->getClientOriginalName();
                            $filePath = $file->storeAs('eduFiles', $filename, 'public');
                            $path = $file->getRealPath();
                            $data = file_get_contents($path);
                            $size = $file->getSize();

                            DB::table('upload_documents')
                                ->insert([
                                    'file_name' => $filename,
                                    'file_path' => $filePath,
                                    'file_size' => $size,
                                    'file_format' => $extension,
                                    'docType' => 'edu',
                                    'id_users' => $id,
                                    'id_Section' => $eduID,
                                    'created_by' => $userid,
                                    'created_at' => Carbon::now(),
                                    'status' => 1,
                                ]);
                        }
                    }
                } else {
                    DB::table('education')
                        ->where('id', $id_edu)
                        ->update([
                            'qualification' => $request->qualification,
                            'id_education_level' => $request->education_level,
                        ]);

                    //Upload supporting documents - educational information
                    if ($request->hasFile('edu_file')) {

                        $files = $request->file('edu_file');

                        foreach ($files as $file) {
                            $extension = $file->getClientOriginalExtension();
                            $filename = $file->getClientOriginalName();
                            $filePath = $file->storeAs('eduFiles', $filename, 'public');
                            $path = $file->getRealPath();
                            $data = file_get_contents($path);
                            $size = $file->getSize();

                            DB::table('upload_documents')
                                ->insert([
                                    'file_name' => $filename,
                                    'file_path' => $filePath,
                                    'file_size' => $size,
                                    'file_format' => $extension,
                                    'docType' => 'edu',
                                    'id_users' => $id,
                                    'id_Section' => $id_edu,
                                    'created_by' => $userid,
                                    'created_at' => Carbon::now(),
                                    'status' => 1,
                                ]);
                        }
                    }
                }
            }


            //Upload scanned IC/Passport document
            if ($request->hasFile('FailGambarIC')) {
                $files = $request->file('FailGambarIC');
                $extension = $files->getClientOriginalExtension();
                $filename = $files->getClientOriginalName();
                $filePath = $files->storeAs('identificationFile', $filename, 'public');
                $path = $files->getRealPath();
                $data = file_get_contents($path);
                $SaizFail = $files->getSize();

                $checkIC = DB::table('user_profile_uploads')
                    ->where('id_users', $request->userid)
                    ->first();

                if ($checkIC) {
                    // Update existing record
                    DB::table('user_profile_uploads')
                        ->where('user_profile_uploads.id_users', '=', $request->id)
                        ->update([
                            'ICpic_name' => $filename,
                            'ICpic_path' => $filePath,
                            'ICpic_size' => $SaizFail,
                            'ICpic_format' => $extension,
                            'updated_by' => $userid,
                            'updated_at' => Carbon::now(),
                            'id_users' => $id
                        ]);
                } else {
                    // Insert new record
                    DB::table('user_profile_uploads')
                        ->insert([
                            'ICpic_name' => $filename,
                            'ICpic_path' => $filePath,
                            'ICpic_size' => $SaizFail,
                            'ICpic_format' => $extension,
                            'created_by' => $userid,
                            'created_at' => Carbon::now(),
                            'id_users' => $id
                        ]);
                }
            }

            //Upload passport photo 
            if ($request->hasFile('FailGambarPassport')) {
                $files = $request->file('FailGambarPassport');
                $extension = $files->getClientOriginalExtension();
                $filename = $files->getClientOriginalName();
                $filePath = $files->storeAs('passportPhoto', $filename, 'public');
                $path = $files->getRealPath();
                $data = file_get_contents($path);
                $SaizFail = $files->getSize();

                $checkPhoto = DB::table('user_profile_uploads')
                    ->where('id_users', $request->id)
                    ->first();

                if ($checkPhoto) {
                    // Update existing record
                    DB::table('user_profile_uploads')
                        ->where('user_profile_uploads.id_users', '=', $request->id)
                        ->update([
                            'passportpic_name' => $filename,
                            'passportpic_path' => $filePath,
                            'passportpic_size' => $SaizFail,
                            'passportpic_format' => $extension,
                            'updated_by' => $userid,
                            'updated_at' => Carbon::now(),
                            'id_users' => $id
                        ]);
                } else {
                    // Insert new record
                    DB::table('user_profile_uploads')
                        ->insert([
                            'passportpic_name' => $filename,
                            'passportpic_path' => $filePath,
                            'passportpic_size' => $SaizFail,
                            'passportpic_format' => $extension,
                            'created_by' => $userid,
                            'created_at' => Carbon::now(),
                            'id_users' => $id
                        ]);
                }
            }

            //Update MOU information
            if ($request->has('mou_name')) {
                $mouId = $request->get('mou_id');
                $mouNames = $request->input('mou_name');
                $mouStartDates = $request->input('mou_start_date');
                $mouEndDates = $request->input('mou_end_date');
                $mouRemarks = $request->input('mou_remarks');
                $mouFiles = $request->file('mou_file');

                foreach ($mouNames as $key => $mouName) {

                    //Check if mou name is not equal to null
                    if ($mouNames[$key] !== null && $mouNames[$key] !== '') {
                        //Check if mou id is not equal to null (for existing mou)
                        if ($mouId !== null && $mouId !== '') {
                            $name = $mouName;
                            $mId = $mouId[$key];
                            $startDate = $mouStartDates[$key];
                            $endDate = $mouEndDates[$key];
                            $remark = $mouRemarks[$key];

                            $mouID = null;

                            $existingMou = DB::table('list_mou')->where('id', $mId)->exists();

                            if ($existingMou) {
                                DB::table('list_mou')
                                    ->where('id', $mId)
                                    ->update([
                                        'mou_name' => $name,
                                        'start_date' => $startDate,
                                        'end_date' => $endDate,
                                        'remarks' => $remark,
                                        'updated_by' => $userid,
                                        'updated_at' => Carbon::now(),
                                    ]);

                                if (isset($mouFiles[$key]) && $mouFiles[$key]->isValid()) {
                                    $files = $mouFiles[$key];
                                    $extension = $files->getClientOriginalExtension();
                                    $filename = $files->getClientOriginalName();
                                    $filePath = $files->storeAs('mouLoaFiles', $filename, 'public');
                                    $path = $files->getRealPath();
                                    $data = file_get_contents($path);
                                    $size = $files->getSize();

                                    DB::table('upload_documents')
                                        ->insert([
                                            'file_name' => $filename,
                                            'file_path' => $filePath,
                                            'file_size' => $size,
                                            'file_format' => $extension,
                                            'docType' => 'mou',
                                            'id_users' => $id,
                                            'id_section' => $mId,
                                            'created_by' => $userid,
                                            'created_at' => Carbon::now(),
                                            'status' => 1,
                                        ]);
                                }
                            } else {
                                $mouID = DB::table('list_mou')
                                    ->insertGetId([
                                        'mou_name' => $name,
                                        'start_date' => $startDate,
                                        'end_date' => $endDate,
                                        'remarks' => $remark,
                                        'status' => 1,
                                        'id_company' => $id_company,
                                        'created_by' => $userid,
                                        'created_at' => Carbon::now(),
                                    ]);

                                if (isset($mouFiles[$key]) && $mouFiles[$key]->isValid()) {
                                    $files = $mouFiles[$key];
                                    $extension = $files->getClientOriginalExtension();
                                    $filename = $files->getClientOriginalName();
                                    $filePath = $files->storeAs('mouLoaFiles', $filename, 'public');
                                    $path = $files->getRealPath();
                                    $data = file_get_contents($path);
                                    $size = $files->getSize();

                                    DB::table('upload_documents')
                                        ->insert([
                                            'file_name' => $filename,
                                            'file_path' => $filePath,
                                            'file_size' => $size,
                                            'file_format' => $extension,
                                            'docType' => 'mou',
                                            'id_users' => $id,
                                            'id_section' => $mouID,
                                            'created_by' => $userid,
                                            'created_at' => Carbon::now(),
                                            'status' => 1,
                                        ]);
                                }
                            }
                        } else {
                            //If mou id is equal to null, then create new mou
                            $name = $mouName;
                            $startDate = $mouStartDates[$key];
                            $endDate = $mouEndDates[$key];
                            $remark = $mouRemarks[$key];

                            $mouID = null;

                            $mouID = DB::table('list_mou')
                                ->insertGetId([
                                    'mou_name' => $name,
                                    'start_date' => $startDate,
                                    'end_date' => $endDate,
                                    'remarks' => $remark,
                                    'status' => 1,
                                    'id_company' => $id_company,
                                    'created_by' => $userid,
                                    'created_at' => Carbon::now(),
                                ]);

                            if (isset($mouFiles[$key]) && $mouFiles[$key]->isValid()) {
                                $files = $mouFiles[$key];
                                $extension = $files->getClientOriginalExtension();
                                $filename = $files->getClientOriginalName();
                                $filePath = $files->storeAs('mouLoaFiles', $filename, 'public');
                                $path = $files->getRealPath();
                                $data = file_get_contents($path);
                                $size = $files->getSize();

                                DB::table('upload_documents')
                                    ->insert([
                                        'file_name' => $filename,
                                        'file_path' => $filePath,
                                        'file_size' => $size,
                                        'file_format' => $extension,
                                        'docType' => 'mou',
                                        'id_users' => $id,
                                        'id_section' => $mouID,
                                        'created_by' => $userid,
                                        'created_at' => Carbon::now(),
                                        'status' => 1,
                                    ]);
                            }
                        }
                    }
                }
            }

            //Update LOA information
            if ($request->has('loa_name')) {
                $loaId = $request->input('loa_id');
                $loaNames = $request->input('loa_name');
                $loaCourses = $request->input('loa_course');
                $loaStartDates = $request->input('loa_start_date');
                $loaEndDates = $request->input('loa_end_date');
                $loaRemarks = $request->input('loa_remarks');
                $auditFacilities = $request->input('loa_audit_facilities');
                $loaFiles = $request->file('loa_file');

                foreach ($loaNames as $key => $loaName) {
                    //Check if loa name is not equal to null
                    if ($loaNames[$key] !== null && $loaNames[$key] !== '') {
                        //Check if load id is not equal to null
                        if ($loaId !== null && $loaId !== '') {
                            $name = $loaName;
                            $lId = $loaId[$key];
                            $course = $loaCourses[$key];
                            $startDate = $loaStartDates[$key];
                            $endDate = $loaEndDates[$key];
                            $remark = $loaRemarks[$key];
                            $auditFacility = isset($auditFacilities[$key]) ? 1 : 0;

                            $loaID = null;

                            $existingLoa = DB::table('list_loa')->where('id', $lId)->exists();

                            if ($existingLoa) {
                                DB::table('list_loa')
                                    ->where('id', $lId)
                                    ->update([
                                        'loa_name' => $name,
                                        'course' => $course,
                                        'start_date' => $startDate,
                                        'end_date' => $endDate,
                                        'remarks' => $remark,
                                        'audit_facilities' => $auditFacility,
                                        'updated_by' => $userid,
                                        'updated_at' => Carbon::now(),
                                    ]);

                                if (isset($loaFiles[$key]) && $loaFiles[$key]->isValid()) {
                                    $file = $loaFiles[$key];
                                    $extension = $file->getClientOriginalExtension();
                                    $filename = $file->getClientOriginalName();
                                    $filePath = $file->storeAs('mouLoaFiles', $filename, 'public');
                                    $path = $file->getRealPath();
                                    $data = file_get_contents($path);
                                    $size = $file->getSize();

                                    DB::table('upload_documents')
                                        ->insert([
                                            'file_name' => $filename,
                                            'file_path' => $filePath,
                                            'file_size' => $size,
                                            'file_format' => $extension,
                                            'docType' => 'loa',
                                            'id_users' => $id,
                                            'id_section' => $lId,
                                            'created_by' => $userid,
                                            'created_at' => Carbon::now(),
                                            'status' => 1,
                                        ]);
                                }
                            } else {
                                $name = $loaName;
                                $course = $loaCourses[$key];
                                $startDate = $loaStartDates[$key];
                                $endDate = $loaEndDates[$key];
                                $remark = $loaRemarks[$key];
                                $auditFacility = isset($auditFacilities[$key]) ? 1 : 0;

                                $loaID = null;

                                $loaID = DB::table('list_loa')
                                    ->insertGetId([
                                        'loa_name' => $name,
                                        'course' => $course,
                                        'start_date' => $startDate,
                                        'end_date' => $endDate,
                                        'remarks' => $remark,
                                        'audit_facilities' => $auditFacility,
                                        'status' => 1,
                                        'id_company' => $id_company,
                                        'created_by' => $userid,
                                        'created_at' => Carbon::now(),
                                    ]);

                                if (isset($loaFiles[$key]) && $loaFiles[$key]->isValid()) {
                                    $file = $loaFiles[$key];
                                    $extension = $file->getClientOriginalExtension();
                                    $filename = $file->getClientOriginalName();
                                    $filePath = $file->storeAs('mouLoaFiles', $filename, 'public');
                                    $path = $file->getRealPath();
                                    $data = file_get_contents($path);
                                    $size = $file->getSize();

                                    DB::table('upload_documents')
                                        ->insert([
                                            'file_name' => $filename,
                                            'file_path' => $filePath,
                                            'file_size' => $size,
                                            'file_format' => $extension,
                                            'docType' => 'loa',
                                            'id_users' => $id,
                                            'id_section' => $loaID,
                                            'created_by' => $userid,
                                            'created_at' => Carbon::now(),
                                            'status' => 1,
                                        ]);
                                }
                            }
                        } else {
                            //If loa id is equal to null, then create new loa
                            $name = $loaName;
                            $course = $loaCourses[$key];
                            $startDate = $loaStartDates[$key];
                            $endDate = $loaEndDates[$key];
                            $remark = $loaRemarks[$key];
                            $auditFacility = isset($auditFacilities[$key]) ? 1 : 0;

                            $loaID = null;

                            $loaID = DB::table('list_loa')
                                ->insertGetId([
                                    'loa_name' => $name,
                                    'course' => $course,
                                    'start_date' => $startDate,
                                    'end_date' => $endDate,
                                    'remarks' => $remark,
                                    'audit_facilities' => $auditFacility,
                                    'status' => 1,
                                    'id_company' => $id_company,
                                    'created_by' => $userid,
                                    'created_at' => Carbon::now(),
                                ]);

                            if (isset($loaFiles[$key]) && $loaFiles[$key]->isValid()) {
                                $file = $loaFiles[$key];
                                $extension = $file->getClientOriginalExtension();
                                $filename = $file->getClientOriginalName();
                                $filePath = $file->storeAs('mouLoaFiles', $filename, 'public');
                                $path = $file->getRealPath();
                                $data = file_get_contents($path);
                                $size = $file->getSize();

                                DB::table('upload_documents')
                                    ->insert([
                                        'file_name' => $filename,
                                        'file_path' => $filePath,
                                        'file_size' => $size,
                                        'file_format' => $extension,
                                        'docType' => 'loa',
                                        'id_users' => $id,
                                        'id_section' => $loaID,
                                        'created_by' => $userid,
                                        'created_at' => Carbon::now(),
                                        'status' => 1,
                                    ]);
                            }
                        }
                    }
                }
            }

            DB::commit();
            return redirect()->route('admin.user-management')
                ->with('success', 'User details have been updated successfully.');
        } catch (Exception $ex) {
            dd($ex->getMessage());
            DB::rollback();

            return redirect()->route('admin.user-management')->with('error', 'Failed to update');
        }
    }

    public function deleteUserRegistration(Request $request, $id)
    {
        // Find the user by ID
        $userid = auth()->user()->id;

        $deleteuser = DB::table('users')
            ->where('users.id', '=', $id)
            ->update([
                'status' => 0,
                'updated_by' => $userid,
                'updated_at' => Carbon::now(),
                'deleted_by' => $userid,
                'deleted_at' => Carbon::now(),
                'deleted_status' => 1,


            ]);

        if ($deleteuser) {
            return redirect()->route('admin.user-management')->with('success', 'User deleted successfully');
        } else {
            return redirect()->route('admin.user-management')->with('error', 'User not found');
        }
    }


    public function setExamResult()
    {

        $userid = auth()->user()->id;
        $training = DB::table('trainings')
            ->join('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->select('training_course.course_name', 'training_course.id_training_course')
            // ->where(function ($query) {
            //     $query->where('trainings.id_approval_status', '=', '1')
            //           ->orWhere('trainings.id_approval_status', '=', '2');
            // })
            ->where('training_course.course_name', '!=', 'Grid Connected Photovoltaic (GCPV) System Design Course') //exclude cgpv design
            ->groupby('training_course.id_training_course')
            ->get();
        //$training = DB::table('training_course')->get();

        $trainingCGPV = DB::table('training_course')
            ->select('training_course.course_name', 'training_course.id_training_course')
            ->where('training_course.course_name', '=', 'Grid Connected Photovoltaic (GCPV) System Design Course')
            ->groupby('training_course.id_training_course')
            ->first();

        //   foreach($trainingCGPV as $cgpv){
        //    echo $cgpv->course_name;
        //   }

        $examConfigurations = ExamConfiguration::all();


        return view('examMgmt.SedaAdmin.setExamResult', compact('userid', 'examConfigurations', 'training', 'trainingCGPV'));
    }

    public function createExamConfiguration(Request $request)
    {
        $userid = auth()->user()->id;
        DB::beginTransaction(); // Start the transaction


        // try {
        //dd($request);
        // dd(($request->input('Theory_Part_E')));
        // $request->input('theory_TextPart_B');
        $configurationName = $request->input('configuration_name');
        $dropdown_training = $request->input('training');
        $gCPV_selected = $request->input('gCPV_System_Design');

        //dd($gCPV_selected);exit;

        if (($dropdown_training != "") && ($gCPV_selected == "")) {
            // echo "dropdown_training"; exit;

            if ($request->input('Theory_Part_A') != "") {
                $Theory_TextPart_A = "<th>" . $request->input('theory_TextPart_A') . "</th>";
            } else {
                $Theory_TextPart_A = "";
            }

            if ($request->input('Theory_Part_B') != "") {
                $Theory_TextPart_B = "<th>" . $request->input('theory_TextPart_B') . "</th>";
            } else {
                $Theory_TextPart_B = "";
            }

            if ($request->input('Theory_Part_C') != "") {
                $Theory_TextPart_C = "<th>" . $request->input('theory_TextPart_C') . "</th>";
            } else {
                $Theory_TextPart_C = "";
            }

            if ($request->input('Theory_Part_D') != "") {
                $Theory_TextPart_D = "<th>" . $request->input('theory_TextPart_D') . "</th>";
            } else {
                $Theory_TextPart_D = "";
            }


            if ($request->input('Theory_Part_E') != "") {
                $Theory_TextPart_E = "<th>" . $request->input('theory_TextPart_E') . "</th>";
            } else {
                $Theory_TextPart_E = "";
            }

            if ($request->input('Practical_Result') != "") {
                $Practical_Result = $request->input('Practical_Result');
                $total_practical = "<th>Total</th>";
            } else {
                $Practical_Result = "";
                $total_practical = "";
            }

            if ($request->input('Practical_Part_A') != "") {
                $Practical_TextPart_A = "<th>" . $request->input('practical_TextPart_A') . "</th>";
            } else {
                $Practical_TextPart_A = "";
            }
            if ($request->input('Practical_Part_B') != "") {
                $Practical_TextPart_B = "<th>" . $request->input('practical_TextPart_B') . "</th>";
            } else {
                $Practical_TextPart_B = "";
            }
            if ($request->input('Practical_Part_C') != "") {
                $Practical_TextPart_C = "<th>" . $request->input('practical_TextPart_C') . "</th>";
            } else {
                $Practical_TextPart_C = "";
            }
            if ($request->input('Practical_Part_D') != "") {
                $Practical_TextPart_D = "<th>" . $request->input('practical_TextPart_D') . "</th>";
            } else {
                $Practical_TextPart_D = "";
            }
            if ($request->input('Practical_Part_E') != "") {
                $Practical_TextPart_E = "<th>" . $request->input('practical_TextPart_E') . "</th>";
            } else {
                $Practical_TextPart_E = "";
            }


            $examConfiguration = new ExamResultHeader;
            $examConfiguration->configuration_name = $configurationName;
            $examConfiguration->id_training_course = $dropdown_training;
            // $examConfiguration->batch_no_full = $batchNo;
            $examConfiguration->header_row_no = "<th>No</th>";
            $examConfiguration->participant_name = "<th>Name</th>";
            $examConfiguration->header_exam_sit = "<th>New/Re-sit</th>";
            $examConfiguration->theory_result = $request->input('Theory_Result');
            $examConfiguration->total_theory_result = "<th>Total</th>";
            $examConfiguration->exam_1 = $Theory_TextPart_A;
            $examConfiguration->exam_2 = $Theory_TextPart_B;
            $examConfiguration->exam_3 = $Theory_TextPart_C;
            $examConfiguration->exam_4 = $Theory_TextPart_D;
            $examConfiguration->exam_5 = $Theory_TextPart_E;
            $examConfiguration->practical_result = $Practical_Result;
            $examConfiguration->total_practical_result = $total_practical;
            $examConfiguration->practical_exam_1 = $Practical_TextPart_A;
            $examConfiguration->practical_exam_2 = $Practical_TextPart_B;
            $examConfiguration->practical_exam_3 = $Practical_TextPart_C;
            $examConfiguration->practical_exam_4 = $Practical_TextPart_D;
            $examConfiguration->practical_exam_5 = $Practical_TextPart_E;
            $examConfiguration->final_result = "<th>Final Result<br>(Pass / Fail)</th>";
            $examConfiguration->header_status = "<th>Status</th>";
            $examConfiguration->header_action = "<th>Action</th>";

            $examConfiguration->created_by = $userid;
            $examConfiguration->created_at = now(); // Use created_at instead of created_by
            // dd($examConfiguration);
            $examConfiguration->save();
        }

        if (($dropdown_training == "") && ($gCPV_selected != "")) {

            // echo "gCPV_selected";exit;

            if ($request->input('fundamental_Result') != "") {
                $Fundamental_Result = $request->input('fundamental_Result');
                $total_Fundamental = "<th>Total</th>";
            } else {
                $Fundamental_Result = "";
                $total_Fundamental = "";
            }

            if ($request->input('Fundamental_Part_A') != "") {
                $Fundamental_TextPart_A = "<th>" . $request->input('fundamental_TextPart_A') . "</th>";
            } else {
                $Fundamental_TextPart_A = "";
            }

            if ($request->input('Fundamental_Part_B') != "") {
                $Fundamental_TextPart_B = "<th>" . $request->input('fundamental_TextPart_B') . "</th>";
            } else {
                $Fundamental_TextPart_B = "";
            }

            if ($request->input('Fundamental_Part_C') != "") {
                $Fundamental_TextPart_C = "<th>" . $request->input('fundamental_TextPart_C') . "</th>";
            } else {
                $Fundamental_TextPart_C = "";
            }


            //
            if ($request->input('design_Result') != "") {
                $Design_Result = $request->input('design_Result');
                $total_Design = "<th>Total</th>";
            } else {
                $Design_Result = "";
                $total_Design = "";
            }

            if ($request->input('Design_Part_A') != "") {
                $Design_TextPart_A = "<th>" . $request->input('design_TextPart_A') . "</th>";
            } else {
                $Design_TextPart_A = "";
            }

            if ($request->input('Design_Part_B') != "") {
                $Design_TextPart_B = "<th>" . $request->input('design_TextPart_B') . "</th>";
            } else {
                $Design_TextPart_B = "";
            }

            if ($request->input('Design_Part_C') != "") {
                $Design_TextPart_C = "<th>" . $request->input('design_TextPart_C') . "</th>";
            } else {
                $Design_TextPart_C = "";
            }

            //
            if ($request->input('cgpvpractical_Result') != "") {
                $CGPVpractical_Result = $request->input('cgpvpractical_Result');
                $total_CGPVpractical = "<th>Total</th>";
            } else {
                $CGPVpractical_Result = "";
                $total_CGPVpractical = "";
            }

            if ($request->input('PracticalGCPV_Part_A') != "") {
                $PracticalGCPV_TextPart_A = "<th>" . $request->input('practicalGCPV_TextPart_A') . "</th>";
            } else {
                $PracticalGCPV_TextPart_A = "";
            }
            if ($request->input('PracticalGCPV_Part_B') != "") {
                $PracticalGCPV_TextPart_B = "<th>" . $request->input('practicalGCPV_TextPart_B') . "</th>";
            } else {
                $PracticalGCPV_TextPart_B = "";
            }
            if ($request->input('PracticalGCPV_Part_C') != "") {
                $PracticalGCPV_TextPart_C = "<th>" . $request->input('practicalGCPV_TextPart_C') . "</th>";
            } else {
                $PracticalGCPV_TextPart_C = "";
            }


            $examConfiguration = new ExamGCPVResultHeader;
            $examConfiguration->configuration_name = $configurationName;
            $examConfiguration->id_training_course = $request->input('gCPV_System_Design2'); //need to change
            // $examConfiguration->batch_no_full = $batchNo;
            $examConfiguration->header_row_no = "<th>No</th>";
            $examConfiguration->participant_name = "<th>Name</th>";
            $examConfiguration->header_exam_sit = "<th>New/Re-sit</th>";


            $examConfiguration->fundamental_result = $Fundamental_Result;
            $examConfiguration->total_fundamental_result = $total_Fundamental;
            $examConfiguration->exam_1 = $Fundamental_TextPart_A;
            $examConfiguration->exam_2 = $Fundamental_TextPart_B;
            $examConfiguration->exam_3 = $Fundamental_TextPart_C;

            $examConfiguration->design_result = $Design_Result;
            $examConfiguration->total_design_result = $total_Design;
            $examConfiguration->design_exam_1 = $Design_TextPart_A;
            $examConfiguration->design_exam_2 = $Design_TextPart_B;
            $examConfiguration->design_exam_3 = $Design_TextPart_C;

            $examConfiguration->practical_result = $CGPVpractical_Result;
            $examConfiguration->total_practical_result = $total_CGPVpractical;
            $examConfiguration->practical_exam_1 = $PracticalGCPV_TextPart_A;
            $examConfiguration->practical_exam_2 = $PracticalGCPV_TextPart_B;
            $examConfiguration->practical_exam_3 = $PracticalGCPV_TextPart_C;



            $examConfiguration->final_result = "<th>Final Result<br>(Pass / Fail)</th>";
            $examConfiguration->header_status = "<th>Status</th>";
            $examConfiguration->header_action = "<th>Action</th>";

            $examConfiguration->created_by = $userid;
            $examConfiguration->created_at = now(); // Use created_at instead of created_by
            // dd($examConfiguration);
            $examConfiguration->save();
        }


        DB::commit(); // Commit the transaction

        return redirect()->route('admin.set-exam-result')
            ->with('success', 'Success to create exam configuration.');


        // } catch (Exception $ex) {
        //  DB::rollback(); // Rollback the transaction in case of an exception
        //  return redirect()->route('admin.set-exam-result')
        //      ->with('error', 'Failed to create exam configuration.');
        //}
    }

    public function tableResult()
    {

        $userid = auth()->user()->id;
        $header_result = DB::table('exam_result_header')->where('exam_result_header.id', '=', 1)->get();
        //$header_result = DB::table('exam_result_header')->get();
        //$training = DB::table('training_course')->get();
        // $examConfigurations = ExamConfiguration::all();

        return view('examMgmt.SedaAdmin.headerExamResult', compact('userid', 'header_result'));
    }


    public function previewTableResult()
    {

        $userid = auth()->user()->id;

        $headerList =  DB::table('exam_result_header')
            // ->leftjoin('trainings', 'trainings.id', '=', 'exam_result_header.id_training')
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'exam_result_header.id_training_course')
            ->select(
                'exam_result_header.id',
                'exam_result_header.id_training',
                'exam_result_header.batch_no_full',
                'exam_result_header.configuration_name',
                'training_course.course_name'
            )
            ->get();

        $headerListGCPV = DB::table('examGCPV_result_header')
            // ->leftjoin('trainings', 'trainings.id', '=', 'examGCPV_result_header.id_training')
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'examGCPV_result_header.id_training_course')
            ->select(
                'examGCPV_result_header.id',
                'examGCPV_result_header.id_training',
                'examGCPV_result_header.batch_no_full',
                'examGCPV_result_header.configuration_name',
                'training_course.course_name'
            )
            ->get();


        return view('examMgmt.SedaAdmin.previewHeaderExamResult', compact('userid', 'headerList', 'headerListGCPV'));
    }




    public function getPreviewResult($id)
    {
        $userid = auth()->user()->id;
        $previewHeaderResult = DB::table('exam_result_header')
            ->where('exam_result_header.id', '=', $id)
            ->get();


        return view('examMgmt.SedaAdmin.getPreviewHeaderExamResult', compact('userid', 'previewHeaderResult'));
    }


    public function getResult($id)
    {

        $userid = auth()->user()->id;
        $previewHeaderResult = DB::table('exam_result_header')
            ->select('exam_result_header.*')
            ->where('exam_result_header.id_training_course', '=', $id)
            ->get();

        return response()->json([
            'status' => 'success',
            'previewHeaderResult' => $previewHeaderResult,
        ], 200);

        //return view('examMgmt.SedaAdmin.getPreviewHeaderExamResult', compact('userid', 'previewHeaderResult'));
    }



    public function getPreviewResultDelete($id)
    {
        $userid = auth()->user()->id;
        //delete
        $deleteHeaderResult = DB::table('exam_result_header')
            ->where('exam_result_header.id', '=', $id)
            ->delete();

        if ($deleteHeaderResult) {

            return redirect()->route('admin-PreviewTableResult')->with('success', 'Record deleted successfully.');
        } else {
            // Deletion failed
            return redirect()->route('admin-PreviewTableResult')->with('error', 'Failed to delete record.');
        }
    }

    public function getPreviewResultGCPV($id)
    {
        $userid = auth()->user()->id;
        $previewHeaderResult = DB::table('examGCPV_result_header')
            ->where('examGCPV_result_header.id', '=', $id)
            ->get();

        return view('examMgmt.SedaAdmin.getPreviewHeaderExamGCPVResult', compact('userid', 'previewHeaderResult'));
    }

    public function getPreviewResultGCPVDelete($id)
    {
        $userid = auth()->user()->id;
        //delete
        $deleteHeaderGCPVResult = DB::table('examGCPV_result_header')
            ->where('examGCPV_result_header.id', '=', $id)
            ->delete();

        if ($deleteHeaderGCPVResult) {

            return redirect()->route('admin-PreviewTableResult')->with('success', 'Record deleted successfully.');
        } else {
            // Deletion failed
            return redirect()->route('admin-PreviewTableResult')->with('error', 'Failed to delete record.');
        }
    }
}
