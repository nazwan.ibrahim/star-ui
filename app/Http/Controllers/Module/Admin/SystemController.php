<?php

namespace App\Http\Controllers\Module\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\View\View;
use App\Models\Country;
use App\Models\State;
use App\Models\Upload_Document;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationConfirmation;
use App\Models\Company;
use App\Models\ExamConfiguration;
use App\Models\ExamResultHeader;
use App\Models\ExamTraining;
use App\Models\Loa;
use Exception;
use App\Models\UploadDocuments;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Http;

class SystemController extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function userAccount() : View
    {

        $userid = auth()->user()->id;
        return view('admin.userAccount',compact('userid'));
    }

    public function acccountActivation() : View
    {

        $userid = auth()->user()->id;
        return view('userRegistration.userRegistrationManagement',compact('userid'));
    
    }

    public function acccountDeactivation() : View
    {

        $userid = auth()->user()->id;
        return view('userRegistration.userRegistrationManagement',compact('userid'));
    }

    public function accountResetPassword() : View
    {

        $userid = auth()->user()->id;
        return view('userRegistration.userRegistrationManagement',compact('userid'));
    
       
    }

    public function resendEmail() : View
    {
        $userid = auth()->user()->id;
        return view('userRegistration.userRegistrationManagement',compact('userid'));
    }

}