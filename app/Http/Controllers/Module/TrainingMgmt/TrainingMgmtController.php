<?php

namespace App\Http\Controllers\Module\TrainingMgmt;

use App\Http\Controllers\Controller;
use App\Models\Application;
use App\Models\AuditTrail;
use App\Models\TrainingObjective;
use Illuminate\Http\Request;
use App\Models\Training;
use Illuminate\Support\Facades\DB;
use App\Models\TrainingCourse;
use App\Models\courseReType;
use App\Models\Participant;
use App\Models\Participant_Attendance;
use App\Models\Feedback_question;
use App\Models\State;
use App\Models\Trainer;
use Carbon\Carbon;
use App\Models\Upload_Document;
use App\Models\UploadDocuments;
use Exception;

use Illuminate\Support\Facades\Mail;
use App\Mail\TrainingAppQuery;

class TrainingMgmtController extends Controller
{
    public function trainingManagement(Request $request)
    {
        $userid = auth()->user()->id;

        return view('trainingMgmt.trainingMgmt', compact('userid'));
    }

    public function getStates(Request $request, $regionId)
    {
        //$states = State::where('id_region', $regionId)->get();
        // $states = DB::table('state')
        // ->get();

        // return response()->json($states);
    }

    public function trainingRegistration()
    {
        $userid = auth()->user()->id;

        $courseList = DB::table('training_course')
            ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('course_re_type', 'course_re_type.id_training_course', '=', 'training_course.id_training_course')
            ->leftjoin('re_type', 're_type.id', '=', 'course_re_type.id_re_type')
            ->leftjoin('training_option', 'training_option.id', '=', 'training_course.id_training_option')
            ->leftjoin('training_partner_course', 'training_partner_course.id_training_course', '=', 'training_course.id_training_course')
            ->leftjoin('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
            ->select(
                'training_course.id_training_course',
                'training_course.course_name',
                'training_course.new_fee',
                'training_course.new_fee_int',
                'training_type.name as course_type',
                're_type.re_type',
                'training_option.option',
                'course_category.course_name as category_name',
                'training_partner_course.course_code as course_code',
                'training_cert_code.code as cert_code',
            )
            ->get();

        $outputData = [];

        foreach ($courseList as $row) {
            $courseName = $row->course_name;

            if (!isset($outputData[$courseName])) {
                $outputData[$courseName] = [
                    'id' => $row->id_training_course,
                    'course_name' => $courseName,
                    'course_type' => $row->course_type,
                    'category_name' => $row->category_name,
                    'training_option' => $row->option,
                    'new_fee' => $row->new_fee,
                    'new_fee_int' => $row->new_fee_int,
                    're_type' => [],
                    'cert_code' => $row->cert_code,
                ];
            }
            $outputData[$courseName]['id'] = $row->id_training_course;
            $outputData[$courseName]['re_type'][] = $row->re_type;

        }

        // Filter out duplicate courses
        $filteredCourses = array_values($outputData);

        $partnertr = DB::table('company')
            ->join('training_partner_course', 'training_partner_course.id_company', '=', 'company.id')
            ->join('training_course', 'training_course.id_training_course', '=', 'training_partner_course.id_training_course')
            ->join('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
            ->select(
                'company.id as id',
                'company.code as company_code',
                'company.name as name',
                'company.address_1',
                'company.address_2',
                'company.postcode',
                'training_partner_course.course_code as course_code',
                'training_cert_code.code as cert_code',
            )
            ->groupby('company.id')
            ->get();
            // dd($partnertr);


            $trainingpartnerlist = DB::table('training_course')
            ->leftjoin('training_partner_course', 'training_course.id_training_course', '=', 'training_partner_course.id_training_course')
            ->leftjoin('company','company.id', '=', 'training_partner_course.id_company')
            ->where('training_partner_course.status', '=', 1)
            ->select(
                'company.id as id',
                'company.name as training_partner',
                'training_partner_course.course_code',
            )
            ->first();
// dd($trainingpartnerlist );


        $trainingMode = DB::table('training_mode')
            ->select(
                'id as mode_id',
                'mode as mode_name',
            )
            ->get();
        $trainer = DB::table('trainer')
            ->leftJoin('users', 'trainer.id_users', '=', 'users.id')
            ->leftJoin('company', 'trainer.id_company', '=', 'company.id')
            ->leftJoin('training_partner_course', 'training_partner_course.id_company', '=', 'company.id')
            ->where('training_partner_course.status', '=', 1)
            ->select('users.fullname', 'trainer.id')
            ->groupby('trainer.id')
            ->get();

        $feetypes = DB::table('fee_type')
            ->get();
        $re_types = DB::table('re_type')
            ->get();

        return view('trainingMgmt.trainingRegistration', compact('userid', 'partnertr', 'trainer', 'feetypes', 're_types', 'trainingMode', 'outputData','trainingpartnerlist', 'courseList', 'filteredCourses'));
    }
    public function trainingRegistrationTP()
    {
        $userid = auth()->user()->id;

        $company_id = DB::table('users')->where('id', $userid)->value('id_company');
        $regions = DB::table('region')->get();
        $states = DB::table('state')->get();
//dd($userid);
        $courseList = DB::table('training_course')
            ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('course_re_type', 'course_re_type.id_training_course', '=', 'training_course.id_training_course')
            ->leftjoin('re_type', 're_type.id', '=', 'course_re_type.id_re_type')
            ->leftjoin('training_option', 'training_option.id', '=', 'training_course.id_training_option')
            ->leftjoin('training_partner_course', 'training_partner_course.id_training_course', '=', 'training_course.id_training_course')
            ->leftjoin('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
            ->select(
                'training_course.id_training_course',
                'training_course.course_name',
                'training_course.new_fee',
                'training_course.new_fee_int',
                'training_type.name as course_type',
                're_type.re_type',
                'training_option.option',
                'course_category.course_name as category_name',
                'training_partner_course.course_code as course_code',
                'training_cert_code.code as cert_code',
            )
            ->where('training_partner_course.id_company', '=', $company_id)
            ->get();

            // dd($courseList);

        $outputData = [];

        foreach ($courseList as $row) {
            $courseName = $row->course_name;

            if (!isset($outputData[$courseName])) {
                $outputData[$courseName] = [
                    'id' => $row->id_training_course,
                    'course_name' => $courseName,
                    'course_type' => $row->course_type,
                    'category_name' => $row->category_name,
                    'training_option' => $row->option,
                    'new_fee' => $row->new_fee,
                    'new_fee_int' => $row->new_fee_int,
                    're_type' => [],
                    'cert_code' => $row->cert_code,
                ];
            }
            $outputData[$courseName]['id'] = $row->id_training_course;
            $outputData[$courseName]['re_type'][] = $row->re_type;
        }

        // Filter out duplicate courses
        $filteredCourses = array_values($outputData);

        $partnertr = DB::table('company')
            ->join('training_partner_course', 'training_partner_course.id_company', '=', 'company.id')
            ->join('training_course', 'training_course.id_training_course', '=', 'training_partner_course.id_training_course')
            ->join('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
            ->select(
                'company.id as id',
                'company.code as company_code',
                'company.name as name',
                'company.address_1',
                'company.address_2',
                'company.postcode',
                'training_partner_course.course_code as course_code',
                'training_cert_code.code as cert_code',
            )
            ->get();

            // dd($partnertr);

        $trainingMode = DB::table('training_mode')
            ->select(
                'id as mode_id',
                'mode as mode_name',
            )
            ->get();

            $trainer = DB::table('trainer')
            ->leftJoin('users', 'trainer.id_users', '=', 'users.id')
            ->leftJoin('company', 'trainer.id_company', '=', 'company.id')
            ->leftJoin('training_partner_course', 'training_partner_course.id_company', '=', 'company.id')
            ->where('training_partner_course.status', '=', 1)
            ->where('training_partner_course.id_company', '=', $company_id)
            ->select('users.fullname', 'trainer.id as trainer_id')
            ->groupby('trainer.id')
            ->get();

        $feetypes = DB::table('fee_type')->get();
        $re_types = DB::table('re_type')->get();

        return view('trainingMgmt.trainingRegistrationTP', compact('userid', 'partnertr', 'trainer', 'feetypes', 'trainingMode', 'outputData', 'courseList', 're_types', 'filteredCourses', 'regions', 'states'));
    }

    public function insertTraining(Request $request)
    {
        // dd($request->all());
        $userid = auth()->user()->id;

        $company_id = DB::table('users')->where('id', $userid)->value('id_company');

        try {
        $this->validate($request, [
            'training_option' => '',
            'training_mode' => '',
            'venue' => '',
            'venue_address' => '',
            'id_state' => '',
            'id_region' => '',
            'id_training_type' => '',
            'trainingCourse' => '',
            'partnerName' => '',
            'total_participant' => '',
            'training_code' => '',
            'batch_no_full' => '',
            'date_start' => '',
            'date_end' => '',
            'start_time' => '',
            'url' => '',
            'end_time' => '',
            'advert_start' => '',
            'advert_end' => '',
            'fee' => '',
            'hrdf_claim' => '',
            'document_description' => '',
            'training_description' => '',
            'supporting_documents.*' => '',
            'training_documents.*' => '',
            'training_image' => '',

        ]);


        $fee = $request->input('fee');
        $isPaid = $fee > 0 ? 1 : 0;

        DB::beginTransaction();

        try {

            $username = auth()->user()->fullname;
            $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

            //Create the audit trail
            AuditTrail::create([
                'username' => $username ?? 'Guest',
                'activity' => 'Create New Training Application',
                'activity_time' => now(),
                'ip_address' => $request->ip(),
                'browser' => $browser,
            ]);
            //Check if the training image was uploaded or not
            if ($request->hasFile('training_image')) {
                $imagePath = $request->file('training_image')->store('training_posters', 'public');
            } else {
                $imagePath = null;
            }

            //Insert new training information into the table trainings
            $newTrainingId = DB::table('trainings')
                ->insertGetId([
                    'training_mode' => $request->input('training_mode'),
                    'id_company' => $company_id ?? $request->input('partnerName'),
                    'venue' => $request->input('venue'),
                    'venue_address' => $request->input('venue_address'),
                    'training_url' => $request->input('url'),
                    'training_image' => $imagePath,
                    'id_state' => $request->input('state'),
                    'id_training_course' => $request->input('trainingCourse'),
                    'total_participant' => $request->input('total_participant'),
                    'training_code' => $request->input('training_code'),
                    'batch_no' => $request->input('batch_no'),
                    'batch_no_full' => $request->input('batch_no_full'),
                    'date_start' => $request->input('date_start'),
                    'date_end' => $request->input('date_end'),
                    'start_time' => $request->input('start_time'),
                    'end_time' => $request->input('end_time'),
                    'advert_start' => $request->input('advert_start'),
                    'advert_end' => $request->input('advert_end'),
                    'is_paid' => $isPaid,
                    'hrdf_claim' => $request->input('hrdf_claim'),
                    'document_description' => $request->input('document_description'),
                    'training_description' => $request->input('training_description'),
                    'attendance_status' => 0,
                    'results_status' => 0,
                    'report_status' => 0,
                    'status' => 1,
                    'id_training_status' => 1,
                    'id_approval_status' => $request->input('submit_status'),
                    'id_training_app_status' => 1
                ]);

            //Insert new objectives into the table training_objective
            $objectives = $request->input('objectives');
            if (!empty($objectives)) {
                foreach ($objectives as $index => $objective) {

                    $existingObjective =
                        TrainingObjective::where('id_training', $newTrainingId)
                        ->where('id', $index)
                        ->first();

                    if ($existingObjective) {
                        $existingObjective
                            ->update(['objective' => $objective]);
                    } else {
                        TrainingObjective::create([
                            'id_training' => $newTrainingId,
                            'objective' => $objective,
                        ]);
                    }
                }
            }

            //Insert new training trainer informations into the table training_trainer
            $selectedTrainers = $request->input('trainer_id');
            if (!empty($selectedTrainers)) {
                foreach ($selectedTrainers as $trainerId) {

                    DB::table('training_trainer')
                        ->insert([
                            'id_trainer' => $trainerId,
                            'id_training' => $newTrainingId,
                        ]);
                }
            }

            //Insert new supporting documents into the table upload_document
            if ($request->hasFile('supporting_documents')) {
                foreach ($request->file('supporting_documents') as $document) {
                    $documentPath = $document->store('supporting_documents');

                    $uploadDocument = new Upload_Document([
                        'file_path' => $documentPath,
                        'file_name' => $document->getClientOriginalName(),
                        'id_training' => $newTrainingId,
                        'file_format' => $document->getClientOriginalExtension(),
                        'file_size' => $document->getSize(),
                        'doc_type' => 'supporting_documents',
                        'updated_at' => now(),
                        'id_users' => $userid,
                    ]);

                    $uploadDocument->save();
                }
            }

            //Insert new training document into the table upload document
            if ($request->hasFile('training_documents')) {
                foreach ($request->file('training_documents') as $document) {
                    $documentPath = $document->store('training_documents');

                    $uploadDocument = new Upload_Document([
                        'file_path' => $documentPath,
                        'file_name' => $document->getClientOriginalName(),
                        'id_training' => $newTrainingId,
                        'file_format' => $document->getClientOriginalExtension(),
                        'file_size' => $document->getSize(),
                        'doc_type' => 'training_notes',
                        'updated_at' => now(),
                        'id_users' => $userid,

                    ]);

                    $uploadDocument->save();
                }
            }

            DB::commit();

            if(auth()->user()->user_type == 1 || auth()->user()->user_type == 2){
                return view('trainingMgmt.trainingMgmt', compact('userid'))->with('success', 'Success to create register training');

            }else if(auth()->user()->user_type == 3){
                return view('trainingMgmt.trainingMgmtAppTP', compact('userid'))->with('success', 'Success to create register training');

            }
        } catch (Exception $ex) {

            DB::rollback();

            return redirect()->back()->with('error', 'Failed to create register training');
        }
        
    } catch (Exception $ex) {
        return redirect()->back()->with('error', 'Failed to create register training.');
    }
}






    public function trainingManagementAppTP(Request $request)
    {
        $userid = auth()->user()->id;

        return view('trainingMgmt.trainingMgmtAppTP', compact('userid'));
    }
    public function editTrainingRegistrationAppTP($id)
    { //dd($id);
        $userid = auth()->user()->id;

        $company_id = DB::table('users')->where('id', $userid)->value('id_company');
        $regions = DB::table('region')->get();
       

        $partnertr = DB::table('company')
            ->where('trainings.id', '=', $id)
            ->where('users.id','=',$userid)
            ->join('users', 'company.id', '=', 'users.id_company')
            ->join('trainings', 'trainings.id_company', '=', 'company.id')
            ->join('training_partner_course', 'training_partner_course.id_company', '=', 'company.id')
            ->join('training_course', 'training_course.id_training_course', '=', 'training_partner_course.id_training_course')
            ->join('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
            ->select(
                'company.id as company_id',
                'company.code as company_code',
                'company.name as company_name',
                'company.address_1',
                'company.address_2',
                'company.postcode',
                'training_partner_course.course_code as course_code',
                'training_cert_code.code as cert_code',
            )
            ->first();

            // dd($partnertr);

        $trainerList = DB::table('users')
            ->join('trainer', 'trainer.id_users', '=', 'users.id')
            ->select('users.id as user_id', 'trainer.id as trainer_id', 'users.fullname AS trainer_fullname')
            ->get();

        $re_types = DB::table('re_type')->get();
        $feetypes = DB::table('fee_type')->get();

        $training = DB::table('trainings')
            ->where('trainings.id', '=', $id)
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('region', 'region.id', '=', 'trainings.id_region')
            ->leftjoin('state', 'state.id_region', '=', 'region.id')
            ->select(
                'trainings.id',
                'training_course.course_name as course_name',
                DB::raw('trainings.date_start as date_start'),
                DB::raw('trainings.date_end as date_end'),
                'trainings.training_code',
                'trainings.venue',
                'trainings.total_participant',
                DB::raw('trainings.advert_start as advert_start'),
                DB::raw('trainings.advert_end as advert_end'),
                'trainings.batch_no',
                'trainings.hrdf_claim',
                'training_course.id_training_option',
                'trainings.training_mode',
                'trainings.training_url',
                'training_course.id_training_type as type_id',
                'training_type.name as training_type',
                'trainings.training_description',
                'trainings.document_description',
                'trainings.id_region',
                'region.name AS region_name',
                'trainings.id_state',
                'state.name AS state_name',
                'trainings.venue_address',
                'trainings.start_time',
                'trainings.end_time',
            )
            ->first();

            $states = DB::table('state')
            ->where('id', $training->id_state)
            ->get();

        $objectives = DB::table('training_objective')
            ->join('trainings', 'trainings.id', '=', 'training_objective.id_training')
            ->where('trainings.id', '=', $id)
            ->select(
                'training_objective.objective',
            )
            ->get();

        $courseList = DB::table('training_course')
            ->where('trainings.id', '=', $id)
            ->join('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
            ->join('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->join('training_option', 'training_option.id', '=', 'training_course.id_training_option')
            ->join('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('training_partner_course', 'training_partner_course.id_training_course', '=', 'training_course.id_training_course')
            ->leftjoin('trainer', 'trainer.id_company', '=', 'training_partner_course.id_company')
            ->leftjoin('users','users.id', '=',  'trainer.id_users')
            ->select(
                'training_course.id_training_course',
                'course_category.id as course_id',
                'course_category.course_name as category_name',
                'training_course.course_name as course_name',
                'new_fee',
                'new_fee_int',
                'training_option.id as option_id',
                'training_option.option as option_name',
                'training_type.id as type_id',
                'training_type.name as type_name',
                'training_partner_course.course_code as course_code',
                'training_partner_course.id_company',
                'trainer.id_company as id_trainer_company',
                'users.id as id_user',
                'users.fullname'
            )
            ->first();

        $trainingMode = DB::table('training_mode')
            ->select(
                'id as mode_id',
                'mode as mode_name',
            )
            ->get();

        $trainer = DB::table('users')
            ->select('fullname')
            ->where('id', '=', $courseList->id_user)
            ->get();

        $selectedReTypes = DB::table('training_course')
            ->join('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
            ->where('trainings.id', '=', $id)
            ->join('course_re_type', 'course_re_type.id_training_course', '=', 'training_course.id_training_course')
            ->pluck('course_re_type.id_re_type')
            ->toArray();

        $resitfees = DB::table('resit_fee')
            ->join('training_course', 'training_course.id_training_course', '=', 'resit_fee.id_training_course')
            ->join('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
            ->join('fee_type', 'fee_type.id_fee', '=', 'resit_fee.id_fee')
            ->where('trainings.id', '=', $id)
            ->select(
                'fee_type.fee_name',
                'resit_fee.amount',
                'resit_fee.fee_category',
            )
            ->get();

        $currentYear = date('y');
        $batchNoFull = $partnertr->cert_code . $courseList->course_code . $currentYear;

        return view('trainingMgmt.editTrainingRegistrationAppTP', compact('userid', 'training', 'partnertr', 'courseList', 'trainer', 'trainingMode', 'selectedReTypes', 're_types', 'feetypes', 'resitfees', 'objectives', 'batchNoFull', 'trainerList', 'regions', 'states'));
    }
    public function updateTrainingRegistrationAppTP(Request $request, $id)
    {
        // dd($request->all());
        $userid = auth()->user()->id;

        $this->validate($request, [
            'training_mode' => '',
            'venue' => '',
            'venue_address' => '',
            'state' => '',
            'total_participant' => '',
            'training_code' => '',
            'date_start' => '',
            'date_end' => '',
            'start_time' => '',
            'end_time' => '',
            'advert_start' => '',
            'url' => '',
            'batch_no_full' => '',
            'advert_end' => '',
            'hrdf_claim' => '',
            'document_description' => '',
            'training_description' => '',
            'supporting_documents.*' => '',
            'training_documents.*' => '',
            'training_image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $fee = $request->input('fee');
        $isPaid = $fee > 0 ? 1 : 0;

        $training = Training::find($id);

        if (!$training) {
            return view('trainingMgmt.trainingMgmt', compact('userid'));
        }

        DB::beginTransaction();

        try {

            $username = auth()->user()->fullname;
            $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

            //Create the audit trail
            AuditTrail::create([
                'username' => $username ?? 'Guest',
                'activity' => 'Edit Training Application ' . $id,
                'activity_time' => now(),
                'ip_address' => $request->ip(),
                'browser' => $browser,
            ]);

            //Check if the training image was uploaded or not
            if ($request->hasFile('training_image')) {
                $imagePath = $request->file('training_image')->store('training_posters', 'public');
            } else {
                $imagePath = null;
            }

            //Update training information
            DB::table('trainings')
                ->where('id', $id)
                ->update([
                    'training_mode' => $request->input('training_mode'),
                    'venue' => $request->input('venue'),
                    'venue_address' => $request->input('venue_address'),
                    'id_state' => $request->input('state'),
                    'id_region' => $request->input('region'),
                    'id_training_course' => $request->input('trainingCourse'),
                    'total_participant' => $request->input('total_participant'),
                    'training_code' => $request->input('training_code'),
                    'training_image' => $imagePath,
                    'date_start' => $request->input('date_start'),
                    'date_end' => $request->input('date_end'),
                    'start_time' => $request->input('start_time'),
                    'end_time' => $request->input('end_time'),
                    'training_url' => $request->input('url'),
                    'advert_start' => $request->input('advert_start'),
                    'advert_end' => $request->input('advert_end'),
                    'batch_no' => $request->input('batch_no_full'),
                    'batch_no_full' => $request->input('batch_no_full'),
                    'is_paid' => $isPaid,
                    'hrdf_claim' => $request->input('hrdf_claim'),
                    'document_description' => $request->input('document_description'),
                    'training_description' => $request->input('training_description'),
                    'attendance_status' => 0,
                    'results_status' => 0,
                    'report_status' => 0,
                    'status' => 1,
                    'id_approval_status' => $request->input('submit_status'),
                    'id_training_app_status' => 1,
                    'id_training_status' => 1,
                    'updated_at' => now(),
                ]);

            //Update supporting documents
            if ($request->hasFile('supporting_documents')) {
                foreach ($request->file('supporting_documents') as $document) {
                    $documentPath = $document->store('supporting_documents');

                    $uploadDocument = new Upload_Document([
                        'file_path' => $documentPath,
                        'file_name' => $document->getClientOriginalName(),
                        'id_training' => $training->id,
                        'file_format' => $document->getClientOriginalExtension(),
                        'file_size' => $document->getSize(),
                        'doc_type' => 'supporting_document',
                        'updated_at' => now(),
                    ]);

                    $uploadDocument->save();
                }
            }

            //Update training documents
            if ($request->hasFile('training_documents')) {
                foreach ($request->file('training_documents') as $document) {
                    $documentPath = $document->store('training_documents');

                    $uploadDocument = new Upload_Document([
                        'file_path' => $documentPath,
                        'file_name' => $document->getClientOriginalName(),
                        'id_training' => $training->id,
                        'file_format' => $document->getClientOriginalExtension(),
                        'file_size' => $document->getSize(),
                        'doc_type' => 'training_notes',
                        'updated_at' => now(),
                    ]);

                    $uploadDocument->save();
                }
            }

            //Update training objectives information
            $objectives = $request->input('training_objective');
            if (!empty($objectives)) {
                foreach ($objectives as $index => $objective) {
                    $existingObjective = TrainingObjective::where('id_training', $id)
                        ->where('objective_index', $index)
                        ->first();

                    if ($existingObjective) {
                        $existingObjective->update(['objective' => $objective]);
                    } else {
                        TrainingObjective::create([
                            'id_training' => $id,
                            'training_objective' => $objective,
                            'objective_index' => $index,
                        ]);
                    }
                }
            }

            DB::commit();

            return view('trainingMgmt.trainingMgmtAppTP', compact('userid'));
        } catch (Exception $ex) {

            DB::rollback();

            return redirect()->route('user-profile')->with('error', 'Failed to update');
        }
    }
    public function editTrainingRegistration($id)
    {
        $userid = auth()->user()->id;
        $re_types = DB::table('re_type')
            ->get();
        $feetypes = DB::table('fee_type')
            ->get();
        $training = DB::table('trainings')
        ->where('trainings.id', '=', $id)
        ->join('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->join('training_type','training_type.id','=','training_course.id_training_type')
        ->join('course_category','course_category.id','=','training_course.id_course_category')
        ->select(
            'trainings.id',
            'training_course.course_name as course_name',
            DB::raw('trainings.date_start as date_start'),
            DB::raw('trainings.date_end as date_end'),
            'trainings.training_code',
            'trainings.venue',
            'trainings.total_participant',
            DB::raw('trainings.advert_start as advert_start'),
            DB::raw('trainings.advert_end as advert_end'),
            'trainings.batch_no',
            'trainings.hrdf_claim',
            'trainings.is_paid',
            'training_course.id_training_option',
            'trainings.training_mode',
            'trainings.training_url',
            'training_course.id_training_type as type_id',
            'training_type.name as training_type',
            'trainings.training_description',
            'trainings.document_description',
            'trainings.venue_address',
            'trainings.start_time',
            'trainings.end_time',
            'trainings.remarks as query',
        )
        ->first();
        // dd($training);
        $objectives = DB::table('training_objective')
        ->join('trainings', 'trainings.id', '=', 'training_objective.id_training')
        ->where('trainings.id', '=', $id)
        ->select(
            'training_objective.objective',
        )
        ->get();
        $courseList = DB::table('training_course')
        ->where('trainings.id', '=', $id)
        ->join('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
        ->join('training_type','training_type.id','=','training_course.id_training_type')
        ->join('training_option', 'training_option.id', '=', 'training_course.id_training_option')
        ->join('course_category', 'course_category.id', '=', 'training_course.id_course_category')
        ->leftjoin('training_partner_course', 'training_partner_course.id_training_course', '=', 'training_course.id_training_course')
        ->leftjoin('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
        ->select(
            'training_course.id_training_course', 
            'course_category.id as course_id',
            'course_category.course_name as category_name',
            'training_course.course_name as course_name',
            'new_fee',
            'new_fee_int', 
            'training_option.id as option_id',
            'training_option.option as option_name',
            'training_type.id as type_id',
            'training_type.name as type_name',
            'training_partner_course.course_code as course_code',
            'training_cert_code.code as cert_code',
            )
            ->first();
        $objectives = DB::table('training_objective')
            ->join('trainings', 'trainings.id', '=', 'training_objective.id_training')
            ->where('trainings.id', '=', $id)
            ->select(
                'training_objective.objective',
            )
            ->get();

            $courseList = DB::table('training_course')
            ->where('trainings.id', '=', $id)
            ->join('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
            ->join('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->join('training_option', 'training_option.id', '=', 'training_course.id_training_option')
            ->join('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('training_partner_course', 'training_partner_course.id_training_course', '=', 'training_course.id_training_course')
            ->leftjoin('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
            ->select(
                'training_course.id_training_course',
                'course_category.id as course_id',
                'course_category.course_name as category_name',
                'training_course.course_name as course_name',
                'new_fee',
                'new_fee_int',
                'training_option.id as option_id',
                'training_option.option as option_name',
                'training_type.id as type_id',
                'training_type.name as type_name',
                'training_partner_course.course_code as course_code',
                'training_cert_code.code as cert_code',
            )
            ->first();

            
        $partnertr = DB::table('trainings')
            ->where('trainings.id', '=', $id)
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftjoin('training_partner_course', 'training_partner_course.id_training_course', '=', 'training_course.id_training_course')
            ->leftjoin('company', 'trainings.id_company', '=', 'company.id')
            ->leftjoin('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
            ->select(
                'company.id as company_id',
                'company.code as company_code',
                'company.name as company_name',
                'company.address_1',
                'company.address_2',
                'company.postcode',
                'training_partner_course.course_code as course_code',
                'training_cert_code.code as cert_code',
            )
            ->first();


        $trainingMode = DB::table('training_mode')
            ->select(
                'id as mode_id',
                'mode as mode_name',
            )
            ->get();
        $trainer = DB::table('users')
            ->join('trainer', 'trainer.id_users', '=', 'users.id')
            ->select('users.id as user_id', 'trainer.id as trainer_id', 'users.fullname') // Include the id columns with aliases
            ->get();
        /* $selectedReTypes = DB::table('course_re_type')
        ->where('id_training_course', '=', $id)
        ->pluck('id_re_type')
        ->toArray(); */
        $selectedReTypes = DB::table('training_course')
            ->join('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
            ->where('trainings.id', '=', $id)
            ->join('course_re_type', 'course_re_type.id_training_course', '=', 'training_course.id_training_course')
            ->pluck('course_re_type.id_re_type')
            ->toArray();
        $resitfees = DB::table('resit_fee')
            ->join('training_course', 'training_course.id_training_course', '=', 'resit_fee.id_training_course')
            ->join('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
            ->join('fee_type', 'fee_type.id_fee', '=', 'resit_fee.id_fee')
            ->where('trainings.id', '=', $id)
            ->select(
                'fee_type.fee_name',
                'resit_fee.amount',
                'resit_fee.fee_category',
            )
            ->get();
        $trainerList = DB::table('training_trainer')
            ->leftJoin('trainer', 'trainer.id', '=', 'training_trainer.id_trainer')
            ->leftJoin('users', 'users.id', 'trainer.id_users')
            ->leftJoin('trainings', 'training_trainer.id_training', '=', 'trainings.id')
            ->where('trainings.id', '=', $id)
            ->select(
                'users.fullname as trainer_fullname',
                'trainer.id as trainer_id',
            )
            ->get();

            // dd( $trainerList);
        $batchcode = DB::table('training_course')
            ->leftJoin('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
            ->leftJoin('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
            ->leftJoin('company', 'company.id', '=', 'trainings.id_company')
            ->where('trainings.id', '=', $id)
            ->select(
                'company.code as company_code',
                'training_cert_code.code as cert_code'
            )
            ->first();

        $fileuploadSupp = DB::table('upload_documents')
        ->leftJoin('trainings', 'trainings.id', '=', 'upload_documents.id_training')
        ->where('trainings.id', '=', $id)
        ->where('upload_documents.docType', '=', 'supporting_documents')
        ->select(
            'upload_documents.file_name as file_name',
            'upload_documents.id_users',
            'upload_documents.id_training',

        )
        ->get();

        // dd($fileuploadSupp);


        $fileuploadNotes = DB::table('upload_documents')
        ->leftJoin('trainings', 'trainings.id', '=', 'upload_documents.id_training')
        ->where('trainings.id', '=', $id)
        ->where('upload_documents.docType', '=', 'training_notes')
        ->select(
            'upload_documents.file_name as file_name',
            'upload_documents.id_users',
            'upload_documents.id_training',

        )
        ->get();


        return view('trainingMgmt.editTrainingRegistration', compact('fileuploadSupp','fileuploadNotes','userid', 'training', 'trainerList', 'batchcode', 'partnertr', 'courseList', 'trainer', 'trainingMode', 'selectedReTypes', 're_types', 'feetypes', 'resitfees', 'objectives'));
    }

    public function approveTrainingRegistration($id)
    {
        $userid = auth()->user()->id;
        $re_types = DB::table('re_type')
        ->get();
        $feetypes = DB::table('fee_type')
        ->get();
        $training = DB::table('trainings')
        ->where('trainings.id', '=', $id)
        ->join('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->join('training_type','training_type.id','=','training_course.id_training_type')
        ->join('course_category','course_category.id','=','training_course.id_course_category')
        ->select(
            'trainings.id',
            'training_course.course_name as course_name',
            DB::raw('trainings.date_start as date_start'),
            DB::raw('trainings.date_end as date_end'),
            'trainings.training_code',
            'trainings.venue',
            'trainings.total_participant',
            DB::raw('trainings.advert_start as advert_start'),
            DB::raw('trainings.advert_end as advert_end'),
            'trainings.batch_no',
            'trainings.hrdf_claim',
            'trainings.is_paid',
            'training_course.id_training_option',
            'trainings.training_mode',
            'trainings.training_url',
            'training_course.id_training_type as type_id',
            'training_type.name as training_type',
            'trainings.training_description',
            'trainings.document_description',
            'trainings.venue_address',
            'trainings.start_time',
            'trainings.end_time',
        )
        ->first();
        $objectives = DB::table('training_objective')
        ->join('trainings', 'trainings.id', '=', 'training_objective.id_training')
        ->where('trainings.id', '=', $id)
        ->select(
            'training_objective.objective',
        )
        ->get();
        $courseList = DB::table('training_course')
        ->where('trainings.id', '=', $id)
        ->join('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
        ->join('training_type','training_type.id','=','training_course.id_training_type')
        ->join('training_option', 'training_option.id', '=', 'training_course.id_training_option')
        ->join('course_category', 'course_category.id', '=', 'training_course.id_course_category')
        ->leftjoin('training_partner_course', 'training_partner_course.id_training_course', '=', 'training_course.id_training_course')
        ->leftjoin('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
        ->select(
            'training_course.id_training_course', 
            'course_category.id as course_id',
            'course_category.course_name as category_name',
            'training_course.course_name as course_name',
            'new_fee',
            'new_fee_int', 
            'training_option.id as option_id',
            'training_option.option as option_name',
            'training_type.id as type_id',
            'training_type.name as type_name',
            'training_partner_course.course_code as course_code',
            'training_cert_code.code as cert_code',
            )
        ->first();

        
        $partnertr = DB::table('trainings')
            ->where('trainings.id', '=', $id)
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftjoin('training_partner_course', 'training_partner_course.id_training_course', '=', 'training_course.id_training_course')
            ->leftjoin('company', 'training_partner_course.id_company', '=', 'company.id')
            ->leftjoin('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
            ->select(
                'company.id as company_id', 
                'company.code as company_code',
                'company.name as company_name',
                'company.address_1',
                'company.address_2',
                'company.postcode',
                'training_partner_course.course_code as course_code',
                'training_cert_code.code as cert_code',
            )
            ->first();

            // dd($partnertr);

        $trainingMode = DB::table('training_mode')
        ->select(
            'id as mode_id',
            'mode as mode_name',
        )
        ->get();
        $trainer = DB::table('users')
        ->join('trainer', 'trainer.id_users', '=', 'users.id')
        ->select('users.id as user_id', 'trainer.id as trainer_id', 'users.fullname') // Include the id columns with aliases
        ->get();
        /* $selectedReTypes = DB::table('course_re_type')
        ->where('id_training_course', '=', $id)
        ->pluck('id_re_type')
        ->toArray(); */
        $selectedReTypes = DB::table('training_course')
        ->join('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
        ->where('trainings.id', '=', $id)
        ->join('course_re_type', 'course_re_type.id_training_course', '=', 'training_course.id_training_course')
        ->pluck('course_re_type.id_re_type')
        ->toArray();
        $resitfees = DB::table('resit_fee')
        ->join('training_course', 'training_course.id_training_course', '=', 'resit_fee.id_training_course')
        ->join('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
        ->join('fee_type', 'fee_type.id_fee', '=', 'resit_fee.id_fee')
        ->where('trainings.id', '=', $id)
        ->select(
            'fee_type.fee_name',
            'resit_fee.amount',
            'resit_fee.fee_category',
        )
        ->get();
        $trainerList = DB::table('trainings')
        ->leftJoin('training_trainer', 'training_trainer.id', '=', 'trainings.id')
        ->leftJoin('trainer', 'trainer.id', '=', 'training_trainer.id')
        ->leftJoin('users', 'users.id', 'trainer.id_users')
        ->where('trainings.id', '=', $id)
        ->select(
            'users.fullname as trainer_fullname',
            'trainer.id as trainer_id',
        )
        ->get();
        $batchcode = DB::table('training_course')
        ->leftJoin('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
        ->leftJoin('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
        ->leftJoin('company', 'company.id', '=', 'trainings.id_company')
        ->where('trainings.id', '=', $id)
        ->select(
            'company.code as company_code',
            'training_cert_code.code as cert_code'
        )
        ->first();

        return view('trainingMgmt.approveTrainingRegistration' , compact('userid' , 'training' , 'trainerList' , 'batchcode' , 'partnertr' , 'courseList' , 'trainer', 'trainingMode', 'selectedReTypes', 're_types', 'feetypes', 'resitfees', 'objectives'));
    }

    public function rejectTraining(Request $request, $id)
    {
        $userid = auth()->user()->id;

        $training = Training::find($id);

        if (!$training) {
            return view('trainingMgmt.trainingMgmt', compact('userid'));
        }

        $username = auth()->user()->fullname;
        $userAgent = $request->header('User-Agent');
        $browser = 'Unknown';

        if (strpos($userAgent, 'Chrome') !== false) {
            $browser = 'Chrome';
        } elseif (strpos($userAgent, 'Firefox') !== false) {
            $browser = 'Firefox';
        } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
            $browser = 'Safari';
        } elseif (strpos($userAgent, 'Edg') !== false) {
            $browser = 'Edge';
        } elseif (strpos($userAgent, 'MSIE') !== false) {
            $browser = 'Internet Explorer';
        } elseif (strpos($userAgent, 'Opera') !== false) {
            $browser = 'Opera';
        }

        $training::where('trainings.id', $id)->update([
            'trainings.id_approval_status' => 3,
            'trainings.updated_at' => now(),
        ]);

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Rejected Training Application for training ID : ' . $id,
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

        return view('trainingMgmt.trainingMgmt', compact('userid'))->with('success', 'Training approved');
    }

    public function updateTrainingRegistration(Request $request, $id)
    {

        $userid = auth()->user()->id;

        // $this->validate($request, [
        //     'training_mode' => '',
        //     'venue' => '',
        //     'venue_address' => '',
        //     'state' => '',
        //     'total_participant' => '',
        //     'training_code' => '',
        //     'url' => '',
        //     'batch_no' => '',
        //     'date_start' => '',
        //     'date_end' => '',
        //     'start_time' => '',
        //     'end_time' => '',
        //     'advert_start' => '',
        //     'advert_end' => '',
        //     'hrdf_claim' => '',
        //     'document_description' => '',
        //     'training_description' => '',
        //     'supporting_documents.*' => '',
        //     'training_documents.*' => '',
        //     'training_image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        // ]);

        // $stateToRegion = [
        //     'Perlis' => 'Northern Region',
        //     'Kedah' => 'Northern Region',
        //     'Pulau Pinang' => 'Northern Region',
        //     'Perak' => 'Northern Region',
        //     'Selangor' => 'Central Region',
        //     'Kuala Lumpur' => 'Central Region',
        //     'Labuan' => 'Central Region',
        //     'Putrajaya' => 'Central Region',
        //     'Kelantan' => 'Eastern Region',
        //     'Terengganu' => 'Eastern Region',
        //     'Pahang' => 'Eastern Region',
        //     'Negeri Sembilan' => 'Southern Region',
        //     'Melaka' => 'Southern Region',
        //     'Johor' => 'Southern Region',
        //     'Sabah' => 'East Malaysia',
        //     'Sarawak' => 'East Malaysia',
        // ];

        // $selectedState = $request['state'];
    
        // $region = $stateToRegion[$selectedState];

        // $request['region'] = $region;

        // $fee = $request->input('fee');
        // $isPaid = $fee > 0 ? 1 : 0;

        $training = Training::find($id);

        if (!$training) {
            return view('trainingMgmt.trainingMgmt', compact('userid'));
        }

        $username = auth()->user()->fullname;
        $userAgent = $request->header('User-Agent');
        $browser = 'Unknown';

        if (strpos($userAgent, 'Chrome') !== false) {
            $browser = 'Chrome';
        } elseif (strpos($userAgent, 'Firefox') !== false) {
            $browser = 'Firefox';
        } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
            $browser = 'Safari';
        } elseif (strpos($userAgent, 'Edg') !== false) {
            $browser = 'Edge';
        } elseif (strpos($userAgent, 'MSIE') !== false) {
            $browser = 'Internet Explorer';
        } elseif (strpos($userAgent, 'Opera') !== false) {
            $browser = 'Opera';
        }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Approved Training Application ' . $id,
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

        $batch_no_full = $request['company_code'] . $request['batch_no'];

        $training::where('id', $id)->update([
            // 'training_mode' => $request->input('training_mode'),
            // 'venue' => $request->input('venue'),
            // 'venue_address' => $request->input('venue_address'),
            // 'state' => $request->input('state'),
            // 'region' => $request->input('region'),
            // 'id_training_course' => $request->input('trainingCourse'),
            // 'total_participant' => $request->input('total_participant'),
            // 'training_code' => $request->input('training_code'),
            // 'date_start' => $request->input('date_start'),
            // 'date_end' => $request->input('date_end'),
            // 'batch_no' => $request->input('batch_no'),
            // 'batch_no_full' => $batch_no_full,
            // 'id_company' => $request->input('company_id'),
            // 'start_time' => $request->input('start_time'),
            // 'end_time' => $request->input('end_time'),
            // 'advert_start' => $request->input('advert_start'),
            // 'advert_end' => $request->input('advert_end'),
            // 'training_url' => $request->input('url'),
            // 'is_paid' => $isPaid,
            // 'hrdf_claim' => $request->input('hrdf_claim'),
            // 'document_description' => $request->input('document_description'),
            // 'training_description' => $request->input('training_description'),
            
            'attendance_status' => 0,
            'results_status' => 0,
            'report_status' => 0,
            // 'status' => 1,
            // 'id_approval_status' => 2,
            // 'id_training_app_status' => 1,
            // 'id_training_status' => 1,
            'remarks' => $request->input('applicationQuery'),
            'updated_at' => now(),
        ]);
    
        // if ($request->hasFile('supporting_documents')) {
        //     foreach ($request->file('supporting_documents') as $document) {
        //         $documentPath = $document->store('supporting_documents');

        //         $uploadDocument = new Upload_Document([
        //             'file_path' => $documentPath,
        //             'file_name' => $document->getClientOriginalName(),
        //             'id_training' => $training->id,
        //             'file_format' => $document->getClientOriginalExtension(),
        //             'file_size' => $document->getSize(),
        //             'doc_type' => 'supporting_document',
        //             'updated_at' => now(),
        //         ]);

        //         $uploadDocument->save();
        //     }
        // }

        // if ($request->hasFile('training_documents')) {
        //     foreach ($request->file('training_documents') as $document) {
        //         $documentPath = $document->store('training_documents');

        //         $uploadDocument = new Upload_Document([
        //             'file_path' => $documentPath,
        //             'file_name' => $document->getClientOriginalName(),
        //             'id_training' => $training->id,
        //             'file_format' => $document->getClientOriginalExtension(),
        //             'file_size' => $document->getSize(),
        //             'doc_type' => 'training_notes',
        //             'updated_at' => now(),
        //         ]);

        //         $uploadDocument->save();
        //     }
        // }

        // $objectives = $request->input('training_objective');
        // if (!empty($objectives)) {
        //     foreach ($objectives as $index => $objective) {
        //         $existingObjective = TrainingObjective::where('id_training', $id)
        //             ->where('objective_index', $index)
        //             ->first();

        //         if ($existingObjective) {
        //             $existingObjective->update(['objective' => $objective]);
        //         } else {
        //             TrainingObjective::create([
        //                 'id_training' => $id,
        //                 'training_objective' => $objective,
        //                 'objective_index' => $index,
        //             ]);
        //         }
        //     }
        // }

        // $selectedTrainers = $request->input('trainer_id');
        // if (!empty($selectedTrainers)) {
        //     foreach ($selectedTrainers as $trainerId) {
        //         DB::table('training_trainer')->insert([
        //             'id_trainer' => $trainerId,
        //             'id_training' => $id,
        //         ]);
        //     }
        // }

        // if ($request->hasFile('training_image')) {
        //     $imagePath = $request->file('training_image')->store('training_posters', 'public');

        //     $training->training_image = $imagePath;
        //     $training->save();
        // }
        $user = DB::table('users')
        ->where('users.id', '=', $id)
        ->select(
            'users.fullname',
            'users.email',
        )
        ->first();
 
        $trainingpartner = DB::table('trainings')
        ->where('trainings.id', '=', $id)
        ->join('company', 'company.id', '=', 'trainings.id_company')
        ->select('trainings.id','trainings.id_company','trainings.id_training_course','trainings.id as trainingid',
        'company.id as company','company.name','company.email','trainings.remarks as query') 
        ->first();

        $query_remark = $trainingpartner->query;

            Mail::to($trainingpartner->email)->send(new TrainingAppQuery($trainingpartner,$query_remark));
            return view('trainingMgmt.trainingMgmt', compact('userid'));
    }


    public function updateToApproveTrainingRegistration(Request $request, $id)
    {

        $userid = auth()->user()->id;

        $this->validate($request, [
            'training_mode' => '',
            'venue' => '',
            'venue_address' => '',
            'state' => '',
            'total_participant' => '',
            'training_code' => '',
            'url' => '',
            'batch_no' => '',
            'date_start' => '',
            'date_end' => '',
            'start_time' => '',
            'end_time' => '',
            'advert_start' => '',
            'advert_end' => '',
            'hrdf_claim' => '',
            'document_description' => '',
            'training_description' => '',
            'supporting_documents.*' => '',
            'training_documents.*' => '',
            'training_image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $stateToRegion = [
            'Perlis' => 'Northern Region',
            'Kedah' => 'Northern Region',
            'Pulau Pinang' => 'Northern Region',
            'Perak' => 'Northern Region',
            'Selangor' => 'Central Region',
            'Kuala Lumpur' => 'Central Region',
            'Labuan' => 'Central Region',
            'Putrajaya' => 'Central Region',
            'Kelantan' => 'Eastern Region',
            'Terengganu' => 'Eastern Region',
            'Pahang' => 'Eastern Region',
            'Negeri Sembilan' => 'Southern Region',
            'Melaka' => 'Southern Region',
            'Johor' => 'Southern Region',
            'Sabah' => 'East Malaysia',
            'Sarawak' => 'East Malaysia',
        ];

        $selectedState = $request['state'];

        $region = $stateToRegion[$selectedState];

        $request['region'] = $region;

        $fee = $request->input('fee');
        $isPaid = $fee > 0 ? 1 : 0;

        $training = Training::find($id);

        if (!$training) {
            return view('trainingMgmt.trainingMgmt', compact('userid'));
        }

        $username = auth()->user()->fullname;
        $userAgent = $request->header('User-Agent');
        $browser = 'Unknown';

        if (strpos($userAgent, 'Chrome') !== false) {
            $browser = 'Chrome';
        } elseif (strpos($userAgent, 'Firefox') !== false) {
            $browser = 'Firefox';
        } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
            $browser = 'Safari';
        } elseif (strpos($userAgent, 'Edg') !== false) {
            $browser = 'Edge';
        } elseif (strpos($userAgent, 'MSIE') !== false) {
            $browser = 'Internet Explorer';
        } elseif (strpos($userAgent, 'Opera') !== false) {
            $browser = 'Opera';
        }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Approved Training Application ' . $id,
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

        $batch_no_full = $request['company_code'] . $request['batch_no'];

        $training::where('id', $id)->update([
            // 'training_mode' => $request->input('training_mode'),
            // 'venue' => $request->input('venue'),
            // 'venue_address' => $request->input('venue_address'),
            // 'state' => $request->input('state'),
            // 'region' => $request->input('region'),
            // 'id_training_course' => $request->input('trainingCourse'),
            // 'total_participant' => $request->input('total_participant'),
            // 'training_code' => $request->input('training_code'),
            // 'date_start' => $request->input('date_start'),
            // 'date_end' => $request->input('date_end'),
            // 'batch_no' => $request->input('batch_no'),
            // 'batch_no_full' => $batch_no_full,
            // 'id_company' => $request->input('company_id'),
            // 'start_time' => $request->input('start_time'),
            // 'end_time' => $request->input('end_time'),
            // 'advert_start' => $request->input('advert_start'),
            // 'advert_end' => $request->input('advert_end'),
            // 'training_url' => $request->input('url'),
            // 'is_paid' => $isPaid,
            // 'hrdf_claim' => $request->input('hrdf_claim'),
            // 'document_description' => $request->input('document_description'),
            // 'training_description' => $request->input('training_description'),
            // 'attendance_status' => 0,
            // 'results_status' => 0,
            // 'report_status' => 0,
            'status' => 1,
            'id_approval_status' => 2,
            'id_training_app_status' => 1,
            'id_training_status' => 1,
            'updated_at' => now(),
        ]);

        if ($request->hasFile('supporting_documents')) {
            foreach ($request->file('supporting_documents') as $document) {
                $documentPath = $document->store('supporting_documents');

                $uploadDocument = new Upload_Document([
                    'file_path' => $documentPath,
                    'file_name' => $document->getClientOriginalName(),
                    'id_training' => $training->id,
                    'file_format' => $document->getClientOriginalExtension(),
                    'file_size' => $document->getSize(),
                    'doc_type' => 'supporting_document',
                    'updated_at' => now(),
                    'id_users' => $userid,

                ]);

                $uploadDocument->save();
            }
        }

        if ($request->hasFile('training_documents')) {
            foreach ($request->file('training_documents') as $document) {
                $documentPath = $document->store('training_documents');

                $uploadDocument = new Upload_Document([
                    'file_path' => $documentPath,
                    'file_name' => $document->getClientOriginalName(),
                    'id_training' => $training->id,
                    'file_format' => $document->getClientOriginalExtension(),
                    'file_size' => $document->getSize(),
                    'doc_type' => 'training_notes',
                    'updated_at' => now(),
                    'id_users' => $userid,

                ]);

                $uploadDocument->save();
            }
        }

        $objectives = $request->input('training_objective');
        if (!empty($objectives)) {
            foreach ($objectives as $index => $objective) {
                $existingObjective = TrainingObjective::where('id_training', $id)
                    ->where('objective_index', $index)
                    ->first();

                if ($existingObjective) {
                    $existingObjective->update(['objective' => $objective]);
                } else {
                    TrainingObjective::create([
                        'id_training' => $id,
                        'training_objective' => $objective,
                        'objective_index' => $index,
                    ]);
                }
            }
        }

        $selectedTrainers = $request->input('trainer_id');
        if (!empty($selectedTrainers)) {
            foreach ($selectedTrainers as $trainerId) {
                DB::table('training_trainer')->insert([
                    'id_trainer' => $trainerId,
                    'id_training' => $id,
                ]);
            }
        }

        if ($request->hasFile('training_image')) {
            $imagePath = $request->file('training_image')->store('training_posters', 'public');

            $training->training_image = $imagePath;
            $training->save();
        }

        return view('trainingMgmt.trainingMgmt', compact('userid'));
    }

    public function trainingManagementTP()
    {
        $userid = auth()->user()->id;

        return view('trainingMgmt.trainingMgmtTP', compact('userid'));
    }

    public function trainingManagementListParticipantTP()
    {
        $userid = auth()->user()->id;

        return view('trainingMgmt.trainingListParticipant', compact('userid'));
    }


    public function approveApplyTraining(Request $request, $id)
    {
        $userid = auth()->user()->id;

        $administrator = DB::table('users')
            ->where('users.user_type', '=', 2)
            ->select(
                'users.fullname',
                'users.email'

            )
            ->get();
        $user = DB::table('users')
            ->where('users.id', '=', $userid)
            ->select(
                'users.fullname',
                'users.email'

            )
            ->first();

        $trainings = DB::table('trainings')
            ->where('trainings.id', '=', $id)
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('company', 'company.id', '=', 'trainings.id_company')
            ->select(
                'trainings.id',
                'training_course.course_name as course_name',
                DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
                DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
                'trainings.training_code',
                'trainings.venue',
                'trainings.fee',
                'trainings.total_participant',
                'trainings.training_mode',
                'trainings.training_description',
                'trainings.venue_address',
                'training_type.name as training_type',
                'trainings.start_time',
                'trainings.end_time',
                'company.name as companyname',
                'company.email as companyemail'

            )
            ->get();

        try {

            $username = auth()->user()->fullname;
            $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

            AuditTrail::create([
                'username' => $username ?? 'Guest',
                'activity' => 'Approve for Training ' . $id,
                'activity_time' => now(),
                'ip_address' => $request->ip(),
                'browser' => $browser,
            ]);

            $training = Application::find($id);

            $training::where('id', $id)->update([
                'id_approval_status' => 2,
                'training_status' => 1,
                'updated_at' => now(),
                'updated_by' => $userid
            ]);


            return redirect()->back()->with('success', 'Application approved');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }

    public function rejectApplyTraining(Request $request, $id)
    {
        $userid = auth()->user()->id;

        $administrator = DB::table('users')
            ->where('users.user_type', '=', 2)
            ->select(
                'users.fullname',
                'users.email'

            )
            ->get();
        $user = DB::table('users')
            ->where('users.id', '=', $userid)
            ->select(
                'users.fullname',
                'users.email'

            )
            ->first();

        $trainings = DB::table('trainings')
            ->where('trainings.id', '=', $id)
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('company', 'company.id', '=', 'trainings.id_company')
            ->select(
                'trainings.id',
                'training_course.course_name as course_name',
                DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
                DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
                'trainings.training_code',
                'trainings.venue',
                'trainings.fee',
                'trainings.total_participant',
                'trainings.training_mode',
                'trainings.training_description',
                'trainings.venue_address',
                'training_type.name as training_type',
                'trainings.start_time',
                'trainings.end_time',
                'company.name as companyname',
                'company.email as companyemail'

            )
            ->get();

        try {

            $username = auth()->user()->fullname;
            $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

            AuditTrail::create([
                'username' => $username ?? 'Guest',
                'activity' => 'Approve for Training ' . $id,
                'activity_time' => now(),
                'ip_address' => $request->ip(),
                'browser' => $browser,
            ]);

            $training = Application::find($id);

            $training::where('id', $id)->update([
                'id_approval_status' => 3,
                'training_status' => 10,
                'updated_at' => now(),
                'updated_by' => $userid
            ]);


            return redirect()->back()->with('success', 'Application rejected');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }

    public function trainingManagementAll()
    {
        $userid = auth()->user()->id;

        return view('trainingMgmt.trainingMgmtAll', compact('userid'));
    }

    public function trainingManagementTPAttendance($id)
    {
        $userid = auth()->user()->id;

        $listparticipant = DB::table('trainings')
            ->where('trainings.id', '=', $id)
            ->leftjoin('users', 'trainings.id_company', '=', 'users.id')
            ->leftjoin('company', 'company.id', '=', 'users.id_company')
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('training_participant', 'training_participant.id_training', '=', 'trainings.id')
            ->leftjoin('users as userA', 'training_participant.id_users', '=', 'userA.id')
            ->leftjoin('participant_attendance', 'training_participant.id', '=', 'participant_attendance.id_training_participant')
            ->select(
                'trainings.id',
                'trainings.training_name',
                'company.name as company_name',
                'course_category.course_name as category_name',
                'training_type.name as type_name',
                DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
                DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
                'trainings.training_status',
                'userA.fullname',
                'participant_attendance.attend_check'
            )
            ->get();

        $participants = DB::table('participant_attendance')
            ->leftjoin('training_participant', 'training_participant.id', '=', 'participant_attendance.id_training_participant')
            ->leftjoin('users as userA', 'training_participant.id_users', '=', 'userA.id')
            ->leftjoin('trainings', 'training_participant.id_training', '=', 'trainings.id')
            ->where('trainings.id', '=', $id)
            ->select('trainings.training_name', 'userA.fullname', 'participant_attendance.id', 'participant_attendance.id_training_participant', 'participant_attendance.attend_check')
            ->get();



        return view('trainingMgmt.trainingMgmtTPAttendance', compact('userid', 'listparticipant', 'participants'));
    }

    public function updateAttendance(Request $request)
    {
        $userId = $request->input('id');
        $attendanceStatus = $request->input('attendance_status');

        $username = auth()->user()->fullname;
        $username = auth()->user()->fullname;
        $userAgent = $request->header('User-Agent');
        $browser = 'Unknown';

        if (strpos($userAgent, 'Chrome') !== false) {
            $browser = 'Chrome';
        } elseif (strpos($userAgent, 'Firefox') !== false) {
            $browser = 'Firefox';
        } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
            $browser = 'Safari';
        } elseif (strpos($userAgent, 'Edg') !== false) {
            $browser = 'Edge';
        } elseif (strpos($userAgent, 'MSIE') !== false) {
            $browser = 'Internet Explorer';
        } elseif (strpos($userAgent, 'Opera') !== false) {
            $browser = 'Opera';
        }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Update User Attendance for User ID ' . $userId,
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

        // Update the attendance status in the database
        Participant_Attendance::where('id', $userId)->update(['attend_check' => $attendanceStatus]);

        return response()->json(['message' => 'Attendance updated successfully']);
    }

    public function deleteParticipant(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            DB::table('participant_attendance')->where('id_training_participant', $id)->delete();

            $trainingparticipant = DB::table('training_participant')->where('id', $id);

            $username = auth()->user()->fullname;
            $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

            AuditTrail::create([
                'username' => $username ?? 'Guest',
                'activity' => 'Delete Training Participant, ID ' . $id,
                'activity_time' => now(),
                'ip_address' => $request->ip(),
                'browser' => $browser,
            ]);

            if ($trainingparticipant) {
                DB::table('training_participant')->where('id', $id)->delete();

                DB::commit();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Participant Has Been Deleted',
                ], 200);
            } else {
                DB::rollback();

                return response()->json([
                    'status' => 'error',
                    'message' => 'Participant Failed to Delete',
                ], 404);
            }
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'An error occurred: ' . $e->getMessage(),
            ], 500);
        }
    }

    public function trainingManagementFeedbackQuestion()
    {
        $userid = auth()->user()->id;

        $feedbackquestion = DB::table('feedback_question')
            ->select(
                'feedback_question.id as questionid',
                'feedback_question.question',
                'feedback_question.feedback_type'
            )
            ->whereNull('feedback_question.deleted_at')
            ->orderBy('feedback_question.id', 'desc')
            ->get();



        return view('trainingMgmt.trainingMgmtFeedbackQuestion', compact('userid', 'feedbackquestion'));
    }

    public function trainingManagementFeedbackQuestionAdd(Request $request)
    {
        $userid = auth()->user()->id;

        $request->validate([
            'feedback_type' => 'required|string|max:255',
            'newQuestion' => 'required|string',
        ]);

        $newQuestion = $request->input('newQuestion');
        $feedback_type = $request->input('feedback_type');

        $username = auth()->user()->fullname;
        $userAgent = $request->header('User-Agent');
        $browser = 'Unknown';

        if (strpos($userAgent, 'Chrome') !== false) {
            $browser = 'Chrome';
        } elseif (strpos($userAgent, 'Firefox') !== false) {
            $browser = 'Firefox';
        } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
            $browser = 'Safari';
        } elseif (strpos($userAgent, 'Edg') !== false) {
            $browser = 'Edge';
        } elseif (strpos($userAgent, 'MSIE') !== false) {
            $browser = 'Internet Explorer';
        } elseif (strpos($userAgent, 'Opera') !== false) {
            $browser = 'Opera';
        }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Add Feedback Question',
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

        $question = new Feedback_question;
        $question->question = $newQuestion;
        $question->feedback_type = $feedback_type;
        $question->status = '1';
        $question->created_by = $userid;
        $question->created_at = Carbon::now();
        if ($question->save()) {
            // If successful, return success response
            return response()->json([
                'questionId' => $question->id,
                'newQuestion' => $newQuestion,
                'feedback_type' => $feedback_type,
                'success' => true,
                'message' => 'Question added successfully'
            ]);
        } else {
            // If there's an error, return error response
            return response()->json([
                'success' => false,
                'message' => 'Error adding question'
            ]);
        }
    }

    public function deleteFeedbackQuestion(Request $request,  $id)
    {
        $userid = auth()->user()->id;

        $username = auth()->user()->fullname;
        $userAgent = $request->header('User-Agent');
        $browser = 'Unknown';

        if (strpos($userAgent, 'Chrome') !== false) {
            $browser = 'Chrome';
        } elseif (strpos($userAgent, 'Firefox') !== false) {
            $browser = 'Firefox';
        } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
            $browser = 'Safari';
        } elseif (strpos($userAgent, 'Edg') !== false) {
            $browser = 'Edge';
        } elseif (strpos($userAgent, 'MSIE') !== false) {
            $browser = 'Internet Explorer';
        } elseif (strpos($userAgent, 'Opera') !== false) {
            $browser = 'Opera';
        }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Delete Feedback Question',
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

        $deletefeedback = DB::table('feedback_question')
            ->where('feedback_question.id', '=', $id)
            ->update([
                'updated_by' => $userid,
                'updated_at' => Carbon::now(),
                'deleted_by' => $userid,
                'deleted_at' => Carbon::now(),

            ]);

        if ($deletefeedback) {
            // If successful, return success response
            return response()->json([
                'deletefeedback' => $deletefeedback,
                'success' => true,
                'message' => 'Question deleted successfully'
            ]);
        } else {
            // If there's an error, return error response
            return response()->json([
                'success' => false,
                'message' => 'Error deleting question'
            ]);
        }
    }


    public function adminTrainingReport($batch_no, $ID) //view training management
    {
        $userid = auth()->user()->id;
        $reportcheck = DB::table('trainings')
        ->where('trainings.id_training_course', '=', $ID)
        ->where('trainings.batch_no_full', '=', $batch_no)
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->select('trainings.id',
        'trainings.batch_no_full',
        'trainings.training_code',
        'trainings.endorse_exam','trainings.endorse_feedback','trainings.endorse_attendance','trainings.endorse_report',
        'training_course.id_training_type'
    
        )
        ->groupby('trainings.batch_no_full')
        ->get();

        return view('trainingMgmt.trainingReport2', compact('userid','reportcheck'));
       
    }

    public function adminTrainingReportParticipant() //view training management
    {
        $userid = auth()->user()->id;
        return view('trainingMgmt.trainingReportParticipant', compact('userid'));
    }

    public function adminAttendanceList($batch_no, $ID)
    {
        $userid = auth()->user()->id;
     
        $reportcheck = DB::table('trainings')
        ->where('trainings.id_training_course', '=', $ID)
        ->where('trainings.batch_no_full', '=', $batch_no)
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->select('trainings.id',
        'trainings.batch_no_full',
        'trainings.training_code',
        'trainings.endorse_exam','trainings.endorse_feedback','trainings.endorse_attendance','trainings.endorse_report',
        'training_course.id_training_type'
        )
        ->groupby('trainings.batch_no_full')
        ->get();

        return view('trainingMgmt.adminAttendanceList', compact('userid','reportcheck'));
       
    } 

    public function adminFeedbackParticipant($batch_no, $ID) //view training management
    {
        $userid = auth()->user()->id;
        $reportcheck = DB::table('trainings')
        ->where('trainings.id_training_course', '=', $ID)
        ->where('trainings.batch_no_full', '=', $batch_no)
        ->where('id_approval_status','=',2)
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->select('trainings.id',
        'trainings.batch_no_full',
        'trainings.training_code',
        'trainings.endorse_exam','trainings.endorse_feedback','trainings.endorse_attendance','trainings.endorse_report',
        'training_course.id_training_type'
        )
        ->groupby('trainings.batch_no_full')
        ->get();

            return view('trainingMgmt.trainingFeedbackParticipant', compact('userid','reportcheck'));
       
    }

    public function participantFeedback($batch_no, $id)
    {
        // dd($batch_no);

        $userid = auth()->user()->id;
        


        return view('trainingMgmt.viewFeedbackParticipant', compact('userid'));
    }


    //TP
    public function TPAttendanceList($batch_no, $ID)
    {
        $userid = auth()->user()->id;
        $reportcheck = DB::table('trainings')
        ->where('trainings.id_training_course', '=', $ID)
        ->where('trainings.batch_no_full', '=', $batch_no)
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->select('trainings.id',
        'trainings.batch_no_full',
        'trainings.training_code',
        'trainings.endorse_exam','trainings.endorse_feedback','trainings.endorse_attendance','trainings.endorse_report',
        'training_course.id_training_type'
    
        )
        ->groupby('trainings.batch_no_full')
        ->get();
        return view('trainingMgmt.TPAttendanceList', compact('userid','reportcheck'));
    }

    public function TPFeedbackParticipant($batch_no, $ID) //view training management
    {
        $userid = auth()->user()->id;

        $reportcheck = DB::table('trainings')
        ->where('trainings.id_training_course', '=', $ID)
        ->where('trainings.batch_no_full', '=', $batch_no)
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->select('trainings.id',
        'trainings.batch_no_full',
        'trainings.training_code',
        'trainings.endorse_exam','trainings.endorse_feedback','trainings.endorse_attendance','trainings.endorse_report',
        'training_course.id_training_type'
    
        )
        ->groupby('trainings.batch_no_full')
        ->get();

        return view('trainingMgmt.TPFeedbackParticipant', compact('userid','reportcheck'));
    }

    public function TPTrainingReport($batch_no, $ID) //view training management
    {
        $userid = auth()->user()->id;
        $report = DB::table('trainings')
            ->where('trainings.id_training_course', '=', $ID)
            ->where('trainings.batch_no_full', '=', $batch_no)
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('company', 'company.id', '=', 'trainings.id_company')
            ->leftJoin('trainer', 'trainer.id_company', '=', 'company.id')
            ->leftJoin('training_trainer', 'training_trainer.id_trainer', '=', 'trainer.id')
            ->leftJoin('users', 'users.id', '=', 'trainer.id_users')
            ->leftJoin('salutation', 'salutation.id', '=', 'users.id_salutation')
            ->select(
                'trainings.id',
                'training_course.course_name AS training_name',
                'trainings.batch_no',
                'trainings.training_code',
                'trainings.training_description',
                'trainings.description',
                'users.fullname AS trainername',
                'salutation.salutation',
                'training_trainer.id_trainer',
                DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
                DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
            )
            ->groupby('training_trainer.id_trainer')
            ->get();

            $reportcheck = DB::table('trainings')
            ->where('trainings.id_training_course', '=', $ID)
            ->where('trainings.batch_no_full', '=', $batch_no)
            ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
            ->select('trainings.id',
            'trainings.batch_no_full',
            'trainings.training_code',
            'trainings.endorse_exam','trainings.endorse_feedback','trainings.endorse_attendance','trainings.endorse_report',
            'training_course.id_training_type'
        
            )
            ->groupby('trainings.batch_no_full')
            ->get();


        return view('trainingMgmt.TPtrainingReport2', compact('userid', 'report','reportcheck'));
    }

    public function TPTrainingReportParticipant() //view training management
    {
        $userid = auth()->user()->id;
        return view('trainingMgmt.TPtrainingReportParticipant', compact('userid'));
    }


    public function AdminPaymentToSeda($batch_no, $ID)
    {
        $userid = auth()->user()->id;

        $reportcheck = DB::table('trainings')
        ->where('trainings.id_training_course', '=', $ID)
        ->where('trainings.batch_no_full', '=', $batch_no)
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->select('trainings.id',
        'trainings.batch_no_full',
        'trainings.training_code',
        'trainings.endorse_exam','trainings.endorse_feedback','trainings.endorse_attendance','trainings.endorse_report',
        'training_course.id_training_type'
    
        )
        ->groupby('trainings.batch_no_full')
        ->get();

        return view('trainingMgmt.PaymentSedaAdmin', compact('userid', 'reportcheck'));
    }

    public function TPPaymentToSeda($batch_no, $ID)
    {
        $userid = auth()->user()->id;

        $reportcheck = DB::table('trainings')
            ->where('trainings.id_training_course', '=', $ID)
            ->where('trainings.batch_no', '=', $batch_no)
            ->select(
                'trainings.id',
                'trainings.batch_no',
                'trainings.training_code',
                'trainings.endorse_exam',
                'trainings.endorse_feedback',
                'trainings.endorse_attendance',
                'trainings.endorse_report'

            )
            ->groupby('trainings.batch_no')
            ->get();

        return view('trainingMgmt.PaymentSedaTP', compact('userid','reportcheck'));
    }
}
