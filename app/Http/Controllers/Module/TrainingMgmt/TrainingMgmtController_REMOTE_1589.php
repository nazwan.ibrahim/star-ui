<?php

namespace App\Http\Controllers\Module\TrainingMgmt;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Training;

class TrainingMgmtController extends Controller
{
    public function trainingManagement(Request $request)
{
    // Fetch all training records from the database and paginate them
    $trainings = Training::paginate(5);

    // Pass the paginated $trainings to the view
    return view('trainingMgmt.trainingMgmt', compact('trainings'));
}

    public function trainingRegistration()
    {
        return view('trainingMgmt.trainingRegistration');
    }

    public function editTrainingRegistration()
    {
        return view('trainingMgmt.editTrainingRegistration');
    }
}
