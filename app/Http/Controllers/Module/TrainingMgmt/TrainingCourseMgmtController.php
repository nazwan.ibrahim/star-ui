<?php

namespace App\Http\Controllers\Module\TrainingMgmt;

use App\Http\Controllers\Controller;
use App\Models\AuditTrail;
use Illuminate\Http\Request;
use App\Models\Training;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;
use App\Models\TrainingCourse;
use App\Models\courseReType;
use Carbon\Carbon;
use Exception;

class TrainingCourseMgmtController extends Controller
{
    public function trainingCourseManagement()
    {
        $userid = auth()->user()->id;
        return view('trainingMgmt.trainingCourseManagement', compact('userid'));
    }

    public function addTrainingCourse()
    {
        $userid = auth()->user()->id;

        $categories = DB::table('course_category')->get();
        $types = DB::table('training_type')->get();
        $options = DB::table('training_option')->get();
        $certs = DB::table('training_cert_code')->get();
        $re_types = DB::table('re_type')->get();
        $partners =  DB::table('company')   
        ->where('user_type','=','3')   
        ->select(
            'company.id',
            'company.name',
            'company.code',
        )      
        ->leftjoin('users', 'company.id', '=', 'users.id_company')
        ->groupBy('company.id')
        ->get();

        $feetypes = DB::table('fee_type')->get();

        return view('trainingMgmt.addTrainingCourse', compact('userid', 'categories', 'types', 're_types', 'partners', 'options', 'certs', 'feetypes'));
    }

    public function addTrainingCourseData(Request $request)
    {
        $userid = auth()->user()->id;
        try {
            $trainingCourseId = DB::table('training_course')->insertGetId([
                'course_name' => $request['course_name'],
                'id_course_category' => $request['course_category'],
                'id_training_type' => $request['training_type'],
                'course_description' => $request['course_description'],
                'id_training_cert' => $request['training_cert'],
                'id_training_option' => $request['training_option'],
                'new_fee' => $request['new_fee'],
                'new_fee_int' => $request['new_fee_int'],
                'created_by' => $userid,
                'created_at' => now(),
                'status' => 1
            ]);
// dd($request->input('training_partners', []));
            if ($trainingCourseId) {

                try {
                
                    $selectedReTypes = $request->input('re_types', []);
                    if($selectedReTypes != null){
                    foreach ($selectedReTypes as $reType) {
                        DB::table('course_re_type')->insert([
                            'id_re_type' => $reType,
                            'id_training_course' => $trainingCourseId,
                            'created_by' => $userid,
                            'created_at' => now(),
                            'status' => 1
                        ]);
                    }
                    }

                    } catch (\Exception $e) {
                        return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());

                }

                try{
                    $selectedFeeType = $request->input('fee_type', []);
                    $amount = $request->input('amount', []);
                    if ($selectedFeeType && count($selectedFeeType) > 0) {
                        foreach ($selectedFeeType as $key => $feeType) {
                            // Skip iterations where the fee type is the placeholder "Please Select"
                            if ($feeType != 0) {
                                DB::table('resit_fee')->insert([
                                    'id_training_course' => $trainingCourseId,
                                    'id_fee' => $feeType,
                                    'fee_category' => 'Local',
                                    'amount' => $amount[$key],
                                    'created_by' => $userid,
                                    'created_at' => now(),
                                    'status' => 1
                                ]);
                            }
                        }
                    }

                } catch (\Exception $e) {
                    return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());

                }
                
                try{
                    $selectedFeeTypeInt = $request->input('fee_typeint', []);
                    $amountint = $request->input('amountint', []);
                    if ($selectedFeeTypeInt && count($selectedFeeTypeInt) > 0) {

                    foreach ($selectedFeeTypeInt as $key => $feeTypeInt) {
                        if ($feeTypeInt != 0) {

                        DB::table('resit_fee')->insert([
                            'id_training_course' => $trainingCourseId,
                            'id_fee' => $feeTypeInt,
                            'fee_category' => 'Int',
                            'amount' => $amountint[$key],
                            'created_by' => $userid,
                            'created_at' => now(),
                            'status' => 1
                        ]);
                    }
                    }
                    }
                } catch (\Exception $e) {
                    return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());

                }
                


             

                    try {

                        $selectedTrainingPartners = $request->input('training_partners', []);
                        $courseCodes = $request->input('course_codes', []);

                        if ($selectedTrainingPartners && count($selectedTrainingPartners) > 0) {

                        foreach ($selectedTrainingPartners as $key => $trainingPartner) {
                            if ($trainingPartner != 0) {

                            DB::table('training_partner_course')->insert([
                                'id_company' => $trainingPartner,
                                'id_training_course' => $trainingCourseId,
                                'course_code' => $courseCodes[$key],
                                'created_by' => $userid,
                                'created_at' => now(),
                                'status' => 1
                            ]);
                        }
                        }
                        }
                    } catch (\Exception $e) {
                        return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());

                    }
            
                

                $userAgent = $request->header('User-Agent');
                $browser = 'Unknown';

                if (strpos($userAgent, 'Chrome') !== false) {
                    $browser = 'Chrome';
                } elseif (strpos($userAgent, 'Firefox') !== false) {
                    $browser = 'Firefox';
                } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                    $browser = 'Safari';
                } elseif (strpos($userAgent, 'Edg') !== false) {
                    $browser = 'Edge';
                } elseif (strpos($userAgent, 'MSIE') !== false) {
                    $browser = 'Internet Explorer';
                } elseif (strpos($userAgent, 'Opera') !== false) {
                    $browser = 'Opera';
                }

                AuditTrail::create([
                    'username' => auth()->user()->fullname ?? 'Guest',
                    'activity' => 'Created new Training Course.',
                    'activity_time' => now(),
                    'ip_address' => $request->ip(),
                    'browser' => $browser,
                ]);

                return redirect()->route('admin.training-course-management')->with('success', 'Success to create training course');
            } else {
                return redirect()->back()->with('error', 'Failed to create training course.');
            }
        } catch (Exception $ex) {
            return redirect()->back()->with('error', 'Failed to create training course.');
        }
    }

    public function viewTrainingCourse(Request $request, $id)
    {

        // dd($id);
        $userid = auth()->user()->id;
        $categories = DB::table('course_category')->get();
        $types = DB::table('training_type')->get();
        $re_types = DB::table('re_type')->get();
        $partners =  DB::table('company')   
        ->where('user_type','=','3')   
        ->select(
            'company.id',
            'company.name',
            'company.code',
        )      
        ->leftjoin('users', 'company.id', '=', 'users.id_company')
        ->groupBy('company.id')
        ->get();

        $certs = DB::table('training_cert_code')->get();
        $options = DB::table('training_option')->get();

        $courselist = DB::table('training_course')
            ->where('training_course.id_training_course', '=', $id)
            ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->leftjoin('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
            ->leftjoin('training_option', 'training_option.id', '=', 'training_course.id_training_option')
            ->select(
                'training_course.id_training_course',
                'training_course.id_course_category',
                'training_course.id_training_option',
                'training_course.id_training_type',
                'training_course.id_training_cert',
                'training_course.course_name',
                'training_cert_code.param as param',
                'training_option.option as option',
                'training_course.course_description',
                'course_category.course_name as category_name',
                'training_type.name as type_name',
                'training_course.new_fee',
                'training_course.new_fee_int',
                'training_course.course_description'
            )
            ->get();

        $selectedReTypes = DB::table('course_re_type')
            ->where('id_training_course', '=', $id)
            ->pluck('id_re_type')
            ->toArray();

        $trainingpartnerlist = DB::table('training_course')
            ->where('training_course.id_training_course', '=', $id)
            ->leftjoin('training_partner_course', 'training_course.id_training_course', '=', 'training_partner_course.id_training_course')
            ->leftjoin('company','company.id', '=', 'training_partner_course.id_company')
            ->where('training_partner_course.status', '=', 1)
            ->select(
                'company.name as training_partner',
                'training_partner_course.course_code',
            )
            ->get();

            // dd($trainingpartnerlist);

        $feeList = DB::table('resit_fee')
            ->where('training_course.id_training_course', '=', $id)
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'resit_fee.id_training_course')
            ->leftjoin('fee_type', 'fee_type.id_fee', '=', 'resit_fee.id_fee')
            ->where('resit_fee.status', '=', 1)
            ->select(
                'resit_fee.amount',
                'fee_type.fee_name',
                'resit_fee.fee_category',
            )
            ->get();

        return view('trainingMgmt.viewTrainingCourse', compact('userid', 'categories', 'types', 're_types', 'certs', 'options', 'courselist', 'selectedReTypes', 'partners', 'trainingpartnerlist', 'feeList'));
    }

    public function editTrainingCourse(Request $request, $id)
    {
        $userid = auth()->user()->id;
        $categories = DB::table('course_category')->get();
        $types = DB::table('training_type')->get();
        $re_types = DB::table('re_type')->get();
        $partners =  DB::table('company')   
        ->where('user_type','=','3')   
        ->select(
            'company.id',
            'company.name',
            'company.code',
        )      
        ->leftjoin('users', 'company.id', '=', 'users.id_company')
        ->groupBy('company.id')
        ->get();
        // dd($partners);
        $feetypes = DB::table('fee_type')->get();
        $certs = DB::table('training_cert_code')->get();
        $options = DB::table('training_option')->get();

        // Fetch the selected re_type values for the specific training course
        $selectedReTypes = DB::table('course_re_type')
            ->where('id_training_course', '=', $id)
            ->pluck('id_re_type')
            ->toArray();

                        // dd($selectedReTypes);


        $courselist = DB::table('training_course')
            ->where('training_course.id_training_course', '=', $id)
            ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->leftjoin('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
            ->leftjoin('training_option', 'training_option.id', '=', 'training_course.id_training_option')
            ->select(
                'training_course.id_training_course',
                'training_course.id_course_category',
                'training_course.id_training_option',
                'training_course.id_training_type',
                'training_course.id_training_cert',
                'training_course.course_name',
                'training_cert_code.param as param',
                'training_option.option as option',
                'training_course.course_description',
                'course_category.course_name as category_name',
                'training_type.name as type_name',
                'training_course.new_fee',
                'training_course.new_fee_int',
                'training_course.course_description'
            )
            ->get();
            // dd($courselist);

        $trainingpartnerlist = DB::table('training_course')
            ->where('training_course.id_training_course', '=', $id)
            ->leftjoin('training_partner_course', 'training_course.id_training_course', '=', 'training_partner_course.id_training_course')
            ->leftjoin('company', 'company.id', '=', 'training_partner_course.id_company')
            ->where('training_partner_course.status', '=', 1)
            ->select(
                'company.id',
                'company.name as name',
                'training_partner_course.id_training_partner_course',
                'training_partner_course.course_code',
            )
            ->get();

            // dd($trainingpartnerlist);


        $feeList = DB::table('resit_fee')
            ->where('training_course.id_training_course', '=', $id)
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'resit_fee.id_training_course')
            ->leftjoin('fee_type', 'fee_type.id_fee', '=', 'resit_fee.id_fee')
            ->where('resit_fee.status', '=', 1)
            ->select(
                'resit_fee.id_resit_fee',
                'resit_fee.id_fee',
                'resit_fee.amount',
                'fee_type.fee_name',
                'resit_fee.fee_category',
            )
            ->get();

            // dd( $feeList);


        return view('trainingMgmt.editTrainingCourse', compact('userid', 'certs', 'options', 'categories', 'feetypes', 'types', 're_types', 'courselist', 'selectedReTypes', 'partners', 'trainingpartnerlist', 'feeList'));
    }

    public function removeTpc(Request $request)
    {

        $userid = auth()->user()->id;
        $tpId = $request->input('tpId');
        $tpCourse = DB::table('training_partner_course')
            ->where('id_training_partner_course', $tpId)
            ->get();


        if (!$tpCourse) {
            return response()->json(['success' => false, 'message' => 'Trainer not found.']);
        }

        DB::beginTransaction();
        try {
            //Remove training partner course information
            DB::table('training_partner_course')
                ->where('id_training_partner_course', $tpId)
                ->delete();

            DB::commit();
            return response()->json(['success' => true, 'message' => 'Training partner course deleted successfully.']);
        } catch (Exception $ex) {
            // dd($ex->getMessage());
            DB::rollback();

            return response()->json(['success' => false, 'message' => 'Failed to delete.']);
        }
    }

    public function removeFee(Request $request)
    {
        $userid = auth()->user()->id;
        $resitId = $request->input('resitId');

        $resitFee = DB::table('resit_fee')
            ->where('id_resit_fee', $resitId)
            ->get();


        if (!$resitFee) {
            return response()->json(['success' => false, 'message' => 'Trainer not found.']);
        }

        DB::beginTransaction();
        try {
            //Remove Local Resit Fee
            DB::table('resit_fee')
                ->where('id_resit_fee', $resitId)
                ->delete();

                // ->update([
                //     'status' => 0,
                //     'deleted_by' => $userid,
                //     'deleted_at' => Carbon::now()
                // ]);

            DB::commit();
            return response()->json(['success' => true, 'message' => 'Resit fee deleted successfully.']);
        } catch (Exception $ex) {
            // dd($ex->getMessage());
            DB::rollback();

            return response()->json(['success' => false, 'message' => 'Failed to delete.']);
        }
    }

    public function updateCourseTraining(Request $request, $id)
    {
        $userid = auth()->user()->id;
        // dd($request->all());

        DB::beginTransaction();
        try {
            // Attempt to update the training course
            $updateTrainingCourse = DB::table('training_course')
                ->where('id_training_course', $id)
                ->update([
                    'course_name' => $request->input('course_name'),
                    'new_fee' => $request->input('new_fee'),
                    'new_fee_int' => $request->input('new_fee_int'),
                    'id_course_category' => $request->input('course_category'),
                    'id_training_cert' => $request->input('training_cert_code'),
                    'id_training_type' => $request->input('training_type'),
                    'course_description' => $request->input('course_description'),
                    'updated_by' => $userid,
                    'updated_at' => now(),
                    'status' => 1
                ]);

            if ($updateTrainingCourse) {
                $existingRecords = DB::table('course_re_type')
                    ->where('id_training_course', $id)
                    ->pluck('id_re_type')
                    ->toArray();

                $newRecordsToAdd = array_diff($request->input('re_types', []), $existingRecords);

                $recordsToRemove = array_diff($existingRecords, $request->input('re_types', []));

                foreach ($newRecordsToAdd as $newRecord) {
                    DB::table('course_re_type')->insert([
                        'id_training_course' => $id,
                        'id_re_type' => $newRecord,
                    ]);
                }

                foreach ($recordsToRemove as $recordToRemove) {
                    DB::table('course_re_type')
                        ->where('id_training_course', $id)
                        ->where('id_re_type', $recordToRemove)
                        ->delete();
                }

                // Update Training Partner Course Information
                if ($request->has('training_partners')) {
                    $tpcIDs = $request->input('id_training_partner_course');
                    $companyIDs = $request->input('training_partners');
                    $courseCodes = $request->input('course_codes');

                    foreach ($companyIDs as $key => $companyID) {
                        if ($companyID !== null && $companyID !== '') {
                            $compID = $companyID;
                            $tpcID = $tpcIDs[$key];
                            $courseCode = $courseCodes[$key];

                            // $existingTPCourse = DB::table('training_partner_course')
                            //     ->where('id_training_partner_course', $tpcID)->exists();

                            $existingTPCourse = DB::table('training_partner_course')
                ->where('id_training_course', $id)
                ->where('id_training_partner_course', $tpcID)
                ->exists();


                            if (!$existingTPCourse) {
                                DB::table('training_partner_course')
                                    ->insert([
                                        'id_training_course' => $id,
                                        'id_company' => $compID,
                                        'course_code' => $courseCode,
                                        'status' => 1,
                                        'created_at' => Carbon::now(),
                                        'created_by' => $userid,
                                    ]);
                            } else {
                                DB::table('training_partner_course')
                                    ->where('id_training_partner_course', $tpcID)
                                    ->update([
                                        'id_company' => $compID,
                                        'course_code' => $courseCode,
                                        'updated_by' => $userid,
                                        'updated_at' => Carbon::now(),
                                    ]);
                            }
                        }
                    }
                }

                //Update Re-Sit Fee Local Information
                if ($request->has('local_feetype')) {
                    $locResitIDs = $request->input('local_id_resit_fee');
                    $locFeeTypes = $request->input('local_feetype');
                    $locAmounts = $request->input('local_amount');

                    foreach ($locFeeTypes as $key => $locFeeType) {
                        if ($locFeeType !== null && $locFeeType !== '') {
                            $locFeeID = $locFeeType;
                            $locResitId = $locResitIDs[$key];
                            $locAmount = $locAmounts[$key];

                            $existingResitFee = DB::table('resit_fee')
                                ->where('id_resit_fee', $locResitId)->exists();

                            if (!$existingResitFee) {
                                DB::table('resit_fee')
                                    ->insert([
                                        'id_training_course' => $id,
                                        'id_fee' => $locFeeID,
                                        'amount' => $locAmount,
                                        'fee_category' => 'Local',
                                        'status' => 1,
                                        'created_at' => Carbon::now(),
                                        'created_by' => $userid,
                                    ]);
                            } else {
                                DB::table('resit_fee')
                                    ->where('id_resit_fee', $locResitId)
                                    ->update([
                                        'id_fee' => $locFeeID,
                                        'amount' => $locAmount,
                                        'updated_by' => $userid,
                                        'updated_at' => Carbon::now(),
                                    ]);
                            }
                        }
                    }
                }

                //Update Re-Sit Fee International Information
                if ($request->has('inter_feetype')) {
                    $locResitIDs = $request->input('inter_id_resit_fee');
                    $locFeeTypes = $request->input('inter_feetype');
                    $locAmounts = $request->input('inter_amount');

                    foreach ($locFeeTypes as $key => $locFeeType) {
                        if ($locFeeType !== null && $locFeeType !== '') {
                            $locFeeID = $locFeeType;
                            $locResitId = $locResitIDs[$key];
                            $locAmount = $locAmounts[$key];

                            $existingResitFee = DB::table('resit_fee')
                                ->where('id_resit_fee', $locResitId)->exists();

                            if (!$existingResitFee) {
                                DB::table('resit_fee')
                                    ->insert([
                                        'id_training_course' => $id,
                                        'id_fee' => $locFeeID,
                                        'amount' => $locAmount,
                                        'fee_category' => 'Int',
                                        'status' => 1,
                                        'created_at' => Carbon::now(),
                                        'created_by' => $userid,
                                    ]);
                            } else {
                                DB::table('resit_fee')
                                    ->where('id_resit_fee', $locResitId)
                                    ->update([
                                        'id_fee' => $locFeeID,
                                        'amount' => $locAmount,
                                        'updated_by' => $userid,
                                        'updated_at' => Carbon::now(),
                                    ]);
                            }
                        }
                    }
                }

                $userAgent = $request->header('User-Agent');
                $browser = 'Unknown';

                if (strpos($userAgent, 'Chrome') !== false) {
                    $browser = 'Chrome';
                } elseif (strpos($userAgent, 'Firefox') !== false) {
                    $browser = 'Firefox';
                } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                    $browser = 'Safari';
                } elseif (strpos($userAgent, 'Edg') !== false) {
                    $browser = 'Edge';
                } elseif (strpos($userAgent, 'MSIE') !== false) {
                    $browser = 'Internet Explorer';
                } elseif (strpos($userAgent, 'Opera') !== false) {
                    $browser = 'Opera';
                }

                AuditTrail::create([
                    'username' => auth()->user()->fullname ?? 'Guest',
                    'activity' => 'Updated training course ' . $id,
                    'activity_time' => now(),
                    'ip_address' => $request->ip(),
                    'browser' => $browser,
                ]);

                DB::commit();
                return redirect()->route('admin.training-course-management')->with('success', 'Training course updated successfully.');
            } else {
                return redirect()->back()->with('error', 'Failed to update the training course.');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }





    public function deleteTrainingCourse(Request $request, $id)
    {

        $userid = auth()->user()->id;

        try {
            DB::beginTransaction();


            $deleteData = DB::table('training_course')
                ->where('training_course.id_training_course', '=', $id)
                ->update([
                    'status' => 0,
                    'updated_by' => $userid,
                    'updated_at' => now(),
                    'deleted_by' => $userid,
                    'deleted_at' => now(),

                ]);

            if ($deleteData) {

                DB::commit();

                $userAgent = $request->header('User-Agent');
                $browser = 'Unknown';

                if (strpos($userAgent, 'Chrome') !== false) {
                    $browser = 'Chrome';
                } elseif (strpos($userAgent, 'Firefox') !== false) {
                    $browser = 'Firefox';
                } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                    $browser = 'Safari';
                } elseif (strpos($userAgent, 'Edg') !== false) {
                    $browser = 'Edge';
                } elseif (strpos($userAgent, 'MSIE') !== false) {
                    $browser = 'Internet Explorer';
                } elseif (strpos($userAgent, 'Opera') !== false) {
                    $browser = 'Opera';
                }

                AuditTrail::create([
                    'username' => auth()->user()->fullname ?? 'Guest',
                    'activity' => 'Deleted training course ' . $id,
                    'activity_time' => now(),
                    'ip_address' => $request->ip(),
                    'browser' => $browser,
                ]);

                return redirect()->route('admin.training-course-management')->with('success', 'Training course deleted successfully');
            } else {
                DB::rollback();

                return redirect()->route('admin.training-course-management')->with('error', 'Training course not found');
            }
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->route('admin.training-course-management')->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }
}
