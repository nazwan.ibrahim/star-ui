<?php

namespace App\Http\Controllers\Module\TrainingMgmt;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Training;
use Illuminate\Support\Facades\DB;
use App\Models\TrainingCourse;
use App\Models\courseReType;
use App\Models\Participant;
use App\Models\Participant_Attendance;
use App\Models\Feedback_question;
use App\Models\Trainer;
use App\Models\Users;
use Carbon\Carbon;
use App\Models\UploadDocuments;
use Illuminate\Http\Response;
use Barryvdh\DomPDF\Facade\Pdf;
use setasign\fpdf\FPDF as aFPDF;
use FPDF; 

class CertificateController extends Controller
{
    public function generateCertificate(Request $request,$id)
    {
        // $userid = auth()->user()->id;
        // dd($id);

        // $user = Users::find($id);

        // if (!$user) {
        //     // Handle the case when the user is not found
        //     abort(404, 'User not found');
        // }
        set_time_limit(120); // Set maximum execution time to 120 seconds
        $user = auth()->user();

        $data = DB::table('users')
        ->select(
            'users.fullname',
            'users.ic_no',
            'training_course.course_name',
            'trainings.venue',
            DB::raw('DATE_FORMAT(trainings.date_start, "%d/%m/%Y") as date_start'),
            DB::raw('DATE_FORMAT(trainings.date_end, "%d/%m/%Y") as date_end'),
            ) 
            ->leftjoin('training_participant', 'training_participant.id_users', '=', 'users.id')
            ->leftjoin('trainings', 'training_participant.id_training', '=', 'trainings.id')
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->where('users.id', $user->id)
            ->where('trainings.id', '=', $id)
            ->first();

            // dd($data);


            // $uniqueData = $data->unique('ic_no');



        $pdf = Pdf::loadview('dashboard.participant.participantCertificateCOA',
        [
            'data' => $data,
        ]);

        // Set the paper orientation to landscape
        $pdf->setPaper('landscape');

        return $pdf->download($data->ic_no . 'COA.pdf');
        }

    public function generateCertificateQP(Request $request,$id)
    {
        set_time_limit(120); // Set maximum execution time to 120 seconds

        $userid = auth()->user()->id;

        $data = DB::table('users')
        ->select(
            'users.fullname',
            'users.ic_no',
            'training_course.course_name',
            'trainings.venue',
            'participant_certificate.certificate_no',
            DB::raw('DATE_FORMAT(trainings.date_start, "%d/%m/%Y") as date_start'),
            DB::raw('DATE_FORMAT(trainings.date_end, "%d/%m/%Y") as date_end'),
            ) 
            ->leftjoin('training_participant', 'training_participant.id_users', '=', 'users.id')
            ->leftjoin('trainings', 'training_participant.id_training', '=', 'trainings.id')
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftjoin('participant_certificate', 'participant_certificate.id_training_participant', '=', 'training_participant.id')
            ->where('users.id', $userid)
            ->where('trainings.id', '=', $id)
            ->first();

            // dd($data);


            // $uniqueData = $data->unique('ic_no');

        $pdf = Pdf::loadView('dashboard.participant.participantCertificateCQP',
        [
            'data' => $data,
        ]);

        // Set the paper orientation to landscape
        $pdf->setPaper('landscape');

        return $pdf->download($data->ic_no .'CQP.pdf');
    
    }

    public function generateAllCertificate(Request $request,$id)
    {
       
        $userid = auth()->user()->id;

        $data = DB::table('users')
        ->select(
            'users.fullname',
            'users.ic_no',
            'training_course.course_name',
            'trainings.venue',
            'participant_certificate.certificate_no',
            DB::raw('DATE_FORMAT(trainings.date_start, "%d/%m/%Y") as date_start'),
            DB::raw('DATE_FORMAT(trainings.date_end, "%d/%m/%Y") as date_end'),
            ) 
            ->leftjoin('training_participant', 'training_participant.id_users', '=', 'users.id')
            ->leftjoin('trainings', 'training_participant.id_training', '=', 'trainings.id')
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftjoin('participant_certificate', 'participant_certificate.id_training_participant', '=', 'training_participant.id')
            ->where('trainings.id', $id)
            ->get();

            // dd($data);


            $uniqueData = $data->unique('ic_no');

       // Create an array to store individual PDFs
        $pdfs = [];

        foreach ($data as $userdetail) {
            $pdf = Pdf::loadView('dashboard.participant.participantCertificateCOA', ['data' => [$userdetail]]);
            $pdf->setPaper('landscape');

            // Add the PDF to the array
            $pdfs[] = $pdf;
        }


        // Merge all PDFs into a single PDF
        $mergedPdf = new FPDF();
        foreach ($pdfs as $pdf) {
            $mergedPdf->AddPage();
            $mergedPdf->SetFont('Arial', '', 12);
            $mergedPdf->Write(10, $pdf->Output([],'S'));
            }

        // Download the merged PDF

        return Response::make($mergedPdf->Output(), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename=all_COA.pdf',
        ]);
    
    }


    public function generateAllCertificateQP(Request $request,$id)
    {
       
        $userid = auth()->user()->id;

        $data = DB::table('users')
        ->select(
            'users.fullname',
            'users.ic_no',
            'training_course.course_name',
            'trainings.venue',
            'participant_certificate.certificate_no',
            DB::raw('DATE_FORMAT(trainings.date_start, "%d/%m/%Y") as date_start'),
            DB::raw('DATE_FORMAT(trainings.date_end, "%d/%m/%Y") as date_end'),
            ) 
            ->leftjoin('training_participant', 'training_participant.id_users', '=', 'users.id')
            ->leftjoin('trainings', 'training_participant.id_training', '=', 'trainings.id')
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftjoin('participant_certificate', 'participant_certificate.id_training_participant', '=', 'training_participant.id')
            ->where('users.id', $userid)
            ->get();

            // dd($data);


            $uniqueData = $data->unique('ic_no');

        // Create an array to store individual PDFs
    $pdfs = [];

    foreach ($data as $userdetail) {
        $pdf = Pdf::loadView('dashboard.participant.participantCertificateCQP', ['data' => [$userdetail]]);
        $pdf->setPaper('landscape');

        // Add the PDF to the array
        $pdfs[] = $pdf;
    }


    // Merge all PDFs into a single PDF
    $mergedPdf = new FPDF();
    foreach ($pdfs as $pdf) {
        $mergedPdf->AddPage();
        $mergedPdf->SetFont('Arial', '', 12);
        $mergedPdf->Write(10, $pdf->Output([],'S'));
        }

    // Download the merged PDF

    return Response::make($mergedPdf->Output(), 200, [
        'Content-Type' => 'application/pdf',
        'Content-Disposition' => 'inline; filename=all_CQP.pdf',
    ]);
    }
}