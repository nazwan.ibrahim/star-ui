<?php

namespace App\Http\Controllers\Module\TrainingMgmt;

use App\Http\Controllers\Controller;
use App\Models\Application;
use App\Models\AuditTrail;
use App\Models\TrainingObjective;
use Illuminate\Http\Request;
use App\Models\Training;
use Illuminate\Support\Facades\DB;
use App\Models\TrainingCourse;
use App\Models\courseReType;
use App\Models\Participant;
use App\Models\Participant_Attendance;
use App\Models\Feedback_question;
use App\Models\Trainer;
use Carbon\Carbon;
use App\Models\Upload_Document;
use App\Models\UploadDocuments;

class TrainingApplicationApprovalController extends Controller
{
    public function trainingManagementListParticipantTP()
    {
        $userid = auth()->user()->id;

        return view('trainingApplication.trainingListParticipant' ,compact('userid'));
    }
    public function trainingManagementListParticipantAdmin()
    {
        $userid = auth()->user()->id;

        return view('trainingApplication.trainingListParticipantAdmin' ,compact('userid'));
    }
    public function trainingManagementListPayParticipantTP()
    {
        $userid = auth()->user()->id;

        return view('trainingApplication.trainingListPayParticipant' ,compact('userid'));
    }
    public function trainingManagementListPayParticipantAdmin()
    {
        $userid = auth()->user()->id;

        return view('trainingApplication.trainingListPayParticipantAdmin' ,compact('userid'));
    }

    public function trainingManagementParticipantDetailsAdmin()
    {
        $userid = auth()->user()->id;

        return view('trainingApplication.trainingParticipantDetailsAdmin' ,compact('userid'));
    }

    public function trainingManagementParticipantDetailsTP()
    {
        $userid = auth()->user()->id;

        return view('trainingApplication.trainingParticipantDetails' ,compact('userid'));
    }


    public function approveApplyTraining(Request $request, $id)
    {
        $userid = auth()->user()->id;

        $administrator = DB::table('users')
            ->where('users.user_type', '=', 2)
            ->select(
                'users.fullname',
                'users.email'

            )
            ->get();
        $user = DB::table('users')
            ->where('users.id', '=', $userid)
            ->select(
                'users.fullname',
                'users.email'

            )
            ->first();

        $trainings = DB::table('trainings')
            ->where('trainings.id', '=', $id)
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('company', 'company.id', '=', 'trainings.id_company')
            ->select(
                'trainings.id',
                'training_course.course_name as course_name',
                DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
                DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
                'trainings.training_code',
                'trainings.venue',
                'trainings.fee',
                'trainings.total_participant',
                'trainings.training_mode',
                'trainings.training_description',
                'trainings.venue_address',
                'training_type.name as training_type',
                'trainings.start_time',
                'trainings.end_time',
                'company.name as companyname',
                'company.email as companyemail'

            )
            ->get();

        try {

            $username = auth()->user()->fullname;
            $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

            AuditTrail::create([
                'username' => $username ?? 'Guest',
                'activity' => 'Approve for Training ' . $id,
                'activity_time' => now(),
                'ip_address' => $request->ip(),
                'browser' => $browser,
            ]);

            $training = Application::find($id);

            $training::where('id', $id)->update([
                'id_approval_status' => 2,
                'training_status' => 1,
                'updated_at' => now(),
                'updated_by' => $userid
            ]);


            return redirect()->back()->with('success', 'Application approved');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }

    public function rejectApplyTraining(Request $request, $id)
    {
        $userid = auth()->user()->id;

        $administrator = DB::table('users')
            ->where('users.user_type', '=', 2)
            ->select(
                'users.fullname',
                'users.email'

            )
            ->get();
        $user = DB::table('users')
            ->where('users.id', '=', $userid)
            ->select(
                'users.fullname',
                'users.email'

            )
            ->first();

        $trainings = DB::table('trainings')
            ->where('trainings.id', '=', $id)
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('company', 'company.id', '=', 'trainings.id_company')
            ->select(
                'trainings.id',
                'training_course.course_name as course_name',
                DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
                DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
                'trainings.training_code',
                'trainings.venue',
                'trainings.fee',
                'trainings.total_participant',
                'trainings.training_mode',
                'trainings.training_description',
                'trainings.venue_address',
                'training_type.name as training_type',
                'trainings.start_time',
                'trainings.end_time',
                'company.name as companyname',
                'company.email as companyemail'

            )
            ->get();

        try {

            $username = auth()->user()->fullname;
            $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

            AuditTrail::create([
                'username' => $username ?? 'Guest',
                'activity' => 'Approve for Training ' . $id,
                'activity_time' => now(),
                'ip_address' => $request->ip(),
                'browser' => $browser,
            ]);

            $training = Application::find($id);

            $training::where('id', $id)->update([
                'id_approval_status' => 3,
                'training_status' => 10,
                'updated_at' => now(),
                'updated_by' => $userid
            ]);


            return redirect()->back()->with('success', 'Application rejected');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }


    public function approvePayTraining(Request $request, $id)
    {
        $userid = auth()->user()->id;

        $administrator = DB::table('users')
            ->where('users.user_type', '=', 2)
            ->select(
                'users.fullname',
                'users.email'

            )
            ->get();
        $user = DB::table('users')
            ->where('users.id', '=', $userid)
            ->select(
                'users.fullname',
                'users.email'

            )
            ->first();

        $trainings = DB::table('trainings')
            ->where('trainings.id', '=', $id)
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('company', 'company.id', '=', 'trainings.id_company')
            ->select(
                'trainings.id',
                'training_course.course_name as course_name',
                DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
                DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
                'trainings.training_code',
                'trainings.venue',
                'trainings.fee',
                'trainings.total_participant',
                'trainings.training_mode',
                'trainings.training_description',
                'trainings.venue_address',
                'training_type.name as training_type',
                'trainings.start_time',
                'trainings.end_time',
                'company.name as companyname',
                'company.email as companyemail'

            )
            ->get();

        try {

            $username = auth()->user()->fullname;
            $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

            AuditTrail::create([
                'username' => $username ?? 'Guest',
                'activity' => 'Approve for Training ' . $id,
                'activity_time' => now(),
                'ip_address' => $request->ip(),
                'browser' => $browser,
            ]);

            $training = Application::find($id);

            $training::where('id', $id)->update([
                'status_payment' => 1,
                'training_status' => 2,
                'updated_at' => now(),
                'updated_by' => $userid
            ]);


            return redirect()->back()->with('success', 'Application approved');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }

    public function rejectPayTraining(Request $request, $id)
    {
        $userid = auth()->user()->id;

        $administrator = DB::table('users')
            ->where('users.user_type', '=', 2)
            ->select(
                'users.fullname',
                'users.email'

            )
            ->get();
        $user = DB::table('users')
            ->where('users.id', '=', $userid)
            ->select(
                'users.fullname',
                'users.email'

            )
            ->first();

        $trainings = DB::table('trainings')
            ->where('trainings.id', '=', $id)
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('company', 'company.id', '=', 'trainings.id_company')
            ->select(
                'trainings.id',
                'training_course.course_name as course_name',
                DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
                DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
                'trainings.training_code',
                'trainings.venue',
                'trainings.fee',
                'trainings.total_participant',
                'trainings.training_mode',
                'trainings.training_description',
                'trainings.venue_address',
                'training_type.name as training_type',
                'trainings.start_time',
                'trainings.end_time',
                'company.name as companyname',
                'company.email as companyemail'

            )
            ->get();

        try {

            $username = auth()->user()->fullname;
            $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

            AuditTrail::create([
                'username' => $username ?? 'Guest',
                'activity' => 'Approve for Training ' . $id,
                'activity_time' => now(),
                'ip_address' => $request->ip(),
                'browser' => $browser,
            ]);

            $training = Application::find($id);

            $training::where('id', $id)->update([
                'id_approval_status' => 3,
                'training_status' => 1,
                'updated_at' => now(),
                'updated_by' => $userid
            ]);


            return redirect()->back()->with('success', 'Application rejected');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }

    
}
