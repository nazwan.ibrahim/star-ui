<?php

namespace App\Http\Controllers\Module\ExamMgmt;

use App\Http\Controllers\Controller;
use App\Models\AuditTrail;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class ExamMgmtController extends Controller
{
    public function examList() {

        $userid = auth()->user()->id;
        return view('examMgmt.examResultList', compact('userid'));

      }

     
    public function updateExamResult(Request $request, $id,$batch_no_full) {//TP and Admin
        $userid = auth()->user()->id;

        $username = auth()->user()->fullname;
        $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Endorsed Exam Result for Training ' . $id . ', Batch ' . $batch_no_full,
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);
       // dd($id);
        $HeaderResult = DB::table('exam_result_header')
       
            ->where('exam_result_header.id_training_course', '=', $id)
            ->orWhere('exam_result_header.batch_no_full', '=', $batch_no_full)
            ->get();

        return view('examMgmt.updateExamResult', compact('userid','HeaderResult','batch_no_full'));

    }

    public function updateExamResult_GCPVDESIGN(Request $request, $id,$batch_no_full) {//TP and Admin
        $userid = auth()->user()->id;

        $username = auth()->user()->fullname;
        $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Endorsed Exam Result for Training ' . $id . ', Batch ' . $batch_no_full,
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);
       // dd($id);
        $HeaderResult = DB::table('examGCPV_result_header')
       
            // ->where('examGCPV_result_header.id_training_course', '=', $id)
            // ->orWhere('examGCPV_result_header.batch_no_full', '=', $batch_no_full)
            ->get();

        return view('examMgmt.SedaAdmin.updateExamResult_GCPVDESIGN', compact('userid','HeaderResult','batch_no_full'));

    }

  //   public function updateExamResultAdmin($id,$batch_no_full) {
  //     $userid = auth()->user()->id;
  //     $HeaderResult = DB::table('exam_result_header')
  //         ->where('exam_result_header.id_training', '=', $id)
  //         ->orWhere('exam_result_header.batch_no_full', '=', $batch_no_full)
  //         ->get();

  //     return view('examMgmt.updateExamResult', compact('userid','HeaderResult','batch_no_full'));

  // }

    public function examListAdmin() {

        $userid = auth()->user()->id;
        return view('examMgmt.SedaAdmin.examResultListAdmin', compact('userid'));

      }


    public function endorseExamResult(Request $request, $ID,$batch_no) { //only review and query at examresult
      //dd($ID);
        $userid = auth()->user()->id;

        $username = auth()->user()->fullname;
        $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Endorsed Exam Result for Training ' . $ID . ', Batch ' . $batch_no,
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

        $HeaderResult = DB::table('exam_result_header')
        ->where('exam_result_header.id_training_course', '=', $ID)
        //->orWhere('exam_result_header.batch_no_full', '=', $batch_no)
        ->get();
        return view('examMgmt.SedaAdmin.endorseExamResult', compact('userid','HeaderResult'));

    }


    public function endorseExamResult_GCPVDESIGN(Request $request, $ID,$batch_no) { //only review and query at examresult

        $userid = auth()->user()->id;

        $username = auth()->user()->fullname;
        $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Endorsed Exam Result for Training ' . $ID . ', Batch ' . $batch_no,
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

        $HeaderResult_GCPVDESIGN = DB::table('examGCPV_result_header')
        ->where('examGCPV_result_header.id_training_course', '=', $ID)
        //->orWhere('examCGPV_result_header.batch_no_full', '=', $batch_no)
        ->get();
        return view('examMgmt.SedaAdmin.endorseExamResult_GCPVDESIGN', compact('userid','HeaderResult_GCPVDESIGN'));

      }


    public function endorseExamResult2(Request $request, $batch_no,$ID) {  //admin endorse at trainingmanagemnet
      $userid = auth()->user()->id;

      $username = auth()->user()->fullname;
      $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Endorsed Exam Result for Training ' . $ID . ', Batch ' . $batch_no,
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

      $reportcheck = DB::table('trainings')
        ->where('trainings.id_training_course', '=', $ID)
        ->where('trainings.batch_no_full', '=', $batch_no)
        ->select('trainings.id',
        'trainings.batch_no_full',
        'trainings.training_code',
        'trainings.endorse_exam','trainings.endorse_feedback','trainings.endorse_attendance','trainings.endorse_report'
    
        )
        ->groupby('trainings.batch_no')
        ->get();

        $HeaderResult = DB::table('exam_result_header')
        ->where('exam_result_header.id_training_course', '=', $ID)
        //->where('exam_result_header.batch_no_full', '=', $batch_no_full)
        ->get();

      return view('examMgmt.SedaAdmin.endorseExamResult2', compact('userid','reportcheck','HeaderResult'));

    }
  
    public function endorseExamResultTP(Request $request, $batch_no_full,$id) {
      $userid = auth()->user()->id;

      $username = auth()->user()->fullname;
      $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Endorsed Exam Result for Training ' . $id . ', Batch ' . $batch_no_full,
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

      $HeaderResult = DB::table('exam_result_header')
      ->where('exam_result_header.id_training_course', '=', $id)
      ->orWhere('exam_result_header.batch_no_full', '=', $batch_no_full)
      ->get();
      return view('examMgmt.SedaAdmin.endorseExamResultTP', compact('userid','HeaderResult'));

    }


    
    public function endorseExamResultTP_GCPV(Request $request, $ID,$batch_no) { 

        $userid = auth()->user()->id;

        $username = auth()->user()->fullname;
        $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Endorsed Exam Result for Training ' . $ID . ', Batch ' . $batch_no,
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

        $HeaderResult_GCPVDESIGN = DB::table('examGCPV_result_header')
        ->where('examGCPV_result_header.id_training_course', '=', $ID)
        ->get();


        return view('examMgmt.SedaAdmin.endorseExamResultTP_GCPV', compact('userid','HeaderResult_GCPVDESIGN'));

      }


 




}
