<?php

namespace App\Http\Controllers\Module\UserProfile;

use App\Http\Controllers\Controller;
use App\Models\Trainer;
use Illuminate\Http\Request;
use App\Models\userProfile;
use App\Models\Users;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserProfileController extends Controller
{
  public function viewPdf($base64Content, $filename)
  {
    $pdfContent = base64_decode($base64Content);

    return response($pdfContent, 200, [
      'Content-Type' => 'application/pdf',
      'Content-Disposition' => 'inline; filename="' . $filename . '"',
    ]);
  }

  public function userProfile()
  {
    $userid = auth()->user()->id;

    //User Information
    $userInfo = DB::table('users')
      ->where('users.id', '=', $userid)
      ->leftJoin('gender', 'gender.id', '=', 'users.id_gender')
      ->leftJoin('nationality', 'nationality.id_nationality', '=', 'users.id_nationality')
      ->leftjoin('state', 'state.id', '=', 'users.id_state')
      ->leftjoin('country', 'country.id', '=', 'users.id_country')
      ->leftjoin('city', 'city.id', '=', 'users.id_city')
      ->leftjoin('education', 'education.id_users', '=', 'users.id')
      ->leftJoin('education_level', 'education_level.id', '=', 'education.id_education_level')
      ->leftjoin('company', 'company.id', '=', 'users.id_company')
      ->leftjoin('user_profile_uploads', 'user_profile_uploads.id_users', '=', 'users.id')
      ->leftjoin('state AS company_state', 'company_state.id', '=', 'company.id_state')
      ->leftjoin('country AS company_country', 'company_country.id', '=', 'company.id_country')
      ->leftjoin('city AS company_city', 'company_city.id', '=', 'company.id_city')
      ->leftjoin('salutation', 'salutation.id', '=', 'users.id_salutation')
      ->leftjoin('user_role', 'users.user_type', '=', 'user_role.code')
      ->leftjoin('identification_type', 'identification_type.id_identity', '=', 'users.id_identity_type')
      ->select(
        'users.id_salutation',
        'salutation.salutation',
        'identification_type.identity_type',
        'user_role.role as userType',
        'users.fullname',
        'users.ic_no',
        'users.certificate_no',
        'users.certificate_category',
        'users.id_gender',
        'gender.name as gender',
        'users.phone_no',
        'users.address_1',
        'users.address_2',
        'users.postcode',
        'users.email',
        'users.position',
        'users.id_nationality as id_nationality',
        'nationality.nationality AS nationality',
        'users.user_type',
        'state.name AS statename',
        'state.id',
        'country.name AS countryname',
        'city.cityname',
        'country.id',
        'education.id AS id_edu',
        'education.qualification',
        'education.id_education_level',
        'education_level.education_level',
        'company.id as id_company',
        'company.name as companyname',
        'company.fax_no',
        'company.phone_no as companyphone',
        'company.postcode as companypostcode',
        'company_city.cityname as companycity',
        'company_state.name as companystate',
        'company_country.name as companycountry',
        'company.address_1 as company_address_1',
        'company.address_2 as company_address_2',
        'user_profile_uploads.profilepic_format',
        'user_profile_uploads.profilepic_path',
        'user_profile_uploads.ICpic_name',
        'user_profile_uploads.ICpic_format',
        'user_profile_uploads.ICpic_path',
        'user_profile_uploads.passportpic_name',
        'user_profile_uploads.passportpic_format',
        'user_profile_uploads.passportpic_path',
        'user_profile_uploads.scandoc_format',
        'user_profile_uploads.scandoc_path',
      )->first();
    // dd($userInfo);

    //Supporting documents uploaded
    $viewUploadedFileEdu = DB::table('upload_documents')
      ->select('id AS file_id', 'file_name')
      ->where('id_users', '=', $userid)
      ->where('id_section', '=', $userInfo->id_edu)
      ->where('docType', '=', 'edu')
      ->get()
      ->toArray();

    //MOU Information
    if ($userInfo->id_company !== null) {
      $mouInfo = DB::table('list_mou as mou')
        ->leftjoin('company', 'mou.id_company', '=', 'company.id')
        ->select(
          'mou.id AS mou_id',
          'mou.mou_name',
          DB::raw('DATE_FORMAT(mou.start_date, "%d/%m/%Y") as mou_start_date'),
          DB::raw('DATE_FORMAT(mou.end_date, "%d/%m/%Y") as mou_end_date'),
          'mou.remarks as mou_remarks'
        )
        ->where('company.id', $userInfo->id_company)
        ->get();

      $mouIds = $mouInfo->pluck('mou_id')->toArray();

      //Attachments MOU
      $viewUploadedFileMou = DB::table('upload_documents')
        ->join('list_mou', 'upload_documents.id_section', '=', 'list_mou.id')
        ->whereIn('list_mou.id', $mouIds)
        ->where('upload_documents.id_users', '=', $userid)
        ->where('upload_documents.docType', '=', 'mou')
        ->get(['upload_documents.id AS file_id', 'upload_documents.file_name', 'upload_documents.file_path', 'list_mou.id']);

      // dd($viewUploadedFileMou);
    }

    //LOA Information
    if ($userInfo->id_company !== null) {
      $loaInfo = DB::table('list_loa as loa')
        ->leftjoin('company', 'loa.id_company', '=', 'company.id')
        ->select(
          'loa.id AS loa_id',
          'loa.loa_name',
          'loa.course as loa_course',
          'loa.remarks as loa_remarks',
          DB::raw('DATE_FORMAT(loa.start_date, "%d/%m/%Y") as loa_start_date'),
          DB::raw('DATE_FORMAT(loa.end_date, "%d/%m/%Y") as loa_end_date'),
          'loa.audit_facilities'
        )
        ->where('company.id', $userInfo->id_company)
        ->get();

      $loaIds = $loaInfo->pluck('loa_id')->toArray();

      //Attachments LOA
      $viewUploadedFileLoa = DB::table('upload_documents')
        ->join('list_loa', 'upload_documents.id_section', '=', 'list_loa.id')
        ->whereIn('list_loa.id', $loaIds)
        ->where('upload_documents.id_users', '=', $userid)
        ->where('upload_documents.docType', '=', 'loa')
        ->get(['upload_documents.id AS file_id', 'upload_documents.file_name', 'list_loa.id', 'upload_documents.file_path']);

      // dd($viewUploadedFileLoa);
    }

    $trainerInfo = null;
    if ($userInfo->id_company !== null) {
      $trainerInfo = DB::table('trainer as trainer')
        ->leftjoin('company', 'trainer.id_company', '=', 'company.id')
        ->leftjoin('users', 'trainer.id_users', '=', 'users.id')
        ->leftjoin('user_role', 'users.id_role', '=', 'user_role.id')
        ->select(
          'trainer.id_users as trainer_id',
          'users.fullname as trainer_name',
          'users.email as trainer_email',
          'users.position as trainer_position',
          'user_role.role as trainer_role',
          'users.status as trainer_status',
        )
        ->where('company.id', $userInfo->id_company)
        ->where('trainer.status', '=', '1')
        ->get();
    }


    if ($userInfo->user_type == 1 || $userInfo->user_type == 2) {
      return view('userProfile.userProfileAdmin', [
        'userInfo' => $userInfo,
        'viewUploadedFileEdu' => $viewUploadedFileEdu,
      ]);
    } elseif ($userInfo->user_type == 5) {
      return view('userProfile.userProfile', [
        'userInfo' => $userInfo,
        'viewUploadedFileEdu' => $viewUploadedFileEdu,
      ]);
    } elseif ($userInfo->user_type == 3) {
      return view('userProfile.userProfileTP', [
        'userInfo' => $userInfo,
        'viewUploadedFileEdu' => $viewUploadedFileEdu,
        'viewUploadedFileMou' => $viewUploadedFileMou,
        'viewUploadedFileLoa' => $viewUploadedFileLoa,
        'mouInfo' => $mouInfo,
        'loaInfo' => $loaInfo,
        'trainerInfo' => $trainerInfo,
      ]);
    } elseif ($userInfo->user_type == 4) {
      return view('userProfile.userProfileTrainer', [
        'userInfo' => $userInfo,
        'viewUploadedFileEdu' => $viewUploadedFileEdu,
      ]);
    }
  }

  public function editUserProfile()
  {
    $userid = auth()->user()->id;
    $genders = DB::table('gender')->get();
    $bumistatus = DB::table('bumiputra')->get();
    $cities = DB::table('city')->get();
    $states = DB::table('state')->orderBy('state.name', 'asc')->get();
    $countries = DB::table('country')->orderBy('country.name', 'asc')->get();
    $trainers = DB::table('users')->where('user_type', '=', '4')->get();
    $salutation = DB::table('salutation')->orderBy('salutation.salutation', 'asc')->get();
    $education_level = DB::table('education_level')->orderBy('education_level_code', 'asc')->get();
    $nationality = DB::table('nationality')->orderBy('nationality', 'asc')->get();
    $identity = DB::table('identification_type')->orderBy('id_identity', 'asc')->get();

    $userInfo = DB::table('users')
      ->where('users.id', '=', $userid)
      ->leftJoin('gender', 'gender.id', '=', 'users.id_gender')
      ->leftJoin('nationality', 'nationality.id_nationality', '=', 'users.id_nationality')
      ->leftjoin('state', 'state.id', '=', 'users.id_state')
      ->leftjoin('country', 'country.id', '=', 'users.id_country')
      ->leftjoin('city', 'city.id', '=', 'users.id_city')
      ->leftjoin('education', 'education.id_users', '=', 'users.id')
      ->leftJoin('education_level', 'education_level.id', '=', 'education.id_education_level')
      ->leftjoin('company', 'company.id', '=', 'users.id_company')
      ->leftjoin('user_profile_uploads', 'user_profile_uploads.id_users', '=', 'users.id')
      ->leftjoin('state AS company_state', 'company_state.id', '=', 'company.id_state')
      ->leftjoin('country AS company_country', 'company_country.id', '=', 'company.id_country')
      ->leftjoin('city AS company_city', 'company_city.id', '=', 'company.id_city')
      ->leftjoin('salutation', 'salutation.id', '=', 'users.id_salutation')
      ->leftjoin('user_role', 'users.user_type', '=', 'user_role.code')
      ->leftjoin('identification_type', 'identification_type.id_identity', '=', 'users.id_identity_type')
      ->select(
        
        'users.id_salutation',
        'salutation.salutation',
        'identification_type.identity_type',
        'user_role.role as userType',
        'users.fullname',
        'users.ic_no',
        'users.certificate_no',
        'users.id_gender',
        'gender.name as gender',
        'users.certificate_no',
        'users.certificate_category',
        'users.phone_no',
        'users.address_1',
        'users.address_2',
        'users.postcode',
        'users.email',
        'users.position',
        'users.id_nationality as id_nationality',
        'nationality.nationality AS nationality',
        'users.user_type',
        'state.name AS statename',
        'state.id',
        'country.name AS countryname',
        'city.cityname',
        'country.id',
        'education.id AS id_edu',
        'education.qualification',
        'education.id_education_level',
        'education_level.education_level',
        'company.id as id_company',
        'company.name as companyname',
        'company.fax_no',
        'company.phone_no as companyphone',
        'company.postcode as companypostcode',
        'company_city.cityname as companycity',
        'company_state.name as companystate',
        'company_country.name as companycountry',
        'company.address_1 as company_address_1',
        'company.address_2 as company_address_2',
        'user_profile_uploads.profilepic_format',
        'user_profile_uploads.profilepic_path',
        'user_profile_uploads.ICpic_name',
        'user_profile_uploads.ICpic_format',
        'user_profile_uploads.ICpic_path',
        'user_profile_uploads.passportpic_name',
        'user_profile_uploads.passportpic_format',
        'user_profile_uploads.passportpic_path',
        'user_profile_uploads.scandoc_format',
        'user_profile_uploads.scandoc_path',
      )->first();

    // dd($userInfo);
    //Attachments Educational
    $viewUploadedFileEdu = DB::table('upload_documents')
      ->select('id AS file_id', 'file_name')
      ->where('id_users', '=', $userid)
      ->where('id_section', '=', $userInfo->id_edu)
      ->where('docType', '=', 'edu')
      ->get()
      ->toArray();

    if ($userInfo->id_company !== null) {
      $mouInfo = DB::table('list_mou as mou')
        ->leftjoin('company', 'mou.id_company', '=', 'company.id')
        ->select(
          'mou.id AS mou_id',
          'mou.mou_name',
          'mou.start_date AS mou_start_date',
          'mou.end_date AS mou_end_date',
          'mou.remarks as mou_remarks'
        )
        ->where('company.id', $userInfo->id_company)
        ->get();

      $mouIds = $mouInfo->pluck('mou_id')->toArray();

      //Attachments MOU
      $viewUploadedFileMou = DB::table('upload_documents')
        ->join('list_mou', 'upload_documents.id_section', '=', 'list_mou.id')
        ->whereIn('list_mou.id', $mouIds)
        ->where('upload_documents.id_users', '=', $userid)
        ->where('upload_documents.docType', '=', 'mou')
        ->get(['upload_documents.id AS file_id', 'upload_documents.file_name', 'upload_documents.file_path', 'list_mou.id']);
    }

    //LOA Information
    if ($userInfo->id_company !== null) {
      $loaInfo = DB::table('list_loa as loa')
        ->leftjoin('company', 'loa.id_company', '=', 'company.id')
        ->select(
          'loa.id AS loa_id',
          'loa.loa_name',
          'loa.course as loa_course',
          'loa.remarks as loa_remarks',
          'loa.start_date as loa_start_date',
          'loa.end_date as loa_end_date',
          'loa.audit_facilities'
        )
        ->where('company.id', $userInfo->id_company)
        ->get();

      $loaIds = $loaInfo->pluck('loa_id')->toArray();

      //Attachments LOA
      $viewUploadedFileLoa = DB::table('upload_documents')
        ->join('list_loa', 'upload_documents.id_section', '=', 'list_loa.id')
        ->whereIn('list_loa.id', $loaIds)
        ->where('upload_documents.id_users', '=', $userid)
        ->where('upload_documents.docType', '=', 'loa')
        ->get(['upload_documents.id AS file_id', 'upload_documents.file_name', 'list_loa.id', 'upload_documents.file_path']);
    }

    $trainerInfo = null;
    if ($userInfo->id_company !== null) {
      $trainerInfo = DB::table('trainer as trainer')
        ->leftjoin('company', 'trainer.id_company', '=', 'company.id')
        ->leftjoin('users', 'trainer.id_users', '=', 'users.id')
        ->leftjoin('user_role', 'users.id_role', '=', 'user_role.id')
        ->select(
          'trainer.id_users as trainer_id',
          'users.fullname as trainer_name',
          'users.email as trainer_email',
          'users.position as trainer_position',
          'user_role.role as trainer_role',
          'users.status as trainer_status',
        )
        ->where('company.id', $userInfo->id_company)
        ->where('trainer.status', '=', '1')
        ->get();
    }

    if ($userInfo->user_type == 1 || $userInfo->user_type == 2) {
      return view('userProfile.updateUserProfileAdmin', [
        'userInfo' => $userInfo,
        'viewUploadedFileEdu' => $viewUploadedFileEdu,
        'genders' => $genders,
        'bumistatus' => $bumistatus,
        'countries' => $countries,
        'states' => $states,
        'cities' => $cities,
        'salutation' => $salutation,
        'nationality'=>$nationality,
        'identity' => $identity

      ]);
    } elseif ($userInfo->user_type == 5) {
      return view('userProfile.updateUserProfile', [
        'userInfo' => $userInfo,
        'viewUploadedFileEdu' => $viewUploadedFileEdu,
        'genders' => $genders,
        'bumistatus' => $bumistatus,
        'countries' => $countries,
        'states' => $states,
        'cities' => $cities,
        'salutation' => $salutation,
        'education_level' => $education_level,
        'nationality'=>$nationality,
        'identity' => $identity

      ]);
    } elseif ($userInfo->user_type == 3) {
      return view('userProfile.updateUserProfileTP', [
        'userInfo' => $userInfo,
        'viewUploadedFileEdu' => $viewUploadedFileEdu,
        'viewUploadedFileMou' => $viewUploadedFileMou,
        'viewUploadedFileLoa' => $viewUploadedFileLoa,
        'mouInfo' => $mouInfo,
        'loaInfo' => $loaInfo,
        'trainerInfo' => $trainerInfo,
        'genders' => $genders,
        'bumistatus' => $bumistatus,
        'countries' => $countries,
        'states' => $states,
        'cities' => $cities,
        'trainers' => $trainers,
        'salutation' => $salutation,
        'nationality'=>$nationality,
        'identity' => $identity


      ]);
    } elseif ($userInfo->user_type == 4) {
      return view('userProfile.updateUserProfileTrainer', [
        'userInfo' => $userInfo,
        'viewUploadedFileEdu' => $viewUploadedFileEdu,
        'genders' => $genders,
        'bumistatus' => $bumistatus,
        'countries' => $countries,
        'states' => $states,
        'cities' => $cities,
        'salutation' => $salutation,
        'education_level' => $education_level,
        'nationality'=>$nationality,
        'identity' => $identity


      ]);
    }
  }

  public function deleteAttachment(Request $request)
  {
    $userid = auth()->user()->id;
    $fileId = $request->input('fileId');

    $checkFile = DB::table('upload_documents')
      ->where('id', $fileId)
      ->get();

    if (!$checkFile) {
      return response()->json(['success' => false, 'message' => 'MOU not found.']);
    }

    DB::beginTransaction();
    try {

      //Delete Attachment
      DB::table('upload_documents')
        ->where('id', $fileId)
        ->delete();

      DB::commit();

      return response()->json(['success' => true, 'message' => 'Attachment deleted successfully.']);
    } catch (Exception $ex) {
      // dd($ex->getMessage());
      DB::rollback();

      return response()->json(['success' => false, 'message' => 'Failed to delete.']);
    }
  }

  public function removeMou(Request $request)
  {
    $userid = auth()->user()->id;
    $mouId = $request->input('mouId');

    $checkMou = DB::table('list_mou')
      ->where('id', $mouId)
      ->get();

    if (!$checkMou) {
      return response()->json(['success' => false, 'message' => 'MOU not found.']);
    }

    DB::beginTransaction();
    try {

      //Remove MOU
      DB::table('list_mou')
        ->where('id', $mouId)
        ->delete();

      DB::commit();

      return response()->json(['success' => true, 'message' => 'MOU deleted successfully.']);
    } catch (Exception $ex) {
      // dd($ex->getMessage());
      DB::rollback();

      return response()->json(['success' => false, 'message' => 'Failed to delete.']);
    }
  }

  public function removeLoa(Request $request)
  {
    $userid = auth()->user()->id;
    $loaId = $request->input('loaId');

    $checkMou = DB::table('list_loa')
      ->where('id', $loaId)
      ->get();

    if (!$checkMou) {
      return response()->json(['success' => false, 'message' => 'MOU not found.']);
    }

    DB::beginTransaction();
    try {

      //Remove MOU
      DB::table('list_loa')
        ->where('id', $loaId)
        ->delete();

      DB::commit();

      return response()->json(['success' => true, 'message' => 'MOU deleted successfully.']);
    } catch (Exception $ex) {
      // dd($ex->getMessage());
      DB::rollback();

      return response()->json(['success' => false, 'message' => 'Failed to delete.']);
    }
  }

  public function updateUserProfile(Request $request)
  {

    $userid = auth()->user()->id;
    $user = Users::find($userid);
    $id_company = DB::table('users')->where('id', $userid)->value('id_company');

    // Check if the user exists
    if (!$user) {
      return redirect()->route('user-profile')
        ->with('error', 'User not found.');
    }

    DB::beginTransaction();
    try {

      //Update user information
      DB::table('users')
        ->where('id', '=', $userid)
        ->update([
          'users.id_salutation' => $request->salutation,
          'users.id_identity_type' => $request->identity,
          'users.fullname' => $request->fullname,
          'users.ic_no' => $request->ic_no,
          'users.id_gender' => $request->gender,
          'users.phone_no' => $request->phone_no,
          'users.address_1' => $request->address_1,
          'users.address_2' => $request->address_2,
          'users.id_state' => $request->state,
          'users.postcode' => $request->postcode,
          'users.id_city' => $request->city,
          'users.id_country' => $request->country,
          'users.id_nationality' => $request->nationality,
          'users.position' => $request->position,
          'users.updated_by' => $userid,
          'users.updated_at' => now(),
          'users.status' => 1
        ]);

      //Update educational information
      if ($request->has('education_level')) {

        $eduID = null;

        $id_edu = DB::table('education')->where('id_users', $userid)->value('id');

        if (!$id_edu) {
          $eduID = DB::table('education')
            ->insertGetId([
              'qualification' => $request->qualification,
              'id_education_level' => $request->education_level,
              'status' => 1,
              'id_users' => $userid,
              'created_at' => Carbon::now(),
            ]);

          //Upload supporting documents - educational information
          if ($request->hasFile('edu_file')) {

            $files = $request->file('edu_file');

            foreach ($files as $file) {
              $extension = $file->getClientOriginalExtension();
              $filename = $file->getClientOriginalName();
              $path = $file->getRealPath();
              $data = file_get_contents($path);
              $size = $file->getSize();

              DB::table('upload_documents')
                ->insert([
                  'file_name' => $filename,
                  'file_path' => base64_encode($data),
                  'file_size' => $size,
                  'file_format' => $extension,
                  'docType' => 'edu',
                  'id_users' => $userid,
                  'id_Section' => $eduID,
                  'created_by' => $userid,
                  'created_at' => now(),
                  'status' => 1,
                ]);
            }
          }
        } else {
          DB::table('education')
            ->where('id', $id_edu)
            ->update([
              'qualification' => $request->qualification,
              'id_education_level' => $request->education_level,
            ]);

          //Upload supporting documents - educational information
          if ($request->hasFile('edu_file')) {

            $files = $request->file('edu_file');

            foreach ($files as $file) {
              $extension = $file->getClientOriginalExtension();
              $filename = $file->getClientOriginalName();
              $path = $file->getRealPath();
              $data = file_get_contents($path);
              $size = $file->getSize();

              DB::table('upload_documents')
                ->insert([
                  'file_name' => $filename,
                  'file_path' => base64_encode($data),
                  'file_size' => $size,
                  'file_format' => $extension,
                  'docType' => 'edu',
                  'id_users' => $userid,
                  'id_Section' => $id_edu,
                  'created_by' => $userid,
                  'created_at' => now(),
                  'status' => 1,
                ]);
            }
          }
        }
      }

      //Update company information
      if ($request->has('companyname')) {
        if (!$id_company) {
          $companyID = DB::table('company')
            ->insertGetId([
              'name' => $request->companyname,
              'phone_no' => $request->companyphone,
              'fax_no' => $request->fax_no,
              'address_1' => $request->company_address_1,
              'address_2' => $request->company_address_2,
              'id_city' => $request->companycity,
              'id_state' => $request->companystate,
              'postcode' => $request->companypostcode,
              'id_country' => $request->companycountry,
              'created_by' => $userid,
              'created_at' => Carbon::now(),
            ]);

          DB::table('users')
            ->where('id', '=', $userid)
            ->update([
              'users.id_company' => $companyID
            ]);
        } else {
          DB::table('company')
            ->leftJoin('users', 'company.id', '=', 'users.id_company')
            ->where('users.id', '=', $userid)
            ->update([
              'company.name' => $request->companyname,
              'company.phone_no' => $request->companyphone,
              'company.fax_no' => $request->fax_no,
              'company.address_1' => $request->company_address_1,
              'company.address_2' => $request->company_address_2,
              'company.id_city' => $request->companycity,
              'company.id_state' => $request->companystate,
              'company.postcode' => $request->companypostcode,
              'company.id_country' => $request->companycountry,
              'company.updated_by' => $userid,
              'company.updated_at' => Carbon::now(),
            ]);
        }
      }

      //Upload scanned IC/Passport document
      if ($request->hasFile('FailGambarIC')) {
        $files = $request->file('FailGambarIC');
        $extension = $files->getClientOriginalExtension();
        $filename = $files->getClientOriginalName();
        $filePath = $files->storeAs('identificationFile', $filename, 'public');
        $path = $files->getRealPath();
        $data = file_get_contents($path);
        $SaizFail = $files->getSize();

        $checkIC = DB::table('user_profile_uploads')
          ->where('id_users', $userid)
          ->first();

        if ($checkIC) {
          // Update existing record
          DB::table('user_profile_uploads')
            ->where('user_profile_uploads.id_users', '=', $userid)
            ->update([
              'ICpic_name' => $filename,
              'ICpic_path' => $filePath,
              'ICpic_size' => $SaizFail,
              'ICpic_format' => $extension,
              'updated_by' => $userid,
              'updated_at' => Carbon::now(),
              'id_users' => $userid
            ]);
        } else {
          // Insert new record
          DB::table('user_profile_uploads')
            ->insert([
              'ICpic_name' => $filename,
              'ICpic_path' => $filePath,
              'ICpic_size' => $SaizFail,
              'ICpic_format' => $extension,
              'created_by' => $userid,
              'created_at' => Carbon::now(),
              'id_users' => $userid
            ]);
        }
      }

      // Upload passport photo
      if ($request->hasFile('FailGambarPassport')) {
        $files = $request->file('FailGambarPassport');
        $extension = $files->getClientOriginalExtension();
        $filename = $files->getClientOriginalName();
        $filePath = $files->storeAs('passportPhoto', $filename, 'public');
        $path = $files->getRealPath();
        $data = file_get_contents($path);
        $SaizFail = $files->getSize();

        $checkPhoto = DB::table('user_profile_uploads')
          ->where('id_users', $userid)
          ->first();

        if ($checkPhoto) {
          // Update existing record
          DB::table('user_profile_uploads')
            ->where('user_profile_uploads.id_users', '=', $userid)
            ->update([
              'passportpic_name' => $filename,
              'passportpic_path' => $filePath,
              'passportpic_size' => $SaizFail,
              'passportpic_format' => $extension,
              'updated_by' => $userid,
              'updated_at' => Carbon::now(),
              'id_users' => $userid
            ]);
        } else {
          // Insert new record
          DB::table('user_profile_uploads')
            ->insert([
              'passportpic_name' => $filename,
              'passportpic_path' => $filePath,
              'passportpic_size' => $SaizFail,
              'passportpic_format' => $extension,
              'created_by' => $userid,
              'created_at' => Carbon::now(),
              'id_users' => $userid
            ]);
        }
      }

      //Update MOU information
      if ($request->has('mou_name')) {
        $mouId = $request->get('mou_id');
        $mouNames = $request->input('mou_name');
        $mouStartDates = $request->input('mou_start_date');
        $mouEndDates = $request->input('mou_end_date');
        $mouRemarks = $request->input('mou_remarks');
        $mouFiles = $request->file('mou_file');

        foreach ($mouNames as $key => $mouName) {
          //Check if mou name is not equal to null
          if ($mouNames[$key] !== null && $mouNames[$key] !== '') {
            //Check if mou id is not equal to null (for existing mou)
            if ($mouId !== null && $mouId !== '') {
              $name = $mouName;
              $mId = $mouId[$key];
              $startDate = $mouStartDates[$key];
              $endDate = $mouEndDates[$key];
              $remark = $mouRemarks[$key];

              $mouID = null;

              $existingMou = DB::table('list_mou')->where('id', $mId)->exists();

              if ($existingMou) {
                DB::table('list_mou')
                  ->where('id', $mId)
                  ->update([
                    'mou_name' => $name,
                    'start_date' => $startDate,
                    'end_date' => $endDate,
                    'remarks' => $remark,
                    'updated_by' => $userid,
                    'updated_at' => Carbon::now(),
                  ]);

                if (isset($mouFiles[$key]) && $mouFiles[$key]->isValid()) {
                  $files = $mouFiles[$key];
                  $extension = $files->getClientOriginalExtension();
                  $filename = $files->getClientOriginalName();
                  $filePath = $files->storeAs('mouLoaFiles', $filename, 'public');
                  $path = $files->getRealPath();
                  $data = file_get_contents($path);
                  $size = $files->getSize();

                  DB::table('upload_documents')
                    ->insert([
                      'file_name' => $filename,
                      'file_path' => $filePath,
                      'file_size' => $size,
                      'file_format' => $extension,
                      'docType' => 'mou',
                      'id_users' => $userid,
                      'id_section' => $mId,
                      'created_by' => $userid,
                      'created_at' => Carbon::now(),
                      'status' => 1,
                    ]);
                }
              } else {
                $mouID = DB::table('list_mou')
                  ->insertGetId([
                    'mou_name' => $name,
                    'start_date' => $startDate,
                    'end_date' => $endDate,
                    'remarks' => $remark,
                    'status' => 1,
                    'id_company' => $id_company,
                    'created_by' => $userid,
                    'created_at' => Carbon::now(),
                  ]);

                if (isset($mouFiles[$key]) && $mouFiles[$key]->isValid()) {
                  $files = $mouFiles[$key];
                  $extension = $files->getClientOriginalExtension();
                  $filename = $files->getClientOriginalName();
                  $filePath = $files->storeAs('mouLoaFiles', $filename, 'public');
                  $path = $files->getRealPath();
                  $data = file_get_contents($path);
                  $size = $files->getSize();

                  DB::table('upload_documents')
                    ->insert([
                      'file_name' => $filename,
                      'file_path' => $filePath,
                      'file_size' => $size,
                      'file_format' => $extension,
                      'docType' => 'mou',
                      'id_users' => $userid,
                      'id_section' => $mouID,
                      'created_by' => $userid,
                      'created_at' => Carbon::now(),
                      'status' => 1,
                    ]);
                }
              }
            } else {
              //If mou id is equal to null, then create new mou
              $name = $mouName;
              $startDate = $mouStartDates[$key];
              $endDate = $mouEndDates[$key];
              $remark = $mouRemarks[$key];

              $mouID = null;

              $mouID = DB::table('list_mou')
                ->insertGetId([
                  'mou_name' => $name,
                  'start_date' => $startDate,
                  'end_date' => $endDate,
                  'remarks' => $remark,
                  'status' => 1,
                  'id_company' => $id_company,
                  'created_by' => $userid,
                  'created_at' => Carbon::now(),
                ]);

              if (isset($mouFiles[$key]) && $mouFiles[$key]->isValid()) {
                $files = $mouFiles[$key];
                $extension = $files->getClientOriginalExtension();
                $filename = $files->getClientOriginalName();
                $filePath = $files->storeAs('mouLoaFiles', $filename, 'public');
                $path = $files->getRealPath();
                $data = file_get_contents($path);
                $size = $files->getSize();

                DB::table('upload_documents')
                  ->insert([
                    'file_name' => $filename,
                    'file_path' => $filePath,
                    'file_size' => $size,
                    'file_format' => $extension,
                    'docType' => 'mou',
                    'id_users' => $userid,
                    'id_section' => $mouID,
                    'created_by' => $userid,
                    'created_at' => Carbon::now(),
                    'status' => 1,
                  ]);
              }
            }
          }
        }
      }

      //Update LOA information
      if ($request->has('loa_name')) {
        $loaId = $request->input('loa_id');
        $loaNames = $request->input('loa_name');
        $loaCourses = $request->input('loa_course');
        $loaStartDates = $request->input('loa_start_date');
        $loaEndDates = $request->input('loa_end_date');
        $loaRemarks = $request->input('loa_remarks');
        $auditFacilities = $request->input('loa_audit_facilities');
        $loaFiles = $request->file('loa_file');

        foreach ($loaNames as $key => $loaName) {
          //Check if loa name is not equal to null
          if ($loaNames[$key] !== null && $loaNames[$key] !== '') {
            //Check if load id is not equal to null
            if ($loaId !== null && $loaId !== '') {
              $name = $loaName;
              $lId = $loaId[$key];
              $course = $loaCourses[$key];
              $startDate = $loaStartDates[$key];
              $endDate = $loaEndDates[$key];
              $remark = $loaRemarks[$key];
              $auditFacility = isset($auditFacilities[$key]) ? 1 : 0;

              $loaID = null;

              $existingLoa = DB::table('list_loa')->where('id', $lId)->exists();

              if ($existingLoa) {
                DB::table('list_loa')
                  ->where('id', $lId)
                  ->update([
                    'loa_name' => $name,
                    'course' => $course,
                    'start_date' => $startDate,
                    'end_date' => $endDate,
                    'remarks' => $remark,
                    'audit_facilities' => $auditFacility,
                    'updated_by' => $userid,
                    'updated_at' => Carbon::now(),
                  ]);

                if (isset($loaFiles[$key]) && $loaFiles[$key]->isValid()) {
                  $file = $loaFiles[$key];
                  $extension = $file->getClientOriginalExtension();
                  $filename = $file->getClientOriginalName();
                  $filePath = $file->storeAs('mouLoaFiles', $filename, 'public');
                  $path = $file->getRealPath();
                  $data = file_get_contents($path);
                  $size = $file->getSize();

                  DB::table('upload_documents')
                    ->insert([
                      'file_name' => $filename,
                      'file_path' => $filePath,
                      'file_size' => $size,
                      'file_format' => $extension,
                      'docType' => 'loa',
                      'id_users' => $userid,
                      'id_section' => $lId,
                      'created_by' => $userid,
                      'created_at' => Carbon::now(),
                      'status' => 1,
                    ]);
                }
              } else {
                $name = $loaName;
                $course = $loaCourses[$key];
                $startDate = $loaStartDates[$key];
                $endDate = $loaEndDates[$key];
                $remark = $loaRemarks[$key];
                $auditFacility = isset($auditFacilities[$key]) ? 1 : 0;

                $loaID = null;

                $loaID = DB::table('list_loa')
                  ->insertGetId([
                    'loa_name' => $name,
                    'course' => $course,
                    'start_date' => $startDate,
                    'end_date' => $endDate,
                    'remarks' => $remark,
                    'audit_facilities' => $auditFacility,
                    'status' => 1,
                    'id_company' => $id_company,
                    'created_by' => $userid,
                    'created_at' => Carbon::now(),
                  ]);

                if (isset($loaFiles[$key]) && $loaFiles[$key]->isValid()) {
                  $file = $loaFiles[$key];
                  $extension = $file->getClientOriginalExtension();
                  $filename = $file->getClientOriginalName();
                  $filePath = $file->storeAs('mouLoaFiles', $filename, 'public');
                  $path = $file->getRealPath();
                  $data = file_get_contents($path);
                  $size = $file->getSize();

                  DB::table('upload_documents')
                    ->insert([
                      'file_name' => $filename,
                      'file_path' => $filePath,
                      'file_size' => $size,
                      'file_format' => $extension,
                      'docType' => 'loa',
                      'id_users' => $userid,
                      'id_section' => $loaID,
                      'created_by' => $userid,
                      'created_at' => Carbon::now(),
                      'status' => 1,
                    ]);
                }
              }
            } else {
              //If loa id is equal to null, then create new loa
              $name = $loaName;
              $course = $loaCourses[$key];
              $startDate = $loaStartDates[$key];
              $endDate = $loaEndDates[$key];
              $remark = $loaRemarks[$key];
              $auditFacility = isset($auditFacilities[$key]) ? 1 : 0;

              $loaID = null;

              $loaID = DB::table('list_loa')
                ->insertGetId([
                  'loa_name' => $name,
                  'course' => $course,
                  'start_date' => $startDate,
                  'end_date' => $endDate,
                  'remarks' => $remark,
                  'audit_facilities' => $auditFacility,
                  'status' => 1,
                  'id_company' => $id_company,
                  'created_by' => $userid,
                  'created_at' => Carbon::now(),
                ]);

              if (isset($loaFiles[$key]) && $loaFiles[$key]->isValid()) {
                $file = $loaFiles[$key];
                $extension = $file->getClientOriginalExtension();
                $filename = $file->getClientOriginalName();
                $filePath = $file->storeAs('mouLoaFiles', $filename, 'public');
                $path = $file->getRealPath();
                $data = file_get_contents($path);
                $size = $file->getSize();

                DB::table('upload_documents')
                  ->insert([
                    'file_name' => $filename,
                    'file_path' => $filePath,
                    'file_size' => $size,
                    'file_format' => $extension,
                    'docType' => 'loa',
                    'id_users' => $userid,
                    'id_section' => $loaID,
                    'created_by' => $userid,
                    'created_at' => Carbon::now(),
                    'status' => 1,
                  ]);
              }
            }
          }
        }
      }

      DB::commit();
      return redirect()->route('user-profile')
        ->with('success', 'User details have been updated successfully.');
    } catch (Exception $ex) {
      // dd($ex->getMessage());
      DB::rollback();

      return redirect()->route('user-profile')->with('error', 'Failed to update');
    }
  }

  public function trainerAttachment()
  {
    $userid = auth()->user()->id;
    $user_type = auth()->user()->user_type;
    $company_id = auth()->user()->id_company;
    return view('userProfile.trainerAttachment', compact('userid', 'user_type', 'company_id'));
  }
}
