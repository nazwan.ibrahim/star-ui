<?php

namespace App\Http\Controllers\Module\UserProfile;

use App\Http\Controllers\Controller;
use App\Models\PasswordCounter;
use Illuminate\Http\Request;
use App\Models\Users;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class changePasswordController extends Controller
{
    
       
        public function changePassword() {

                $userid = auth()->user()->id;
                return view('userProfile.changePassword', compact('userid'));
      
              }

        public function updateChangePassword(Request $request)
        {
                $userid = auth()->user()->id;

                $rules = [
                    'current_password' => 'required',
                    'new_password' => [
                        'required',
                    ],
                    'confirm_password' => 'required|same:new_password',
                ];

                // Validation messages
                $messages = [
                    'confirm_password.same' => 'The new password and confirmation password must match.',
                ];

                // Validate the request data
                $validator = Validator::make($request->all(), $rules, $messages);

                // Check if validation fails
                if ($validator->fails()) {
                        return redirect()->route('change-password')->withErrors($validator)->withInput();
                }

                try {
                    $user = Users::find($userid);
                
                    if (!$user) {
                        return redirect()->route('change-password')->with('error', 'User not found.');
                    }
            
                    // *Check if the current password matches the one in the database
                    if (Hash::check($request->new_password, $user->password)) {
                        return redirect()->back()->with('error', 'New password cannot be the same as previous password');
                    }

                    $previousPasswords = PasswordCounter::where('email', $user->email)->get();

                    foreach ($previousPasswords as $previousPassword) {
                        if (Hash::check($request->new_password, $previousPassword->password)) {
                            return redirect()->back()->with('error', 'New password cannot be the same as a previous password');
                        }
                    }

                    PasswordCounter::insert([
                        'id_users' => $userid,
                        'email' => $user->email, 
                        'password' => bcrypt($request->new_password),
                        'confirm_password' => bcrypt($request->confirm_password),
                        'created_at' => now(),
                      ]);

                    // Update the password
                    $user->update([
                        'password' => bcrypt($request->new_password),
                        'password_confirmation' => bcrypt($request->confirm_password),
                        'updated_at' => now(),
                        'updated_by' => $userid,

                    ]);
                    return redirect()->route('change-password')->with('success', 'Password successfully updated.');
                } catch (\Exception $e) {
                        return redirect()->route('change-password')->with('error', 'An error occurred: ' . $e->getMessage());
                }

               
        }

}