<?php

namespace App\Http\Controllers\Module\QuestionBank;

use App\Http\Controllers\Controller;
use App\Mail\QuestionBankRequested as MailQuestionBankRequest;
use App\Mail\QuestionBankShare;
use App\Models\Question_bank_file;
use App\Models\Question_bank_request;
use App\Models\QuestionBank;
use App\Models\QuestionBankRequest;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PDF;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PgSql\Lob;
use Spatie\PdfMerger\PdfMerger;

class QuestionBankController extends Controller
{

    public function questionbanklist()
    {
        $userid = auth()->user()->id;

        //Query for the list of submission question bank (waiting for approval) on admin
        $submissionQuestionList = DB::table('question_bank')
            ->leftJoin('question_bank_file', 'question_bank_file.id_question_bank', '=', 'question_bank.id')
            ->leftJoin('trainings', 'trainings.id', '=', 'question_bank.id_trainings')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('company', 'company.id', '=', 'question_bank.id_company')
            ->leftJoin('status_approval', 'status_approval.id', '=', 'question_bank.id_approval_status')
            ->select(
                'question_bank.id as question_bank_id',
                'question_bank.id_company',
                'question_bank.id_approval_status',
                'company.name',
                'training_course.course_name',
                'trainings.batch_no_full',
                'status_approval.status_name',
                'question_bank_file.*',
                'question_bank.remarks as remarks',

            )
            ->where('question_bank.status', '=', '1')
            ->where('question_bank.id_approval_status', '!=', '2')
            ->orderby('question_bank.id', 'desc')
            ->get();

        $outputData = [];

        foreach ($submissionQuestionList as $row) {

            $bankid = $row->id_question_bank;

            if (!isset($outputData[$bankid])) {
                $outputData[$bankid] = [
                    'id_question_bank' => $row->id_question_bank,
                    'id_company' => $row->id_company,
                    'status_id' => $row->id_approval_status,
                    'status_name' => $row->status_name,
                    'company_name' => $row->name,
                    'course_name' => $row->course_name,
                    'batch_no' => $row->batch_no_full,
                    'remarks' => $row->remarks,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],

                ];
            }

            $outputData[$bankid]['type'][] = $row->type;
            $outputData[$bankid]['category'][] = $row->category;
            $outputData[$bankid]['file_names'][$row->type][$row->category][] = $row->file_name;
        }

        //Query for the list of approved submission question bank on admin
        $approvedQuestionList = DB::table('question_bank')
            ->leftJoin('question_bank_file', 'question_bank_file.id_question_bank', '=', 'question_bank.id')
            ->leftJoin('trainings', 'trainings.id', '=', 'question_bank.id_trainings')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('company', 'company.id', '=', 'question_bank.id_company')
            ->leftJoin('status_approval', 'status_approval.id', '=', 'question_bank.id_approval_status')
            ->select(
                'question_bank.id as question_bank_id',
                'question_bank.id_company',
                'question_bank.id_approval_status',
                'company.name',
                'training_course.course_name',
                'trainings.batch_no_full',
                'status_approval.status_name',
                'question_bank_file.*',
                'question_bank.remarks as remarks',

            )
            ->where('question_bank.status', '=', '1')
            ->where('question_bank.id_approval_status', '=', '2')
            ->orderby('question_bank.id', 'desc')
            ->get();

        $outputData2 = [];

        foreach ($approvedQuestionList as $row) {

            $bankid = $row->id_question_bank;

            if (!isset($outputData2[$bankid])) {
                $outputData2[$bankid] = [
                    'id_question_bank' => $row->id_question_bank,
                    'id_company' => $row->id_company,
                    'status_id' => $row->id_approval_status,
                    'status_name' => $row->status_name,
                    'company_name' => $row->name,
                    'course_name' => $row->course_name,
                    'batch_no' => $row->batch_no_full,
                    'remarks' => $row->remarks,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],

                ];
            }

            $outputData2[$bankid]['type'][] = $row->type;
            $outputData2[$bankid]['category'][] = $row->category;
            $outputData2[$bankid]['file_names'][$row->type][$row->category][] = $row->file_name;
        }

        //Query for the list of shared requested question bank on admin
        $sharedQuestionList = DB::table('question_bank')
            ->leftJoin('trainings', 'trainings.id', '=', 'question_bank.id_trainings')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('question_bank_file', 'question_bank.id', '=', 'question_bank_file.id_question_bank')
            ->leftJoin('question_bank_request', 'question_bank_request.id_question_bank', '=', 'question_bank.id')
            ->leftjoin('company', 'company.id', '=', 'question_bank_request.id_training_partner')
            ->select(
                'question_bank.id',
                'question_bank_request.id_training_partner',
                'company.name',
                'training_course.course_name',
                'question_bank_file.*'
            )
            ->where('question_bank_request.id_request_status', '=', '2')
            ->get();

        $outputData3 = [];

        foreach ($sharedQuestionList as $row) {
            $bankid = $row->id_question_bank;

            if (!isset($outputData3[$bankid])) {
                $outputData3[$bankid] = [
                    'id_question_bank' => $row->id,
                    'id_training_partner' => $row->id_training_partner,
                    'company_name' => $row->name,
                    'course_name' => $row->course_name,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],

                ];
            }

            $outputData3[$bankid]['type'][] = $row->type;
            $outputData3[$bankid]['category'][] = $row->category;
            $outputData3[$bankid]['file_names'][$row->type][$row->category][] = $row->file_name;
        }

        //Query for the list of requested question bank
        $requestedQuestionList = DB::table('question_bank')
            ->leftJoin('trainings', 'trainings.id', '=', 'question_bank.id_trainings')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('question_bank_file', 'question_bank.id', '=', 'question_bank_file.id_question_bank')
            ->leftjoin('company', 'company.id', '=', 'question_bank.id_company')
            ->select(
                'question_bank.id',
                'question_bank.id_company',
                'company.name',
                'training_course.course_name',
                'trainings.batch_no_full',
                'question_bank_file.*'
            )
            ->where('question_bank.id_approval_status', '=', '2')
            ->where('question_bank.status', '=', '1')
            ->orderby('question_bank.id', 'desc')
            ->get();

        $outputData4 = [];

        foreach ($requestedQuestionList as $row) {

            $bankid = $row->id_question_bank;

            if (!isset($outputData4[$bankid])) {
                $outputData4[$bankid] = [
                    'id_question_bank' => $row->id_question_bank,
                    'id_company' => $row->id_company,
                    'company_name' => $row->name,
                    'course_name' => $row->course_name,
                    'batch_no' => $row->batch_no_full,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],

                ];
            }

            $outputData4[$bankid]['type'][] = $row->type;
            $outputData4[$bankid]['category'][] = $row->category;
            $outputData4[$bankid]['file_names'][$row->type][$row->category][] = $row->file_name;
        }

        return view('questionBank.questionBankList', compact('userid', 'outputData', 'outputData2', 'outputData3', 'outputData4'));
    }

    public function questionBankDelete($id)
    {
        $userid = auth()->user()->id;

        $existingBankQuestion = DB::table('question_bank')->where('id', '=', $id)->get();

        if (!$existingBankQuestion) {
            return redirect()->route('admin.questionbank-list')->with('error', 'Question bank not found.');
        }

        DB::beginTransaction();

        try {

            //Delete question bank
            DB::table('question_bank')
                ->where('id', $id)
                ->update([
                    'status' => 0,
                    'deleted_by' => $userid,
                    'deleted_at' => Carbon::now()
                ]);

            //Delete question bank file
            DB::table('question_bank_file')
                ->where('id_question_bank', $id)
                ->update([
                    'status' => 0,
                    'deleted_by' => $userid,
                    'deleted_at' => Carbon::now()
                ]);

            DB::commit();

            return redirect()->route('admin.questionbank-list')->with('success', 'Question bank deleted successfully.');
        } catch (Exception $ex) {

            DB::rollback();

            return redirect()->route('admin.questionbank-list')->with('error', 'Failed to delete question bank.');
        }
    }

    public function questionbanksharelist()
    {
        $userid = auth()->user()->id;

        //Query for the list of approved question banks
        $questionbanklist = DB::table('question_bank')
            ->leftJoin('trainings', 'trainings.id', '=', 'question_bank.id_trainings')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('question_bank_file', 'question_bank.id', '=', 'question_bank_file.id_question_bank')
            ->leftjoin('company', 'company.id', '=', 'question_bank.id_company')
            ->select(
                'question_bank.id',
                'question_bank.id_company',
                'company.name',
                'training_course.course_name',
                'trainings.batch_no_full',
                'question_bank_file.*'
            )
            ->where('question_bank.id_approval_status', '=', '2')
            ->where('question_bank.status', '=', '1')
            ->orderby('question_bank.id', 'desc')
            ->get();

        $outputData = [];

        foreach ($questionbanklist as $row) {

            $bankid = $row->id_question_bank;

            if (!isset($outputData[$bankid])) {
                $outputData[$bankid] = [
                    'id_question_bank' => $row->id_question_bank,
                    'id_company' => $row->id_company,
                    'company_name' => $row->name,
                    'course_name' => $row->course_name,
                    'batch_no' => $row->batch_no_full,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],

                ];
            }

            $outputData[$bankid]['type'][] = $row->type;
            $outputData[$bankid]['category'][] = $row->category;
            $outputData[$bankid]['file_names'][$row->type][$row->category][] = $row->file_name;
        }

        return view('questionBank.questionBankShareList', compact('userid', 'outputData'));
    }

    public function questionbankhistorylist()
    {
        $userid = auth()->user()->id;

        $questionbanklist = DB::table('question_bank')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'question_bank.id_training_course')
            ->leftJoin('question_bank_file', 'question_bank.id', '=', 'question_bank_file.id_question_bank')
            ->leftJoin('question_bank_request', 'question_bank_request.id_question_bank', '=', 'question_bank.id')
            ->leftjoin('company', 'company.id', '=', 'question_bank_request.id_training_partner')
            ->select(
                'question_bank.id',
                'question_bank_request.id_training_partner',
                'company.name',
                'training_course.course_name',
                'question_bank_file.*'
            )
            ->where('question_bank_request.id_request_status', '=', '2')
            ->get();
        // dd($questionbanklist);

        $outputData = [];


        foreach ($questionbanklist as $row) {
            $bankid = $row->id_question_bank;
            // dd($questionbanklist);
            if (!isset($outputData[$bankid])) {
                $outputData[$bankid] = [
                    'id_question_bank' => $row->id,
                    'id_training_partner' => $row->id_training_partner,
                    'company_name' => $row->name,
                    'course_name' => $row->course_name,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],

                ];
            }

            $outputData[$bankid]['type'][] = $row->type;
            $outputData[$bankid]['category'][] = $row->category;
            $outputData[$bankid]['file_names'][$row->type][$row->category][] = $row->file_name;
        }

        // dd($outputData);
        return view('questionBank.questionBankHistoryList', compact('userid', 'outputData'));
    }

    public function questionbankshow($id)
    {
        $userid = auth()->user()->id;

        //Query for the details of the question bank
        $questionbanklist = DB::table('question_bank')
            ->where('question_bank.id', '=', $id)
            ->leftJoin('trainings', 'trainings.id', '=', 'question_bank.id_trainings')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('question_bank_file', 'question_bank.id', '=', 'question_bank_file.id_question_bank')
            ->leftjoin('company', 'company.id', '=', 'question_bank.id_company')
            ->select(
                'question_bank.id',
                'question_bank.id_company',
                'company.name',
                'training_course.course_name',
                'question_bank_file.*'
            )
            ->where('question_bank.status', '=', '1')
            ->get();

        $outputData = [];

        foreach ($questionbanklist as $row) {

            $bankid = $row->id_question_bank;

            if (!isset($outputData[$bankid])) {
                $outputData[$bankid] = [
                    'id_question_bank' => $row->id_question_bank,
                    'id_company' => $row->id_company,
                    'company_name' => $row->name,
                    'course_name' => $row->course_name,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],

                ];
            }

            $outputData[$bankid]['type'][] = $row->type;
            $outputData[$bankid]['category'][] = $row->category;
            $outputData[$bankid]['file_names'][$row->type][$row->category][] = $row->file_name;
        }

        return view('questionBank.questionBankShow', compact('userid', 'outputData', 'bankid'));
    }

    public function questionbankverify($id)
    {
        $userid = auth()->user()->id;

        //Query for the details of the question bank
        $questionbanklist = DB::table('question_bank')
            ->where('question_bank.id', '=', $id)
            ->leftJoin('trainings', 'trainings.id', '=', 'question_bank.id_trainings')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('question_bank_file', 'question_bank.id', '=', 'question_bank_file.id_question_bank')
            ->leftjoin('company', 'company.id', '=', 'question_bank.id_company')
            ->select(
                'question_bank.id',
                'question_bank.id_company',
                'company.name',
                'training_course.course_name',
                'question_bank_file.*'
            )
            ->where('question_bank.status', '=', '1')
            ->get();

        $outputData = [];

        foreach ($questionbanklist as $row) {

            $bankid = $row->id_question_bank;

            if (!isset($outputData[$bankid])) {
                $outputData[$bankid] = [
                    'id_question_bank' => $row->id_question_bank,
                    'id_company' => $row->id_company,
                    'company_name' => $row->name,
                    'course_name' => $row->course_name,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],

                ];
            }

            $outputData[$bankid]['type'][] = $row->type;
            $outputData[$bankid]['category'][] = $row->category;
            $outputData[$bankid]['file_names'][$row->type][$row->category][] = $row->file_name;
        }

        return view('questionBank.questionBankVerify', compact('userid', 'outputData', 'bankid'));
    }

    public function questionbankverifyupdate(Request $request, $id)
    {
        $userid = auth()->user()->id;

        //Query for the details of the question bank
        $questionbanklist = DB::table('question_bank')
            ->where('question_bank.id', '=', $id)
            ->leftJoin('trainings', 'trainings.id', '=', 'question_bank.id_trainings')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('question_bank_file', 'question_bank.id', '=', 'question_bank_file.id_question_bank')
            ->leftjoin('company', 'company.id', '=', 'question_bank.id_company')
            ->select(
                'question_bank.id',
                'question_bank.id_company',
                'company.name',
                'company.email',
                'training_course.course_name',
                'training_course.id_training_course',
                'question_bank_file.*'
            )
            ->where('question_bank.status', '=', '1')
            ->get();

        $outputData = [];

        foreach ($questionbanklist as $row) {
            $filePath = public_path('storage/uploads' . DIRECTORY_SEPARATOR . $row->file_name);

            $password = $request->input('password_file');

            $bankid = $row->id_question_bank;

            if (!isset($outputData[$bankid])) {
                $outputData[$bankid] = [
                    'id' => $row->id,
                    'id_training_course' => $row->id_training_course,
                    'company_id' => $row->id_company,
                    'company_name' => $row->name,
                    'course_name' => $row->course_name,
                    'company_email' => $row->email,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],
                    'attachment' => [
                        'path' => [],
                    ],
                ];
            }

            $outputData[$bankid]['type'][] = $row->type;
            $outputData[$bankid]['category'][] = $row->category;
            $outputData[$bankid]['file_names'][$row->type][$row->category][] = $row->file_name;
            $outputData[$bankid]['attachment']['path'][] = str_replace('\\', '/', $filePath);
        }

        foreach ($outputData as $data) {

            if ($data) {

                if ($request->has('reject_status')) {
                    //Reject the submission question bank
                    DB::table('question_bank')
                        ->where('id', $bankid)
                        ->update([
                            'id_approval_status' => 3,
                            'remarks' => $request->input('remarks'),
                            'updated_at' => now(),
                            'updated_by' => $userid,
                        ]);
                } elseif ($request->has('approve_status')) {
                    //Approve the submission question bank
                    DB::table('question_bank')
                        ->where('id', $bankid)
                        ->update([
                            'id_approval_status' => 2,
                            'remarks' => $request->input('remarks'),
                            'updated_at' => now(),
                            'updated_by' => $userid,
                        ]);
                }
            }
            $filePaths = $data['attachment']['path'];

            if ($filePaths) {
                Mail::to($data['company_email'])->send(new QuestionBankShare($data));

                return redirect()->back()->with('success', 'Success send question bank');
            } else {
                return back()->with('error', 'File is not found');
            }
        }
    }

    public function questionbankshare($id)
    {
        $userid = auth()->user()->id;

        //Query for the details of the question bank
        $questionbanklist = DB::table('question_bank')
            ->leftJoin('trainings', 'trainings.id', '=', 'question_bank.id_trainings')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('question_bank_file', 'question_bank.id', '=', 'question_bank_file.id_question_bank')
            ->leftjoin('company', 'company.id', '=', 'question_bank.id_company')
            ->select(
                'question_bank.id',
                'question_bank.id_company',
                'question_bank.id_trainings',
                'company.name',
                'training_course.course_name',
                'trainings.batch_no_full',
                'question_bank_file.*'
            )
            ->where('question_bank.id_approval_status', '=', '2')
            ->where('question_bank.status', '=', '1')
            ->where('question_bank.id', '=', $id)
            ->orderby('question_bank.id', 'desc')
            ->get();

        $outputData = [];

        foreach ($questionbanklist as $row) {
            $bankid = $row->id_question_bank;

            //Query for the list of rquested training partners
            $trainingpartner = DB::table('question_bank_request')
                ->join('question_bank', 'question_bank.id_trainings', '=', 'question_bank_request.id_trainings')
                ->join('company', 'company.id', '=', 'question_bank_request.id_request_tp')
                ->select(
                    'question_bank_request.id_request_tp as company_id',
                    'company.name',
                    'question_bank.id_trainings'
                )
                ->where('question_bank_request.id_request_status', '=', 1)
                ->where('question_bank.id_trainings', '=', $row->id_trainings)
                ->groupBy('question_bank_request.id_request_tp', 'company.name', 'question_bank.id_trainings')
                ->get();

            if (!isset($outputData[$bankid])) {
                $outputData[$bankid] = [
                    'id' => $row->id,
                    'company_name' => $row->name,
                    'course_name' => $row->course_name,
                    'batch_no' => $row->batch_no_full,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],
                ];
            }

            $outputData[$bankid]['type'][] = $row->type;
            $outputData[$bankid]['category'][] = $row->category;
            $outputData[$bankid]['file_names'][$row->type][$row->category][] = $row->file_name;
        }

        return view('questionBank.questionBankShare', compact('userid', 'outputData', 'bankid', 'trainingpartner'));
    }

    public function questionbanksharesubmit(Request $request, $id)
    {
        $userid = auth()->user()->id;

        $selectedTrainingPartnerId = $request->input('training_partner');

        //Query for the details of the question bank
        $questionbanklist = DB::table('question_bank')
            ->leftJoin('trainings', 'trainings.id', '=', 'question_bank.id_trainings')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('question_bank_file', 'question_bank.id', '=', 'question_bank_file.id_question_bank')
            ->leftjoin('company', 'company.id', '=', 'question_bank.id_company')
            ->select(
                'question_bank.id',
                'question_bank.id_company as company_id',
                'company.name',
                'company.email',
                'training_course.course_name',
                'training_course.id_training_course',
                'trainings.id as training_id',
                'trainings.batch_no_full',
                'question_bank_file.*'
            )
            ->where('question_bank.id_approval_status', '=', '2')
            ->where('question_bank.status', '=', '1')
            ->where('question_bank.id', '=', $id)
            ->orderby('question_bank.id', 'desc')
            ->get();


        $outputData = [];
        foreach ($questionbanklist as $row) {
            $filePath = public_path('storage/uploads' . DIRECTORY_SEPARATOR . $row->file_name);

            $password = $request->input('password_file');

            $bankid = $row->id_question_bank;

            if (!isset($outputData[$bankid])) {
                $outputData[$bankid] = [
                    'question_bank_id' => $row->id,
                    'training_id' => $row->training_id,
                    'id_training_course' => $row->id_training_course,
                    'company_id' => $row->company_id,
                    'company_name' => $row->name,
                    'course_name' => $row->course_name,
                    'company_email' => $row->email,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],
                    'attachment' => [
                        'path' => [],
                    ],
                ];
            }

            $outputData[$bankid]['type'][] = $row->type;
            $outputData[$bankid]['category'][] = $row->category;
            $outputData[$bankid]['file_names'][$row->type][$row->category][] = $row->file_name;

            $outputData[$bankid]['attachment']['path'][] = str_replace('\\', '/', $filePath);
        }

        foreach ($outputData as $data) {

            if ($data) {
                DB::table('question_bank_request')
                    ->where('id_request_tp', $selectedTrainingPartnerId)
                    ->where('id_training_course', $data['id_training_course'])
                    ->update([
                        'id_question_bank' => $id,
                        'id_training_partner' => $data['company_id'],
                        'id_request_status' => 2,
                        'status' => 1,
                        'updated_at' => now(),
                        'updated_by' => $userid,
                    ]);
            }

            $filePaths = $data['attachment']['path'];

            if ($filePaths) {
                Mail::to($data['company_email'])->send(new QuestionBankShare($data));

                return redirect()->back()->with('success', 'Question bank was successfully submitted.');
            } else {
                return back()->with('error', 'File is not found');
            }
        }
    }

    function encryptFile($filePath, $password)
    {
        $encryptedFilePath = $filePath . '.enc';
        $command = "openssl enc -aes-256-cbc -salt -in \"$filePath\" -out \"$encryptedFilePath\" -k \"$password\"";
        shell_exec($command);

        return $encryptedFilePath;
    }

    public function questionbankrequestlist()
    {
        $userid = auth()->user()->id;

        $company_id = DB::table('users')->where('id', '=', $userid)->value('id_company');

        //Query for the list of submission question bank on TP
        $submissionlist = DB::table('question_bank')
            ->where('question_bank.id_company', '=', $company_id)
            ->leftJoin('question_bank_file', 'question_bank_file.id_question_bank', '=', 'question_bank.id')
            ->leftJoin('trainings', 'trainings.id', '=', 'question_bank.id_trainings')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('company', 'company.id', '=', 'question_bank.id_company')
            ->leftJoin('status_approval', 'status_approval.id', '=', 'question_bank.id_approval_status')
            ->select(
                'question_bank.id',
                'question_bank.remarks',
                'question_bank.id_company',
                'company.name',
                'training_course.course_name',
                'question_bank.id_approval_status as status_id',
                'status_approval.status_name',
                'trainings.batch_no_full',
                'question_bank_file.*'
            )
            ->where('question_bank.status', '=', '1')
            ->orderby('question_bank.id', 'desc')
            ->get();

        // dd($submissionlist);

        $outputData = [];

        foreach ($submissionlist as $row) {
            $question_bank = $row->id_question_bank;

            if (!isset($outputData[$question_bank])) {
                $outputData[$question_bank] = [
                    'id_question_bank' => $row->id_question_bank,
                    'id' => $row->id,
                    'batch_no' => $row->batch_no_full,
                    'version' => $row->version,
                    'status_id' => $row->status_id,
                    'status_name' => $row->status_name,
                    'course_name' => $row->course_name,
                    'remarks' => $row->remarks,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],
                ];
            }
            $outputData[$question_bank]['id'] = $row->id;
            $outputData[$question_bank]['type'][] = $row->type;
            $outputData[$question_bank]['category'][] = $row->category;
            $outputData[$question_bank]['batch_no'] = $row->batch_no_full;
            $outputData[$question_bank]['file_names'][$row->type][$row->category][] = $row->file_name;
        }

        //Query for the list of requested question bank on TP
        $requestlist = DB::table('question_bank_request')
            ->where('question_bank_request.id_request_tp', '=', $company_id)
            ->leftJoin('trainings', 'trainings.id', '=', 'question_bank_request.id_trainings')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('company', 'company.id', '=', 'question_bank_request.id_training_partner')
            ->leftJoin('status_approval', 'status_approval.id', '=', 'question_bank_request.id_request_status')
            ->select(
                'question_bank_request.*',
                'company.name as company_name',
                'training_course.course_name',
                'trainings.training_code',
                'trainings.batch_no_full',
                'status_approval.status_name'
            )
            ->get();

        $outputDataRequest = [];


        foreach ($requestlist as $row) {

            $version = $row->version;
            // dd($row);
            if (!isset($outputDataRequest[$version])) {
                $outputDataRequest[$version] = [
                    'id' => $row->id,
                    'company_name' => $row->company_name,
                    'course_name' => $row->course_name,
                    'batch_no' => $row->batch_no_full,
                    'status_name' => $row->status_name,
                    'type' => [],
                    'category' => [],

                ];
            }
            $outputDataRequest[$version]['id'] = $row->id;
            $outputDataRequest[$version]['type'] = $row->type;
            $outputDataRequest[$version]['category'] = $row->category;
            $outputDataRequest[$version]['batch_no'] = $row->batch_no_full;
            $outputDataRequest[$version][$row->type][$row->category][] = $row->version;
        }

        return view('questionBank.questionBankRequestList', compact('userid', 'outputData', 'outputDataRequest'));
    }

    public function questionbankrequestnew()
    {
        $userid = auth()->user()->id;

        //Query for dropdown ongoing training courses
        $trainingcourse = DB::table('question_bank')
            ->select(
                'question_bank.id',
                'trainings.id as id_training',
                'question_bank.id_company',
                'training_course.id_training_course',
                'training_course.course_name',
                'trainings.batch_no_full',
                'trainings.training_code'
            )
            ->join('trainings', 'trainings.id', '=', 'question_bank.id_trainings')
            ->join('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->where('question_bank.id_approval_status', '=', 2)
            ->where('trainings.id_training_status', '=', 2)
            ->where('question_bank.status', '=', 1)
            ->groupBy('question_bank.id')
            ->orderBy('training_course.course_name')
            ->get();

        return view('questionBank.questionBankRequestNew', compact('userid', 'trainingcourse'));
    }

    public function questionBankRequestAdd(Request $request)
    {
        $userid = auth()->user()->id;

        $company_id = DB::table('users')->where('id', '=', $userid)->value('id_company');

        $users = DB::table('users')
            ->where('users.user_type', '=', '2')
            ->first();

        //Get the value from the selected dropdown
        $selectedTrainingCourseId = $request->input('id_trainings');

        if ($selectedTrainingCourseId) {
            //Split the combined values using the delimiter
            $values = explode('|', $selectedTrainingCourseId);
            $idTraining = $values[0];
            $idTrainingCourse = $values[1];

            $trainingcourse = DB::table('training_course')->where('id_training_course', '=', $idTrainingCourse)->first();

            $latestVersion = DB::table('question_bank_request')
                ->where('id_training_course', $idTrainingCourse)
                ->orderBy('version', 'desc')
                ->first();

            if ($latestVersion) {
                $newVersion = $latestVersion->version + 1;
            } else {
                $newVersion = 1;
            }
        }

        DB::beginTransaction();

        try {
            //Check at least one training course was selected
            if ($selectedTrainingCourseId && $selectedTrainingCourseId !== 0) {

                //Check at least one checkbox was selected
                if (($request->has('theory_question')) || ($request->has('theory_solution')) || ($request->has('practical_question')) || ($request->has('practical_solution'))) {

                    if ($request->has('theory_question')) {
                        DB::table('question_bank_request')
                            ->insert([
                                'id_request_tp' => $company_id,
                                'id_trainings' => $idTraining,
                                'id_training_course' => $idTrainingCourse,
                                'status' => 1,
                                'id_request_status' => 1,
                                'type' => 'Theory',
                                'category' => "Question",
                                'version' => $newVersion,
                                'created_at' => Carbon::now(),
                                'created_by' => $userid
                            ]);
                    }

                    if ($request->has('theory_solution')) {
                        DB::table('question_bank_request')
                            ->insert([
                                'id_request_tp' => $company_id,
                                'id_trainings' => $idTraining,
                                'id_training_course' => $idTrainingCourse,
                                'status' => 1,
                                'id_request_status' => 1,
                                'type' => 'Theory',
                                'category' => "Solution",
                                'version' => $newVersion,
                                'created_at' => Carbon::now(),
                                'created_by' => $userid
                            ]);
                    }

                    if ($request->has('practical_question')) {
                        DB::table('question_bank_request')
                            ->insert([
                                'id_request_tp' => $company_id,
                                'id_trainings' => $idTraining,
                                'id_training_course' => $idTrainingCourse,
                                'status' => 1,
                                'type' => 'Practical',
                                'category' => "Question",
                                'id_request_status' => 1,
                                'version' => $newVersion,
                                'created_at' => Carbon::now(),
                                'created_by' => $userid
                            ]);
                    }

                    if ($request->has('practical_solution')) {
                        DB::table('question_bank_request')
                            ->insert([
                                'id_request_tp' => $company_id,
                                'id_trainings' => $idTraining,
                                'id_training_course' => $idTrainingCourse,
                                'status' => 1,
                                'type' => 'Practical',
                                'category' => "Solution",
                                'id_request_status' => 1,
                                'version' => $newVersion,
                                'created_at' => Carbon::now(),
                                'created_by' => $userid
                            ]);
                    }
                } else {
                    return redirect()->back()->with('error', 'Please select at least one checkbox.');
                }
            } else {
                return redirect()->back()->with('error', 'Please select training course.');
            }

            DB::commit();

            Mail::to($users->email)->send(new MailQuestionBankRequest($users, $trainingcourse));

            return redirect()->route('questionbankrequestlist')->with('success', 'Your request submitted successfully');
        } catch (Exception $ex) {

            DB::rollback();

            return redirect()->route('questionbankrequestlist')->with('error', 'Failed to submit');
        }
    }

    public function questionbankrequestview($id)
    {
        $userid = auth()->user()->id;

        $submissionlist = DB::table('question_bank')
            ->where('question_bank.id', '=', $id)
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'question_bank.id_training_course')
            ->leftJoin('question_bank_file', 'question_bank.id', '=', 'question_bank_file.id_question_bank')
            ->leftJoin('company', 'company.id', '=', 'question_bank.id_company')
            ->select('question_bank.id', 'company.name', 'company.email', 'training_course.course_name', 'question_bank_file.*')
            ->get();

        $outputData = [];

        foreach ($submissionlist as $row) {
            $courseName = $row->course_name;

            if (!isset($outputData[$courseName])) {
                $outputData[$courseName] = [
                    'id_question_bank' => $row->id_question_bank,
                    'id' => $row->id,
                    'course_name' => $courseName,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],
                ];
            }
            $outputData[$courseName]['id_question_bank'] = $row->id_question_bank;
            $outputData[$courseName]['type'][] = $row->type;
            $outputData[$courseName]['category'][] = $row->category;
            $outputData[$courseName]['file_names'][$row->type][$row->category][] = $row->file_name;
        }

        return view('questionBank.questionBankRequestView', compact('userid', 'outputData', 'submissionlist'));
    }

    public function questionbanksubmissionnew()
    {
        $userid = auth()->user()->id;
        // dd($userid);

        //Query for dropdown approved training courses
        $trainingcourse = DB::table('training_course')
            ->leftjoin('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
            ->leftjoin('training_partner_course', 'training_course.id_training_course', '=', 'training_partner_course.id_training_course')
            ->leftjoin('company', 'training_partner_course.id_company', '=', 'company.id')
            ->leftjoin('users', 'users.id_company', '=', 'company.id')
            ->where('users.id', '=', $userid)
            ->where('users.user_type', '=', '3')
            ->where('trainings.id_approval_status', '=', 2)
            ->groupBy('trainings.id', 'trainings.training_code', 'trainings.batch_no_full')
            ->select(
                'users.id as userid',
                'company.id as company_id',
                'trainings.id as id_training',
                'training_course.course_name',
                'training_course.id_training_course',
                'trainings.batch_no_full',
                'trainings.training_code'
            )
            ->orderBy('training_course.course_name')
            ->get();



        return view('questionBank.questionBankSubmissionNew', compact('userid', 'trainingcourse'));
    }

    public function questionbanksubmissionadd(Request $request)
    {
        $userid = auth()->user()->id;

        $company_id = DB::table('users')->where('id', '=', $userid)->value('id_company');

        //Get the value from the selected dropdown
        $selectedTrainingPartnerId = $request->input('id_trainings');
        //Split the combined values using the delimiter
        $values = explode('|', $selectedTrainingPartnerId);

        DB::beginTransaction();

        try {
            //Check at least one training course was selected
            if ($selectedTrainingPartnerId && $selectedTrainingPartnerId !== 0) {

                //Insert new submission question bank - table question_bank
                $qsbank = QuestionBank::create([
                    'id_trainings' => $values[0],
                    'id_training_course' => $values[1],
                    'id_company' => $company_id,
                    'id_approval_status' => 1,
                    'status' => 1,
                    'created_by' => $userid,
                    'created_at' => now(),
                ]);

                //Check at least one file was uploaded
                if (($request->hasFile('theoryquestion') && $request->file('theoryquestion')->isValid()) || ($request->hasFile('theorysolution') && $request->file('theorysolution')->isValid()) || ($request->hasFile('practicalquestion') && $request->file('practicalquestion')->isValid()) || ($request->hasFile('practicalsolution') && $request->file('practicalsolution')->isValid())) {

                    //Insert the uploaded file information - table question_bank_file
                    if ($request->hasFile('theoryquestion')) {
                        $uploadedFile = $request->file('theoryquestion');
                        $fileName = $uploadedFile->getClientOriginalName();
                        $extension = $uploadedFile->getClientOriginalExtension();
                        $allowedExtensions = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];
                        if (in_array(strtolower($extension), $allowedExtensions)) {
                            $filePath = $uploadedFile->storeAs('uploads', $fileName, 'public');

                            $qsbank->files()->create([
                                'file_name' => $fileName,
                                'file_path' => $filePath,
                                'file_format' => $extension,
                                'status' => 1,
                                'type' => 'Theory',
                                'category' => 'Question',
                            ]);
                        } else {
                            return redirect()->back()->with('error', 'Invalid file type. Only PDF, Word, and Excel files are allowed.');
                        }
                    }

                    //Insert the uploaded file information - table question_bank_file
                    if ($request->hasFile('theorysolution')) {
                        $uploadedFile = $request->file('theorysolution');
                        $fileName = $uploadedFile->getClientOriginalName();
                        $extension = $uploadedFile->getClientOriginalExtension();
                        $allowedExtensions = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];
                        if (in_array(strtolower($extension), $allowedExtensions)) {
                            $filePath = $uploadedFile->storeAs('uploads', $fileName, 'public');

                            $qsbank->files()->create([
                                'file_name' => $fileName,
                                'file_path' => $filePath,
                                'file_format' => $extension,
                                'status' => 1,
                                'type' => 'Theory',
                                'category' => 'Solution',
                            ]);
                        } else {
                            return redirect()->back()->with('error', 'Invalid file type. Only PDF, Word, and Excel files are allowed.');
                        }
                    }

                    //Insert the uploaded file information - table question_bank_file
                    if ($request->hasFile('practicalquestion')) {
                        $uploadedFile = $request->file('practicalquestion');
                        $fileName = $uploadedFile->getClientOriginalName();
                        $extension = $uploadedFile->getClientOriginalExtension();
                        $allowedExtensions = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];
                        if (in_array(strtolower($extension), $allowedExtensions)) {
                            $filePath = $uploadedFile->storeAs('uploads', $fileName, 'public');

                            $qsbank->files()->create([
                                'file_name' => $fileName,
                                'file_path' => $filePath,
                                'file_format' => $extension,
                                'status' => 1,
                                'type' => 'Practical',
                                'category' => 'Question',
                            ]);
                        } else {
                            return redirect()->back()->with('error', 'Invalid file type. Only PDF, Word, and Excel files are allowed.');
                        }
                    }

                    //Insert the uploaded file information - table question_bank_file
                    if ($request->hasFile('practicalsolution')) {
                        $uploadedFile = $request->file('practicalsolution');
                        $fileName = $uploadedFile->getClientOriginalName();
                        $extension = $uploadedFile->getClientOriginalExtension();
                        $allowedExtensions = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];
                        if (in_array(strtolower($extension), $allowedExtensions)) {
                            $filePath = $uploadedFile->storeAs('uploads', $fileName, 'public');

                            $qsbank->files()->create([
                                'file_name' => $fileName,
                                'file_path' => $filePath,
                                'file_format' => $extension,
                                'status' => 1,
                                'type' => 'Practical',
                                'category' => 'Solution',
                            ]);
                        } else {
                            return redirect()->back()->with('error', 'Invalid file type. Only PDF, Word, and Excel files are allowed.');
                        }
                    }
                } else {
                    return redirect()->back()->with('error', 'Please upload at least one file.');
                }

                DB::commit();

                return redirect()->route('questionbankrequestlist')->with('success', 'You have successfully uploaded the files.');
            } else {
                return redirect()->back()->with('error', 'Please select training course.');
            }
        } catch (\Exception $e) {

            DB::rollback();
            return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }

    public function questionbanksubmissionview($id)
    {
        $userid = auth()->user()->id;

        //Query for view details of question bank submission
        $submissionlist = DB::table('question_bank')
            ->where('question_bank.id', '=', $id)
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'question_bank.id_training_course')
            ->leftJoin('question_bank_file', 'question_bank.id', '=', 'question_bank_file.id_question_bank')
            ->leftJoin('company', 'company.id', '=', 'question_bank.id_company')
            ->select('question_bank.id', 'company.name', 'company.email', 'training_course.course_name', 'question_bank_file.*')
            ->get();

        $outputData = [];

        foreach ($submissionlist as $row) {
            $courseName = $row->course_name;

            if (!isset($outputData[$courseName])) {
                $outputData[$courseName] = [
                    'id_question_bank' => $row->id_question_bank,
                    'id' => $row->id,
                    'course_name' => $courseName,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],
                ];
            }
            $outputData[$courseName]['id_question_bank'] = $row->id_question_bank;
            $outputData[$courseName]['type'][] = $row->type;
            $outputData[$courseName]['category'][] = $row->category;
            $outputData[$courseName]['file_names'][$row->type][$row->category][] = $row->file_name;
        }

        return view('questionBank.questionBankSubmissionView', compact('userid', 'outputData', 'submissionlist'));
    }

    public function questionBankSubmissionEdit($id)
    {
        $userid = auth()->user()->id;

        //Query for dropdown approved training courses
        $trainingcourse = DB::table('training_course')
            ->leftjoin('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
            ->leftjoin('training_partner_course', 'training_partner_course.id_training_course', '=', 'training_course.id_training_course')
            ->leftjoin('company', 'training_partner_course.id_company', '=', 'company.id')
            ->leftjoin('users', 'users.id_company', '=', 'company.id')
            ->where('users.id', '=', $userid)
            ->where('users.user_type', '=', '3')
            ->where('trainings.id_approval_status', '=', 2)
            ->groupBy('trainings.id', 'trainings.training_code', 'trainings.batch_no_full')
            ->select(
                'users.id as userid',
                'company.id as company_id',
                'trainings.id as id_training',
                'training_course.course_name',
                'training_course.id_training_course',
                'trainings.batch_no_full',
                'trainings.training_code'
            )
            ->orderBy('training_course.course_name')
            ->get();

        //Query for view details of question bank submission
        $submissionlist = DB::table('question_bank')
            ->where('question_bank.id', '=', $id)
            ->leftJoin('trainings', 'trainings.id', '=', 'question_bank.id_trainings')
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('question_bank_file', 'question_bank.id', '=', 'question_bank_file.id_question_bank')
            ->leftJoin('company', 'company.id', '=', 'question_bank.id_company')
            ->select(
                'question_bank.id',
                'question_bank.id_trainings',
                'company.name',
                'company.email',
                'training_course.course_name',
                'question_bank_file.*'
            )
            ->get();

        $outputData = [];

        foreach ($submissionlist as $row) {
            $courseName = $row->course_name;
            $bankid = $row->id_question_bank;

            if (!isset($outputData[$courseName])) {
                $outputData[$courseName] = [
                    'id_question_bank' => $row->id_question_bank,
                    'id_trainings' => $row->id_trainings,
                    'id' => $row->id,
                    'course_name' => $courseName,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],
                ];
            }
            $outputData[$courseName]['id_question_bank'] = $row->id_question_bank;
            $outputData[$courseName]['type'][] = $row->type;
            $outputData[$courseName]['category'][] = $row->category;
            $outputData[$courseName]['file_names'][$row->type][$row->category][] = $row->file_name;
        }

        return view('questionBank.questionBankSubmissionEdit', compact('trainingcourse', 'bankid', 'userid', 'outputData', 'submissionlist'));
    }

    public function questionBankSubmissionUpdate(Request $request, $id)
    {
        $userid = auth()->user()->id;

        $company_id = DB::table('users')->where('id', '=', $userid)->value('id_company');

        //Get the value from the selected dropdown
        $selectedTrainingPartnerId = $request->input('id_trainings');
        //Split the combined values using the delimiter
        $values = explode('|', $selectedTrainingPartnerId);

        DB::beginTransaction();

        try {
            //Check at least one training course was selected
            if ($selectedTrainingPartnerId && $selectedTrainingPartnerId !== 0) {

                //Check existing question bank
                $existingBankQuestion = DB::table('question_bank')->where('id', '=', $id)->get();

                //If the question bank exists
                if ($existingBankQuestion) {
                    //Update new submission question bank - table question_bank
                    DB::table('question_bank')
                        ->where('question_bank.id', '=', $id)
                        ->update([
                            'id_trainings' => $values[0],
                            'id_training_course' => $values[1],
                            'id_company' => $company_id,
                            'id_approval_status' => 1,
                            'updated_by' => $userid,
                            'updated_at' => Carbon::now()
                        ]);
                }

                if ($request->hasFile('theoryquestion')) {
                    $uploadedFile = $request->file('theoryquestion');
                    $fileName = $uploadedFile->getClientOriginalName();
                    $extension = $uploadedFile->getClientOriginalExtension();
                    $allowedExtensions = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];
                    if (in_array(strtolower($extension), $allowedExtensions)) {
                        $filePath = $uploadedFile->storeAs('uploads', $fileName, 'public');

                        //Check existing question bank file
                        $existingQuestionBankFile = DB::table('question_bank_file')
                            ->where('id_question_bank', '=', $id)
                            ->where('type', '=', 'Theory')
                            ->where('category', '=', 'Question')
                            ->first();

                        //If the question bank file exists, update the information
                        if ($existingQuestionBankFile) {

                            $fileID = $existingQuestionBankFile->id;

                            DB::table('question_bank_file')
                                ->where('id', '=', $fileID)
                                ->update([
                                    'file_name' => $fileName,
                                    'file_path' => $filePath,
                                    'file_format' => $extension,
                                ]);
                        }
                    } else {
                        return redirect()->back()->with('error', 'Invalid file type. Only PDF, Word, and Excel files are allowed.');
                    }
                }

                if ($request->hasFile('theorysolution')) {
                    $uploadedFile = $request->file('theorysolution');
                    $fileName = $uploadedFile->getClientOriginalName();
                    $extension = $uploadedFile->getClientOriginalExtension();
                    $allowedExtensions = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];
                    if (in_array(strtolower($extension), $allowedExtensions)) {
                        $filePath = $uploadedFile->storeAs('uploads', $fileName, 'public');

                        //Check existing question bank file
                        $existingQuestionBankFile = DB::table('question_bank_file')
                            ->where('id_question_bank', '=', $id)
                            ->where('type', '=', 'Theory')
                            ->where('category', '=', 'Solution')
                            ->first();

                        //If the question bank file exists, update the information
                        if ($existingQuestionBankFile) {

                            $fileID = $existingQuestionBankFile->id;

                            DB::table('question_bank_file')
                                ->where('id', '=', $fileID)
                                ->update([
                                    'file_name' => $fileName,
                                    'file_path' => $filePath,
                                    'file_format' => $extension,
                                ]);
                        }
                    } else {
                        return redirect()->back()->with('error', 'Invalid file type. Only PDF, Word, and Excel files are allowed.');
                    }
                }

                if ($request->hasFile('practicalquestion')) {
                    $uploadedFile = $request->file('practicalquestion');
                    $fileName = $uploadedFile->getClientOriginalName();
                    $extension = $uploadedFile->getClientOriginalExtension();
                    $allowedExtensions = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];
                    if (in_array(strtolower($extension), $allowedExtensions)) {
                        $filePath = $uploadedFile->storeAs('uploads', $fileName, 'public');

                        //Check existing question bank file
                        $existingQuestionBankFile = DB::table('question_bank_file')
                            ->where('id_question_bank', '=', $id)
                            ->where('type', '=', 'Practical')
                            ->where('category', '=', 'Question')
                            ->first();

                        //If the question bank file exists, update the information
                        if ($existingQuestionBankFile) {

                            $fileID = $existingQuestionBankFile->id;

                            DB::table('question_bank_file')
                                ->where('id', '=', $fileID)
                                ->update([
                                    'file_name' => $fileName,
                                    'file_path' => $filePath,
                                    'file_format' => $extension,
                                ]);
                        }
                    } else {
                        return redirect()->back()->with('error', 'Invalid file type. Only PDF, Word, and Excel files are allowed.');
                    }
                }

                //Insert the uploaded file information - table question_bank_file
                if ($request->hasFile('practicalsolution')) {
                    $uploadedFile = $request->file('practicalsolution');
                    $fileName = $uploadedFile->getClientOriginalName();
                    $extension = $uploadedFile->getClientOriginalExtension();
                    $allowedExtensions = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];
                    if (in_array(strtolower($extension), $allowedExtensions)) {
                        $filePath = $uploadedFile->storeAs('uploads', $fileName, 'public');

                        //Check existing question bank file
                        $existingQuestionBankFile = DB::table('question_bank_file')
                            ->where('id_question_bank', '=', $id)
                            ->where('type', '=', 'Practical')
                            ->where('category', '=', 'Solution')
                            ->first();

                        //If the question bank file exists, update the information
                        if ($existingQuestionBankFile) {

                            $fileID = $existingQuestionBankFile->id;

                            DB::table('question_bank_file')
                                ->where('id', '=', $fileID)
                                ->update([
                                    'file_name' => $fileName,
                                    'file_path' => $filePath,
                                    'file_format' => $extension,
                                ]);
                        }
                    } else {
                        return redirect()->back()->with('error', 'Invalid file type. Only PDF, Word, and Excel files are allowed.');
                    }
                }

                DB::commit();

                return redirect()->route('questionbankrequestlist')->with('success', 'Question bank details successfully updated.');
            } else {
                return redirect()->back()->with('error', 'Please select training course.');
            }
        } catch (\Exception $e) {

            DB::rollback();
            return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }

    public function questionBankReupload(Request $request, $id)
    {
        // dd($request->all());
        $userid = auth()->user()->id;

        DB::beginTransaction();

        try {

            //Check existing question bank
            $existingBankQuestion = DB::table('question_bank')->where('id', '=', $id)->get();

            //If the question bank exists
            if ($existingBankQuestion) {
                //Update new submission question bank - table question_bank
                DB::table('question_bank')
                    ->where('question_bank.id', '=', $id)
                    ->update([
                        'updated_by' => $userid,
                        'updated_at' => Carbon::now()
                    ]);
            }

            if ($request->hasFile('theoryquestion')) {
                $uploadedFile = $request->file('theoryquestion');
                $fileName = $uploadedFile->getClientOriginalName();
                $extension = $uploadedFile->getClientOriginalExtension();
                $allowedExtensions = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];
                if (in_array(strtolower($extension), $allowedExtensions)) {
                    $filePath = $uploadedFile->storeAs('uploads', $fileName, 'public');

                    //Check existing question bank file
                    $existingQuestionBankFile = DB::table('question_bank_file')
                        ->where('id_question_bank', '=', $id)
                        ->where('type', '=', 'Theory')
                        ->where('category', '=', 'Question')
                        ->first();

                    //If the question bank file exists, update the information
                    if ($existingQuestionBankFile) {

                        $fileID = $existingQuestionBankFile->id;

                        DB::table('question_bank_file')
                            ->where('id', '=', $fileID)
                            ->update([
                                'file_name' => $fileName,
                                'file_path' => $filePath,
                                'file_format' => $extension,
                            ]);
                    }
                } else {
                    return redirect()->back()->with('error', 'Invalid file type. Only PDF, Word, and Excel files are allowed.');
                }
            }

            if ($request->hasFile('theorysolution')) {
                $uploadedFile = $request->file('theorysolution');
                $fileName = $uploadedFile->getClientOriginalName();
                $extension = $uploadedFile->getClientOriginalExtension();
                $allowedExtensions = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];
                if (in_array(strtolower($extension), $allowedExtensions)) {
                    $filePath = $uploadedFile->storeAs('uploads', $fileName, 'public');

                    //Check existing question bank file
                    $existingQuestionBankFile = DB::table('question_bank_file')
                        ->where('id_question_bank', '=', $id)
                        ->where('type', '=', 'Theory')
                        ->where('category', '=', 'Solution')
                        ->first();

                    //If the question bank file exists, update the information
                    if ($existingQuestionBankFile) {

                        $fileID = $existingQuestionBankFile->id;

                        DB::table('question_bank_file')
                            ->where('id', '=', $fileID)
                            ->update([
                                'file_name' => $fileName,
                                'file_path' => $filePath,
                                'file_format' => $extension,
                            ]);
                    }
                } else {
                    return redirect()->back()->with('error', 'Invalid file type. Only PDF, Word, and Excel files are allowed.');
                }
            }

            if ($request->hasFile('practicalquestion')) {
                $uploadedFile = $request->file('practicalquestion');
                $fileName = $uploadedFile->getClientOriginalName();
                $extension = $uploadedFile->getClientOriginalExtension();
                $allowedExtensions = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];
                if (in_array(strtolower($extension), $allowedExtensions)) {
                    $filePath = $uploadedFile->storeAs('uploads', $fileName, 'public');

                    //Check existing question bank file
                    $existingQuestionBankFile = DB::table('question_bank_file')
                        ->where('id_question_bank', '=', $id)
                        ->where('type', '=', 'Practical')
                        ->where('category', '=', 'Question')
                        ->first();

                    //If the question bank file exists, update the information
                    if ($existingQuestionBankFile) {

                        $fileID = $existingQuestionBankFile->id;

                        DB::table('question_bank_file')
                            ->where('id', '=', $fileID)
                            ->update([
                                'file_name' => $fileName,
                                'file_path' => $filePath,
                                'file_format' => $extension,
                            ]);
                    }
                } else {
                    return redirect()->back()->with('error', 'Invalid file type. Only PDF, Word, and Excel files are allowed.');
                }
            }

            //Insert the uploaded file information - table question_bank_file
            if ($request->hasFile('practicalsolution')) {
                $uploadedFile = $request->file('practicalsolution');
                $fileName = $uploadedFile->getClientOriginalName();
                $extension = $uploadedFile->getClientOriginalExtension();
                $allowedExtensions = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];
                if (in_array(strtolower($extension), $allowedExtensions)) {
                    $filePath = $uploadedFile->storeAs('uploads', $fileName, 'public');

                    //Check existing question bank file
                    $existingQuestionBankFile = DB::table('question_bank_file')
                        ->where('id_question_bank', '=', $id)
                        ->where('type', '=', 'Practical')
                        ->where('category', '=', 'Solution')
                        ->first();

                    //If the question bank file exists, update the information
                    if ($existingQuestionBankFile) {

                        $fileID = $existingQuestionBankFile->id;

                        DB::table('question_bank_file')
                            ->where('id', '=', $fileID)
                            ->update([
                                'file_name' => $fileName,
                                'file_path' => $filePath,
                                'file_format' => $extension,
                            ]);
                    }
                } else {
                    return redirect()->back()->with('error', 'Invalid file type. Only PDF, Word, and Excel files are allowed.');
                }
            }

            DB::commit();

            return redirect()->route('admin.questionbank-list')->with('success', 'Question bank details successfully updated.');
        } catch (\Exception $e) {

            DB::rollback();
            return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }

    public function download()
    {

        $userid = auth()->user()->id;
        $requestlist = DB::table('question_bank_request')
            ->where('question_bank_request.id_training_partner', '=', $userid)
            ->leftjoin('question_bank', 'question_bank.id', '=', 'question_bank_request.id_question_bank')
            ->leftjoin('company', 'company.id', '=', 'question_bank.id_company')
            ->leftjoin('question_bank_file', 'question_bank_request.id_question_bank', '=', 'question_bank_file.id')
            ->leftjoin('training_course', 'training_course.id_training_course', '=', 'question_bank.id_training_course')
            ->leftjoin('question_bank_request_file', 'question_bank_request_file.id_question_bank', '=', 'question_bank.id')
            ->select('question_bank_request.id', 'company.name as company_name', 'training_course.course_name', 'question_bank_file.*')


            ->get();

        // dd($requestlist);

        $outputDataRequest = [];


        foreach ($requestlist as $row) {

            $bankid = $row->id;
            // dd($row);
            if (!isset($outputDataRequest[$bankid])) {
                $outputDataRequest[$bankid] = [
                    'company_name' => $row->company_name,
                    'course_name' => $row->course_name,
                    'type' => [],
                    'category' => [],
                    'file_names' => [],
                    'attachment' => [
                        'path' => [],
                    ],
                ];
            }

            $outputDataRequest[$bankid]['type'][] = $row->type;
            $outputDataRequest[$bankid]['category'][] = $row->category;
            $outputDataRequest[$bankid]['file_names'][$row->type][$row->category][] = $row->file_name;
            $outputDataRequest[$bankid]['attachment']['path'][] = public_path('storage/uploads' . DIRECTORY_SEPARATOR . $row->file_name);
        }

        foreach ($outputDataRequest as $data) {
            foreach ($data['attachment']['path'] as $filePath) {

                if (Storage::exists($filePath)) {
                    $fileName = basename($filePath);
                    $downloadLink = route('download.file', ['file' => $fileName]);
                    // Generate a download link or initiate the download as needed
                    echo "<a href='$downloadLink'>Download $fileName</a><br>";
                } else {
                    return back()->with('error', 'File not found');
                }
            }
        }
    }


    public function addTPList()
    {
        try {
            $trainingPartnerList = DB::table('question_bank as qb')
                ->leftjoin('company as cp', 'qb.id_company', '=', 'cp.id')
                ->select(
                    'cp.id',
                    'cp.name'
                )
                ->get();

            if ($trainingPartnerList->isEmpty()) {
                // Handle empty case or redirect
            } else {
                Log::info('Successfully fetched training partner list.');
                return view('questionBank.addTPList', ['partnerList' => $trainingPartnerList]);
            }
        } catch (\Exception $e) {
            Log::error('Error fetching training partner list: ' . $e->getMessage());
        }
    }
}
