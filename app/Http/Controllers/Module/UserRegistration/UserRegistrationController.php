<?php

namespace App\Http\Controllers\Module\UserRegistration;

use App\Http\Controllers\Controller;
use App\Models\AuditTrail;
use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationDone;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password as RulesPassword;

class UserRegistrationController extends Controller
{
    //
    public function registerPage()
    {
        $identity = DB::table('identification_type')->orderBy('id_identity', 'asc')->get();
        return view('userRegistration.userRegistration',[
            'identity' => $identity,
        ]);
    }

    public function createRegistrationPage(Request $request)
    {

            $request->validate([
                'fullname' => 'required|string|max:255',
                'ic_no' => 'required|string|max:255|unique:users',
                'phone_no' => 'required|string|max:255',
                'email' => 'required|email|unique:users|max:255',
                'password' => [
                    'required',
                    'string',
                    RulesPassword::min(8)
                        ->mixedCase()
                        ->numbers()
                        ->symbols(),
                ],
                'terms' => 'accepted', 
            ], [
                'terms.accepted' => 'You must accept the terms and conditions.',
            ]);

      $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

        AuditTrail::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Created New User',
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

            $user = Users::create([
                'fullname' => $request['fullname'],
                'id_identity' => $request['identity'],
                'ic_no' => $request['ic_no'],
                'phone_no' => $request['phone_no'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'password_confirmation'=> bcrypt($request['password_confirmation']),
                'user_type' => 5,
                'status'=> 1,
                'terms' => $request['terms'] ? '1' : '0',
                'created_at' => now()
            ]);
            
            if(!$user){
                return redirect()->back()->with('error','Failed to register');
    
            }else{
                
            Mail::to($user->email)->send(new RegistrationDone($user));
            return redirect()->route('login')->with('success','Success to register'); 
            }
        
    }
}
