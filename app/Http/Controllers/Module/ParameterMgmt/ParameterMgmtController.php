<?php

namespace App\Http\Controllers\Module\ParameterMgmt;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\TrainingCourse;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
class ParameterMgmtController extends Controller
{
    //
    public function bannerConfiguration()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        return view('admin.banners.index', compact('banners'));
    }

    public function createBanner()
    {
        $userid = auth()->user()->id;

        return view('admin.banners.create');
    }

    public function country()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.country', compact('userid'));
    }

    public function addCountry()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.addCountry', compact('userid'));
    }

    public function addNewCountry(Request $request)
    {

        $userid = auth()->user()->id;
        try{

            $country = DB::table('country')
            ->where('country.name', '=',$request['name'])
            ->select('country.name')->get();

            foreach($country as $countrei){
                $namecountry = $countrei->name;
            }

            if($namecountry === $request['name']){
                return redirect()->route('country')->with('error', 'Country has been exist');
            }
            $addCountry = DB::table('country')->insertGetId([
                'name' => $request['name'],
                'iso' => $request['iso'],
                'iso3' => $request['iso3'],
                'numcode' => $request['numcode'],
                'phonecode' => $request['phonecode'],
                'created_by' => $userid,
                'created_at' => now()
            ]);
            // dd($trainingCourseId);

            if ($addCountry) {
                return redirect()->route('country')->with('success', 'Success to create country');
            } else {
                return redirect()->back()->with('error', 'Failed to create country.');
            }
        }catch(Exception $ex){
            return redirect()->back()->with('error', 'Failed to create country.');
        }

    }
    public function editCountry(Request $request,$id)
    {
        $userid = auth()->user()->id;
        $country = DB::table('country')
        ->where('country.id', '=', $id)
        ->select('country.id','country.name','country.iso','country.nicename'
        ,'country.iso3','country.numcode','country.phonecode')->get();
    
        return view('parameterMgmt.editCountry', compact('userid','country'));
    }

    public function updateCountry(Request $request,$id)
    {
        $userid = auth()->user()->id;

        try {
            $updateCountry = DB::table('country')
            ->where('id', $id)
            ->update([
                'name' => $request->input('name'),
                'iso' => $request->input('iso'),
                'nicename' => $request->input('nicename'),
                'iso3' => $request->input('iso3'),
                'numcode' => $request->input('numcode'),
                'phonecode' => $request->input('phonecode')
            ]);

            if ($updateCountry) {
                return redirect()->route('country')->with('success', 'Country updated successfully.');
            } else {
                return redirect()->back()->with('error', 'Failed to update the Country.');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }

    public function deleteCountry($id)
    {
        
        $userid = auth()->user()->id;

        try {
            DB::beginTransaction();
            $deleteData = DB::table('country')
            ->where('country.id','=',$id)
            ->delete();

            if ($deleteData) {
                DB::commit();
                return redirect()->route('country')->with('success', 'Country Deleted successfully');
     
            } else {
                DB::rollback();
                return redirect()->route('country')->with('error', 'Country not found');
            }
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('country')->with('error', 'The data cannot be deleted because the data is in use.');
        }
    }

    public function gender()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.gender', compact('userid'));
    }

    public function editGender(Request $request,$id)
    {
        $userid = auth()->user()->id;
        $gender = DB::table('gender')
        ->where('gender.id', '=', $id)
        ->select('gender.id','gender.name')->get();
    
        return view('parameterMgmt.editGender', compact('userid','gender'));
    }

    public function updateGender(Request $request,$id)
    {
        $userid = auth()->user()->id;

        try {
            $updateGender = DB::table('gender')
            ->where('id', $id)
            ->update([
                'name' => $request->input('gender_name')
            ]);

            if ($updateGender) {
                return redirect()->route('gender')->with('success', 'Gender updated successfully.');
            } else {
                return redirect()->back()->with('error', 'Failed to update the gender.');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }

    public function certCode()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.certCode', compact('userid'));
    }

    public function addCertCode()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.addCertCode', compact('userid'));
    }

    public function addNewCertCode(Request $request)
    {

        $userid = auth()->user()->id;
        try{
            $addNewCertCode = DB::table('training_cert_code')->insertGetId([
                'code' => $request['code'],
                'param' => $request['param'],
                'created_by' => $userid,
                'created_at' => now(),
                'status' => 1
            ]);
            // dd($trainingCourseId);

            if ($addNewCertCode) {
                return redirect()->route('certCode')->with('success', 'Success to create Certificate');
            } else {
                return redirect()->back()->with('error', 'Failed to create Certificate.');
            }
        }catch(Exception $ex){
            return redirect()->back()->with('error', 'Failed to create Certificate.');
        }

    }

    public function editCertCode(Request $request,$id)
    {
        $userid = auth()->user()->id;
        $training_cert_code = DB::table('training_cert_code')
        ->where('training_cert_code.id', '=', $id)
        ->select('training_cert_code.id','training_cert_code.code',
                'training_cert_code.param','training_cert_code.status')->get();
    
        return view('parameterMgmt.editCertCode', compact('userid','training_cert_code'));
    }

    public function updateCertCode(Request $request,$id)
    {
        $userid = auth()->user()->id;

        try {
            $updateCertCode = DB::table('training_cert_code')
            ->where('id', $id)
            ->update([
                'code' => $request->input('code'),
                'param' => $request->input('param')
            ]);

            if ($updateCertCode) {
                return redirect()->route('certCode')->with('success', 'Certificate updated successfully.');
            } else {
                return redirect()->back()->with('error', 'Failed to update the certificate.');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }

    public function deleteCertCode($id)
    {
        
        $userid = auth()->user()->id;

        try {
            DB::beginTransaction();
            $deleteData = DB::table('training_cert_code')
            ->where('training_cert_code.id','=',$id)
            ->delete();

            if ($deleteData) {
                DB::commit();
                return redirect()->route('certCode')->with('success', 'Certificate Deleted successfully');
     
            } else {
                DB::rollback();
                return redirect()->route('certCode')->with('error', 'Certificate not found');
            }
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('certCode')->with('error', 'The data cannot be deleted because the data is in use.');
        }
    }

    public function feeType()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.feeType', compact('userid'));
    }

    public function addFeeType()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.addFeeType', compact('userid'));
    }

    public function addNewFeeType(Request $request)
    {

        $userid = auth()->user()->id;
        try{
            $addNewFeeType = DB::table('fee_type')->insertGetId([
                'fee_code' => $request['fee_code'],
                'fee_name' => $request['fee_name'],
                'created_by' => $userid,
                'created_at' => now(),
                'status' => 1
            ]);
            // dd($trainingCourseId);

            if ($addNewFeeType) {
                return redirect()->route('feeType')->with('success', 'Success to create Fee Type');
            } else {
                return redirect()->back()->with('error', 'Failed to create Fee Type.');
            }
        }catch(Exception $ex){
            return redirect()->back()->with('error', 'Failed to create Fee Type.');
        }

    }
    
    public function trainingCourse()
    {
        $userid = auth()->user()->id;

        $company_id = DB::table('company')
        ->join('users', 'users.id_company', '=', 'company.id')
        ->where('users.id', '=', $userid)
        ->value('company.id');

        info('Company ID: ' . $company_id);

        $courseList = DB::table('training_course')
            ->leftjoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->leftjoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftjoin('course_re_type', 'course_re_type.id_training_course', '=', 'training_course.id_training_course')
            ->leftjoin('re_type', 're_type.id', '=', 'course_re_type.id_re_type')
            ->leftjoin('training_option', 'training_option.id', '=', 'training_course.id_training_option')
            ->leftjoin('training_partner_course', 'training_partner_course.id_training_course', '=', 'training_course.id_training_course')
            ->leftjoin('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
            ->select(
                'training_course.id_training_course',
                'training_course.course_name',
                'training_course.new_fee',
                'training_course.new_fee_int',
                'training_type.name as course_type',
                're_type.re_type',
                'training_option.option',
                'course_category.course_name as category_name',
                'training_partner_course.course_code as course_code',
                'training_cert_code.code as cert_code',
            )
            ->get();

        $outputData = [];

        foreach ($courseList as $row) {
            $courseName = $row->course_name;

            if (!isset($outputData[$courseName])) {
                $outputData[$courseName] = [
                    'id' => $row->id_training_course,
                    'course_name' => $courseName,
                    'course_type' => $row->course_type,
                    'category_name' => $row->category_name,
                    'training_option' => $row->option,
                    'new_fee' => $row->new_fee,
                    'new_fee_int' => $row->new_fee_int,
                    're_type' => [],
                    'cert_code' => $row->cert_code,
                ];
            }
            $outputData[$courseName]['id'] = $row->id_training_course;
            $outputData[$courseName]['re_type'][] = $row->re_type;
        }

        // Filter out duplicate courses
        $filteredCourses = array_values($outputData);

        $partnertr = DB::table('company')
            ->join('training_partner_course', 'training_partner_course.id_company', '=', 'company.id')
            ->join('training_course', 'training_course.id_training_course', '=', 'training_partner_course.id_training_course')
            ->join('training_cert_code', 'training_cert_code.id', '=', 'training_course.id_training_cert')
            ->select(
                'company.id as id', 
                'company.code as company_code',
                'company.name as name',
                'company.address_1',
                'company.address_2',
                'company.postcode',
                'training_partner_course.course_code as course_code',
                'training_cert_code.code as cert_code',
            )
            ->get();
      

            return view('parameterMgmt.addFeeType', compact('userid', 'filteredCourses'));

    }

    public function editFeeType(Request $request,$id)
    {
        $userid = auth()->user()->id;
        $fee_type = DB::table('fee_type')
        ->where('fee_type.id_fee', '=', $id)
        ->select('fee_type.id_fee','fee_type.fee_code',
                'fee_type.fee_name','fee_type.status')->get();
    
        return view('parameterMgmt.editFeeType', compact('userid','fee_type'));
    }

    public function updateFeeType(Request $request,$id)
    {
        $userid = auth()->user()->id;

        try {
            $updateFeeType = DB::table('fee_type')
            ->where('id_fee', $id)
            ->update([
                'fee_code' => $request->input('fee_code'),
                'fee_name' => $request->input('fee_name')
            ]);

            if ($updateFeeType) {
                return redirect()->route('feeType')->with('success', 'Fee Type updated successfully.');
            } else {
                return redirect()->back()->with('error', 'Failed to update the Fee Type.');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }

    public function deleteFeeType($id)
    {
        
        $userid = auth()->user()->id;

        try {
            DB::beginTransaction();
            $deleteData = DB::table('fee_type')
            ->where('fee_type.id_fee','=',$id)
            ->delete();

            if ($deleteData) {
                DB::commit();
                return redirect()->route('feeType')->with('success', 'Fee Type Deleted successfully');
     
            } else {
                DB::rollback();
                return redirect()->route('feeType')->with('error', 'Fee Type not found');
            }
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('feeType')->with('error', 'The data cannot be deleted because the data is in use.');
        }
    }

    public function trainingOption()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.trainingOption', compact('userid'));
    }

    public function addTrainingOption()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.addTrainingOption', compact('userid'));
    }

    public function addNewTrainingOption(Request $request)
    {

        $userid = auth()->user()->id;
        try{
            $addNewTrainingOption = DB::table('training_option')->insertGetId([
                'option' => $request['option'],
                'created_by' => $userid,
                'created_at' => now(),
                'status' => 1
            ]);
            // dd($trainingCourseId);

            if ($addNewTrainingOption) {
                return redirect()->route('trainingOption')->with('success', 'Success to create Training Option');
            } else {
                return redirect()->back()->with('error', 'Failed to create Training Option.');
            }
        }catch(Exception $ex){
            return redirect()->back()->with('error', 'Failed to create Training Option.');
        }

    }

    public function editTrainingOption(Request $request,$id)
    {
        $userid = auth()->user()->id;
        $training_option = DB::table('training_option')
        ->where('training_option.id', '=', $id)
        ->select('training_option.id','training_option.option','training_option.status')->get();
    
        return view('parameterMgmt.editTrainingOption', compact('userid','training_option'));
    }

    public function updateTrainingOption(Request $request,$id)
    {
        $userid = auth()->user()->id;

        try {
            $updateTrainingOption = DB::table('training_option')
            ->where('id', $id)
            ->update([
                'option' => $request->input('option')
            ]);

            if ($updateTrainingOption) {
                return redirect()->route('trainingOption')->with('success', 'Training Option updated successfully.');
            } else {
                return redirect()->back()->with('error', 'Failed to update the Training Option.');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }

    public function deleteTrainingOption($id)
    {
        
        $userid = auth()->user()->id;

        try {
            DB::beginTransaction();
            $deleteData = DB::table('training_option')
            ->where('id','=',$id)
            ->delete();

            if ($deleteData) {
                DB::commit();
                return redirect()->route('trainingOption')->with('success', 'Training Option Deleted successfully');
     
            } else {
                DB::rollback();
                return redirect()->route('trainingOption')->with('error', 'Training Option not found');
            }
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('trainingOption')->with('error', 'The data cannot be deleted because the data is in use.');
        }
    }

    //paste baru education level
    public function educationLevel()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.educationLevel', compact('userid'));
    }

    public function addEduLevel()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.addEduLevel', compact('userid'));
    }

    public function addNewEducationLevel(Request $request)
    {

        $userid = auth()->user()->id;
        try{
            $addNewEducationLevel = DB::table('education_level')->insertGetId([
                'education_level' => $request['education_level'],
                'created_by' => $userid,
                'created_at' => now(),
                'status' => 1
            ]);
            // dd($trainingCourseId);

            if ($addNewEducationLevel) {
                return redirect()->route('educationLevel')->with('success', 'Success to create Education Level');
            } else {
                return redirect()->back()->with('error', 'Failed to create Education Level.');
            }
        }catch(Exception $ex){
            return redirect()->back()->with('error', 'Failed to create Education Level.');
        }

    }

    public function editEduLevel(Request $request,$id)
    {
        $userid = auth()->user()->id;
        $edu_level = DB::table('education_level')
        ->where('education_level.id', '=', $id)
        ->select('education_level.id','education_level.education_level','education_level.status')->get();
    
        return view('parameterMgmt.editEduLevel', compact('userid','edu_level'));
    }

    public function updateEduLevel(Request $request,$id)
    {
        $userid = auth()->user()->id;

        try {
            $updateEduLevel = DB::table('education_level')
            ->where('id', $id)
            ->update([
                'education_level' => $request->input('education_level')
            ]);

            if ($updateEduLevel) {
                return redirect()->route('educationLevel')->with('success', 'Education Level updated successfully.');
            } else {
                return redirect()->back()->with('error', 'Failed to update the Education Level.');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }

    public function deleteEducationLevel($id)
    {
        
        $userid = auth()->user()->id;

        try {
            DB::beginTransaction();
            $deleteData = DB::table('education_level')
            ->where('id','=',$id)
            ->delete();

            if ($deleteData) {
                DB::commit();
                return redirect()->route('educationLevel')->with('success', 'Education Level Deleted successfully');
     
            } else {
                DB::rollback();
                return redirect()->route('educationLevel')->with('error', 'Education Level not found');
            }
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('educationLevel')->with('error', 'The data cannot be deleted because the data is in use.');
        }
    }
    //paste baru education level

    public function trainingMode()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.trainingMode', compact('userid'));
    }

    public function addTrainingMode()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.addTrainingMode', compact('userid'));
    }

    public function addNewTrainingMode(Request $request)
    {

        $userid = auth()->user()->id;
        try{
            $addNewTrainingMode = DB::table('training_mode')->insertGetId([
                'code' => $request['code'],
                'mode' => $request['mode'],
                'created_by' => $userid,
                'created_at' => now(),
                'status' => 1
            ]);
            // dd($trainingCourseId);

            if ($addNewTrainingMode) {
                return redirect()->route('trainingMode')->with('success', 'Success to create Training Mode');
            } else {
                return redirect()->back()->with('error', 'Failed to create Training Mode.');
            }
        }catch(Exception $ex){
            return redirect()->back()->with('error', 'Failed to create Training Mode.');
        }

    }

    public function editTrainingMode(Request $request,$id)
    {
        $userid = auth()->user()->id;
        $training_mode = DB::table('training_mode')
        ->where('training_mode.id', '=', $id)
        ->select('training_mode.id','training_mode.code','training_mode.mode','training_mode.status')->get();
    
        return view('parameterMgmt.editTrainingMode', compact('userid','training_mode'));
    }

    public function updateTrainingMode(Request $request,$id)
    {
        $userid = auth()->user()->id;

        try {
            $updateTrainingMode = DB::table('training_mode')
            ->where('id', $id)
            ->update([
                'code' => $request->input('code'),
                'mode' => $request->input('mode')
            ]);

            if ($updateTrainingMode) {
                return redirect()->route('trainingMode')->with('success', 'Training Mode updated successfully.');
            } else {
                return redirect()->back()->with('error', 'Failed to update the Training Mode.');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }

    public function deleteTrainingModen($id)
    {
        
        $userid = auth()->user()->id;

        try {
            DB::beginTransaction();
            $deleteData = DB::table('training_mode')
            ->where('id','=',$id)
            ->delete();

            if ($deleteData) {
                DB::commit();
                return redirect()->route('trainingMode')->with('success', 'Training Mode Deleted successfully');
     
            } else {
                DB::rollback();
                return redirect()->route('trainingMode')->with('error', 'Training Mode not found');
            }
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('trainingMode')->with('error', 'The data cannot be deleted because the data is in use.');
        }
    }

    public function trainingType()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.trainingType', compact('userid'));
    }

    public function addTrainingType()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.addTrainingType', compact('userid'));
    }

    public function addNewTrainingType(Request $request)
    {

        $userid = auth()->user()->id;
        try{
            $addNewTrainingType = DB::table('training_type')->insertGetId([
                'code' => $request['code'],
                'name' => $request['name'],
                'created_by' => $userid,
                'created_at' => now(),
                'status' => 1
            ]);
            // dd($trainingCourseId);

            if ($addNewTrainingType) {
                return redirect()->route('trainingType')->with('success', 'Success to create Training Type');
            } else {
                return redirect()->back()->with('error', 'Failed to create Training Type.');
            }
        }catch(Exception $ex){
            return redirect()->back()->with('error', 'Failed to create Training Type.');
        }

    }

    public function editTrainingType(Request $request,$id)
    {
        $userid = auth()->user()->id;
        $training_type = DB::table('training_type')
        ->where('training_type.id', '=', $id)
        ->select('training_type.id','training_type.code','training_type.name','training_type.status')->get();
    
        return view('parameterMgmt.editTrainingType', compact('userid','training_type'));
    }

    public function updateTrainingType(Request $request,$id)
    {
        $userid = auth()->user()->id;

        try {
            $updateTrainingType = DB::table('training_type')
            ->where('id', $id)
            ->update([
                'code' => $request->input('code'),
                'name' => $request->input('name')
            ]);

            if ($updateTrainingType) {
                return redirect()->route('trainingType')->with('success', 'Training Type updated successfully.');
            } else {
                return redirect()->back()->with('error', 'Failed to update the Training Type.');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }

    public function deleteTrainingType($id)
    {
        
        $userid = auth()->user()->id;

        try {
            DB::beginTransaction();
            $deleteData = DB::table('training_type')
            ->where('id','=',$id)
            ->delete();

            if ($deleteData) {
                DB::commit();
                return redirect()->route('trainingType')->with('success', 'Training Type Deleted successfully');
     
            } else {
                DB::rollback();
                return redirect()->route('trainingType')->with('error', 'Training Type not found');
            }
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('trainingType')->with('error', 'The data cannot be deleted because the data is in use.');
        }
    }

    public function reType()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.reType', compact('userid'));
    }

    public function addReType()
    {
        $userid = auth()->user()->id;

        $banners = Banner::get();
        // return view('admin.banners.index', compact('banners'));
        return view('parameterMgmt.addReType', compact('userid'));
    }

    public function addNewReType(Request $request)
    {

        $userid = auth()->user()->id;
        try{
            $addNewReType = DB::table('re_type')->insertGetId([
                're_type' => $request['re_type'],
                'created_by' => $userid,
                'created_at' => now(),
                'status' => 1
            ]);
            // dd($trainingCourseId);

            if ($addNewReType) {
                return redirect()->route('reType')->with('success', 'Success to create RE Type');
            } else {
                return redirect()->back()->with('error', 'Failed to create RE Type.');
            }
        }catch(Exception $ex){
            return redirect()->back()->with('error', 'Failed to create RE Type.');
        }

    }

    public function editReType(Request $request,$id)
    {
        $userid = auth()->user()->id;
        $re_type = DB::table('re_type')
        ->where('re_type.id', '=', $id)
        ->select('re_type.id','re_type.re_type','re_type.re','re_type.status')->get();
    
        return view('parameterMgmt.editReType', compact('userid','re_type'));
    }

    public function updateReType(Request $request,$id)
    {
        $userid = auth()->user()->id;

        try {
            $updateReType = DB::table('re_type')
            ->where('id', $id)
            ->update([
                're_type' => $request->input('re_type'),
                're' => $request->input('re')
            ]);

            if ($updateReType) {
                return redirect()->route('reType')->with('success', 'RE Type updated successfully.');
            } else {
                return redirect()->back()->with('error', 'Failed to update the RE Type.');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'An error occurred: ' . $e->getMessage());
        }
    }

    public function deleteReType($id)
    {
        
        $userid = auth()->user()->id;

        try {
            DB::beginTransaction();
            $deleteData = DB::table('re_type')
            ->where('id','=',$id)
            ->delete();

            if ($deleteData) {
                DB::commit();
                return redirect()->route('reType')->with('success', 'RE Type Deleted successfully');
     
            } else {
                DB::rollback();
                return redirect()->route('reType')->with('error', 'RE Type not found');
            }
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('reType')->with('error', 'The data cannot be deleted because the data is in use.');
        }
    }


    public function store_count($request,$type)
    {
        $userid = auth()->user()->id;

        //using array limit
        return Banner::where('banner_type', $type)
        ->where('isActive', 1)->count() < $this->limits[$type] && $request->isActive == 1;
    }

    public function update_count($banner,$type)
    {
        $userid = auth()->user()->id;

        return Banner::whereNotIn('id', [$banner->id])
        ->where('isActive', 1)
        ->where('type', $banner->banner_type)->count() < $this->limits[$type] && $banner->isActive == 1;
    }

    public function storeBanner(Request $request)
    {
        //validating form data
        $data = $request->validate([
            'title' => "required",
            'url' => "required",
            'image' => "required",
            'image_title' => "max:255",
            'image_alt' => "max:255",
            'banner_type' => "required|in:small,medium,large,news",
            'isActive' => "nullable|in:0,1" //active or not
        ]);

        //validating images active count
        if (!$this->store_count($request, $request->banner_typex)) {
        return redirect()->back()->withInput($request->all())
            ->withErrors(['isActive' => '1' . $this->limits[$request['banner_type']] . '1']);
    }

        Banner::create($data);
        return redirect(route('admin.banners.index'));
    }

    public function showBanner()
    {
        $userid = auth()->user()->id;

        $banner = Banner::findOrFail();
        return view('admin.banners.edit');
    }
    public function update(Request $request, $id)
    {
        $userid = auth()->user()->id;

        $banner = Banner::findOrFail($id);
        //validate update form data here
        //your validation
        //validating images active count
        if(!$this->update_count($banner, $request->banner_type)){
            return redirect()->back()
           ->withInput($request->all())
           ->withErrors($this->limits[$request['banner_type']]);
        }
        $banner = $banner->fill([
            'title' => $request['title'],
            'url' => $request['url'],
            'image_title' => $request['image_title'],
            'image_alt' => $request['image_alt'],
            'banner_type' => $request['banner_type'],
            'isActive' => $request['isActive'] ?? 0
        ]);
        if ($request->has('image')) {
            if (file_exists($banner->image)) {
                unlink($banner->image);
            }
            $banner->image = $request['image'];
        }
        $banner->update();
        return redirect(route('admin.banners.index'));
    }
    public function delete($id)
    {
        $banner = Banner::findOrFail($id);
        if (file_exists($banner->image)) {
           unlink($banner->image);
        }
        $banner->delete();

        return redirect(route('admin.banners.index'));
    }

    public function set_active($id)
    {
        $banner = Banner::findOrFail($id);
        $this->validate_count((new Request([])), $banner->banner_type);
        $banner->update(['isActive' => 1]);
        return redirect(route('admin.banners.index'));
    }

    public $limits = [
        'small' => 4,
        'medium' => 4,
        'large' => 4,
        'news' => 4
    ];
}
