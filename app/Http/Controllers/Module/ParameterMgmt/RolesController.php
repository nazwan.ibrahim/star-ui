<?php

namespace App\Http\Controllers\Module\ParameterMgmt;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use Carbon\Carbon;

class RolesController extends Controller
{
    public function mainList()
    {
        $userid = auth()->user()->id;

        return view('roles.ListRoles',compact('userid'));
    }

    public function createRole()
    {
        $userid = auth()->user()->id;

        return view('roles.addRoles',compact('userid'));
    }

    public function editRole()
    {
        $userid = auth()->user()->id;

        return view('roles.editRoles',compact('userid'));
    }
}