<?php

namespace App\Http\Controllers\Module\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use App\Models\Users;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Models\AccessLog;

class LoginController extends Controller
{


    protected $redirectTo = RouteServiceProvider::HOME;
    // use AuthenticatesUsers;

    //
    public function __construct()
    {
        $this->middleware('guest')->except([
            'logout'
        ]);
    }
    //Login form 
    public function loginPage()
    {
        return view('login.login');
    }

    public function authenticate(Request $request)
    {
        try {
            // Validate the request data
            $request->validate([
                'email' => 'required | email',
                'password' => 'required',
            ]);
        
            // Get the user credentials from the request
            $credentials = $request->only('email', 'password');
            $credentials['status'] = 1;

            // Attempt to log in the user
        if (Auth::attempt($credentials)) {

            $userAgent = $request->header('User-Agent');
            $browser = 'Unknown';

            if (strpos($userAgent, 'Chrome') !== false) {
                $browser = 'Chrome';
            } elseif (strpos($userAgent, 'Firefox') !== false) {
                $browser = 'Firefox';
            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                $browser = 'Safari';
            } elseif (strpos($userAgent, 'Edg') !== false) {
                $browser = 'Edge';
            } elseif (strpos($userAgent, 'MSIE') !== false) {
                $browser = 'Internet Explorer';
            } elseif (strpos($userAgent, 'Opera') !== false) {
                $browser = 'Opera';
            }

            AccessLog::create([
                'username' => auth()->user()->fullname ?? 'Guest',
                'activity' => 'Login',
                'activity_time' => now(),
                'ip_address' => $request->ip(),
                'browser' => $browser,
            ]);

            $userWithCompany = $request->user()->join('company', 'users.id_company', '=', 'company.id')
            ->select('users.*', 'company.name as company_name') // Add other columns as needed
            ->first();
        
            if (auth()->user()->user_type == '1'){
                    return redirect()->route('admin.dashboard');
                }
                elseif (auth()->user()->user_type == '2') {
                    return redirect()->route('admin.dashboard');
                } elseif (auth()->user()->user_type == '3') { //training partner- type 3
                    return redirect()->route('TP-training-schedule-history');
                } elseif (auth()->user()->user_type == '4') { //trainer  - type 4 
                    return redirect()->route('trainer-training-schedule-history');
                } else {
                    return redirect()->route('participant.dashboard');
                }
            } else {
                return redirect()->back()
                    ->with('error', 'User Credentials Invalid');
            }
        } catch (\Exception $e) {
            // Handle connection or other unexpected errors here
            return redirect()->back()
            ->with('error', 'An error occurred while processing your request.');        
        }
    }


    public function logout(Request $request): RedirectResponse
    {
        // $username = auth()->user()->id;
        $userAgent = $request->header('User-Agent');
        $browser = 'Unknown';

        if (strpos($userAgent, 'Chrome') !== false) {
            $browser = 'Chrome';
        } elseif (strpos($userAgent, 'Firefox') !== false) {
            $browser = 'Firefox';
        } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
            $browser = 'Safari';
        } elseif (strpos($userAgent, 'Edg') !== false) {
            $browser = 'Edge';
        } elseif (strpos($userAgent, 'MSIE') !== false) {
            $browser = 'Internet Explorer';
        } elseif (strpos($userAgent, 'Opera') !== false) {
            $browser = 'Opera';
        }

        AccessLog::create([
            'username' => $username ?? 'Guest',
            'activity' => 'Logout',
            'activity_time' => now(),
            'ip_address' => $request->ip(),
            'browser' => $browser,
        ]);

        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('login')
            ->withSuccess('You have logged out successfully!');;
    }      
}
