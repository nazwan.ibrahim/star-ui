<?php

namespace App\Http\Controllers\Module\Login;

use App\Http\Controllers\Controller;
use App\Models\PasswordCounter;
use Illuminate\Http\Request;
use App\Models\Users;
use Exception;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;

class ForgotPasswordController extends Controller
{
    //

    public function forgotPasswordForm(){
        return view('auth.forgotpassword');
      }

    public function submitForgotPasswordForm(Request $request)
    { 
      try{
        $request->validate([
            'email' => 'required|email|exists:users',
        ]);
        $token = Str::random(64);

        DB::table('password_resets')->insert([
            'email' => $request->email, 
            'token' => $token, 
            'created_at' => now(),
            'expires_at' => now()->addMinutes(5), // Adjusted to expire in 15 minutes
          ]);

        Mail::send('auth.forgotpasswordmail', ['token' => $token, 'email' => $request->email], function ($message) use ($request) {
            $message->to($request->email);
            $message->subject('STAR Reset Password');
        });

        return back()->with('success', 'We have e-mailed your password reset link!');
      }catch(Exception $ex){
        return back()->with('error', 'Invalid email!');
      }
    
    } 
    
    public function showResetPasswordForm($token,$encoded_email) { 
      $email = urldecode($encoded_email);

      // Check if the token is expired
      $passwordReset = DB::table('password_resets')
        ->where('token', $token)
        ->first();

    if (!$passwordReset || now() >$passwordReset->expires_at) {
        return redirect()->route('forgot-password')->with('error', 'The password reset link is invalid or has expired. Please request a new one.');
      }else{
      return view('auth.resetpassword')->with(['token' => $token, 'email' => $email]);
    }

  }

    public function submitResetPasswordForm(Request $request)
      {
          $request->validate([
              'email' => 'required|email|exists:users',
              'newpassword' => 'required|string|min:8',
              'confirmpassword' => 'required'
          ]);

          $userd = Users::where('email', $request->email)->first();

          if (Hash::check($request->newpassword, $userd->password)) {
            return redirect()->back()->with('error', 'New password cannot be the same as previous password');
        }

          $previousPasswords = PasswordCounter::where('email', $request->email)->get();

          foreach ($previousPasswords as $previousPassword) {
              if (Hash::check($request->newpassword, $previousPassword->password)) {
                  return redirect()->back()->with('error', 'New password cannot be the same as a previous password');
              }
          }

          $updatePassword = DB::table('password_resets')
                              ->where([
                                'email' => $request->email, 
                                'token' => $request->token,
                              ])
                              ->first();

            if (!$updatePassword || now() > $updatePassword->expires_at) {
              return redirect()->route('forgot-password')->with('error', 'Invalid or expired token!');
          }
            $user = Users::where('email', $request->email)
              ->update(['password' => bcrypt($request->newpassword),'password_confirmation' => bcrypt($request->confirmpassword)]);
                
              if ($user) {


              DB::table('password_resets')->where('email', $request->email)->delete();

              PasswordCounter::insert([
                'email' => $request->email, 
                'password' => bcrypt($request->newpassword),
                'confirm_password' => bcrypt($request->confirmpassword),
                'created_at' => now(),
              ]);

              return redirect()->route('reset-success')->with('success', 'Your password has been changed!');
          } else {
              return redirect()->route('forgot-password')->with('error', 'Failed to reset password');
          }
      }
    
      public function forgotUsernamePage(){
        $pageConfigs = ['bodyCustomClass'=> 'bg-full-screen-image'];
        return view('auth.forgotusername',['pageConfigs' => $pageConfigs]);
      }

      public function resetsuccessPage(){
        $pageConfigs = ['bodyCustomClass'=> 'bg-full-screen-image'];
        return view('auth.resetsuccess',['pageConfigs' => $pageConfigs]);
      }

      public function usernameSentPage(){
        $pageConfigs = ['bodyCustomClass'=> 'bg-full-screen-image'];
        return view('auth.emailusername',['pageConfigs' => $pageConfigs]);
      }
      
}
