<?php

namespace App\Http\Controllers\Module\EventMgmt;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventMgmtController extends Controller
{
    //
    public function UpcomingEvent()
    {
        $userid = auth()->user()->id;

        return view('eventMgmt.upcomingEvent' ,compact('userid'));

    }

    public function applyTrainingList()
    {
        $userid = auth()->user()->id;

        return view('eventMgmt.applyTrainingEvent' ,compact('userid'));
    }
}
