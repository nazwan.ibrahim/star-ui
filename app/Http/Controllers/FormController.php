<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormController extends Controller
{
    public function submitForm(Request $request)
    {
        $data = $request->validate([
            'trainingOption' => 'required',
            'modeOfTraining' => 'required',
            'courseCategory' => 'required',
            'trainingType' => 'required',
            'participantLimit' => 'required|string|max:255',
            'trainingStartDate' => 'required|date',
            'trainingEndDate' => 'required|date',
            'trainingStartTime' => 'required',
            'trainingEndTime' => 'required',
            'advertisementStartDate' => 'required|date',
            'advertisementEndDate' => 'required|date',
            'fee' => 'required|numeric',
            'hrdfClaimable' => 'required',
            'supportingDocumentDescription' => 'required',
            'trainingDescription' => 'required',
            'is_resit' => 'boolean',
            'trainingCourse' => 'required|exists:training_course,id_training_course',
            'state' => 'required',
            'venue' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'trainingCode' => 'required|string|max:255',
            /* 'isPaid' => 'boolean', */
        ]);

        $stateToRegion = [
            'Perlis' => 'Northern Region',
            'Kedah' => 'Northern Region',
            'Pulau Pinang' => 'Northern Region',
            'Perak' => 'Northern Region',
            'Selangor' => 'Central Region',
            'Kuala Lumpur' => 'Central Region',
            'Labuan' => 'Central Region',
            'Putrajaya' => 'Central Region',
            'Kelantan' => 'Eastern Region',
            'Terengganu' => 'Eastern Region',
            'Pahang' => 'Eastern Region',
            'Negeri Sembilan' => 'Southern Region',
            'Melaka' => 'Southern Region',
            'Johor' => 'Southern Region',
            'Sabah' => 'East Malaysia',
            'Sarawak' => 'East Malaysia',
        ];
    
        $selectedState = $data['state'];
    
        $region = $stateToRegion[$selectedState];

        /* $data['isPaid'] = $request->has('isPaid') ? 1 : 0; */
        $data['is_resit'] = $request->has('resitCheckbox') ? 1 : 0;

        DB::table('trainings')->insert([
            'id_training_option' => $data['trainingOption'],
            'training_mode' => $data['modeOfTraining'],
            'id_course_category' => $data['courseCategory'],
            'id_training_type' => $data['trainingType'],
            'total_participant' => $data['participantLimit'],
            'date_start' => $data['trainingStartDate'],
            'date_end' => $data['trainingEndDate'],
            'start_time' => $data['trainingStartTime'],
            'end_time' => $data['trainingEndTime'],
            'advert_start' => $data['advertisementStartDate'],
            'advert_end' => $data['advertisementEndDate'],
            'fee' => $data['fee'],
            'hrdf_claim' => $data['hrdfClaimable'],
            'document_description' => $data['supportingDocumentDescription'],
            'training_description' => $data['trainingDescription'],
            'is_resit' => $data['is_resit'],
            'id_training_course' => $data['trainingCourse'],
            'state' => $selectedState,
            'region' => $region,
            'venue' => $data['venue'],
            'venue_address' => $data['address'],
            'training_code' => $data['trainingCode'],
            /* 'is_paid' => $data['isPaid'], */
            'attendance_status' => 0,
            'results_status' => 0,
            'report_status' => 0,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        return redirect()->route('training-management');
    }
}
