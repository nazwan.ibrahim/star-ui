<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;



class UserRegistrationAPIController extends Controller
{
    public function login(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
            [
                'email' => 'required|email',
                'password' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            if(!Auth::attempt($request->only(['email', 'password']))){
                return response()->json([
                    'status' => false,
                    'message' => 'Email & Password does not match with our record.',
                ], 401);
            }

            $users = Users::where('email', $request->email)->first();

            return response()->json([
                'status' => true,
                'message' => 'User Logged In Successfully',
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return response()->json(['message' => 'You are successfully logged out'], 200);
    }

    //self-registration participant
    public function registerparticipant(Request $request)
    {   
        try {
        $validateUser = Validator::make($request->all(), 
            [
                'fullname' => 'required',
                'ic_no' => 'required',
                'email' => 'required|email',
                'password' => [
                    'required',
                    'string',
                    Password::min(8)
                        ->mixedCase()
                        ->numbers()
                        ->symbols()
                        ->uncompromised(),
                    'confirmed'
                ],
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }


        $user = Users::create([
            'fullname' => $request['fullname'],
            'ic_no' => $request['ic_no'],
            'phone_no' => $request['phone_no'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'password_confirmation'=> bcrypt($request['confirmpassword']),
            'user_type'=> 2,
            'terms' => $request['terms'] ? '1' : '0'
        ]);
        
        return response()->json([
            'status' => true,
            'user' => $user,
            'message' => 'User Created Successfully',
        ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }

    }

    //Training Partner Registration
    public function adminregistration(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), 
                [
                    'fullname' => 'required',
                    'ic_no' => 'required',
                    'email' => 'required|email',
                    'password' => [
                        'required',
                        'string',
                        Password::min(8)
                            ->mixedCase()
                            ->numbers()
                            ->symbols()
                            ->uncompromised(),
                        'confirmed'
                    ],
                ]);
    
                if($validateUser->fails()){
                    return response()->json([
                        'status' => false,
                        'message' => 'validation error',
                        'errors' => $validateUser->errors()
                    ], 401);
                }
    
    
            $user = Users::create([
                'fullname' => $request['fullname'],
                'ic_no' => $request['ic_no'],
                'phone_no' => $request['phone_no'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'password_confirmation'=> bcrypt($request['confirmpassword']),
                'user_type'=> $request['user_type'],
                'terms' => $request['terms'] ? '1' : '0'
            ]);
            
            return response()->json([
                'status' => true,
                'user' => $user,
                'message' => 'User Created Successfully',
            ], 200);
    
            } catch (\Throwable $th) {
                return response()->json([
                    'status' => false,
                    'message' => $th->getMessage()
                ], 500);
        }
    
    }

    public function detailuser(Request $request, $userid)
    {
        $viewUser = DB::table('users')
        ->where('users.id', '=', $userid)
        ->leftjoin('state AS user_state', 'user_state.id', '=', 'users.id_state')
        ->leftjoin('country AS user_country', 'user_country.id', '=', 'users.id_country')
        ->leftjoin('education', 'education.id_users', '=', 'users.id')
        ->leftjoin('company', 'company.id', '=', 'users.id_company')
        ->leftjoin('state AS company_state', 'company_state.id', '=', 'company.id_state')
        ->leftjoin('country AS company_country', 'company_country.id', '=', 'company.id_country')
        ->leftjoin('list_loa as loa', 'loa.id_users','=','users.id')
        ->leftjoin('list_mou as mou', 'mou.id_users','=','users.id')
        ->leftjoin('list_trainer as trainer', 'trainer.id_users','=','users.id')
        ->select(
            'users.fullname',
            'users.ic_no',
            'users.gender',
            'users.phone_no',
            'users.address_1',
            'users.address_2',
            'users.postcode',
            'users.email',
            'users.position',
            'users.bumi_status',
            'user_state.name AS user_statename',
            'user_state.id AS user_state_id',
            'user_country.name AS user_countryname',
            'user_country.id AS user_country_id',
            'education.qualification',
            'education.education_level',
            'company.name AS companyname',
            'company.phone_no AS companyphone_no',
            'company.fax_no',
            'company.postcode',
            'company.address_1 AS company_address_1',
            'company.address_2 AS company_address_2',
            'company_state.name as company_state',
            'company_country.name AS company_countryname',
            'trainer.fullname as trainer_name',
            'trainer.email as trainer_email',
            'trainer.position as trainer_position',
            'trainer.role as trainer_role',
            'trainer.status as trainer_status'
        )
        ->get();
        return response()->json([
            'status' => 'success',
            'viewUser' => $viewUser,
        ], 200);
     
    }

    public function updateuser(Request $request,$userid)
    {
        try {
            $validateUser = Validator::make($request->all(), 
                [
                    'fullname' => 'required',
                    'ic_no' => 'required',
                    'email' => 'required|email',
                    'password' => [
                        'required',
                        'string',
                        Password::min(8)
                            ->mixedCase()
                            ->numbers()
                            ->symbols()
                            ->uncompromised(),
                        'confirmed'
                    ],
                ]);
    
                if($validateUser->fails()){
                    return response()->json([
                        'status' => false,
                        'message' => 'validation error',
                        'errors' => $validateUser->errors()
                    ], 401);
                }
    
    
                $updateUser = DB::table('users')
                ->where('users.id', '=', $request->userid)
                ->update([
                'fullname' => $request['fullname'],
                'ic_no' => $request['ic_no'],
                'phone_no' => $request['phone_no'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'password_confirmation'=> bcrypt($request['confirmpassword']),
                'user_type'=> $request['user_type'],
                'terms' => $request['terms'] ? '1' : '0'
            ]);

            $updateEducation = DB::table('education')
            ->update([
                'qualification' => $request->Qualification,
                'education_level' => $request->EducationLevel,
               
            ]);

            $updateCompany = DB::table('company')
            ->update([
                'name' => $request->CompanyName,
                'address_1' => $request->CompanyAddressLine1,
                'address_2' => $request->CompanyAddressLine2,
             
            ]);
       
            
            return response()->json([
                'status' => true,
                'updateUser' => $updateUser,
                'updateEducation' => $updateEducation,
                'updateCompany' => $updateCompany,
                'message' => 'User Updated Successfully',
            ], 200);
    
            } catch (\Throwable $th) {
                return response()->json([
                    'status' => false,
                    'message' => $th->getMessage()
                ], 500);
        }
    }

    public function delete($id)
    {
        $user = Users::find($id)->delete(); 
        if($user){

            return response()->json([
                'status' => 'success',
                'message'=>'Successfully Deleted'],200);
        }
        else{
            return response()->json([
                'status' => 'success',
                'message'=>'Failed to Delete'],400);
        }
    }
}
