<?php

namespace App\Http\Controllers\API\TrainingManagement;

use App\Http\Controllers\Controller;
use App\Models\AuditTrail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Training;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class TrainingManagementAPIController extends Controller
{

    public function getSelectedCourseReTypes(Request $request)
    {
        $selectedCourseId = $request->input('courseId');

        $selectedReTypes = DB::table('training_course')
            ->join('course_re_type', 'course_re_type.id_training_course', '=', 'training_course.id_training_course')
            ->where('training_course.id_training_course', $selectedCourseId)
            ->pluck('course_re_type.id_re_type')
            ->toArray();

        return response()->json($selectedReTypes);
    }
    
    public function trainingManagementList(Request $request)
    {
        try{
            $trainingList = DB::table('trainings as t')
            ->leftjoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
            ->leftjoin('course_category as c', 'tc.id_course_category', '=', 'c.id')
            ->leftjoin('training_type as tt', 'tc.id_training_type', '=', 'tt.id')
            ->leftjoin('training_option as to', 'tc.id_training_option', '=', 'to.id')
            ->whereNull('t.deleted_at')
            ->select(
                DB::raw('t.date_start, DATE_FORMAT(t.date_start,  "%d/%m/%Y") as date_start'),
                DB::raw('t.date_end, DATE_FORMAT(t.date_end,  "%d/%m/%Y") as date_end'),
                't.venue',
                't.fee',
                'tc.course_name',
                'tt.name as training_type',
                't.id_approval_status',
                't.batch_no_full',
                't.id',
                't.deleted_at',
                'c.course_name as course_category',
            )
            ->whereIn('t.id_approval_status', [1,3,4]) 
            ->orderby('t.date_start','asc')
            ->get();

                Log::info('Successfully fetched training list.');
                return response()->json([
                    'status' => 'success',
                    'upcomingList' => $trainingList,
                ], 200);
            
        } catch (\Exception $e) {
            Log::error('Error fetching training list: ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }
    }

   

    public function trainingApplication($id)
    {
        $re_types = DB::table('re_type')
            ->get();
        $feetypes = DB::table('fee_type')
            ->get();
        $courseList = DB::table('training_course')
            ->where('training_course.id_training_course', '=', $id)
            ->join('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->join('training_option', 'training_option.id', '=', 'training_course.id_training_option')
            ->join('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->select(
                'training_course.id_training_course',
                'course_category.id as course_id',
                'course_category.course_name as category_name',
                'training_course.course_name as course_name',
                'new_fee',
                'new_fee_int',
                'training_option.id as option_id',
                'training_option.option as option_name',
                'training_type.id as type_id',
                'training_type.name as type_name',
            )
            ->first();
        $trainingMode = DB::table('training_mode')
            ->select(
                'id as mode_id',
                'mode as mode_name',
            )
            ->get();
        $trainer = DB::table('trainer')
        ->leftJoin('training_trainer', 'training_trainer.id_trainer', '=', 'trainer.id')
        ->leftJoin('users', 'trainer.id_users', '=', 'users.id')
        ->leftJoin('company', 'trainer.id_company', '=', 'company.id')
        ->leftJoin('training_partner_course', 'training_partner_course.id_company', '=', 'company.id')
        ->where('training_partner_course.id_training_course', '=', 1)
        ->select('users.fullname')
        ->get();
        $selectedReTypes = DB::table('course_re_type')
            ->where('id_training_course', '=', $id)
            ->pluck('id_re_type')
            ->toArray();
        $resitfees = DB::table('resit_fee')
            ->join('training_course', 'training_course.id_training_course', '=', 'resit_fee.id_training_course')
            ->join('fee_type', 'fee_type.id_fee', '=', 'resit_fee.id_fee')
            ->where('training_course.id_training_course', '=', $id)
            ->select(
                'fee_type.fee_name',
                'resit_fee.amount',
                'resit_fee.fee_category',
            )
            ->get();

        Log::info('Successfully fetched training list.');

        return response()->json([
            'status' => 'success',
            'resitfees' => $resitfees,
            'courseList' => $courseList,
            'selectedReTypes' => $selectedReTypes,
            'trainer' => $trainer,
            'trainingMode' => $trainingMode,
            'feetypes' => $feetypes,
            're_types' => $re_types,
        ], 200);
    }

    public function trainingManagementList2(Request $request)
    {
        $currentDate = Carbon::now();

        try{
            $trainingList = DB::table('trainings as t')
            ->leftjoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
            ->leftjoin('course_category as c', 'tc.id_course_category', '=', 'c.id')
            ->leftjoin('training_type as tt', 'tc.id_training_type', '=', 'tt.id')
            ->whereNull('t.deleted_at')
            ->whereNotNull('t.date_start')
            ->whereNotNull('t.date_end')
            ->select(
                DB::raw('t.date_start, DATE_FORMAT(t.date_start,  "%d/%m/%Y") as date_start'),
                DB::raw('t.date_end, DATE_FORMAT(t.date_end,  "%d/%m/%Y") as date_end'),
                't.venue',
                't.fee',
                'tc.course_name',
                'tt.name as training_type',
                't.id_approval_status',
                't.batch_no_full',
                't.id',
                't.deleted_at',
                'c.course_name as course_category',
            )
            ->whereIn('t.id_approval_status', [2])
            ->where('t.date_end', '>', $currentDate)
            ->orderby('t.date_start','asc')
            ->get();
           
                Log::info('Successfully fetched training list.');
                return response()->json([
                    'status' => 'success',
                    'trainingList' => $trainingList,
                ], 200);
            
        } catch (\Exception $e) {
            Log::error('Error fetching training list: ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

    public function softDeleteTraining(Request $request, $id)
    {
        try {
            DB::table('trainings')
                ->where('id', $id)
                ->update([
                    'deleted_at' => now(),
                ]);

                $userAgent = $request->header('User-Agent');
                $browser = 'Unknown';

                if (strpos($userAgent, 'Chrome') !== false) {
                    $browser = 'Chrome';
                } elseif (strpos($userAgent, 'Firefox') !== false) {
                    $browser = 'Firefox';
                } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                    $browser = 'Safari';
                } elseif (strpos($userAgent, 'Edg') !== false) {
                    $browser = 'Edge';
                } elseif (strpos($userAgent, 'MSIE') !== false) {
                    $browser = 'Internet Explorer';
                } elseif (strpos($userAgent, 'Opera') !== false) {
                    $browser = 'Opera';
                }

                AuditTrail::create([
                    'username' => auth()->user()->fullname ?? 'Guest',
                    'activity' => 'Deleted training  ' . $id,
                    'activity_time' => now(),
                    'ip_address' => $request->ip(),
                    'browser' => $browser,
                ]);

            return redirect()->route('TrainingManagement')->with([
                'status' => 'success',
                'message' => 'Training record soft deleted successfully.',
            ], 200);
        } catch (\Exception $e) {
            Log::error('Error soft deleting training record: ' . $e->getMessage());
            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

    public function trainingManagementListAll(Request $request){
                
        $trainingalllist = DB::table('trainings')
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->leftjoin('training_partner_course','training_partner_course.id_training_course','=','training_course.id_training_course')
        ->leftjoin('company','training_partner_course.id_company','=','company.id')
        ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        ->leftjoin('participant_examresult','participant_examresult.id_training_course','=','trainings.id_training_course')
        ->select('trainings.id','trainings.id_training_course','participant_examresult.endorsed_status',
        'company.name as company_name',
        'course_category.course_name as category_name','training_course.course_name as training_name',
        'training_type.name as type_name','trainings.batch_no','trainings.batch_no_full',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
        'trainings.id_training_status as status',
        )
        ->whereIn('trainings.id_approval_status', [2])
        ->groupBy('trainings.id')
        ->orderby('trainings.id','desc')
        ->get();

        return response()->json([
            'status' => 'success',
            'trainingalllist' => $trainingalllist,
        ], 200);
        // }
    }

    public function trainingManagementListAllTP(Request $request, $userid){
        $userInfo = DB::table('company')
                ->leftjoin('users','company.id','=','users.id_company')
                ->where('users.id', '=',$userid)
                ->select('users.id', 'users.fullname','company.id as companyid')
                ->first();

        $trainingtplist = DB::table('trainings')
        ->where('training_partner_course.id_company','=', $userInfo->companyid)
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->leftjoin('training_partner_course','training_partner_course.id_training_course','=','training_course.id_training_course')
        ->leftjoin('company','training_partner_course.id_company','=','company.id')
        ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        ->select('trainings.id','trainings.id_training_course',
        'company.name as company_name',
        'course_category.course_name as category_name','training_course.course_name as training_name',
        'trainings.batch_no','trainings.batch_no_full',
        'training_type.name as type_name','trainings.id_approval_status','trainings.endorsed_status as endorsed_training',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
        'trainings.id_training_status as status',
        )
        ->whereIn('trainings.id_approval_status', [2])
        ->groupBy('trainings.id')
         ->orderby('trainings.id','desc')
      //  ->groupby('participant_examresult.id_training_course','participant_examresult.batch_no_full')
        ->get();
       
            return response()->json([
                'status' => 'success',
                'trainingtplist' => $trainingtplist,
            ], 200);
        
    }

    public function trainingManagementListTP(Request $request, $userid){
        $userInfo = DB::table('company')
                ->leftjoin('users','company.id','=','users.id_company')
                ->where('users.id', '=',$userid)
                ->select('users.id', 'users.fullname','company.id as companyid')
                ->first();

          
        $trainingtplist = DB::table('trainings')
        ->where('company.id','=', $userInfo->companyid)
        ->leftjoin('company','trainings.id_company','=','company.id')
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        ->select('trainings.id','trainings.id_training_course',
        'company.name as company_name',
        'course_category.course_name as category_name','training_course.course_name as training_name',
        'trainings.batch_no','trainings.batch_no_full',
        'training_type.name as type_name','trainings.id_approval_status','trainings.endorsed_status as endorsed_training',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
        'trainings.id_training_status as status',
        )
        ->whereIn('trainings.id_approval_status', [1, 3,4])
        //  ->orderby('trainings.id','desc')
         ->orderby('trainings.date_start','asc')
      //  ->groupby('participant_examresult.id_training_course','participant_examresult.batch_no_full')
        ->get();
       
            return response()->json([
                'status' => 'success',
                'trainingtplist' => $trainingtplist,
            ], 200);
        
    }

    public function trainingManagementListTP2(Request $request, $userid){
        $userInfo = DB::table('company')
                ->leftjoin('users','company.id','=','users.id_company')
                ->where('users.id', '=',$userid)
                ->select('users.id', 'users.fullname','company.id as companyid')
                ->first();

        $trainingtplist = DB::table('trainings')
        ->where('company.id','=', $userInfo->companyid)
        ->leftjoin('company','trainings.id_company','=','company.id')
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        ->select('trainings.id','trainings.id_training_course',
        'company.name as company_name',
        'course_category.course_name as category_name','training_course.course_name as training_name',
        'trainings.batch_no','trainings.batch_no_full',
        'training_type.name as type_name','trainings.id_approval_status','trainings.endorsed_status as endorsed_training',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
        'trainings.id_training_status as status',
        )
        ->whereIn('trainings.id_approval_status', [2])
        ->orderby('trainings.id','desc')
        //  ->groupby('participant_examresult.id_training_course','participant_examresult.batch_no_full')
        ->get();
       
            return response()->json([
                'status' => 'success',
                'trainingtplist' => $trainingtplist,
            ], 200);
        
    }


    Public function trainingProvider(Request $request, $batch_full,$id){
        //var_dump($batch_full,$ID);
                $trainingDetails = DB::table('trainings')
                ->where('trainings.id','=',$id)
                ->where('trainings.batch_no_full','=',$batch_full)
                ->leftJoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
                ->select('trainings.id','trainings.batch_no','trainings.batch_no_full','training_course.id_training_course','training_course.course_name as training_name','trainings.venue','trainings.batch_no_full',
                DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
                DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'))
            
                ->get();
        
                return response()->json([
                    'status' => 'success',
                    'trainingDetails' => $trainingDetails,
                ], 200);
        
            }

    public function trainingManagementListParticipantTP(Request $request,$id){
    // public function trainingManagementListParticipantTP(Request $request,$id){
        // $userInfo = DB::table('company')
        //         ->leftjoin('users','company.id','=','users.id_company')
        //         ->where('users.id', '=',$userid)
        //         ->select('users.id', 'users.fullname','company.id as companyid')
        //         ->first();
        // dd($id);
            //var_dump($userInfo->companyid);
        $trainingparticipantlist = DB::table('training_participant')
        ->leftjoin('trainings','training_participant.id_training','=','trainings.id')
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->leftjoin('training_partner_course','training_partner_course.id_training_course','=','training_course.id_training_course')
        ->leftjoin('users','training_participant.id_users','=','users.id')
        ->where('trainings.id','=',$id)
        ->select(
        'users.fullname','users.email','users.phone_no','training_participant.id','users.ic_no','training_participant.id_approval_status',
        'status_payment'
        )
        ->groupBy('users.id')
        ->orderby('users.fullname','desc')
        ->get();

      
            return response()->json([
                'status' => 'success',
                'trainingparticipantlist' => $trainingparticipantlist,
            ], 200);
        
    }

    public function trainingManagementListParticipantPayTP(Request $request,$id){
        // public function trainingManagementListParticipantTP(Request $request,$id){
            // $userInfo = DB::table('company')
            //         ->leftjoin('users','company.id','=','users.id_company')
            //         ->where('users.id', '=',$userid)
            //         ->select('users.id', 'users.fullname','company.id as companyid')
            //         ->first();
            // dd($id);
                //var_dump($userInfo->companyid);
            $trainingparticipantpaylist = DB::table('training_participant')
            ->leftjoin('trainings','training_participant.id_training','=','trainings.id')
            ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
            ->leftjoin('training_partner_course','training_partner_course.id_training_course','=','training_course.id_training_course')
            ->leftjoin('company','training_partner_course.id_company','=','company.id')
            ->leftjoin('users','training_participant.id_users','=','users.id')
            ->where('trainings.id','=',$id)
            ->select(
            'users.fullname','users.email','users.phone_no','training_participant.id','users.ic_no','training_participant.id_approval_status',
            'training_participant.status_payment'
            )
            ->whereIn('trainings.id_approval_status', [2])
            ->whereIn('training_participant.id_approval_status', [2])
            ->groupBy('users.id')
            ->orderby('users.fullname','desc')
            ->get();
    
          
                return response()->json([
                    'status' => 'success',
                    'trainingparticipantpaylist' => $trainingparticipantpaylist,
                ], 200);
            
        }
    


    public function trainingManagementTPAttendance(Request $request,$ID,$batch_no){
        
        $listparticipant = DB::table('training_participant')
    ->where('trainings.id_training_course', '=', $ID)
    ->where('trainings.batch_no', '=', $batch_no)
    ->leftJoin('trainings', 'trainings.id', '=', 'training_participant.id_training')
    ->leftJoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
    ->leftJoin('users', 'users.id', '=', 'training_participant.id_users')
    ->leftjoin('participant_attendance','training_participant.id','=','participant_attendance.id_training_participant')
    ->select('users.fullname','participant_attendance.id as participant_attendance_id','training_participant.id', 'participant_attendance.attend_check','participant_attendance.remark',
   DB::raw('participant_attendance.date_attend, DATE_FORMAT(participant_attendance.date_attend,  "%d/%m/%Y") as date_attend'),
   DB::raw('participant_attendance.time_check, DATE_FORMAT(participant_attendance.time_check,  "%H:%i:%s") as time_check'),
   DB::raw('participant_attendance.time_out, DATE_FORMAT(participant_attendance.time_out,  "%H:%i:%s") as time_out'),
   DB::raw('DATEDIFF(trainings.date_end, trainings.date_start) + 1 as total_days'),
   //DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
  )

    ->get();
        
        // $listparticipant = DB::table('trainings')
        // ->where('trainings.id', '=', $id)
        // ->leftjoin('users','trainings.id_company','=','users.id')
        // ->leftjoin('company','users.id_company','=','company.id')
        // ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        // ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        // ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        // ->leftjoin('training_participant','training_participant.id_training','=','trainings.id')
        // ->leftjoin('users as userA','training_participant.id_users','=','userA.id')
        // ->leftjoin('participant_attendance','training_participant.id','=','participant_attendance.id_training_participant')
        // ->select('trainings.id','trainings.training_name',
        // 'company.name as company_name',
        // 'course_category.course_name as category_name',
        // 'training_type.name as type_name',
        // DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        // DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
        // 'trainings.training_status',
        // 'userA.fullname','participant_attendance.attend_check'
        // )
        // ->get();

       
            return response()->json([
                'status' => 'success',
                'listparticipant' => $listparticipant,
            ], 200);
        
    

    }

    public function trainingPostpone(Request $request,$id)
    {

        try {
            $userInfo = DB::table('users')
                ->where('users.id', '=',$request->UserID)
                ->select('users.id', 'users.fullname')
                ->get();
        
            $trainingPostpone = DB::table('trainings')
                ->where('trainings.id', '=', $id)
                ->leftJoin('users', 'trainings.id_company', '=', 'users.id')
                ->leftJoin('company', 'users.id_company', '=', 'company.id')
                ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
                ->leftJoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
                ->leftJoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
                ->select('trainings.id', 'trainings.training_name',
                    'company.name as company_name',
                    'course_category.course_name as category_name',
                    'training_type.name as type_name',
                    DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
                    DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
                    'trainings.training_status'
                )
                ->get();
        
           
                return response()->json([
                    'status' => 'success',
                    'trainingPostpone' => $trainingPostpone,
                    'userInfo' => $userInfo,
                ], 200);
            
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'An error occurred: ' . $e->getMessage(),
            ], 500);
        }
        
    }

    public function updateTrainingPostpone(Request $request, $id)
    {
        $updateTrainingPostpone = DB::table('trainings')
        ->where('id', $id)
        ->update([
            'date_start' => $request->input('date_start'),
            'date_end' => $request->input('date_end'),
            'remarks' => $request->input('remarks'),
            'updated_by' => $request->userid,
            'updated_at' => Carbon::now(),
            'deleted_by' => $request->userid,
            'deleted_at' => Carbon::now(),
        
        ]);
        return response()->json([
            'updateTrainingPostpone' => $updateTrainingPostpone,
        ], 200);
    }



    Public function participantReportList(Request $request,$ID){
        //  var_dump($ID);

        $plist = DB::table('training_participant')
        ->where('training_participant.id_training', '=', $ID)
        ->leftJoin('trainings', 'trainings.id', '=', 'training_participant.id_training')
        ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
        ->leftJoin('users', 'users.id', '=', 'training_participant.id_users')
        ->select('training_participant.id_training', 'training_participant.id_users','trainings.id','training_course.course_name',
        'trainings.batch_no',
        'users.fullname',
            DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
            DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
            
        )
        ->get();
                   

                    //   $exam = DB::select("
                    //   SELECT DISTINCT 
                    //       exam_practical.id_participant_examresult,
                    //       participant_examresult.id,
                    //       participant_examresult.exam_type,
                    //       users.fullname,
                    //       exam_practical.id_training,
                    //       exam_practical.exam_1 as part_A,
                    //       exam_practical.exam_2 as part_B,
                    //       exam_practical.exam_3 as part_C,
                    //       exam_practical.total_mark as total_mark_practical,
                    //       exam_practical.result_status as result_status_practical,
                    //       exam_theory.exam_1 as part_A_theory,
                    //       exam_theory.exam_2 as part_B_theory,
                    //       exam_theory.exam_3 as part_C_theory,
                    //       exam_theory.total_mark as total_mark_theory,
                    //       exam_theory.result_status as result_status_theory,
                    //       participant_examresult.final_verdict_status,
                    //       participant_examresult.batch_no,
                    //       participant_examresult.endorsed_status
                    //   FROM STAR.exam_practical
                    //   LEFT JOIN STAR.exam_theory ON 
                    //       exam_theory.id_participant_examresult = exam_practical.id_participant_examresult
                    //       AND exam_practical.id_training = exam_theory.id_training
                    //   LEFT JOIN STAR.participant_examresult ON 
                    //       participant_examresult.id_training_participant = exam_practical.id_participant_examresult
                    //       AND participant_examresult.id_training = exam_practical.id_training
                    //   LEFT JOIN STAR.users ON users.id = exam_practical.id_participant_examresult
                    //   WHERE participant_examresult.batch_no = $ID);

                return response()->json([
                      'status' => 'success',
                      'participantlist' => $plist,
                  ], 200);


              
            
        

  }



  Public function participantReportDetails(Request $request,$batch_no,$ID,){
    
    $course =   DB::table('trainings')
    ->where('trainings.batch_no_full', '=', $batch_no)
    ->where('trainings.id_training_course', '=', $ID)
    ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
    ->select('trainings.id','training_course.course_name as course_name','trainings.batch_no_full'
    
    )
    ->get();


    $provider = DB::table('trainings')
    //->where('users.id', '=', $ID)
    ->where('trainings.batch_no_full', '=', $batch_no)
    ->where('trainings.id_training_course', '=', $ID)
    ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
    ->leftJoin('company','company.id', '=','trainings.id_company')
    ->select('trainings.id','training_course.course_name as course_name',
    'trainings.batch_no','trainings.venue_address','trainings.training_code',
    'company.name as providername',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
        
    )
    ->get();


    $report = DB::table('trainings')
    ->where('trainings.id_training_course', '=', $ID)
    ->where('trainings.batch_no_full', '=', $batch_no)
    ->leftJoin('training_course','training_course.id_training_course', '=', 'trainings.id_training_course')
    ->leftJoin('company','company.id','=', 'trainings.id_company')
    ->leftJoin('trainer','trainer.id_company', '=', 'company.id')
    ->leftJoin('training_trainer','training_trainer.id_trainer', '=', 'trainer.id')
    ->leftJoin('users','users.id', '=', 'trainer.id_users')
    ->select('trainings.id',
    'training_course.course_name AS training_name',
    'trainings.batch_no',
    'trainings.training_code',
    'trainings.training_description',
    'trainings.description',
    'users.fullname AS trainername',
    'training_trainer.id_trainer',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),    
    )
    ->groupby('training_trainer.id_trainer')
    ->get();
               

            return response()->json([
                  'status' => 'success',
                  'provider' => $provider,
                  'course' => $course,
                  'report' => $report,
                //   'assessment' => $assessment,
                //   'picIC' => $picIC
              ], 200);

}




public function ReportDetails(Request $request,$batch_no,$ID){

    $myreport = DB::table('training_report')
    ->where('training_report.id_training', '=',$ID )
    ->where('training_report.batch_no', '=',$batch_no )
    ->leftjoin('trainings','trainings.id','=','training_report.id_training')
    // ->leftJoin('trainer','trainer.id_company', '=', 'company.id')
    // ->leftJoin('training_trainer','training_trainer.id_trainer', '=', 'trainer.id')
    ->select('training_report.provider_name','training_report.venue','training_report.training_name','training_report.training_code',
    'training_report.competent_trainer','training_report.competent_trainer','training_report.synopsis','training_report.introduction',
    'training_report.prepared_by','training_report.conclusion',
    'training_report.result_upload_path',
    'training_report.result_upload_name',
    'training_report.result_upload_format',
    'training_report.report_upload_path',
    'training_report.report_upload_name',
    'training_report.report_upload_format',
    'training_report.payment_upload_path',
    'training_report.payment_upload_name',
    'training_report.payment_upload_format',
    'training_report.supportive_activity_path',
    'training_report.supportive_activity_name',
    'training_report.supportive_activity_format',
    'training_report.batch_no_full',
    DB::raw('training_report.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
    DB::raw('training_report.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
   
   //DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
  )

    ->get();


    $DoneEndorsed = DB::table('participant_examresult')
    ->where('participant_examresult.batch_no', '=',$batch_no )
    ->where('participant_examresult.id_training_course', '=', $ID)
    ->leftjoin('users','users.id','=','participant_examresult.endorsed_by')
    ->leftjoin('salutation','salutation.id','=','users.id_salutation')
    ->select('users.fullname','salutation.salutation',
    DB::raw('participant_examresult.endorsed_date, DATE_FORMAT(participant_examresult.endorsed_date,  "%d/%m/%Y") as endorsed_date'))
    ->first();


    return response()->json([
        'status' => 'success',
        'myreport' => $myreport,
        'DoneEndorsed' => $DoneEndorsed,
    ], 200);

}


public function adminAttendanceList(Request $request,$batch_no,$ID){

    $examlist = DB::table('training_participant')
    ->where('trainings.id_training_course', '=', $ID)
    ->where('trainings.batch_no_full', '=', $batch_no)
    ->where('training_participant.id_approval_status', '=',2)
    ->leftJoin('trainings', 'trainings.id', '=', 'training_participant.id_training')
    ->leftJoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
    ->leftJoin('users', 'users.id', '=', 'training_participant.id_users')
    ->leftjoin('participant_attendance','training_participant.id','=','participant_attendance.id_training_participant')
    ->select('users.fullname',
    'participant_attendance.id as participant_attendance_id',
    'training_participant.id as id_training_participant',
    'participant_attendance.attend_check',
    'participant_attendance.remark',
   DB::raw('participant_attendance.date_attend, DATE_FORMAT(participant_attendance.date_attend,  "%d/%m/%Y") as date_attend'),
   DB::raw('participant_attendance.time_check, DATE_FORMAT(participant_attendance.time_check,  "%H:%i:%s") as time_check'),
   DB::raw('participant_attendance.time_out, DATE_FORMAT(participant_attendance.time_out,  "%H:%i:%s") as time_out'),
   DB::raw('DATEDIFF(trainings.date_end, trainings.date_start) + 1 as total_days'),
    )
   //DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
   ->groupBy('training_participant.id')
  

    ->get();
    // dd($examlist);

    return response()->json([
        'status' => 'success',
        'examlist' => $examlist,
    ], 200);

}


public function adminStatusList(Request $request,$batch_no,$ID){

    $statuslist = DB::table('training_participant')
    ->where('trainings.id_training_course', '=', $ID)
    ->where('trainings.batch_no_full', '=', $batch_no)
    ->where('training_participant.id_approval_status', '=', 2)
    ->leftJoin('trainings', 'trainings.id', '=', 'training_participant.id_training')
    ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
    ->leftJoin('users', 'users.id', '=', 'training_participant.id_users')
    ->leftJoin('participant_attendance', 'training_participant.id', '=', 'participant_attendance.id_training_participant')
    ->select('participant_attendance.id_training_participant', 'users.fullname', 
             DB::raw('count(participant_attendance.id_training_participant) as total_attend'),
             DB::raw('sum(case when participant_attendance.attend_check = 1 then 1 else 0 end) as attend_count'),
             DB::raw('GROUP_CONCAT(participant_attendance.remark) as remarks')
            )
    ->groupBy('training_participant.id')
    ->get(); 

return response()->json([
    'status' => 'success',
    'statuslist' => $statuslist,
], 200);

}

Public function participantFeedbackList(Request $request,$batch_no,$ID){
      //var_dump($batch_no);

    $plist = DB::table('training_participant')
    //->where('training_participant.id_training', '=', $ID)
    ->where('trainings.id_training_course', '=', $ID)
    ->where('trainings.batch_no_full', '=', $batch_no)
    ->leftJoin('trainings', 'trainings.id', '=', 'training_participant.id_training')
    ->leftJoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
    ->leftJoin('users', 'users.id', '=', 'training_participant.id_users')
    ->leftJoin('participant_feedback','participant_feedback.id_training_participant','=','training_participant.id')
    ->select('training_participant.id as id_training_participant','training_participant.id_training', 'training_participant.id_users','trainings.id','training_course.course_name as training_name',
    'trainings.batch_no','trainings.batch_no_full','participant_feedback.submitted_status',
    'users.fullname','users.ic_no','trainings.endorse_feedback',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
        
    )
    ->groupby('users.id')
    ->get();



                

            return response()->json([
                  'status' => 'success',
                  'participantlist' => $plist,
              ], 200);
    }


    Public function participantFeedbackView(Request $request,$batch_no,$ID){
        //  var_dump($ID);
    
        $viewFeedback = DB::table('feedback_question')
        ->where('participant_feedback.id_training_participant', '=', $ID)
        ->leftJoin('participant_feedback', 'participant_feedback.id_feedback_question', '=', 'feedback_question.id')
        ->leftJoin('training_participant', 'training_participant.id', '=', 'participant_feedback.id_training_participant')
        ->select('feedback_question.id','feedback_question.question','participant_feedback.feedback',
        'participant_feedback.id_training_participant')
        
        //->groupby('users.id')
        ->get();
    
    
//         select feedback_question.id,feedback_question.id_training,feedback_question.question,participant_feedback.feedback,
//    participant_feedback.id_users
//    from feedback_question 
//    left join participant_feedback on  participant_feedback.id_feedback_question  = feedback_question.id 
//    left join training_participant on  training_participant.id = participant_feedback.id_training_participant
//    where participant_feedback.id_users = 8;
    
                    
    
                return response()->json([
                      'status' => 'success',
                      'viewFeedback' => $viewFeedback,
                  ], 200);
        }

        public function insertAttendance(Request $request, $batch_no,$ID)
                    {

                        $userAgent = $request->header('User-Agent');
                        $browser = 'Unknown';

                        if (strpos($userAgent, 'Chrome') !== false) {
                            $browser = 'Chrome';
                        } elseif (strpos($userAgent, 'Firefox') !== false) {
                            $browser = 'Firefox';
                        } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                            $browser = 'Safari';
                        } elseif (strpos($userAgent, 'Edg') !== false) {
                            $browser = 'Edge';
                        } elseif (strpos($userAgent, 'MSIE') !== false) {
                            $browser = 'Internet Explorer';
                        } elseif (strpos($userAgent, 'Opera') !== false) {
                            $browser = 'Opera';
                        }

                        AuditTrail::create([
                            'username' => auth()->user()->fullname ?? 'Guest',
                            'activity' => 'Insert attendance for Training ' . $ID . ' , batch number ' . $batch_no,
                            'activity_time' => now(),
                            'ip_address' => $request->ip(),
                            'browser' => $browser,
                        ]);
                        // $dateCheck = Carbon::createFromFormat('d/m/Y', $request->input('date_check'))->format('Y-m-d');
                        // $insertParticipant = DB::table('participant_attendance')
                        //         ->insert([
                        //             'id_training_participant' => $request->input('id_training_participant'),
                        //             'id_training' => $request->input('id_training'),
                        //             'attend_check' => $request->input('attend_check'),
                        //             'date_attend' => $dateCheck,
                        //             'time_check' => $request->input('time_check'),
                        //             'time_out' => $request->input('time_out'),
                        //             'remark' => $request->input('remark'),
                        //             'created_by' => $request->input('UserID'),
                        //             'created_at' => now(),
                        //         ]);
                            
                        if ($request->has('attendanceData')) {
                            $attendanceData = $request->input('attendanceData');
                    
                            foreach ($attendanceData as $data) {
                                $formattedDate = Carbon::createFromFormat('d/m/Y', $data['date_attend'])->format('Y-m-d');
                                $data['date_attend'] = $formattedDate;
                    
                                // Insert each set of data into the database
                                $insertParticipant = DB::table('participant_attendance')->insert($data);
                            }
                    
                            return response()->json([
                                'insertParticipant' => $insertParticipant,
                            ], 201);
                        } else {
                            return response()->json([
                                'error' => 'No attendance data provided',
                            ], 400);
                        }
                    }


        

                    public function updateAttendance(Request $request, $batch_no,$ID)
                    {

                        $userAgent = $request->header('User-Agent');
                        $browser = 'Unknown';

                        if (strpos($userAgent, 'Chrome') !== false) {
                            $browser = 'Chrome';
                        } elseif (strpos($userAgent, 'Firefox') !== false) {
                            $browser = 'Firefox';
                        } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                            $browser = 'Safari';
                        } elseif (strpos($userAgent, 'Edg') !== false) {
                            $browser = 'Edge';
                        } elseif (strpos($userAgent, 'MSIE') !== false) {
                            $browser = 'Internet Explorer';
                        } elseif (strpos($userAgent, 'Opera') !== false) {
                            $browser = 'Opera';
                        }

                        AuditTrail::create([
                            'username' => auth()->user()->fullname ?? 'Guest',
                            'activity' => 'Updated attendance for Training ' . $ID . ' , batch number ' . $batch_no,
                            'activity_time' => now(),
                            'ip_address' => $request->ip(),
                            'browser' => $browser,
                        ]);

                        //var_dump($request->all());
                        $updateTrainingPostpone = DB::table('participant_attendance')
                        ->where('id_training', $ID)
                        ->update([
                            
                           'submitted_status' => $request->submitted_status,
                            'updated_by' => $request->userid,
                            'updated_at' => Carbon::now(),
                           
                        ]);
                        return response()->json([
                            'updateTrainingPostpone' => $updateTrainingPostpone,
                        ], 200);
                    }

                    

                    
                    public function updateFeedbackSubmit(Request $request, $batch_no,$ID)
                    {

                        $userAgent = $request->header('User-Agent');
                        $browser = 'Unknown';

                        if (strpos($userAgent, 'Chrome') !== false) {
                            $browser = 'Chrome';
                        } elseif (strpos($userAgent, 'Firefox') !== false) {
                            $browser = 'Firefox';
                        } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                            $browser = 'Safari';
                        } elseif (strpos($userAgent, 'Edg') !== false) {
                            $browser = 'Edge';
                        } elseif (strpos($userAgent, 'MSIE') !== false) {
                            $browser = 'Internet Explorer';
                        } elseif (strpos($userAgent, 'Opera') !== false) {
                            $browser = 'Opera';
                        }

                        AuditTrail::create([
                            'username' => auth()->user()->fullname ?? 'Guest',
                            'activity' => 'Updated feedback submission for Training ' . $ID . ' , batch number ' . $batch_no,
                            'activity_time' => now(),
                            'ip_address' => $request->ip(),
                            'browser' => $browser,
                        ]);


                        //var_dump($request->all());
                        $updateFeedbackSub = DB::table('participant_feedback')
                        ->where('id_training', $ID)
                        ->update([
                            
                           'submitted_status' => $request->submitted_status,
                            'updated_by' => $request->userid,
                            'updated_at' => Carbon::now(),
                           
                        ]);
                        return response()->json([
                            'updateFeedbackSub' => $updateFeedbackSub,
                        ], 200);
                    }



                    //trainingreport

                    public function trainingReportTPInformation(Request $request,$batch_no,$ID){

                        //     dd(($request->file('FailGambar0')));

                        $userAgent = $request->header('User-Agent');
                        $browser = 'Unknown';

                        if (strpos($userAgent, 'Chrome') !== false) {
                            $browser = 'Chrome';
                        } elseif (strpos($userAgent, 'Firefox') !== false) {
                            $browser = 'Firefox';
                        } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                            $browser = 'Safari';
                        } elseif (strpos($userAgent, 'Edg') !== false) {
                            $browser = 'Edge';
                        } elseif (strpos($userAgent, 'MSIE') !== false) {
                            $browser = 'Internet Explorer';
                        } elseif (strpos($userAgent, 'Opera') !== false) {
                            $browser = 'Opera';
                        }

                        AuditTrail::create([
                            'username' => auth()->user()->fullname ?? 'Guest',
                            'activity' => 'Submitted Training report for training  ' . $ID . ' , batch number ' . $batch_no,
                            'activity_time' => now(),
                            'ip_address' => $request->ip(),
                            'browser' => $browser,
                        ]);

                                $dateStart = $request->dateStartV;
                                $carbonDateStart = Carbon::createFromFormat('d/m/Y', $dateStart);
                                $formattedDateStart = $carbonDateStart->toDateTimeString();
                                $formattedDateCustomStart = $carbonDateStart->format('Y-m-d H:i:s');

                                $dateEnd = $request->dateEndV;
                                $carbonDateEnd = Carbon::createFromFormat('d/m/Y', $dateEnd);
                                $formattedDateEnd = $carbonDateEnd->toDateTimeString();
                                $formattedDateCustomEnd = $carbonDateEnd->format('Y-m-d H:i:s');

                               


                       // var_dump($request->BatchNumberV); var_dump($request->dateStartV); var_dump($request->dateEndV);
                             $insertReport = DB::table('training_report')
                       
                            ->insert([
                                 'id_training' =>$ID,
                                 'batch_no' => $batch_no,
                                 'batch_no_full' => $request->BatchNumberV,
                                 'provider_name' => $request->providerCompanyV,
                                 'date_start' => $formattedDateCustomStart,
                                 'date_end' => $formattedDateCustomEnd,
                                 'venue' => $request->VenueidV,
                                 'training_name' => $request->trainingNameV,
                                 'training_code' => $request->courseCodeV,
                                 'competent_trainer' => $request->Competent_trainer,
                                 'synopsis' => $request->training_Synopsis,
                                 'introduction' => $request->training_Introduction,
                                 'prepared_by' => $request->trainer,
                                 'conclusion' => $request->training_conclusion,   
                                 'created_by' => $request->UserID,
                                'created_at' => Carbon::now(),
                                
                             ]);
                         

                             $endorse_exam = DB::table('trainings')
                             ->where('trainings.batch_no', '=', $batch_no)
                             ->where('trainings.id', '=', $ID)
                         
                             ->update([
                                 
                                 'endorse_report' => 1, //submitted to endorse
                                 'updated_at' => Carbon::now(),
                                 
                             ]);
               
                     
                               
                            
                        
             
                                 if ($request->hasFile('FailKeputusan')) { //use
             
                                     $files = $request->file('FailKeputusan');
                                     foreach($files as $file){
                                         $extension = $file->getClientOriginalExtension();
                                         $name = $file->getClientOriginalName();
                                         $filename = pathinfo($name, PATHINFO_FILENAME);
                                         $namaFail1 = explode('.', $name);
                                         $path = $file->getRealPath();
                                         $data = file_get_contents($path);
                                         $SaizFail = $file->getSize();
                                     
                     
                                         $updateGambarIC = DB::table('training_report')
                                         ->where('training_report.id_training', '=', $ID)
                                         ->where('training_report.batch_no', '=', $batch_no)
                                         ->update([
                                         
                                             'result_upload_name' => $filename,
                                             'result_upload_path' => base64_encode($data),
                                             'result_upload_size' => $SaizFail,
                                             'result_upload_format' => $extension,
             
                                             'created_by' => $request->UserID,
                                             'created_at' => Carbon::now(),
                                         ]);
                                     }
                                 }

                                 if ($request->hasFile('FailReport')) {  //use
                                    $files = $request->file('FailReport');
                                    foreach($files as $file){
                                        $extension = $file->getClientOriginalExtension();
                                        $name = $file->getClientOriginalName();
                                        $filename = pathinfo($name, PATHINFO_FILENAME);
                                        $namaFail1 = explode('.', $name);
                                        $path = $file->getRealPath();
                                        $data = file_get_contents($path);
                                        $SaizFail = $file->getSize();
                                    
                    
                                        $updateFileReport = DB::table('training_report')
                                        ->where('training_report.id_training', '=', $ID)
                                        ->where('training_report.batch_no', '=', $batch_no)
                                        ->update([
                                        
                                            'report_upload_name' => $filename,
                                            'report_upload_path' => base64_encode($data),
                                            'report_upload_size' => $SaizFail,
                                            'report_upload_format' => $extension,
            
                                            'created_by' => $request->UserID,
                                            'created_at' => Carbon::now(),
            
                                        ]);
                                    }
                                }

                                 
             
                                if ($request->hasFile('FailResit')) {
                                    $file = $request->file('FailResit');
                            
                                    foreach($files as $file){
                                        $extension = $file->getClientOriginalExtension();
                                        $name = $file->getClientOriginalName();
                                        $filename = pathinfo($name, PATHINFO_FILENAME);
                                        $namaFail1 = explode('.', $name);
                                        $path = $file->getRealPath();
                                        $data = file_get_contents($path);
                                        $SaizFail = $file->getSize();
                                    
                    
                                        $updateFileReport = DB::table('training_report')
                                        ->where('training_report.id_training', '=', $ID)
                                        ->where('training_report.batch_no', '=', $batch_no)
                                        ->update([
                                        
                                            'payment_upload_name' => $filename,
                                            'payment_upload_path' => base64_encode($data),
                                            'payment_upload_size' => $SaizFail,
                                            'payment_upload_format' => $extension,
            
                                            'created_by' => $request->UserID,
                                            'created_at' => Carbon::now(),
            
                                        ]);
                                    }
                                }
                            
             
                                 if ($request->hasFile('FailAktiviti')) {  //use
                                     $files = $request->file('FailAktiviti');
                                     foreach($files as $file){
                                         $extension = $file->getClientOriginalExtension();
                                         $name = $file->getClientOriginalName();
                                         $filename = pathinfo($name, PATHINFO_FILENAME);
                                         $namaFail1 = explode('.', $name);
                                         $path = $file->getRealPath();
                                         $data = file_get_contents($path);
                                         $SaizFail = $file->getSize();
                                     
                     
                                         $updateGambarPassport = DB::table('training_report')
                                         ->where('training_report.id_training', '=', $ID)
                                         ->where('training_report.batch_no', '=', $batch_no)
                                         ->update([
                                         
                                             'supportive_activity_name' => $filename,
                                             'supportive_activity_path' => base64_encode($data),
                                             'supportive_activity_size' => $SaizFail,
                                             'supportive_activity_format' => $extension,
             
                                             'created_by' => $request->UserID,
                                             'created_at' => Carbon::now(),
             
                                         ]);
                                     }
                                 }

             
             
                              
                         
                         
                             // return response()->json([
                             //     'updateUser' => $updateUser,
                             //     'updateEducation' => $updateEducation,
                             //     'updateCompany' => $updateCompany,
                             //     'updateProfilPic' => $updateProfilPic,
                             //     'updateGambarIC' => $updateGambarIC,
                             //     'updateGambarPassport' => $updateGambarPassport,
                             //     'updateGambarDoc' => $updateGambarDoc,
                                 
                             // ]);
                         
                         }



                         public function paymentOutline(Request $request,$batch_no,$ID){
                           // dd( $ID);
                            $courseinfo =   DB::table('trainings')
                            ->where('trainings.batch_no_full', '=',$batch_no)
                            ->where('trainings.id_training_course', '=',$ID)
                            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
                            ->select('trainings.id  as trainingid','training_course.id_training_course','training_course.course_name as course_name','trainings.batch_no_full' 
                            )
                            ->get();
                        
                        
                            $provider = DB::table('trainings')
                            //->where('users.id', '=', $ID)
                            ->where('trainings.batch_no_full', '=', $batch_no)
                            ->where('trainings.id_training_course', '=', $ID)
                            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
                            ->leftJoin('company','company.id', '=','trainings.id_company')
                            ->select('trainings.id','training_course.course_name as course_name',
                            'trainings.batch_no_full','trainings.venue_address','trainings.training_code',
                            'company.id as providerid','company.name as providername',
                                DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
                                DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
                                
                            )
                            ->get();
                        
                        
                          
                            $paymenttoseda = DB::table('payment_to_seda')
                                ->where('payment_to_seda.id_training_course', '=',$ID )
                                ->where('trainings.batch_no_full', '=',$batch_no )
                                ->leftjoin('trainings','trainings.id','=','payment_to_seda.id_training')
                                ->select('trainings.batch_no_full','payment_to_seda.total_paid',
                                'payment_to_seda.invoice_upload_name',
                                'payment_to_seda.invoice_upload_path',
                                'payment_to_seda.invoice_upload_format',
                          
                                'payment_to_seda.receipt_upload_name',
                                'payment_to_seda.receipt_upload_path',
                                'payment_to_seda.receipt_upload_format',
                           
                            )

                            ->get();
                        
                                    return response()->json([
                                          'status' => 'success',
                                          'provider' => $provider,
                                          'courseinfo' => $courseinfo,
                                          'paymenttoseda' => $paymenttoseda,
                                        //   'assessment' => $assessment,
                                        //   'picIC' => $picIC
                                      ], 200);
                        
                        }


                        public function savePaymentDocument(Request $request,$batch_no,$ID){ //fromSEDAtoTP

                            //     dd(($request->file('FailGambar0')));
    
                            $userAgent = $request->header('User-Agent');
                            $browser = 'Unknown';
    
                            if (strpos($userAgent, 'Chrome') !== false) {
                                $browser = 'Chrome';
                            } elseif (strpos($userAgent, 'Firefox') !== false) {
                                $browser = 'Firefox';
                            } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                                $browser = 'Safari';
                            } elseif (strpos($userAgent, 'Edg') !== false) {
                                $browser = 'Edge';
                            } elseif (strpos($userAgent, 'MSIE') !== false) {
                                $browser = 'Internet Explorer';
                            } elseif (strpos($userAgent, 'Opera') !== false) {
                                $browser = 'Opera';
                            }
    
                            AuditTrail::create([
                                'username' => auth()->user()->fullname ?? 'Guest',
                                'activity' => 'Submitted Training report for training  ' . $ID . ' , batch number ' . $batch_no,
                                'activity_time' => now(),
                                'ip_address' => $request->ip(),
                                'browser' => $browser,
                            ]);
    
                             
    
                           // var_dump($request->BatchNumberV); var_dump($request->dateStartV); var_dump($request->dateEndV);
                                 $insertReport = DB::table('payment_to_seda')
                           
                                ->insert([
                                     'id_training_course' => $request->courseID,
                                     'id_company' =>  $request->companyID,    
                                     'id_training' => $request->trainingID,
                                     'total_paid' => $request->totalPaid,
                                    
                                     'created_by' => $request->UserID,
                                    'created_at' => Carbon::now(),
                                    
                                 ]);
                             
    
                            
                 
                                     if ($request->hasFile('FileInvoice')) { 
                 
                                         $files = $request->file('FileInvoice');
                                         foreach($files as $file){
                                             $extension = $file->getClientOriginalExtension();
                                             $name = $file->getClientOriginalName();
                                             $filename = pathinfo($name, PATHINFO_FILENAME);
                                             $namaFail1 = explode('.', $name);
                                             $path = $file->getRealPath();
                                             $data = file_get_contents($path);
                                             $SaizFail = $file->getSize();
                                         
                         
                                             $updateFileInvoice = DB::table('payment_to_seda')
                                             ->where('payment_to_seda.id_training_course', '=', $request->courseID)
                                             ->where('payment_to_seda.id_training', '=',$request->trainingID)
                                             ->update([
                                             
                                                 'invoice_upload_name' => $filename,
                                                 'invoice_upload_path' => base64_encode($data),
                                                 'invoice_upload_size' => $SaizFail,
                                                 'invoice_upload_format' => $extension,
                 
                                                 'created_by' => $request->UserID,
                                                 'created_at' => Carbon::now(),
                                             ]);
                                         }
                                     }
    
                               
                             }


                             public function savePaymentDocumentTP(Request $request,$batch_no,$ID){ //fromTPtoSEDA

                                //     dd(($request->file('FailGambar0')));
        
                                $userAgent = $request->header('User-Agent');
                                $browser = 'Unknown';
        
                                if (strpos($userAgent, 'Chrome') !== false) {
                                    $browser = 'Chrome';
                                } elseif (strpos($userAgent, 'Firefox') !== false) {
                                    $browser = 'Firefox';
                                } elseif (strpos($userAgent, 'Safari') !== false && strpos($userAgent, 'Chrome') === false) {
                                    $browser = 'Safari';
                                } elseif (strpos($userAgent, 'Edg') !== false) {
                                    $browser = 'Edge';
                                } elseif (strpos($userAgent, 'MSIE') !== false) {
                                    $browser = 'Internet Explorer';
                                } elseif (strpos($userAgent, 'Opera') !== false) {
                                    $browser = 'Opera';
                                }
        
                                AuditTrail::create([
                                    'username' => auth()->user()->fullname ?? 'Guest',
                                    'activity' => 'Submitted Training report for training  ' . $ID . ' , batch number ' . $batch_no,
                                    'activity_time' => now(),
                                    'ip_address' => $request->ip(),
                                    'browser' => $browser,
                                ]);
        
                               // var_dump($request->BatchNumberV); var_dump($request->dateStartV); var_dump($request->dateEndV);
                                    //  $insertReport = DB::table('payment_to_seda')
                               
                                    // ->insert([
                                    //      'id_training_course' => $request->courseID,
                                    //      'company_id' =>  $request->companyID,    
                                    //      'id_training' => $request->trainingID,
                                    //      'total_paid' => $request->totalPaid,
                                        
                                    //      'created_by' => $request->UserID,
                                    //     'created_at' => Carbon::now(),
                                        
                                    //  ]);
                                 
        
                                
                     
        
                                         if ($request->hasFile('FileReceipt')) {  
                                            $files = $request->file('FileReceipt');
                                            foreach($files as $file){
                                                $extension = $file->getClientOriginalExtension();
                                                $name = $file->getClientOriginalName();
                                                $filename = pathinfo($name, PATHINFO_FILENAME);
                                                $namaFail1 = explode('.', $name);
                                                $path = $file->getRealPath();
                                                $data = file_get_contents($path);
                                                $SaizFail = $file->getSize();
                                                
                            
                                                $updateFileReport = DB::table('payment_to_seda')
                                                ->where('payment_to_seda.id_training_course', '=', $request->courseID)
                                                ->where('payment_to_seda.id_training', '=',$request->trainingID)
                                                ->update([
                                                
                                                    'receipt_upload_name' => $filename,
                                                    'receipt_upload_path' => base64_encode($data),
                                                    'receipt_upload_size' => $SaizFail,
                                                    'receipt_upload_format' => $extension,
                    
                                                    'updated_by' => $request->UserID,
                                                    'updated_at' => Carbon::now(),
                    
                                                ]);
                                            }
                                        }      
                     
                                        // if ($request->hasFile('FailResit')) {
                                        //     $file = $request->file('FailResit');
                                    
                                        //     foreach($files as $file){
                                        //         $extension = $file->getClientOriginalExtension();
                                        //         $name = $file->getClientOriginalName();
                                        //         $filename = pathinfo($name, PATHINFO_FILENAME);
                                        //         $namaFail1 = explode('.', $name);
                                        //         $path = $file->getRealPath();
                                        //         $data = file_get_contents($path);
                                        //         $SaizFail = $file->getSize();
                                            
                            
                                        //         $updateFileReport = DB::table('training_report')
                                        //         ->where('training_report.id_training', '=', $ID)
                                        //         ->where('training_report.batch_no', '=', $batch_no)
                                        //         ->update([
                                                
                                        //             'payment_upload_name' => $filename,
                                        //             'payment_upload_path' => base64_encode($data),
                                        //             'payment_upload_size' => $SaizFail,
                                        //             'payment_upload_format' => $extension,
                    
                                        //             'created_by' => $request->UserID,
                                        //             'created_at' => Carbon::now(),
                    
                                        //         ]);
                                        //     }
                                        // }
                                 
                                 }
    


}