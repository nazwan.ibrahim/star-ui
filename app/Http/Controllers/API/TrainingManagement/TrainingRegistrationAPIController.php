<?php

namespace App\Http\Controllers\API\TrainingManagement;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\TrainingCourse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class TrainingRegistrationAPIController extends Controller
{
    public function TrainingCourseList(Request $request){
        try{
            $trainingCourseList = DB::table('trainings as t')
            ->leftjoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
            ->select(
                'tc.course_name',
                'tc.id_training_course',
            )
            ->get();
            return response()->json($trainingCourseList);
            if ($trainingCourseList->isEmpty()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Training not found',
                ], 404);
            } else {
                Log::info('Successfully fetched training list.');
                return response()->json([
                    'status' => 'success',
                    'courseList' => $trainingCourseList,
                ], 200);
            }
        }
        catch (\Exception $e) {
            Log::error('Error fetching training list: ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }
}