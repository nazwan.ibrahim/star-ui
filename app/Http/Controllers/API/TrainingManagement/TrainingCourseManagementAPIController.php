<?php

namespace App\Http\Controllers\API\TrainingManagement;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Training;
use App\Models\TrainingCourse;
use App\Models\courseReType;
use Illuminate\Support\Facades\DB;


class TrainingCourseManagementAPIController extends Controller
{
    public function courseManagementList()
    {

        $courselist = DB::table('training_course')
        ->leftjoin('training_partner_course','training_course.id_training_course','=','training_partner_course.id_training_course')
        ->leftjoin('training_cert_code','training_course.id_training_cert','=','training_cert_code.id')
        ->leftjoin('resit_fee','training_course.id_training_course','=','resit_fee.id_training_course')
        ->leftjoin('training_option','training_course.id_training_option','=','training_option.id')
        ->leftjoin('company','company.id','=','training_partner_course.id_company')
        ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        ->leftjoin('course_re_type','course_re_type.id_training_course','=','training_course.id_training_course')
        ->leftjoin('re_type','re_type.id','=','course_re_type.id_re_type')
        ->select('training_course.id_training_course','training_course.course_name','training_partner_course.course_code',
        'training_cert_code.code', 'training_option.option','training_course.new_fee','company.name as companyname',
        'course_category.course_name as category_name',
        'training_type.name as type_name','training_course.status')
        ->groupBy('training_course.id_training_course')
        ->orderBy('training_course.created_at', 'desc')   
        ->get();

       if (!$courselist) {
        return response()->json([
            'status' => 'error',
            'message' => 'Training not found',
        ], 404);
        }else{
            return response()->json([
                'status' => 'success',
                'courselist' => $courselist,
            ], 200);
        }
    
        
        
    }

    public function addCourseTraining(Request $request)
    {
        try {
            $trainingcourse = TrainingCourse::create([
                'course_name' => $request['course_name'],
                'id_course_category' => $request['id'],
                'id_training_type' => $request['id'],
                'course_description' => $request['course_description'],
                'is_paid' => $request->input('is_paid', 0),
                'is_free' => $request->input('is_free', 0),
                'fee' => $request['fee'],
                'receipt' => $request['receipt'],
            ]);
            
            $trainingCourseId = $trainingcourse->id;
            
            $reType = courseReType::create([
                'id_re_type' => $request->input('re_type', []),
                'training_course_id' => $trainingCourseId, 
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Training Course Created',
            ], 200);

        } catch (\InvalidArgumentException) {
            return response()->json([
                'status' => false,
                'message' => 'Failed to create'
            ], 500);
        }
       
    }

    public function showCourseTraining(Request $request,$id)
    {
        $courselist = DB::table('training_course')
        ->where('training_course.id_training_course', '=', $id)
        ->leftjoin('training_partner_course','training_course.id_training_course','=','training_partner_course.id_training_course')
        ->leftjoin('company','company.id','=','training_partner_course.id_company')
        ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        ->leftjoin('course_re_type','course_re_type.id_training_course','=','training_course.id_training_course')
        ->leftjoin('re_type','re_type.id','=','course_re_type.id_re_type')
        ->select('training_course.id_training_course',
        'training_course.course_name',
        'training_course.fee',
        'training_course.receipt',
        'training_course.is_free',
        'training_course.is_paid',
        'training_course.id_course_category',
        'training_course.id_training_type',
        'training_course.course_description',
        'course_category.course_name as category_name',
        'training_type.name as type_name',
        'training_partner_course.course_code',
        'company.name as companyname',
        )
        ->get();

        return response()->json([
            'status' => 'success',
            'courselist' => $courselist,
        ], 200);
     
    }

    public function updateCourseTraining(Request $request, $id)
    {
        $updateTrainingCourse = DB::table('training_course')
            ->where('id_training_course', $id)
            ->update([
                'course_name' => $request->input('course_name'),
                'id_course_category' => $request->input('course_category'),
                'id_training_type' => $request->input('training_type'),
                'course_description' => $request->input('course_description'),
                'is_paid' => $request->input('is_paid', 0),
                'is_free' => $request->input('is_free', 0),
                'fee' => $request->input('fee'),
                'receipt' => $request->input('receipt'),
            ]);
    
        $updateCourseReType = DB::table('course_re_type')
            ->where('id_training_course', $id)
            ->update([
                'id_re_type' => $request->input('re_types', []),
            ]);
    
        return response()->json([
            'updateTrainingCourse' => $updateTrainingCourse,
            'updateCourseReType' => $updateCourseReType,
        ], 200);
    }

    public function deleteCourseTraining(Request $request,$id)
    {
        try {
            DB::beginTransaction();

            DB::table('course_re_type')->where('id_training_course', $id)->delete();

            $courselist = DB::table('training_course')->where('id_training_course', $id);

            if ($courselist) {
                DB::table('training_course')->where('id_training_course', $id)->delete();

                DB::commit();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Training Course Deleted',
                ], 200) ;           
            } else {
                DB::rollback();

                return response()->json([
                    'status' => 'error',
                    'message' => 'Training Course Failed to Delete',
                ], 404) ;                 }
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'An error occurred: ' . $e->getMessage(),
            ], 500) ;
        }
    }
}
