<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Users;

class changePasswordAPIController extends Controller
{
    public function changepassword(Request $request) {
//dd($request->NewPassword);exit;
        $kemaskiniUserPassword = Users::where('email', '=', 'idayu@milradius.com.my');

        $kemaskiniUserPassword->update([
           
             'password' => bcrypt($request->NewPassword),
             'password_confirmation' => bcrypt($request->ConfirmPassword),
            // 'postcode' => $request->NewPassword,
            // 'address_2' => $request->ConfirmPassword,
            'updated_at' => Carbon::now(),
           
        ]);


        return response()->json([
            'status' => 'success',
            'kemaskiniUserPassword' => $kemaskiniUserPassword,
        ], 200);

    }

    public function alluser(Request $request) {

        $maklumatTutupID = Users::where('email', '=', 'arif@milradius.com.my')
        ->select(
            'users.fullname',
            'users.ic_no',
           
          
        )
        ->get();
        
                return response()->json([
                    'status' => 'success',
                    'maklumatTutupID' => $maklumatTutupID,
                ], 200);
             
            }

}
