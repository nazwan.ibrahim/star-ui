<?php

namespace App\Http\Controllers\API\ExternalAPI;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ExternalApiController extends Controller
{
    public function index()
    {
        $response = Http::get('https://spqp-staging.seda.gov.my');
    
        $jsonData = $response->json();
          
        dd($jsonData);
    }

    public function store()
    {
        
        $token = "744adbe35c9bb0cecfb1c72b01b1b48f8376f4b0"; 

        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->post('https://spqp-staging.seda.gov.my/api/qualified_person/notify', [
            "profile" => [
                "name" => "razimi", 
                "mykad_no" => "88888888888", 
                "gender" => "M", 
                "nationality" => "MALAYSIAN", 
                "mobile_no" => "012232342343", 
                "email" => "razimi@seda.gov.my", 
                "qualification_background" => "Degree in mechanical", 
                "identification_type" => "mykad"
            ],
            "address" => [
                "line1" => "address line 1", 
                "line2" => "address line 2",
                "line3" => "address line 3",
                "postcode" => "62100", 
                "city" => "Putrajaya", 
                "state" => "01", 
                "country" => "MALAYSIA" 
            ],
            "certificate" => [
                "category_code" => "GCPV", // *required. GCPV Systems Design / Wireman Chargeman Solar PV 
                "category_name" => "GCPV Systems Design", // *required. GCPV Systems Design / Wireman Chargeman Solar PV 
                "certificate_no" => "GCPV12345678", 
                "institute_name" => "Universiti Tecknologi Mara", 
                "institute_code" => "UiTMxx", 
                "batch_no" => "batch-001" 
            ]
        ]);

        $jsonData = $response->json();

        dd($jsonData);
    }

    public function verify()
    {
        $token = "744adbe35c9bb0cecfb1c72b01b1b48f8376f4b0"; 

        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->post('https://spqp-staging.seda.gov.my/api/qualified_person/certificate/verify', [
            "mykad_no" => "880808888888",
            "certificate_no" => "SEDA077"
        ]);

        $jsonData = $response->json();
        
        dd($jsonData);


    }


}