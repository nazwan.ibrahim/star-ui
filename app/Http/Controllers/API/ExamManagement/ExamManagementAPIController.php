<?php
namespace App\Http\Controllers\API\ExamManagement;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use App\Mail\QueryExam;

class ExamManagementAPIController extends Controller
{

    
    Public function examTrainingListTP(Request $request,$userid){

        $userInfo = DB::table('company')
        ->leftjoin('users','company.id','=','users.id_company')
        ->where('users.id', '=',$userid)
        ->select('users.id', 'users.fullname','company.id as companyid')
        ->first();
//dd($userInfo->companyid);
   
    //     $examlist = DB::table('trainings')
    //     ->where('trainings.id_company','=',$userInfo->companyid)
    //     ->leftjoin('company','company.id','=','trainings.id_company')
    //     ->leftjoin('participant_examresult','participant_examresult.id_training_course','=','trainings.id_training_course')
    //     ->leftjoin('training_trainer','training_trainer.id_training','=','trainings.id')
    //     ->leftjoin('trainer','trainer.id','=','training_trainer.id_trainer')
    //     ->leftjoin('users','users.id','=','trainer.id_users')
    //     ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
    //     ->select('trainings.id','trainings.id_training_course','trainings.batch_no','trainings.batch_no_full','trainings.venue',
    // DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
    // DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
    // 'trainings.id_training_status','trainings.id_company','trainings.id_training_status as status','users.fullname',
    // 'participant_examresult.endorsed_status','company.name as trainingpartner','training_course.course_name',
    // 'training_course.id_training_option')
    // // ->where('trainings.deleted_at','=','')
    // ->orderby('trainings.id_training_status','desc')
    // ->groupby('batch_no')
//dd($userInfo->companyid);
    $examlist = DB::table('trainings')
    ->where('trainings.id_company','=', $userInfo->companyid)
    ->leftjoin('company','trainings.id_company','=','company.id')
    ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
    ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
    ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
    ->select('trainings.id','trainings.id_training_course',
    'company.name as company_name',
    'course_category.course_name as category_name','training_course.course_name',
    'trainings.batch_no','trainings.batch_no_full','trainings.venue',
    'training_type.name as type_name','trainings.id_approval_status','trainings.endorsed_status as endorsed_training',
    DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
    DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
    'trainings.id_training_status as status',
    )
    ->orderby('trainings.id_training_status','desc')
    

    ->get();

    return response()->json([
        'status' => 'success',
        'examlist' => $examlist,
    ], 200);

}

    Public function examTrainingList(Request $request){

   
            $examlist = DB::table('trainings')
            ->leftjoin('company','company.id','=','trainings.id_company')
            ->leftjoin('participant_examresult','participant_examresult.id_training_course','=','trainings.id_training_course')
            ->leftjoin('training_trainer','training_trainer.id_training','=','trainings.id')
            ->leftjoin('trainer','trainer.id','=','training_trainer.id_trainer')
            ->leftjoin('users','users.id','=','trainer.id_users')
            ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
            ->select('trainings.id','trainings.id_training_course','trainings.batch_no','trainings.batch_no_full','trainings.venue',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
        'trainings.id_training_status','trainings.id_company','trainings.status','users.fullname',
        'participant_examresult.endorsed_status','company.name as trainingpartner','training_course.course_name',
        'training_course.id_training_option')
        // ->where('trainings.deleted_at','=','')
        ->orderby('id_training_status','desc')
        ->groupby('batch_no')
    
        ->get();

        return response()->json([
            'status' => 'success',
            'examlist' => $examlist,
        ], 200);

    }


    Public function examDetails(Request $request, $ID,$batch_full){

        $trainingDetails = DB::table('trainings')
        ->where('trainings.id_training_course','=',$ID)
        ->where('trainings.batch_no_full','=',$batch_full)
        ->leftJoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->select('trainings.id','trainings.batch_no','training_course.course_name as training_name','trainings.venue','trainings.batch_no_full',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'))
    
        ->get();

        return response()->json([
            'status' => 'success',
            'trainingDetails' => $trainingDetails,
        ], 200);

    }

    Public function examDetailsAdmin(Request $request, $id){

        $trainingDetails = DB::table('trainings')
        ->where('trainings.batch_no','=',$id)
        ->leftJoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->select('trainings.id','trainings.batch_no','training_course.course_name as training_name','trainings.venue',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'))
    
        ->get();

        return response()->json([
            'status' => 'success',
            'trainingDetails' => $trainingDetails,
        ], 200);

    }


    Public function examNameList(Request $request,$ID,$batch_full){
          //  var_dump($ID);var_dump($batch_full);

                       $checkHeaderName= DB::select("SELECT exam_result_header.id_training_course, exam_result_header.exam_1,exam_result_header.exam_2,
                       exam_result_header.exam_3, exam_result_header.exam_4,exam_result_header.exam_5,
                       exam_result_header.practical_exam_1,exam_result_header.practical_exam_2,
                       exam_result_header.practical_exam_3, exam_result_header.practical_exam_4,exam_result_header.practical_exam_5
                        FROM exam_result_header
                        WHERE exam_result_header.id_training_course = $ID");

                        $headerName = [];

                        // Loop through each exam column and strip tags
                        foreach ($checkHeaderName[0] as $key => $value) {
                            $headerName[$key] = strip_tags($value);
                        }
                       
                        //check training's date
                        $checkdate = DB::select("SELECT trainings.id_training_course, trainings.date_start
                        FROM trainings
                        WHERE trainings.batch_no_full= '$batch_full' and trainings.id_training_course = $ID");

                        $startDate = Carbon::parse($checkdate[0]->date_start);
                        $currentDate = Carbon::now();

                        
                        // if ($currentDate->gt($startDate)) {
                        //     // Current date is greater than the start date
                        //     dd('here');
                        // } else {
                        //     // Current date is not greater than the start date
                        //     echo "Current date is not greater than the start date.";
                        // }

                        // Return the cleaned names as JSON
                       // return response()->json(['cleanedNames' => $cleanedNames]);

                        $exam = DB::select("
                        SELECT DISTINCT 
                        participant_examresult.id_training_participant,exam_practical.id_participant_examresult,participant_examresult.batch_no_full,
                        participant_examresult.exam_type,participant_examresult.submitted_status,
                        users.fullname,participant_examresult.id,
                        exam_practical.id_training,
                        exam_practical.exam_1 as part_A,
                        exam_practical.exam_2 as part_B,
                        exam_practical.exam_3 as part_C,
                        exam_practical.total_mark as total_mark_practical,
                        exam_practical.result_status as result_status_practical,
                        exam_theory.exam_1 as part_A_theory,
                        exam_theory.exam_2 as part_B_theory,
                        exam_theory.exam_3 as part_C_theory,
                        exam_theory.total_mark as total_mark_theory,
                        exam_theory.result_status as result_status_theory,
                        participant_examresult.final_verdict_status,participant_examresult.endorsed_status
                        from participant_examresult
                  left join exam_practical on exam_practical.id_participant_examresult = participant_examresult.id 
                  left join exam_theory on exam_theory.id_participant_examresult = participant_examresult.id 
                  left join training_participant on training_participant.id = participant_examresult.id_training_participant
                  left join users on users.id = training_participant.id_users 
                  where participant_examresult.batch_no_full= '$batch_full' and participant_examresult.id_training_course = $ID
                  and exam_theory.id_training_course = $ID and exam_practical.id_training_course = $ID ;
                        ");

                       
                        if ((!$exam || count($exam) === 0)  && ($currentDate->lt($startDate))) { //less than

                            $findparticipaneexam = DB::select("
                            select training_participant.id as id_training_participant,training_participant.id_training,
                            training_participant.id_users,trainings.batch_no,trainings.id_training_course,
                            trainings.batch_no_full from training_participant
                            left join trainings on ((trainings.id = training_participant.id_training))
                            where trainings.batch_no_full = '$batch_full'
                            ");

                            foreach ($findparticipaneexam as $participantexam){
                                $id_training_participant =  $participantexam->id_training_participant;
                                $id_training = $participantexam->id_training;
                                $id_training_course = $participantexam->id_training_course;
                                $batch_no = $participantexam->batch_no;
                                $batch_no_full = $participantexam->batch_no_full;

                                $addParticipant = DB::table('participant_examresult')
                                ->insertGetId([
                                        'id_training_participant' => $id_training_participant,
                                        'exam_type' =>'New',
                                        'id_training' => $id_training,
                                        'id_training_course' => $id_training_course,
                                        'batch_no' => $batch_no,
                                        'batch_no_full' => $batch_no_full,
                                        'created_by' => $request->Createdby,
                                        'created_at' => Carbon::now(),
                                    
                                ]);
                        
                                if($addParticipant != null){

                                    $addParticipantTheory = DB::table('exam_theory')
                                    ->insert([
                                            'id_participant_examresult' => $addParticipant,
                                            'id_training' => $id_training,
                                            'id_training_course' => $id_training_course,
                                            'batch_no_full' => $batch_no_full,
                                            'created_by' => $request->Createdby,
                                            'created_at' => Carbon::now(),
                                          
                        
                                    ]);

                                    $addParticipantPractical = DB::table('exam_practical')
                                    ->insert([
                                            'id_participant_examresult' => $addParticipant,
                                            'id_training' => $id_training,
                                            'id_training_course' => $id_training_course,
                                            'batch_no_full' => $batch_no_full,
                                            'created_by' => $request->Createdby,
                                            'created_at' => Carbon::now(),
                                           
                        
                                    ]);

                                }


                            }

                        }

                        
                        if (($exam || count($exam) !== 0)  && ($currentDate->lt($startDate))) { //less than

                            $deletePE =  DB::table('participant_examresult')
                            ->where('id_training_course', '=', $ID)
                            ->where('batch_no_full', '=', $batch_full)
                            ->delete();
                           

                            $deleteET = DB::table('exam_theory')
                            ->where('id_training_course', '=', $ID)
                            ->where('batch_no_full', '=', $batch_full)
                            ->delete();
                            

                            $deleteEP =  DB::table('exam_practical')
                            ->where('id_training_course', '=', $ID)
                            ->where('batch_no_full', '=', $batch_full)
                            ->delete();

                            $findparticipaneexam = DB::select("
                            select training_participant.id as id_training_participant,training_participant.id_training,
                            training_participant.id_users,trainings.batch_no,trainings.id_training_course,
                            trainings.batch_no_full from training_participant
                            left join trainings on ((trainings.id = training_participant.id_training))
                            where trainings.batch_no_full = '$batch_full'
                            ");

                            foreach ($findparticipaneexam as $participantexam){
                                $id_training_participant =  $participantexam->id_training_participant;
                                $id_training = $participantexam->id_training;
                                $id_training_course = $participantexam->id_training_course;
                                $batch_no = $participantexam->batch_no;
                                $batch_no_full = $participantexam->batch_no_full;

                                $addParticipant = DB::table('participant_examresult')
                                ->insertGetId([
                                        'id_training_participant' => $id_training_participant,
                                        'exam_type' =>'New',
                                        'id_training' => $id_training,
                                        'id_training_course' => $id_training_course,
                                        'batch_no' => $batch_no,
                                        'batch_no_full' => $batch_no_full,
                                        'created_by' => $request->Createdby,
                                        'created_at' => Carbon::now(),
                                    
                                ]);
                        
                                if($addParticipant != null){

                                    $addParticipantTheory = DB::table('exam_theory')
                                    ->insert([
                                            'id_participant_examresult' => $addParticipant,
                                            'id_training' => $id_training,
                                            'id_training_course' => $id_training_course,
                                            'batch_no_full' => $batch_no_full,
                                            'created_by' => $request->Createdby,
                                            'created_at' => Carbon::now(),
                                          
                        
                                    ]);

                                    $addParticipantPractical = DB::table('exam_practical')
                                    ->insert([
                                            'id_participant_examresult' => $addParticipant,
                                            'id_training' => $id_training,
                                            'id_training_course' => $id_training_course,
                                            'batch_no_full' => $batch_no_full,
                                            'created_by' => $request->Createdby,
                                            'created_at' => Carbon::now(),
                                           
                        
                                    ]);

                                }


                            }
                           
                        }


                  return response()->json([
                        'status' => 'success',
                        'examlist' => $exam,
                        'checkHeaderName' => $checkHeaderName,
                        'headerName' => $headerName
                          
                    ], 200);

            // Now $processedData contains the desired result without duplicate rows

    }


    Public function examNameList_GCPVDESIGN(Request $request,$ID,$batch_full){
        //  var_dump($ID);var_dump($batch_full);

                     $checkHeaderName= DB::select("SELECT
                     id_training_course,
                     exam_1 as fundamental_exam_1,
                     exam_2 as fundamental_exam_2,
                     exam_3 as fundamental_exam_3,
                     design_exam_1,
                     design_exam_2,
                     design_exam_3,
                     practical_exam_1,
                     practical_exam_2,
                     practical_exam_3
                     FROM examGCPV_result_header");

                      $headerName = [];

                      // Loop through each exam column and strip tags
                      foreach ($checkHeaderName[0] as $key => $value) {
                          $headerName[$key] = strip_tags($value);
                      }

                      // Return the cleaned names as JSON
                     // return response()->json(['cleanedNames' => $cleanedNames]);

                      $exam = DB::select("
                      SELECT DISTINCT
                            participant_examresult.id_training_participant,
                            exam_practical.id_participant_examresult,
                            participant_examresult.batch_no_full,
                            participant_examresult.exam_type,
                            participant_examresult.submitted_status,
                            users.fullname,
                            participant_examresult.id,
                            exam_practical.id_training,
                            exam_fundamental.exam_1 AS part_A_fundamental,
                            exam_fundamental.exam_2 AS part_B_fundamental,
                            exam_fundamental.exam_3 AS part_C_fundamental,
                            exam_fundamental.total_mark AS total_mark_fundamental,
                            exam_fundamental.result_status AS result_status_fundamental,  
                            exam_theory.exam_1 AS part_A_design,
                            exam_theory.exam_2 AS part_B_design,
                            exam_theory.exam_3 AS part_C_design,
                            exam_theory.total_mark AS total_mark_design,
                            exam_theory.result_status AS result_status_design,
                            exam_practical.exam_1 AS part_A_practical,
                            exam_practical.exam_2 AS part_B_practical,
                            exam_practical.exam_3 AS part_C_practical,
                            exam_practical.total_mark AS total_mark_practical,
                            exam_practical.result_status AS result_status_practical,   
                           
                            participant_examresult.final_verdict_status
                        FROM participant_examresult
                        LEFT JOIN exam_practical ON exam_practical.id_participant_examresult = participant_examresult.id
                        LEFT JOIN exam_theory ON exam_theory.id_participant_examresult = participant_examresult.id
                        LEFT JOIN exam_fundamental ON exam_fundamental.id_participant_examresult = participant_examresult.id
                        LEFT JOIN training_participant ON training_participant.id = participant_examresult.id_training_participant
                        LEFT JOIN users ON users.id = training_participant.id_users
                        WHERE participant_examresult.batch_no_full = '$batch_full'
                            AND participant_examresult.id_training_course = $ID
                            AND exam_theory.id_training_course = $ID
                            AND exam_practical.id_training_course = $ID;
                      ");

                      if (!$exam || count($exam) === 0) {

                          $findparticipaneexam = DB::select("
                          select training_participant.id as id_training_participant,training_participant.id_training,
                          training_participant.id_users,trainings.batch_no,trainings.id_training_course,
                          trainings.batch_no_full from training_participant
                          left join trainings on ((trainings.id = training_participant.id_training))
                          where trainings.batch_no_full = '$batch_full'
                          ");

                          foreach ($findparticipaneexam as $participantexam){
                              $id_training_participant =  $participantexam->id_training_participant;
                              $id_training = $participantexam->id_training;
                              $id_training_course = $participantexam->id_training_course;
                              $batch_no = $participantexam->batch_no;
                              $batch_no_full = $participantexam->batch_no_full;

                              $addParticipant = DB::table('participant_examresult')
                              ->insertGetId([
                                      'id_training_participant' => $id_training_participant,
                                      'exam_type' =>'New',
                                      'id_training' => $id_training,
                                      'id_training_course' => $id_training_course,
                                      'batch_no' => $batch_no,
                                      'batch_no_full' => $batch_no_full,
                                      'created_by' => $request->Createdby,
                                      'created_at' => Carbon::now(),
                                  
                              ]);
                      
                              if($addParticipant != null){

                                  $addParticipantTheory = DB::table('exam_theory')
                                  ->insert([
                                          'id_participant_examresult' => $addParticipant,
                                          'id_training' => $id_training,
                                          'id_training_course' => $id_training_course,
                                          'batch_no_full' => $batch_no_full,
                                          'created_by' => $request->Createdby,
                                          'created_at' => Carbon::now(),
                                        
                      
                                  ]);

                                  $addParticipantPractical = DB::table('exam_practical')
                                  ->insert([
                                          'id_participant_examresult' => $addParticipant,
                                          'id_training' => $id_training,
                                          'id_training_course' => $id_training_course,
                                          'batch_no_full' => $batch_no_full,
                                          'created_by' => $request->Createdby,
                                          'created_at' => Carbon::now(),
                                         
                      
                                  ]);


                                  $addParticipantFundamental = DB::table('exam_fundamental')
                                  ->insert([
                                          'id_participant_examresult' => $addParticipant,
                                          'id_training' => $id_training,
                                          'id_training_course' => $id_training_course,
                                          'batch_no_full' => $batch_no_full,
                                          'created_by' => $request->Createdby,
                                          'created_at' => Carbon::now(),
                                         
                      
                                  ]);

                              }


                          }

                      }

                return response()->json([
                      'status' => 'success',
                      'examlist' => $exam,
                      'checkHeaderName' => $checkHeaderName,
                      'headerName' => $headerName
                  ], 200);

          // Now $processedData contains the desired result without duplicate rows

  }


    Public function examDetailsTPSubmit(Request $request, $batch_full,$ID){
//var_dump($batch_full,$ID);
        $trainingDetails = DB::table('trainings')
        ->where('trainings.id_training_course','=',$ID)
        ->where('trainings.batch_no_full','=',$batch_full)
        ->leftJoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->select('trainings.id','trainings.batch_no','trainings.batch_no_full','training_course.id_training_course','training_course.course_name as training_name','trainings.venue','trainings.batch_no_full',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'))
    
        ->get();

        return response()->json([
            'status' => 'success',
            'trainingDetails' => $trainingDetails,
        ], 200);

    }

    Public function examDetailsAdmin2(Request $request,$ID, $batch_full){
        //var_dump($batch_full,$ID);
                $trainingDetails = DB::table('trainings')
                ->where('trainings.id_training_course','=',$ID)
                ->where('trainings.batch_no_full','=',$batch_full)
                ->leftJoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
                ->select('trainings.id','trainings.id_training_course','trainings.batch_no','trainings.batch_no_full','training_course.course_name as training_name','trainings.venue','trainings.batch_no_full',
                DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
                DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'))
            
                ->get();
        
                return response()->json([
                    'status' => 'success',
                    'trainingDetails' => $trainingDetails,
                ], 200);
        
            }
        

    Public function examNameListTPSubmit(Request $request,$batch_full,$ID){
        //  var_dump($ID);var_dump($batch_full);

        $checkHeaderName= DB::select("SELECT exam_result_header.id_training_course, exam_result_header.exam_1,exam_result_header.exam_2,
                       exam_result_header.exam_3, exam_result_header.exam_4,exam_result_header.exam_5,
                       exam_result_header.practical_exam_1,exam_result_header.practical_exam_2,
                       exam_result_header.practical_exam_3, exam_result_header.practical_exam_4,exam_result_header.practical_exam_5
                        FROM exam_result_header
                        WHERE exam_result_header.id_training_course = $ID");

                        $headerName = [];

                        // Loop through each exam column and strip tags
                        foreach ($checkHeaderName[0] as $key => $value) {
                            $headerName[$key] = strip_tags($value);
                        }


                      $exam = DB::select("
                      SELECT DISTINCT 
                      participant_examresult.id_training_participant,exam_practical.id_participant_examresult,participant_examresult.batch_no_full,
                      participant_examresult.exam_type,participant_examresult.submitted_status,
                      users.fullname,participant_examresult.id,
                      exam_practical.id_training,
                      exam_practical.exam_1 as part_A,
                      exam_practical.exam_2 as part_B,
                      exam_practical.exam_3 as part_C,
                      exam_practical.total_mark as total_mark_practical,
                      exam_practical.result_status as result_status_practical,
                      exam_theory.exam_1 as part_A_theory,
                      exam_theory.exam_2 as part_B_theory,
                      exam_theory.exam_3 as part_C_theory,
                      exam_theory.total_mark as total_mark_theory,
                      exam_theory.result_status as result_status_theory,
                      participant_examresult.final_verdict_status,
                      trainings.endorse_exam
                      from participant_examresult
                left join exam_practical on exam_practical.id_participant_examresult = participant_examresult.id 
                left join exam_theory on exam_theory.id_participant_examresult = participant_examresult.id 
                left join training_participant on training_participant.id = participant_examresult.id_training_participant
                left join trainings on trainings.id = participant_examresult.id_training
                left join users on users.id = training_participant.id_users 
                where participant_examresult.batch_no= '$batch_full' and participant_examresult.id_training_course = $ID
                and exam_theory.id_training_course = $ID and exam_practical.id_training_course = $ID ;
                      ");


                      

                return response()->json([
                      'status' => 'success',
                      'examlist' => $exam,
                      'headerName' => $headerName
                  ], 200);

        

  }

    Public function getExamResult(Request $request,$ID,$batch_full){
// var_dump($ID);var_dump($batch_full);
                        $exam = DB::select("
                        SELECT DISTINCT 
                        exam_practical.id_participant_examresult,
                        participant_examresult.id,participant_examresult.id_training,
                        participant_examresult.exam_type,
                        users.fullname,
                        exam_practical.exam_1 as part_A,
                        exam_practical.exam_2 as part_B,
                        exam_practical.exam_3 as part_C,
                        exam_practical.total_mark as total_mark_practical,
                        exam_practical.result_status as result_status_practical,
                        exam_theory.exam_1 as part_A_theory,
                        exam_theory.exam_2 as part_B_theory,
                        exam_theory.exam_3 as part_C_theory,
                        exam_theory.total_mark as total_mark_theory,
                        exam_theory.result_status as result_status_theory,
                        participant_examresult.final_verdict_status,participant_examresult.id
                        FROM STAR.training_participant
                        LEFT JOIN users ON users.id = training_participant.id_users
                        LEFT JOIN trainings  ON training_participant.id_training = trainings.id
                        LEFT JOIN participant_examresult ON 
                        participant_examresult.id_training_participant = training_participant.id
                         LEFT JOIN STAR.exam_theory ON 
                        exam_theory.id_participant_examresult = participant_examresult.id
                         LEFT JOIN STAR.exam_practical ON 
                        exam_practical.id_participant_examresult = participant_examresult.id
                        where participant_examresult.batch_no_full= '$batch_full' and participant_examresult.id = $ID
                    ");

                return response()->json([
                    'status' => 'success',
                    'exam' => $exam,
                ], 200);

    }

    public function editExamResult(Request $request){

        if($request->examArray !== null){
            foreach($request->examArray as $data)
            {

               
                $partA = $data['part_A'] ?? '';
                $partB = $data['part_B'] ?? '';
                $partC = $data['part_C'] ?? '';
                $total_Mark_practical = $data['total_Mark_practical'] ?? '';
                $result_status = $data['result_Status_practical'] ?? '';
                

                $part_A_theory = $data['part_A_theory'] ?? '';
                $part_B_theory = $data['part_B_theory'] ?? '';
                $part_C_theory = $data['part_C_theory'] ?? '';
                $total_Mark_theory = $data['total_Mark_theory'] ?? '';
                $result_Status_theory =  $data['result_Status_theory'] ?? '';


                

                $practical = DB::table('exam_practical')
                ->where('exam_practical.id_participant_examresult', '=', $data['id_Participant_examresult'])
                ->where('exam_practical.id_training', '=', $data['id_Training'])
                
                    ->update([
                        'exam_1' => $partA,
                        'exam_2' => $partB,
                        'exam_3' => $partC,

                        'total_mark' => $total_Mark_practical,
                        'result_status' => $result_status,
                    
                    ]);

    

                    $theory = DB::table('exam_theory')
                    ->where('exam_theory.id_participant_examresult', '=', $data['id_Participant_examresult'])
                    ->where('exam_theory.id_training', '=', $data['id_Training'])
                
                    ->update([
                        'exam_1' => $part_A_theory,
                        'exam_2' => $part_B_theory,
                        'exam_3' => $part_C_theory,
            
                        'total_mark' => $total_Mark_theory,
                        'result_status' => $result_Status_theory,
                        
                    ]);


                    $finalresult = DB::table('participant_examresult')
                    ->where('participant_examresult.id_training', '=',  $data['id_Training'])
                    ->where('participant_examresult.id_training_participant', '=', $data['id_Training_participant'])
                
                    ->update([
                        
                        'final_verdict_status' => $data['final_Verdict_status'],
                        
                    ]);

            }

                return response()->json([
                'practical' => $practical,
                'theory' => $theory,
                'finalresult' => $finalresult
                ]);

        }
    }

    public function editExamResult_GCPV(Request $request){

        if($request->examArray !== null){
            foreach($request->examArray as $data)
            {

               
                $partA = $data['part_A'] ?? '';
                $partB = $data['part_B'] ?? '';
                $partC = $data['part_C'] ?? '';
                $total_Mark_practical = $data['total_Mark_practical'] ?? '';
                $result_status = $data['result_Status_practical'] ?? '';
                

                $part_A_theory = $data['part_A_theory'] ?? '';
                $part_B_theory = $data['part_B_theory'] ?? '';
                $part_C_theory = $data['part_C_theory'] ?? '';
                $total_Mark_theory = $data['total_Mark_theory'] ?? '';
                $result_Status_theory =  $data['result_Status_theory'] ?? '';


                

                $practical = DB::table('exam_practical')
                ->where('exam_practical.id_participant_examresult', '=', $data['id_Participant_examresult'])
                ->where('exam_practical.id_training', '=', $data['id_Training'])
                
                    ->update([
                        'exam_1' => $partA,
                        'exam_2' => $partB,
                        'exam_3' => $partC,

                        'total_mark' => $total_Mark_practical,
                        'result_status' => $result_status,
                    
                    ]);

    

                    $theory = DB::table('exam_theory')
                    ->where('exam_theory.id_participant_examresult', '=', $data['id_Participant_examresult'])
                    ->where('exam_theory.id_training', '=', $data['id_Training'])
                
                    ->update([
                        'exam_1' => $part_A_theory,
                        'exam_2' => $part_B_theory,
                        'exam_3' => $part_C_theory,
            
                        'total_mark' => $total_Mark_theory,
                        'result_status' => $result_Status_theory,
                        
                    ]);


                    $finalresult = DB::table('participant_examresult')
                    ->where('participant_examresult.id_training', '=',  $data['id_Training'])
                    ->where('participant_examresult.id_training_participant', '=', $data['id_Training_participant'])
                
                    ->update([
                        
                        'final_verdict_status' => $data['final_Verdict_status'],
                        
                    ]);

            }

                return response()->json([
                'practical' => $practical,
                'theory' => $theory,
                'finalresult' => $finalresult
                ]);

        }
    }


    public function editExamResultEndorsed(Request $request){

        $checksubmision = DB::table('trainings')
                            ->where('trainings.batch_no_full', '=', $request->Batch_No)
                            ->where('trainings.id_training_course', '=', $request->Course_id)    
                            ->select('trainings.endorse_exam', 'trainings.endorse_feedback','trainings.endorse_attendance','trainings.endorse_report')
                            ->first();
      
       // dd($checksubmision->endorse_exam);//if fullfilled
       if (($checksubmision->endorse_exam=='1') && ($checksubmision->endorse_feedback=='1') &&
        ($checksubmision->endorse_attendance=='1')&&($checksubmision->endorse_report=='1'))
        {
    
            //to update endorsed status
            $endorsed = DB::table('participant_examresult')
                        ->where('participant_examresult.batch_no_full', '=', $request->Batch_No)
                        ->where('participant_examresult.id_training_course', '=', $request->Course_id)            
                        ->update([             
                            'endorsed_status' => 1,
                            'endorsed_by' => $request->userID,
                            'endorsed_date' => Carbon::now(),
                        ]);

            //to update endorsed status
            $endorsedintrainings = DB::table('trainings')
                        ->where('trainings.batch_no_full', '=', $request->Batch_No)
                        ->where('trainings.id_training_course', '=', $request->Course_id)            
                        ->update([             
                            'endorsed_status' => 1,
                        ]);
     
            //to get all id batch  
            $selectuserToEndorsed = DB::table('participant_examresult')
            ->where('participant_examresult.batch_no', '=', $request->Batch_No)
            ->select('participant_examresult.id_training_participant', 'participant_examresult.batch_no')
            ->get();
        
 
            $itemCount = count($selectuserToEndorsed);

            for ($i = 0; $i < $itemCount; $i++) {
                $item = $selectuserToEndorsed[$i];
                $idTrainingParticipant = $item->id_training_participant;
                
                // Generate cert no
                $genCertNo = 'SPC' . rand(1000, 9999);

                // Insert cert no
                $genCert = DB::table('participant_certificate')
                    ->insert([
                        'id_training_participant' => $idTrainingParticipant,
                        'certificate_no' => $genCertNo,
                        'created_at' => Carbon::now(),
                    ]);

                
            }
    
    

            $selectuserToNotify = DB::table('participant_examresult')
            ->where('participant_examresult.batch_no', '=', $request->Batch_No)
            ->leftjoin('participant_certificate','participant_certificate.id_training_participant','=','participant_examresult.id_training_participant')
            ->select('participant_examresult.id_training_participant', 'participant_examresult.batch_no')
            ->groupby('participant_examresult.id_training_participant')
            ->get();

          //  dd(count($selectuserToNotify));exit;

            $count_cert = 0; // Initialize the count variable
            foreach ($selectuserToNotify as $item2) {

                    $idTrainingParticipant = $item2->id_training_participant;

               //     dd($idTrainingParticipant);exit;
                
                    $toVerifyCert = DB::table('participant_examresult')
                    ->where('participant_examresult.batch_no', '=', $request->Batch_No)
                    ->where('participant_examresult.id_training_participant', '=', $idTrainingParticipant)
                    ->select('participant_examresult.id_training_participant', 'participant_examresult.id_training', 'participant_examresult.batch_no',
                    'participant_examresult.final_verdict_status',
                    'users.fullname','users.ic_no as mykad_no', 'gender.name as gender', 'users.id_bumiputra as nationality', 'users.phone_no as mobile_no','users.email',
                    'users.position as qualification_background', 'users.address_1 as line1', 'users.address_1 as line2', 'users.address_3 as line3',
                    'users.postcode','users.town as city', 'users.id_state as state','users.id_country','country.nicename as country',
                    'training_course.course_name as category',
                    'participant_certificate.certificate_no','company.name as institute_name','company.code as institute_code')
                
                    ->leftjoin('users', 'users.id', '=', 'participant_examresult.id_training_participant')
                    ->leftjoin('country', 'country.id', '=', 'users.id_country')
                    ->leftjoin('gender', 'gender.id', '=', 'users.id_gender')
                    ->leftjoin('trainings', 'trainings.id', '=', 'participant_examresult.id_training')
                    ->leftjoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
                    ->leftjoin('participant_certificate', 'participant_certificate.id_training_participant', '=', 'users.id')
                    ->leftjoin('company','company.id', '=', 'trainings.id_company')
                    ->groupby('participant_examresult.id_training_participant')
                    ->get();
               

                    foreach ($toVerifyCert as $result) {

                        
                        // $genCert = DB::table('participant_certificate_test')
                        // ->insert([
                        //     'id_training_participant' => $result->id_training_participant,
                        //     'certificate_no' => $genCertNo,
                        //     'certificate_name' => $result->fullname,
                        //     'created_at' => Carbon::now(),
                        // ]);
                        
                
                

                        if ($result->gender == "Male") {
                            $gender = 'M';
                        } else {
                            $gender = 'F';
                        }



                        //        //to notify with seda
                        $data = [
                            'profile' => [
                                'name' => $result->fullname,
                                'mykad_no' => $result->mykad_no,
                                'gender' => $gender,
                                'nationality' => 'MALAYSIAN',
                                'mobile_no' =>  $result->mobile_no,
                                'email' => $result->email,
                                'qualification_background' => 'Degree in mechanical',
                                'identification_type' => 'mykad', // mykad_passport. *required
                            ],
                            'address' => [
                                'line1' => $result->line1,
                                'line2' => $result->line2,
                                'line3' => $result->line3,
                                'postcode' => $result->postcode,
                                'city' => $result->state,
                                'state' => '01',
                                'country' => 'MALAYSIA',
                            ],
                            'certificate' => [
                                'category' => $result->category, // *required. GCPV Systems Design / Wireman Chargeman Solar PV
                                'certificate_no' => $result->certificate_no,
                                'institute_name' => $result->institute_name,
                                'institute_code' =>  $result->institute_code,
                            // 'batch_no' => $toVerifyCert->first()->batch_no,
                            'batch_no' => 'batch-001',
                            ],
                        ];
                
                
                
                        $response = Http::withOptions([
                            'verify' => false,
                            ])->withHeaders([
                            'Authorization' => 'Bearer '. env('ACCESS_TOKEN'),
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            ])->post('https://spqp-staging.seda.gov.my/api/qualified_person/notify', $data);

                    
                  
                    }

                $count_cert++;
            
            }//end foreach

             if ($jsonData['status'] == 'OK') {
             //   dd($jsonData['status']);
                return response()->json($jsonData);
             } else {
            //     // Handle the case where the 'status' key is not present or doesn't equal 'OK'
            //     // You can log or return an error response as needed
               return response()->json($jsonData);
            //  //   dd($jsonData['status']);
             }
         
          
            }else{
                return response()->json([
                    'checksubmision' => $checksubmision,
                 
                ]);
            }
          
       
       }




       public function editExamResultQuery(Request $request){


        $getTPname = DB::table('trainings')
        ->where('trainings.batch_no_full','=',$request->Batch_No)
        ->where('trainings.id_training_course','=',$request->Id_course)
        ->leftjoin('company','company.id','=','trainings.id_company')
        ->leftjoin('participant_examresult','participant_examresult.batch_no','=','trainings.batch_no')
        ->leftjoin('training_trainer','training_trainer.id_training','=','trainings.id')
        ->leftjoin('trainer','trainer.id','=','training_trainer.id_trainer')
        ->leftjoin('users','users.id','=','trainer.id_users')
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->select('trainings.id','trainings.id_training_course','trainings.batch_no','trainings.batch_no_full','trainings.venue',
    'trainings.id_training_status','trainings.id_company','trainings.status','users.fullname','users.email',
    'participant_examresult.endorsed_status','company.name as trainingpartner','training_course.course_name',
    DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
    DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
    'training_course.id_training_option')
    // ->where('trainings.deleted_at','=','')
    ->orderby('id_training_status','desc')
    ->groupby('batch_no')

    ->first();
       
    //dd($examlist->fullname);
       
        $sendquery = DB::table('participant_examresult')
        ->where('participant_examresult.batch_no', '=', $request->Batch_No)
        ->where('participant_examresult.id_training_course', '=', $request->Id_course)
       
        ->update([
            
            'endorsed_status' => 2,
            'endorsed_date' => Carbon::now(),
            
        ]);

        $findTP="";
        // Route::get('send-mail', function () {

            $details = [
                'title' => 'Mail from Seda STAR',
                'body' => 'Please change participant mark',
                'subject' => '',
                'url' => ''
            ];
        
            //Mail::to('nurulaiza@seetrustudio.com')->send(new \App\Mail\MyTestMail($details));
        
           // dd("Email is Sent.");
        // });

    

      Mail::to($getTPname->email)->send(new QueryExam($getTPname));
      //Mail::to('nurulaiza@seetrustudio.com')->send(new QueryExam($getTPname));


         return response()->json([
        'sendquery' => $sendquery,
     
    ]);


}


            public function submitExamToEndorse(Request $request){
                
                  //  dd($request);exit;
                $endorse_exam = DB::table('trainings')
                ->where('trainings.batch_no', '=', $request->Batch_No)
                ->where('trainings.id', '=', $request->Training_ID)
            
                ->update([
                    
                    'endorse_exam' => 1, //submitted to endorse
                    'updated_at' => Carbon::now(),
                    
                ]);


                return response()->json([
                'endorse_exam' => $endorse_exam,
            
                ]);

            }


            public function submitFeedbackToEndorse(Request $request){
                
                 // dd($request->all);exit;
              $endorse_exam = DB::table('trainings')
              ->where('trainings.batch_no', '=', $request->Batch_No)
              ->where('trainings.id', '=', $request->Training_ID)
          
              ->update([
                  
                  'endorse_feedback' => 1, //submitted to endorse
                  'updated_at' => Carbon::now(),
                  
              ]);


              return response()->json([
              'endorse_exam' => $endorse_exam,
          
              ]);

            }



            


    Public function examNameListAdmin(Request $request,$batch_full,$ID){
       //   var_dump($ID);

               
                    $checkHeaderName= DB::select("SELECT exam_result_header.id_training_course, exam_result_header.exam_1,exam_result_header.exam_2,
                    exam_result_header.exam_3, exam_result_header.exam_4,exam_result_header.exam_5,
                    exam_result_header.practical_exam_1,exam_result_header.practical_exam_2,
                    exam_result_header.practical_exam_3, exam_result_header.practical_exam_4,exam_result_header.practical_exam_5
                     FROM exam_result_header
                     WHERE exam_result_header.id_training_course = $ID");

                     $headerName = [];

                     // Loop through each exam column and strip tags
                     foreach ($checkHeaderName[0] as $key => $value) {
                         $headerName[$key] = strip_tags($value);
                     }

                    $exam = DB::select("
                    SELECT DISTINCT 
                      participant_examresult.id_training_participant,exam_practical.id_participant_examresult,participant_examresult.batch_no_full,
                      participant_examresult.exam_type,participant_examresult.submitted_status,
                      users.fullname,participant_examresult.id,
                      exam_practical.id_training,
                      exam_practical.exam_1 as part_A,
                      exam_practical.exam_2 as part_B,
                      exam_practical.exam_3 as part_C,
                      exam_practical.total_mark as total_mark_practical,
                      exam_practical.result_status as result_status_practical,
                      exam_theory.exam_1 as part_A_theory,
                      exam_theory.exam_2 as part_B_theory,
                      exam_theory.exam_3 as part_C_theory,
                      exam_theory.total_mark as total_mark_theory,
                      exam_theory.result_status as result_status_theory,
                      participant_examresult.final_verdict_status
                      from participant_examresult
                left join exam_practical on exam_practical.id_participant_examresult = participant_examresult.id 
                left join exam_theory on exam_theory.id_participant_examresult = participant_examresult.id 
                left join training_participant on training_participant.id = participant_examresult.id_training_participant
                left join users on users.id = training_participant.id_users 
                where participant_examresult.batch_no_full= '$batch_full' and participant_examresult.id_training_course = $ID
                and exam_theory.id_training_course = $ID and exam_practical.id_training_course = $ID ;
                    
                    ");
                     

                return response()->json([
                      'status' => 'success',
                      'examlist' => $exam,
                      'headerName' => $headerName
                  ], 200);

    
  }



  Public function examNameListAdminCGPV(Request $request,$batch_full,$ID){
     // var_dump($ID);

            
                 $checkHeaderName= DB::select("SELECT examGCPV_result_header.id_training_course,
                 examGCPV_result_header.exam_1,
                 examGCPV_result_header.exam_2,
                 examGCPV_result_header.exam_3, 
                 examGCPV_result_header.design_exam_1,
                 examGCPV_result_header.design_exam_2,
                 examGCPV_result_header.design_exam_3,
                 examGCPV_result_header.practical_exam_1,
                 examGCPV_result_header.practical_exam_2,
                 examGCPV_result_header.practical_exam_3
                  FROM examGCPV_result_header
                  WHERE examGCPV_result_header.id_training_course = $ID");

                  $headerName = [];

                  // Loop through each exam column and strip tags
                  foreach ($checkHeaderName[0] as $key => $value) {
                      $headerName[$key] = strip_tags($value);
                  }

                 $exam = DB::select("
                 SELECT DISTINCT 
                   participant_examresult.id_training_participant,exam_practical.id_participant_examresult,participant_examresult.batch_no_full,
                   participant_examresult.exam_type,participant_examresult.submitted_status,
                   users.fullname,participant_examresult.id,

                  
                   exam_fundamental.exam_1 as part_A_fundamental,
                   exam_fundamental.exam_2 as part_B_fundamental,
                   exam_fundamental.exam_3 as part_C_fundamental,
                   exam_fundamental.total_mark as total_mark_fundamental,
                   exam_fundamental.result_status as result_status_fundamental,
                  

                   exam_theory.exam_1 as part_A_design,
                   exam_theory.exam_2 as part_B_design,
                   exam_theory.exam_3 as part_C_design,
                   exam_theory.total_mark as total_mark_design,
                   exam_theory.result_status as result_status_design,

                   exam_practical.id_training,
                   exam_practical.exam_1 as part_A_practical,
                   exam_practical.exam_2 as part_B_practical,
                   exam_practical.exam_3 as part_C_practical,
                   exam_practical.total_mark as total_mark_practical,
                   exam_practical.result_status as result_status_practical,

                   

                   participant_examresult.final_verdict_status
                   from participant_examresult
             left join exam_practical on exam_practical.id_participant_examresult = participant_examresult.id 
             left join exam_theory on exam_theory.id_participant_examresult = participant_examresult.id 
             left join exam_fundamental on exam_fundamental.id_participant_examresult = participant_examresult.id 
             left join training_participant on training_participant.id = participant_examresult.id_training_participant
             left join users on users.id = training_participant.id_users 
             where participant_examresult.batch_no_full= '$batch_full' and participant_examresult.id_training_course = $ID
             and exam_theory.id_training_course = $ID and exam_practical.id_training_course = $ID and 
             exam_fundamental.id_training_course = $ID;
                 
                 ");
                  

             return response()->json([
                   'status' => 'success',
                   'examlist' => $exam,
                   'headerName' => $headerName
               ], 200);

 
}
  
}

    
