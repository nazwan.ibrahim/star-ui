<?php

namespace App\Http\Controllers\API\Login;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Users;



class ResetPasswordAPIController extends Controller
{
    public function resetpassword(Request $request) {

 //var_dump($request->UserName.bcrypt($request->NewPassword).bcrypt($request->ConfirmPassword));exit;
        $kemaskiniUserPassword = Users::where('email', '=', $request->UserName);

        

        $kemaskiniUserPassword->update([
           
             'password' => bcrypt($request->NewPassword),
             'password_confirmation' => bcrypt($request->ConfirmPassword),
             'updated_at' => Carbon::now(),
            // 'postcode' => $request->NewPassword,
            // 'address_2' => $request->ConfirmPassword,
           
        ]);


        return response()->json([
            'status' => 'success',
            'kemaskiniUserPassword' => $kemaskiniUserPassword,
        ], 200);

    }

   

}
