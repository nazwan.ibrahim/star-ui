<?php

namespace App\Http\Controllers\API\ParameterManagement;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Training;
use App\Models\TrainingCourse;
use App\Models\courseReType;
use Illuminate\Support\Facades\DB;


class ParameterMgmtAPIController extends Controller
{
    
    public function countryList()
    {

        $countrylist = DB::table('country') 
        ->get();

       if (!$countrylist) {
        return response()->json([
            'status' => 'error',
            'message' => 'Country not found',
        ], 404);
        }else{
            return response()->json([
                'status' => 'success',
                'countrylist' => $countrylist,
            ], 200);
        }
    }

    public function genderList()
    {

        $genderlist = DB::table('gender') 
        ->get();

       if (!$genderlist) {
        return response()->json([
            'status' => 'error',
            'message' => 'Gender not found',
        ], 404);
        }else{
            return response()->json([
                'status' => 'success',
                'genderlist' => $genderlist,
            ], 200);
        }
    }

    public function certCodeList()
    {

        $certCodelist = DB::table('training_cert_code') 
        ->get();

       if (!$certCodelist) {
        return response()->json([
            'status' => 'error',
            'message' => 'Certificate Code not found',
        ], 404);
        }else{
            return response()->json([
                'status' => 'success',
                'certCodelist' => $certCodelist,
            ], 200);
        }
    }

    public function feeTypeList()
    {

        $feeTypelist = DB::table('fee_type') 
        ->get();

       if (!$feeTypelist) {
        return response()->json([
            'status' => 'error',
            'message' => 'Fee Type not found',
        ], 404);
        }else{
            return response()->json([
                'status' => 'success',
                'feeTypelist' => $feeTypelist,
            ], 200);
        }
    }

    public function trainingOptionList()
    {

        $trainingOptionlist = DB::table('training_option') 
        ->get();

       if (!$trainingOptionlist) {
        return response()->json([
            'status' => 'error',
            'message' => 'Training Option not found',
        ], 404);
        }else{
            return response()->json([
                'status' => 'success',
                'trainingOptionlist' => $trainingOptionlist,
            ], 200);
        }
    }

    public function eduLevelList()
    {

        $eduLevelList = DB::table('education_level') 
        ->get();

       if (!$eduLevelList) {
        return response()->json([
            'status' => 'error',
            'message' => 'Education not found',
        ], 404);
        }else{
            return response()->json([
                'status' => 'success',
                'eduLevelList' => $eduLevelList,
            ], 200);
        }
    }

    public function trainingModeList()
    {

        $trainingModelist = DB::table('training_mode') 
        ->get();

       if (!$trainingModelist) {
        return response()->json([
            'status' => 'error',
            'message' => 'Training Mode not found',
        ], 404);
        }else{
            return response()->json([
                'status' => 'success',
                'trainingModelist' => $trainingModelist,
            ], 200);
        }
    }

    public function trainingTypeList()
    {

        $trainingTypelist = DB::table('training_type') 
        ->get();

       if (!$trainingTypelist) {
        return response()->json([
            'status' => 'error',
            'message' => 'Training Type not found',
        ], 404);
        }else{
            return response()->json([
                'status' => 'success',
                'trainingTypelist' => $trainingTypelist,
            ], 200);
        }
    }

    public function reTypeList()
    {

        $reTypelist = DB::table('re_type') 
        ->get();

       if (!$reTypelist) {
        return response()->json([
            'status' => 'error',
            'message' => 'RE Type not found',
        ], 404);
        }else{
            return response()->json([
                'status' => 'success',
                'reTypelist' => $reTypelist,
            ], 200);
        }
    }
}
