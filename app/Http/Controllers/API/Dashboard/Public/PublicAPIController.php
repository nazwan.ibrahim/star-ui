<?php

namespace App\Http\Controllers\API\Dashboard\Public;


use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Training;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PublicAPIController extends Controller {

    public function AllList(Request $request){
        $now = Carbon::now();

        try{

            $AllList = DB::table('trainings as t')
            ->leftJoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
            ->leftJoin('course_category as cc', 'tc.id_course_category', '=', 'cc.id')
            ->leftjoin('status_training as st', 't.id_training_status', '=', 'st.id')
            ->leftjoin('state', 'state.id', '=', 't.id_state')
            ->where('t.id_approval_status', '=', 2)
            ->where('t.id_training_status', '=', 1)
            ->whereNotNull('date_start')
            ->whereNotNull('date_end')
            ->select(
                't.id as training_id',
                'tc.course_name as programme_name',
                DB::raw('DATE_FORMAT(t.date_start, "%d/%m/%Y") as date_start'),
                DB::raw('DATE_FORMAT(t.date_end, "%d/%m/%Y") as date_end'),
                'cc.id as course_id',
                't.venue',
                'tc.new_fee',
                'st.id as status_id',
                'st.status_name',
                'state.name'
            )
            ->orderby('t.date_start','asc')
            ->get();
            
                Log::info('Successfully fetched training list.');

                return response()->json([
                    'status' => 'success',
                    'AllList' => $AllList,
                ], 200);
            
        }
        catch (\Exception $e) {
            Log::error('Error fetching training list: ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

    public function searchCourse()
    {
        $now = Carbon::now();
      
            $trainingCourse = DB::table('training_course')
            ->leftJoin('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
            ->select('trainings.id','training_course.id_training_course',
                     'training_course.course_name','trainings.venue'
                    ) 
                    ->where('trainings.id_approval_status', '=', 2)
                    ->where('trainings.id_training_status', '=', 1)
                    ->whereDate('date_start', '>=', $now->toDateString())
                    ->whereDate('date_end', '>=', $now->toDateString())
                    ->groupby('trainings.id_training_course')
                    ->get();

                    $trainingVenue = DB::table('training_course')
                    ->leftJoin('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
                    ->select('trainings.id','training_course.id_training_course',
                             'training_course.course_name','trainings.venue'
                            ) 
                            ->where('trainings.id_approval_status', '=', 2)
                            ->where('trainings.id_training_status', '=', 1)
                            ->whereDate('date_start', '>=', $now->toDateString())
                            ->whereDate('date_end', '>=', $now->toDateString())
                            ->get();
               
                return response()->json([
                    'status' => 'success',
                    'trainingCourse' => $trainingCourse,
                    'trainingVenue' =>$trainingVenue,
                ], 200);
        
    }

    public function searchState()
    {
        $now = Carbon::now();
      
            $trainingState = DB::table('training_course')
            ->leftJoin('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
            ->leftJoin('state', 'state.id', '=', 'trainings.id_state')
            ->select('training_course.id_training_course',
                     'training_course.course_name','trainings.venue', 'state.name'
                    ) 
                    ->whereDate('date_start', '>=', $now->toDateString())
                    ->whereDate('date_end', '>=', $now->toDateString())
                    ->where('trainings.id_approval_status', '=', 2)
                    ->groupby('trainings.id_state')
                    ->get();

                    $trainingDate = DB::table('training_course')
                    ->leftJoin('trainings', 'trainings.id_training_course', '=', 'training_course.id_training_course')
                    ->select('training_course.id_training_course',
                             'training_course.course_name','trainings.venue',
                                DB::raw('DATE_FORMAT(trainings.date_start, "%d/%m/%Y") as date_start'),
                                DB::raw('DATE_FORMAT(trainings.date_end, "%d/%m/%Y") as date_end'),
                            ) 
                            ->whereDate('date_start', '>=', $now->toDateString())
                            ->whereDate('date_end', '>=', $now->toDateString())
                            ->where('trainings.id_approval_status', '=', 2)
                            ->groupby('trainings.id_state')
                            ->get();       

               
                return response()->json([
                    'status' => 'success',
                    'trainingState' => $trainingState,
                    'trainingDate' => $trainingDate,
                ], 200);
        
    }
    
    public function SEList(Request $request){
        try{
            $SEList = DB::table('trainings as t')
            ->leftJoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
            ->leftJoin('course_category as cc', 'tc.id_course_category', '=', 'cc.id')
            ->where('cc.id', 2)
            ->where('t.id_training_status', 3)
            ->whereNotNull('date_start')
            ->whereNotNull('date_end')
            ->select(
                'tc.course_name as programme_name',
                DB::raw('DATE_FORMAT(t.date_start, "%d/%m/%Y") as date_start'),
                DB::raw('DATE_FORMAT(t.date_end, "%d/%m/%Y") as date_end'),
                'cc.id as course_id',
                't.venue',
                't.fee',
                't.status'
            )
            ->get();
            
                Log::info('Successfully fetched training list.');

                $SECounts = $SEList->groupBy('programme_name')->map->count();

                return response()->json([
                    'status' => 'success',
                    'SECounts' => $SECounts,
                    'SEList' => $SEList,
                ], 200);
            
        }
        catch (\Exception $e) {
            Log::error('Error fetching course data : ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

    public function REList(Request $request){
        try{
            $REList = DB::table('trainings as t')
            ->leftJoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
            ->leftJoin('course_category as cc', 'tc.id_course_category', '=', 'cc.id')
            ->where('cc.id', 1)
            ->where('t.id_training_status', 3)
            ->whereNotNull('date_start')
            ->whereNotNull('date_end')
            ->select(
                'tc.course_name as programme_name',
                DB::raw('DATE_FORMAT(t.date_start, "%d/%m/%Y") as date_start'),
                DB::raw('DATE_FORMAT(t.date_end, "%d/%m/%Y") as date_end'),
                'cc.id as course_id',
                't.venue',
                't.fee',
                't.status'
            )
            ->get();
           
                Log::info('Successfully fetched training list.');

                $RECounts = $REList->groupBy('programme_name')->map->count();

                return response()->json([
                    'status' => 'success',
                    'RECounts' => $RECounts,
                    'REList' => $REList,
                ], 200);
            
        }
        catch (\Exception $e) {
            Log::error('Error fetching course data : ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

    public function EEList(Request $request){
        try{
            $EEList = DB::table('trainings as t')
            ->leftJoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
            ->leftJoin('course_category as cc', 'tc.id_course_category', '=', 'cc.id')
            ->where('cc.id', 3)
            ->where('t.id_training_status', 3)
            ->whereNotNull('date_start')
            ->whereNotNull('date_end')
            ->select(
                'tc.course_name as programme_name',
                DB::raw('DATE_FORMAT(t.date_start, "%d/%m/%Y") as date_start'),
                DB::raw('DATE_FORMAT(t.date_end, "%d/%m/%Y") as date_end'),
                'cc.id as course_id',
                't.venue',
                't.fee',
                't.status'
            )
            ->get();
            
                Log::info('Successfully fetched training list.');

                $EECounts = $EEList->groupBy('programme_name')->map->count();

                return response()->json([
                    'status' => 'success',
                    'EECounts' => $EECounts,
                    'EEList' => $EEList,
                ], 200);
            
        }
        catch (\Exception $e) {
            Log::error('Error fetching course data : ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

    public function TrainingCards(Request $request){
        try{
            $TrainingCards = DB::table('trainings as t')
            ->leftJoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
            ->whereNotNull('date_start')
            ->whereNotNull('date_end')
            ->select(
                DB::raw('DATE_FORMAT(t.date_start, "%d/%m/%Y") as date_start'),
                'tc.course_name as course_name',
                't.training_image as training_image',
                't.venue as training_venue',
                't.fee as training_fee',
            )
            ->get();
            
                Log::info('Successfully fetched training list.');

                return response()->json([
                    'status' => 'success',
                    'TrainingCards' => $TrainingCards,
                ], 200);
            
        }
        catch (\Exception $e) {
            Log::error('Error fetching course data : ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

    public function CompetencyChart(Request $request){
        try{
            $TrainingCount = DB::table('trainings as t')
            ->leftJoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
            ->whereNotNull('date_start')
            ->whereNotNull('date_end')
            ->where('t.id_training_status', 3)
            ->select(
                DB::raw('DATE_FORMAT(t.date_end, "%d/%m/%Y") as date_end'),
                'tc.course_name as course_name',
            )
            ->get();
            
                Log::info('Successfully fetched training list.');

                return response()->json([
                    'status' => 'success',
                    'TrainingCount' => $TrainingCount,
                ], 200);
            
        }
        catch (\Exception $e) {
            Log::error('Error fetching course data : ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

    public function TPList(Request $request, $ID){
        try{
            $TPList = DB::table('trainings as t')
            ->leftJoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
            ->leftJoin('course_category as cc', 'tc.id_course_category', '=', 'cc.id')
            ->whereNotNull('date_start')
            ->whereNotNull('date_end')
            ->where('training.id_company', '=', $ID)
            ->select(
                't.training_name as programme_name',
                DB::raw('DATE_FORMAT(t.date_start, "%d/%m/%Y") as date_start'),
                DB::raw('DATE_FORMAT(t.date_end, "%d/%m/%Y") as date_end'),
                'cc.id as course_id',
                't.venue',
                't.fee',
                't.status'
            )
            ->get();
            
                Log::info('Successfully fetched training list.');

                return response()->json([
                    'status' => 'success',
                    'TPList' => $TPList,
                ], 200);
            
        }
        catch (\Exception $e) {
            Log::error('Error fetching course data : ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }
    
    public function TrainerList(Request $request, $ID){
        try{
            $TrainerList = DB::table('trainings as t')
            ->leftJoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
            ->leftJoin('course_category as cc', 'tc.id_course_category', '=', 'cc.id')
            ->leftJoin('training_trainer as tt', 't.id_', '=', 'tt.id')
            ->whereNotNull('date_start')
            ->whereNotNull('date_end')
            ->where('training_trainer.id_trainer', '=', $ID)
            ->select(
                't.training_description as programme_name',
                DB::raw('DATE_FORMAT(t.date_start, "%d/%m/%Y") as date_start'),
                DB::raw('DATE_FORMAT(t.date_end, "%d/%m/%Y") as date_end'),
                'cc.id as course_id',
                't.venue',
                't.fee',
                't.status'
            )
            ->get();
            
                Log::info('Successfully fetched training list.');

                return response()->json([
                    'status' => 'success',
                    'TrainerList' => $TrainerList,
                ], 200);
            
        }
        catch (\Exception $e) {
            Log::error('Error fetching course data : ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function AllTrainingList(Request $request){
        $now = Carbon::now();

        try{

            $AllTrainingList = DB::table('trainings as t')
            ->leftJoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
            ->leftJoin('course_category as cc', 'tc.id_course_category', '=', 'cc.id')
            ->leftJoin('training_type as ttp', 'tc.id_training_type', '=', 'ttp.id')
            ->leftjoin('status_training as st', 't.id_training_status', '=', 'st.id')
            ->leftjoin('state', 'state.id', '=', 't.id_state')
            ->where('t.id_approval_status', '=', 2)
            ->where('t.id_training_status', '=', 1)
            ->where('date_start','>',$now)
            ->whereNotNull('date_start')
            ->whereNotNull('date_end')
            ->select(
                't.id as training_id',
                'tc.course_name as programme_name',
                DB::raw('DATE_FORMAT(t.date_start, "%d/%m/%Y") as date_start'),
                DB::raw('DATE_FORMAT(t.date_end, "%d/%m/%Y") as date_end'),
                'cc.id as course_id',
                't.venue',
                'cc.course_name as course_category',
                'ttp.name as type_name',
                't.batch_no_full',
                't.id_approval_status',
                'tc.new_fee',
                'st.id as status_id',
                'st.status_name',
                'state.name'
            )
            ->orderby('t.date_start','asc')
            ->get();
            
                Log::info('Successfully fetched training list.');

                return response()->json([
                    'status' => 'success',
                    'AllTrainingList' => $AllTrainingList,
                ], 200);
            
        }
        catch (\Exception $e) {
            Log::error('Error fetching training list: ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }
}