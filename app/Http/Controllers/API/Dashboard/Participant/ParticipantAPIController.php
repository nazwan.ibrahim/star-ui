<?php
namespace App\Http\Controllers\API\Dashboard\Participant;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use stdClass;

class ParticipantAPIController extends Controller
{
    public function ParticipantTrainingList(Request $request,$ID){
                //dd($ID);
        $participantTraining =  DB::table('trainings')
        ->whereNull('trainings.deleted_at')
        ->select([
            'training_course.course_name as training_name',
            'trainings.id',
            DB::raw('DATE_FORMAT(trainings.date_start, "%d/%m/%Y") as date_start'),
            DB::raw('DATE_FORMAT(trainings.date_end, "%d/%m/%Y") as date_end'),
            'trainings.venue',
            'training_participant.training_status',
            'training_participant.id_approval_status',
            'training_participant.id_training',
            'training_participant.id_users',
        ])
        ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
        ->leftJoin('training_participant', 'training_participant.id_training', '=', 'trainings.id')
        ->leftJoin('users', 'users.id', '=', 'training_participant.id_users')
        ->where('training_participant.id_users', '=', $ID)
        ->orderby('trainings.date_start','DESC')
        ->get();

        return response()->json([
            'status' => 'success',
            'participantTraining' => $participantTraining,
        ], 200);
    }

    public function ParticipantTrainingDetails(Request $request,$ID){
        // dd($ID);

        $detailsTraining =  DB::table('trainings')
        ->select([
            'training_course.course_name as training_name',
            'trainings.id',
            'date_start',
            'date_end',
            'venue_address',
            'company.name as provider_name',
            'trainings.fee',
            'trainings.fee_receipt',
            'trainings.is_paid',
            'trainings.is_resit',
        ])
        ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
        ->leftJoin('training_partner_course','training_partner_course.id_training_course','=','training_course.id_training_course')
        ->leftJoin('company', 'company.id', '=', 'training_partner_course.id_company')
        ->where('trainings.id', '=', $ID)
        ->get();


        $detailsTrainingParticipant =  DB::table('trainings')
        ->where('trainings.id', '=', $ID)
        ->select([
            'training_participant.id_training',
            'training_participant.id_users',
            'training_participant.id_approval_status',
            'training_participant.status_payment',
            'training_participant.status_attend',
            'training_participant.status_feedback',
            'trainings.id',
            'training_course.course_name',
            'trainings.is_paid',
            DB::raw('DATE_FORMAT(trainings.date_start, "%d/%m/%Y") as date_attend'),
            DB::raw('DATE_FORMAT(trainings.start_time, "%d/%m/%Y") as time_attend'),
            'participant_payment.id as payment_id',
            'participant_examresult.final_verdict_status', 
            'participant_certificate.certificate_no'
        ])
        
        ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
        ->leftJoin('participant_attendance', 'participant_attendance.id_training', '=', 'trainings.id')
        ->leftJoin('participant_examresult', 'participant_examresult.id_training', '=', 'trainings.id') 
        ->leftJoin('training_participant','training_participant.id_training','=','trainings.id')
        ->leftJoin('users','users.id','=','training_participant.id_users')
        ->leftJoin('participant_payment', 'participant_payment.id_training_participant', '=', 'users.id') 
        ->leftJoin('participant_certificate','participant_certificate.id_training_participant','=','users.id')
        ->groupby('trainings.id')
        ->get();

        return response()->json([
            'status' => 'success',
            'details' => $detailsTraining,
            'detailsParticipant' => $detailsTrainingParticipant,
        ], 200);

    }
    public function ParticipantPaymentDetails(Request $request,$ID){
        $detailsTrainingParticipant =  DB::table('training_participant')
        ->where('training_participant.id_training', '=', $ID)
        ->leftJoin('trainings', 'training_participant.id_training', '=', 'trainings.id')
        ->leftJoin('users', 'training_participant.id_users', '=', 'users.id')
        ->leftJoin('country', 'country.id', '=', 'users.id_country')
        ->leftJoin('state','state.id','=','users.id_state')
        ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
        ->join('training_type','training_type.id','=','training_course.id_training_type')
        ->join('course_category','course_category.id','=','training_course.id_course_category')
        ->leftJoin('company', 'company.id', '=', 'trainings.id_company')
        ->select([
            'training_participant.id as id_training_participant',
            'training_participant.id_training',
            'training_participant.id_users',
            'users.fullname',
            'users.email',
            'users.phone_no',
            'users.address_1','users.address_2','users.postcode',
            'state.name as state',
            'country.nicename as country',
            'trainings.id',
            'training_course.course_name as course_name',
            'training_course.new_fee as training_fee',
            'training_course.new_fee_int as training_fee_int',
            DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
            DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
            'trainings.training_code',
            'trainings.venue',
            'trainings.total_participant',
            'trainings.training_mode',
            'trainings.training_description',
            'trainings.state',
            'trainings.venue_address',
            'trainings.is_paid',
            'training_type.name as training_type',
            'trainings.start_time',
            'trainings.end_time',
            'company.name as provider_name',
        ])
        ->get();
        
        return response()->json([
            'status' => 'success',
            'detailsTrainingParticipant' => $detailsTrainingParticipant,
        ], 200);
    }

    public function ParticipantAvailableList(Request $request, $ID)
    {
        try {
            $participantTrainingIds = DB::table('trainings as t')
                ->join('training_participant as tp', 'tp.id_training', '=', 't.id')
                ->where('tp.id_users', $ID)
                ->pluck('t.id')
                ->toArray();

            $trainingList = DB::table('trainings as t')
                ->whereNull('t.deleted_at')
                ->leftjoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
                ->leftjoin('course_category as c', 'tc.id_course_category', '=', 'c.id')
                ->leftjoin('training_type as tt', 'tc.id_training_type', '=', 'tt.id')
                ->where('t.id_training_app_status', 1)
                ->leftjoin('status_training_app as st', 't.id_training_app_status', '=', 'st.id')
                ->whereNotIn('t.id', $participantTrainingIds)
                ->where('t.id_approval_status', '=', 2)
                ->select(
                    DB::raw('t.date_start, DATE_FORMAT(t.date_start,  "%d/%m/%Y") as date_start'),
                    DB::raw('t.date_end, DATE_FORMAT(t.date_end,  "%d/%m/%Y") as date_end'),
                    't.venue',
                    't.fee',
                    'tc.course_name',
                    'tt.name as training_type',
                    't.id_training_app_status',
                    't.id_training_status',
                    'st.id as status_id',
                    'st.status_name',
                    't.id',
                    't.deleted_at',
                    'c.course_name as course_category',
                )
                ->get();

            
                Log::info('Successfully fetched training list.');
                return response()->json([
                    'status' => 'success',
                    'upcomingList' => $trainingList,
                ], 200);
            
        } catch (\Exception $e) {
            Log::error('Error fetching training list: ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

        
 
    public function postParticipantFeedback(Request $request)
    {
       // var_dump('$request->idTrainingParticipant');
        // $request->validate([
        //     'name' => 'required|string|max:255',
        //     // Add validation rules for other fields
        // ]);

     //   $jsonData = $request->json()->all();
     $jsonData = $request->json()->all();

     foreach ($jsonData as $key => $value) {
         // Check if the key starts with 'newFeedback' and has a corresponding 'idFeedback'
         if (strpos($key, 'newFeedback') === 0 && isset($jsonData['idFeedback' . substr($key, 11)])) {
             $genCert = DB::table('participant_feedback')->insert([
                 'id_feedback_question' => $jsonData['idFeedback' . substr($key, 11)],
                 'feedback' => $value,
                // 'id_users' => $userID,
                 'id_training_participant' => $request->idTrainingParticipant,
                 //'id_training' => $request->idTraining,
                 'created_at' => now(),
             ]);
         }
     }
 
     return response()->json(['message' => 'Data inserted successfully'], 200);
    }
    

    }

        
