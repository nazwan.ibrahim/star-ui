<?php
namespace App\Http\Controllers\API\Dashboard\TrainingPartner;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Users;
use Illuminate\Support\Facades\DB;

class TrainingPartnerAPIController extends Controller
{
    public function TPattendanceList(Request $request){

        $examlist = DB::table('participant_attendance')
        ->leftjoin('users','users.id','=','participant_attendance.id_training_participant')
        ->select('users.fullname','participant_attendance.id','participant_attendance.attend_check','participant_attendance.remark',
       DB::raw('participant_attendance.date_attend, DATE_FORMAT(participant_attendance.date_attend,  "%d/%m/%Y") as date_attend'),
       //DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
      )
    
        ->get();

        return response()->json([
            'status' => 'success',
            'examlist' => $examlist,
        ], 200);

    }

    public function TPTrainingSchedule(Request $request,$userid){
        $currentDate = Carbon::now();
    
        $userCompanyId = DB::table('users')
            ->where('id', $userid)
            ->value('id_company');
    
        $TrainingSchedule =  DB::table('trainings')
            ->select([
                'trainings.batch_no_full as batch_no',
                'company.name AS partner_name',
                'training_course.course_name as training_name',
                'trainings.id_company',
                DB::raw('DATE_FORMAT(date_start, "%d/%m/%Y") as date_start'),
                DB::raw('DATE_FORMAT(date_end, "%d/%m/%Y") as date_end'),
                'training_code',
                'training_course.id_course_category as category_id',
                'training_course.id_training_type as type_id',
                'course_category.course_name as course_category',
                'training_type.name as training_type'
            ])
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('training_partner_course','training_partner_course.id_training_course','=','training_course.id_training_course')
            ->leftJoin('company','company.id','=','training_partner_course.id_company')
            ->leftJoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftJoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->whereNotNull('trainings.date_start')
            ->where('trainings.date_end', '>=', $currentDate)
            ->where('trainings.id_company', $userCompanyId)
            ->get();
    
        return response()->json([
            'status' => 'success',
            'TrainingSchedule' => $TrainingSchedule,
        ], 200);
    }


    public function TPTrainingHistory(Request $request,$userid){
        $currentDate = Carbon::now();
    
        $userCompanyId = DB::table('users')
            ->where('id', $userid)
            ->value('id_company');
    
        $TrainingHistory =  DB::table('trainings')
            ->select([
                'trainings.batch_no_full as batch_no',
                'company.name AS partner_name',
                'training_course.course_name as training_name',
                'trainings.id_company',
                'trainings.id',
                DB::raw('DATE_FORMAT(date_start, "%d/%m/%Y") as date_start'),
                DB::raw('DATE_FORMAT(date_end, "%d/%m/%Y") as date_end'),
                'training_code',
                'training_course.id_course_category as category_id',
                'training_course.id_training_type as type_id',
                'course_category.course_name as course_category',
                'training_type.name as training_type'
            ])
            ->leftJoin('training_course', 'training_course.id_training_course', '=', 'trainings.id_training_course')
            ->leftJoin('training_partner_course','training_partner_course.id_training_course','=','training_course.id_training_course')
            ->leftJoin('company','company.id','=','training_partner_course.id_company')
            ->leftJoin('course_category', 'course_category.id', '=', 'training_course.id_course_category')
            ->leftJoin('training_type', 'training_type.id', '=', 'training_course.id_training_type')
            ->whereNotNull('trainings.date_start')
            ->where('trainings.date_end', '<', $currentDate)
            ->where('trainings.id_company', $userCompanyId)
            ->where('trainings.id_approval_status', 2)
            ->groupBy('trainings.id')
            ->get();
    
        return response()->json([
            'status' => 'success',
            'TrainingHistory' => $TrainingHistory,
        ], 200);
    }



    public function examDetails(Request $request, $id){

        $trainingDetails = DB::table('trainings')
        ->where('trainings.id','=',$id)
        ->select('trainings.id','trainings.training_name','trainings.venue',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'))
    
        ->get();

        return response()->json([
            'status' => 'success',
            'trainingDetails' => $trainingDetails,
        ], 200);

    }


    public function examNameList(Request $request){

             
                $data = DB::table('participant_exam AS pe1')
                ->select(
                    'u.fullname',
                    'pe1.id_training_participant',
                    'pe1.exam_type AS exam_type_1',
                    'pe1.exam_1 AS exam_1_1',
                    'pe1.exam_2 AS exam_2_1',
                    'pe1.exam_3 AS exam_3_1',
                    'pe1.total_result AS result_1',
                    'pe1.result_status AS status_1',
                    'pe1.final_verdict_status AS verdict_status_1',
                    'pe2.exam_type AS exam_type_2',
                    'pe2.id_training_participant AS id_training_participant_2',
                    'pe2.exam_1 AS exam_1_2',
                    'pe2.exam_2 AS exam_2_2',
                    'pe2.exam_3 AS exam_3_2',
                    'pe2.result_status AS status_2',
                    'pe2.total_result AS result_2',
                    'pe2.final_verdict_status AS verdict_status_2'
                )
                ->leftJoin('users AS u', 'u.id', '=', 'pe1.id_training_participant')
                ->leftJoin('participant_exam AS pe2', function ($join) {
                    $join->on('pe1.id_training_participant', '=', 'pe2.id_training_participant')
                        ->where('pe1.id', '<>', 'pe2.id');
                })
                ->get();

                
                // Process the data to remove duplicate rows
                $processedData = [];
                foreach ($data as $row) {
                    $id_training_participant='';
                $idTrainingParticipant = $row->id_training_participant;
                if (!isset($processedData[$idTrainingParticipant])) {
                    $processedData[$idTrainingParticipant] = [
                        'id_training_participant' => $row->id_training_participant,
                        'fullname' => $row->fullname,
                        'exam_type_1' => $row->exam_type_1,
                        'exam_1_1' => $row->exam_1_1,
                        'exam_2_1' => $row->exam_2_1,
                        'exam_3_1' => $row->exam_3_1,
                        'result_1' => $row->result_1,
                        'status_1' => $row->status_1,
                        'verdict_status_1' => $row->verdict_status_1,
                       // 'exam_type_2' => $row->exam_type_2,
                        'exam_1_2' => $row->exam_1_2,
                        'exam_2_2' => $row->exam_2_2,
                        'exam_3_2' => $row->exam_3_2,
                        'result_2' => $row->result_2,
                        'status_2' => $row->status_2,
                        'verdict_status_2' => $row->verdict_status_2,
                    ];
                }
            }


                  return response()->json([
                        'status' => 'success',
                        'processedData' => $processedData,
                    ], 200);

            // Now $processedData contains the desired result without duplicate rows

    }


    public function getExamResult(Request $request,$ID){
 
        $participant_exam_data = DB::table('participant_exam AS pe1')
       // ->join('participant_exam','=','')
        ->where('pe1.id_training_participant','=',$ID)
        ->select('pe1.id as idExamPractical','pe1.id_training_participant','pe1.exam_type as exam_type_1','pe1.exam_1','pe1.exam_2','pe1.exam_3',
        'pe1.total_result as result_practical','pe1.result_status as status_practical','pe1.final_verdict_status','pe2.id as idExamTheory','pe2.exam_type as exam_type_2', 'pe2.exam_1 AS exam_1_theory',  'pe2.exam_2 AS exam_2_theory',
        'pe2.exam_3 AS exam_3_theory','pe2.total_result as result_theory','pe2.result_status as status_theory','pe2.final_verdict_status as verdict_status_theory' )
        ->leftJoin('participant_exam AS pe2', function ($join) {
            $join->on('pe1.id_training_participant', '=', 'pe2.id_training_participant')
                ->where('pe1.id', '<>', 'pe2.id');
        })
        ->get();

        $participant_exam = [];
        foreach ($participant_exam_data as $row) {
           // $id_training_participant='';
        $idTrainingParticipant = $row->id_training_participant;
        if (!isset($participant_exam[$idTrainingParticipant])) {
            $participant_exam[$idTrainingParticipant] = [
                'id_training_participant' => $row->id_training_participant,
          
                'exam_type_1' => $row->exam_type_1,
                'exam_type_2' => $row->exam_type_2,
                'exam_1' => $row->exam_1,
                'exam_1_theory' => $row->exam_1_theory,
                'exam_2' => $row->exam_2,
                'exam_2_theory' => $row->exam_2_theory,
                'exam_3' => $row->exam_3,
                'exam_3_theory' => $row->exam_3_theory,
                'result_practical' => $row->result_practical,
                'result_theory' => $row->result_theory,
                'status_practical' => $row->status_practical,
                'status_theory' => $row->status_theory,              
                'verdict_status_theory' => $row->verdict_status_theory,

                'idExamPractical' => $row->idExamPractical,              
                'idExamTheory' => $row->idExamTheory,
             
            ];
        }
    }

          return response()->json([
                'result_status' => 'success',
                'participant_exam' => $participant_exam,
            ], 200);

    }

    public function editExamResult(Request $request){
      //  var_dump($request->exam_1);exit;

       if(isset($request->exam_type_1)){
       // var_dump($request->statusResultPracticalExam);exit;

            $examType1 = DB::table('participant_exam')
        ->where('participant_exam.exam_type', '=', $request->exam_type_1)
        ->where('participant_exam.id', '=', $request->idExamPractical)
        
            ->update([
                'exam_1' => $request->exam_1,
                'exam_2' => $request->exam_2,
                'exam_3' => $request->exam_3,

                'total_result' => $request->totalResultPractical,
                'result_status' => $request->statusResultPracticalExam,
            
            ]);

        }
        if(isset($request->exam_type_2)){
    

            $examType2 = DB::table('participant_exam')
            ->where('participant_exam.exam_type', '=', $request->exam_type_2)
            ->where('participant_exam.id', '=', $request->idExamTheory)
        
            ->update([
                'exam_1' => $request->exam_1_theory,
                'exam_2' => $request->exam_2_theory,
                'exam_3' => $request->exam_3_theory,
    
                'total_result' => $request->totalResultTheory,
                'result_status' => $request->statusResultTheory,
                
            ]);

        }

        return response()->json([
            'examType1' => $examType1,
            'examType2' => $examType2,
        ]);
    
    }

}

    
