<?php

namespace App\Http\Controllers\API\Reporting;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Training;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Models\Users;


class ReportingAPIController extends Controller {

    public function trainingList(Request $request) {
        try {
            $reportingTrainingList = DB::table('trainings as t')
                ->leftJoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
                ->select(
                    't.id',
                    'tc.id_training_type',
                    'tc.id_training_option',
                    't.batch_no_full',
                    'tc.course_name as course_name',
                    DB::raw('DATE_FORMAT(t.date_start, "%d/%m/%Y") as date_start'),
                    DB::raw('DATE_FORMAT(t.date_end, "%d/%m/%Y") as date_end'),
                    DB::raw('DATE_FORMAT(t.date_end, "%Y") as year_end'),
                    't.venue',
                    'tc.new_fee',
                    't.total_participant',
                    't.status',
                )
                ->get();

            if ($reportingTrainingList->isEmpty()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Training not found',
                ], 404);
            } else {
                Log::info('Successfully fetched training list.');

                return response()->json([
                    'status' => 'success',
                    'reportingTrainingList' => $reportingTrainingList,
                ], 200);
            }
        } catch (\Exception $e) {
            Log::error('Error fetching training list: ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

    public function paymentList(Request $request) {
        try {
            $reportingPaymentList = DB::table('trainings as t')
            ->leftJoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
            ->leftJoin('state as state', 't.id_state', '=', 'state.id')
            /* ->join('company as tp', 'tp.id', '=', 't.id_company') */
            ->select(
                'tc.course_name as course_name',
                't.id',
                'state.name as state',
                'tc.id_training_option',
                DB::raw('DATE_FORMAT(t.date_start, "%d/%m/%Y") as date_start'),
                DB::raw('DATE_FORMAT(t.date_end, "%d/%m/%Y") as date_end'),
                DB::raw('DATE_FORMAT(t.date_end, "%Y") as year_end'),
                't.venue',
                'tc.new_fee',
                't.total_participant',
                't.status',
                't.id',
                DB::raw('IF(t.fee_receipt IS NULL, 0, 1) as has_fee_receipt') // Check if fee_receipt is null
            )
            ->get();

            $courseList = DB::table('training_course as tc')
            ->select(
                'tc.course_name',
                'tc.id_training_course',
            )
            ->get();
            if ($reportingPaymentList->isEmpty()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Training not found',
                ], 404);
            } else {
                Log::info('Successfully fetched payment list.');

                return response()->json([
                    'status' => 'success',
                    'reportingPaymentList' => $reportingPaymentList,
                    'courseList' => $courseList,
                ], 200);
            }
        } catch (\Exception $e) {
            Log::error('Error fetching payment list: ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

    public function accessLog(Request $request) {
        try {

            $accessLog = DB::table('access_logs')
            ->get();

            if ($accessLog->isEmpty()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Record not found',
                ], 404);
            } else {
                Log::info('Successfully fetched payment list.');

                return response()->json([
                    'status' => 'success',
                    'accessLog' => $accessLog,
                ], 200);
            }
        } catch (\Exception $e) {
            Log::error('Error fetching payment list: ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

    public function auditTrail(Request $request) {
        try {

            $auditTrail = DB::table('audit_trails')
            ->get();

            if ($auditTrail->isEmpty()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Record not found',
                ], 404);
            } else {
                Log::info('Successfully fetched payment list.');

                return response()->json([
                    'status' => 'success',
                    'auditTrail' => $auditTrail,
                ], 200);
            }
        } catch (\Exception $e) {
            Log::error('Error fetching payment list: ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }

    public function trainingListTP(Request $request) {

        $userid = auth()->user()->id;
        $user = Users::find($userid);
        $id_company = $user->id_company;
        
        try {
            $reportingTrainingList = DB::table('trainings as t')
                ->where('t.id_company', $id_company)
                ->leftJoin('company as c', 't.id_company', '=', 'c.id')
                ->leftJoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
                ->select(
                    't.id',
                    'tc.id_training_type',
                    'tc.id_training_option',
                    't.batch_no',
                    't.region',
                    'tc.course_name as course_name',
                    DB::raw('DATE_FORMAT(t.date_start, "%d/%m/%Y") as date_start'),
                    DB::raw('DATE_FORMAT(t.date_end, "%d/%m/%Y") as date_end'),
                    DB::raw('DATE_FORMAT(t.date_end, "%Y") as year_end'),
                    't.venue',
                    'tc.new_fee',
                    't.total_participant',
                    't.status',
                )
                ->get();

            if ($reportingTrainingList->isEmpty()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Training not found',
                ], 404);
            } else {
                Log::info('Successfully fetched training list.');

                return response()->json([
                    'status' => 'success',
                    'reportingTrainingList' => $reportingTrainingList,
                ], 200);
            }
        } catch (\Exception $e) {
            Log::error('Error fetching training list: ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }
}
