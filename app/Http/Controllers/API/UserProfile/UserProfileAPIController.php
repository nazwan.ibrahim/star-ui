<?php

namespace App\Http\Controllers\API\UserProfile;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Users;
use Illuminate\Support\Facades\DB;



class UserProfileAPIController extends Controller
{
    public function resetpassword(Request $request) {

//var_dump($request->UserName.$request->NewPassword.$request->ConfirmPassword);exit;
        $kemaskiniUserPassword = Users::where('email', '=', $request->UserName);

        $kemaskiniUserPassword->update([
           
             'password' => bcrypt($request->NewPassword),
             'password_confirmation' => bcrypt($request->ConfirmPassword),
             'updated_at' => Carbon::now(),
            // 'postcode' => $request->NewPassword,
            // 'address_2' => $request->ConfirmPassword,
           
        ]);


        return response()->json([
            'status' => 'success',
            'kemaskiniUserPassword' => $kemaskiniUserPassword,
        ], 200);

    }

    public function userInformation(Request $request,$userid) {

       // var_dump($userid);
        $userInfo = DB::table('users')
        ->where('users.id', '=', $userid)
        ->leftjoin('state', 'state.id', '=','users.id_state')
        ->leftjoin('country', 'country.id', '=','users.id_country')
        ->leftjoin('city', 'city.id', '=','users.id_city')
        ->leftjoin('education', 'education.id_users', '=','users.id')
        ->leftjoin('company', 'company.id', '=','users.id_company')
        ->leftjoin('user_profile_uploads', 'user_profile_uploads.id_users', '=','users.id')
        ->select(
            'users.fullname','users.ic_no', 'users.gender','users.phone_no','users.address_1', 'users.address_2','users.postcode',
            'users.email','users.position','users.id_bumiputra as bumi_status','users.user_type',
            'state.name AS statename','state.id','country.name AS countryname','city.cityname',
            'country.id',
            'education.qualification','education_level',
            'company.name as companyname','company.address_1 as company_address_1','company.address_2 as company_address_2',
            'user_profile_uploads.profilepic_format','user_profile_uploads.profilepic_path',
            'user_profile_uploads.ICpic_format','user_profile_uploads.ICpic_path',
            'user_profile_uploads.passportpic_format','user_profile_uploads.passportpic_path',
            'user_profile_uploads.scandoc_format','user_profile_uploads.scandoc_path',
        )
        ->get();
                return response()->json([
                    'status' => 'success',
                    'userInfo' => $userInfo,
                ], 200);
             
            }


    public function UpdateUserInformation(Request $request){

           //     dd(($request->file('FailGambar0')));

                $updateUser = DB::table('users')
               ->where('users.id', '=', $request->UserID)
              // ->where('email', '=', 'ai@seetru')
                ->update([
                    'fullname' => $request->FullName,
                    'ic_no' => $request->IcNo,
                    'gender' => $request->Gender,
                    'phone_no' => $request->PhoneNumber,
                    'address_1' => $request->AddressLine1,
                    'address_2' => $request->AddressLine2,
                    'postcode' => $request->Postcode,
                    'email' => $request->EmailAddress,
                    'position' => $request->Position,
                    'id_bumiputra' => $request->Status,
                    'id_city' => $request->City,
                    'id_state' => $request->State,
                    'id_country' => $request->Country,

                ]);
            
        
                    $updateEducation = DB::table('education')
                    ->update([
                        'qualification' => $request->Qualification,
                        'education_level' => $request->EducationLevel,
                       
                    ]);
              
            
                $updateCompany = DB::table('company')
                    ->update([
                        'name' => $request->CompanyName,
                        'address_1' => $request->CompanyAddressLine1,
                        'address_2' => $request->CompanyAddressLine2,
                     
                    ]);
               
                    if ($request->hasFile('FailGambar')) {

                        $files = $request->file('FailGambar');
                        foreach($files as $file){
                            $extension = $file->getClientOriginalExtension();
                            $name = $file->getClientOriginalName();
                            $filename = pathinfo($name, PATHINFO_FILENAME);
                            $namaFail1 = explode('.', $name);
                           
                            $path = $file->getRealPath();
                            $data = file_get_contents($path);
                            $SaizFail = $file->getSize();
                         
        
                            $updateProfilPic = DB::table('user_profile_uploads')
                            ->where('user_profile_uploads.id_users', '=', $request->UserID)
                            ->update([
                                'profilepic_name' => $filename,
                                'profilepic_path' => base64_encode($data),
                                'profilepic_size' => $SaizFail,
                                'profilepic_format' => $extension,

                              
                                'updated_by' => $request->UpdatedBy,
                                'updated_at' => Carbon::now(),
                            ]);
                        }
                    }


                    // if ($request->hasFile('FailGambarIC')) {

                    //     $files = $request->file('FailGambarIC');
                    //     foreach($files as $file){
                    //         $extension = $file->getClientOriginalExtension();
                    //         $name = $file->getClientOriginalName();
                    //         $filename = pathinfo($name, PATHINFO_FILENAME);
                    //         $namaFail1 = explode('.', $name);
                    //         $path = $file->getRealPath();
                    //         $data = file_get_contents($path);
                    //         $SaizFail = $file->getSize();
                        
        
                    //         $updateGambarIC = DB::table('user_profile_uploads')
                    //         ->where('user_profile_uploads.id_users', '=', $request->UserID)
                    //         ->update([
                            
                    //             'ICpic_name' => $filename,
                    //             'ICpic_path' => base64_encode($data),
                    //             'ICpic_size' => $SaizFail,
                    //             'ICpic_format' => $extension,

                    //             'updated_by' => $request->UpdatedBy,
                    //             'updated_at' => Carbon::now(),
                    //         ]);
                    //     }
                    // }

                //

                    // if ($request->hasFile('FailGambarPassport')) {
                    //     $files = $request->file('FailGambarPassport');
                    //     foreach($files as $file){
                    //         $extension = $file->getClientOriginalExtension();
                    //         $name = $file->getClientOriginalName();
                    //         $filename = pathinfo($name, PATHINFO_FILENAME);
                    //         $namaFail1 = explode('.', $name);
                    //         $path = $file->getRealPath();
                    //         $data = file_get_contents($path);
                    //         $SaizFail = $file->getSize();
                        
        
                    //         $updateGambarPassport = DB::table('user_profile_uploads')
                    //         ->where('user_profile_uploads.id_users', '=', $request->UserID)
                    //         ->update([
                            
                    //             'passportpic_name' => $filename,
                    //             'passportpic_path' => base64_encode($data),
                    //             'passportpic_size' => $SaizFail,
                    //             'passportpic_format' => $extension,

                    //             'updated_by' => $request->UpdatedBy,
                    //             'updated_at' => Carbon::now(),

                    //         ]);
                    //     }
                    // }


                    // if ($request->hasFile('FailGambarDoc')) {

                    //     $files = $request->file('FailGambarDoc');
                    //     foreach($files as $file){
                    //         $extension = $file->getClientOriginalExtension();
                    //         $name = $file->getClientOriginalName();
                    //         $filename = pathinfo($name, PATHINFO_FILENAME);
                    //         $namaFail1 = explode('.', $name);
                    //         $path = $file->getRealPath();
                    //         $data = file_get_contents($path);
                    //         $SaizFail = $file->getSize();
                         
        
                    //         $updateGambarDoc = DB::table('user_profile_uploads')
                    //         ->where('user_profile_uploads.id_users', '=', $request->UserID)
                    //         ->update([
                              
                    //             'scandoc_name' => $filename,
                    //             'scandoc_path' => base64_encode($data),
                    //             'scandoc_size' => $SaizFail,
                    //             'scandoc_format' => $extension,
    
                    //             'updated_by' => $request->UpdatedBy,
                    //             'updated_at' => Carbon::now(),
    
                    //         ]);
                    //     }
                    // }
                
            
            
                // return response()->json([
                //     'updateUser' => $updateUser,
                //     'updateEducation' => $updateEducation,
                //     'updateCompany' => $updateCompany,
                //     'updateProfilPic' => $updateProfilPic,
                //     'updateGambarIC' => $updateGambarIC,
                //     'updateGambarPassport' => $updateGambarPassport,
                //     'updateGambarDoc' => $updateGambarDoc,
                    
                // ]);
            
            }


            public function updateBookInformation(Request $request,$id){
        
                // var_dump($request->all());exit;
            //   var_dump($request->BookTitle);exit;
                $updatebook = DB::table('book_inventory')
                ->where('book_inventory.id','=',$id)
                ->update([
                        'book_code' => $request->BookCode,
                        'book_title' =>$request->BookTitle,
                        'book_description' => $request->BookDescription,
                        'updated_by' => $request->UpdatedBy,
                        'updated_at' => Carbon::now(),
                       
                ]);
        
                // if($addbook != null){
                    $updatebookdetail = DB::table('book_details')
                    ->where('book_details.id_book_inventory','=',$id)
                    ->update([
                            'online_price' => $request->SellPrice,
                            'stock_in' => $request->StockIn,
                            'stock_out' => $request->StockOut,
                            'updated_by' => $request->UpdatedBy,
                            'updated_at' => Carbon::now(),
                            
        
                    ]);
                    //Storage::makeDirectory("\gambarCenderamataTemporary");
                    if ($request->hasFile('FailGambar')) {
                        $files = $request->file('FailGambar');
                        foreach($files as $file){
                            $extension = $file->getClientOriginalExtension();
                            $name = $file->getClientOriginalName();
                            $filename = pathinfo($name, PATHINFO_FILENAME);
                            $namaFail1 = explode('.', $name);
                            $path = $file->getRealPath();
                            $data = file_get_contents($path);
                            $SaizFail = $file->getSize();
                         
        
                            $updateCoverBook = DB::table('book_cover')
                            ->where('book_cover.id_book_inventory','=',$id)
                            ->update([
                                'cover_name' => $filename,
                                'file_cover' => base64_encode($data),
                                'file_size' => $SaizFail,
                                'file_format' => $extension,
                                'updated_by' => $request->UpdatedBy,
                                'updated_at' => Carbon::now(),
                            ]);
                        }
                    }
        
                // }
        
                // if($updatebookdetail) {
                //     return response()->json([
                //         'status' => true,
                //         'data' => $$id,
                //     ])->setStatusCode(200);
                // } else {
                //         return response()->json([
                //                 'status' => 'error',
                //                 'data' => 'unsuccessful'
                //         ], 500);
                // }
            }


            //Trainer attachment
            
            public function TrainerListAttachment(Request $request,$usertype,$userid) {
                
              //   var_dump($company_id.''.$usertype);
               
                if($usertype == 3){ //TP
                    $trainerlist = DB::table('users')
                    ->where('users.id', '=', $userid)
                    //->where('users.user_type','=',$usertype)
                    ->leftjoin('company', 'company.id','=', 'users.id_company')
                    ->leftJoin('trainer','company.id','=', 'trainer.id_company')
                    ->leftjoin('users as user1', 'trainer.id_users','=', 'user1.id')
                    ->leftjoin('state', 'state.id','=', 'users.id_state')
                    ->leftjoin('trainer_docs', 'trainer_docs.id_trainer','=', 'users.id')
                    ->leftjoin('trainingpartner_docs', 'trainingpartner_docs.id_company','=', 'trainer.id_company')
                    ->select(
                        'trainer.id_users as trainer_id_users','trainer.id_company','user1.fullname','user1.user_type','user1.certificate_no',
                        'users.id_state','state.name as state_name',
                        'trainer_docs.file_name as cert_name', 
                        'trainer_docs.file_cover as cert_cover',
                        'trainer_docs.file_format as cert_format', 
                        'trainingpartner_docs.file_name', 'trainingpartner_docs.file_cover', 'trainingpartner_docs.file_cover', 
                        'trainingpartner_docs.file_format',
                        'company.name as training_partner',
                    )
                    ->groupby('trainer.id_users')
                    ->get();

                }



                if($usertype == 2){//admin

                    $trainerlist = DB::table('trainer')
                    //->where('trainer.id_company', '=', $company_id)
                    //->where('users.user_type','=',$usertype)
                    ->leftjoin('users','trainer.id_users','=','users.id')
                    ->leftjoin('company', 'company.id','=', 'trainer.id_company')
                    ->leftjoin('state', 'state.id','=', 'users.id_state')
                    ->leftjoin('trainer_docs', 'trainer_docs.id_trainer','=', 'users.id')
                    ->leftjoin('trainingpartner_docs', 'trainingpartner_docs.id_company','=', 'trainer.id_company')
                    ->select(
                       'trainer.id_users as trainer_id_users','trainer.id_company','users.fullname','users.user_type','users.certificate_no',
                       'users.id_state','state.name as state_name',
                       'trainer_docs.file_name as cert_name', 
                       'trainer_docs.file_cover as cert_cover',
                       'trainer_docs.file_format as cert_format', 
                       'trainingpartner_docs.file_name', 'trainingpartner_docs.file_cover', 'trainingpartner_docs.file_cover', 
                       'trainingpartner_docs.file_format',                       
                       'company.name as training_partner',
 
                    )
                    ->groupby('trainer.id_users')
                    ->get();
                 }

                if($usertype == 4){//Trainer

                    $trainerlist = DB::table('trainer')
                    ->where('users.id', '=', $userid)
                    ->where('users.user_type','=',$usertype)
                    ->leftjoin('users','trainer.id_users','=','users.id')
                    ->leftjoin('company','company.id','=','trainer.id_company')
                    ->leftjoin('state', 'state.id','=', 'users.id_state')
                    ->leftjoin('trainer_docs', 'trainer_docs.id_trainer','=', 'users.id')
                    ->leftjoin('trainingpartner_docs', 'trainingpartner_docs.id_company','=', 'trainer.id_company')
                    ->select(
                       'trainer.id_users as trainer_id_users','trainer.id_company','users.fullname','users.user_type','users.certificate_no',
                       'users.id_state','state.name as state_name',
                       'trainer_docs.file_name as cert_name', 
                       'trainer_docs.file_cover as cert_cover',
                       'trainer_docs.file_format as cert_format', 
                       'trainingpartner_docs.file_name', 'trainingpartner_docs.file_cover', 'trainingpartner_docs.file_cover', 
                       'trainingpartner_docs.file_format' ,
                       'company.name as training_partner'
                    )
                    ->groupby('trainer.id_users')
                    ->get();
                }

                

                  ////$usertype = 5{}//participant

                
                return response()->json([
                    'status' => 'success',
                    'trainerlist' => $trainerlist,
                    ], 200); 
                }
         
}
