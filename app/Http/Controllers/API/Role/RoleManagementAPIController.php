<?php

namespace App\Http\Controllers\API\Role;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use App\Models\User_Role;



class RoleManagementAPIController extends Controller
{
    public function roleList()
    {
        
        $roleList = DB::table('user_role')
        ->whereNull('deleted_at')
        ->get();

       if (!$roleList) {
        return response()->json([
            'status' => 'error',
            'message' => 'Role not found',
        ], 404);
        }else{
            return response()->json([
                'status' => 'success',
                'roleList' => $roleList,
            ], 200);
        }
    }

    public function addRole(Request $request)
    {   
        try {
        $validateUser = Validator::make($request->all(), 
            [
                'role' => 'required',
                'status' => 'required',
                
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }


        $roles = User_Role::create([
            'code' => $request['code'],
            'role' => $request['role'],
            'status' => $request['status'],
            'created_by' => $request->createdBy,
            'created_at' => Carbon::now(),
        ]);
        
        return response()->json([
            'status' => true,
            'roles' => $roles,
            'message' => 'roles Created Successfully',
        ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }

    }


    public function updateRole(Request $request,$userid)
    {
        try {
            $validateRole = Validator::make($request->all(), 
                [
                    'role' => 'required',
                    'status' => 'required',
                ]);
    
                if($validateRole->fails()){
                    return response()->json([
                        'status' => false,
                        'message' => 'validation error',
                        'errors' => $validateRole->errors()
                    ], 401);
                }
    
    
                $updateRole = DB::table('user_role')
                ->where('user_role.id', '=', $request->userid)
                ->update([
                'code' => $request['code'],
                'role' => $request['role'],
                'status' => $request['status'],
                'updated_by' => $request->updatedBy,
                'updated_at' => Carbon::now(),
            ]);       
            
            return response()->json([
                'status' => true,
                'updateRole' => $updateRole,
                'message' => 'User Updated Successfully',
            ], 200);
    
            } catch (\Throwable $th) {
                return response()->json([
                    'status' => false,
                    'message' => $th->getMessage()
                ], 500);
        }
    }

    public function delete(Request $request,$id)
    {
        $role = User_Role::find($id); 
        if($role){

            $updateRole = DB::table('user_role')
            ->where('user_role.id', '=', $role)
            ->update([
            'deleted_by' => $request->updatedBy,
            'deleted_at' => Carbon::now(),
        ]);       

            return response()->json([
                'status' => 'success',
                'message'=>'Successfully Deleted'],200);
        }
        else{
            return response()->json([
                'status' => 'success',
                'message'=>'Failed to Delete'],400);
        }
    }
}
