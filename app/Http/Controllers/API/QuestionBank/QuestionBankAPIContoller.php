<?php

namespace App\Http\Controllers\API\QuestionBank;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Training;
use App\Models\TrainingCourse;
use App\Models\courseReType;
use Illuminate\Support\Facades\DB;


class QuestionBankAPIController extends Controller
{
    public function TrainingPartnerList(Request $request)
    {
        try {
            $trainingPartnerList = DB::table('question_bank as qb')
                ->leftjoin('company as cp', 'qb.id_company', '=', 'cp.id')
                ->select(
                    'cp.id',
                    'cp.name'
                )
                ->get();

            if ($trainingPartnerList->isEmpty()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Training not found',
                ], 404);
            } else {
                Log::info('Successfully fetched training partner list.');
                return response()->json([
                    'status' => 'success',
                    'partnerList' => $trainingPartnerList,
                ], 200);
            }
        } catch (\Exception $e) {
            Log::error('Error fetching training partner list: ' . $e->getMessage());

            return response()->json([
                'status' => 'error',
                'message' => 'Internal Server Error',
            ], 500);
        }
    }
}
