<?php

namespace App\Http\Controllers\API\UserManagement;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;



class UserManagementAPIController extends Controller
{
    public function userManagementList()
    {

        $userlist = DB::table('users')
        ->leftJoin('company','company.id','=','users.id_company')
        ->select('users.*','company.name as companyname')
        ->where('user_type','<>','1')
        ->orderBy('id','desc')->get();

         return response()->json([
             'status' => 'success',
             'userlist' => $userlist,
         ], 200);
        
    }

    public function userInformation(Request $request,$userid) {

        // var_dump($userid);
         $userInfo = DB::table('users')
         //->where('email', '=', 'ai@seetru')
         ->where('users.id', '=', $userid)
         ->leftjoin('state', 'state.id', '=','users.id_state')
         ->leftjoin('country', 'country.id', '=','users.id_country')
         ->leftjoin('education', 'education.id_users', '=','users.id')
         ->leftjoin('company', 'company.id', '=','users.id_company')
         ->leftjoin('user_profile_uploads', 'user_profile_uploads.id_users', '=','users.id')
         ->select(
             'users.fullname','users.ic_no', 'users.gender','users.phone_no','users.address_1', 'users.address_2','users.postcode',
             'users.email','users.position','users.id_bumiputra as bumi_status',
             'state.name AS statename','state.id','country.name AS countryname',
             'country.id',
             'education.qualification','education_level',
             'company.name as companyname','company.address_1 as company_address_1','company.address_2 as company_address_2',
             'user_profile_uploads.profilepic_format','user_profile_uploads.profilepic_path',
             'user_profile_uploads.ICpic_format','user_profile_uploads.ICpic_path',
             'user_profile_uploads.passportpic_format','user_profile_uploads.passportpic_path',
             'user_profile_uploads.scandoc_format','user_profile_uploads.scandoc_path',
         )
         ->get();
                 return response()->json([
                     'status' => 'success',
                     'userInfo' => $userInfo,
                 ], 200);
              
             }
 
 
     public function UpdateUserInformation(Request $request){
 
            //     dd(($request->file('FailGambar0')));
 
                 $updateUser = DB::table('users')
                ->where('users.id', '=', $request->UserID)
               // ->where('email', '=', 'ai@seetru')
                 ->update([
                     'fullname' => $request->FullName,
                     'ic_no' => $request->IcNo,
                     'gender' => $request->Gender,
                     'phone_no' => $request->PhoneNumber,
                     'address_1' => $request->AddressLine1,
                     'address_2' => $request->AddressLine2,
                     'postcode' => $request->Postcode,
                     'email' => $request->EmailAddress,
                     'position' => $request->Position,
                     'id_bumiputra' => $request->Status,
                    
                 ]);
             
         
                     $updateEducation = DB::table('education')
                     ->update([
                         'qualification' => $request->Qualification,
                         'education_level' => $request->EducationLevel,
                        
                     ]);
               
             
                 $updateCompany = DB::table('company')
                     ->update([
                         'name' => $request->CompanyName,
                         'address_1' => $request->CompanyAddressLine1,
                         'address_2' => $request->CompanyAddressLine2,
                      
                     ]);
                
                     if ($request->hasFile('FailGambar')) {
 
                         $files = $request->file('FailGambar');
                         foreach($files as $file){
                             $extension = $file->getClientOriginalExtension();
                             $name = $file->getClientOriginalName();
                             $filename = pathinfo($name, PATHINFO_FILENAME);
                             $namaFail1 = explode('.', $name);
                             $path = $file->getRealPath();
                             $data = file_get_contents($path);
                             $SaizFail = $file->getSize();
                          
         
                             $updateProfilPic = DB::table('user_profile_uploads')
                             ->where('user_profile_uploads.id_users', '=', $request->UserID)
                             ->update([
                                 'profilepic_name' => $filename,
                                 'profilepic_path' => base64_encode($data),
                                 'profilepic_size' => $SaizFail,
                                 'profilepic_format' => $extension,
 
                               
                                 'updated_by' => $request->UpdatedBy,
                                 'updated_at' => Carbon::now(),
                             ]);
                         }
                     }
 
 
                     if ($request->hasFile('FailGambarIC')) {
 
                         $files = $request->file('FailGambarIC');
                         foreach($files as $file){
                             $extension = $file->getClientOriginalExtension();
                             $name = $file->getClientOriginalName();
                             $filename = pathinfo($name, PATHINFO_FILENAME);
                             $namaFail1 = explode('.', $name);
                             $path = $file->getRealPath();
                             $data = file_get_contents($path);
                             $SaizFail = $file->getSize();
                         
         
                             $updateGambarIC = DB::table('user_profile_uploads')
                             ->where('user_profile_uploads.id_users', '=', $request->UserID)
                             ->update([
                             
                                 'ICpic_name' => $filename,
                                 'ICpic_path' => base64_encode($data),
                                 'ICpic_size' => $SaizFail,
                                 'ICpic_format' => $extension,
 
                                 'updated_by' => $request->UpdatedBy,
                                 'updated_at' => Carbon::now(),
                             ]);
                         }
                     }
 
                 //
 
                     if ($request->hasFile('FailGambarPassport')) {
                         $files = $request->file('FailGambarPassport');
                         foreach($files as $file){
                             $extension = $file->getClientOriginalExtension();
                             $name = $file->getClientOriginalName();
                             $filename = pathinfo($name, PATHINFO_FILENAME);
                             $namaFail1 = explode('.', $name);
                             $path = $file->getRealPath();
                             $data = file_get_contents($path);
                             $SaizFail = $file->getSize();
                         
         
                             $updateGambarPassport = DB::table('user_profile_uploads')
                             ->where('user_profile_uploads.id_users', '=', $request->UserID)
                             ->update([
                             
                                 'passportpic_name' => $filename,
                                 'passportpic_path' => base64_encode($data),
                                 'passportpic_size' => $SaizFail,
                                 'passportpic_format' => $extension,
 
                                 'updated_by' => $request->UpdatedBy,
                                 'updated_at' => Carbon::now(),
 
                             ]);
                         }
                     }
 
 
                     if ($request->hasFile('FailGambarDoc')) {
 
                         $files = $request->file('FailGambarDoc');
                         foreach($files as $file){
                             $extension = $file->getClientOriginalExtension();
                             $name = $file->getClientOriginalName();
                             $filename = pathinfo($name, PATHINFO_FILENAME);
                             $namaFail1 = explode('.', $name);
                             $path = $file->getRealPath();
                             $data = file_get_contents($path);
                             $SaizFail = $file->getSize();
                          
         
                             $updateGambarDoc = DB::table('user_profile_uploads')
                             ->where('user_profile_uploads.id_users', '=', $request->UserID)
                             ->update([
                               
                                 'scandoc_name' => $filename,
                                 'scandoc_path' => base64_encode($data),
                                 'scandoc_size' => $SaizFail,
                                 'scandoc_format' => $extension,
     
                                 'updated_by' => $request->UpdatedBy,
                                 'updated_at' => Carbon::now(),
     
                             ]);
                         }
                     }
                 
             
             
             }
 

}