<?php

namespace App\Http\Controllers\API\EventManagement;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventManagementAPIController extends Controller
{

    public function listOfTraining(Request $request){

        $listoftraining = DB::table('trainings')
        ->leftjoin('users','trainings.id_company','=','users.id')
        ->leftjoin('company','users.id_company','=','company.id')
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        ->select('trainings.id','training_course.course_name as training_name',
        'company.name as company_name',
        'course_category.course_name as category_name',
        'training_type.name as type_name',
        'trainings.venue',
        'trainings.fee',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
        'trainings.id_training_app_status',
        )
        ->where('trainings.date_start', '!=', NULL)
        ->get();

       if (!$listoftraining) {
        return response()->json([
            'status' => 'error',
            'message' => 'Training not found',
        ], 404);
        }else{
            return response()->json([
                'status' => 'success',
                'listoftraining' => $listoftraining,
            ], 200);
        }
    }

    public function upcomingEvent(Request $request){

        // $upcomingevent = DB::table('trainings')
        // ->leftjoin('users','trainings.id_company','=','users.id')
        // ->leftjoin('company','users.id_company','=','company.id')
        // ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        // ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        // ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        // ->select('trainings.id','training_course.course_name as training_name',
        // 'company.name as company_name',
        // 'course_category.course_name as category_name',
        // 'training_type.name as type_name',
        // 'trainings.venue',
        // 'trainings.fee',
        // DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        // DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
        // 'trainings.id_training_app_status',
        // )
        // ->where('trainings.date_start', '=', null)
        // ->get();

    //    if (!$upcomingevent) {
    //     return response()->json([
    //         'status' => 'error',
    //         'message' => 'Training not found',
    //     ], 404);
    //     }else{
    //         return response()->json([
    //             'status' => 'success',
    //             'upcomingevent' => $upcomingevent,
    //         ], 200);
    //     }

    $currentDate = Carbon::now();

    try{
        $upcomingevent = DB::table('trainings as t')
        ->leftjoin('training_course as tc', 't.id_training_course', '=', 'tc.id_training_course')
        ->leftjoin('course_category as c', 'tc.id_course_category', '=', 'c.id')
        ->leftjoin('training_type as tt', 'tc.id_training_type', '=', 'tt.id')
        ->whereNull('t.deleted_at')
        ->whereNotNull('t.date_start')
        ->whereNotNull('t.date_end')
        ->select(
            DB::raw('t.date_start, DATE_FORMAT(t.date_start,  "%d/%m/%Y") as date_start'),
            DB::raw('t.date_end, DATE_FORMAT(t.date_end,  "%d/%m/%Y") as date_end'),
            't.venue',
            't.fee',
            'tc.course_name as training_name',
            'tt.name as training_type',
            't.id_approval_status',
            't.batch_no_full',
            't.id',
            't.deleted_at',
            'c.course_name as course_category',
        )
        ->whereIn('t.id_approval_status', [2])
        ->where('t.date_end', '>', $currentDate)
        ->orderby('t.date_start','asc')
        ->get();
       
           
            return response()->json([
                'status' => 'success',
                'upcomingevent' => $upcomingevent,
            ], 200);
        
    } catch (\Exception $e) {
        Log::error('Error fetching training list: ' . $e->getMessage());

        return response()->json([
            'status' => 'error',
            'message' => 'Internal Server Error',
        ], 500);
    }
    }

    public function applyTraining($id)
    {
        $applytraining = DB::table('trainings')
        ->where('trainings.id', '=', $id)
        ->leftjoin('users','trainings.id_company','=','users.id')
        ->leftjoin('company','users.id_company','=','company.id')
        ->leftjoin('training_course','training_course.id_training_course','=','trainings.id_training_course')
        ->leftjoin('training_type','training_type.id','=','training_course.id_training_type')
        ->leftjoin('course_category','course_category.id','=','training_course.id_course_category')
        ->select('trainings.id','trainings.training_name',
        'company.name as company_name',
        'course_category.course_name as category_name',
        'training_type.name as type_name',
        'trainings.venue',
        'trainings.fee',
        DB::raw('trainings.date_start, DATE_FORMAT(trainings.date_start,  "%d/%m/%Y") as date_start'),
        DB::raw('trainings.date_end, DATE_FORMAT(trainings.date_end,  "%d/%m/%Y") as date_end'),
        'trainings.id_training_app_status',
        )
        ->get();

       if (!$applytraining) {
        return response()->json([
            'status' => 'error',
            'message' => 'Training not found',
        ], 404);
        }else{
            return response()->json([
                'status' => 'success',
                'upcomingevent' => $applytraining,
            ], 200);
        }
    }

}
