<?php

namespace App\Http\Controllers\API\BookManagement;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;



class BookMarketPlaceAPIController extends Controller
{
   
    public function bookMarket(){
        $bookmarket = DB::table('book_inventory')
        ->where('book_inventory.deleted_status','=',0)
        ->leftjoin('book_details','book_details.id_book_inventory','=','book_inventory.id')
        ->leftjoin('book_cover','book_cover.id_book_inventory', '=', 'book_inventory.id')
        ->select('book_inventory.book_code','book_title','book_description','book_cover.cover_name','book_cover.file_cover',
        'book_cover.file_format')
        ->get();


        return response()->json([
            'status' => 'success',
            'bookmarket' => $bookmarket,
        ], 200);   
    }

    
    
  
      
}
