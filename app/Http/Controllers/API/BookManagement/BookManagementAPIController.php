<?php

namespace App\Http\Controllers\API\BookManagement;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\SendEmail;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Mail;
use App\Models\Users;
use Illuminate\Support\Facades\DB;



class BookManagementAPIController extends Controller
{
   

   

    public function addBookInformation(Request $request){
        //$date = Carbon::now()->format('Ymdh:i:s');
//var_dump($request->BookTitle);exit;
        $addbook = DB::table('book_inventory')
        ->insertGetId([
                'book_code' => $request->BookCode,
                'book_title' => $request->BookTitle,
                'book_description' => $request->BookDescription,
                'created_by' => $request->Createdby,
                'created_at' => Carbon::now(),
                'deleted_status' => 0,
        ]);

        if($addbook != null){
            $addbookdetail = DB::table('book_details')
            ->insert([
                    'id_book_inventory' => $addbook,
                    'online_price' => $request->SellPrice,
                    'walkin_price' => $request->WalkinPrice,
                    'special_price' => $request->SpecialPrice,
                    'stock_in' => $request->StockIn,
                    'status' => 0,
                    'created_by' => $request->Createdby,
                    'created_at' => Carbon::now(),
                    //'deleted_status' => 0,

            ]);
            //Storage::makeDirectory("\gambarCenderamataTemporary");
            if ($request->hasFile('FailGambar')) {
                $files = $request->file('FailGambar');
                foreach($files as $file){
                    $extension = $file->getClientOriginalExtension();
                    $name = $file->getClientOriginalName();
                    $filename = pathinfo($name, PATHINFO_FILENAME);
                    $namaFail1 = explode('.', $name);
                    $path = $file->getRealPath();
                    $data = file_get_contents($path);
                    $SaizFail = $file->getSize();
                 

                    $coverBook = DB::table('book_cover')
                    ->insert([
                        'id_book_inventory' => $addbook,
                        'cover_name' => $filename,
                        'file_cover' => base64_encode($data),
                        'file_size' => $SaizFail,
                        'file_format' => $extension,
                        'created_by' => $request->Createdby,
                        'created_at' => Carbon::now(),
                        'deleted_status' => 0,
                    ]);

                    //park jap for trainer attachment.

                    // $coverBook = DB::table('trainingpartner_docs')
                    // ->insert([
                    //   //  'id_book_inventory' => $addbook,
                    //     'file_name' => $filename,
                    //     'file_cover' => base64_encode($data),
                    //     'file_size' => $SaizFail,
                    //     'file_format' => $extension,
                    //     'created_by' => $request->Createdby,
                    //     'created_at' => Carbon::now(),
                    //     'deleted_status' => 0,
                    // ]);
                }
            }

        }

        // if($daftarCenderamata) {
        //     return response()->json([
        //         'status' => true,
        //         'data' => $refNumber,
        //     ])->setStatusCode(200);
        // } else {
        //         return response()->json([
        //                 'status' => 'error',
        //                 'data' => 'Gagal masukkan data'
        //         ], 500);
        // }
    }

    public function bookInventory(){
        $bookInventorylist = DB::table('book_inventory')
        ->where('book_inventory.deleted_status','=',0)
        ->leftjoin('book_details','book_details.id_book_inventory','=','book_inventory.id')
        ->select('book_inventory.id','book_inventory.book_title','book_details.online_price','book_details.stock_in',
        'book_details.stock_out','book_details.stock_balance','book_details.stock_reserve','reserve_remark','book_details.status')
        ->get();


        return response()->json([
            'status' => 'success',
            'bookInventorylist' => $bookInventorylist,
        ], 200);

    }


    public function bookInfo($id){
        $bookInventory = DB::table('book_inventory')
        ->where('book_inventory.deleted_status','=',0)
        ->where('book_inventory.id','=',$id)
        ->leftjoin('book_details','book_details.id_book_inventory','=','book_inventory.id')
        ->leftjoin('book_cover','book_cover.id_book_inventory', '=', 'book_details.id_book_inventory')
        ->select('book_inventory.id','book_inventory.book_title','book_inventory.book_description','book_inventory.book_code',
        'book_details.online_price','book_details.walkin_price','book_details.special_price',
        'book_details.stock_in',
        'book_details.stock_out','book_details.stock_reserve','reserve_remark',
        'book_details.status','book_cover.cover_name','book_cover.file_cover','book_cover.file_size','book_cover.file_format')
        ->get();


        return response()->json([
            'status' => 'success',
            'bookInventory' => $bookInventory,
        ], 200);   
    }

    public function updateBookInformation(Request $request,$id){
      
        // var_dump($request->all());exit;
    //   var_dump($request->BookTitle);exit;
        $updatebook = DB::table('book_inventory')
        ->where('book_inventory.id','=',$id)
        ->update([
                'book_code' => $request->BookCode,
                'book_title' =>$request->BookTitle,
                'book_description' => $request->BookDescription,
                'updated_by' => $request->UpdatedBy,
                'updated_at' => Carbon::now(),
               
        ]);

       
    //    dd($request->QuantityReserve);
        if((($request->QuantityReserve == '')||($request->QuantityReserve == 0)) &&( $request->StockbalanceInput!= 0)){

            $status = "1";//Stock Available

        }else if(($request->QuantityReserve != '')&&($request->QuantityReserve != 0)){

            $status = "2";//Reserved
          
        }else if((($request->QuantityReserve == '')||($request->QuantityReserve == 0)) &&( $request->StockbalanceInput == 0)){

            $status = "3";//Out Of Stock
           // dd('3');
        }

        // if($addbook != null){
            $updatebookdetail = DB::table('book_details')
            ->where('book_details.id_book_inventory','=',$id)
            ->update([
                    'online_price' => $request->SellPrice,
                    'special_price' => $request->WalkInPrice,
                    'walkin_price' => $request->SpecialPrice,
                    'stock_in' => $request->StockIn,
                    'stock_out' => $request->StockOut,
                    'stock_balance' => $request->StockbalanceInput,
                    'stock_reserve' => $request->QuantityReserve,
                    'reserve_remark' => $request->ReserveRemark,
                    'status' => $status,
                    'updated_by' => $request->ReserveRemark,
                    'updated_at' => Carbon::now(),
                    

            ]);
           
            if ($request->hasFile('FailGambar')) {
                $files = $request->file('FailGambar');
                foreach($files as $file){
                    $extension = $file->getClientOriginalExtension();
                    $name = $file->getClientOriginalName();
                    $filename = pathinfo($name, PATHINFO_FILENAME);
                    $namaFail1 = explode('.', $name);
                    $path = $file->getRealPath();
                    $data = file_get_contents($path);
                    $SaizFail = $file->getSize();
                 

                    $updateCoverBook = DB::table('book_cover')
                    ->where('book_cover.id_book_inventory','=',$id)
                    ->update([
                        'cover_name' => $filename,
                        'file_cover' => base64_encode($data),
                        'file_size' => $SaizFail,
                        'file_format' => $extension,
                        'updated_by' => $request->UpdatedBy,
                        'updated_at' => Carbon::now(),
                    ]);
                }
            }

        // }

        // if($updatebookdetail) {
        //     return response()->json([
        //         'status' => true,
        //         'data' => $$id,
        //     ])->setStatusCode(200);
        // } else {
        //         return response()->json([
        //                 'status' => 'error',
        //                 'data' => 'unsuccessful'
        //         ], 500);
        // }
    }

    public function bookDelete(Request $request,$id){

        //var_dump($id);exit;

        $deletebookinventory = DB::table('book_inventory')
        ->where('book_inventory.id','=',$id)
        ->update([
               
                'updated_by' => $request->userID,
                'updated_at' => Carbon::now(),
                'deleted_by' => $request->userID,
                'deleted_at' => Carbon::now(),
                'deleted_status' => 1,
                

        ]);

        $deletebookdetails = DB::table('book_details')
        ->where('book_details.id_book_inventory','=',$id)
        ->update([
               
                'updated_by' => $request->userID,
                'updated_at' => Carbon::now(),
                'deleted_by' => $request->userID,
                'deleted_at' => Carbon::now(),
                'deleted_status' => 1,
                

        ]);

        $deletebookcover = DB::table('book_cover')
        ->where('book_cover.id_book_inventory','=',$id)
        ->update([
               
                'updated_by' => $request->userID,
                'updated_at' => Carbon::now(),
                'deleted_by' => $request->userID,
                'deleted_at' => Carbon::now(),
                'deleted_status' => 1,
                

        ]);

    }


    public function bookTransactionList($userID,$inventoryID){
//dd($inventoryID);
        $bookTransactionList = DB::table('book_order_item')
        ->where('book_order_item.user_id','=',$userID)
        ->where('book_order_item.id_book_inventory','=',$inventoryID)  
        ->leftjoin('book_payment','book_order_item.order_no', '=', 'book_payment.order_no') 
        ->leftjoin('users','users.id', '=', 'book_order_item.user_id')
        ->select('book_order_item.order_no', 'book_order_item.user_id', 'book_order_item.id_book_inventory', 'users.fullname', 
        'book_order_item.online_price_perunit','book_order_item.total_price',
        'book_order_item.quantity','book_order_item.updated_at as date_purchase','book_payment.status_payment')
        ->get();


        return response()->json([
            'status' => 'success',
            'bookTransactionList' => $bookTransactionList,
        ], 200);   
    }

      
}
