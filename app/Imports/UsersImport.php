<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Users;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

class UsersImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {

        $defaultPassword = bcrypt('Abcd1234@');
        
        return new Users([
            'name'     => $row[0],
            'email'    => $row[1], 
            'agency'   => $row[2],
            'phone_no' => $row[3],
            'password'   => $defaultPassword,
            'user_type' => 2,
        ]);
    }
}




