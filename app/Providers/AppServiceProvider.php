<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */

    public function boot()
    {
        Validator::extend('custom_password', function ($attribute, $value, $parameters, $validator) {
            // Check if the password meets the requirements
            return preg_match('/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@#$%^&+=!])(?!.*\s).{8,}$/', $value);
        });
    
        Validator::replacer('custom_password', function ($message, $attribute, $rule, $parameters) {
            // Customize the validation error message
            return str_replace(':attribute', $attribute, 'The :attribute must contain at least one uppercase letter, one lowercase letter, one number, one symbol, and be at least 8 characters in length.');
        });
    }

}
