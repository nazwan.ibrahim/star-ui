<?php

namespace App\Providers;
use Illuminate\Auth\Notifications\ResetPassword;
use App\Models\Users;

// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        //
        ResetPassword::createUrlUsing(function (Users $user, string $token) {
            return env('APP_URL').'/auth/reset/password?token='.$token.'?email='.$user->email;;
        });
    }
}
