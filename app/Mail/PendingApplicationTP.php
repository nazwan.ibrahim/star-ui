<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;

class PendingApplicationTP extends Mailable
{
    use Queueable, SerializesModels;

    public $trainings;
    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param string $password
     */
    public function __construct($trainings)
    {
        $this->trainings = $trainings;

    }
    

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Pending Application')
            ->view('email.pending-application-tp');
    }
}