<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;

class TrainingAppQuery extends Mailable
{
    use Queueable, SerializesModels;

   // public $user;
    public $applicationQuery;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param string $password
     */
    public function __construct($query_remark)
    {
       // $this->user = $user;
        $this->query_remark = $query_remark;

    }
    

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Training Application Query')
            ->view('email.TrainingApplicationQuery');
    }
}