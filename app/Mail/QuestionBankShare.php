<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;

class QuestionBankShare extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param string $password
     */
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    

    public function build()
    {

        $mail = $this->view('email.question-bank')->with($this->data);
    
        foreach ($this->data['attachment']['path'] as $path) {
            $mail->attach($path, ['as' => basename($path)]);
        }
    
        return $mail;
    }
}