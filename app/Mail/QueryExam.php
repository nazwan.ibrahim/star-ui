<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;

class QueryExam extends Mailable
{
    use Queueable, SerializesModels;

    public $getTPname;
   // public $sendquery;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param string $password
     */
    public function __construct($getTPname)
    {
        $this->getTPname = $getTPname;
      //  $this->sendquery = $sendquery;

    }
    

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Query Exam')
            ->view('email.query-exam');
    }
}