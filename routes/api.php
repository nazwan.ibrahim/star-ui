<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Login\ResetPasswordAPIController;
use App\Http\Controllers\API\changePasswordAPIController;
use App\Http\Controllers\API\UserProfile\UserProfileAPIController;
use App\Http\Controllers\API\ExamManagement\ExamManagementAPIController;
use App\Http\Controllers\API\UserManagement\UserManagementAPIController;
use App\Http\Controllers\API\EventManagement\EventManagementAPIController;
use App\Http\Controllers\API\TrainingManagement\TrainingCourseManagementAPIController;
use App\Http\Controllers\API\TrainingManagement\TrainingManagementAPIController;
use App\Http\Controllers\API\TrainingManagement\TrainingRegistrationAPIController;
use App\Http\Controllers\API\UserRegistrationAPIController;
use App\Http\Controllers\API\Reporting\ReportingAPIController;
use App\Http\Controllers\API\BookManagement\BookManagementAPIController;
use App\Http\Controllers\API\BookManagement\BookMarketPlaceAPIController;
use App\Http\Controllers\API\Dashboard\TrainingPartner\TrainingPartnerAPIController;
use App\Http\Controllers\API\Dashboard\Trainer\TrainerAPIController;
use App\Http\Controllers\API\Dashboard\Public\PublicAPIController;
use App\Http\Controllers\API\Role\RoleManagementAPIController;
use App\Http\Controllers\API\Dashboard\Participant\ParticipantAPIController;
use App\Http\Controllers\API\QuestionBank\QuestionBankAPIController;
use App\Http\Controllers\API\ParameterManagement\ParameterMgmtAPIController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!  
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('testMail')->group(function () {
    Route::get('/{email}', [TestMailAPIController::class, 'index']);
});

//resetpassword
Route::prefix('login')->group(function () {
    Route::post('/userlogin', [UserRegistrationAPIController::class, 'login']);
    Route::post('/userlogout', [UserRegistrationAPIController::class, 'logout']);
    Route::post('/registerparticipant', [UserRegistrationAPIController::class, 'registerparticipant']);
    Route::put('/resetpassword', [ResetPasswordAPIController::class, 'resetpassword']);

    Route::delete('/delete/{$id}', [UserRegistrationAPIController::class, 'delete']);

});

//profileManagement
Route::prefix('profile')->group(function () {
    Route::put('/userProfile', [UserProfileAPIController::class, 'resetpassword']);
    Route::get('/{userid}/userInformation', [UserProfileAPIController::class, 'userInformation']);
    Route::post('/{userid}/UpdateUserInformation', [UserProfileAPIController::class, 'UpdateUserInformation']);   

    //Trainer attachment
    Route::get('/{usertype}/{userID}/TrainerListAttachment', [UserProfileAPIController::class, 'TrainerListAttachment']);   
});


Route::prefix('password')->group(function () {
    Route::put('/changepassword', [ChangePasswordAPIController::class, 'changepassword']);
});

Route::put("update", [UserProfileController::class, 'update']);

//exam management
Route::prefix('exam')->group(function () {
    Route::get('/examTrainingList', [ExamManagementAPIController::class, 'examTrainingList']);
    Route::get('/{id}/examTrainingListTP', [ExamManagementAPIController::class, 'examTrainingListTP']);
    Route::get('/{batch_full}/{ID}/examNameList', [ExamManagementAPIController::class, 'examNameList']);
    Route::get('/{batch_full}/{ID}/examNameList_GCPVDESIGN', [ExamManagementAPIController::class, 'examNameList_GCPVDESIGN']);
    Route::get('/{ID}/{batch_full}/examNameListTPSubmit', [ExamManagementAPIController::class, 'examNameListTPSubmit']);//in training management
    Route::get('/{batch_full}/{ID}/examDetailsTPSubmit', [ExamManagementAPIController::class, 'examDetailsTPSubmit']);//in training management
    Route::get('/{ID}/{batch_full}/examDetailsAdmin2', [ExamManagementAPIController::class, 'examDetailsAdmin2']);//in exam
    Route::get('/{batch_full}/{ID}/getExamResult', [ExamManagementAPIController::class, 'getExamResult']);
    Route::get('/{batch_full}/{ID}/examDetails', [ExamManagementAPIController::class, 'examDetails']);
    Route::put('/{batch_full}/{ID}/editExamResult', [ExamManagementAPIController::class, 'editExamResult']);

    Route::put('/{batch_full}/{ID}/editExamResult_GCPV', [ExamManagementAPIController::class, 'editExamResult_GCPV']);

    Route::put('/{ID}/endorsedExamResult', [ExamManagementAPIController::class, 'endorsedExamResult']);
    Route::get('/{batch_full}/{ID}/examNameListAdmin', [ExamManagementAPIController::class, 'examNameListAdmin']);
    Route::get('/{batch_full}/{ID}/examNameListAdminCGPV', [ExamManagementAPIController::class, 'examNameListAdminCGPV']);
    Route::get('/{ID}/examDetailsAdmin', [ExamManagementAPIController::class, 'examDetailsAdmin']);
    Route::put('/editExamResultEndorsed', [ExamManagementAPIController::class, 'editExamResultEndorsed']);
    Route::get('/trainingPartner', [ExamManagementAPIController::class, 'trainingPartner']);

    Route::put('/submitExamToEndorse', [ExamManagementAPIController::class, 'submitExamToEndorse']);
    Route::put('/submitFeedbackToEndorse', [ExamManagementAPIController::class, 'submitFeedbackToEndorse']);
    

    Route::put('/endorsedExamResult', [ExamManagementAPIController::class, 'endorsedExamResult']);
    Route::put('/editExamResultQuery', [ExamManagementAPIController::class, 'editExamResultQuery']);
   

    

    Route::get('/getTableHeaer', [ExamManagementAPIController::class, 'getTableHeaer']);
  
});

//book management
Route::prefix('book')->group(function () {
    Route::post('/addBookInformation', [BookManagementAPIController::class, 'addBookInformation']);
    Route::get('/bookInventory', [BookManagementAPIController::class, 'bookInventory']);
    Route::get('{inventoryID}/bookInfo', [BookManagementAPIController::class, 'bookInfo']);
    //update
    Route::post('{inventoryID}/updateBookInformation', [BookManagementAPIController::class, 'updateBookInformation']);
    Route::put('{dataID}/bookDelete', [BookManagementAPIController::class, 'bookDelete']);

    Route::get('{userID}/{inventoryID}/bookTransactionList', [BookManagementAPIController::class, 'bookTransactionList']);


    Route::get('/bookMarket', [BookMarketPlaceAPIController::class, 'bookMarket']);
});

Route::prefix('user')->group(function () {
    Route::get('/userManagementList', [UserManagementAPIController::class, 'userManagementList']);
  
});

Route::prefix('training')->group(function () {
    Route::get('/courseManagementList', [TrainingCourseManagementAPIController::class, 'courseManagementList']);
    Route::delete('/{id_training_course}/deleteCourseTraining', [TrainingCourseManagementAPIController::class, 'deleteCourseTraining']);
    Route::put('/{id_training_course}/updateCourseTraining', [TrainingCourseManagementAPIController::class, 'updateCourseTraining']);
    Route::get('/{id_training_course}/showCourseTraining', [TrainingCourseManagementAPIController::class, 'showCourseTraining']);

    Route::get('/{batch_full}/{ID}/trainingProvider', [TrainingManagementAPIController::class, 'trainingProvider']);//in training management

  
    Route::get('/trainingApplicationInfo/{id}', [TrainingManagementAPIController::class, 'trainingApplication']);
    Route::get('/{id}/trainingManagementListTP', [TrainingManagementAPIController::class, 'trainingManagementListTP']);
    Route::get('/{id}/trainingManagementListTP2', [TrainingManagementAPIController::class, 'trainingManagementListTP2']);
    Route::get('/trainingManagementListAll', [TrainingManagementAPIController::class, 'trainingManagementListAll']);
    Route::get('/{id}/trainingManagementListAllTP', [TrainingManagementAPIController::class, 'trainingManagementListAllTP']);
    Route::get('/trainingManagementList', [TrainingManagementAPIController::class, 'trainingManagementList']);
    Route::get('/trainingManagementList2', [TrainingManagementAPIController::class, 'trainingManagementList2']);
    Route::get('/getSelectedCourseReTypes', [TrainingManagementAPIController::class, 'getSelectedCourseReTypes']);
    Route::get('/{id}/trainingManagementListParticipantTP', [TrainingManagementAPIController::class, 'trainingManagementListParticipantTP']);
    Route::get('/{id}/trainingManagementListParticipantPayTP', [TrainingManagementAPIController::class, 'trainingManagementListParticipantPayTP']);

    Route::get('/softDelete/{id}', [TrainingManagementAPIController::class, 'softDeleteTraining']);
    Route::get('/{id}/trainingPostpone', [TrainingManagementAPIController::class, 'trainingPostpone']);
    Route::put('/{id}/updateTrainingPostpone', [TrainingManagementAPIController::class, 'updateTrainingPostpone']);
    Route::get('{batch_no}/{ID}/trainingManagementTPAttendance', [TrainingManagementAPIController::class, 'trainingManagementTPAttendance']);

    Route::get('/{ID}/participantReportList', [TrainingManagementAPIController::class, 'participantReportList']);
    Route::get('{batch_no}/{ID}/participantReportDetails', [TrainingManagementAPIController::class, 'participantReportDetails']);
    Route::get('{batch_no}/{ID}/ReportDetails', [TrainingManagementAPIController::class, 'ReportDetails']);

    Route::get('{batch_no}/{ID}/adminAttendanceList', [TrainingManagementAPIController::class, 'adminAttendanceList']);
    Route::get('{batch_no}/{ID}/adminStatusList', [TrainingManagementAPIController::class, 'adminStatusList']);

    Route::get('{batch_no}/{ID}/participantFeedbackList', [TrainingManagementAPIController::class, 'participantFeedbackList']);


    Route::get('{batch_no}/{ID}/participantFeedbackView', [TrainingManagementAPIController::class, 'participantFeedbackView']);
    Route::post('{batch_no}/{ID}/insertAttendance', [TrainingManagementAPIController::class, 'insertAttendance']);

    Route::put('{batch_no}/{ID}/updateAttendance', [TrainingManagementAPIController::class, 'updateAttendance']);

    Route::put('{batch_no}/{ID}/updateFeedbackSubmit', [TrainingManagementAPIController::class, 'updateFeedbackSubmit']);

    Route::post('{batch_no}/{ID}/trainingReportTPInformation', [TrainingManagementAPIController::class, 'trainingReportTPInformation']); 

    Route::get('{batch_no}/{ID}/paymentOutline', [TrainingManagementAPIController::class, 'paymentOutline']);

    Route::post('{batch_no}/{ID}/savePaymentDocument', [TrainingManagementAPIController::class, 'savePaymentDocument']); 

    Route::post('{batch_no}/{ID}/savePaymentDocumentTP', [TrainingManagementAPIController::class, 'savePaymentDocumentTP']);

    

});

Route::prefix('event')->group(function () {
    Route::get('/listOfTraining', [EventManagementAPIController::class, 'listOfTraining']);
    Route::get('/upcomingEvent', [EventManagementAPIController::class, 'upcomingEvent']);
    Route::get('/{id_training_course}/applyTraining', [EventManagementAPIController::class, 'applyTraining']);
});

//Role
Route::get('/role/rolelist', [RoleManagementAPIController::class, 'roleList']);

//Reporting
Route::get('/reporting/trainingList', [ReportingAPIController::class, 'trainingList']);
Route::get('/reporting/paymentList', [ReportingAPIController::class, 'paymentList']);
Route::get('/reporting/accessLog', [ReportingAPIController::class, 'accessLog']);
Route::get('/reporting/auditTrail', [ReportingAPIController::class, 'auditTrail']);
Route::get('/reporting/trainingListTP', [ReportingAPIController::class, 'trainingListTP']);


//Dashboard - trainingPartner
Route::prefix('dashboard')->group(function () {
    Route::get('{batch_no}/{ID}/trainingPartner/TPattendanceList', [TrainingPartnerAPIController::class, 'TPattendanceList']);

    Route::get('trainingPartner/{userid}/TPTrainingSchedule', [TrainingPartnerAPIController::class, 'TPTrainingSchedule']);
    Route::get('trainingPartner/{userid}/TPTrainingHistory', [TrainingPartnerAPIController::class, 'TPTrainingHistory']);
  
});


//Dashboard - trainer
Route::prefix('dashboard')->group(function () {
    Route::get('trainer/{userid}/TrainerAttendanceList', [TrainerAPIController::class, 'TrainerAttendanceList']);

    Route::get('trainer/{userid}/TrainerTrainingSchedule', [TrainerAPIController::class, 'TrainerTrainingSchedule']);
    Route::get('trainer/{userid}/TrainerTrainingHistory', [TrainerAPIController::class, 'TrainerTrainingHistory']);
  
});

//Dashboard - participant
Route::prefix('dashboard')->group(function () {
  
    Route::get('participant/{ID}/ParticipantTrainingList', [ParticipantAPIController::class, 'ParticipantTrainingList']);
    Route::get('participant/{ID}/ParticipantTrainingList2', [ParticipantAPIController::class, 'ParticipantTrainingList2']);
    Route::get('participant/{ID}/ParticipantTrainingDetails', [ParticipantAPIController::class, 'ParticipantTrainingDetails']);
    Route::get('participant/{ID}/ParticipantPaymentDetails', [ParticipantAPIController::class, 'ParticipantPaymentDetails']);
    Route::get('participant/{ID}/ParticipantAvailableList', [ParticipantAPIController::class, 'ParticipantAvailableList']);   
    
    //Route::post('participant/{userID}/postParticipantFeedback', [ParticipantAPIController::class, 'postParticipantFeedback']);
    Route::post('participant/postParticipantFeedback', [ParticipantAPIController::class, 'postParticipantFeedback']);
  
});


//Dashboard - Public
Route::prefix('dashboard')->group(function () {
    Route::get('AllList', [PublicAPIController::class, 'AllList']);
    Route::get('AllTrainingList', [PublicAPIController::class, 'AllTrainingList']);
    Route::get('SEList', [PublicAPIController::class, 'SEList']);
    Route::get('REList', [PublicAPIController::class, 'REList']);
    Route::get('EEList', [PublicAPIController::class, 'EEList']);
    Route::get('TrainingCards', [PublicAPIController::class, 'TrainingCards']);
    Route::get('CompetencyChart', [PublicAPIController::class, 'CompetencyChart']);
    Route::get('TPList/{ID}', [PublicAPIController::class, 'TPList']);
    Route::get('TrainerList/{ID}', [PublicAPIController::class, 'TrainerList']);


    Route::get('searchCourse', [PublicAPIController::class, 'searchCourse']);
    Route::get('searchState', [PublicAPIController::class, 'searchState']);
});


//Question bank - Seda admin
Route::prefix('questionbank')->group(function () {
    Route::get('TrainingPartnerList', [QuestionBankAPIController::class, 'TrainingPartnerList']);
});


//Parameter Management - Seda admin
Route::prefix('parameter')->group(function () {
    Route::get('countryList', [ParameterMgmtAPIController::class, 'countryList']);
    Route::get('genderList', [ParameterMgmtAPIController::class, 'genderList']);
    // Route::put('/{id}/updateGender', [ParameterMgmtAPIController::class, 'updateGender']);
    Route::get('certCodeList', [ParameterMgmtAPIController::class, 'certCodeList']);
    Route::get('feeTypeList', [ParameterMgmtAPIController::class, 'feeTypeList']);
    Route::get('trainingOptionList', [ParameterMgmtAPIController::class, 'trainingOptionList']);
    Route::get('eduLevelList', [ParameterMgmtAPIController::class, 'eduLevelList']);
    Route::get('trainingModeList', [ParameterMgmtAPIController::class, 'trainingModeList']);
    Route::get('trainingTypeList', [ParameterMgmtAPIController::class, 'trainingTypeList']);
    Route::get('reTypeList', [ParameterMgmtAPIController::class, 'reTypeList']);
});
    
    
