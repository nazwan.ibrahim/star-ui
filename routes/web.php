<?php

//use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Module\Admin\AdminController;
use App\Http\Controllers\Module\Admin\SystemController;
use App\Http\Controllers\Module\Auth\AuthenticationController;
use App\Http\Controllers\Module\Dashboard\DashboardController;
use App\Http\Controllers\Module\Hello\HelloController;
use App\Http\Controllers\Module\Login\LoginController;
use App\Http\Controllers\Module\Login\ForgotPasswordController;
use App\Http\Controllers\Module\TrainingMgmt\TrainingMgmtController;
use App\Http\Controllers\Module\TrainingMgmt\TrainingCourseMgmtController;
use App\Http\Controllers\Module\TrainingMgmt\CertificateController;
use App\Http\Controllers\Module\UserRegistration\UserRegistrationController;
use App\Http\Controllers\Module\UserProfile\UserProfileController;
use App\Http\Controllers\Module\UserProfile\changePasswordController;
use App\Http\Controllers\Module\UserProfile\profilePictureController;
use App\Http\Controllers\Module\ExamMgmt\ExamMgmtController;
use App\Http\Controllers\Module\EventMgmt\EventMgmtController;
use App\Http\Controllers\Module\QuestionBank\QuestionBankController;
use App\Http\Controllers\Module\Reporting\ReportingController;
use App\Http\Controllers\Module\BookMgmt\BookMgmtController;
use App\Http\Controllers\Module\BookMgmt\BookMgmtListController;
use App\Http\Controllers\Module\ParameterMgmt\RolesController;
use App\Http\Controllers\Module\Verify\VerifyController;
use App\Http\Controllers\Module\ParameterMgmt\ParameterMgmtController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Module\TrainingMgmt\TrainingApplicationApprovalController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
//---------------------------------------------



//---------------------------------------------
Route::get('/', [DashboardController::class, 'publicDashboard'])->name('home');


Route::get('/dashboard', [DashboardController::class, 'publicDashboard'])->name('home');

Route::get('/paymentError', [DashboardController::class, 'paymentError'])->name('paymenterror');

Route::post('/return', [DashboardController::class, 'returnPage']);

// Route::post('/return',function() {
//     dd("Payment");
// });

Route::get('send-mail', function () {

    $details = [
        'title' => 'Mail from Seda STAR',
        'body' => 'This is for testing email using smtp',
        'subject' => '',
        'url' => ''
    ];

    Mail::to('nurulaiza@seetrustudio.com')->send(new \App\Mail\MyTestMail($details));

    dd("Email is Sent.");
});

//Authentication  Route
Route::group(['prefix' => 'auth'], function () {
    Route::get('forgot-password', [ForgotPasswordController::class, 'forgotPasswordForm'])->name('forgot-password');
    Route::post('forgot-password', [ForgotPasswordController::class, 'submitForgotPasswordForm'])->name('forgot-password-post');
    Route::get('reset-password/{token}/{encoded_email}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset-password-get');
    Route::post('reset-password', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset-password-post');
    Route::get('reset-success', [ForgotPasswordController::class, 'resetsuccessPage'])->name('reset-success');
    Route::get('forgot-username', [ForgotPasswordController::class, 'forgotUsernamePage'])->name('forgot-username');
    Route::get('username-success', [ForgotPasswordController::class, 'usernameSentPage'])->name('username-success');
});

//main 
Route::controller(LoginController::class)->group(function () {
    Route::get('/login', 'loginPage')->name('login');
    Route::post('/authenticate', 'authenticate')->name('authenticate');
    Route::get('/logout', 'logout')->name('user-logout');
});

//User Profile  Route
Route::group(['prefix' => 'auth'], function () {
    Route::get('profile', [UserProfileController::class, 'userProfile']);
});

Route::get('/register', [UserRegistrationController::class, 'registerPage'])->name('user-register');
Route::post('/register', [UserRegistrationController::class, 'createRegistrationPage'])->name('createUserRegistration');


Route::get('/verify/certificate', [VerifyController::class, 'verifyPage'])->name('verifyPage');
Route::post('/verify/certificate', [VerifyController::class, 'verifyCertificate'])->name('verifyCertificate');

//Marketplace
Route::get('/book/listOfBook', [BookMgmtListController::class, 'bookList'])->name('list-book');
Route::get('/book/listOfBookPublic', [BookMgmtListController::class, 'bookListPublic'])->name('list-book-public');
Route::get('/book/BookDetail/{id}', [BookMgmtListController::class, 'bookDetail'])->name('book-detail');
//Route::get('/book/cartcheckout',  [BookMgmtListController::class, 'cartCheckout'])->name('cart-checkout'); 

//Route::post('/cart/addToCart', [BookMgmtListController::class, 'addToCart']);

Route::post('/book/shopping-cart', [BookMgmtListController::class, 'storeSelectedItems'])->name('shopping-cart');
Route::get('/book/showSelectedItems', [BookMgmtListController::class, 'showSelectedItems'])->name('show-Selected-Items');


Route::post('/book/bookCheckout', [BookMgmtListController::class, 'bookCheckout'])->name('checkout-cart');
Route::get('/book/checkout', [BookMgmtListController::class, 'showCheckoutPage'])->name('checkout-page');

Route::post('/book/paymentGateway', [BookMgmtListController::class, 'processPayment'])->name('book_process_payment');
Route::post('book/bookPaymentInvoice/{id}', [BookMgmtListController::class, 'bookPaymentInvoice'])->name('book-Payment-Invoice');
//Route::get('/book/return', [BookMgmtListController::class, 'bookPaymentInvoice'])->name('book-Payment-Invoice');
//Route::post('return', [DashboardController::class, 'participantPaymentInvoice'])->name('participant-Payment-Invoice');




//changePassword
Route::get('/changePassword', [changePasswordController::class, 'changePassword'])->name('change-password');
Route::post('/updateChangePassword', [changePasswordController::class, 'updateChangePassword'])->name('update-change-password');

//profile picture
Route::get('/profilePic', [profilePictureController::class, 'profilePic'])->name('profile-picture');

Route::get('public/participant/apply/{id}', [DashboardController::class, 'participantApply'])->name('participant-Apply');

//questionBank
Route::group(['prefix' => 'question-bank'], function () {
    Route::get('/question-bank-list', [QuestionBankController::class, 'questionbanklist'])->name('questionbanklist');
    Route::get('/question-bank-submission-list', [QuestionBankController::class, 'questionbanksubmissionlist'])->name('questionbanksubmissionlist');
    Route::get('/question-bank-request-list', [QuestionBankController::class, 'questionbankrequestlist'])->name('questionbankrequestlist');
});

//userProfile
Route::group(['prefix' => 'profile'], function () {
    Route::get('userProfile', [UserProfileController::class, 'userProfile'])->name('user-profile');
    Route::get('updateUserProfile', [UserProfileController::class, 'editUserProfile'])->name('update-user-profile');
    Route::put('saveUserProfile', [UserProfileController::class, 'updateUserProfile'])->name('save-user-profile');
    Route::get('trainerAttachment', [UserProfileController::class, 'trainerAttachment'])->name('trainer-attachment');
    Route::post('/remove-mou', [UserProfileController::class, 'removeMou'])->name('remove-mou');
    Route::post('/remove-loa', [UserProfileController::class, 'removeLoa'])->name('remove-loa');
    Route::get('/view-pdf/{base64Content}/{filename}', [UserProfileController::class, 'viewPdf'])->name('viewPdf');
});

Route::get('/get-postcode/{postcode}', [AdminController::class, 'getPostcode']);
Route::get('/get-city/{citiesId}', [AdminController::class, 'getCity']);
Route::get('/get-states/{stateId}', [AdminController::class, 'getStates']);
Route::get('/get-country/{countryId}', [AdminController::class, 'getCountry']);


//super user
Route::middleware(['auth', 'super-user:superuser'])->group(function () {

    // Route::get('/', [DashboardController::class, 'adminDashboard'])->name('admin.dashboard');
    // Route::get('/user-management', [AdminController::class, 'userRegistrationManagement'])->name('admin.user-management');
    // Route::get('/register-new-user', [AdminController::class, 'registerTrainingPartner'])->name('admin.training-partner-registration');
    // Route::post('/create-new-user', [AdminController::class, 'createRegistration'])->name('admin.create-registration');
    // Route::get('/view-user-registerd/{id}', [AdminController::class, 'viewUserRegistration'])->name('admin.viewUser');
    // Route::get('/edit-user-registered/{id}', [AdminController::class, 'editUserRegistration'])->name('admin.editUser');
    // Route::put('/update-user-registered/{id}', [AdminController::class, 'updateUserRegistration'])->name('admin.updateUser');
    // Route::get('/user-management/{id}', [AdminController::class, 'deleteUserRegistration'])->name('admin.deleteUser');
    // Route::post('/register-new-user', [AdminController::class, 'getQPperson'])->name('admin.getQP');

    //     Route::get('/training-course-management', [TrainingCourseMgmtController::class, 'trainingCourseManagement'])->name('admin.training-course-management');
    //     Route::get('/add-training-course', [TrainingCourseMgmtController::class, 'addTrainingCourse'])->name('admin.add-training-course');
    //     Route::post('/add-training-course-data', [TrainingCourseMgmtController::class, 'addTrainingCoursedata'])->name('admin.add-training-course-data');
    //     Route::get('/edit-training-course/{id}', [TrainingCourseMgmtController::class, 'editTrainingCourse'])->name('admin.edit-training-course');
    //     Route::post('/update-course-training/{id}', [TrainingCourseMgmtController::class, 'updateCourseTraining'])->name('admin.update-training-course');
    //     Route::get('/delete-training-course/{id}', [TrainingCourseMgmtController::class, 'deleteTrainingCourse'])->name('admin.delete-training-course');
    //     Route::get('/view-training-course/{id}', [TrainingCourseMgmtController::class, 'viewTrainingCourse'])->name('admin.view-training-course');

    //     // training management
    //     Route::get('/training-management', [TrainingMgmtController::class, 'trainingManagement'])->name('TrainingManagement');
    //     Route::get('/training-registration', [TrainingMgmtController::class, 'trainingRegistration'])->name('TrainingRegistration');
    //     Route::put('/training-register', [TrainingMgmtController::class, 'insertTraining'])->name('TrainingInsert');
    //     Route::get('/edit-training-registration/{id}', [TrainingMgmtController::class, 'editTrainingRegistration'])->name('editTrainingRegistration');
    //     Route::put('/update-training-registration/{id}', [TrainingMgmtController::class, 'updateTrainingRegistration'])->name('update-training-registration');

    //     Route::get('/training-management-list', [TrainingMgmtController::class,'trainingManagementAll'])->name('admin.training-list');
    //     Route::get('/training-management-attendance/{id}', [TrainingMgmtController::class,'trainingManagementTPAttendance'])->name('admin.attendance');
    //     Route::get('/training-status-view/{id}', [TrainingMgmtController::class,'trainingStatusView'])->name('admin.statys');
    //     Route::post('/update-attendance', [TrainingMgmtController::class,'updateAttendance'])->name('admin.update-attendance');
    //     Route::delete('/delete-participant/{id}', [TrainingMgmtController::class, 'deleteParticipant'])->name('admin.delete-participant');

    //     Route::get('/training-management-feedbacklist/{batch_no}/{id}', [TrainingMgmtController::class,'adminFeedbackParticipant'])->name('admin-feedbacklist');
    //     Route::get('/training-management-feedback/{batch_no}/{id}', [TrainingMgmtController::class,'trainingManagementFeedbackQuestion'])->name('trainingpartner.feedbackquestion');
    //     Route::post('/add-training-management-feedback', [TrainingMgmtController::class,'trainingManagementFeedbackQuestionAdd'])->name('admin.addfeedbackquestion');
    //     Route::delete('/delete-feedback/{id}', [TrainingMgmtController::class, 'deleteFeedbackQuestion'])->name('admin.delete-feedback');

    //     Route::get('training-management/endorseExamResult/{batch_no}/{id}', [ExamMgmtController::class, 'endorseExamResult2'])->name('endorse-exam-result2');

    //     Route::get('training-management/training-report/{batch_no}/{id}', [TrainingMgmtController::class, 'adminTrainingReport'])->name('admin-TrainingReport');
    //     Route::get('training-management/trainingReportParticipant/{batch_no}/{id}', [TrainingMgmtController::class, 'adminTrainingReportParticipant'])->name('admin-TrainingReportParticipant');

    //     Route::get('admin/training-management/attendance-list/{batch_no}/{id}', [TrainingMgmtController::class, 'adminAttendanceList'])->name('admin.attendance');


    //     Route::get('training-management/participant-feedback/{batch_no}/{id}', [TrainingMgmtController::class, 'participantFeedback'])->name('view-participant-feedback');

    //     Route::get('/event-management', [EventMgmtController::class, 'UpcomingEvent'])->name('admin.event-management');
    //     Route::get('/apply-event/{id}', [EventMgmtController::class, 'applyTrainingList'])->name('admin.apply-training');

    //     Route::get('question-bank-list', [QuestionBankController::class, 'questionbanklist'])->name('admin.questionbank-list');
    //     Route::get('question-bank-show/{id}', [QuestionBankController::class, 'questionbankshow'])->name('admin.questionbankshow');
    //     Route::get('question-bank-share/{id}', [QuestionBankController::class, 'questionbankshare'])->name('admin.questionbankshare');
    //     Route::post('question-bank-share/{id}', [QuestionBankController::class, 'questionbanksharesubmit'])->name('admin.questionbanksharesubmit');

    //     Route::get('exam/setExamResult', [AdminController::class, 'setExamResult'])->name('admin.set-exam-result');
    //     Route::post('exam/createExamConfiguration', [AdminController::class, 'createExamConfiguration'])->name('admin.create-exam-config');

    //     Route::get('exam/headerTableResult', [AdminController::class, 'tableResult'])->name('admin-tableResult');
    //     Route::get('exam/previewTableResult', [AdminController::class, 'previewTableResult'])->name('admin-PreviewTableResult');
    //     Route::get('exam/getPreviewResult/{id}', [AdminController::class, 'getPreviewResult'])->name('admin-getPreviewResult');
    //     Route::get('training-report', [ReportingController::class, 'trainingReporting'])->name('admin.training-report');
    //     Route::get('payment-report', [ReportingController::class, 'paymentReporting'])->name('admin.payment-report');
    //     Route::get('payment-details/{id}', [ReportingController::class, 'paymentDetails'])->name('admin.payment-details');
    //     Route::get('training-details/{id}', [ReportingController::class, 'paymentDetails'])->name('admin.payment-details');  

    //     // Exam management
    //     Route::get('listOfExamAdmin', [ExamMgmtController::class, 'examListAdmin'])->name('exam-list-admin');
    //     Route::get('updateExamResultAdmin/{id}/{batch_no_full}', [ExamMgmtController::class, 'updateExamResult'])->name('update-exam-result-admin');
    //     Route::get('endorseExamResult/{batch_no}', [ExamMgmtController::class, 'endorseExamResult'])->name('endorse-exam-result');

    //     //book management
    //     Route::get('/addBook', [BookMgmtController::class, 'addBook'])->name('add-book');
    //     Route::get('/bookInventory', [BookMgmtController::class, 'bookInventory'])->name('book-inventory');
    //     Route::get('/bookInventoryUpdate/{bookInventoryID}', [BookMgmtController::class, 'bookInventoryUpdate'])->name('bookInventoryUpdate');

});

//admin route.
Route::middleware(['auth', 'admin:admin'])->group(function () {
    Route::get('/system', [SystemController::class, 'userAccount'])->name('userAccount');

    Route::get('/admin', [DashboardController::class, 'adminDashboard'])->name('admin.dashboard');
    Route::get('/admin/user-management', [AdminController::class, 'userRegistrationManagement'])->name('admin.user-management');
    Route::get('/admin/register-training-partner', [AdminController::class, 'registerTrainingPartner'])->name('admin.training-partner-registration');
    Route::post('/admin/create-registration', [AdminController::class, 'createRegistration'])->name('admin.create-registration');
    Route::get('/admin/upload-potential-participant', [AdminController::class, 'UploadBulkRegistration'])->name('admin.upload-bulk-registration');
    Route::get('/admin/view-registered/{id}', [AdminController::class, 'viewUserRegistration'])->name('admin.viewUser');
    Route::get('/admin/edit-registered/{id}', [AdminController::class, 'editUserRegistration'])->name('admin.editUser');
    Route::put('/admin/update-registered/{id}', [AdminController::class, 'updateUserRegistration'])->name('admin.updateUser');
    Route::get('/admin/user-management/{id}', [AdminController::class, 'deleteUserRegistration'])->name('admin.deleteUser');
    Route::post('/remove-mou', [AdminController::class, 'removeMou'])->name('remove-mou');
    Route::post('/remove-loa', [AdminController::class, 'removeLoa'])->name('remove-loa');
    Route::post('/remove-trainer', [AdminController::class, 'removeTrainer'])->name('remove-trainer');
    Route::post('/save-trainer', [AdminController::class, 'saveTrainer'])->name('save-trainer');
    Route::post('/admin/getUser', [AdminController::class, 'getQPperson'])->name('admin.getQP');
    Route::get('/get-states/{countryId}', [AdminController::class, 'getStates']);
    Route::get('/admin/training-course-management', [TrainingCourseMgmtController::class, 'trainingCourseManagement'])->name('admin.training-course-management');
    Route::get('/admin/add-training-course', [TrainingCourseMgmtController::class, 'addTrainingCourse'])->name('admin.add-training-course');
    Route::post('/admin/add-training-course-data', [TrainingCourseMgmtController::class, 'addTrainingCoursedata'])->name('admin.add-training-course-data');
    Route::get('/admin/edit-training-course/{id}', [TrainingCourseMgmtController::class, 'editTrainingCourse'])->name('admin.edit-training-course');
    Route::post('/admin/update-course-training/{id}', [TrainingCourseMgmtController::class, 'updateCourseTraining'])->name('admin.update-training-course');
    Route::post('/admin/remove-tpc', [TrainingCourseMgmtController::class, 'removeTpc'])->name('admin.remove-tpc');
    Route::post('/admin/remove-fee', [TrainingCourseMgmtController::class, 'removeFee'])->name('admin.remove-fee');
    Route::get('/admin/delete-training-course/{id}', [TrainingCourseMgmtController::class, 'deleteTrainingCourse'])->name('admin.delete-training-course');
    Route::get('/admin/view-training-course/{id}', [TrainingCourseMgmtController::class, 'viewTrainingCourse'])->name('admin.view-training-course');


    //admin training management
    Route::get('/admin/training-management', [TrainingMgmtController::class, 'trainingManagement'])->name('TrainingManagement');
    Route::get('/admin/training-registration', [TrainingMgmtController::class, 'trainingRegistration'])->name('TrainingRegistration');
    Route::put('/admin/training-register', [TrainingMgmtController::class, 'insertTraining'])->name('TrainingInsert');
    Route::get('/admin/edit-training-registration/{id}', [TrainingMgmtController::class, 'editTrainingRegistration'])->name('editTrainingRegistration');
    Route::get('/admin/approve-training-registration/{id}', [TrainingMgmtController::class, 'approveTrainingRegistration'])->name('approveTrainingRegistration');
    Route::put('/admin/rejectTraining/{id}', [TrainingMgmtController::class, 'rejectTraining'])->name('admin.rejectTraining');
    Route::put('/admin/update-training-registration/{id}', [TrainingMgmtController::class, 'updateTrainingRegistration'])->name('update-training-registration');
    Route::put('/admin/updateToApprove-training-registration/{id}', [TrainingMgmtController::class, 'updateToApproveTrainingRegistration'])->name('updateToApprove-training-registration');

    Route::get('/admin/training-management-list', [TrainingMgmtController::class, 'trainingManagementAll'])->name('admin.training-list');
    Route::get('/admin/training-management-attendance/{id}', [TrainingMgmtController::class, 'trainingManagementTPAttendance'])->name('admin.attendance');
    Route::get('/admin/training-status-view/{id}', [TrainingMgmtController::class, 'trainingStatusView'])->name('admin.statys');
    Route::post('/admin/update-attendance', [TrainingMgmtController::class, 'updateAttendance'])->name('admin.update-attendance');
    Route::delete('/admin/delete-participant/{id}', [TrainingMgmtController::class, 'deleteParticipant'])->name('admin.delete-participant');

    Route::get('/admin/training-management-feedbacklist/{batch_no}/{id}', [TrainingMgmtController::class, 'adminFeedbackParticipant'])->name('admin-feedbacklist');
    Route::get('/admin/training-management-feedback', [TrainingMgmtController::class, 'trainingManagementFeedbackQuestion'])->name('admin.feedbackquestion');
    Route::post('/admin/add-training-management-feedback', [TrainingMgmtController::class, 'trainingManagementFeedbackQuestionAdd'])->name('admin.addfeedbackquestion');
    Route::delete('/admin/delete-feedback/{id}', [TrainingMgmtController::class, 'deleteFeedbackQuestion'])->name('admin.delete-feedback');

    Route::get('admin/training-management/endorseExamResult/{batch_no}/{id}', [ExamMgmtController::class, 'endorseExamResult2'])->name('endorse-exam-result2');

    Route::get('admin/training-management/training-report/{batch_no}/{id}', [TrainingMgmtController::class, 'adminTrainingReport'])->name('admin-TrainingReport-TM');
    Route::get('admin/training-management/trainingReportParticipant/{batch_no}/{id}', [TrainingMgmtController::class, 'adminTrainingReportParticipant'])->name('admin-TrainingReportParticipant');

    Route::get('admin/training-management/attendance-list/{batch_no}/{id}', [TrainingMgmtController::class, 'adminAttendanceList'])->name('admin.attendance');


    Route::get('admin/training-management/participant-feedback/{batch_no}/{id}', [TrainingMgmtController::class, 'participantFeedback'])->name('view-participant-feedback');
    // Route::get('/admin/training-management/participant-feedback/{batch_no}/{id}', [TrainingMgmtController::class, 'participantFeedback'])->name('view-participant-feedback');

    Route::get('/admin/training-management-participant-list/{id}', [TrainingApplicationApprovalController::class, 'trainingManagementListParticipantAdmin'])->name('trainingManagementListParticipant');
    Route::get('/admin/training-management-participant-pay-list/{id}', [TrainingApplicationApprovalController::class, 'trainingManagementListPayParticipantAdmin'])->name('trainingManagementListPayParticipantAdmin');
    Route::get('/admin/training-management-participant-details/{id}', [TrainingApplicationApprovalController::class, 'trainingManagementParticipantDetailsAdmin'])->name('trainingManagementListParticipant');
    Route::get('/admin/approve-training-participant/{id}', [TrainingApplicationApprovalController::class, 'approveApplyTraining'])->name('approveApplyTrainingAdmin');
    Route::get('/admin/reject-training-participant/{id}', [TrainingApplicationApprovalController::class, 'rejectApplyTraining'])->name('rejectApplyTrainingAdmin');


    Route::get('/admin/event-management', [EventMgmtController::class, 'UpcomingEvent'])->name('admin.event-management');
    Route::get('/admin/apply-event/{id}', [EventMgmtController::class, 'applyTrainingList'])->name('admin.apply-training');

    Route::get('admin/generate-all-qpcertificate', [CertificateController::class, 'generateAllCertificateQP']);
    Route::get('admin/generate-all-certificate', [CertificateController::class, 'generateAllCertificate']);
    
    Route::get('admin/question-bank-list', [QuestionBankController::class, 'questionbanklist'])->name('admin.questionbank-list');
    Route::get('admin/question-bank-share-list', [QuestionBankController::class, 'questionbanksharelist'])->name('admin.questionbanksharelist');
    Route::get('admin/question-bank-history-list', [QuestionBankController::class, 'questionbankhistorylist'])->name('admin.questionbankhistorylist');
    Route::get('admin/question-bank-show/{id}', [QuestionBankController::class, 'questionbankshow'])->name('admin.questionbankshow');
    Route::get('admin/question-bank-verify/{id}', [QuestionBankController::class, 'questionbankverify'])->name('admin.questionbankverify');
    Route::get('admin/question-bank-delete/{id}', [QuestionBankController::class, 'questionBankDelete'])->name('admin.questionbankdelete');
    Route::post('admin/question-bank-verify-approval/{id}', [QuestionBankController::class, 'questionbankverifyupdate'])->name('admin.questionbankverifyupdate');
    Route::get('admin/question-bank-share/{id}', [QuestionBankController::class, 'questionbankshare'])->name('admin.questionbankshare');
    Route::post('admin/question-bank-submit/{id}', [QuestionBankController::class, 'questionbanksharesubmit'])->name('admin.questionbanksharesubmit');
    Route::post('admin/question-bank-reupload/{id}', [QuestionBankController::class, 'questionBankReupload'])->name('admin.questionBankReupload');

    Route::get('/admin/exam/setExamResult', [AdminController::class, 'setExamResult'])->name('admin.set-exam-result');
    Route::post('/admin/exam/createExamConfiguration', [AdminController::class, 'createExamConfiguration'])->name('admin.create-exam-config');

    Route::get('/admin/exam/headerTableResult', [AdminController::class, 'tableResult'])->name('admin-tableResult');
    Route::get('/admin/exam/previewTableResult', [AdminController::class, 'previewTableResult'])->name('admin-PreviewTableResult');
    Route::get('/admin/exam/getPreviewResult/{id}', [AdminController::class, 'getPreviewResult'])->name('admin-getPreviewResult');
    Route::get('/admin/exam/getResult/{id}', [AdminController::class, 'getResult'])->name('admin-getResult');
    Route::get('/admin/exam/getPreviewResultDelete/{id}', [AdminController::class, 'getPreviewResultDelete'])->name('admin-getPreviewResultDelete');
    Route::get('/admin/exam/getPreviewResultGCPV/{id}', [AdminController::class, 'getPreviewResultGCPV'])->name('admin-getPreviewResultGCPV');
    Route::get('/admin/exam/getPreviewResultGCPVDelete/{id}', [AdminController::class, 'getPreviewResultGCPVDelete'])->name('admin-getPreviewResultGCPVDelete');

    // Route::post('/admin/exam/getPreviewResult', [AdminController::class, 'getPreviewResult'])->name('admin-getPreviewResult');


    Route::get('/admin/training-report', [ReportingController::class, 'trainingReporting'])->name('admin.training-report');
    Route::get('/admin/payment-report', [ReportingController::class, 'paymentReporting'])->name('admin.payment-report');
    Route::get('/admin/access-log', [ReportingController::class, 'accessLog'])->name('admin.access-log');
    Route::get('/admin/audit-trail', [ReportingController::class, 'auditTrail'])->name('admin.audit-trail');
    Route::get('/admin/payment-details/{id}', [ReportingController::class, 'paymentDetails'])->name('admin.payment-details');
    Route::get('/admin/training-details/{id}', [ReportingController::class, 'trainingDetails'])->name('admin.training-details');

    Route::get('/admin/training-management/AdminPayment-seda/{batch_no}/{id}', [TrainingMgmtController::class, 'AdminPaymentToSeda'])->name('Admin-Payment-to-seda');

    // EXam management
    Route::get('/exam/listOfExamAdmin', [ExamMgmtController::class, 'examListAdmin'])->name('exam-list-admin');

    Route::get('/exam/updateExamResultAdmin/{id}/{batch_no_full}', [ExamMgmtController::class, 'updateExamResult'])->name('update-exam-result-admin');
    Route::get('/exam/updateExamResultAdmin_GCPVDESIGN/{id}/{batch_no_full}', [ExamMgmtController::class, 'updateExamResult_GCPVDESIGN'])->name('update-exam-result-adminGCPVDESIGN');

    Route::get('/exam/endorseExamResult/{id}/{batch_no_full}', [ExamMgmtController::class, 'endorseExamResult'])->name('endorse-exam-result');
    Route::get('/exam/endorseExamResult_GCPVDESIGN/{id}/{batch_no_full}', [ExamMgmtController::class, 'endorseExamResult_GCPVDESIGN'])->name('endorse-exam-result_GCPVDESIGN');

    //book management
    Route::get('/book/addBook', [BookMgmtController::class, 'addBook'])->name('add-book');
    Route::get('/book/bookInventory', [BookMgmtController::class, 'bookInventory'])->name('book-inventory');
    Route::get('/book/bookInventoryUpdate/{bookInventoryID}', [BookMgmtController::class, 'bookInventoryUpdate'])->name('bookInventoryUpdate');

    //Parameter Management
    Route::get('/parameterManagement/country', [ParameterMgmtController::class, 'country'])->name('country');
    Route::get('/parameterManagement/add-country', [ParameterMgmtController::class, 'addCountry'])->name('addCountry');
    Route::post('/parameterManagement/addnew-country', [ParameterMgmtController::class, 'addNewCountry'])->name('addNewCountry');
    Route::get('/parameterManagement/edit-country/{id}', [ParameterMgmtController::class, 'editCountry'])->name('editCountry');
    Route::post('/parameterManagement/update-country/{id}', [ParameterMgmtController::class, 'updateCountry'])->name('admin.updateCountry');
    Route::get('/parameterManagement/delete-country/{id}', [ParameterMgmtController::class, 'deleteCountry'])->name('deleteCountry');


    Route::get('/parameterManagement/gender', [ParameterMgmtController::class, 'gender'])->name('gender');
    Route::get('/parameterManagement/edit-gender/{id}', [ParameterMgmtController::class, 'editGender'])->name('editGender');
    Route::post('/parameterManagement/update-gender/{id}', [ParameterMgmtController::class, 'updateGender'])->name('admin.updateGender');

    Route::get('/parameterManagement/certCode', [ParameterMgmtController::class, 'certCode'])->name('certCode');
    Route::get('/parameterManagement/add-certCode', [ParameterMgmtController::class, 'addCertCode'])->name('addCertCode');
    Route::post('/parameterManagement/addnew-certCode', [ParameterMgmtController::class, 'addNewCertCode'])->name('addNewCertCode');
    Route::get('/parameterManagement/edit-certCode/{id}', [ParameterMgmtController::class, 'editCertCode'])->name('editCertCode');
    Route::post('/parameterManagement/update-certCode/{id}', [ParameterMgmtController::class, 'updateCertCode'])->name('admin.updateCertCode');
    Route::get('/parameterManagement/delete-certCode/{id}', [ParameterMgmtController::class, 'deleteCertCode'])->name('deleteCertCode');


    Route::get('/parameterManagement/feeType', [ParameterMgmtController::class, 'feeType'])->name('feeType');
    Route::get('/parameterManagement/add-feeType', [ParameterMgmtController::class, 'addFeeType'])->name('addFeeType');
    Route::post('/parameterManagement/addnew-feeType', [ParameterMgmtController::class, 'addNewFeeType'])->name('addNewFeeType');
    Route::get('/parameterManagement/edit-feeType/{id}', [ParameterMgmtController::class, 'editFeeType'])->name('editFeeType');
    Route::post('/parameterManagement/update-feeType/{id}', [ParameterMgmtController::class, 'updateFeeType'])->name('admin.updateFeeType');
    Route::get('/parameterManagement/delete-feeType/{id}', [ParameterMgmtController::class, 'deleteFeeType'])->name('deleteFeeType');


    Route::get('/parameterManagement/trainingOption', [ParameterMgmtController::class, 'trainingOption'])->name('trainingOption');
    Route::get('/parameterManagement/add-trainingOption', [ParameterMgmtController::class, 'addTrainingOption'])->name('addTrainingOption');
    Route::post('/parameterManagement/addnew-trainingOption', [ParameterMgmtController::class, 'addNewTrainingOption'])->name('addNewTrainingOption');
    Route::get('/parameterManagement/edit-trainingOption/{id}', [ParameterMgmtController::class, 'editTrainingOption'])->name('editTrainingOption');
    Route::post('/parameterManagement/update-trainingOption/{id}', [ParameterMgmtController::class, 'updateTrainingOption'])->name('admin.updateTrainingOption');
    Route::get('/parameterManagement/delete-trainingOption/{id}', [ParameterMgmtController::class, 'deleteTrainingOption'])->name('deleteTrainingOption');

    Route::get('/parameterManagement/educationLevel', [ParameterMgmtController::class, 'educationLevel'])->name('educationLevel');
    Route::get('/parameterManagement/add-educationLevel', [ParameterMgmtController::class, 'addEduLevel'])->name('addEduLevel');
    Route::post('/parameterManagement/addnew-educationLevel', [ParameterMgmtController::class, 'addNewEducationLevel'])->name('addNewEducationLevel');
    Route::get('/parameterManagement/edit-educationLevel/{id}', [ParameterMgmtController::class, 'editEduLevel'])->name('editEduLevel');
    Route::post('/parameterManagement/update-educationLevel/{id}', [ParameterMgmtController::class, 'updateEduLevel'])->name('admin.updateEduLevel');
    Route::get('/parameterManagement/delete-educationLevel/{id}', [ParameterMgmtController::class, 'deleteEducationLevel'])->name('deleteEducationLevel');


    Route::get('/parameterManagement/trainingMode', [ParameterMgmtController::class, 'trainingMode'])->name('trainingMode');
    Route::get('/parameterManagement/add-trainingMode', [ParameterMgmtController::class, 'addTrainingMode'])->name('addTrainingMode');
    Route::post('/parameterManagement/addnew-trainingMode', [ParameterMgmtController::class, 'addNewTrainingMode'])->name('addNewTrainingMode');
    Route::get('/parameterManagement/edit-trainingMode/{id}', [ParameterMgmtController::class, 'editTrainingMode'])->name('editTrainingMode');
    Route::post('/parameterManagement/update-trainingMode/{id}', [ParameterMgmtController::class, 'updateTrainingMode'])->name('admin.updateTrainingMode');
    Route::get('/parameterManagement/delete-trainingMode/{id}', [ParameterMgmtController::class, 'deleteTrainingModen'])->name('deleteTrainingModen');


    Route::get('/parameterManagement/trainingType', [ParameterMgmtController::class, 'trainingType'])->name('trainingType');
    Route::get('/parameterManagement/add-trainingType', [ParameterMgmtController::class, 'addTrainingType'])->name('addTrainingType');
    Route::post('/parameterManagement/addnew-trainingType', [ParameterMgmtController::class, 'addNewTrainingType'])->name('addNewTrainingType');
    Route::get('/parameterManagement/edit-trainingType/{id}', [ParameterMgmtController::class, 'editTrainingType'])->name('editTrainingType');
    Route::post('/parameterManagement/update-trainingType/{id}', [ParameterMgmtController::class, 'updateTrainingType'])->name('admin.updateTrainingType');
    Route::get('/parameterManagement/delete-trainingType/{id}', [ParameterMgmtController::class, 'deleteTrainingType'])->name('deleteTrainingType');


    Route::get('/parameterManagement/reType', [ParameterMgmtController::class, 'reType'])->name('reType');
    Route::get('/parameterManagement/add-reType', [ParameterMgmtController::class, 'addReType'])->name('addReType');
    Route::post('/parameterManagement/addnew-reType', [ParameterMgmtController::class, 'addNewReType'])->name('addNewReType');
    Route::get('/parameterManagement/edit-reType/{id}', [ParameterMgmtController::class, 'editReType'])->name('editReType');
    Route::post('/parameterManagement/update-reType/{id}', [ParameterMgmtController::class, 'updateReType'])->name('admin.updateReType');
    Route::get('/parameterManagement/delete-reType/{id}', [ParameterMgmtController::class, 'deleteReType'])->name('deleteReType');
});

//training partner route.
Route::middleware(['auth', 'training-partner:trainingpartner'])->group(function () {

    Route::get('/training-partner', [DashboardController::class, 'trainingPartnerDashboard'])->name('trainingpartner.dashboard');

    // Route::get('/exam/listOfExam', [ExamMgmtController::class, 'examList'])->name('exam-list');
    // Route::get('/exam/updateExamResult/{id}', [ExamMgmtController::class, 'updateExamResult'])->name('update-exam-result');
    // Route::get('/exam/viewExamResult', [ExamMgmtController::class, 'viewExamResult'])->name('view-exam-result');
    // //admin
    // Route::get('/exam/listOfExamAdmin', [ExamMgmtController::class, 'examListAdmin'])->name('exam-list-admin');

    Route::get('/training-partner/training-management', [TrainingMgmtController::class, 'trainingManagementAppTP'])->name('TrainingManagementAppTP');
    Route::get('/training-partner/add-training-registration', [TrainingMgmtController::class, 'trainingRegistrationTP'])->name('addTrainingRegistrationAppTP');
    Route::put('/training-partner/add-training-registration', [TrainingMgmtController::class, 'insertTraining'])->name('storeTrainingRegistrationAppTP');
    Route::get('/training-partner/edit-training-registration/{id}', [TrainingMgmtController::class, 'editTrainingRegistrationAppTP'])->name('editTrainingRegistrationAppTP');
    Route::put('/training-partner/update-training-registration/{id}', [TrainingMgmtController::class, 'updateTrainingRegistrationAppTP'])->name('update-training-registrationAppTP');
    Route::get('/training-partner/training-management-participant-list/{id}', [TrainingApplicationApprovalController::class, 'trainingManagementListParticipantTP'])->name('trainingManagementListParticipantTP');
    Route::get('/training-partner/training-management-participant-pay-list/{id}', [TrainingApplicationApprovalController::class, 'trainingManagementListPayParticipantTP'])->name('trainingManagementListParticipantPayTP');
    Route::get('/training-partner/training-management-participant-details/{id}', [TrainingApplicationApprovalController::class, 'trainingManagementParticipantDetailsTP'])->name('trainingManagementListParticipantTP');
    Route::get('/training-partner/approve-training-participant/{id}', [TrainingMgmtController::class, 'approveApplyTraining'])->name('approveApplyTrainingTP');
    Route::get('/training-partner/reject-training-participant/{id}', [TrainingMgmtController::class, 'rejectApplyTraining'])->name('rejectApplyTrainingTP');
    Route::get('/training-partner/approve-pay-training-participant/{id}', [TrainingApplicationApprovalController::class, 'approvePayTraining'])->name('approvePayTrainingTP');
    Route::get('/training-partner/reject--pay-training-participant/{id}', [TrainingApplicationApprovalController::class, 'rejectPayTraining'])->name('rejectPayTrainingTP');
    //
    // Route::get('/training-partner/get-states/{countryId}', [TrainingMgmtController::class, 'getStates'])->name('getStates');
   Route::get('/training-partner/get-states', [TrainingMgmtController::class, 'getStates'])->name('getStates');

  
    Route::get('/training-partner/question-bank-request-list', [QuestionBankController::class, 'questionbankrequestlist'])->name('questionbankrequestlist');
    Route::get('/training-partner/question-bank-request-new', [QuestionBankController::class, 'questionbankrequestnew'])->name('questionbankrequestnew');
    Route::post('/training-partner/question-bank-request-add', [QuestionBankController::class, 'questionBankRequestAdd'])->name('questionBankRequestAdd');
    Route::get('/training-partner/question-bank-request-view/{id}', [QuestionBankController::class, 'questionbankrequestview'])->name('questionbankrequestview');
    Route::get('/training-partner/question-bank-submission-view/{id}', [QuestionBankController::class, 'questionbanksubmissionview'])->name('questionbanksubmissionview');
    Route::get('/training-partner/question-bank-submission-edit/{id}', [QuestionBankController::class, 'questionBankSubmissionEdit'])->name('questionbanksubmissionedit');
    Route::post('/training-partner/question-bank-submission-update/{id}', [QuestionBankController::class, 'questionBankSubmissionUpdate'])->name('questionbanksubmissionupdate');

    Route::get('/training-partner/question-bank-submission-new', [QuestionBankController::class, 'questionbanksubmissionnew'])->name('questionbanksubmissionnew');
    Route::post('/training-partner/question-bank-submission-add', [QuestionBankController::class, 'questionbanksubmissionadd'])->name('questionbanksubmissionadd');
    Route::get('/training-partner/download/{file}', [QuestionBankController::class, 'download'])->name('download.file');

    Route::get('/training-partner/training-management-list', [TrainingMgmtController::class, 'trainingManagementTP'])->name('training-partner.training-list');
    Route::get('/training-partner/training-management-attendance/{id}', [TrainingMgmtController::class, 'trainingManagementTPAttendance'])->name('training-partner.attendance');
    Route::post('/training-partner/update-attendance', [TrainingMgmtController::class, 'updateAttendance'])->name('training-partner.update-attendance');
    Route::delete('/training-partner/delete-participant/{id}', [TrainingMgmtController::class, 'deleteParticipant'])->name('training-partner.delete-participant');

    Route::get('/training-partner/training-report', [ReportingController::class, 'trainingReportingTP'])->name('training-partner.training-report');

    //flow uat
    Route::get('/training-partner/training-management-feedbacklist/{batch_no}/{id}', [TrainingMgmtController::class, 'TPFeedbackParticipant'])->name('TP-feedbacklist');
    Route::get('/training-partner/training-management-feedback/{batch_no}/{id}', [TrainingMgmtController::class, 'trainingManagementFeedbackQuestion'])->name('trainingpartner.feedbackquestion');
    Route::get('training-partner/training-management/submitExamResult/{batch_no}/{id}', [ExamMgmtController::class, 'endorseExamResultTP'])->name('endorse-exam-resultTP');

    Route::get('/training-partner/training-management/training-report/{batch_no}/{id}', [TrainingMgmtController::class, 'TPTrainingReport'])->name('TP-TrainingReport-TM');
    Route::get('/training-partner/training-management/trainingReportParticipant/{batch_no}/{id}', [TrainingMgmtController::class, 'TPTrainingReportParticipant'])->name('TP-TrainingReportParticipant');

    Route::get('/training-partner/training-management/attendance-list/{batch_no}/{id}', [TrainingMgmtController::class, 'TPAttendanceList'])->name('TP.attendance');
    Route::get('/training-partner/training-management/participant-feedback/{batch_no}/{id}', [TrainingMgmtController::class, 'participantFeedback'])->name('view-participant-feedback');

    Route::get('/training-partner/training-management/TPPayment-seda/{batch_no}/{id}', [TrainingMgmtController::class, 'TPPaymentToSeda'])->name('TP-Payment-to-seda');



    //exam
    Route::get('/exam/listOfExam', [ExamMgmtController::class, 'examList'])->name('exam-list');
    Route::get('/exam/updateExamResult/{id}/{batch_no_full}', [ExamMgmtController::class, 'updateExamResult'])->name('update-exam-result');
    // Route::get('/exam/submitExamResult/{id}/{batch_no_full}', [ExamMgmtController::class, 'updateExamResult'])->name('update-exam-result');

    //dashboard
    Route::get('dashboard/training-partner/attendance-list/{id}', [DashboardController::class, 'TPAttendanceList'])->name('TP-Attendance-List');
    Route::get('dashboard/training-partner/update-exam-list/{id}', [DashboardController::class, 'TPUpdateExamResult'])->name('TP-UpdateExamResult-List');
    Route::get('dashboard/training-partner/training-report/{id}', [DashboardController::class, 'TPTrainingReport'])->name('TP-TrainingReport');
    Route::get('dashboard/training-partner/training-schedule-history', [DashboardController::class, 'TPTrainingScheduleHistory'])->name('TP-training-schedule-history');
});


//Training Schedule - Trainer Dashboard
Route::middleware(['auth', 'trainer:trainer'])->group(function () {

    Route::get('/trainer/event', [DashboardController::class, 'trainerDashboard'])->name('trainer.dashboard');

    Route::get('dashboard/trainer/attendance-list/{batch_no}/{id}', [DashboardController::class, 'trainerAttendanceList'])->name('trainer-AttendanceList');
    Route::get('dashboard/trainer/update-exam-list/{id}', [DashboardController::class, 'trainerUpdateExamResult'])->name('trainer-UpdateExamResult');
    Route::get('dashboard/trainer/training-report/{id}', [DashboardController::class, 'trainerTrainingReport'])->name('trainer-TrainingReport');
    Route::get('dashboard/trainer/training-schedule-history', [DashboardController::class, 'trainerTrainingScheduleHistory'])->name('trainer-training-schedule-history');
});

// Participant Dashboard
Route::middleware(['auth', 'participant:participant'])->group(function () {
    Route::get('dashboard/participant', [DashboardController::class, 'participantDashboard'])->name('participant.dashboard');
    Route::get('dashboard/participant/apply/{id}', [DashboardController::class, 'participantApply'])->name('participant-Apply');
    Route::get('dashboard/participant/trainingDetails/{id}', [DashboardController::class, 'participantTrainingDetails'])->name('participant-Training-Details');
    Route::get('dashboard/participant/paymentDetails/{id}', [DashboardController::class, 'participantPaymentDetails'])->name('participant-Payment-Details');
//    Route::post('dashboard/participant/pay', [DashboardController::class, 'participantPaymentInvoice'])->name('participant-Payment-Invoice');
    Route::get('dashboard/participant/paymentDetailsInvoice/{id}', [DashboardController::class, 'participantPaymentInvoice'])->name('participant-Payment-Invoice');
    Route::get('dashboard/participant/participantToMakePayment/{id}', [DashboardController::class, 'participantToMakePayment'])->name('participant-To-Payment');
    Route::post('dashboard/participant/apply-for-training/{id}', [DashboardController::class, 'applyForTraining'])->name('applyForTraining');
    Route::post('dashboard/participant/paymentGateway/{id}', [DashboardController::class, 'processPayment'])->name('process_payment');
    Route::get('dashboard/participant/participant-feedback/{id}', [DashboardController::class, 'participantFeedback'])->name('participant-feedback');
    Route::post('dashboard/participant/add-participant-feedback', [DashboardController::class, 'addParticipantFeedback'])->name('add-participant-feedback');
    Route::get('dashboard/participant/generate-certificate/{id}', [CertificateController::class, 'generateCertificate'])->name('generateCertificate');
    Route::get('dashboard/participant/generate-certificateQP/{id}', [CertificateController::class, 'generateCertificateQP'])->name('generateCertificateQP');
});